﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PRCNCORE.RoleSubMenus
{
    public class RoleSubMenu
    {

        private int menuId;

        public int MenuId
        {
            get { return this.menuId; }
        }

        private string menuName;

        public string MenuName
        {
            get { return this.menuName; }
        }

        private int subMenuId;

        public int SubMenuId
        {
            get { return this.subMenuId; }
        }

        private string subMenuName;

        public string SubMenuName
        {
            get { return this.subMenuName; }
        }

        private string subMenuURL;

        public string SubMenuURL
        {
            get { return this.subMenuURL; }
        }

        private int canAdd;

        public int CanAdd
        {
            get { return this.canAdd; }
        }

        private int canDelete;

        public int CanDelete
        {
            get { return this.canDelete; }
        }

        private int canEdit;

        public int CanEdit
        {
            get { return this.canEdit; }
        }

        private string className;
        public string ClassName
        {
            get { return this.className; }
        }

        private string smallClassColor;
        public string SmallClassColor
        {
            get { return this.smallClassColor; }
        }

        private string smallClassName;
        public string SmallClassName
        {
            get { return this.smallClassName; }
        }

        internal RoleSubMenu(int menuId, string menuName, int subMenuId, string subMenuName, string subMenuURL,int canAdd, int canDelete, int canEdit, string className, string smallClassName, string smallClassColor)
        {
            this.menuId = menuId;
            this.menuName = menuName;
            this.subMenuName = subMenuName;
            this.subMenuId = subMenuId;
            this.subMenuURL = subMenuURL;
            this.canEdit = canEdit;
            this.canDelete = canDelete;
            this.canAdd = canAdd;
            this.className = className;
            this.smallClassName = smallClassName;
            this.smallClassColor = smallClassColor;
        }
    
    }

    public class RoleSubMenuContext
    {

        private int menuId;

        public int MenuId
        {
            get { return this.menuId; }
        }

        private string menuName;

        public string MenuName
        {
            get { return this.menuName; }
        }

        private int subMenuId;

        public int SubMenuId
        {
            get { return this.subMenuId; }
        }

        private string subMenuName;

        public string SubMenuName
        {
            get { return this.subMenuName; }
        }

        private string subMenuURL;

        public string SubMenuURL
        {
            get { return this.subMenuURL; }
        }

        private string roleActionName;

        public string RoleActionName
        {
            get { return this.roleActionName; }
        }

        private string uniqueId;

        public string UniqueId
        {
            get { return this.uniqueId; }
        }

        private int roleActionValue;

        public int RoleActionValue
        {
            get { return this.roleActionValue; }
        }

        private string className;
        public string ClassName
        {
            get { return this.className; }
        }

        private string smallClassColor;
        public string SmallClassColor
        {
            get { return this.smallClassColor; }
        }

        private string smallClassName;
        public string SmallClassName
        {
            get { return this.smallClassName; }
        }

        private string roleLabel;
        public string RoleLabel
        {
            get { return this.roleLabel; }
        }

        private int status;
        public int Status
        {
            get { return this.status; }
        }
        private int oldRoleActionValue;
        public int OldRoleActionValue
        {
            get { return this.oldRoleActionValue; }
        }
        internal RoleSubMenuContext(
            int menuId, string menuName, int subMenuId, string subMenuName, 
            string subMenuURL, string roleActionName, string uniqueId, int roleActionValue, 
            string className, string smallClassName, string smallClassColor, string roleLabel, int status, int oldRoleActionValue)
        {
            this.menuId = menuId;
            this.menuName = menuName;
            this.subMenuName = subMenuName;
            this.subMenuId = subMenuId;
            this.subMenuURL = subMenuURL;
            this.roleActionName = roleActionName;
            this.uniqueId = uniqueId;
            this.roleActionValue = roleActionValue;
            this.className = className;
            this.smallClassName = smallClassName;
            this.smallClassColor = smallClassColor;
            this.roleLabel = roleLabel;
            this.status = status;
            this.oldRoleActionValue = oldRoleActionValue;

        }

    }
}
