﻿using PRCNCORE.Backend;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace PRCNCORE.RoleSubMenus
{
    public class SubMenuContext
    {
        public SubMenuContext(string Icon, string Text, string Action, bool IsVisible)
        {
            this.Icon = Icon;
            this.Text = Text;
            this.Action = Action;
            this.IsVisible = IsVisible;
        }
        public string Icon {get;set;}
        public string Text {get;set;}
        public string Action { get; set; }
        public bool IsVisible { get; set; }
    }

    public class RoleSubMenuCollection : ReadOnlyCollection<RoleSubMenu>
    {
        internal RoleSubMenuCollection()
            : base(new List<RoleSubMenu>())
        {

        }

        public void Add(RoleSubMenu roleSubMenu)
        {
            if (roleSubMenu != null && !this.Contains(roleSubMenu))
            {
                this.Items.Add(roleSubMenu);
            }
        }

        public JObject GetJSONJObject()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (RoleSubMenu item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("Roles", jArray);

            return jObjectDis;
        }

        public JArray GetJSONJArray()
        {
            JArray jArray = new JArray();

            foreach (RoleSubMenu item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            return jArray;
        }
    }

    public class RoleSubMenuContextCollection : ReadOnlyCollection<RoleSubMenuContext>
    {
        internal RoleSubMenuContextCollection()
            : base(new List<RoleSubMenuContext>())
        {

        }

        public void Add(RoleSubMenuContext roleSubMenu)
        {
            if (roleSubMenu != null && !this.Contains(roleSubMenu))
            {
                this.Items.Add(roleSubMenu);
            }
        }

        public JObject GetJSONJObject()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (RoleSubMenuContext item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("Roles", jArray);

            return jObjectDis;
        }

        public JArray GetJSONJArray()
        {
            JArray jArray = new JArray();

            foreach (RoleSubMenuContext item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            return jArray;
        }
    }
}

