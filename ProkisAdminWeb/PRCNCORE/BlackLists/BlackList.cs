﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE
{
    public class BlackList
    {
        private int id;
        private string name;
        private string tckn;
        private string birthYear;
        private string description;
        private string insertionDate;
        private string createdUser;
        private int status;

        internal BlackList(int id,
                           string name,
                           string tckn,
                           string birthYear,
                           string description,
                           string insertionDate,
                           string createdUser,
                           int status)
        {

            this.id = id;
            this.name = name;
            this.tckn = tckn;
            this.birthYear = birthYear;
            this.description = description;
            this.insertionDate = insertionDate;
            this.createdUser = createdUser;
            this.status = status;
        }

        public int Id
        {
            get
            {
                return this.id;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
        }

        public string Tckn
        {
            get
            {
                return this.tckn;
            }
        }

        public string BirthYear
        {
            get
            {
                return this.birthYear;
            }
        }

        public string Description
        {
            get
            {
                return this.description;
            }
        }

        public string InsertionDate
        {
            get
            {
                return this.insertionDate;
            }
        }

        public string CreatedUSer
        {
            get
            {
                return this.createdUser;
            }
        }

        public int Status
        {
            get
            {
                return this.status;
            }
        }
    }
}
