﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Cities
{
    public class CityCollection : ReadOnlyCollection<City>
    {
        internal CityCollection()
            : base(new List<City>())
        {

        }

        internal void Add(City status)
        {
            if (status != null && !this.Contains(status))
            {
                this.Items.Add(status);
            }
        }

        public JObject GetJSONJObject()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (City item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("Cities", jArray);

            return jObjectDis;
        }

        public JArray GetJSONJArray()
        {
            JArray jArray = new JArray();

            foreach (City item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            return jArray;
        }
    }
}