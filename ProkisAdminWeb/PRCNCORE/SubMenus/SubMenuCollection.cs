﻿using PRCNCORE.Backend;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.SubMenus
{
    public class SubMenuCollection: ReadOnlyCollection<SubMenu>
    {
        internal SubMenuCollection()
             : base(new List<SubMenu>())
        {

        }

        internal void Add(SubMenu menu)
        {
            if (menu != null && !this.Contains(menu))
            {
                this.Items.Add(menu);
            }
        }

        public JObject GetJSONJObject()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (SubMenu item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("SubMenus", jArray);

            return jObjectDis;
        }

        public JArray GetJSONJArray()
        {
            JArray jArray = new JArray();

            foreach (SubMenu item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            return jArray;
        }
    }
}

