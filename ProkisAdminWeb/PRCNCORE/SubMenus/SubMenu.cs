﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.SubMenus
{
    public class SubMenu
    {
        private int id;

        public int Id
        {
            get { return this.id; }
        }

        private string name;

        public string Name
        {
            get { return this.name; }
        }

        private int menuId;

        public int MenuId
        {
            get { return this.menuId; }
        }

        internal SubMenu(int id, string name, int menuId)
        {
            this.id = id;
            this.name = name;
            this.menuId = menuId;
        }
    }
}
