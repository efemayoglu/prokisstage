﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.KioskConditions
{
    public class KioskConditionDetailCollection : ReadOnlyCollection<KioskConditionDetail>
    {
        internal KioskConditionDetailCollection()
            : base(new List<KioskConditionDetail>())
        {

        }

        internal void Add(KioskConditionDetail kiosk)
        {
            if (kiosk != null && !this.Contains(kiosk))
            {
                this.Items.Add(kiosk);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (KioskConditionDetail item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("kioskConditionDetail", jArray);

            return jObjectDis;
        }

    }
}

