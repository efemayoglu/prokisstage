﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.KioskConditions
{
    public class KioskCondition
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private int kioskId;
        public int KioskId
        {
            get { return kioskId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string kioskAddress;
        public string KioskAddress
        {
            get { return kioskAddress; }
        }

        //private string updatedDate;
        //public string UpdatedDate
        //{
        //    get { return updatedDate; }
        //}

        private string amount;
        public string Amount
        {
            get { return amount; }
        }

        private int cashCount;
        public int CashCount
        {
            get { return cashCount; }
        }

        private int coinCount;
        public int CoinCount
        {
            get { return coinCount; }
        }

        private int ribbonRate;
        public int RibbonRate
        {
            get { return ribbonRate; }
        }

        private string cashBoxAcceptorTypeNamedataRow;
        public string CashBoxAcceptorTypeNamedataRow
        {
            get { return cashBoxAcceptorTypeNamedataRow; }
        }

        private string capacity;
        public string Capacity
        {
            get { return capacity; }
        }

        private string explanation;
        public string Explanation
        {
            get { return explanation; }
        }

        private string dispenserCount;
        public string DispenserCount
        {
            get { return dispenserCount; }
        }

        private string dispenserAmount;
        public string DispenserAmount
        {
            get { return dispenserAmount; }
        }

        private int resultCount;
        public int ResultCount
        {
            get { return resultCount; }
        }

        private string className;
        public string ClassName
        {
            get { return className; }
        }

        internal KioskCondition(int resultCount
            , int id
            , int kioskId
            , string kioskName
            , string kioskAddress
           // , string updatedDate
            , string amount
            , int cashCount
            , int coinCount
            , int ribbonRate
            , string capacity
            , string cashBoxAcceptorTypeNamedataRow
            , string explanation
            , string dispenserCount
            , string dispenserAmount
            , string className
            )
        {
            this.id = id;
            this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.kioskAddress = kioskAddress;
           // this.updatedDate = updatedDate;
            this.amount = amount;
            this.cashCount = cashCount;
            this.coinCount = coinCount;
            this.ribbonRate = ribbonRate;
            this.capacity = capacity;
            this.cashBoxAcceptorTypeNamedataRow = cashBoxAcceptorTypeNamedataRow;
            this.explanation = explanation;
            this.dispenserCount = dispenserCount;
            this.dispenserAmount = dispenserAmount;
            this.resultCount = resultCount;
            this.className = className;
        }
    }
}