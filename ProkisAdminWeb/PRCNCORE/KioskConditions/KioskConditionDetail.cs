﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.KioskConditions
{
    public class KioskConditionDetail
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private int kioskId;
        public int KioskId
        {
            get { return kioskId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string updatedDate;
        public string UpdatedDate
        {
            get { return updatedDate; }
        }

        private string amount;
        public string Amount
        {
            get { return amount; }
        }

        private int cashCount;
        public int CashCount
        {
            get { return cashCount; }
        }

        private int coinCount;
        public int CoinCount
        {
            get { return coinCount; }
        }

        private int ribbonLen;
        public int RibbonLen
        {
            get { return ribbonLen; }
        }

        private string receivedAmount;
        public string ReceivedAmount
        {
            get { return receivedAmount; }
        }

        private int cashTypeId;
        public int CashTypeId
        {
            get { return cashTypeId; }
        }

        private string cashTypeName;
        public string CashTypeName
        {
            get { return cashTypeName; }
        }

        private string aboneNo;
        public string AboneNo
        {
            get { return aboneNo; }
        }

        private int transactionId;
        public int TransactionId
        {
            get { return transactionId; }
        }

      


        internal KioskConditionDetail(int id
            , int kioskId
            , string kioskName
            , string updatedDate
            , string amount
            , int cashCount
            , int coinCount
            , int ribbonLen
            , string receivedAmount
            , int cashTypeId
            , string cashTypeName
            , string aboneNo
            , int transactionId
            )
        {
            this.id = id;
            this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.updatedDate = updatedDate;
            this.amount = amount;
            this.cashCount = cashCount;
            this.coinCount = coinCount;
            this.ribbonLen = ribbonLen;
            this.receivedAmount = receivedAmount;
            this.cashTypeId = cashTypeId;
            this.cashTypeName = cashTypeName;
            this.aboneNo = aboneNo;
            this.transactionId = transactionId;
            
            
        }
    }
}
