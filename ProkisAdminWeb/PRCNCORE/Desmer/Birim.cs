﻿using Newtonsoft.Json;

namespace PRCNCORE.Desmer
{
    public class Birim
    {
        [JsonProperty("kod")]
        public string Kod { get; set; }

        [JsonProperty("adi")]
        public string Adı { get; set; }
        
        [JsonProperty("ilKod")]
        public string IlKod { get; set; }
        
        [JsonProperty("il")]
        public string Il { get; set; }
    }
}
