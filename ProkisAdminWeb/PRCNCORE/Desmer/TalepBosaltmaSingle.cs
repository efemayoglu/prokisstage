﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using PRCNCORE.Input;

namespace PRCNCORE.Desmer
{
    public class TalepBosaltmaSingle
    {
        public TalepBosaltmaSingle()
        {
            TalepTarih = DateTime.Now;
        }

        [JsonProperty("referansId")]
        public string ReferansId { get; set; }

        [JsonProperty("hizmetNoktaKod")]
        public string HizmetNoktaKod { get; set; }

        [JsonProperty("nomKod")]
        public string NomKod { get; set; }

        [JsonProperty("tutar")]
        public decimal Tutar { get; set; }

        [JsonProperty("tutarParaBirimi")]
        public string TutarParaBirimi { get; set; }

        [JsonProperty("talepTipi")]
        public string TalepTipi { get; set; }

        [JsonProperty("arizaTipi")]
        public string ArizaTipi { get; set; }

        [JsonProperty("talepTarih")]
        public DateTime? TalepTarih { get; set; }

        [JsonProperty("aciklama")]
        public string Aciklama { get; set; }

        [JsonProperty("kupurler")]
        public List<Kupur> Kupurler { get; set; }

        [JsonProperty("barkodlar")]
        public List<Barkod> Barkodlar { get; set; }

        [JsonProperty("reference")]
        public InputSaveKioskReferenceSystem Reference { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

    }
}