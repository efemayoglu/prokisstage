﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace PRCNCORE.Desmer
{
    public class DateRange
    {
        [JsonProperty("startDate")]
        public DateTime StartDate { get; set; }

        [JsonProperty("endDate")]
        public DateTime EndDate { get; set; }
    }
}