﻿using Newtonsoft.Json;

namespace PRCNCORE.Desmer
{
  public class NoktaInput
  {
    [JsonProperty("islemTipi")]
    public int IslemTipi { get; set; }
    
    [JsonProperty("kod")]
    public string Kod { get; set; }
    
    [JsonProperty("adi")]
    public string Adı { get; set; }
    
    [JsonProperty("grupKod")]
    public string GrupKod { get; set; }
    
    [JsonProperty("ilKod")]
    public string IlKod { get; set; }
    
    [JsonProperty("birimKod")]
    public string BirimKod { get; set; }
    
    [JsonProperty("adres")]
    public string Adres { get; set; }
    
    [JsonProperty("telefon")]
    public string Telefon { get; set; }
    
    [JsonProperty("durum")]
    public bool Durum { get; set; }
    
    [JsonProperty("result")]
    public string Result { get; set; }
  }
}