﻿using Newtonsoft.Json;

namespace PRCNCORE.Desmer
{
  public class Grup
  {
    [JsonProperty("kod")]
    public string Kod { get; set; }

    [JsonProperty("adi")]
    public string Adı { get; set; }
  }
}