﻿using Newtonsoft.Json;
using PRCNCORE.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Desmer
{
    public class AddTalepBosaltmaInput
    {

        [JsonProperty("taleps")]
        public List<TalepBosaltmaSingle> TalepReferenceInputs { get; set; }

    }
}
