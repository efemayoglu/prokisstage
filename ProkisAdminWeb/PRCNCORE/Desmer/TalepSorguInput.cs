﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace PRCNCORE.Desmer
{
    public class TalepSorguInput
    {
        [JsonProperty("islemTipi")]
        public int IslemTipi { get; set; }

        [JsonProperty("referansId")]
        public string ReferansId { get; set; }

        [JsonProperty("hizmetNoktaKod")]
        public string HizmetNoktaKod { get; set; }

        [JsonProperty("nomKod")]
        public string NomKod { get; set; }

        [JsonProperty("tutar")]
        public decimal Tutar { get; set; }

        [JsonProperty("tutarParaBirimi")]
        public string TutarParaBirimi { get; set; }

        [JsonProperty("talepTipi")]
        public string TalepTipi { get; set; }

        [JsonProperty("arizaTipi")]
        public string ArizaTipi { get; set; }

        [JsonProperty("talepDurum")]
        public int TalepDurum { get; set; }

        [JsonProperty("talepTarih")]
        public DateTime TalepTarih { get; set; }

        [JsonProperty("aciklama")]
        public string Aciklama { get; set; }

        [JsonProperty("kupurler")]
        public List<Kupur> Kupurler { get; set; }

        [JsonProperty("barkodlar")]
        public List<Barkod> Barkodlar { get; set; }

        [JsonProperty("aracPlaka")]
        public string AracPlaka { get; set; }

        [JsonProperty("result")]
        public string Result { get; set; }
    }
}