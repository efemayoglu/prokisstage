﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using PRCNCORE.Parser.Other;

namespace PRCNCORE.Desmer
{
    public class Talep
    {
        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("className")]
        public string StatusColor { get; set; }

        [JsonProperty("islemTipi")]
        public int IslemTipi { get; set; }

        [JsonProperty("islemTipiDetay")]
        public string IslemTipiDetay { get; set; }

        [JsonProperty("referansId")]
        public string ReferansId { get; set; }

        [JsonProperty("hizmetNoktaKod")]
        public string HizmetNoktaKod { get; set; }

        [JsonProperty("hizmetNoktaName")]
        public string HizmetNoktaName { get; set; }

        [JsonProperty("nomKod")]
        public string NomKod { get; set; }

        [JsonProperty("tutar")]
        public decimal Tutar { get; set; }

        [JsonProperty("tutarParaBirimi")]
        public string TutarParaBirimi { get; set; }

        [JsonProperty("talepTipi")]
        public string TalepTipi { get; set; }

        [JsonProperty("arizaTipi")]
        public string ArizaTipi { get; set; }

        [JsonProperty("talepDurum")]
        public int TalepDurum { get; set; }

        [JsonProperty("talepDurumDetay")]
        public string TalepDurumDetay { get; set; }

        [JsonProperty("talepTarih")]
        public DateTime? TalepTarih { get; set; }

        [JsonProperty("aciklama")]
        public string Aciklama { get; set; }

        [JsonProperty("kupurler")]
        public List<Kupur> Kupurler { get; set; }

        [JsonProperty("barkodlar")]
        public List<Barkod> Barkodlar { get; set; }

        [JsonProperty("personneller")]
        public List<Personnel> Personneller { get; set; }

        [JsonProperty("aracPlaka")]
        public string AracPlaka { get; set; }

        [JsonProperty("result")]
        public string Result { get; set; }

        [JsonProperty("fulfilId")]
        public string FulfilId { get; set; }


    }

    public class Barkod
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("barkod")]
        public string BarkodL { get; set; }
    }

    public class Personnel
    {
        [JsonProperty("tcKimlik")]
        public string TcKimlik { get; set; }

        [JsonProperty("adiSoyadi")]
        public string AdiSoyadi { get; set; }
    }
}