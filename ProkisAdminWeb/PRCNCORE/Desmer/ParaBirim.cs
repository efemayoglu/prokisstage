﻿using Newtonsoft.Json;

namespace PRCNCORE.Desmer
{
  public class ParaBirim
  {
    [JsonProperty("kod")]
    public string Kod { get; set; }
    
    [JsonProperty("deger")]
    public double Deger { get; set; }
    
    [JsonProperty("tur")]
    public string Tur { get; set; }
    
  }
}