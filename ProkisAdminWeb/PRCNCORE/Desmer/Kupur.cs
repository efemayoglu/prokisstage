﻿using Newtonsoft.Json;

namespace PRCNCORE.Desmer
{
    public class Kupur
    {
        [JsonProperty("kupurKod")]
        public string KupurKod { get; set; }

        [JsonProperty("adet")]
        public int Adet { get; set; }
    }
}