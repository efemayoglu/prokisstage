﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace PRCNCORE.Desmer
{
    public class TaleplerInput
    {
        [JsonProperty("startDate")]
        public DateTime StartDate { get; set; }

        [JsonProperty("endDate")]
        public DateTime EndDate { get; set; }

        [JsonProperty("islemTipi")]
        public int IslemTipi { get; set; }

        [JsonProperty("talepDurum")]
        public int TalepDurum { get; set; }

        [JsonProperty("talepTipi")]
        public string TalepTipi { get; set; }

        [JsonProperty("referansId")]
        public string ReferansId { get; set; }

        [JsonProperty("ilKod")]
        public string IlKod { get; set; }

        [JsonProperty("kioskList")]
        public List<string> KioskList { get; set; }
    }
}