﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Users
{
    public class RoleAction
    {
        public int id { get; set; }

        public string roleName { get; set; }
        public bool status { get; set; }
        public string roleLabel { get; set; }

        internal RoleAction(int id, string roleName, bool status, string roleLabel)
        {
            this.id = id;
            this.roleName = roleName;
            this.status = status;
            this.roleLabel = roleLabel;
        }

        public int Id
        {
            get
            {
                return this.id;
            }
        }

        public string RoleName
        {
            get
            {
                return this.roleName;
            }
        }

        public bool Status
        {
            get
            {
                return this.status;
            }
        }

        public string RoleLabel
        {
            get
            {
                return this.roleLabel;
            }
        }

    }
}
