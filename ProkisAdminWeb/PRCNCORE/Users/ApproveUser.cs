﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Users
{
    public class ApproveUser
    {
        private int id;
        private string name;
        private string username;
        private string email;
        private string password;
        private string telephone;
        private string cellphone;
        private string statusName;
        private string roleName;
        private string updatedDate;
        private string updatedUser;

         internal ApproveUser(int id,
                         string name,
                         string username,
                         string email,
                         string password,
                         string telephone,
                         string cellphone,
                         string statusName,
                         string roleName,
                         string updatedDate,
                         string updatedUser)

        {

            this.id =id;
            this.name = name;
            this.username = username;
            this.email = email;
            this.password= password;
            this.telephone=telephone;
            this.cellphone=cellphone;
            this.statusName=statusName;
            this.roleName=roleName;
            this.updatedDate = updatedDate;
            this.updatedUser = updatedUser;
        }

         public int Id
         {
             get
             {
                 return this.id;
             }
         }

         public string Name
         {
             get
             {
                 return this.name;
             }
         }

         public string Username
         {
             get
             {
                 return this.username;
             }
         }

         public string Email
         {
             get
             {
                 return this.email;
             }
         }


         public string Password
         {
             get
             {
                 return this.password;
             }
         }

         public string Telephone
         {
             get
             {
                 return this.telephone;
             }
         }

         public string Cellphone
         {
             get
             {
                 return this.cellphone;
             }
         }


         public string StatusName
         {
             get
             {
                 return this.statusName;
             }
         }

         public string RoleName
         {
             get
             {
                 return this.roleName;
             }
         }

         public string UpdatedDate
         {
             get
             {
                 return this.updatedDate;
             }
         }

         public string UpdatedUser
         {
             get
             {
                 return this.updatedUser;
             }
         }
    }
}
