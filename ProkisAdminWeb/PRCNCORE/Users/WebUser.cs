﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Users
{
    public class WebUser
    {
        private int id;
        private string name;
        private string username;
        private string email;
        private string password;
        private string telephone;
        private string cellphone;
        private int userStatusId;
        private string statusName;
        private int roleId;
        private string roleName;
        private DateTime createdDate;
        private DateTime lastLoginDate;
        private int passwordMustChangeId;
        private DateTime lastPasswordChangeDate;
        private string lastPasswords;
        private int wrongAccessTryCount;
        private DateTime lastWrongAccessTryDate;
        private string profileImagePath;
        private string profileFilePath;
        private string professionTitle;
        private string mainPage;
        // Methods
        internal WebUser(int id,
                         string name,
                         string username,
                         string email,
                         string password,
                         string telephone,
                         string cellphone,
                         int userStatusId,
                         string statusName,
                         int roleId,
                         string roleName,
                         DateTime createdDate,
                         DateTime lastLoginDate,
                         int passwordMustChangeId,
                         DateTime lastPasswordChangeDate,
                         string lastPasswords,
                         int wrongAccessTryCount,
                         DateTime lastWrongAccessTryDate,
                         string profileImagePath,
                         string profileFilePath,
                         string professionTitle,
                         string mainPage
            )                   
        {                      

            this.id =id;
            this.name = name;
            this.username = username;
            this.email = email;
            this.password= password;
            this.telephone=telephone;
            this.cellphone=cellphone;
            this.userStatusId=userStatusId;
            this.statusName=statusName;
            this.roleId=roleId;
            this.roleName=roleName;
            this.createdDate = createdDate;
            this.lastLoginDate = lastLoginDate;
            this.passwordMustChangeId = passwordMustChangeId;
            this.lastPasswordChangeDate = lastPasswordChangeDate;
            this.lastPasswords = lastPasswords;
            this.wrongAccessTryCount = wrongAccessTryCount;
            this.lastWrongAccessTryDate = lastWrongAccessTryDate;
            this.profileImagePath=profileImagePath;
            this.profileFilePath=profileFilePath;
            this.professionTitle=professionTitle;
            this.mainPage = mainPage;
        }

        // Properties
        public string ProfileImagePath
        {
            get
            {
                return this.profileImagePath;
            }
        }
        public string ProfileFilePath
        {
            get
            {
                return this.profileFilePath;
            }
        }
        public string ProfessionTitle
        {
            get
            {
                return this.professionTitle;
            }
        }
        public int Id
        {
            get
            {
                return this.id;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
        }

        public string MainPage
        {
            get
            {
                return this.mainPage;
            }
        }


        public string Username
        {
            get
            {
                return this.username;
            }
        }

        public string Email
        {
            get
            {
                return this.email;
            }
        }


        public string Password
        {
            get
            {
                return this.password;
            }
        }

        public string Telephone
        {
            get
            {
                return this.telephone;
            }
        }

        public string Cellphone
        {
            get
            {
                return this.cellphone;
            }
        }

        public int UserStatusId
        {
            get
            {
                return this.userStatusId;
            }
        }

        public string StatusName
        {
            get
            {
                return this.statusName;
            }
        }

        public int RoleId
        {
            get
            {
                return this.roleId;
            }
        }

        public string RoleName
        {
            get
            {
                return this.roleName;
            }
        }

        public DateTime CreatedDate
        {
            get
            {
                return this.createdDate;
            }
        }

        public DateTime LastLoginDate
        {
            get
            {
                return this.lastLoginDate;
            }
        }

        public int PasswordMustChangeId
        {
            get
            {
                return this.passwordMustChangeId;
            }
        }

        public DateTime LastPasswordChangeDate
        {
            get
            {
                return this.lastPasswordChangeDate;
            }
        }

        public string LastPasswords
        {
            get
            {
                return this.lastPasswords;
            }
        }

        public int WrongAccessTryCount
        {
            get
            {
                return this.wrongAccessTryCount;
            }
        }

        public DateTime LastWrongAccessTryDate
        {
            get
            {
                return this.lastWrongAccessTryDate;
            }
        }

    }
}