﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputSaveKioskReferenceSystem
    {
        [JsonProperty("adminUserId")]
        public int adminUserId { get; set; }

        [JsonProperty("kioskId")]
        public string kioskId { get; set; }

        [JsonProperty("referenceNo")]
        public string referenceNo { get; set; }

        [JsonProperty("explanation")]
        public string explanation { get; set; }

    }
    public class InputGetKioskReferenceSystem
    {
        [JsonProperty("referenceNo")]
        public int referenceNo { get; set; }

        public int Id { get; set; }

        public int InstitutionId { get; set; }

        public DateTime InsertionDate { get; set; }

        public string Status { get; set; }

        public string Explanation { get; set; }

        public int UserId { get; set; }

        public int ReferenceNo { get; set; }

        public int DesmerId { get; set; }
    }
}
