﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputGetEmptyKioskName
    {
        [JsonProperty("expertUserId")]
        public int expertUserId { get; set; }
    }
}
