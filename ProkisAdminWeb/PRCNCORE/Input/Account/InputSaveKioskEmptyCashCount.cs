﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputSaveKioskEmptyCashCount
    {
        [JsonProperty("expertUserId")]
        public int expertUserId { get; set; }

        [JsonProperty("kioskId")]
        public int kioskId { get; set; }

        [JsonProperty("cashTypeId")]
        public int cashTypeId { get; set; }

        [JsonProperty("count")]
        public int count { get; set; }

        [JsonProperty("kioskEmptyId")]
        public int kioskEmptyId { get; set; }

        [JsonProperty("desmerId")]
        public int desmerId { get; set; }
    }
}
