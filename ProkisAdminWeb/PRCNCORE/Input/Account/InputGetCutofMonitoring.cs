﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Account
{
    public class InputGetCutofMonitoring
    {
        [JsonProperty("cutId")]
        public int cutId { get; set; }
    }
}
