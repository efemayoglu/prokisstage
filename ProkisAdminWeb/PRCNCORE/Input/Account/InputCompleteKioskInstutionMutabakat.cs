﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputCompleteKioskInstutionMutabakat
    {
        [JsonProperty("userId")]
        public int userId { get; set; }

        [JsonProperty("mutabakatDate")]
        public DateTime mutabakatDate { get; set; }

        [JsonProperty("kioskId")]
        public int kioskId { get; set; }

        [JsonProperty("instutionId")]
        public int instutionId { get; set; }

        [JsonProperty("faturaCount")]
        public int faturaCount { get; set; }

        [JsonProperty("faturaAmount")]
        public string faturaAmount { get; set; }

        [JsonProperty("kartDolumCount")]
        public int kartDolumCount { get; set; }

        [JsonProperty("kartDolumAmount")]
        public string kartDolumAmount { get; set; }

        [JsonProperty("totalCount")]
        public int totalCount { get; set; }

        [JsonProperty("totalAmount")]
        public string totalAmount { get; set; }

        [JsonProperty("instutionCount")]
        public int instutionCount { get; set; }

        [JsonProperty("instutionAmount")]
        public string instutionAmount { get; set; }

        [JsonProperty("whichSide")]
        public string whichSide { get; set; }

        [JsonProperty("cancelAmount")]
        public string cancelAmount { get; set; }

        [JsonProperty("cancelCount")]
        public string cancelCount { get; set; }

    }
}
