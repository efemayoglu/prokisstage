﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Account
{
    public class InputListSaveKioskSettings
    {
        [JsonProperty("billFieldNotificationLimit")]
        public int billFieldNotificationLimit { get; set; }

        [JsonProperty("kioskEmptyId")]
        public int kioskEmptyId { get; set; }

        [JsonProperty("inputAmount")]
        public string inputAmount { get; set; }

        [JsonProperty("outputAmount")]
        public string outputAmount { get; set; }

        [JsonProperty("explanation")]
        public string explanation { get; set; }

        
    }
}
