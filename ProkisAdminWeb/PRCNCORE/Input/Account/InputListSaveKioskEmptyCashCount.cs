﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputListSaveKioskEmptyCashCount
    {
        [JsonProperty("cashCounts")]
        public List<InputSaveKioskEmptyCashCount> cashCounts { get; set; }

        [JsonProperty("expertUserId")]
        public int expertUserId { get; set; }

        [JsonProperty("kioskId")]
        public int kioskId { get; set; }

        [JsonProperty("emptyKioskId")]
        public int emptyKioskId { get; set; }

        [JsonProperty("code")]
        public string code { get; set; }

    }
}
