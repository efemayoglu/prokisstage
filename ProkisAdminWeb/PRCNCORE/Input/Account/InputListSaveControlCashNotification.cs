﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Account
{
    public class InputListSaveControlCashNotification
    {
        

        [JsonProperty("adminUserId")]
        public int expertUserId { get; set; }

        [JsonProperty("kioskId")]
        public int kioskId { get; set; }

        [JsonProperty("firstDenomNumber")]
        public string firstDenomNumber { get; set; }

        [JsonProperty("secondDenomNumber")]
        public string secondDenomNumber { get; set; }

        [JsonProperty("thirdDenomNumber")]
        public string thirdDenomNumber { get; set; }

        [JsonProperty("fourthDenomNumber")]
        public string fourthDenomNumber { get; set; }

        [JsonProperty("fifthDenomNumber")]
        public string fifthDenomNumber { get; set; }

        [JsonProperty("sixthDenomNumber")]
        public string sixthDenomNumber { get; set; }

        [JsonProperty("seventhDenomNumber")]
        public string seventhDenomNumber { get; set; }

        [JsonProperty("eighthDenomNumber")]
        public string eighthDenomNumber { get; set; }

        [JsonProperty("ninthDenomNumber")]
        public string ninthDenomNumber { get; set; }

        [JsonProperty("tenthDenomNumber")]
        public string tenthDenomNumber { get; set; }

        [JsonProperty("eleventhDenomNumber")]
        public string eleventhDenomNumber { get; set; }

        [JsonProperty("twelfthDenomNumber")]
        public string twelfthDenomNumber { get; set; }

        [JsonProperty("totalAmount")]
        public decimal totalAmount { get; set; }

        //[JsonProperty("approvedCodeapprovedCode")]
        //public string approvedCodeapprovedCode { get; set; }


        [JsonProperty("firstDenomNumberOld")]
        public string firstDenomNumberOld { get; set; }

        [JsonProperty("secondDenomNumberOld")]
        public string secondDenomNumberOld { get; set; }

        [JsonProperty("thirdDenomNumberOld")]
        public string thirdDenomNumberOld { get; set; }

        [JsonProperty("fourthDenomNumberOld")]
        public string fourthDenomNumberOld { get; set; }

        [JsonProperty("fifthDenomNumberOld")]
        public string fifthDenomNumberOld { get; set; }

        [JsonProperty("sixthDenomNumberOld")]
        public string sixthDenomNumberOld { get; set; }

        [JsonProperty("seventhDenomNumberOld")]
        public string seventhDenomNumberOld { get; set; }

        [JsonProperty("eighthDenomNumberOld")]
        public string eighthDenomNumberOld { get; set; }

        [JsonProperty("ninthDenomNumberOld")]
        public string ninthDenomNumberOld { get; set; }

        [JsonProperty("tenthDenomNumberOld")]
        public string tenthDenomNumberOld { get; set; }

        [JsonProperty("eleventhDenomNumberOld")]
        public string eleventhDenomNumberOld { get; set; }

        [JsonProperty("twelfthDenomNumberOld")]
        public string twelfthDenomNumberOld { get; set; }

        [JsonProperty("cassette1Status")]
        public int cassette1Status { get; set; }

        [JsonProperty("cassette2Status")]
        public int cassette2Status { get; set; }

        [JsonProperty("cassette3Status")]
        public int cassette3Status { get; set; }

        [JsonProperty("cassette4Status")]
        public int cassette4Status { get; set; }

        [JsonProperty("hopper1Status")]
        public int hopper1Status { get; set; }

        [JsonProperty("hopper2Status")]
        public int hopper2Status { get; set; }

        [JsonProperty("hopper3Status")]
        public int hopper3Status { get; set; }

        [JsonProperty("hopper4Status")]
        public int hopper4Status { get; set; }

        [JsonProperty("kioskIds")]
        public string kioskIds { get; set; }

        [JsonProperty("controlCashId")]
        public string controlCashId { get; set; }

    }


    public class DeleteControlCashNotification
    {
        public int id { get; set; }
        public int status { get; set; }


    }
}
