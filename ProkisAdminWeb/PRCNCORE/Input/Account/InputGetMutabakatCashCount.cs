﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputGetMutabakatCashCount
    {
        [JsonProperty("whichSide")]
        public int whichSide { get; set; }

        [JsonProperty("kioskEmptyId")]
        public int kioskEmptyId { get; set; }
    }
}
