﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Account
{
    public class InputListSaveCutOffMonitoring
    {
        [JsonProperty("adminUserId")]
        public int expertUserId { get; set; }

        [JsonProperty("startDate")]
        public DateTime startDate { get; set; }

        [JsonProperty("endDate")]
        public DateTime endDate { get; set; }

        [JsonProperty("cutDurationText")]
        public string cutDurationText { get; set; }

        [JsonProperty("kioskId")]
        public string kioskId { get; set; }

        [JsonProperty("instutionId")]
        public int instutionId { get; set; }

        [JsonProperty("cutReasonText")]
        public string cutReasonText { get; set; }

     
        [JsonProperty("insertDate")]
        public DateTime insertDate { get; set; }

        [JsonProperty("cutId")]
        public int cutId { get; set; }

    }
}
