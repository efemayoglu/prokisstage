﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Account
{
    public class InputListGunsonuMutabakat
    {
        [JsonProperty("mutabakatDate")]
        public DateTime mutabakatDate { get; set; }

        [JsonProperty("kioskId")]
        public Int64 kioskId { get; set; }

        [JsonProperty("instutionId")]
        public int instutionId { get; set; }

        [JsonProperty("adminUserId")]
        public Int64 adminUserId { get; set; }

        [JsonProperty("orderType")]
        public int OrderType { get; set; }

        [JsonProperty("processTypeId")]
        public int? processTypeId { get; set; } 

    }
}
