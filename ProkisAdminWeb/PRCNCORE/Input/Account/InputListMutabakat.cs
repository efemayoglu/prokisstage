﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputListMutabakat
    {
        [JsonProperty("searchText")]
        public string SearchText { get; set; }

        [JsonProperty("numberOfItemsPerPage")]
        public int NumberOfItemsPerPage { get; set; }

        [JsonProperty("currentPageNum")]
        public int CurrentPageNum { get; set; }

        [JsonProperty("recordCount")]
        public int RecordCount { get; set; }

        [JsonProperty("pageCount")]
        public int PageCount { get; set; }

        [JsonProperty("startDate")]
        public DateTime StartDate { get; set; }

        [JsonProperty("endDate")]
        public DateTime EndDate { get; set; }

        [JsonProperty("orderSelectionColumn")]
        public int OrderSelectionColumn { get; set; }

        [JsonProperty("descAsc")]
        public int DescAsc { get; set; }

        [JsonProperty("kioskId")]
        public string KioskIds { get; set; }

        [JsonProperty("enteredUser")]
        public string EnteredUser { get; set; }

        [JsonProperty("operatorUser")]
        public string OperatorUser { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }



        [JsonProperty("kioskEmptyStatus")]
        public string KioskEmptyStatus { get; set; }

        [JsonProperty("instutionId")]
        public int InstutionId { get; set; }

        [JsonProperty("orderType")]
        public int OrderType { get; set; }
    }
}
