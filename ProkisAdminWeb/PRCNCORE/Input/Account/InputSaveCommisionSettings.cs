﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Account
{
    public class InputSaveCommisionSettings
    {

        [JsonProperty("adminUserId")]
        public int adminUserId { get; set; }

        [JsonProperty("creditAski")]
        public string creditAski { get; set; }

        [JsonProperty("mobileAski")]
        public string mobileAski { get; set; }

        [JsonProperty("cashAski")]
        public string cashAski { get; set; }

        [JsonProperty("yellowAski")]
        public string yellowAski { get; set; }

        [JsonProperty("redAski")]
        public string redAski { get; set; }

        [JsonProperty("creditBaskent")]
        public string creditBaskent { get; set; }

        [JsonProperty("mobileBaskent")]
        public string mobileBaskent { get; set; }

        [JsonProperty("cashBaskent")]
        public string cashBaskent { get; set; }

        [JsonProperty("yellowBaskent")]
        public string yellowBaskent { get; set; }

        [JsonProperty("redBaskent")]
        public string redBaskent { get; set; }

        [JsonProperty("creditAksaray")]
        public string creditAksaray { get; set; }

        [JsonProperty("mobileAksaray")]
        public string mobileAksaray { get; set; }

        [JsonProperty("cashAksaray")]
        public string cashAksaray { get; set; }

        [JsonProperty("yellowAksaray")]
        public string yellowAksaray { get; set; }

        [JsonProperty("redAksaray")]
        public string redAksaray { get; set; }




        [JsonProperty("instutionId")]
        public int instutionId { get; set; }

     

        [JsonProperty("creditCommision")]
        public string creditCommision { get; set; }

        [JsonProperty("mobileCommision")]
        public string mobileCommision { get; set; }

        [JsonProperty("cashCommision")]
        public string cashCommision { get; set; }

        [JsonProperty("yellowAlarm")]
        public string yellowAlarm { get; set; }

        [JsonProperty("redAlarm")]
        public string redAlarm { get; set; }
    }
}
