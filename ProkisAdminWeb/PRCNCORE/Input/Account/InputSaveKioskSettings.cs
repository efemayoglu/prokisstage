﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Account
{
    public class InputSaveKioskSettings
    {
        [JsonProperty("adminUserId")]
        public int adminUserId { get; set; }

        [JsonProperty("billFieldNotificationLimit")]
        public string billFieldNotificationLimit { get; set; }

        [JsonProperty("isNotificationLimitReceiptActive")]
        public int isNotificationLimitReceiptActive { get; set; }

        [JsonProperty("cashBoxNotificationLimit")]
        public string cashBoxNotificationLimit { get; set; }

        [JsonProperty("isNotificationCashBoxLimitActive")]
        public int isNotificationCashBoxLimitActive { get; set; }

        [JsonProperty("isYellowAlertActive")]
        public int isYellowAlertActive { get; set; }

        [JsonProperty("isRedAlertActive")]
        public int isRedAlertActive { get; set; }

        [JsonProperty("moneyCodeGenerationLimit")]
        public string moneyCodeGenerationLimit { get; set; }

        [JsonProperty("moneyCodeGenerationCountLimit")]
        public string moneyCodeGenerationCountLimit { get; set; }

        [JsonProperty("oneyCodeGenerationCountLimitPeriod")]
        public string oneyCodeGenerationCountLimitPeriod { get; set; }

        [JsonProperty("bGazUpperLoadingLimit")]
        public string bGazUpperLoadingLimit { get; set; }
    }
}
