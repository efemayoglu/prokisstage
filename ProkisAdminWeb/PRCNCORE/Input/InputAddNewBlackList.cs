﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputAddNewBlackList
    {
        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("tckn")]
        public string tckn { get; set; }

        [JsonProperty("birthYear")]
        public string birthYear { get; set; }

        [JsonProperty("description")]
        public string description { get; set; }

        [JsonProperty("updatedUser")]
        public int updatedUser { get; set; }

    }
}
