﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputUpdateMerchantPassword
    {

        [JsonProperty("apiKey")]
        public string ApiKey { get; set; }

        [JsonProperty("apiPass")]
        public string ApiPass { get; set; }

        [JsonProperty("merchantId")]
        public int MerchantId { get; set; }

        [JsonProperty("merchantPassword")]
        public string MerchantPassword { get; set; }

        [JsonProperty("updatedDate")]
        public string UpdatedDate { get; set; }

        [JsonProperty("updatedUserId")]
        public int UpdatedUserId { get; set; }
    }
}
