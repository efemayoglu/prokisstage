﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Kiosk
{
    public class InputSaveKiosk
    {
        [JsonProperty("kioskName")]
        public string KioskName { get; set; }

        [JsonProperty("kioskAddress")]
        public string KioskAddress { get; set; }

        [JsonProperty("kioskIp")]
        public string KioskIp { get; set; }

        [JsonProperty("kioskStatus")]
        public int KioskStatus { get; set; }

        [JsonProperty("updatedUser")]
        public int UpdatedUser { get; set; }

        [JsonProperty("latitude")]
        public string Latitude { get; set; }

        [JsonProperty("longitude")]
        public string Longitude { get; set; }

        [JsonProperty("regionId")]
        public int RegionId { get; set; }

        [JsonProperty("usageFee")]
        public string usageFee { get; set; }

        [JsonProperty("kioskTypeId")]
        public int kioskTypeId { get; set; }

        [JsonProperty("kioskCity")]
        public int kioskCity { get; set; }

        [JsonProperty("insertionDate")]
        public string insertionDate { get; set; }

        [JsonProperty("acceptorTypeId")]
        public int acceptorTypeId { get; set; }

        [JsonProperty("explanation")]
        public string  explanation { get; set; }

        [JsonProperty("connectionType")]
        public int connectionType { get; set; }

        [JsonProperty("cardReaderType")]
        public int cardReaderType { get; set; }

        [JsonProperty("institutionId")]
        public int institutionId { get; set; }

        [JsonProperty("riskClass")]
        public int riskClass { get; set; }

        [JsonProperty("isCredit")]
        public int isCredit { get; set; }

        [JsonProperty("isMobile")]
        public int isMobile { get; set; }


        [JsonProperty("bankNameId")]
        public int BankNameId { get; set; }

        [JsonProperty("isInvoiceActive")]
        public bool IsInvoiceActive { get; set; }

        [JsonProperty("isPrepaidActive")]
        public bool IsPrepaidActive { get; set; }

        [JsonProperty("isExtendedScreenActive")]
        public bool IsExtendedScreenActive { get; set; }


        [JsonProperty("districtId")]
        public int districtId { get; set; }

        [JsonProperty("neighborhoodId")]
        public int neighborhoodId { get; set; }

       

    }
}
