﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Kiosk
{
    public class InputListSaveErrorLogs
    {
        [JsonProperty("adminUserId")]
        public int expertUserId { get; set; }

        [JsonProperty("explanation")]
        public string explanation { get; set; }

        [JsonProperty("errorId")]
        public int errorId { get; set; }
    }
}
