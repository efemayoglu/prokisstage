﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Kiosk
{
    public class InputListKioskConditionDetail
    {
        [JsonProperty("apiKey")]
        public string ApiKey { get; set; }

        [JsonProperty("apiPass")]
        public string ApiPass { get; set; }

        [JsonProperty("kioskId")]
        public int KioskId { get; set; }

        [JsonProperty("numberOfItemsPerPage")]
        public int NumberOfItemsPerPage { get; set; }

        [JsonProperty("currentPageNum")]
        public int CurrentPageNum { get; set; }

        [JsonProperty("recordCount")]
        public int RecordCount { get; set; }

        [JsonProperty("pageCount")]
        public int PageCount { get; set; }
    }
}
