﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Kiosk
{
    public class InputSaveKioskCommand
    {
        [JsonProperty("kioskId")]
        public int kioskId { get; set; }

        [JsonProperty("instutionId")]
        public int institutionId { get; set; }

        [JsonProperty("createdUserId")]
        public int createdUserId { get; set; }

        [JsonProperty("commandTypeId")]
        public int commandTypeId { get; set; }

        [JsonProperty("commandString")]
        public string commandString { get; set; }

        [JsonProperty("commandReasonId")]
        public int commandReasonId { get; set; }

        [JsonProperty("explanation")]
        public string explanation { get; set; }

        [JsonProperty("commandId")]
        public int commandId { get; set; }
    }
}
