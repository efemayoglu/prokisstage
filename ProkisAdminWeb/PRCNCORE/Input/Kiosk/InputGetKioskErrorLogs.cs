﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Kiosk
{
    public class InputGetKioskErrorLogs
    {
        [JsonProperty("errorId")]
        public int errorId { get; set; }
    }
}
