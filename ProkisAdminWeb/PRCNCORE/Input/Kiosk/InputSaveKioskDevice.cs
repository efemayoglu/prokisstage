﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Kiosk
{
    public class InputSaveKioskDevice
    {
        [JsonProperty("kioskId")]
        public int kioskId { get; set; }

        [JsonProperty("deviceId")]
        public int deviceId { get; set; }

        [JsonProperty("portNumber")]
        public string portNumber { get; set; }

        [JsonProperty("adminUserId")]
        public int adminUserId { get; set; }

        [JsonProperty("itemId")]
        public Int64 itemId { get; set; }

    }
}
