﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{

    public class SaveSystemSettingParser
    {



            public int Id { get; set; }

        public int InstutionId { get; set; }

        public int NewStatus { get; set; }
        public int ApprovedUserId { get; set; }

        public string ScreenMessage { get; set; }
    }
    public class SystemSetting
    {
        public SystemSetting()
        {

        }
        public SystemSetting(
              int Id
            , string CreatedUserName
            , int CreatedUserId
            , string ApprovedUserName
            , int ApprovedUserId
            , int ApprovedStatus
            , int OldValue
            //,  int NewValue
            , string InstutionName
            , DateTime InsertionDate
            , DateTime ApprovedDate
            , string OldValueName
            , string ApprovedStatusName
            )
        {


            this.Id = Id;
            this.CreatedUserName = CreatedUserName;
            this.CreatedUserId = CreatedUserId;
            this.ApprovedUserName = ApprovedUserName;
            this.ApprovedUserId = ApprovedUserId;
            this.ApprovedStatus = ApprovedStatus;
            this.OldValue = OldValue;
            this.InstutionName = InstutionName;
            this.InsertionDate = InsertionDate;
            this.ApprovedDate = ApprovedDate;

            this.ApprovedStatusName = ApprovedStatusName;
            this.OldValueName = OldValueName;

        }

        public SystemSetting(
              int Id
            ,  string CreatedUserName
            ,  int CreatedUserId
            ,  string ApprovedUserName
            ,  int ApprovedUserId
            ,  int ApprovedStatus
            ,  int OldValue
            //,  int NewValue
            ,  string InstutionName
            ,  DateTime InsertionDate
            ,  DateTime ApprovedDate
            )
        {


            this.Id = Id;
            this.CreatedUserName       = CreatedUserName  ;
            this.CreatedUserId         = CreatedUserId    ;
            this.ApprovedUserName      = ApprovedUserName ;
            this.ApprovedUserId        = ApprovedUserId   ;
            this.ApprovedStatus        = ApprovedStatus   ;
            this.OldValue              = OldValue         ;
            //this.NewValue              = NewValue         ;
            this.InstutionName         = InstutionName    ;
            this.InsertionDate         = InsertionDate    ;
            this.ApprovedDate          = ApprovedDate     ;

        }
        public int      Id                       { get; set; }
        public string   CreatedUserName       { get; set; }
        public int      CreatedUserId            { get; set; }
        public string   ApprovedUserName      { get; set; }
        public int      ApprovedUserId           { get; set; }
        public int      ApprovedStatus           { get; set; }
        public int      OldValue                 { get; set; }
        public int      NewValue                 { get; set; }
        public string   InstutionName         { get; set; }
        public DateTime InsertionDate       { get; set; }
        public DateTime ApprovedDate        { get; set; }
        public DateTime StartDate           { get; set; }
        public DateTime EndDate             { get; set; }


        public string OldValueName { get; set; }
        public string ApprovedStatusName { get; set; }
    }                 

    public class EmployeeScheduling
    {
        public EmployeeScheduling(int id, string title, DateTime startDate, DateTime endDate, bool allDay, string backgroundColor, string borderColor)
        {
            this.Id = id;
            this.Title           = title          ;
            this.StartDate       = startDate      ;
            this.EndDate         = endDate        ;
            this.AllDay          = allDay         ;
            this.BackgroundColor = backgroundColor;
            this.BorderColor =     borderColor;
                               
        }
        public EmployeeScheduling()
        {

        }

        public int      Id              { get; set; }
        public string   Title           { get; set; }
        public DateTime StartDate       { get; set; }
        public DateTime EndDate         { get; set; }
        public bool     AllDay          { get; set; }
        public string   BackgroundColor { get; set; }
        public string   BorderColor     { get; set; }
        public bool isDeleted { get; set; }

    }




    public class InputListKiosksReportsNew

    {
        public string ApiKey { get; set; }
        public string ApiPass { get; set; }
        public string SearchText { get; set; }
        public int NumberOfItemsPerPage { get; set; }
        public int CurrentPageNum { get; set; }
        public int RecordCount { get; set; }
        public int PageCount { get; set; }
        public int OrderSelectionColumn { get; set; }
        public int DescAsc { get; set; }
        public int OrderType { get; set; }
        public int InstutionId { get; set; }
        public string KioskId { get; set; }
        public int BankNameId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string PaymentType { get; set; }
        public string ProcessType { get; set; }
    }

    public class OutputListKiosksReportsNew

    {



        public string ResultCount { get; set; }

        public string KioskName { get; set; }

        public string CashAmount { get; set; }

        public string SuccessTxnCount { get; set; }

        public string InstitutionAmount { get; set; }

        public string UsageFee { get; set; }

        public string ParaUstu { get; set; }

        public string CreditCardAmount { get; set; }

        public string CreditCardCommisionAmount { get; set; }

        public string MobilePaymentAmount { get; set; }

        public string MobilePaymentCommisionAmount { get; set; }

        public string RecCount { get; set; }

        public string RecTotal { get; set; }

        public string ReverseRecCount { get; set; }

        public string ReverseRecTotal { get; set; }

        public string NonReverseRecCount { get; set; }

        public string NonReverseRecTotal { get; set; }



    }

}
