﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputListCashCounts
    {
        [JsonProperty("itemId")]
        public int ItemId { get; set; }

        [JsonProperty("operatinType")]
        public int OperatinType { get; set; }

        [JsonProperty("numberOfItemsPerPage")]
        public int NumberOfItemsPerPage { get; set; }

        [JsonProperty("currentPageNum")]
        public int CurrentPageNum { get; set; }

        [JsonProperty("orderSelectionColumn")]
        public int OrderSelectionColumn { get; set; }

        [JsonProperty("descAsc")]
        public int DescAsc { get; set; }

        [JsonProperty("startDate")]
        public DateTime StartDate { get; set; }

        [JsonProperty("endDate")]
        public DateTime EndDate { get; set; }
    }
}
