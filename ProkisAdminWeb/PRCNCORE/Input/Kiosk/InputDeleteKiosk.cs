﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputDeleteKiosk
    {
        [JsonProperty("apiKey")]
        public string ApiKey { get; set; }

        [JsonProperty("apiPass")]
        public string ApiPass { get; set; }

        [JsonProperty("kioskId")]
        public int KioskId { get; set; }
    }
}
