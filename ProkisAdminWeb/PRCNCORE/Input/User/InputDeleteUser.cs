﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputDeleteUser
    {
        [JsonProperty("userId")]
        public int userId { get; set; }
    }
}
