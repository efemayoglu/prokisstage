﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputRoleAction
    {
       [JsonProperty("roleActionId")]
        public int Id { get; set; }

        [JsonProperty("roleName")]
        public string RoleName{ get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("roleLabel")]
        public string RoleLabel { get; set; }

    }
}
