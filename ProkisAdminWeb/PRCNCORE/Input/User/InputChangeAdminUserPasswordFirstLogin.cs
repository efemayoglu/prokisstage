﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputChangeAdminUserPasswordFirstLogin
    {
        [JsonProperty("admUserId")]
        public int adminUserID { get; set; }

        [JsonProperty("newPassword")]
        public string newPassword { get; set; }

        [JsonProperty("oldPasswords")]
        public string oldPasswords { get; set; }

        [JsonProperty("updatedUser")]
        public int updatedUser { get; set; }
    }
}
