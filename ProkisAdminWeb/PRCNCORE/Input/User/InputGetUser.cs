﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputGetUser
    {
        [JsonProperty("userName")]
        public string userName { get; set; }

        [JsonProperty("password")]
        public string password { get; set; }

        [JsonProperty("minute")]
        public int minute { get; set; }

        [JsonProperty("tryLimit")]
        public int tryLimit { get; set; }

        [JsonProperty("expireDayCount")]
        public int expireDayCount { get; set; }
    }
}
