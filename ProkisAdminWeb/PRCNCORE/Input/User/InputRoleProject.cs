﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputRoleProject
    {
        public int roleId { get; set; }
    }
    public class InputUserRoleConnection
    {
        public int userId { get; set; }
    }
    public class InputAddProject
    {
        public string name { get; set; }
    }

    public class InputUpdateRoleProject
    {
        public int roleId { get; set; }
        public int projectId { get; set; }

    }
    public class InputUpdateUserRoleConnection
    {
        public int userId { get; set; }
        public string roleId{ get; set; }

    }

}
