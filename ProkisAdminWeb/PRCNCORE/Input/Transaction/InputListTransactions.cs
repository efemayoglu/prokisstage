﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputListTransactions
    {

        [JsonProperty("searchText")]
        public string SearchText { get; set; }

        [JsonProperty("numberOfItemsPerPage")]
        public int NumberOfItemsPerPage { get; set; }

        [JsonProperty("currentPageNum")]
        public int CurrentPageNum { get; set; }

        [JsonProperty("recordCount")]
        public int RecordCount { get; set; }

        [JsonProperty("pageCount")]
        public int PageCount { get; set; }

        [JsonProperty("startDate")]
        public DateTime StartDate { get; set; }

        [JsonProperty("endDate")]
        public DateTime EndDate { get; set; }

        [JsonProperty("orderSelectionColumn")]
        public int OrderSelectionColumn { get; set; }

        [JsonProperty("descAsc")]
        public int DescAsc { get; set; }

        [JsonProperty("customerId")]
        public int CustomerId { get; set; }

        [JsonProperty("kioskId")]
        public string KioskId { get; set; }

        [JsonProperty("platform")]
        public string platform { get; set; }

        [JsonProperty("whichAction")]
        public int whichAction { get; set; }

        [JsonProperty("itemId")]
        public Int64 itemId { get; set; }

        [JsonProperty("isUsed")]
        public int isUsed { get; set; }

        [JsonProperty("isActive")]
        public int isActive { get; set; }

        [JsonProperty("instutionId")]
        public int instutionId { get; set; }

        [JsonProperty("operationTypeId")]
        public int operationTypeId { get; set; }

        [JsonProperty("txnStatusId")]
        public string txnStatusId { get; set; }

        [JsonProperty("processType")]
        public int processType { get; set; }

        [JsonProperty("orderType")]
        public int OrderType { get; set; }

        [JsonProperty("cardRepairStatus")]
        public int CardRepairStatus { get; set; }

        [JsonProperty("repairId")]
        public int RepairId { get; set; }

        [JsonProperty("isCardDeleted")]
         public int IsCardDeleted { get; set; }

        [JsonProperty("Rnd")]
        public string Rnd { get; set; }  

        [JsonProperty("MuhasebeStatusId")]
        public int MuhasebeStatusId { get; set; }

        [JsonProperty("MuhasebeStatus")]
        public string MuhasebeStatus { get; set; }

        [JsonProperty("AboneNo")]
        public string AboneNo { get; set; }

    }
    public class InputListCustomerContact
    {

        [JsonProperty("searchText")]
        public string SearchText { get; set; }

        [JsonProperty("numberOfItemsPerPage")]
        public int NumberOfItemsPerPage { get; set; }

        [JsonProperty("currentPageNum")]
        public int CurrentPageNum { get; set; }

        [JsonProperty("recordCount")]
        public int RecordCount { get; set; }

        [JsonProperty("pageCount")]
        public int PageCount { get; set; }

        [JsonProperty("startDate")]
        public DateTime StartDate { get; set; }

        [JsonProperty("endDate")]
        public DateTime EndDate { get; set; }

        [JsonProperty("orderSelectionColumn")]
        public int OrderSelectionColumn { get; set; }

        [JsonProperty("descAsc")]
        public int DescAsc { get; set; }

        [JsonProperty("transactionId")]
        public int TransactionId { get; set; }

        [JsonProperty("instutionId")]
        public int instutionId { get; set; }

        [JsonProperty("orderType")]
        public int OrderType { get; set; }

    }

    public class InputListTransactionsDetails
    {
        [JsonProperty("transactionId")]
        public long TransactionId { get; set; }
    }

    public class InputListAutoMutabakat
    {
        [JsonProperty("searchText")]
        public string SearchText { get; set; }

        [JsonProperty("numberOfItemsPerPage")]
        public int NumberOfItemsPerPage { get; set; }

        [JsonProperty("currentPageNum")]
        public int CurrentPageNum { get; set; }

        [JsonProperty("recordCount")]
        public int RecordCount { get; set; }

        [JsonProperty("pageCount")]
        public int PageCount { get; set; }

        [JsonProperty("startDate")]
        public DateTime StartDate { get; set; }

        [JsonProperty("endDate")]
        public DateTime EndDate { get; set; }

        [JsonProperty("orderSelectionColumn")]
        public int OrderSelectionColumn { get; set; }

        [JsonProperty("descAsc")]
        public int DescAsc { get; set; }

        [JsonProperty("instutionId")]
        public int InstutionId { get; set; }

        [JsonProperty("accessToken")]
        public string AccessToken { get; set; }

        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("state")]
        public int State { get; set; }

        [JsonProperty("adminUserId")]
        public Int64 AdminUserId { get; set; }

        [JsonProperty("kioskId")]
        public int KioskId { get; set; }

        [JsonProperty("orderType")]
        public int OrderType { get; set; }

    }

    public class InputPhoneNumber
    {
        [JsonProperty("transactionId")]
        public string transactionId { get; set; }

        [JsonProperty("operationType")]
        public string operationType { get; set; }
    }
    public class InputCancelTnx
    {
        [JsonProperty("transactionId")]
        public string transactionId { get; set; }

    }
}
