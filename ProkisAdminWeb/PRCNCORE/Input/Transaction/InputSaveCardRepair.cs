﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Transaction
{
    public class InputSaveCardRepair
    {
        [JsonProperty("transactionId")]
        public int transactionId { get; set; }

        [JsonProperty("adminUserId")]
        public int adminUserId { get; set; }
 
        [JsonProperty("anaKredi")]
        public string anaKredi { get; set; }

        [JsonProperty("yedekKredi")]
        public string yedekKredi { get; set; }
    }
}
