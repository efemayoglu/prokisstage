﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputListTransactionConditions
    {
        [JsonProperty("searchText")]
        public string SearchText { get; set; }

        [JsonProperty("numberOfItemsPerPage")]
        public int NumberOfItemsPerPage { get; set; }

        [JsonProperty("currentPageNum")]
        public int CurrentPageNum { get; set; }

        [JsonProperty("recordCount")]
        public int RecordCount { get; set; }

        [JsonProperty("pageCount")]
        public int PageCount { get; set; }

        [JsonProperty("startDate")]
        public DateTime StartDate { get; set; }

        [JsonProperty("endDate")]
        public DateTime EndDate { get; set; }

        [JsonProperty("orderSelectionColumn")]
        public int OrderSelectionColumn { get; set; }

        [JsonProperty("descAsc")]
        public int DescAsc { get; set; }

        [JsonProperty("kioskId")]
        public string KioskId { get; set; }

        [JsonProperty("instutionId")]
        public int instutionId { get; set; }

        [JsonProperty("paymentTypeId")]
        public int paymentTypeId { get; set; }

        [JsonProperty("cardTypeId")]
        public int cardTypeId { get; set; }

        [JsonProperty("completeStatusId")]
        public int completeStatusId { get; set; }

        [JsonProperty("orderType")]
        public int OrderType { get; set; }

        [JsonProperty("paymentType")]
        public int PaymentType { get; set; }

        
    }
}

