﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Transaction
{
    public class InputSaveKioskLocalLog
    {
        [JsonProperty("kioskId")]
        public string KioskId { get; set; }

        [JsonProperty("transactionId")]
        public int TransactionId { get; set; }

        [JsonProperty("startDateField")]
        public DateTime StartDate { get; set; }

        [JsonProperty("endDateField")]
        public DateTime EndDate { get; set; }

        [JsonProperty("adminUserId")]
        public Int64 AdminUserId { get; set; }
 
     

 
    }
}
