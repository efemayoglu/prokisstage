﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Transaction
{
    public class InputListUpdateRepairedCard
    {
        [JsonProperty("adminUserId")]
        public int expertUserId { get; set; }

        [JsonProperty("anaKredi")]
        public string anaKredi { get; set; }

        [JsonProperty("yedekKredi")]
        public string yedekKredi { get; set; }

        [JsonProperty("repairId")]
        public int repairId { get; set; }
    }
}
