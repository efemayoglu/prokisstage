﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputGetTransaction
    {
        [JsonProperty("transactionId")]
        public int transactionId { get; set; }
    }

    public class InputCancelTransactionService
    {
        [JsonProperty("transactionId")]
        public int transactionId { get; set; }

        [JsonProperty("userId")]
        public int userId { get; set; }

        [JsonProperty("adminUserId")]
        public int adminUserId { get; set; }

        [JsonProperty("customerNo")]
        public int customerNo { get; set; }

        [JsonProperty("billDate")]
        public string billDate { get; set; }

        [JsonProperty("billNumber")]
        public int billNumber { get; set; }
  
        [JsonProperty("serviceResult")]
        public string serviceResult { get; set; }
    }


    public class InputChangeStatusTransactionService
    {
        [JsonProperty("transactionId")]
        public int transactionId { get; set; }

        [JsonProperty("userId")]
        public int userId { get; set; }

        [JsonProperty("adminUserId")]
        public int adminUserId { get; set; }

        [JsonProperty("explanation")]
        public string explanation { get; set; }

        [JsonProperty("newStatus")]
        public string newStatus { get; set; }

    }
}
