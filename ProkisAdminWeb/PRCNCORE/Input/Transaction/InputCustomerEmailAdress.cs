﻿using Newtonsoft.Json;

namespace PRCNCORE.Input
{
    public class InputCustomerEmailAdress
    {
        internal InputCustomerEmailAdress(long customerId, string emailAdress)
        {
            this.CustomerId = customerId;
            this.EmailAddress = emailAdress;
        }
        internal InputCustomerEmailAdress(string emailAdress)
        {
            this.EmailAddress = emailAdress;
        }
        internal InputCustomerEmailAdress(long customerId)
        {
            this.CustomerId = customerId;
        }


        [JsonProperty("customerId")]
        public long CustomerId { get; set; }
        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }
    }


    public class InputCustomerEmailAdress2
    {
        


        [JsonProperty("customerId")]
        public long CustomerId { get; set; }
        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }
    }

    public class InputCustomerEmailAdress3
    {



        [JsonProperty("transactionId")]
        public long TransactionId { get; set; }
        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }
    }

    public class InputCustomerEmailAdressStatus
    {

        [JsonProperty("aboneNo")]
        public string AboneNo { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
    }

}
