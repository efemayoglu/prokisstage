﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputGetRegions
    {
        [JsonProperty("cityId")]
        public int cityId { get; set; }
    }

    public class InputGetDistrict
    {
        [JsonProperty("districtId")]
        public int districtId { get; set; }
    }
}
