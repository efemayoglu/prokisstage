﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Source
{
    public class Sources
    {
        public Sources(int id, string explanation,
            DateTime createdDate, int createdUserId, string createdUserName,
             string link, int sourceCategoryId, string sourceCategoryExplanation, int sourceCategoryTypeId, string sourceTypeExplanation)
        {
            this.Id = id;
            this.SourceCategoryExplanation = sourceCategoryExplanation;
            this.SourceCategoryTypeId = sourceCategoryTypeId;
            this.Explanation = explanation;
            this.CreatedDate = createdDate;
            this.CreatedUserId = createdUserId;
            this.CreatedUserName = createdUserName;
            this.Link = link;
            this.SourceCategoryId = sourceCategoryId;
            this.SourceTypeExplanation = sourceTypeExplanation;
        }

        public Sources(string explanation, 
            DateTime createdDate, int createdUserId, string createdUserName,
             string link, int sourceCategoryId, string sourceCategoryExplanation, int sourceCategoryTypeId, string sourceTypeExplanation)
        {
            this.SourceCategoryExplanation = sourceCategoryExplanation;
            this.SourceCategoryTypeId = sourceCategoryTypeId;
            this.Explanation = explanation;
            this.CreatedDate = createdDate;
            this.CreatedUserId = createdUserId;
            this.CreatedUserName = createdUserName;
            this.Link = link;
            this.SourceCategoryId = sourceCategoryId;
            this.SourceTypeExplanation = sourceTypeExplanation;
        }
        public Sources()
        {

        }
        public int Id { get; set; }
        public string Explanation { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedUserId { get; set; }
        public string CreatedUserName { get; set; }
        public string Link { get; set; }
        public int SourceCategoryId { get; set; }
        public int SourceCategoryTypeId { get; set; }
        public string SourceCategoryExplanation { get; set; }
        public int SourceTypeId { get; set; }
        public string SourceTypeExplanation { get; set; }
    }
}
