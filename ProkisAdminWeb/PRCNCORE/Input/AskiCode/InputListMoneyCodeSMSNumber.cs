﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.AskiCode
{
    public class InputListMoneyCodeSMSNumber
    {
       
        [JsonProperty("moneyCodeId")]
        public Int64 moneyCodeId { get; set; }

        [JsonProperty("adminUserId")]
        public int adminUserId { get; set; }
    }
}
