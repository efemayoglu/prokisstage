﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.AskiCode
{
    public class InputSaveSendMoneyCode
    {
        [JsonProperty("transactionId")]
        public Int64 transactionId { get; set; }

        [JsonProperty("moneyCodeId")]
        public Int64 moneyCodeId { get; set; }

        [JsonProperty("cellPhone")]
        public string cellPhone { get; set; }

        [JsonProperty("adminUserId")]
        public int adminUserId { get; set; }
    }
}
