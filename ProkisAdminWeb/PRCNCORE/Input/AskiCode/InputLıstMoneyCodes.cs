﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputLıstMoneyCodes
    {
        [JsonProperty("searchText")]
        public string SearchText { get; set; }

        [JsonProperty("numberOfItemsPerPage")]
        public int NumberOfItemsPerPage { get; set; }

        [JsonProperty("currentPageNum")]
        public int CurrentPageNum { get; set; }

        [JsonProperty("recordCount")]
        public int RecordCount { get; set; }

        [JsonProperty("pageCount")]
        public int PageCount { get; set; }

        [JsonProperty("startDate")]
        public DateTime StartDate { get; set; }

        [JsonProperty("endDate")]
        public DateTime EndDate { get; set; }

        [JsonProperty("orderSelectionColumn")]
        public int OrderSelectionColumn { get; set; }

        [JsonProperty("descAsc")]
        public int DescAsc { get; set; }

        [JsonProperty("codeStatus")]
        public int codeStatus { get; set; }

        [JsonProperty("generatedType")]
        public int generatedType { get; set; }

        [JsonProperty("instutionId")]
        public int instutionId { get; set; }

        [JsonProperty("generatedKioskIds")]
        public string GeneratedKioskIds { get; set; }

        [JsonProperty("usedKioskIds")]
        public string UsedKioskIds { get; set; }

        [JsonProperty("paymentType")]
        public int PaymentType { get; set; }

        [JsonProperty("orderType")]
        public int OrderType { get; set; }
    }
}
