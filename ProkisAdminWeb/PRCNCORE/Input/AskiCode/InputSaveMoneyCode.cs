﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputSaveMoneyCode
    {
        [JsonProperty("transactionId")]
        public int transactionId { get; set; }

        [JsonProperty("userId")]
        public int userId { get; set; }

        [JsonProperty("codeAmount")]
        public string codeAmount { get; set; }

        [JsonProperty("cellPhone")]
        public string cellPhone { get; set; }
    }
}
