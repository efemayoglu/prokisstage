﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class SwitchInput
    {
        [JsonProperty("apikey")]
        public string ApiKey { get; set; }

        [JsonProperty("apipass")]
        public string ApiPass { get; set; }

        [JsonProperty("functionCode")]
        public int FunctionCode { get; set; }

        [JsonProperty("adminUserId")]
        public Int64 adminUserId { get; set; }

        [JsonProperty("pageUrl")]
        public string  pageUrl { get; set; }
    }
}
