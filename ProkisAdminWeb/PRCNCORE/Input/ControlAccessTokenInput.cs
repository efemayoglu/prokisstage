﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class ControlAccessTokenInput
    {
        [JsonProperty("apiKey")]
        public string ApiKey { get; set; }

        [JsonProperty("apiPass")]
        public string ApiPass { get; set; }

        [JsonProperty("adminUserId")]
        public Int64 adminUserId { get; set; }

        [JsonProperty("Retokenize")]
        public string Retokenize { get; set; }
    }
}
