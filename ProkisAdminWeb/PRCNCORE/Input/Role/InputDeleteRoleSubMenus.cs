﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Role
{
    public class InputDeleteRoleSubMenus
    {
        [JsonProperty("id")]
        public int id { get; set; }
    }
}
