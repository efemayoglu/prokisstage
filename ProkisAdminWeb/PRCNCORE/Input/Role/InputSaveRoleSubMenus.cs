﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Role
{
    public class InputSaveRoleSubMenus
    {
        [JsonProperty("RoleSubMenus")]
        public List<InputSaveRoleSubMenu> RoleSubMenus { get; set; }

        [JsonProperty("createdserId")]
        public int CreatedserId { get; set; }
    }

    public class InputSaveRoleSubMenusContext
    {
        [JsonProperty("Contexts")]
        public List<InputSaveRoleSubMenuContext> Contexts { get; set; }

        [JsonProperty("RoleId")]
        public int RoleId { get; set; }
    }

    public class InputApproveRoleContext
    {
        public int RoleId { get; set; }

        public int OperationType { get; set; }

        public long ApprovedUserId { get; set; }

    }
}
