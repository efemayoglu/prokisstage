﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Role
{
    public class InputSaveRoleSubMenu
    {
        [JsonProperty("roleId")]
        public int roleId { get; set; }

        [JsonProperty("canEdit")]
        public int canEdit { get; set; }

        [JsonProperty("canAdd")]
        public int canAdd { get; set; }

        [JsonProperty("canDelete")]
        public int canDelete { get; set; }

        [JsonProperty("subMenuId")]
        public int subMenuId { get; set; }
    }

    public class InputSaveRoleSubMenuContext
    {
        [JsonProperty("RoleActionId")]
        public int RoleActionId { get; set; }

        [JsonProperty("SubMenuId")]
        public int SubMenuId { get; set; }

        [JsonProperty("Status")]
        public int Status { get; set; }
    }
}
