﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class UpdateSubMenu
    {
        [JsonProperty("apikey")]
        public string ApiKey { get; set; }

        [JsonProperty("apipass")]
        public string ApiPass { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
