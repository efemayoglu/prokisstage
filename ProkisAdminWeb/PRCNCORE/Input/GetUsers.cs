﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class GetUsers
    {
        [JsonProperty("apikey")]
        public string ApiKey { get; set; }

        [JsonProperty("apipass")]
        public string ApiPass { get; set; }
    }
}
