﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class UpdateUser
    {
        [JsonProperty("apikey")]
        public string ApiKey { get; set; }

        [JsonProperty("apipass")]
        public string ApiPass { get; set; }

        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("telephone")]
        public string Telephone { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("cellPhone")]
        public string CellPhone { get; set; }

        [JsonProperty("hospitalId")]
        public int HospitalId { get; set; }

        [JsonProperty("supplierId")]
        public int SupplierId { get; set; }

        [JsonProperty("roleId")]
        public int RoleId { get; set; }

        [JsonProperty("userType")]
        public int UserType { get; set; }
    }
}
