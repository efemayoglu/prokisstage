﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputGetSecretKey
    {
        [JsonProperty("userId")]
        public int userId { get; set; }

        [JsonProperty("customerId")]
        public int customerId { get; set; }
    }
}
