﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputListCustomers
    {
        [JsonProperty("searchText")]
        public string SearchText { get; set; }

        [JsonProperty("numberOfItemsPerPage")]
        public int NumberOfItemsPerPage { get; set; }

        [JsonProperty("currentPageNum")]
        public int CurrentPageNum { get; set; }

        [JsonProperty("recordCount")]
        public int RecordCount { get; set; }

        [JsonProperty("pageCount")]
        public int PageCount { get; set; }

        [JsonProperty("orderSelectionColumn")]
        public int OrderSelectionColumn { get; set; }

        [JsonProperty("descAsc")]
        public int DescAsc { get; set; }

        [JsonProperty("instutionId")]
        public int InstutionId { get; set; }
    
    }
}
