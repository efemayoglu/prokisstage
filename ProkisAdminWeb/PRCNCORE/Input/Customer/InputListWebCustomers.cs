﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputListWebCustomers
    {
        [JsonProperty("searchNameText")]
        public string searchNameText { get; set; }

        [JsonProperty("searchCustNoText")]
        public string searchCustNoText { get; set; }

        [JsonProperty("searchTcknText")]
        public string searchTcknText { get; set; }

        [JsonProperty("numberOfItemsPerPage")]
        public int NumberOfItemsPerPage { get; set; }

        [JsonProperty("currentPageNum")]
        public int CurrentPageNum { get; set; }

        [JsonProperty("recordCount")]
        public int RecordCount { get; set; }

        [JsonProperty("pageCount")]
        public int PageCount { get; set; }

        [JsonProperty("tableType")]
        public int TableType { get; set; }


    }
}

