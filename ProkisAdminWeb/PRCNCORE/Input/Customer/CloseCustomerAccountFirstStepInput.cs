﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input.Customer
{
    public class CloseCustomerAccountFirstStepInput
    {
        [JsonProperty("userId")]
        public Int64 userId { get; set; }

        [JsonProperty("itemId")]
        public Int64 itemId { get; set; }
    }
}
