﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputDeleteOperation
    {
        [JsonProperty("userId")]
        public int userId { get; set; }

        [JsonProperty("storedProcedure")]
        public string storedProcedure { get; set; }

        [JsonProperty("itemId")]
        public int itemId { get; set; }

        [JsonProperty("type")]
        public int type { get; set; }

        [JsonProperty("vitelTicketNo")]
        public string vitelTicketNo { get; set; }
    }
}
