﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Input
{
    public class InputShowLocalLogDetail
    {
        [JsonProperty("localLogId")]
        public string localLogId { get; set; }
    }

    public class InputSaveControlCashNotification
    {
        [JsonProperty("adminUserId")]
        public string adminUserId { get; set; }

        [JsonProperty("searchText")]
        public string SearchText { get; set; }

        [JsonProperty("numberOfItemsPerPage")]
        public int NumberOfItemsPerPage { get; set; }

        [JsonProperty("currentPageNum")]
        public int CurrentPageNum { get; set; }

        [JsonProperty("recordCount")]
        public int RecordCount { get; set; }

        [JsonProperty("pageCount")]
        public int PageCount { get; set; }

        [JsonProperty("startDate")]
        public DateTime StartDate { get; set; }

        [JsonProperty("endDate")]
        public DateTime EndDate { get; set; }
      

        [JsonProperty("orderSelectionColumn")]
        public int OrderSelectionColumn { get; set; }

        [JsonProperty("descAsc")]
        public int DescAsc { get; set; }

        [JsonProperty("kioskId")]
        public string KioskIds { get; set; }

        [JsonProperty("instutionId")]
        public int InstutionId { get; set; }

        [JsonProperty("orderType")]
        public int OrderType { get; set; }

        [JsonProperty("controlCashId")]
        public int ControlCashId { get; set; }

        [JsonProperty("referenceStatus")]
        public int ReferenceStatus { get; set; }

        [JsonProperty("logStatus")]
        public int LogStatus { get; set; }

        [JsonProperty("StatusId")]
        public int StatusId { get; set; }


        [JsonProperty("kioskfullfillstatus")]
        public int Kioskfullfillstatus { get; set; }

        //[JsonProperty("kioskfullfillstatus")]
        //public int Kioskfullfillstatus { get; set; }
    }
}
