﻿using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace PRCNCORE.Utilities
{
    public class HsmOperation
    {
        public class FMCMD
        {
            public static string SET_TOKEN_PIN = "0010";
            public static string GET_TRANSFER_KEY = "0013";
            public static string GET_LMK_KCV = "0014";
            public static string IMPORT_LMK = "0020";
            public static string IMPORT_ZMK = "0023";
            public static string IMPORT_KEY = "0025";
            public static string EXPORT_KEY = "0027";
            public static string CRYPT_WITH_KEY_NAME = "0040";
            public static string CRYPT_ACE_V2000_ADVANCED_81_SCP02 = "0100";
        }
        public class FMATTR
        {
            public static string CKA_ENCRYPT = "00000104";
            public static string CKA_DECRYPT = "00000105";
        }
        public class FMMECH
        {
            public static string CKM_DES3_ECB = "00000132";
            public static string CKM_DES3_CBC = "00000133";
        }

        public static string HSM_SUCCESS = "FF";
        public static string CKU_USER = "01";
        public static string CKU_SO = "00";
        public static string PADDING_8000 = "8000000000000000";
        public static string ZERO_BUFFER_8_BYTES = "0000000000000000";
        public static string FFFF_BUFFER_8_BYTES = "FFFFFFFFFFFFFFFF";
        public static int MAX_TCP_MSG_LEN = 5 * 1204;

        public static string SLOT_ID = "0000";

        public string sError = "";

        private int portNum = 2071;
        private string hostName = "127.0.0.1";
        private int receiveTimeout = 300000;

        private string[] ListenMessageList = new string[1000000];

        public void Config(int iHsmSlot, string sHsmIP, int iHsmPort)
        {
            SLOT_ID = iHsmSlot.ToString().PadLeft(4, '0');
            hostName = sHsmIP;
            portNum = iHsmPort;
        }

        public string TDES(string inputData, string attribute, string mechanism, string sKeyName)
        {
            string SendData = "";
            string RecvData = "";
            string outData = "";

            try
            {
                Exception ex6 = new Exception(DateTime.Now.ToString() + "tdes started. : input : " + inputData + ", att: " + attribute + ", keyname: " + sKeyName);
                Utility.LogError(ex6);

                if (inputData.Length % 16 != 0)
                {
                    inputData += FFFF_BUFFER_8_BYTES.Substring(0, (16 - inputData.Length % 16));
                    //throw new Exception("W300016.1 Hsm TDES check input data length failed");
                }

                SendData += FMCMD.CRYPT_WITH_KEY_NAME;
                SendData += SLOT_ID;
                SendData += (attribute.Length / 2).ToString("X2").PadLeft(4, '0');
                SendData += attribute;//ENC-DEC
                SendData += (mechanism.Length / 2).ToString("X2").PadLeft(4, '0');
                SendData += mechanism;//ECB-CBC
                SendData += (inputData.Length / 2).ToString("X2").PadLeft(4, '0');
                SendData += inputData;//DATA
                SendData += (sKeyName.Length / 2).ToString("X2").PadLeft(4, '0');
                SendData += sKeyName;//LMK(KEY)

                Exception ex7 = new Exception(DateTime.Now.ToString() + "send date prepared. : " + SendData);
                Utility.LogError(ex7);

                this.SendRecv(SendData, ref RecvData);
                if (RecvData.Substring(0, 2) != HSM_SUCCESS)
                {
                    throw new Exception("W300016 Hsm TDES process failed");
                }


                outData = RecvData.Substring(2, inputData.Length);//crypted data
                Exception ex12 = new Exception(DateTime.Now.ToString() + "outpıt data" + outData);
                Utility.LogError(ex12);
            }
            catch (Exception ex)
            {
                throw new Exception("W300016 Hsm TDES process failed");
            }
            return outData;
        }

        public void SendRecv(string SendData, ref string RecvData)
        {
            Exception ex8 = new Exception(DateTime.Now.ToString() + "send receive started. : " + SendData);
            Utility.LogError(ex8);

            string SendMessage = "";
            string RecvMessage = "";
            int iRecvMsgLen = MAX_TCP_MSG_LEN;
            int index = 0;
            int iRecvDataLen = 0;
            try
            {
                SendMessage = "1071";
                SendMessage += (SendData.Length / 2).ToString("X2").PadLeft(4, '0');
                SendMessage += SendData;
                SendMessage += "2071";
                SendMessage += Utility.CalculateLRC(SendMessage);

                Exception ex9 = new Exception(DateTime.Now.ToString() + "SendMessage prepared. :1071 and data len addded " + SendData);
                Utility.LogError(ex9);

                if (SendReceiveMessage(SendMessage, ref RecvMessage, iRecvMsgLen) == false)
                {
                    throw new Exception("HSM Send/Receive Message Error!");
                }

                Exception ex10 = new Exception(DateTime.Now.ToString() + "Receive Message" + RecvMessage);
                Utility.LogError(ex10);


                // first 2 byte: STX
                if (RecvMessage.Substring(0, 4) != "1071")
                {
                    throw new Exception("W300016 Hsm Message Operation Not Success - STX Error !!");
                }
                index += 4;

                iRecvDataLen = Convert.ToInt32(RecvMessage.Substring(index, 4), 16) * 2;
                index += 4;

                RecvData = RecvMessage.Substring(index, iRecvDataLen);
                index += iRecvDataLen;

                // first 2 byte: ETX
                if (RecvMessage.Substring(index, 4) != "2071")
                {
                    throw new Exception("W300016 Hsm Message Operation Not Success - ETX Error !!");
                }
                index += 4;
                // first 2 byte: LRC
                if (RecvMessage.Substring(index, 2) != Utility.CalculateLRC(RecvMessage.Substring(0, index)))
                {
                    throw new Exception("W300016 Hsm Message Operation Not Success - LRC Error !!");
                }

                Exception ex11 = new Exception(DateTime.Now.ToString() + "Receive Message parsed" + RecvMessage);
                Utility.LogError(ex11);

            }
            catch (Exception ex)
            {
                throw new Exception("W300098 Exception.Message: " + ex.Message);
            }
        }

        public bool SendReceiveMessage(string sendingData, ref string receivedData, int receivedLen)
        {
            int discarted = 0;
            byte[] sendBuff = Utility.GetBytes(sendingData, out discarted);
            byte[] OutBuff = new byte[receivedLen];

            try
            {
                Exception ex9 = new Exception(DateTime.Now.ToString() + " inside SendReceiveMessage:" + sendingData);
                Utility.LogError(ex9);

                using (TcpClient tcpClient = new TcpClient(hostName, portNum))
                {
                    Exception ex10 = new Exception(DateTime.Now.ToString() + " hostname :" + hostName + " ,hostname :" + hostName + " ,portnum :" + portNum);
                    Utility.LogError(ex10);

                    tcpClient.ReceiveTimeout = receiveTimeout;
                    using (NetworkStream socket = tcpClient.GetStream())
                    {
                        socket.Write(sendBuff, 0, sendBuff.Length);
                        Int32 bytes = socket.Read(OutBuff, 0, receivedLen);
                        receivedData = Utility.ToString(OutBuff);
                        receivedData = receivedData.Substring(0, bytes * 2);
                        socket.Close();
                    }
                    tcpClient.Close();

                    Exception ex11 = new Exception(DateTime.Now.ToString() + " tcp received :" + receivedData);
                    Utility.LogError(ex11);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
