using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.InteropServices;
using System.Net.Sockets;
using System.Configuration;
using System.Xml;
using System.Net;
using System.Net.Mail;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Web.Security;
using SMSReplyParser;
using Newtonsoft.Json;
using System.Net.Security;
using Newtonsoft.Json.Linq;

namespace PRCNCORE.Utilities
{
    public class Utility
    {
        public const int LENGTHDATETIME = 14;
        public const int LENGTHDATE = 8;
        public const int LENGTHTIME = 6;

        public const byte INTEGER_FORMAT = 1;
        public const byte HEX_FORMAT = 2;

        private static XmlDocument appConfigXML;
        private static string encryptionKey1 = "x(76#ccv~<df1ig&)>nvb1VnM%%sO4$;";
        private static XmlNodeList streamingServers;

        private byte[] IV = new byte[] { 0x37, 120, 0x54, 0x8e, 0xac, 0x33, 9, 0xc9, 0xc7, 0x52, 0xec, 0x1b, 0x5e, 5, 0x8a, 0xe5 };
        private byte[] key = new byte[0];

        public const int LOG_ACTIVE = 0;

        public static class ExpDll
        {
            public const int MODE_RSA_SIGN = 1;
            public const int MODE_RSA_DESIGN = 2;
            public const int MODE_RSA_ENCRYPT = 11;
            public const int MODE_RSA_DECRYPT = 12;

            public const string DLL_C_PLUS_PLUS = @"CPlusPlus.dll";

            [DllImport(ExpDll.DLL_C_PLUS_PLUS, EntryPoint = "WriteBinarySpecial")]
            public static extern int WriteBinarySpecial(StringBuilder cInput, Int32 iInputLength, StringBuilder cFilePath, StringBuilder _Mode);

            [DllImport(ExpDll.DLL_C_PLUS_PLUS, EntryPoint = "WriteBinarySpecialDesc")]
            public static extern int CPlusPlusWriteBinarySpecialDesc(StringBuilder cData2Write, int iDataLen, StringBuilder cFileName, StringBuilder _Mode, StringBuilder cDesc);

            [DllImport(ExpDll.DLL_C_PLUS_PLUS, EntryPoint = "ReadBinarySpecial")]
            public static extern int CPlusPlusReadBinarySpecial(StringBuilder cDataFromRead, ref int iDataLen, int index, StringBuilder cFileName);

            [DllImport(ExpDll.DLL_C_PLUS_PLUS, EntryPoint = "DES123")]
            public static extern int CPlusPlusDES123(
                                       StringBuilder cInput, StringBuilder cKey, StringBuilder cIV,
                                       Int32 iDes123, Int32 iCryptMode, Int32 iCipherMode,
                                       StringBuilder cOutput, ref Int32 iOutputLen, ref Int32 iRet);

            [DllImport(ExpDll.DLL_C_PLUS_PLUS, EntryPoint = "RsaKeyGen")]
            public static extern int CPlusPlusRsaKeyGen(int iSizeBits, StringBuilder cModule, StringBuilder cPubExpo, StringBuilder cPriExpo, StringBuilder cP, StringBuilder cQ, StringBuilder cDP, StringBuilder cDQ, StringBuilder cQP, ref int iRetCode, StringBuilder cErrDesc);

            [DllImport(ExpDll.DLL_C_PLUS_PLUS, EntryPoint = "RSADesignCertificate")]
            public static extern int CPlusPlusRSADesignCertificate(StringBuilder cModule, StringBuilder cExpo, StringBuilder cCertificate, StringBuilder dcrypData);

            [DllImport(ExpDll.DLL_C_PLUS_PLUS, EntryPoint = "RsaCryptoService")]
            public static extern int CPlusPlusRsaCryptoService(int iMode, StringBuilder cModule, StringBuilder cPubExpo, StringBuilder cPriExpo, StringBuilder cP, StringBuilder cQ, StringBuilder cInput, StringBuilder cOutput, ref int iRetCode, StringBuilder cErrDesc);

        }

        public static string ExecuteHttpRequest(string url,
                                              string method,
                                              string contentType,
                                              byte[] postData,
                                              string ipAddress,
                                              ref HttpStatusCode statusCode)
        {
            string httpResponse = "";
            Encoding encoding = Encoding.UTF8;
            HttpWebRequest httpWebRequest = null;
            try
            {
                httpWebRequest = (HttpWebRequest)WebRequest.Create(url);

                ServicePointManager.ServerCertificateValidationCallback = new
                                   RemoteCertificateValidationCallback
                                   (
                                   delegate { return true; }
                                   );
                httpWebRequest.Method = method;

                if (method == "POST")
                {
                    httpWebRequest.Timeout = 30 * 1000;

                    if (!string.IsNullOrEmpty(contentType))
                        httpWebRequest.ContentType = contentType;

                    httpWebRequest.ContentLength = postData.LongLength;
                    Stream requestStream = httpWebRequest.GetRequestStream();
                    requestStream.Write(postData, 0, postData.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }

                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                BufferedStream responseStream = new BufferedStream(httpWebResponse.GetResponseStream());
                StreamReader rd = new StreamReader(responseStream, encoding);
                httpResponse = rd.ReadToEnd();
                rd.Close();
                rd.Dispose();
                statusCode = httpWebResponse.StatusCode;
            }
            catch (WebException exp)
            {
                httpResponse = exp.Message;
                try
                {
                    statusCode = ((HttpWebResponse)exp.Response).StatusCode;
                }
                catch
                {
                    statusCode = HttpStatusCode.RequestTimeout;
                }
            }
            catch (Exception exp)
            {
                httpResponse = exp.Message;
                statusCode = HttpStatusCode.BadRequest;
            }
            finally
            {

            }
            return httpResponse;
        }

        public string DeleteLine(string InBuff)
        {
            try
            {
                InBuff = InBuff.Replace("\r", "");
                InBuff = InBuff.Replace("\n", "");
            }
            catch (Exception exp)
            {
            }

            return InBuff;
        }


        public static string ExecuteHttpRequest(string url,
                                              string method,
                                              string contentType,
                                              string soapAction,
                                              byte[] postData,
                                              string ipAddress,
                                              ref HttpStatusCode statusCode)
        {
            string httpResponse = "";
            Encoding encoding = Encoding.UTF8;
            HttpWebRequest httpWebRequest = null;
            try
            {
                httpWebRequest = (HttpWebRequest)WebRequest.Create(url);

                ServicePointManager.ServerCertificateValidationCallback = new
                                   RemoteCertificateValidationCallback
                                   (
                                   delegate { return true; }
                                   );
                httpWebRequest.Method = method;

                if (method == "POST")
                {

                    httpWebRequest.Timeout = Convert.ToInt32(GetConfigValue("AskiWebServiceTimeout")) * 1000;

                    if (!string.IsNullOrEmpty(contentType))
                        httpWebRequest.ContentType = contentType;

                    if (!string.IsNullOrEmpty(soapAction))
                        httpWebRequest.Headers.Add("SOAPAction", soapAction);

                    httpWebRequest.ContentLength = postData.LongLength;
                    Stream requestStream = httpWebRequest.GetRequestStream();
                    requestStream.Write(postData, 0, postData.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }

                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                BufferedStream responseStream = new BufferedStream(httpWebResponse.GetResponseStream());
                StreamReader rd = new StreamReader(responseStream, encoding);
                httpResponse = rd.ReadToEnd();
                rd.Close();
                rd.Dispose();
                statusCode = httpWebResponse.StatusCode;
            }
            catch (WebException exp)
            {
                //Log.Error("WSE12 - Exception Occured", exp);
                httpResponse = exp.Message;
                try
                {
                    statusCode = ((HttpWebResponse)exp.Response).StatusCode;
                }
                catch
                {
                    statusCode = HttpStatusCode.RequestTimeout;
                    //Log.Error("WSE14 - Exception Occured", exp);
                }
            }
            catch (Exception exp)
            {
                //Log.Error("WSE16 - Exception Occured", exp);
                httpResponse = exp.Message;
                statusCode = HttpStatusCode.BadRequest;
            }
            finally
            {

            }
            return httpResponse;
        }

        public static bool SendSMS(string webServiceUrl, string message, string telephoneNo)
        {
            bool ret = false;
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            try
            {
                string responseText = Utility.ExecuteHttpRequest
                                  (webServiceUrl,
                                  "POST",
                                  "text/xml",
                                  System.Text.Encoding.UTF8.GetBytes(Utility.getSMSBodyData(message, telephoneNo)),
                                  "",
                                  ref statusCode);

                if (statusCode == System.Net.HttpStatusCode.OK)
                {
                    SMSReplyParserData data = JsonConvert.DeserializeObject<SMSReplyParserData>(responseText);

                    //if (data.Response.Status.Code == 200 && data.Response.Status.Description == "OK")
                    if (responseText.Contains("ID:")) ret = true;
                }
                else
                {
                    ret = false;
                }
            }
            catch (Exception exp)
            {
            }
            finally
            {

            }

            return ret;
        }

        private static string getSMSBodyData(string message, string telNo)
        {
            string resp = "<SingleTextSMS><UserName>BirlesikOdeme-2500</UserName><PassWord>FonSMS_bod_2016</PassWord><Action>0</Action><Mesgbody>" + message + "</Mesgbody><Numbers>" + telNo + "</Numbers><Originator>BIRLESIKODM</Originator><SDate></SDate></SingleTextSMS>";
            return resp;
            
            //return "{\"Credential\":{\"Username\":\"birlesikodeme\",\"Password\":\"qws741\"},\"Header\":{\"From\":\"PRATIKNOKTA\",\"ScheduledDeliveryTime\":\"0001-01-01T00:00:00.0000000\",\"ValidityPeriod\":0,\"Route\":0},\"Message\":\"" + message + "\",\"To\":[\"" + telNo + "\"],\"DataCoding\":\"Default\"}";
        }

        public static void SendEmail(string senderEmailAddress,
        string senderName,
        string[] emailAddresses,
        string subject,
        string message,
        string[] attachmentFiles)
        {
            System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
            mailMessage.From = mailMessage.Sender = new System.Net.Mail.MailAddress(senderEmailAddress, senderName);
            for (int i = 0; i < emailAddresses.Length; i++)
            {
                mailMessage.To.Add(new MailAddress(emailAddresses[i]));
            }

            mailMessage.IsBodyHtml = true;
            mailMessage.Subject = subject;
            mailMessage.Body = message;
            mailMessage.SubjectEncoding = mailMessage.BodyEncoding = System.Text.Encoding.GetEncoding("iso-8859-9");

            if (attachmentFiles != null && attachmentFiles.Length > 0)
            {
                for (int i = 0; i < attachmentFiles.Length; i++)
                {
                    if (System.IO.File.Exists(attachmentFiles[i]))
                        mailMessage.Attachments.Add(new System.Net.Mail.Attachment(attachmentFiles[i]));
                }
            }

            try
            {

                System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient();
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.EnableSsl = true;
                smtpClient.Port = 587;

                string smtpUsername = "info@birlesikodeme.com";
                string smtpPassword = "faksonay1903";
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(smtpUsername, smtpPassword);

                smtpClient.Send(mailMessage);
            }
            catch (Exception exp)
            {
                int a = 1;
            }
            finally
            {

            }
        }

        public static JObject ConvertXML2Json(string xmlData)
        {
            JObject x = null;

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlData);

                string jsonText = JsonConvert.SerializeXmlNode(doc);

                x = JObject.Parse(jsonText);
            }
            catch (Exception exp)
            {
                x = null;
                //Log.Error("OPR99 - Exception Occured", exp);
            }

            return x;

        }


        /// <summary>
        /// string dizisindeki aranılan elemanın indexini döner.
        /// </summary>
        /// <param name="piSourceArray">Kaynak dizi</param>
        /// <param name="SearchText">Aranılan eleman</param>
        /// <returns></returns>
        public static int FindStringArrayIndex(string[] piSourceArray,
            string SearchText)
        {
            if (SearchText.Length == 0)
                return -1;

            for (int i = 0; i < piSourceArray.Length; i++)
            {
                if (piSourceArray[i] == SearchText)
                    return i;

            }
            return -1;
        }

        public static DateTime FillDate(string piDate)
        {
            DateTime dt;
            string DateFormat = "yyyyMMdd";

            if (piDate.Length == LENGTHTIME) DateFormat = "yyMMdd";

            DateTime.TryParseExact(piDate, DateFormat, null, System.Globalization.DateTimeStyles.None, out dt);
            return dt;
        }

        public static DateTime FillTime(string piTime)
        {
            DateTime dt;
            DateTime.TryParseExact(piTime, "hhmmss", null, System.Globalization.DateTimeStyles.None, out dt);
            return dt;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="piDateTime"></param>
        /// <param name="piDateTimeFlag">'D' = Date, 'T' = Time</param>
        public static DateTime FillDateTime(string piDateTime, char piDateTimeFlag)
        {
            DateTime dt = new DateTime(1900, 1, 1);

            if (piDateTime.Length == LENGTHDATETIME)
            {
                if (piDateTimeFlag == 'D')
                    dt = FillDate(piDateTime.Substring(0, LENGTHDATE));
                else if (piDateTimeFlag == 'T')
                    dt = FillTime(piDateTime.Substring(LENGTHDATE));
            }
            else if (piDateTime.Length >= LENGTHDATE)
            {
                if (piDateTimeFlag == 'D')
                    dt = FillDate(piDateTime.Substring(0, LENGTHDATE));
            }
            else if (piDateTime.Length == LENGTHTIME)
            {
                if (piDateTimeFlag == 'T')
                    dt = FillTime(piDateTime);
                else if (piDateTimeFlag == 'D') // yyMMdd formatından dolayı
                    dt = FillDate(piDateTime.Substring(0, LENGTHTIME));
            }
            return dt;

        }

        public static string calculateSHA512(string data)
        {
            try
            {
                System.Security.Cryptography.SHA512 sha = new System.Security.Cryptography.SHA512CryptoServiceProvider();
                byte[] bytes = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(data);
                byte[] hashingbytes = sha.ComputeHash(bytes);

                string hash = Convert.ToBase64String(hashingbytes);

                return hash;
            }
            catch
            {
                return "4587";
            }
        }

        public static string CalculateSHA512Pin(string data)
        {
            try
            {
                System.Security.Cryptography.SHA512 sha = new System.Security.Cryptography.SHA512CryptoServiceProvider();
                byte[] bytes = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(data);
                byte[] hashingbytes = sha.ComputeHash(bytes);

                string hash = ToString(hashingbytes);

                return hash;
            }
            catch
            {
                return "4587";
            }
        }

        public static string generateRandomNum(int low, int high)
        {
            try
            {
                Random rndGen = new Random();
                int num = rndGen.Next(low, high);
                return num.ToString();
            }
            catch
            {
                return "2";
            }
        }
        /// <summary>
        /// Converts 1 or 2 character string into equivalant byte value
        /// </summary>
        /// <param name="hex">1 or 2 character string</param>
        /// <returns>byte</returns>
        public static byte HexToByte(string hex)
        {
            if (hex.Length > 2 || hex.Length <= 0)
                throw new ArgumentException("hex must be 1 or 2 characters in length");
            byte newByte = byte.Parse(hex, System.Globalization.NumberStyles.HexNumber);
            return newByte;
        }

        /// <summary>
        /// Creates a byte array from the hexadecimal string. Each two characters are combined
        /// to create one byte. First two hexadecimal characters become first byte in returned array.
        /// Non-hexadecimal characters are ignored. 
        /// </summary>
        /// <param name="hexString">string to convert to byte array</param>
        /// <param name="discarded">number of characters in string ignored</param>
        /// <returns>byte array, in the same left-to-right order as the hexString</returns>
        public static byte[] GetBytes(string hexString, out int discarded)
        {
            discarded = 0;
            string newString = "";
            char c;
            // remove all none A-F, 0-9, characters
            for (int i = 0; i < hexString.Length; i++)
            {
                c = hexString[i];
                if (IsHexDigit(c))
                    newString += c;
                else
                    discarded++;
            }
            // if odd number of characters, discard last character
            if (newString.Length % 2 != 0)
            {
                discarded++;
                newString = newString.Substring(0, newString.Length - 1);
            }

            int byteLength = newString.Length / 2;
            byte[] bytes = new byte[byteLength];
            string hex;
            int j = 0;
            for (int i = 0; i < bytes.Length; i++)
            {
                hex = new String(new Char[] { newString[j], newString[j + 1] });
                bytes[i] = HexToByte(hex);
                j = j + 2;
            }
            return bytes;
        }
        public static byte[] GetBytes(string hexString)
        {
            string newString = "";
            char c;
            // remove all none A-F, 0-9, characters
            for (int i = 0; i < hexString.Length; i++)
            {
                c = hexString[i];
                if (IsHexDigit(c))
                    newString += c;
            }
            // if odd number of characters, discard last character
            if (newString.Length % 2 != 0)
            {
                newString = newString.Substring(0, newString.Length - 1);
            }

            int byteLength = newString.Length / 2;
            byte[] bytes = new byte[byteLength];
            string hex;
            int j = 0;
            for (int i = 0; i < bytes.Length; i++)
            {
                hex = new String(new Char[] { newString[j], newString[j + 1] });
                bytes[i] = HexToByte(hex);
                j = j + 2;
            }
            return bytes;
        }
        public static string FormatDataLength(string StrLength)
        {
            string formattedStr = "";
            int dataLength = 0;
            dataLength = StrLength.Length;
            if (dataLength % 2 != 0)
            {
                throw new Exception("Error Data Length Cannot Be Odd Value");
            }
            else
                dataLength = StrLength.Length / 2;

            switch (dataLength.ToString().Length)
            {
                case 1:
                    formattedStr = "000" + dataLength.ToString();
                    break;
                case 2:
                    formattedStr = "00" + dataLength.ToString();
                    break;
                case 3:
                    formattedStr = "0" + dataLength.ToString();
                    break;
                case 4:
                    formattedStr = dataLength.ToString();
                    break;
                default:
                    formattedStr = dataLength.ToString();
                    break;
            }

            return formattedStr;

        }
        public static string TrimRightStr(string str, string trimwith)
        {
            int index = 0;

            while (true)
            {
                index = str.LastIndexOf(trimwith);
                if (index <= 0)
                    break;

                str = str.Substring(0, index);
            }

            return str;
        }
        public static string TrimLeftStr(string str, string trimwith)
        {
            int index = 0;

            while (true)
            {
                index = str.IndexOf(trimwith);
                if (index <= 0)
                    break;

                str = str.Substring(str.Length - (index + trimwith.Length));
            }

            return str;
        }
        public static string PadRightStr(string str, string padwith, int totalsize)
        {
            if (str.Length >= totalsize)
                return str;

            int timespad = (totalsize - str.Length) / padwith.Length;

            for (int i = 0; i < timespad; i++)
            {
                str += padwith;
            }

            return str;
        }
        public static string PadLeftStr(string str, string padwith, int totalsize)
        {
            string newpad = "";
            if (str.Length > totalsize)
                return str;

            int timespad = (totalsize - str.Length) / padwith.Length;

            for (int i = 0; i < timespad; i++)
            {
                newpad += padwith;
            }

            return newpad + str;
        }
        public static string ToString(byte[] bytes)
        {
            string hexString = "";
            for (int i = 0; i < bytes.Length; i++)
            {
                hexString += bytes[i].ToString("X2");
            }
            return hexString;
        }

        /// <summary>
        /// Returns true is c is a hexadecimal digit (A-F, a-f, 0-9)
        /// </summary>
        /// <param name="c">Character to test</param>
        /// <returns>true if hex digit, false if not</returns>
        public static bool IsHexDigit(Char c)
        {
            int numChar;
            int numA = Convert.ToInt32('A');
            int num1 = Convert.ToInt32('0');
            c = Char.ToUpper(c);
            numChar = Convert.ToInt32(c);
            if (numChar >= numA && numChar < (numA + 6))
                return true;
            if (numChar >= num1 && numChar < (num1 + 10))
                return true;
            return false;
        }
        public static bool IsDecimalDigit(Char c)
        {
            int numChar;
            int numA = Convert.ToInt32('A');
            int num1 = Convert.ToInt32('0');
            c = Char.ToUpper(c);
            numChar = Convert.ToInt32(c);
            if (numChar >= numA && numChar < (numA + 6))
                return false;
            if (numChar >= num1 && numChar < (num1 + 10))
                return true;
            return false;
        }
        public static byte[] StructureToByteArray(object obj)
        {
            int len = Marshal.SizeOf(obj);

            byte[] arr = new byte[len];

            IntPtr ptr = Marshal.AllocHGlobal(len);

            Marshal.StructureToPtr(obj, ptr, true);

            Marshal.Copy(ptr, arr, 0, len);

            Marshal.FreeHGlobal(ptr);

            return arr;
        }
        public static byte[] HexToBytes(string hexString)
        {
            //check for null
            if (hexString == null) return null;
            //get length
            int len = hexString.Length;
            if (len % 2 == 1) return null;
            int len_half = len / 2;
            //create a byte array
            byte[] bs = new byte[len_half];
            try
            {
                //convert the hexstring to bytes
                for (int i = 0; i != len_half; i++)
                {
                    bs[i] = (byte)Int32.Parse(hexString.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(”Exception : ” + ex.Message);
            }
            //return the byte array
            return bs;
        }

        /// <summary>
        /// 16'lık sayı sisteminde ki bir değeri 2'lik sayı sistemine dönüştürüyor.
        /// </summary>
        /// <param name="pBcdString">16'lık değeri</param>
        /// <returns></returns>
        /// 
        public static void WriteError(string FilePath, string ErrSource, string ErrDesc, string stackTrace)
        {
            string ErrLogFile = "";
            StreamWriter sw;
            try
            {

                //Directory checking
                if (!Directory.Exists(FilePath))
                    Directory.CreateDirectory(FilePath);
                //File operations
                ErrLogFile = FilePath + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                if (!File.Exists(ErrLogFile))
                    sw = File.CreateText(ErrLogFile);
                else
                {
                    if ((File.GetAttributes(ErrLogFile) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    {
                        File.SetAttributes(ErrLogFile, System.IO.FileAttributes.Normal);
                    }
                    sw = File.AppendText(ErrLogFile);
                }
                //Writing error to file
                sw.WriteLine("Date Time    : " + DateTime.Now);
                sw.WriteLine("Source       : " + ErrSource);
                sw.WriteLine("Description  : " + ErrDesc);
                sw.WriteLine("Stack Trace  : " + stackTrace);
                sw.WriteLine("----------------------------------------");
                sw.Close();
            }
            catch (Exception ex)
            {

            }
        }
        public static string Bcd2Bit(string pBcdString)
        {
            string TagBin = "", Bcd2Bit = "";
            int i, j, TagValInt, TgLen;

            try
            {
                TgLen = pBcdString.Trim().Length;
                for (i = 0; i < TgLen; i++)
                {
                    TagValInt = (int)Convert.ToUInt32(pBcdString.Substring(i, 1), 16);
                    if (TagValInt == 0)
                    {
                        Bcd2Bit = Bcd2Bit + "0000";
                    }
                    else
                    {
                        while (TagValInt != 0)
                        {
                            TagBin = (TagValInt % 2) + TagBin;
                            TagValInt = (int)(TagValInt / 2);
                        }
                        j = TagBin.Length;
                        while (j < 4)
                        {
                            TagBin = "0" + TagBin;
                            j = j + 1;
                        }
                        Bcd2Bit = Bcd2Bit + TagBin;
                        TagBin = "";
                    }
                }
                return Bcd2Bit;
            }
            catch (System.Exception ExcpNo)
            {
                string rc = ExcpNo.Message.ToString();
                return "";
            }
        }

        public static string Bit2Bcd(string pBitString)
        {
            string Bit2Bcd = "";
            int i, StrVal;

            try
            {

                for (i = 0; i < pBitString.Length; i++)
                {
                    StrVal = Convert.ToInt32(pBitString.Substring(i, 1)) * 8;
                    i = i + 1;
                    StrVal = StrVal + Convert.ToInt32(pBitString.Substring(i, 1)) * 4;
                    i = i + 1;
                    StrVal = StrVal + Convert.ToInt32(pBitString.Substring(i, 1)) * 2;
                    i = i + 1;
                    StrVal = StrVal + Convert.ToInt32(pBitString.Substring(i, 1)) * 1;
                    //==============================================================
                    switch (StrVal)
                    {
                        case 10:
                            Bit2Bcd = Bit2Bcd + "A";
                            break;
                        case 11:
                            Bit2Bcd = Bit2Bcd + "B";
                            break;
                        case 12:
                            Bit2Bcd = Bit2Bcd + "C";
                            break;
                        case 13:
                            Bit2Bcd = Bit2Bcd + "D";
                            break;
                        case 14:
                            Bit2Bcd = Bit2Bcd + "E";
                            break;
                        case 15:
                            Bit2Bcd = Bit2Bcd + "F";
                            break;
                        default:
                            Bit2Bcd = Bit2Bcd + StrVal;
                            break;
                    }
                    //==============================================================
                }
                return Bit2Bcd;
            }
            catch (System.Exception ExcpNo)
            {
                string rc = ExcpNo.Message.ToString();
                return "";
            }
        }

        public static bool ControlFormat(string Value, int ValueLen, byte FormatType, ref string ReturnValue)
        {
            try
            {
                int iAsc = 0, i = 0;
                int[] WrongCharsInValue = new int[Value.Length];

                //Value'nun bütün karakterleri birer birer kontrol ediliyor.
                //for (i = 0; i < ValueLen; i++)
                foreach (char c in Value)
                {
                    //Sırası gelen değişkene atanıyor.
                    //iAsc = Strings.Asc(Value.Substring(i, 1));
                    iAsc = c;
                    // Ilk başta karakterin uygun oldugunu kabul ediyoruz.
                    WrongCharsInValue[i] = 1;
                    //INTEGER FORMAT
                    if (FormatType == INTEGER_FORMAT)
                    {
                        if (!((iAsc >= 0x30) && (iAsc <= 0x39)))
                        {
                            WrongCharsInValue[i] = 0;
                            continue;
                        }
                    }
                    //HEXADECIMAL FORMAT
                    else if (FormatType == HEX_FORMAT)
                    {
                        if (!((iAsc >= 0x30) && (iAsc <= 0x39)) && !((iAsc >= 0x41) && (iAsc <= 0x46)) && !((iAsc >= 0x61) && (iAsc <= 0x66)))
                        {
                            WrongCharsInValue[i] = 0;
                            continue;
                        }
                        // Küçük harf olanlar (a,b,c,d,e,f) büyük harf'lere dönüştürülüyor.
                        if (((iAsc >= 0x61) && (iAsc <= 0x66)))
                            //Value = Value.Substring(0, i) +
                            //        Convert.ToString(Strings.Chr(iAsc - 0x20)) +
                            //        Value.Substring(i + 1);
                            Value = Value.Substring(0, i) +
                                    Convert.ToString((char)(iAsc - 0x20)) +
                                    Value.Substring(i + 1);
                    }
                    i++;
                }
                // Istenilen formatta olmayan karakterler Value değerinden çıkartılıp,
                // uygun olanlar geri gönderiliyor.
                for (i = 0; i < ValueLen; i++)
                {
                    if (WrongCharsInValue[i] == 1)
                        ReturnValue = ReturnValue + Value.Substring(i, 1);
                }
                //ReturnValue = Value.Substring(0,i);	
                if (ReturnValue.ToString().Trim() == "")
                    return false;
                else
                    return true;
            }
            catch (System.Exception ExcpNo)
            {
                string rc = ExcpNo.Message.ToString();
                return false;
            }
        }

        public static bool ControlFormat(int iAsc, byte FormatType, ref byte rAsc)
        {
            try
            {

                if (FormatType == INTEGER_FORMAT)
                {
                    if (!((iAsc >= 0x30) && (iAsc <= 0x39)))
                        return false;
                    else
                        rAsc = Convert.ToByte(iAsc);
                }
                else if (FormatType == HEX_FORMAT)
                {
                    if (!((iAsc >= 0x30) && (iAsc <= 0x39)) && !((iAsc >= 0x41) && (iAsc <= 0x46)) && !((iAsc >= 0x61) && (iAsc <= 0x66)))
                        return false;
                    else
                        rAsc = Convert.ToByte(iAsc);

                    if (((iAsc >= 0x61) && (iAsc <= 0x66)))
                        rAsc = Convert.ToByte(iAsc - 0x20);
                }
                return true;
            }
            catch (System.Exception ExcpNo)
            {
                string rc = ExcpNo.Message.ToString();
                return false;
            }
        }

        /// <summary>
        /// dosya yolundan klasor veya dosya ismini doner.
        /// </summary>
        /// <param name="piFilePath">File path</param>
        /// <param name="Type">'F' : FileName, 'D': Directory Name </param>
        /// <param name="piExtension">File ise extension olup olmayacağı</param>
        /// <returns></returns>
        public static string FindFolder(string piFilePath, char Type, bool piExtension)
        {
            if (piFilePath == null)
                return "";
            for (int i = piFilePath.Length - 1; i >= 0; i--)
            {
                if (piFilePath.Substring(i, 1) == "\\")
                {
                    if (Type == 'F')
                    {
                        string fn = piFilePath.Substring(i + 1);
                        int index = fn.IndexOf('.');
                        if (index != -1) // uzantısı varsa
                        {
                            if (piExtension == false)
                                fn = fn.Substring(0, index);
                        }
                        return fn;
                    }
                    else
                    {
                        return piFilePath.Substring(0, i - 1);
                    }
                }
            }
            return "";
        }

        /// <summary>
        /// Hex string datasını dosyaya binary olarak yazar.
        /// </summary>
        /// <param name="piFilePath">Dosya</param>
        /// <param name="piHexString">hex data</param>
        public static void CreateFile(string piFilePath, string piHexString)
        {
            if (File.Exists(piFilePath))
                File.Delete(piFilePath);

            FileStream fs = File.Create(piFilePath);
            int discarded = 0;

            byte[] bData = GetBytes(piHexString, out discarded);
            fs.Write(bData, 0, bData.Length);
            fs.Close();
        }
        public static string Format857(char Inchar)
        {
            string hex = "";
            char bi = 'İ';
            char bu = 'Ü';
            char bs = 'Ş';
            char bg = 'Ğ';
            char bo = 'Ö';
            char bc = 'Ç';
            char su = 'ü';
            char si = 'i';
            char ss = 'ş';
            char sg = 'ğ';
            char so = 'ö';
            char sc = 'ç';
            char sI = 'ı';

            if (Inchar == bu) hex = "9A";
            else if (Inchar == bi) hex = "98";
            else if (Inchar == bs) hex = "9E";
            else if (Inchar == bg) hex = "A6";
            else if (Inchar == bo) hex = "99";
            else if (Inchar == bc) hex = "80";
            else if (Inchar == su) hex = "81";
            else if (Inchar == si) hex = "69";
            else if (Inchar == ss) hex = "9F";
            else if (Inchar == sg) hex = "A7";
            else if (Inchar == so) hex = "94";
            else if (Inchar == sc) hex = "87";
            else if (Inchar == sI) hex = "8D";
            else
                hex = "";

            return hex;
        }

        #region ascii to hex
        public static string AsciiToHex_NEW(string asciiString)
        {
            string hex = "";
            foreach (char c in asciiString)
            {
                if (c == '\n')
                    hex += "0A";

                else if (c == '\r')
                    hex += "0D";

                else if (c == '\t')
                    hex += "09";

                else
                {
                    int tmp = c;
                    hex += String.Format("{0:X2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
                }
            }
            return hex;
        }
        public static string AsciiToHex_857(string asciiString)
        {
            string hex857 = "";
            string hex = "";
            int tmp = 0;

            foreach (char c in asciiString)
            {
                hex857 = Format857(c);
                if (hex857 != "")
                {
                    hex += hex857;
                }
                if (c == '\n')
                    hex += "0A";

                else if (c == '\r')
                    hex += "0D";

                else if (c == '\t')
                    hex += "09";
                else
                {
                    tmp = c;
                    hex += String.Format("{0:X2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
                }
            }
            return hex;
        }

        public static string AsciiToHex_Format(string asciiString, string sFormatName)
        {
            string hexFormat = "";
            string hex = "";
            int tmp = 0;

            foreach (char c in asciiString)
            {
                if (sFormatName == "Format857")
                {
                    hexFormat = Format857(c);
                }
                else if (sFormatName == "UTF8")
                {
                    // buraya hangi format isteniyorsa o eklenecek.. Örneğin Azerbaycan a gidildi, oranın karakter dönüşümü yapılacak.
                    //hexFormat = FormatAzerBaijan(c);
                }

                if (hexFormat != "")
                {
                    hex += hexFormat;
                }
                else
                {
                    tmp = c;
                    hex += String.Format("{0:X2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
                }
            }
            return hex;
        }
        #endregion
        public static string AsciiToHex(string piAsciiString)
        {
            string hex = "";
            int tmp;
            byte byt;

            foreach (char c in piAsciiString)
            {
                if ((int)c > 255)
                {
                    byt = (byte)c;
                    hex += byt.ToString("00");
                }
                else
                {
                    tmp = c;
                    hex += tmp.ToString("X2");
                }
            }
            return hex;
        }

        public static string Hex2Asc(string strSource)
        {
            StringBuilder sb = new StringBuilder();

            if (strSource.Length % 2 != 0)
                strSource = strSource.PadRight(strSource.Length + 1, '0');

            for (int i = 0; i < strSource.Length; i += 2)
            {
                string hs = strSource.Substring(i, 2);
                if (hs == "00")
                    sb.Append(".");
                else
                    sb.Append(Convert.ToChar(Convert.ToUInt32(hs, 16)));
            }
            string sAscii = sb.ToString();
            return sAscii;
        }
        public static string HexToDec(string SrcStr, int srcstart)
        {
            string returnValue = "";
            //try
            //{
            long UsCarpan = 0, DecimalNum = 0, PreDecimalNum = 0;

            for (int i = 0; i < SrcStr.Length; i++)
            {
                UsCarpan = 1;
                if ((SrcStr.Substring(i + srcstart, 1) == "A") || (SrcStr.Substring(i + srcstart, 1) == "B") || (SrcStr.Substring(i + srcstart, 1) == "C") || (SrcStr.Substring(i + srcstart, 1) == "D") || (SrcStr.Substring(i + srcstart, 1) == "E") || (SrcStr.Substring(i + srcstart, 1) == "F"))
                {
                    for (int j = 1; j < SrcStr.Length - i; j++)
                        UsCarpan = UsCarpan * 16;
                    char asciiValue = Convert.ToChar(SrcStr.Substring(i + srcstart, 1));
                    string hexValue = Convert.ToString((int)asciiValue, 16);
                    PreDecimalNum = UsCarpan * (int.Parse(hexValue) - 31);
                }
                else
                {
                    for (int j = 1; j < SrcStr.Length - i; j++)
                        UsCarpan = UsCarpan * 16;
                    char asciiValue = Convert.ToChar(SrcStr.Substring(i + srcstart, 1));
                    string hexValue = Convert.ToString((int)asciiValue, 16);
                    PreDecimalNum = UsCarpan * (int.Parse(hexValue) - 30);
                }
                DecimalNum = DecimalNum + PreDecimalNum;
            }
            returnValue = DecimalNum.ToString();
            //}
            //catch (Exception ex)
            //{

            //}
            return returnValue;
        }
        public static string HexToAscii(string piHexString)
        {
            StringBuilder sb = new StringBuilder();
            string hex = piHexString;
            string ZeroChar = "";

            if ((hex.Length % 2) != 0) // 1 nibble added
                hex += "F";
            for (int i = 0; i < piHexString.Length; i += 2)
            {
                ZeroChar = hex.Substring(i, 2);

                //int il = 0;
                if (ZeroChar == "00")
                    sb.Append("0");
                else
                    sb.Append(Convert.ToChar(Int32.Parse(hex.Substring(i, 2), System.Globalization.NumberStyles.HexNumber)));
            }
            return sb.ToString();
        }
        public static byte[] StrToByteArray(string str)
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            return encoding.GetBytes(str);
        }
        // gelen datayı büyük harf yapar, boşlukları siler.
        public static string UpperAnd_DeleteSpace(string InBuff)
        {
            string OutBuff = "";

            OutBuff = InBuff.ToUpper();
            OutBuff = OutBuff.Replace(" ", "");
            OutBuff = OutBuff.Replace("\t", "");
            OutBuff = OutBuff.Replace("\r", "");
            OutBuff = OutBuff.Replace("\n", "");

            return OutBuff;
        }

        public static string GenerateUserId(int userIdLength, bool isComplex)
        {
            char[] chars = new char[] {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'j', 'h', 'i', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 'q', 's', 't', 'v', 'w', 'y', 'z',  
                                            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'J', 'H', 'I', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'Q', 'S', 'T', 'V', 'W', 'Y', 'Z', 
                                            '1', '2', '3', '4', '5', '6', '7', '8', '9'};
            string aUserId = "";
            Random rndGen = new Random();

            for (int i = 0; i < userIdLength; i++)
            {
                int index;
                if (isComplex)
                    index = rndGen.Next(chars.Length);
                else
                    index = rndGen.Next(chars.Length - 14);
                aUserId += chars[index];
            }
            return aUserId;
        }

        public static void ToHexadecimal(int n)
        {
            if (n == 0)
                return;
            else
            {
                int r = n % 16;
                n = n / 16;
                ToHexadecimal(n);


                switch (r)
                {
                    case 10:
                        Console.Write("A");
                        break;
                    case 11:
                        Console.Write("B");
                        break;
                    case 12:
                        Console.Write("C");
                        break;
                    case 13:
                        Console.Write("D");
                        break;
                    case 14:
                        Console.Write("E");
                        break;
                    case 15:
                        Console.Write("F");
                        break;
                    default:
                        Console.Write(r);
                        break;

                }

            }
        }
        public static int power(int a, int b)
        {
            try
            {
                int res;
                int i;

                res = 1;
                for (i = 0; i < b; i++)
                {
                    res = res * a;
                }
                return res;
            }
            catch (System.Exception ex)
            {
                //MessageBox.Show("Error: " + ex.Message, "Exception Info",
                //                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return 1024;
            }
        }
        public static byte[] ParseHex(string hex)
        {
            int offset = hex.StartsWith("0x") ? 2 : 0;
            if ((hex.Length % 2) != 0)
            {
                throw new ArgumentException("Invalid length: " + hex.Length);
            }
            byte[] ret = new byte[(hex.Length - offset) / 2];

            for (int i = 0; i < ret.Length; i++)
            {
                ret[i] = (byte)((ParseNybble(hex[offset]) << 4)
                                 | ParseNybble(hex[offset + 1]));
                offset += 2;
            }
            return ret;
        }

        public static int ParseNybble(char c)
        {
            if (c >= '0' && c <= '9')
            {
                return c - '0';
            }
            if (c >= 'A' && c <= 'F')
            {
                return c - 'A' + 10;
            }
            if (c >= 'a' && c <= 'f')
            {
                return c - 'a' + 10;
            }
            throw new ArgumentException("Invalid hex digit: " + c);
        }

        public static bool CheckParity_S(ref string sKey)
        {
            int discarded = 0;
            int LenKey = 0;
            int i = 0, k = 0, ParityCnt = 0, p = 0, LastBit = 0;
            byte[] TempOldKey = new byte[512];
            byte[] NewKey = new byte[512];

            try
            {
                byte[] Key = GetBytes(sKey, out discarded);

                LenKey = sKey.Length / 2;
                Array.Copy(Key, TempOldKey, LenKey);
                //memcpy(TempOldKey, Key, LenKey);

                for (i = 0; i < LenKey; i++)
                {
                    ParityCnt = 0;
                    LastBit = 0;
                    for (k = 7; k > -1; k--)
                    {
                        p = power(2, k);
                        if ((Key[i] / p) == 1)
                        {
                            ParityCnt += 1;
                            Key[i] = (byte)(Convert.ToInt32(Key[i]) - p);
                            if (k == 0)
                                LastBit = 1;
                        }
                    }
                    if ((ParityCnt % 2) != 1)//Eger Parity sayısı tek sayı değilse son bit değitirilecek.
                    {
                        if (LastBit == 1)//Son bit setmi
                            NewKey[i] = (byte)(Convert.ToInt32(TempOldKey[i]) - 1);
                        else
                            NewKey[i] = (byte)(Convert.ToInt32(TempOldKey[i]) + 1);
                    }
                    else
                        NewKey[i] = TempOldKey[i];
                }

                Array.Copy(NewKey, Key, LenKey);
                sKey = ToString(Key); ;
                return true;
                //memcpy(Key, NewKey, LenKey);
            }
            catch (System.Exception ex)
            {
                return false;
            }
        }
        public static bool CheckParity_B(ref byte[] Key, int LenKey)
        {
            int i = 0, k = 0, ParityCnt = 0, p = 0, LastBit = 0;
            byte[] TempOldKey = new byte[512];
            byte[] NewKey = new byte[512];

            try
            {
                Array.Copy(Key, TempOldKey, LenKey);
                //memcpy(TempOldKey, Key, LenKey);

                for (i = 0; i < LenKey; i++)
                {
                    ParityCnt = 0;
                    LastBit = 0;
                    for (k = 7; k > -1; k--)
                    {
                        p = power(2, k);
                        if ((Key[i] / p) == 1)
                        {
                            ParityCnt += 1;
                            Key[i] = (byte)(Convert.ToInt32(Key[i]) - p);
                            if (k == 0)
                                LastBit = 1;
                        }
                    }
                    if ((ParityCnt % 2) != 1)//Eger Parity sayısı tek sayı değilse son bit değitirilecek.
                    {
                        if (LastBit == 1)//Son bit setmi
                            NewKey[i] = (byte)(Convert.ToInt32(TempOldKey[i]) - 1);
                        else
                            NewKey[i] = (byte)(Convert.ToInt32(TempOldKey[i]) + 1);
                    }
                    else
                        NewKey[i] = TempOldKey[i];
                }

                Array.Copy(NewKey, Key, LenKey);
                return true;
                //memcpy(Key, NewKey, LenKey);
            }
            catch (System.Exception ex)
            {
                return false;
            }
        }
        public static string ConvertNibble(string sInput)
        {
            byte[] bInput = new byte[sInput.Length / 2];
            int i = 0;
            byte firstNibble = 0x00;
            byte secondNibble = 0x00;

            bInput = HexToBytes(sInput);

            for (i = 0; i < sInput.Length / 2; i++)
            {
                firstNibble = (byte)(bInput[i] & 0xF0);
                secondNibble = (byte)(bInput[i] & 0x0F);
                firstNibble = (byte)(firstNibble >> 4);
                secondNibble = (byte)(secondNibble << 4);

                bInput[i] = (byte)(firstNibble | secondNibble);
            }
            return ToString(bInput);
        }
        public static string ConvertTR2EN(string sInput)
        {
            string sOutput = "";
            sOutput = sInput;

            sOutput = sOutput.Replace('Ğ', 'G');
            sOutput = sOutput.Replace('Ü', 'U');
            sOutput = sOutput.Replace('Ş', 'S');
            sOutput = sOutput.Replace('İ', 'I');
            sOutput = sOutput.Replace('Ö', 'O');
            sOutput = sOutput.Replace('Ç', 'C');
            sOutput = sOutput.Replace('ğ', 'g');
            sOutput = sOutput.Replace('ü', 'u');
            sOutput = sOutput.Replace('ş', 's');
            sOutput = sOutput.Replace('ö', 'o');
            sOutput = sOutput.Replace('ç', 'c');

            return sOutput;
        }

        public static void GetStringParameter(string str, string findS, string findE, ref string param, ref int index)
        {
            int indexS = 0, indexE = 0;
            string value = "";

            value = str.Substring(index);

            indexS = value.IndexOf(findS);
            indexS += findS.Length;

            if (string.IsNullOrEmpty(findE))
                indexE = indexS + value.Substring(indexS).Length; // end seperatör yoksa, start seperatörden sonraki tüm veriyi al.
            else
                indexE = indexS + value.Substring(indexS).IndexOf(findE); // start index den sonraki findEnd i ara. 

            param = value.Substring(indexS, indexE - indexS);

            index = indexE + findE.Length;

            return;
        }
        public static string Reverse(string str)
        {
            int len = str.Length;
            char[] arr = new char[len];

            for (int i = 0; i < len; i++)
            {
                arr[i] = str[len - 1 - i];
            }

            return new string(arr);
        }
        public static string ReverseStr(string sInput)
        {
            int i = 0;
            string sOutput = "";

            for (i = sInput.Length - 2; i >= 0; i -= 2)
            {
                sOutput += sInput.Substring(i, 2);
            }

            return sOutput;
        }
        public static string CalculateLRC(string sInput)
        {
            int i = 0;
            byte[] bInput = new byte[sInput.Length / 2];
            byte LRC = 0x00;

            bInput = Utility.HexToBytes(sInput);

            for (i = 0; i < sInput.Length / 2; i++)
            {
                LRC ^= bInput[i];
            }

            return LRC.ToString("X2");
        }
        public static string RSAPublicEncrypt(string sInp, string sPub, string sExp)
        {
            int iRetCode = 0;
            StringBuilder sbModulus = new StringBuilder(sPub);
            StringBuilder sbExponent = new StringBuilder(sExp);
            StringBuilder sbInput = new StringBuilder(sInp);

            StringBuilder sbPriExponent = new StringBuilder(0);
            StringBuilder sbPrimeP = new StringBuilder(0);
            StringBuilder sbPrimeQ = new StringBuilder(0);

            StringBuilder sbOutput = new StringBuilder(10000);
            StringBuilder sbRetDesc = new StringBuilder(1000);
            try
            {
                ExpDll.CPlusPlusRsaCryptoService(ExpDll.MODE_RSA_ENCRYPT, sbModulus, sbExponent, sbPriExponent, sbPrimeP, sbPrimeQ, sbInput, sbOutput, ref iRetCode, sbRetDesc);
                return sbOutput.ToString();
            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
            return "";
        }

        public static string sha1(string sInput)
        {
            System.Security.Cryptography.SHA1Managed SHA_X = new System.Security.Cryptography.SHA1Managed();
            byte[] hash = SHA_X.ComputeHash(HexToBytes(sInput));
            return ToString(hash);
        }

        public static string HashSha1(string input)
        {
           return  FormsAuthentication.HashPasswordForStoringInConfigFile(input, "sha1");
        }

        //Function to get random string
        public static string RandomString(int len)
        {
            string sBuff = "";
            Random random = new Random();

            while (sBuff.Length < len)
            {
                sBuff += random.Next(1, 99999999).ToString("X2");
            }
            return sBuff.Substring(0, len);
        }

        public static string GetIPAddress_InterNetwork()
        {
            string IPAddress = "";
            string strHostName = "";
            try
            {
                strHostName = System.Net.Dns.GetHostName();
                System.Net.IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);
                System.Net.IPAddress[] ipv4Addresses = Array.FindAll(System.Net.Dns.GetHostEntry(string.Empty).AddressList, a => a.AddressFamily == AddressFamily.InterNetwork);

                IPAddress = ipv4Addresses[0].ToString();
                return IPAddress;
            }
            catch (Exception ex)
            {
            }
            return "127.0.0.1";
        }

        public static void WriteLog(string sPath, string sLogData, bool logFlag, bool encoding)
        {
            try
            {
                if (logFlag != true)
                    return;
                StreamWriter file = new StreamWriter(sPath, encoding);
                file.WriteLine("#| " + DateTime.Now + "  -----------  " + sLogData);
                file.Close();
            }
            catch (System.Exception ex)
            {
                //MessageBox.Show("Error: " + ex.Message, "Exception Info",
                //                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }
        public static string GetStartupPath()
        {
            return Environment.CurrentDirectory;
        }
        public static int GetConfigInt(string configName, int defaultValue)
        {
            try { return int.Parse(GetConfigStr(configName, defaultValue.ToString())); }
            catch { return defaultValue; }
        }
        public static string GetConfigStr(string configName, string defaultValue)
        {
            try
            {
                if (ConfigurationManager.AppSettings.Count != 0)
                {
                    if (ConfigurationManager.AppSettings[configName] == null)
                        return defaultValue;
                    else
                        return ConfigurationManager.AppSettings[configName];
                }
                return defaultValue;

            }
            catch
            {
                return defaultValue;
            }
        }
        public static void SetConfigStr(string configName, string setValue)
        {
            try
            {
                ConfigurationManager.AppSettings[configName] = setValue;
            }
            catch
            {
            }
        }
        public static string getHostName(string ip)
        {
            /* 
            This method is created by Anton Zamov. 
            web site: http://zamov.online.fr 

            Feel free to use and redistribute this method 
            in condition that you keep this message intact. 
            */
            try
            {
                System.Net.IPHostEntry IpEntry = System.Net.Dns.GetHostByAddress(ip);
                return IpEntry.HostName.ToString();
            }
            catch
            {
                return "";
            }
        }
        public static void Exit(int code)
        {
            Environment.Exit(code);
        }

        public static void WriteInfoLog(string fileDirectory, string Desc, string signCode)
        {
            string filePath = fileDirectory + DateTime.Now.Year.ToString() + "\\" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "\\" + DateTime.Now.Day.ToString().PadLeft(2, '0');

            string LogFile = "";
            StreamWriter sw;
            try
            {
                //Directory checking
                if (!Directory.Exists(filePath))
                    Directory.CreateDirectory(filePath);
                //File operations
                int hourValSrt = DateTime.Now.Hour;
                int hourValEnd = DateTime.Now.Hour + 1;
                LogFile = filePath + "\\" + hourValSrt.ToString().PadLeft(2, '0') + "-" + hourValEnd.ToString().PadLeft(2, '0') + ".txt";
                if (!File.Exists(LogFile))
                    sw = File.CreateText(LogFile);
                else
                {
                    if ((File.GetAttributes(LogFile) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    {
                        File.SetAttributes(LogFile, System.IO.FileAttributes.Normal);
                    }
                    sw = File.AppendText(LogFile);
                }
                //Writing error to file
                sw.WriteLine("Date Time    : " + DateTime.Now);
                sw.WriteLine("SignCode  : " + signCode.ToString());
                sw.WriteLine(Desc);
                sw.WriteLine("----------------------------------------");
                sw.Close();
            }
            catch (Exception ex)
            {

            }
        }

        public static void WriteErrorLog(string logDirectory, Exception Exp, string signCode)
        {
            string filePath = logDirectory + DateTime.Now.Year.ToString() + "\\" + DateTime.Now.Month.ToString().PadLeft(2, '0') + "\\" + DateTime.Now.Day.ToString().PadLeft(2, '0');
            string ErrLogFile = "";
            StreamWriter sw;
            try
            {
                //Directory checking
                if (!Directory.Exists(filePath))
                    Directory.CreateDirectory(filePath);
                //File operations
                int hourValSrt = DateTime.Now.Hour;
                int hourValEnd = DateTime.Now.Hour + 1;
                ErrLogFile = filePath + "\\" + hourValSrt.ToString().PadLeft(2, '0') + "-" + hourValEnd.ToString().PadLeft(2, '0') + ".txt";
                if (!File.Exists(ErrLogFile))
                    sw = File.CreateText(ErrLogFile);
                else
                {
                    if ((File.GetAttributes(ErrLogFile) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    {
                        File.SetAttributes(ErrLogFile, System.IO.FileAttributes.Normal);
                    }
                    sw = File.AppendText(ErrLogFile);
                }
                //Writing error to file
                sw.WriteLine("Date Time    : " + DateTime.Now);
                sw.WriteLine("Sign Code    : " + signCode);
                sw.WriteLine("Source       : " + Exp.Source);
                sw.WriteLine("Description  : " + Exp.Message);
                sw.WriteLine("Stack Trace  : " + Exp.StackTrace);
                sw.WriteLine("----------------------------------------");
                sw.Close();
            }
            catch
            {

            }
        }

        public static void LogError(Exception ex)
        {
            string strLogMessage = "\nMessage : " + ex.Message +
                "\nSource : " + ex.Source +
                "\nTarget Site : " + ex.TargetSite +
                "\nStack Trace : " + ex.StackTrace;
            string logName = "PRCN";
            if (!EventLog.SourceExists(logName))
            {
                EventLog.CreateEventSource(logName, logName);
            }
            EventLog log = new EventLog();
            log.Source = logName;
            strLogMessage += "\r\n\r\n--------------------------\r\n\r\n" + ex.ToString();
            log.WriteEntry(strLogMessage, EventLogEntryType.Error, 65534);
        }

        public static void LogError(Exception ex, string specificMessage)
        {
            string strLogMessage = "\nMessage : " + ex.Message +
                "\nSpecificMessage : " + specificMessage +
                "\nSource : " + ex.Source +
                "\nTarget Site : " + ex.TargetSite +
                "\nStack Trace : " + ex.StackTrace;

            string logName = "PRCN";
            if (!EventLog.SourceExists(logName))
            {
                EventLog.CreateEventSource(logName, logName);
            }
            EventLog log = new EventLog();
            log.Source = logName;
            strLogMessage += "\r\n\r\n--------------------------\r\n\r\n" + ex.ToString();
            log.WriteEntry(strLogMessage, EventLogEntryType.Error, 65534);
        }

        public static string GetConfigValue(string configKey)
        {
            string configValue = ConfigurationManager.AppSettings[configKey];

            if (configValue == null || configValue == "")
            {
                if (appConfigXML == null)
                {
                    appConfigXML = new XmlDocument();
                    appConfigXML.Load(ConfigurationManager.AppSettings["ConfigFilesPath"]);
                }
                try
                {
                    XmlNode selectedNode = appConfigXML.SelectSingleNode("configuration/appSettings/add[@key='" + configKey + "']");
                    configValue = selectedNode.Attributes["value"].Value;
                }
                catch (Exception exp)
                {
                    Exception innerException = new Exception(("Could not load the config element : " + configKey));
                    innerException.Source = exp.Source;
                    WriteErrorLog("C:\\PRCN\\Log\\", exp, "GetConfigValue" + "---key---" +configKey);
                }
                finally
                {

                }
            }
            return configValue;

        }

        public static bool Contains(string[] array, string element)
        {
            bool status = false;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == element)
                {
                    status = true;
                    break;
                }
            }
            return status;
        }

        public static bool Contains(int[] array, int element)
        {
            bool status = false;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == element)
                {
                    status = true;
                    break;
                }
            }
            return status;
        }

        public static string GetAsString(object[] array, char delimeter)
        {
            string result = "";
            for (int i = 0; i < array.Length; i++)
            {
                result += array[i].ToString() + delimeter;
            }
            result = result.Trim(delimeter);
            return result;
        }

        public static string GetAsString(int[] array, char delimeter)
        {
            string result = "";
            for (int i = 0; i < array.Length; i++)
            {
                result += array[i].ToString() + delimeter;
            }
            result = result.Trim(delimeter);
            return result;
        }

        public static string GetAsString(double[] array, char delimeter)
        {
            string result = "";
            for (int i = 0; i < array.Length; i++)
            {
                result += array[i].ToString() + delimeter;
            }
            result = result.Trim(delimeter);
            return result;
        }

        //public static void SendEmail(string senderEmailAddress,
        //    string senderName,
        //    string[] emailAddresses,
        //    string subject,
        //    string message,
        //    string[] attachmentFiles)
        //{
        //    System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
        //    mailMessage.From = mailMessage.Sender = new System.Net.Mail.MailAddress(senderEmailAddress, senderName);
        //    for (int i = 0; i < emailAddresses.Length; i++)
        //    {
        //        mailMessage.To.Add(new MailAddress(emailAddresses[i]));
        //    }

        //    mailMessage.IsBodyHtml = true;
        //    mailMessage.Subject = subject;
        //    mailMessage.Body = message;
        //    mailMessage.SubjectEncoding = mailMessage.BodyEncoding = System.Text.Encoding.GetEncoding("iso-8859-9");

        //    if (attachmentFiles != null && attachmentFiles.Length > 0)
        //    {
        //        for (int i = 0; i < attachmentFiles.Length; i++)
        //        {
        //            if (System.IO.File.Exists(attachmentFiles[i]))
        //                mailMessage.Attachments.Add(new System.Net.Mail.Attachment(attachmentFiles[i]));
        //        }
        //    }

        //    try
        //    {
        //        int smtpServerPort = Convert.ToInt32(GetConfigValue("SMTPServerPort"));
        //        bool smtpUseSSL = Convert.ToBoolean(GetConfigValue("SMTPServerUseSSL"));

        //        if (smtpServerPort == 0)
        //            smtpServerPort = 25;

        //        System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient();
        //        smtpClient.Host = GetConfigValue("SMTPServer");
        //        smtpClient.EnableSsl = smtpUseSSL;
        //        smtpClient.Port = smtpServerPort;

        //        bool requiresLogin = Convert.ToBoolean(GetConfigValue("SMTPServerRequiresLogin"));

        //        if (requiresLogin)
        //        {
        //            string smtpUsername = GetConfigValue("SMTPServerUsername");
        //            string smtpPassword = GetConfigValue("SMTPServerPassword");
        //            smtpClient.UseDefaultCredentials = false;
        //            smtpClient.Credentials = new NetworkCredential(smtpUsername, smtpPassword);
        //        }

        //        smtpClient.Send(mailMessage);
        //    }
        //    catch (Exception exp)
        //    {
        //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "sendEmail");

        //    }
        //    finally
        //    {

        //    }
        // }

        public static string EncryptString(string stringToEncrypt)
        {
            if (stringToEncrypt != null)
            {
                string first = EncryptString(stringToEncrypt, encryptionKey1);
                return first;
            }
            else
            {
                return null;
            }
        }

        private static string EncryptString(string stringToEncrypt, string encKey)
        {
            AESEncryption encProvider = new AESEncryption();
            return encProvider.Encrypt(stringToEncrypt, encKey);
        }

        public static string DecryptString(string stringToDecrypt)
        {
            stringToDecrypt = stringToDecrypt.Replace(" ", "+");
            if (stringToDecrypt != null)
            {
                string first = DecryptString(stringToDecrypt, encryptionKey1);
                return first;
            }
            else
            {
                return null;
            }
        }

        private static string DecryptString(string stringToDecrypt, string encKey)
        {
            AESEncryption encProvider = new AESEncryption();
            return encProvider.Decrypt(stringToDecrypt, encKey);
        }

        public static string GeneratePassword(int passwordLength, bool isComplex)
        {
            char[] chars = new char[] {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'j', 'h', 'i', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 'q', 's', 't', 'v', 'w', 'y', 'z',  
                                            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'J', 'H', 'I', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'Q', 'S', 'T', 'V', 'W', 'Y', 'Z', 
                                            '1', '2', '3', '4', '5', '6', '7', '8', '9', 
                                            '!', '?', '#', '$', '%', '&', '/', '(', ')', '=', '*', '\\', '-', '_'};
            string aPassword = "";
            Random rndGen = new Random();

            for (int i = 0; i < passwordLength; i++)
            {
                int index;
                if (isComplex)
                    index = rndGen.Next(chars.Length);
                else
                    index = rndGen.Next(chars.Length - 14);
                aPassword += chars[index];
            }
            return aPassword;
        }

        public static string ExecuteHttpRequest(string url,
                                                      string method,
                                                      byte[] postData,
                                                      ref HttpStatusCode statusCode)
        {
            string httpResponse = "";
            Encoding encoding = Encoding.UTF8;
            HttpWebRequest httpWebRequest = null;
            try
            {
                //httpWebRequest.Headers.Add("deneme", "deneme");

                httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.Method = method;

                if (method == "POST")
                {
                    httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                    httpWebRequest.ContentLength = postData.LongLength;
                    Stream requestStream = httpWebRequest.GetRequestStream();
                    requestStream.Write(postData, 0, postData.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }

                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                BufferedStream responseStream = new BufferedStream(httpWebResponse.GetResponseStream());
                StreamReader rd = new StreamReader(responseStream, encoding);
                httpResponse = rd.ReadToEnd();
                rd.Close();
                rd.Dispose();
                statusCode = httpWebResponse.StatusCode;
            }
            catch (WebException exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UExecuteHttpRequest");
                httpResponse = exp.Message;
                try
                {
                    statusCode = ((HttpWebResponse)exp.Response).StatusCode;
                }
                catch
                {
                    statusCode = HttpStatusCode.RequestTimeout;
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExecuteHttpRequest");

                httpResponse = exp.Message;
                statusCode = HttpStatusCode.BadRequest;
            }
            finally
            {

            }
            return httpResponse;
        }

        public static string ExecuteHttpRequest(string url,
                                                      string method,
                                                      NameValueCollection parameters,
                                                      ref HttpStatusCode statusCode)
        {
            string httpResponse = "";
            Encoding encoding = Encoding.UTF8;

            string postData = "";
            for (int i = 0; i < parameters.AllKeys.Length; i++)
            {
                postData += parameters.Keys[i] + "=" + parameters[parameters.Keys[i].ToString()] + "&";
            }
            postData = postData.TrimEnd(new char[] { '&' });

            byte[] postDataAsByte = null;

            if (method == "GET")
            {
                url = url.TrimEnd(new char[] { '&', '?', ' ' }) + "?" + postData;
            }
            else if (method == "POST")
            {
                postDataAsByte = encoding.GetBytes(postData);
            }

            httpResponse = ExecuteHttpRequest(url, method, postDataAsByte, ref statusCode);

            return httpResponse;
        }

        public static string ExecuteHttpRequest(string url,
                                                     string method,
                                                     byte[] postData,
                                                     string accessToken,
                                                     ref HttpStatusCode statusCode)
        {
            string httpResponse = "";
            Encoding encoding = Encoding.UTF8;
            HttpWebRequest httpWebRequest = null;
            try
            {


                httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.Method = method;
                httpWebRequest.Timeout = Convert.ToInt32(Utility.GetConfigValue("TIMEOUT"))*1000;

                if (!String.IsNullOrEmpty(accessToken))
                    httpWebRequest.Headers.Add("AccessToken", accessToken);

                if (method == "POST")
                {
                    httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                    httpWebRequest.ContentLength = postData.LongLength;
                    Stream requestStream = httpWebRequest.GetRequestStream();
                    requestStream.Write(postData, 0, postData.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }

                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                BufferedStream responseStream = new BufferedStream(httpWebResponse.GetResponseStream());
                StreamReader rd = new StreamReader(responseStream, encoding);
                httpResponse = rd.ReadToEnd();
                rd.Close();
                rd.Dispose();
                statusCode = httpWebResponse.StatusCode;
            }
            catch (WebException exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UExecuteHttpRequest");
                httpResponse = exp.Message;
                try
                {
                    statusCode = ((HttpWebResponse)exp.Response).StatusCode;
                }
                catch
                {
                    statusCode = HttpStatusCode.RequestTimeout;
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExecuteHttpRequest");

                httpResponse = exp.Message;
                statusCode = HttpStatusCode.BadRequest;
            }
            finally
            {

            }
            return httpResponse;
        }
    }

    public class AESEncryption
    {
        private byte[] IV = new byte[] { 0x37, 120, 0x54, 0x8e, 0xac, 0x33, 9, 0xc9, 0xc7, 0x52, 0xec, 0x1b, 0x5e, 5, 0x8a, 0xe5 };
        private byte[] key = new byte[0];

        public string Decrypt(string stringToDecrypt, string sEncryptionKey)
        {
            byte[] buffer = new byte[stringToDecrypt.Length];
            try
            {
                this.key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 0x20));
                AesCryptoServiceProvider provider = new AesCryptoServiceProvider();
                provider.KeySize = 0x100;
                provider.IV = this.IV;
                provider.Key = this.key;
                ICryptoTransform transform = provider.CreateDecryptor();
                buffer = Convert.FromBase64String(stringToDecrypt);
                MemoryStream stream = new MemoryStream();
                CryptoStream stream2 = new CryptoStream(stream, transform, CryptoStreamMode.Write);
                stream2.Write(buffer, 0, buffer.Length);
                stream2.FlushFinalBlock();
                return Encoding.UTF8.GetString(stream.ToArray());
            }
            catch (Exception)
            {
                return null;
            }
        }

        public string Encrypt(string stringToEncrypt, string sEncryptionKey)
        {
            try
            {
                this.key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 0x20));
                AesCryptoServiceProvider provider = new AesCryptoServiceProvider();
                provider.KeySize = 0x100;
                provider.IV = this.IV;
                provider.Key = this.key;
                ICryptoTransform transform = provider.CreateEncryptor();
                byte[] bytes = Encoding.UTF8.GetBytes(stringToEncrypt);
                MemoryStream stream = new MemoryStream();
                CryptoStream stream2 = new CryptoStream(stream, transform, CryptoStreamMode.Write);
                stream2.Write(bytes, 0, bytes.Length);
                stream2.FlushFinalBlock();
                return Convert.ToBase64String(stream.ToArray());
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string CalculateLRC(string sInput)
        {
            int i = 0;
            byte[] bInput = new byte[sInput.Length / 2];
            byte LRC = 0x00;

            bInput = HexToBytesPrc(sInput);

            for (i = 0; i < sInput.Length / 2; i++)
            {
                LRC ^= bInput[i];
            }

            return LRC.ToString("X2");
        }

        public static byte[] HexToBytes(string hexString)
        {
            //check for null
            if (hexString == null) return null;
            //get length
            int len = hexString.Length;
            if (len % 2 == 1) return null;
            int len_half = len / 2;
            //create a byte array
            byte[] bs = new byte[len_half];
            try
            {
                //convert the hexstring to bytes
                for (int i = 0; i != len_half; i++)
                {
                    bs[i] = (byte)Int32.Parse(hexString.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(”Exception : ” + ex.Message);
            }
            //return the byte array
            return bs;
        }

        public static byte[] GetBytes(string hexString, out int discarded)
        {
            discarded = 0;
            string newString = "";
            char c;
            // remove all none A-F, 0-9, characters
            for (int i = 0; i < hexString.Length; i++)
            {
                c = hexString[i];
                if (IsHexDigit(c))
                    newString += c;
                else
                    discarded++;
            }
            // if odd number of characters, discard last character
            if (newString.Length % 2 != 0)
            {
                discarded++;
                newString = newString.Substring(0, newString.Length - 1);
            }

            int byteLength = newString.Length / 2;
            byte[] bytes = new byte[byteLength];
            string hex;
            int j = 0;
            for (int i = 0; i < bytes.Length; i++)
            {
                hex = new String(new Char[] { newString[j], newString[j + 1] });
                bytes[i] = HexToByte(hex);
                j = j + 2;
            }
            return bytes;
        }

        public static byte HexToByte(string hex)
        {
            if (hex.Length > 2 || hex.Length <= 0)
                throw new ArgumentException("hex must be 1 or 2 characters in length");
            byte newByte = byte.Parse(hex, System.Globalization.NumberStyles.HexNumber);
            return newByte;
        }

        public static bool IsHexDigit(Char c)
        {
            int numChar;
            int numA = Convert.ToInt32('A');
            int num1 = Convert.ToInt32('0');
            c = Char.ToUpper(c);
            numChar = Convert.ToInt32(c);
            if (numChar >= numA && numChar < (numA + 6))
                return true;
            if (numChar >= num1 && numChar < (num1 + 10))
                return true;
            return false;
        }

        public static byte[] HexToBytesPrc(string hexString)
        {
            //check for null
            if (hexString == null) return null;
            //get length
            int len = hexString.Length;
            if (len % 2 == 1) return null;
            int len_half = len / 2;
            //create a byte array
            byte[] bs = new byte[len_half];
            try
            {
                //convert the hexstring to bytes
                for (int i = 0; i != len_half; i++)
                {
                    bs[i] = (byte)Int32.Parse(hexString.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(”Exception : ” + ex.Message);
            }
            //return the byte array
            return bs;
        }

        public static byte[] GetBytesPrcn(string hexString, out int discarded)
        {
            discarded = 0;
            string newString = "";
            char c;
            // remove all none A-F, 0-9, characters
            for (int i = 0; i < hexString.Length; i++)
            {
                c = hexString[i];
                if (IsHexDigit(c))
                    newString += c;
                else
                    discarded++;
            }
            // if odd number of characters, discard last character
            if (newString.Length % 2 != 0)
            {
                discarded++;
                newString = newString.Substring(0, newString.Length - 1);
            }

            int byteLength = newString.Length / 2;
            byte[] bytes = new byte[byteLength];
            string hex;
            int j = 0;
            for (int i = 0; i < bytes.Length; i++)
            {
                hex = new String(new Char[] { newString[j], newString[j + 1] });
                bytes[i] = HexToByte(hex);
                j = j + 2;
            }
            return bytes;
        }

    }

}
