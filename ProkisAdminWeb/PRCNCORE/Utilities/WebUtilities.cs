﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Web;
using System.Xml;
using System.Xml.XPath;
using PRCNCORE.Constants;

namespace PRCNCORE.Utilities
{
    public class WebUtilities
    {
        public static string GetPagingText(int pageCount,
            int currentPageNum,
            int recordCount)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<!-- recordCount=" + recordCount + ",pageCount=" + pageCount + ",pageNo=" + currentPageNum + " //-->");
            int startIndex = currentPageNum - 10;
            if (startIndex < 0)
                startIndex = 0;
            int endIndex = currentPageNum + 10;

            if (endIndex > pageCount)
                endIndex = pageCount;

            if (currentPageNum != 0)
                sb.Append("<a href=\"javascript:postForPaging(" + (currentPageNum - 1) + ");\" class=pagingItem>&lt;-- Previous</a>\n");
            for (int i = startIndex; i < endIndex; i++)
            {
                if (i != currentPageNum)
                    sb.Append("<a href=\"javascript:postForPaging(" + +(i) + ");\" class=pagingItem>" + (i + 1) + "</a>\n");
                else
                    sb.Append("<span class=pagingItemCurrent>" + (i + 1) + "</span>\n");
            }
            if (currentPageNum < (pageCount - 1))
                sb.Append("<a href=\"javascript:postForPaging(" + (currentPageNum + 1) + ");\" class=pagingItem>Next --&gt;</a>\n");
            return sb.ToString();
        }

        public static bool IsSecureConnection(HttpRequest request)
        {
            bool result = false;

            result = request.IsSecureConnection;

            if (!result && request.Headers["X-SSL-cipher"] != null)
            {
                result = true;
            }
            else
            {
                result = false;
            }

            return result;
        }
    }
}
