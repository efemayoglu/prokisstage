﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Utilities
{
    public class Dekont
    {
        public List<DekontFields> kioskList { get; set; }
    }
    public class DekontFields
    {
        public string InsertionDate { get; set; }
        public string TransactionId { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string KioskName { get; set; }
        public string PaymentType { get; set; }
        public string CreditCardCustomer { get; set; }
        public string CreditCardNumber { get; set; }
        public string InstitutionAmount { get; set; }
        public string UsageFee { get; set; }
        public string CommisionFee { get; set; }
        public string InstitutionId { get; set; }
        public string Institution { get; set; }
        public string TotalAmount { get; set; }

    }
}
