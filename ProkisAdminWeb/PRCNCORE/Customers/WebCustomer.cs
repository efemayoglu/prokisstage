﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Customers
{
    public class WebCustomer
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private string name;
        public string Name
        {
            get { return name; }
        }

        private string customerNo;
        public string CustomerNo
        {
            get { return customerNo; }
        }

        private string tckn;
        public string Tckn
        {
            get { return tckn; }
        }

        private string email;
        public string Email
        {
            get { return email; }
        }

        private string cellPhone;
        public string CellPhone
        {
            get { return cellPhone; }
        }

        private string status;
        public string Status
        {
            get { return status; }
        }

        private string approveUser;
        public string ApproveUser
        {
            get { return approveUser; }
        }

        private string approveTime;
        public string ApproveTime
        {
            get { return approveTime; }
        }

        private int approveStatus;
        public int ApproveStatus
        {
            get { return approveStatus; }
        }

        internal WebCustomer(int id
            , string name
            , string customerNo
            , string tckn
            , string email
            , string cellPhone
            , string status)
        {
            this.id = id;
            this.name = name;
            this.customerNo = customerNo;
            this.tckn = tckn;
            this.email = email;
            this.cellPhone = cellPhone;
            this.status = status;
        }

        internal WebCustomer(int id
           , string name
           , string customerNo
           , string tckn
           , string email
           , string cellPhone
           , string status
           , string approveUser
           , string approveTime
           , int approveStatus)
        {
            this.id = id;
            this.name = name;
            this.customerNo = customerNo;
            this.tckn = tckn;
            this.email = email;
            this.cellPhone = cellPhone;
            this.status = status;
            this.approveUser = approveUser;
            this.approveTime = approveTime;
            this.approveStatus = approveStatus;
        }
    }
}
