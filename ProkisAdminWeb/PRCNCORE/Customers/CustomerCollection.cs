﻿using PRCNCORE.Backend;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace PRCNCORE.Customers
{
    public class CustomerCollection : ReadOnlyCollection<Customer>
    {
        internal CustomerCollection()
            : base(new List<Customer>())
        {

        }

        internal void Add(Customer customer)
        {
            if (customer != null && !this.Contains(customer))
            {
                this.Items.Add(customer);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (Customer item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("Customers", jArray);

            return jObjectDis;
        }
    }
}


