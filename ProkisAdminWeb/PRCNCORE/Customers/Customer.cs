﻿using PRCNCORE.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PRCNCORE.Customers
{
    public class Customer
    {
        private int customerId;
        public int CustomerId
        {
            get { return customerId; }
        }

        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
        }

        private string aboneNo;
        public string AboneNo
        {
            get { return aboneNo; }
        }

        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

        private string lastTransactionDate;
        public string LastTransactionDate
        {
            get { return lastTransactionDate; }
        }

        private string transactionCount;
        public string TransactionCount
        {
            get { return transactionCount; }
        }

        private string instutionName;
        public string InstutionName
        {
            get { return instutionName; }
        }

        private int instutionId;
        public int InstutionId
        {
            get { return instutionId; }
        }

        internal Customer(int customerId
            , string customerName
            , string aboneNo
            , string insertionDate
            , string lastTransactionDate
            , string transactionCount
            , string instutionName
            , int instutionId)
        {
            this.customerId = customerId;
            this.customerName = customerName;
            this.aboneNo = aboneNo;
            this.insertionDate = insertionDate;
            this.lastTransactionDate = lastTransactionDate;
            this.transactionCount = transactionCount;
            this.instutionName = instutionName;
            this.instutionId = instutionId;
        }
    }
}