﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Customers
{
    public class WebCustomerCollection : ReadOnlyCollection<WebCustomer>
    {
        internal WebCustomerCollection()
            : base(new List<WebCustomer>())
        {

        }

        internal void Add(WebCustomer customer)
        {
            if (customer != null && !this.Contains(customer))
            {
                this.Items.Add(customer);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (WebCustomer item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("Customers", jArray);

            return jObjectDis;
        }
    }
}

