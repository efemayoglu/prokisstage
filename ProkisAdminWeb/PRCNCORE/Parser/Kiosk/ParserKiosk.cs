﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserKiosk
    {
        [JsonProperty("kioskId")]
        public int KioskId { get; set; }

        [JsonProperty("kioskName")]
        public string KioskName { get; set; }

        [JsonProperty("kioskAddress")]
        public string KioskAddress { get; set; }

        [JsonProperty("kioskIp")]
        public string KioskIp { get; set; }

        [JsonProperty("kioskStatusId")]
        public int KioskStatusId { get; set; }

        [JsonProperty("kioskStatusName")]
        public string KioskStatusName { get; set; }

        [JsonProperty("kioskInsertionDate")]
        public string KioskInsertionDate { get; set; }

        [JsonProperty("latitude")]
        public string Latitude { get; set; }

        [JsonProperty("longitude")]
        public string Longitude { get; set; }

        [JsonProperty("cityId")]
        public int CityId { get; set; }

        [JsonProperty("cityName")]
        public string CityName { get; set; }

        [JsonProperty("regionId")]
        public int RegionId { get; set; }

        [JsonProperty("regionName")]
        public string RegionName { get; set; }

        [JsonProperty("kioskTypeId")]
        public int KioskTypeId { get; set; }

        [JsonProperty("kioskTypeName")]
        public string KioskTypeName { get; set; }

        [JsonProperty("usageFee")]
        public string UsageFee { get; set; }

        [JsonProperty("appVersion")]
        public string appVersion { get; set; }

        [JsonProperty("lastAppAliveTime")]
        public string lastAppAliveTime { get; set; }

        [JsonProperty("capacity")]
        public int Capacity { get; set; }

        [JsonProperty("Explanation")]
        public string Explanation { get; set; }

        [JsonProperty("CashBoxAcceptorTypeName")]
        public string CashBoxAcceptorTypeName { get; set; }

        [JsonProperty("CashBoxAcceptorTypeId")]
        public string CashBoxAcceptorTypeId { get; set; }

        [JsonProperty("AccomplishedTransactions")]
        public string AccomplishedTransactions { get; set; }
 
        [JsonProperty("connectionTypeId")]
        public int ConnectionTypeId { get; set; }

        [JsonProperty("connectionType")]
        public string ConnectionType { get; set; }

        [JsonProperty("cardReaderTypeId")]
        public int CardReaderTypeId { get; set; }

        [JsonProperty("cardReaderType")]
        public string CardReaderType { get; set; }


        [JsonProperty("institutionName")]
        public string institutionName { get; set; }

        [JsonProperty("institutionId")]
        public int institutionId { get; set; }

        [JsonProperty("riskClass")]
        public string riskClass { get; set; }

        [JsonProperty("isCreditCard")]
        public string IsCreditCard { get; set; }

        [JsonProperty("isMobilePayment")]
        public string IsMobilePayment { get; set; }

        [JsonProperty("AppAliveDurationTime")]
        public string AppAliveDurationTime { get; set; }

        [JsonProperty("MakbuzStatus")]
        public string MakbuzStatus { get; set; }

        [JsonProperty("JCMStatus")]
        public string JCMStatus { get; set; }
       
    }
}
