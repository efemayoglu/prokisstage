﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserListKioskName
    {
        [JsonProperty("KioskNames")]
        public List<ParserKioskName> KioskNames { get; set; }

        [JsonProperty("errorCode")]
        public int errorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string errorDescription { get; set; }

    }

    public class ParserPhoneNumber
    {
        [JsonProperty("phoneNumber")]
        public string phoneNumber { get; set; }

    }
    public class CancelTransactionResult
    {

        [JsonProperty("CancelTransaction")]
        public List<CancelTransaction> CancelTransaction { get; set; }

    }
    public class CancelTransaction
    {
        
        [JsonProperty("BillNumber")]
        public string MakbuzNo { get; set; }

        [JsonProperty("BillDate")]
        public string MakbuzTarihi { get; set; }

        [JsonProperty("CustomerNo")]
        public string AboneNo { get; set; }

    }
}
