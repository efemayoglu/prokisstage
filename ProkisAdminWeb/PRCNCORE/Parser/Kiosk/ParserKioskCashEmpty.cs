﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserKioskCashEmpty
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("kioskId")]
        public int kioskId { get; set; }

        [JsonProperty("kioskName")]
        public string kioskName { get; set; }

        [JsonProperty("expertUserId")]
        public string expertUserId { get; set; }

        [JsonProperty("expertUserName")]
        public string expertUserName { get; set; }

        [JsonProperty("cashCount")]
        public int cashCount { get; set; }

        [JsonProperty("coinCount")]
        public int coinCount { get; set; }

        [JsonProperty("ribbonRate")]
        public int ribbonRate { get; set; }

        [JsonProperty("updatedDate")]
        public string updatedDate { get; set; }

        [JsonProperty("amount")]
        public string amount { get; set; }

        [JsonProperty("securityCode")]
        public string securityCode { get; set; }

        [JsonProperty("ReferenceNo")]
        public string referenceNo { get; set; }

    }
}
