﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Kiosk
{
    public class ParserGetKioskReferenceNumber
    {
        [JsonProperty("GetKioskReferenceNumber")]
        public ParserKioskReferenceNumber KioskReferenceNumber { get; set; }
    }
    public class ParserKioskReferenceNumber
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("ReferenceNumber")]
        public string ReferenceNumber { get; set; }
    }

}
