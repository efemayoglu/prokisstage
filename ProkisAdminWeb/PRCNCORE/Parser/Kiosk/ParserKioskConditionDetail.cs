﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserKioskConditionDetail
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("kioskId")]
        public int kioskId { get; set; }

        [JsonProperty("kioskName")]
        public string kioskName { get; set; }

        [JsonProperty("updatedDate")]
        public string updatedDate { get; set; }

        [JsonProperty("aboneNo")]
        public string aboneNo { get; set; }

        [JsonProperty("amount")]
        public string amount { get; set; }

        [JsonProperty("cashCount")]
        public int cashCount { get; set; }

        [JsonProperty("coinCount")]
        public int coinCount { get; set; }

        [JsonProperty("ribbonRate")]
        public int ribbonRate { get; set; }

        [JsonProperty("receivedAmount")]
        public string receivedAmount { get; set; }

        [JsonProperty("cashTypeId")]
        public int cashTypeId { get; set; }

        [JsonProperty("cashTypeName")]
        public string cashTypeName { get; set; }

        [JsonProperty("transactionId")]
        public int transactionId { get; set; }

  }
}
