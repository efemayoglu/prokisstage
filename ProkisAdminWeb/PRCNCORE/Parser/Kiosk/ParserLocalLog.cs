﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Kiosk
{
    public class ParserLocalLog
    {
        [JsonProperty("logDate")]
        public string logDate { get; set; }

        [JsonProperty("logId")]
        public string logId { get; set; }

        [JsonProperty("kioskName")]
        public string kioskName { get; set; }

        [JsonProperty("logDetail")]
        public string logDetail { get; set; }
    }
    public class ParserErrorLog
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("InsertionDate")]
        public string InsertionDate { get; set; }

        [JsonProperty("ProcessDate")]
        public string ProcessDate { get; set; }

        [JsonProperty("DeviceType")]
        public string DeviceType { get; set; }

        [JsonProperty("ErrorType")]
        public string ErrorType { get; set; }

        [JsonProperty("ErrorId")]
        public string ErrorId { get; set; }

        [JsonProperty("RepairState")]
        public string State { get; set; }

        [JsonProperty("RepairTime")]
        public string RepairTime { get; set; }

        [JsonProperty("RepairOperatorName")]
        public string RepairOperatorName { get; set; }

        [JsonProperty("RepairOperatorName2")]
        public string RepairOperatorName2 { get; set; }

        [JsonProperty("Explanation")]
        public string Explanation { get; set; }

        [JsonProperty("KioskId")]
        public string KioskId { get; set; }

        [JsonProperty("IsNotified")]
        public string IsNotified { get; set; }

        [JsonProperty("NotificationDate")]
        public string NotificationDate { get; set; }

        [JsonProperty("NotificationType")]
        public string NotificationType { get; set; }

        [JsonProperty("ApprovedUser")]
        public string ApprovedUser { get; set; }

        [JsonProperty("ApprovedDate")]
        public string ApprovedDate { get; set; }

        [JsonProperty("CalculatedTime")]
        public string CalculatedTime { get; set; }

        //[JsonProperty("ErrorLogId")]
        //public string ErrorLogId { get; set; }

    }
}
