﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Kiosk
{
    public class ParserKioskReport
    {
        [JsonProperty("KioskId")]
        public string KioskId { get; set; }

        [JsonProperty("KioskName")]
        public string KioskName { get; set; }

        [JsonProperty("TxnCount")]
        public string TxnCount { get; set; }

        [JsonProperty("GeneratedAskiAmount")]
        public string GeneratedAskiAmount { get; set; }

        [JsonProperty("UsedAskiAmount")]
        public string UsedAskiAmount { get; set; }

        [JsonProperty("GeneratedAskiCount")]
        public string GeneratedAskiCount { get; set; }

        [JsonProperty("UsedAskiCount")]
        public string UsedAskiCount { get; set; }

        [JsonProperty("ReceivedAmount")]
        public string ReceivedAmount { get; set; }

        [JsonProperty("KioskEmptyAmount")]
        public string KioskEmptyAmount { get; set; }

        [JsonProperty("EarnedMoney")]
        public string EarnedMoney { get; set; }

        [JsonProperty("ParaUstu")]
        public string ParaUstu { get; set; }

        [JsonProperty("TotalAmount")]
        public string TotalAmount { get; set; }

        [JsonProperty("SuccessTxnCount")]
        public string SuccessTxnCount { get; set; }

        [JsonProperty("InstutionAmount")]
        public string InstutionAmount { get; set; }

        [JsonProperty("CreditCardAmount")]
        public string CreditCardAmount { get; set; }

        [JsonProperty("CommisionAmount")]
        public string CommisionAmount { get; set; }
    }
}
