﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Kiosk
{
    public class ParserGetKioskErrorLogs
    {
        [JsonProperty("GetKioskErrorLogs")]
        public ParserErrorLog Errors { get; set; }
    }
}
