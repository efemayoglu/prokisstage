﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Kiosk
{
    public class ParserGetKioskCommand
    {
        [JsonProperty("GetKioskCommand")]
        public ParserKioskCommand KioskCommand { get; set; }
    }
}
