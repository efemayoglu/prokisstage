﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserGetKiosk
    {
        [JsonProperty("Kiosk")]
        public ParserKiosk Kiosk { get; set; }
    }
}
