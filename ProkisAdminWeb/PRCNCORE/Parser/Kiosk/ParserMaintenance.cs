﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Kiosk
{
    public class ParserMaintenance
    {
        [JsonProperty("maintenanceDate")]
        public string maintenanceDate { get; set; }

        [JsonProperty("maintenanceId")]
        public string maintenanceId { get; set; }

        [JsonProperty("kioskName")]
        public string kioskName { get; set; }

        [JsonProperty("maintenanceUser")]
        public string maintenanceUser { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }
    }
}
