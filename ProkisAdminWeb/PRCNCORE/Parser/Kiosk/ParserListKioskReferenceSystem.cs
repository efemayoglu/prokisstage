﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Kiosk
{
    public class ParserListKioskReferenceSystem
    {
        [JsonProperty("errorCode")]
        public int errorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string errorDescription { get; set; }

        [JsonProperty("recordCount")]
        public int recordCount { get; set; }

        [JsonProperty("pageCount")]
        public int pageCount { get; set; }

        [JsonProperty("KioskReferenceSystem")]
        public List<ParserKioskReferenceSystem> kioskReferenceSystem { get; set; }
    }
}
