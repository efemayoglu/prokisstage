﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Kiosk
{
    public class ParserKioskReferenceSystem
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("KioskName")]
        public string kioskName { get; set; }

        [JsonProperty("InstitutionName")]
        public string institutionName { get; set; }

        [JsonProperty("InsertionDate")]
        public string insertionDate { get; set; }

        [JsonProperty("Status")]
        public string status { get; set; }

        [JsonProperty("Explanation")]
        public string explanation { get; set; }

        [JsonProperty("CreatedUser")]
        public string createdUser { get; set; }

        [JsonProperty("ReferenceNo")]
        public string referenceNo { get; set; }

        
    }
}
