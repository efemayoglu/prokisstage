﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserListKioskCondition
    {
        [JsonProperty("errorCode")]
        public int errorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string errorDescription { get; set; }

        [JsonProperty("recordCount")]
        public int recordCount { get; set; }

        [JsonProperty("pageCount")]
        public int pageCount { get; set; }

        [JsonProperty("TotalCashCount")]
        public string TotalCashCount { get; set; }

        [JsonProperty("TotalCoinCount")]
        public string TotalCoinCount { get; set; }

        [JsonProperty("TotalAmount")]
        public string TotalAmount { get; set; }

        [JsonProperty("TotalDispenserAmount")]
        public string TotalDispenserAmount { get; set; }

        [JsonProperty("TotalDispenserCount")]
        public string TotalDispenserCount { get; set; }

        [JsonProperty("kioskCondition")]
        public List<ParserKioskCondition> kioskCondition { get; set; }
    }

    public class ParserListKioskLocalLogDetail
    {
        [JsonProperty("errorCode")]
        public int errorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string errorDescription { get; set; }

        [JsonProperty("KioskLocalLogDetail")]
        public List<ParserListKioskLocalLogDetailList> ParserListKioskLocalLogDetailList { get; set; }
    }

    public class ParserListKioskLocalLogDetailList
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("InsertionDate")]
        public string InsertionDate { get; set; }

        [JsonProperty("RecordInsertionDate")]
        public string RecordInsertionDate { get; set; }

        [JsonProperty("Detail")]
        public string Detail { get; set; }

        [JsonProperty("TotalAmount")]
        public string TotalAmount { get; set; }

        [JsonProperty("Status")]
        public string Status { get; set; }
    }
}
