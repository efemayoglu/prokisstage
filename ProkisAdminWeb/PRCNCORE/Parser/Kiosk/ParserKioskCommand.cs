﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserKioskCommand
    {
        [JsonProperty("commandId")]
        public int CommandId { get; set; }

        [JsonProperty("kioskName")]
        public string KioskName { get; set; }

        [JsonProperty("createdUserName")]
        public string CreatedUserName { get; set; }

        [JsonProperty("createdDate")]
        public string CreatedDate { get; set; }

        [JsonProperty("CommandType")]
        public string CommandType { get; set; }

        [JsonProperty("commandString")]
        public string CommandString { get; set; }

        [JsonProperty("executedDate")]
        public string ExecutedDate { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("CommandReason")]
        public string commandReason { get; set; }

        [JsonProperty("Explanation")]
        public string explanation { get; set; }

        [JsonProperty("CommandReasonId")]
        public string commandReasonId { get; set; }

        [JsonProperty("CommandTypeId")]
        public string CommandTypeId { get; set; }

        [JsonProperty("kioskId")]
        public string KioskId { get; set; }

        [JsonProperty("institution")]
        public string Institution { get; set; }

        [JsonProperty("institutionId")]
        public string InstitutionId { get; set; }

    }
}
