﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Kiosk
{
    public class ParserDevice
    {
        [JsonProperty("deviceId")]
        public Int64 deviceId { get; set; }

        [JsonProperty("deviceName")]
        public string deviceName { get; set; }

        [JsonProperty("kioskName")]
        public string kioskName { get; set; }

        [JsonProperty("port")]
        public string port { get; set; }
    }
}
