﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Kiosk
{
    public class ParserListKioskReport
    {
        [JsonProperty("errorCode")]
        public int errorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string errorDescription { get; set; }

        [JsonProperty("recordCount")]
        public int recordCount { get; set; }

        [JsonProperty("pageCount")]
        public int pageCount { get; set; }

        [JsonProperty("TotalTxnCount")]
        public string TotalTxnCount { get; set; }

        [JsonProperty("TotalGeneratedAskiAmount")]
        public string TotalGeneratedAskiAmount { get; set; }

        [JsonProperty("TotalUsedAskiAmount")]
        public string TotalUsedAskiAmount { get; set; }

        [JsonProperty("TotalGeneratedAskiCount")]
        public string TotalGeneratedAskiCount { get; set; }

        [JsonProperty("TotalUsedAskiCount")]
        public string TotalUsedAskiCount { get; set; }

        [JsonProperty("TotalReceivedAmount")]
        public string TotalReceivedAmount { get; set; }

        [JsonProperty("TotalKioskEmptyAmount")]
        public string TotalKioskEmptyAmount { get; set; }

        [JsonProperty("TotalEarnedAmount")]
        public string TotalEarnedAmount { get; set; }

        [JsonProperty("TotalParaUstu")]
        public string TotalParaUstu { get; set; }

        [JsonProperty("TotalSuccesTxnCount")]
        public string TotalSuccesTxnCount { get; set; }

        [JsonProperty("TotalInstitutionAmount")]
        public string TotalInstitutionAmount { get; set; }

        [JsonProperty("TotalTotalAmount")]
        public string TotalTotalAmount { get; set; }

        [JsonProperty("TotalCreditCardAmount")]
        public string TotalCreditCardAmount { get; set; }

        [JsonProperty("TotalCommisionAmount")]
        public string TotalCommisionAmount { get; set; }

        [JsonProperty("kioskReports")]
        public List<ParserKioskReport> KioskReports { get; set; }
    }
}
