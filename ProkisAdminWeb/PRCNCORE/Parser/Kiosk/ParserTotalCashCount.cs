﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Kiosk
{
    public class ParserTotalCashCount
    {
        [JsonProperty("cashTypeName")]
        public string cashTypeName { get; set; }

        [JsonProperty("cashCount")]
        public int cashCount { get; set; }

        [JsonProperty("cashTypeValue")]
        public string cashTypeValue { get; set; }
    }
}
