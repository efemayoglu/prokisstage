﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Kiosk
{
    public class ParserKioskMonitoring
    {
        [JsonProperty("KioskName")]
        public string kioskName { get; set; }

        [JsonProperty("LastSuccessTime")]
        public string lastSuccessTime { get; set; }

        [JsonProperty("LastAppAliveTime")]
        public string lastAppAliveTime { get; set; }

        [JsonProperty("SuccessTxnCount")]
        public string successTxnCount { get; set; }

        [JsonProperty("SuccessTxnAmount")]
        public string successTxnAmount { get; set; }

        [JsonProperty("Address")]
        public string address { get; set; }

        [JsonProperty("Latitude")]
        public string latitude { get; set; }

        [JsonProperty("Longitude")]
        public string longitude { get; set; }

        [JsonProperty("ResultCount")]
        public string ResultCount { get; set; }

        //[JsonProperty("totalAmount")]
        //public string totalAmount { get; set; }

    }
}
