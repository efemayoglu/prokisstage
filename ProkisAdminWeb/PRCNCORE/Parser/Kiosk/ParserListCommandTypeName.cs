﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserListCommandTypeName
    {
        [JsonProperty("CommandTypeNames")]
        public List<ParserCommandTypeName> CommandTypeNames { get; set; }
    }

    public class ParserListCommandReasonName
    {
        [JsonProperty("CommandReasonNames")]
        public List<ParserCommandReasonName> CommandReasonNames { get; set; }
    }
}
