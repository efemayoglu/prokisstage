﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserListKioskCashEmpty
    {
        [JsonProperty("errorCode")]
        public int errorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string errorDescription { get; set; }

        [JsonProperty("recordCount")]
        public int recordCount { get; set; }

        [JsonProperty("pageCount")]
        public int pageCount { get; set; }

        [JsonProperty("TotalCashCount")]
        public string TotalCashCount { get; set; }

        [JsonProperty("TotalCoinCount")]
        public string TotalCoinCount { get; set; }

        [JsonProperty("TotalAmount")]
        public string TotalAmount { get; set; }

        [JsonProperty("KioskCashEmpty")]
        public List<ParserKioskCashEmpty> kioskCashEmpty { get; set; }
    }
}
