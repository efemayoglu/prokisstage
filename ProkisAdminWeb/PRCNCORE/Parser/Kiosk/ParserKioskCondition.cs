﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserKioskCondition
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("kioskId")]
        public int kioskId { get; set; }

        [JsonProperty("kioskName")]
        public string kioskName { get; set; }

        [JsonProperty("kioskAddress")]
        public string kioskAddress { get; set; }

        [JsonProperty("cashCount")]
        public int cashCount { get; set; }

        [JsonProperty("coinCount")]
        public int coinCount { get; set; }

        [JsonProperty("ribbonRate")]
        public int ribbonRate { get; set; }

        //[JsonProperty("updatedDate")]
        //public string updatedDate { get; set; }

        [JsonProperty("amount")]
        public string amount { get; set; }

        [JsonProperty("capacity")]
        public string capacity { get; set; }

        [JsonProperty("cashBoxAcceptorTypeNamedataRow")]
        public string cashBoxAcceptorTypeNamedataRow { get; set; }

        [JsonProperty("Explanation")]
        public string explanation { get; set; }

        [JsonProperty("DispenserCount")]
        public string dispenserCount { get; set; }

        [JsonProperty("DispenserAmount")]
        public string dispenserAmount { get; set; }
    }
}
