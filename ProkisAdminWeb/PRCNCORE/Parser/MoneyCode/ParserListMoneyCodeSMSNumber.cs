﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.MoneyCode
{
    public class ParserListMoneyCodeSMSNumber
    {
        [JsonProperty("errorCode")]
        public int errorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string errorDescription { get; set; }

        [JsonProperty("recordCount")]
        public int recordCount { get; set; }

        [JsonProperty("pageCount")]
        public int pageCount { get; set; }

        [JsonProperty("MoneyCodeSMSNumber")]
        public List<ParserMoneyCodeSMSNumber> listMoneyCodeSMSNumber { get; set; }
    }
}
