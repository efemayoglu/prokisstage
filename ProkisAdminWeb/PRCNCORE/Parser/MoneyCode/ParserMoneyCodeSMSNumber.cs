﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.MoneyCode
{
    public class ParserMoneyCodeSMSNumber
    {
        [JsonProperty("operationId")]
        public string OperationId { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("insertionDate")]
        public string InsertionDate { get; set; }

       
    }
}
