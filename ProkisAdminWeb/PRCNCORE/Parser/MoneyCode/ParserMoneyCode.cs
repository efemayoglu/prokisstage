﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserMoneyCode
    {
        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("statusId")]
        public int statusId { get; set; }

        [JsonProperty("statusName")]
        public string statusName { get; set; }

        [JsonProperty("generatedKioskName")]
        public string generatedKioskName { get; set; }

        [JsonProperty("usedKioskName")]
        public string usedKioskName { get; set; }

        [JsonProperty("insertionDate")]
        public string insertionDate { get; set; }

        [JsonProperty("customerName")]
        public string customerName { get; set; }

        [JsonProperty("aboneNo")]
        public string aboneNo { get; set; }

        [JsonProperty("cardId")]
        public string cardId { get; set; }

        [JsonProperty("transactionId")]
        public string transactionId { get; set; }

        [JsonProperty("codeAmount")]
        public string codeAmount { get; set; }

        [JsonProperty("codeNumber")]
        public string codeNumber { get; set; }

        [JsonProperty("codeGenerationCase")]
        public string codeGenerationCase { get; set; }

        [JsonProperty("useDate")]
        public string useDate { get; set; }

        [JsonProperty("generatedUserId")]
        public string generatedUserId { get; set; }

        [JsonProperty("generatedUserName")]
        public string generatedUserName { get; set; }

        [JsonProperty("instutionName")]
        public string instutionName { get; set; }

        [JsonProperty("deletingReason")]
        public string deletingReason { get; set; }

        [JsonProperty("SendingSMS")]
        public string sendingSMS { get; set; }

        [JsonProperty("ApprovalSMS")]
        public string approvalSMS { get; set; }

        [JsonProperty("CodeDeleteUser")]
        public string codeDeleteUser { get; set; }

        [JsonProperty("CountSMSCell")]
        public string countSMSCell { get; set; }

        [JsonProperty("PaymentType")]
        public string paymentType { get; set; }

        [JsonProperty("ApprovedUser")]
        public string approvedUser { get; set; }

        [JsonProperty("ApprovedDate")]
        public string approvedDate { get; set; }
    }
}
