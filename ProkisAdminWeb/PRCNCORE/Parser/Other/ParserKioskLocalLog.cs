﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Other
{
    public class ParserKioskLocalLog
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("transactionId")]
        public string TransactionId { get; set; }

        [JsonProperty("kioskName")]
        public string KioskName { get; set; }

        [JsonProperty("kioskId")]
        public int KioskId { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("CustomerNo")]
        public string CustomerNo { get; set; }

        [JsonProperty("insertionDate")]
        public string InsertionDate { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("logStatus")]
        public string LogStatus { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("LogStatusText")]
        public string LogStatusText { get; set; }
        
    }
}
