﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Other
{
    public class ParserListConnectionType
    {
        [JsonProperty("ConnectionType")]
        public List<ParserConnectionType> ConnectionType { get; set; }
    }
}
