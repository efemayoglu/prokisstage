﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserAccountTransaction
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("TransactionDate")]
        public string TransactionDate { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("TransactionType")]
        public string TransactionType { get; set; }

        [JsonProperty("Amount")]
        public string Amount { get; set; }

        [JsonProperty("SenderNo")]
        public string SenderNo { get; set; }

        [JsonProperty("SenderName")]
        public string SenderName { get; set; }

        [JsonProperty("SenderBalance")]
        public string SenderBalance { get; set; }

        [JsonProperty("RecieverNo")]
        public string RecieverNo { get; set; }

        [JsonProperty("RecieverName")]
        public string RecieverName { get; set; }

        [JsonProperty("RecieverBalance")]
        public string RecieverBalance { get; set; }

        [JsonProperty("TransactionPrice")]
        public string TransactionPrice { get; set; }

        [JsonProperty("TransactionPlatform")]
        public string TransactionPlatform { get; set; }

        [JsonProperty("ActionSatusId")]
        public int ActionSatusId { get; set; }

        [JsonProperty("ActionSatus")]
        public string ActionSatus { get; set; }

        [JsonProperty("FraudTypeId")]
        public int FraudTypeId { get; set; }

        [JsonProperty("FraudType")]
        public string FraudType { get; set; }

        [JsonProperty("FraudApproveTime")]
        public string FraudApproveTime { get; set; }

        [JsonProperty("FraudApproveUser")]
        public string FraudApproveUser { get; set; }
    }
}
