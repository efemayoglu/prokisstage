﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserCustomerAccount
    {
        [JsonProperty("id")]
        public Int64 id { get; set; }

        [JsonProperty("customerNo")]
        public string customerNo { get; set; }

        [JsonProperty("customerName")]
        public string customerName { get; set; }

        [JsonProperty("amount")]
        public string amount { get; set; }

        [JsonProperty("date")]
        public string date { get; set; }

    }
}
