﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserWebCustomer
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("customerNo")]
        public string customerNo { get; set; }

        [JsonProperty("tckn")]
        public string tckn { get; set; }

        [JsonProperty("email")]
        public string email { get; set; }

        [JsonProperty("cellphone")]
        public string cellphone { get; set; }

        [JsonProperty("status")]
        public string status { get; set; }

        [JsonProperty("approveUser")]
        public string approveUser { get; set; }

        [JsonProperty("approveTime")]
        public string approveTime { get; set; }

        [JsonProperty("approveStatus")]
        public int approveStatus { get; set; }
    }
}
