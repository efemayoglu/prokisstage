﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Other
{
    public class ParserListCity
    {
        [JsonProperty("Cities")]
        public List<ParserCity> Cities { get; set; }
    }
}
