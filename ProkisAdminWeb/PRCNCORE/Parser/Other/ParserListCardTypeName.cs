﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserListCardTypeName
    {
        [JsonProperty("CardTypeNames")]
        public List<ParserKioskName> ParserListCardTypeNames { get; set; }
    }
}
