﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Other
{
    public class ParserListRegion
    {
        [JsonProperty("Regions")]
        public List<ParserCity> Regions { get; set; }
    }
}
