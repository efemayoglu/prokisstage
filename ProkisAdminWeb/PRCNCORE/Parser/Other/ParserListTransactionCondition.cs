﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserListTransactionCondition
    {
        [JsonProperty("errorCode")]
        public int errorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string errorDescription { get; set; }

        [JsonProperty("recordCount")]
        public int recordCount { get; set; }

        [JsonProperty("pageCount")]
        public int pageCount { get; set; }

        [JsonProperty("totalTotalAmount")]
        public string totalTotalAmount { get; set; }

        [JsonProperty("totalCashAmount")]
        public string totalCashAmount { get; set; }

        [JsonProperty("totalCodeAmount")]
        public string totalCodeAmount { get; set; }

        [JsonProperty("totalInsttutionAmount")]
        public string totalInsttutionAmount { get; set; }

        [JsonProperty("totalKioskUsageFee")]
        public string totalKioskUsageFee { get; set; }

        [JsonProperty("totalCalculatedChangeAmount")]
        public string totalCalculatedChangeAmount { get; set; }

        [JsonProperty("totalGivenChangeAmount")]
        public string totalGivenChangeAmount { get; set; }

        [JsonProperty("totalPaidFaturaCount")]
        public string totalPaidFaturaCount { get; set; }

        [JsonProperty("totalCreditCardAmount")]
        public string totalCreditCardAmount { get; set; }

        [JsonProperty("totalCommisionAmount")]
        public string totalCommisionAmount { get; set; }

        [JsonProperty("transactionConditions")]
        public List<ParserTransactionCondition> TransactionConditions { get; set; }
    }
}
