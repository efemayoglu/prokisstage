﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Other
{
    public class ParserRepairCard
    {
        [JsonProperty("transactionId")]
        public int TransactionId { get; set; }

        [JsonProperty("customerName")]
        public string CustomerName { get; set; }

        [JsonProperty("anaKredi")]
        public string AnaKredi { get; set; }

        [JsonProperty("yedekKredi")]
        public string YedekKredi { get; set; }

        [JsonProperty("transactionDate")]
        public string TransactionDate { get; set; }

        [JsonProperty("insertionDate")]
        public string InsertionDate { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("isDeleted")]
        public string IsDeleted { get; set; }

        [JsonProperty("repairDate")]
        public string RepairDate { get; set; }

        [JsonProperty("repairedTransactionId")]
        public string RepairedTransactionId { get; set; }

        [JsonProperty("repairedId")]
        public string RepairedId { get; set; }

        [JsonProperty("kioskName")]
        public string KioskName { get; set; }
    }
}
