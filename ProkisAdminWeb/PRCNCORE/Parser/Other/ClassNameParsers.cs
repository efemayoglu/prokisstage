﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Other
{
    public class ClassNameParsers
    {
        public List<ClassName> ClassName { get; set; }
    }
    public class ClassName
    {
        public ClassName(string fieldName, string fieldValue)
        {
            this.FieldName = fieldName;
            this.FieldValue = fieldValue;
        }

        public string FieldName { get; set; }
        public string FieldValue { get; set; }
    }
}
