﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserPurseTransaction
    {
        [JsonProperty("transactionId")]
        public int TransactionId { get; set; }

        [JsonProperty("kioskName")]
        public string KioskName { get; set; }

        [JsonProperty("customerNo")]
        public string customerNo { get; set; }

        [JsonProperty("customerName")]
        public string CustomerName { get; set; }

        [JsonProperty("transactionDate")]
        public string TransactionDate { get; set; }

        [JsonProperty("transactionKind")]
        public string TransactionKind { get; set; }
    }
}
