﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Other
{
    public class ParserMobilePaymentTransaction
    {
        [JsonProperty("transactionId")]
        public int TransactionId { get; set; }

        [JsonProperty("kioskName")]
        public string KioskName { get; set; }

        [JsonProperty("kioskId")]
        public int KioskId { get; set; }

        [JsonProperty("customerName")]
        public string CustomerName { get; set; }

        [JsonProperty("aboneNo")]
        public string AboneNo { get; set; }

        [JsonProperty("transactionDate")]
        public string TransactionDate { get; set; }

        //MobileNo
        [JsonProperty("mobileNo")]
        public string MobileNo { get; set; }

        //Status
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("instutionName")]
        public string InstutionName { get; set; }

        [JsonProperty("muhasebeStatus")]
        public string MuhasebeStatus { get; set; }

        [JsonProperty("transactionAmount")]
        public string TransactionAmount { get; set; }
    }
}
