﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserTransactionDetail
    {
        [JsonProperty("transactionId")]
        public int TransactionId { get; set; }

        [JsonProperty("processDescription")]
        public string ProcessDescription { get; set; }

        [JsonProperty("kioskName")]
        public string kioskName { get; set; }

        [JsonProperty("transactionDate")]
        public string TransactionDate { get; set; }

        [JsonProperty("processStatus")]
        public string ProcessStatus { get; set; }
    }
}
