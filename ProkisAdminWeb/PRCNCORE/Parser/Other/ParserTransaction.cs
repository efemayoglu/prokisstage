﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserTransaction
    {
        //[JsonProperty("Name")]
        //public string Name { get; set; }
     
        [JsonProperty("transactionId")]
        public int TransactionId { get; set; }

        [JsonProperty("kioskName")]
        public string KioskName { get; set; }

        [JsonProperty("kioskId")]
        public int KioskId { get; set; }

        [JsonProperty("cardId")]
        public string CardId { get; set; }

        [JsonProperty("customerName")]
        public string CustomerName { get; set; }

        [JsonProperty("aboneNo")]
        public string AboneNo { get; set; }

        [JsonProperty("transactionDate")]
        public string TransactionDate { get; set; }

        [JsonProperty("processStatus")]
        public string ProcessStatus { get; set; }

        [JsonProperty("instutionName")]
        public string InstutionName { get; set; }

        [JsonProperty("operationTypeName")]
        public string OperationTypeName { get; set; }
    }

    public class ParserAutoMutabakat
    {
        [JsonProperty("InstitutionNumber")]
        public string institutionNumber { get; set; }

        [JsonProperty("InstitutionAmount")]
        public string institutionAmount { get; set; }

        [JsonProperty("InstitutionCancelNumber")]
        public string institutionCancelNumber { get; set; }
    
        [JsonProperty("institutionCancelAmount")]
        public string institutionCancelAmount { get; set; }

        [JsonProperty("bosNumber")]
        public string bosNumber { get; set; }

        [JsonProperty("bosAmount")]
        public string bosAmount { get; set; }

        [JsonProperty("bosCancelNumber")]
        public string bosCancelNumber { get; set; }

        [JsonProperty("bosCancelAmount")]
        public string bosCancelAmount { get; set; }

        [JsonProperty("account")]
        public string account { get; set; }

        [JsonProperty("insertionDate")]
        public string insertionDate { get; set; }

        [JsonProperty("isSucess")]
        public string isSucess { get; set; }

        [JsonProperty("message")]
        public string message { get; set; }

        [JsonProperty("userId")]
        public string userId { get; set; }

        [JsonProperty("manualApproveDate")]
        public string manualApproveDate { get; set; }

        [JsonProperty("insMif")]
        public string insMif { get; set; }

        [JsonProperty("insMakbuz")]
        public string insMakbuz { get; set; }

        [JsonProperty("insDate")]
        public string insDate { get; set; }

        [JsonProperty("explanation")]
        public string explanation { get; set; }

    }
}
