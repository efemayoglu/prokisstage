﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserDailyReport
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("insertionDate")]
        public string InsertionDate { get; set; }

        [JsonProperty("totalBalance")]
        public string TotalBalance { get; set; }

        [JsonProperty("activeBalance")]
        public string ActiveBalance { get; set; }

        [JsonProperty("pasifBalance")]
        public string PasifBalance { get; set; }

    }
}
