﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserCustomer
    {
        [JsonProperty("customerId")]
        public int CustomerId { get; set; }

        [JsonProperty("customerName")]
        public string CustomerName { get; set; }

        [JsonProperty("aboneNo")]
        public string AboneNo { get; set; }

        [JsonProperty("insertionDate")]
        public string InsertionDate { get; set; }

        [JsonProperty("lastTransactionDate")]
        public string LastTransactionDate { get; set; }

        [JsonProperty("transactionCount")]
        public string TransactionCount { get; set; }

        [JsonProperty("instutionName")]
        public string InstutionName { get; set; }

        [JsonProperty("instutionId")]
        public int InstutionId { get; set; }
    }
}
