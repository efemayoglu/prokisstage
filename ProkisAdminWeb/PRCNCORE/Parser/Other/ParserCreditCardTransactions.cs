﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Other
{
    public class ParserCreditCardTransactions
    {
        [JsonProperty("transactionId")]
        public int TransactionId { get; set; }

        [JsonProperty("kioskName")]
        public string KioskName { get; set; }

        [JsonProperty("kioskId")]
        public int KioskId { get; set; }

        [JsonProperty("customerName")]
        public string CustomerName { get; set; }

        [JsonProperty("aboneNo")]
        public string AboneNo { get; set; }

        [JsonProperty("transactionDate")]
        public string TransactionDate { get; set; }
 
        [JsonProperty("creditCardNo")]
        public string CreditCardNo { get; set; }

        [JsonProperty("creditCardOwner")]
        public string CreditCardOwner { get; set; }

        [JsonProperty("instutionName")]
        public string InstutionName { get; set; }

        [JsonProperty("rrn")]
        public string RRN { get; set; }

        [JsonProperty("transactionAmountCell")]
        public string TransactionAmountCell { get; set; }

        [JsonProperty("muhasebeStatus")]
        public string MuhasebeStatus { get; set; }

        [JsonProperty("approvedCode")]
        public string ApprovedCode { get; set; }

        [JsonProperty("batch")]
        public string Batch { get; set; }
    }
}
