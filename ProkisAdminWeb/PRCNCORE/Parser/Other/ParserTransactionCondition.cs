﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserTransactionCondition
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("transactionId")]
        public string TransactionId { get; set; }

        [JsonProperty("kioskName")]
        public string KioskName { get; set; }

        [JsonProperty("transactionDate")]
        public string TransactionDate { get; set; }

        [JsonProperty("instutionName")]
        public string InstutionName { get; set; }

        [JsonProperty("totalAmount")]
        public string TotalAmount { get; set; }

        [JsonProperty("cashAmount")]
        public string CashAmount { get; set; }

        [JsonProperty("codeAmount")]
        public string CodeAmount { get; set; }

        [JsonProperty("creditCardAmount")]
        public string CreditCardAmount { get; set; }

        [JsonProperty("instutionAmount")]
        public string InstutionAmount { get; set; }

        [JsonProperty("commisionAmount")]
        public string CommisionAmount { get; set; }

        [JsonProperty("calculatedChange")]
        public string CalculatedChange { get; set; }

        [JsonProperty("givenChange")]
        public string GivenChange { get; set; }

        [JsonProperty("usageFee")]
        public string UsageFee { get; set; }

        [JsonProperty("paidFaturaCount")]
        public string PaidFaturaCount { get; set; }

        [JsonProperty("transationStatus")]
        public string TransationStatus { get; set; }

        [JsonProperty("customerName")]
        public string CustomerName { get; set; }

        [JsonProperty("payTypeName")]
        public string PayTypeName { get; set; }

        [JsonProperty("reverseRecord")]
        public string ReverseRecord { get; set; }



    }
}
