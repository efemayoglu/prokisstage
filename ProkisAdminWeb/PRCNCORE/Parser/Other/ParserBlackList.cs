﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserBlackList
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Tckn")]
        public string Tckn { get; set; }

        [JsonProperty("BirthYear")]
        public string BirthYear { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("CreatedUser")]
        public string CreatedUser { get; set; }

        [JsonProperty("InsertionDate")]
        public string InsertionDate { get; set; }

        [JsonProperty("Status")]
        public int Status { get; set; }

    }
}
