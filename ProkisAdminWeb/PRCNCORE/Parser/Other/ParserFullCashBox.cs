﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserFullCashBox
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("kioskId")]
        public int kioskId { get; set; }

        [JsonProperty("kioskName")]
        public string kioskName { get; set; }

        [JsonProperty("expertUserId")]
        public string expertUserId { get; set; }

        [JsonProperty("expertUserName")]
        public string expertUserName { get; set; }

        [JsonProperty("updatedDate")]
        public string updatedDate { get; set; }

        [JsonProperty("amount")]
        public string amount { get; set; }
    }
}
