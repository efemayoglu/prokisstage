﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Other
{
    public class ParserListPaymentType
    {
        [JsonProperty("PaymentType")]
        public List<ParserPaymentType> PaymentType { get; set; }
    }
}
