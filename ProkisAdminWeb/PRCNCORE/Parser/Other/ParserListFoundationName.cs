﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserListInstutionName
    {
        [JsonProperty("InstutionNames")]
        public List<ParserInstutionName> InstutionNames { get; set; }
    }

    public class ParserListBankName
    {
        [JsonProperty("BankNames")]
        public List<ParserBankName> BankNames { get; set; }
    }

    public class ParserListCompleteStatus
    {
        [JsonProperty("CompleteStatusName")]
        public List<ParserCompleteStatus> completeStatusName { get; set; }
    }
}
