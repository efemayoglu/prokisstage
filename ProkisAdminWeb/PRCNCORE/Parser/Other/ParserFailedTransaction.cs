﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Other
{
    public class ParserFailedTransaction
    {
        [JsonProperty("transactionId")]
        public int TransactionId { get; set; }

        [JsonProperty("kioskName")]
        public string KioskName { get; set; }

        [JsonProperty("kioskId")]
        public int KioskId { get; set; }

        [JsonProperty("cardId")]
        public string CardId { get; set; }

        [JsonProperty("customerName")]
        public string CustomerName { get; set; }

        [JsonProperty("aboneNo")]
        public string AboneNo { get; set; }

        [JsonProperty("transactionDate")]
        public string TransactionDate { get; set; }

        [JsonProperty("processStatus")]
        public string ProcessStatus { get; set; }

        [JsonProperty("instutionName")]
        public string InstutionName { get; set; }

        [JsonProperty("operationTypeName")]
        public string OperationTypeName { get; set; }

        [JsonProperty("logDate")]
        public string LogDate { get; set; }

        [JsonProperty("cellPhone")]
        public string CellPhone { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("explanation")]
        public string Explanation { get; set; }

        [JsonProperty("callDate")]
        public string CallDate { get; set; }

        [JsonProperty("callCount")]
        public string CallCount { get; set; }

    }
}
