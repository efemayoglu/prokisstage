﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserMutabakatCashCount
    {
        [JsonProperty("cashTypeId")]
        public int cashTypeId { get; set; }

        [JsonProperty("cashCount")]
        public int cashCount { get; set; }

        [JsonProperty("cashTypeName")]
        public string cashTypeName { get; set; }

        [JsonProperty("cashTypeValue")]
        public string cashTypeValue { get; set; }
    }
}
