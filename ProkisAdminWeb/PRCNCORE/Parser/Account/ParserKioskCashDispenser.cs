﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Account
{
    public class ParserKioskCashDispenser
    {
        [JsonProperty("Id")]
        public string Id { get; set; }


        [JsonProperty("KioskName")]
        public string kioskId { get; set; }

        [JsonProperty("KioskId")]
        public string kioskID { get; set; }

        [JsonProperty("InstitutionName")]
        public string institutionId { get; set; }

        [JsonProperty("InsertionDate")]
        public string insertionDate { get; set; }


        [JsonProperty("FirstDenomValue")]
        public string firstDenomValue { get; set; }

        [JsonProperty("FirstDenomNumber")]
        public string firstDenomNumber { get; set; }

        [JsonProperty("SecondDenomValue")]
        public string secondDenomValue { get; set; }

        [JsonProperty("SecondDenomNumber")]
        public string secondDenomNumber { get; set; }

        [JsonProperty("ThirdDenomValue")]
        public string thirdDenomValue { get; set; }

        [JsonProperty("ThirdDenomNumber")]
        public string thirdDenomNumber { get; set; }

        [JsonProperty("FourthDenomValue")]
        public string fourthDenomValue { get; set; }

        [JsonProperty("FourthDenomNumber")]
        public string fourthDenomNumber { get; set; }

        [JsonProperty("FifthDenomValue")]
        public string fifthDenomValue { get; set; }

        [JsonProperty("FifthDenomNumber")]
        public string fifthDenomNumber { get; set; }

        [JsonProperty("SixthDenomValue")]
        public string sixthDenomValue { get; set; }

        [JsonProperty("SixthDenomNumber")]
        public string sixthDenomNumber { get; set; }

        [JsonProperty("SeventhDenomValue")]
        public string seventhDenomValue { get; set; }

        [JsonProperty("SeventhDenomNumber")]
        public string seventhDenomNumber { get; set; }

        [JsonProperty("EighthDenomValue")]
        public string eighthDenomValue { get; set; }

        [JsonProperty("EighthDenomNumber")]
        public string eighthDenomNumber { get; set; }

        [JsonProperty("NinthDenomValue")]
        public string ninthDenomValue { get; set; }

        [JsonProperty("NinthDenomNumber")]
        public string ninthDenomNumber { get; set; }

        [JsonProperty("TenthDenomValue")]
        public string tenthDenomValue { get; set; }

        [JsonProperty("TenthDenomNumber")]
        public string tenthDenomNumber { get; set; }

        [JsonProperty("EleventhDenomValue")]
        public string eleventhDenomValue { get; set; }

        [JsonProperty("EleventhDenomNumber")]
        public string eleventhDenomNumber { get; set; }

        [JsonProperty("Casette1Reject")]
        public string Casette1Reject { get; set; }

        [JsonProperty("Casette2Reject")]
        public string Casette2Reject { get; set; }

        [JsonProperty("Casette3Reject")]
        public string Casette3Reject { get; set; }

        [JsonProperty("Casette4Reject")]
        public string Casette4Reject { get; set; }
    }
}
