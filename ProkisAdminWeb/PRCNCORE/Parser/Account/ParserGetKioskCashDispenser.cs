﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Account
{
    public class ParserGetKioskCashDispenser
    {
        [JsonProperty("GetKioskCashDispenser")]
        public ParserKioskCashDispenser KioskCashDispenser { get; set; }
    }
}
