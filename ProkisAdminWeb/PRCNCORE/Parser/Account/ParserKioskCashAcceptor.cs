﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace PRCNCORE.Parser.Account
{
    public class ParserKioskCashAcceptor
    {
        [JsonProperty("KioskName")]
        public string kioskId { get; set; }

        [JsonProperty("InstitutionName")]
        public string institutionId { get; set; }

        [JsonProperty("InsertionDate")]
        public string insertionDate { get; set; }

        [JsonProperty("FirstDenomValue")]
        public string firstDenomValue { get; set; }

        [JsonProperty("FirstDenomNumber")]
        public string firstDenomNumber { get; set; }

        [JsonProperty("SecondDenomValue")]
        public string secondDenomValue { get; set; }

        [JsonProperty("SecondDenomNumber")]
        public string secondDenomNumber { get; set; }

        [JsonProperty("ThirdDenomValue")]
        public string thirdDenomValue { get; set; }

        [JsonProperty("ThirdDenomNumber")]
        public string thirdDenomNumber { get; set; }

        [JsonProperty("FourthDenomValue")]
        public string fourthDenomValue { get; set; }

        [JsonProperty("FourthDenomNumber")]
        public string fourthDenomNumber { get; set; }

        [JsonProperty("FifthDenomValue")]
        public string fifthDenomValue { get; set; }

        [JsonProperty("FifthDenomNumber")]
        public string fifthDenomNumber { get; set; }

        [JsonProperty("SixthDenomValue")]
        public string sixthDenomValue { get; set; }

        [JsonProperty("SixthDenomNumber")]
        public string sixthDenomNumber { get; set; }

        [JsonProperty("SeventhDenomValue")]
        public string seventhDenomValue { get; set; }

        [JsonProperty("SeventhDenomNumber")]
        public string seventhDenomNumber { get; set; }

        [JsonProperty("EighthDenomValue")]
        public string eighthDenomValue { get; set; }

        [JsonProperty("EighthDenomNumber")]
        public string eighthDenomNumber { get; set; }

        [JsonProperty("NinthDenomValue")]
        public string ninthDenomValue { get; set; }

        [JsonProperty("NinthDenomNumber")]
        public string ninthDenomNumber { get; set; }

        [JsonProperty("TenthDenomValue")]
        public string tenthDenomValue { get; set; }

        [JsonProperty("TenthDenomNumber")]
        public string tenthDenomNumber { get; set; }

        [JsonProperty("EleventhDenomValue")]
        public string eleventhDenomValue { get; set; }

        [JsonProperty("EleventhDenomNumber")]
        public string eleventhDenomNumber { get; set; }

    }
}
