﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Account
{
    public class ParserCommisionSetting
    {
        [JsonProperty("insName")]
        public string InsName { get; set; }

        [JsonProperty("instutionId")]
        public string InstutionId { get; set; }

        [JsonProperty("creditCommission")]
        public string CreditCommission { get; set; }

        [JsonProperty("mobileCommission")]
        public string MobileCommission { get; set; }

        [JsonProperty("cashCommission")]
        public string CashCommission { get; set; }

        [JsonProperty("yellowAlarm")]
        public string YellowAlarm { get; set; }

        [JsonProperty("redAlarm")]
        public string RedAlarm { get; set; }
    }
}
