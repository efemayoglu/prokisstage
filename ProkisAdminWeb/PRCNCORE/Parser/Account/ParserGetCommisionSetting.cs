﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Account
{
    public class ParserGetCommisionSetting
    {
        [JsonProperty("errorCode")]
        public int errorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string errorDescription { get; set; }
  
        [JsonProperty("adminUserId")]
        public int adminUserId { get; set; }

        [JsonProperty("kioskCommisionSetting")]
        public List<ParserCommisionSetting> CommisionSetting { get; set; }

        //[JsonProperty("creditAski")]
        //public int creditAski { get; set; }

        //[JsonProperty("mobileAski")]
        //public int mobileAski { get; set; }

        //[JsonProperty("cashAski")]
        //public int cashAski { get; set; }

        //[JsonProperty("yellowAski")]
        //public int yellowAski { get; set; }

        //[JsonProperty("redAski")]
        //public int redAski { get; set; }

        //[JsonProperty("creditBaskent")]
        //public int creditBaskent { get; set; }

        //[JsonProperty("mobileBaskent")]
        //public int mobileBaskent { get; set; }

        //[JsonProperty("cashBaskent")]
        //public int cashBaskent { get; set; }

        //[JsonProperty("yellowBaskent")]
        //public int yellowBaskent { get; set; }

        //[JsonProperty("redBaskent")]
        //public int redBaskent { get; set; }

        //[JsonProperty("creditAksaray")]
        //public int creditAksaray { get; set; }

        //[JsonProperty("mobileAksaray")]
        //public int mobileAksaray { get; set; }

        //[JsonProperty("cashAksaray")]
        //public int cashAksaray { get; set; }

        //[JsonProperty("yellowAksaray")]
        //public int yellowAksaray { get; set; }

        //[JsonProperty("redAksaray")]
        //public int redAksaray { get; set; }
    }
}
