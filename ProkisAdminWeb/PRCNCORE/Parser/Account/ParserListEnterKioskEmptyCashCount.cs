﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Account
{
    public class ParserListEnterKioskEmptyCashCount
    {
        [JsonProperty("enterkioskemptycashcount")]
        public List<ParserEnterKioskEmptyCashCount> Kiosks { get; set; }

        [JsonProperty("errorCode")]
        public int errorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string errorDescription { get; set; }

        [JsonProperty("recordCount")]
        public int recordCount { get; set; }

        [JsonProperty("pageCount")]
        public int pageCount { get; set; }
    }
}
