﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Account
{
    public class ParserListSetting
    {
        [JsonProperty("errorCode")]
        public int errorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string errorDescription { get; set; }

        [JsonProperty("recordCount")]
        public int recordCount { get; set; }

        [JsonProperty("pageCount")]
        public int pageCount { get; set; }

        [JsonProperty("serverTime")]
        public DateTime serverTime { get; set; }

        [JsonProperty("adminUserId")]
        public int adminUserId { get; set; }

        [JsonProperty("BillFieldNotificationLimit")]
        public string billFieldNotificationLimit { get; set; }

        [JsonProperty("IsNotificationLimitReceiptActive")]
        public int isNotificationLimitReceiptActive { get; set; }

        [JsonProperty("CashBoxNotificationLimit")]
        public string cashBoxNotificationLimit { get; set; }

        [JsonProperty("IsNotificationCashBoxLimitActive")]
        public int isNotificationCashBoxLimitActive { get; set; }

        [JsonProperty("IsYellowAlertActive")]
        public int isYellowAlertActive { get; set; }

        [JsonProperty("IsRedAlertActive")]
        public int isRedAlertActive { get; set; }

        [JsonProperty("MoneyCodeGenerationLimit")]
        public string moneyCodeGenerationLimit { get; set; }

        [JsonProperty("MoneyCodeGenerationCountLimit")]
        public string moneyCodeGenerationCountLimit { get; set; }

        [JsonProperty("OneyCodeGenerationCountLimitPeriod")]
        public string oneyCodeGenerationCountLimitPeriod { get; set; }

        [JsonProperty("BGazUpperLoadingLimit")]
        public string bGazUpperLoadingLimit { get; set; }

    }
}
