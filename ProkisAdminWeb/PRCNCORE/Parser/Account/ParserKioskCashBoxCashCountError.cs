﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Account
{
    public class ParserKioskCashBoxCashCountError
    {
        [JsonProperty("TransactionId")]
        public string transactionId { get; set; }

        [JsonProperty("KioskName")]
        public string kioskName { get; set; }

        [JsonProperty("InsertionDate")]
        public string insertionDate { get; set; }



        [JsonProperty("Reject1")]
        public string reject1 { get; set; }

        [JsonProperty("Reject2")]
        public string reject2 { get; set; }

        [JsonProperty("Reject3")]
        public string reject3 { get; set; }

        [JsonProperty("Reject4")]
        public string reject4 { get; set; }

        [JsonProperty("Value1")]
        public string value1 { get; set; }

        [JsonProperty("Value2")]
        public string value2 { get; set; }

        [JsonProperty("Value3")]
        public string value3 { get; set; }

        [JsonProperty("Value4")]
        public string value4 { get; set; }

        [JsonProperty("Exit1")]
        public string exit1 { get; set; }

        [JsonProperty("Exit2")]
        public string exit2 { get; set; }

        [JsonProperty("Exit3")]
        public string exit3 { get; set; }

        [JsonProperty("Exit4")]
        public string exit4 { get; set; }

        [JsonProperty("Error")]
        public string error { get; set; }

        [JsonProperty("ErrorStatus")]
        public string errorStatus { get; set; }

        [JsonProperty("Exception")]
        public string exception { get; set; }

        [JsonProperty("TotalLog")]
        public string totalLog { get; set; }

        [JsonProperty("TotalDispensed")]
        public string totalDispensed { get; set; }

        [JsonProperty("TotalDifference")]
        public string totalDifference { get; set; }
    }
}
