﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserMutabakat
    {
        [JsonProperty("ID")]
        public int id { get; set; }

        [JsonProperty("MutabakatType")]
        public string mutabakatType { get; set; }

        [JsonProperty("MutabakatDate")]
        public string mutabakatDate { get; set; }

        [JsonProperty("MutabakatUser")]
        public string mutabakatUser { get; set; }

        [JsonProperty("InputAmount")]
        public string inputAmount { get; set; }

        [JsonProperty("OutputAmount")]
        public string outputAmount { get; set; }

        [JsonProperty("Institution")]
        public string institution { get; set; }

        [JsonProperty("CashInflow")]
        public string cashInflow { get; set; }

        [JsonProperty("InstitutionAmount")]
        public string institutionAmount { get; set; }

        [JsonProperty("ActiveCode")]
        public string activeCode { get; set; }

        [JsonProperty("UsedCode")]
        public string usedCode { get; set; }

        [JsonProperty("AmountDifference")]
        public string amountDifference { get; set; }

        [JsonProperty("CodeInFlow")]
        public string codeInFlow { get; set; }

        [JsonProperty("Explanation")]
        public string explanation { get; set; }

        [JsonProperty("AccountOwner")]
        public string accountOwner { get; set; }

        [JsonProperty("KioskName")]
        public string kioskName { get; set; }

        [JsonProperty("ResultCount")]
        public string resultCount { get; set; }

        [JsonProperty("KioskEmptyId")]
        public string emptyKioskId { get; set; }

        [JsonProperty("InsertionDate")]
        public string insertionDate { get; set; }

        [JsonProperty("OperatorName")]
        public string operatorName { get; set; }

        [JsonProperty("AccountDate")]
        public string accountDate { get; set; }

        [JsonProperty("StatusId")]
        public string StatusId { get; set; }

        [JsonProperty("Status")]
        public string status { get; set; }

        [JsonProperty("ReferenceNumber")]
        public string ReferenceNumber { get; set; }

    }

    public class ParserMutabakatReports
    {

        [JsonProperty("ResultCount")]
        public int ResultCount { get; set; }
        
        [JsonProperty("InsertionDate")]
        public string insertionDate { get; set; }

        [JsonProperty("KioskName")]
        public string kioskName { get; set; }

        [JsonProperty("UpdatedDate")]
        public string UpdatedDate { get; set; }

        [JsonProperty("LastCashOutDate")]
        public string LastCashOutDate { get; set; }

        [JsonProperty("InputAmount")]
        public string inputAmount { get; set; }

        [JsonProperty("OutputAmount")]
        public string outputAmount { get; set; }

        [JsonProperty("AmountDifference")]
        public string amountDifference { get; set; }

        [JsonProperty("Institution")]
        public string Institution { get; set; }

        [JsonProperty("CashAmount")]
        public string CashAmount { get; set; }

        [JsonProperty("CodeAmount")]
        public string CodeAmount { get; set; }

        [JsonProperty("InstitutionAmount")]
        public string institutionAmount { get; set; }

        [JsonProperty("UsageAmount")]
        public string UsageAmount { get; set; }

        [JsonProperty("CalculatedChangeAmount")]
        public string CalculatedChangeAmount { get; set; }

        [JsonProperty("GivenChangeAmount")]
        public string GivenChangeAmount { get; set; }

        [JsonProperty("ActiveCode")]
        public string activeCode { get; set; }

        [JsonProperty("UsedCode")]
        public string usedCode { get; set; }

        [JsonProperty("kioskGeneratedActiveCode")]
        public string KioskGeneratedActiveCode { get; set; }

        [JsonProperty("KioskGeneratedKioskUsedCode")]
        public string KioskGeneratedKioskUsedCode { get; set; }

        [JsonProperty("ManuelGeneratedActiveCode")]
        public string ManuelGeneratedActiveCode { get; set; }

        [JsonProperty("ManuelGeneratedUsedCode")]
        public string ManuelGeneratedUsedCode { get; set; }

        [JsonProperty("AnotherKioskGeneratedUsedCode")]
        public string AnotherKioskGeneratedUsedCode { get; set; }



    }
}
