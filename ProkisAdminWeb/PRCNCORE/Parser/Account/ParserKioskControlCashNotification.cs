﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Account
{
    public class ParserKioskControlCashNotification
    {
        [JsonProperty("KioskName")]
        public string kioskId { get; set; }

        [JsonProperty("InstitutionName")]
        public string institutionId { get; set; }

        [JsonProperty("InsertionDate")]
        public string insertionDate { get; set; }

        [JsonProperty("FirstDenomNumber")]
        public string firstDenomNumber { get; set; }

        [JsonProperty("SecondDenomNumber")]
        public string secondDenomNumber { get; set; }

        [JsonProperty("ThirdDenomNumber")]
        public string thirdDenomNumber { get; set; }

        [JsonProperty("FourthDenomNumber")]
        public string fourthDenomNumber { get; set; }

        [JsonProperty("FifthDenomNumber")]
        public string fifthDenomNumber { get; set; }

        [JsonProperty("SixthDenomNumber")]
        public string sixthDenomNumber { get; set; }

        [JsonProperty("SeventhDenomNumber")]
        public string seventhDenomNumber { get; set; }

        [JsonProperty("EighthDenomNumber")]
        public string eighthDenomNumber { get; set; }

        [JsonProperty("NinthDenomNumber")]
        public string ninthDenomNumber { get; set; }

        [JsonProperty("TenthDenomNumber")]
        public string tenthDenomNumber { get; set; }

        [JsonProperty("EleventhDenomNumber")]
        public string eleventhDenomNumber { get; set; }

        [JsonProperty("CreatedUser")]
        public string createdrUser { get; set; }

        [JsonProperty("UserCashOut")]
        public string  userCashOut { get; set; }

        [JsonProperty("CashOutDate")]
        public string cashOutDate { get; set; }

        [JsonProperty("Sum")]
        public string sum { get; set; }

        [JsonProperty("Status")]
        public string status { get; set; }

        [JsonProperty("StatusId")]
        public int statusId { get; set; }

        [JsonProperty("ControlCashId")]
        public string controlCashId { get; set; }

        [JsonProperty("KioskId")]
        public string KioskId { get; set; }


    }
}
