﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Account
{
    public class ParserGetControlCashNotification
    {
        [JsonProperty("GetControlCashNotification")]
        public ParserKioskControlCashNotification ControlCashNotifications { get; set; }
        
    }
}
