﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Account
{
    public class ParserKioskDispenserMonitoring
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("createruser")]
        public string Createruser { get; set; }

        [JsonProperty("insertionDate")]
        public string InsertionDate { get; set; }

        [JsonProperty("insertionAmount")]
        public string InsertionAmount { get; set; }

        [JsonProperty("kioskName")]
        public string KioskName { get; set; }


        [JsonProperty("FirstDenomValue")]
        public string firstDenomValue { get; set; }

        [JsonProperty("FirstDenomNumber")]
        public string firstDenomNumber { get; set; }

        [JsonProperty("SecondDenomValue")]
        public string secondDenomValue { get; set; }

        [JsonProperty("SecondDenomNumber")]
        public string secondDenomNumber { get; set; }

        [JsonProperty("ThirdDenomValue")]
        public string thirdDenomValue { get; set; }

        [JsonProperty("ThirdDenomNumber")]
        public string thirdDenomNumber { get; set; }

        [JsonProperty("FourthDenomValue")]
        public string fourthDenomValue { get; set; }

        [JsonProperty("FourthDenomNumber")]
        public string fourthDenomNumber { get; set; }

        [JsonProperty("FifthDenomValue")]
        public string fifthDenomValue { get; set; }

        [JsonProperty("FifthDenomNumber")]
        public string fifthDenomNumber { get; set; }

        [JsonProperty("SixthDenomValue")]
        public string sixthDenomValue { get; set; }

        [JsonProperty("SixthDenomNumber")]
        public string sixthDenomNumber { get; set; }

        [JsonProperty("SeventhDenomValue")]
        public string seventhDenomValue { get; set; }

        [JsonProperty("SeventhDenomNumber")]
        public string seventhDenomNumber { get; set; }

        [JsonProperty("EighthDenomValue")]
        public string eighthDenomValue { get; set; }

        [JsonProperty("EighthDenomNumber")]
        public string eighthDenomNumber { get; set; }

        [JsonProperty("NinthDenomValue")]
        public string ninthDenomValue { get; set; }

        [JsonProperty("NinthDenomNumber")]
        public string ninthDenomNumber { get; set; }

        [JsonProperty("TenthDenomValue")]
        public string tenthDenomValue { get; set; }

        [JsonProperty("TenthDenomNumber")]
        public string tenthDenomNumber { get; set; }

        [JsonProperty("EleventhDenomValue")]
        public string eleventhDenomValue { get; set; }

        [JsonProperty("EleventhDenomNumber")]
        public string eleventhDenomNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("FirstDenomValueOld")]
        public string firstDenomValueOld { get; set; }

        [JsonProperty("FirstDenomNumberOld")]
        public string firstDenomNumberOld { get; set; }

        [JsonProperty("SecondDenomValueOld")]
        public string secondDenomValueOld { get; set; }

        [JsonProperty("SecondDenomNumberOld")]
        public string secondDenomNumberOld { get; set; }

        [JsonProperty("ThirdDenomValueOld")]
        public string thirdDenomValueOld { get; set; }

        [JsonProperty("ThirdDenomNumberOld")]
        public string thirdDenomNumberOld { get; set; }

        [JsonProperty("FourthDenomValueOld")]
        public string fourthDenomValueOld { get; set; }

        [JsonProperty("FourthDenomNumberOld")]
        public string fourthDenomNumberOld { get; set; }

        [JsonProperty("FifthDenomValueOld")]
        public string fifthDenomValueOld { get; set; }

        [JsonProperty("FifthDenomNumberOld")]
        public string fifthDenomNumberOld { get; set; }

        [JsonProperty("SixthDenomValueOld")]
        public string sixthDenomValueOld { get; set; }

        [JsonProperty("SixthDenomNumberOld")]
        public string sixthDenomNumberOld { get; set; }

        [JsonProperty("SeventhDenomValueOld")]
        public string seventhDenomValueOld { get; set; }

        [JsonProperty("SeventhDenomNumberOld")]
        public string seventhDenomNumberOld { get; set; }

        [JsonProperty("EighthDenomValueOld")]
        public string eighthDenomValueOld { get; set; }

        [JsonProperty("EighthDenomNumberOld")]
        public string eighthDenomNumberOld { get; set; }

        [JsonProperty("NinthDenomValueOld")]
        public string ninthDenomValueOld { get; set; }

        [JsonProperty("NinthDenomNumberOld")]
        public string ninthDenomNumberOld { get; set; }

        [JsonProperty("TenthDenomValueOld")]
        public string tenthDenomValueOld { get; set; }

        [JsonProperty("TenthDenomNumberOld")]
        public string tenthDenomNumberOld { get; set; }

        [JsonProperty("EleventhDenomValueOld")]
        public string eleventhDenomValueOld { get; set; }

        [JsonProperty("EleventhDenomNumberOld")]
        public string eleventhDenomNumberOld { get; set; }



        //
        [JsonProperty("Casette1Reject")]
        public string casette1Reject { get; set; }
        [JsonProperty("Casette2Reject")]
        public string casette2Reject { get; set; }
        [JsonProperty("Casette3Reject")]
        public string casette3Reject { get; set; }
        [JsonProperty("Casette4Reject")]
        public string casette4Reject { get; set; }

        [JsonProperty("Casette1RejectOld")]
        public string casette1RejectOld { get; set; }
        [JsonProperty("Casette2RejectOld")]
        public string casette2RejectOld { get; set; }
        [JsonProperty("Casette3RejectOld")]
        public string casette3RejectOld { get; set; }
        [JsonProperty("Casette4RejectOld")]
        public string casette4RejectOld { get; set; }

       
    }
}
