﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserAccountDetail
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("transactionId")]
        public int transactionId { get; set; }

        [JsonProperty("accountRangeId")]
        public int accountRangeId { get; set; }

        [JsonProperty("accountRangeName")]
        public string accountRangeName { get; set; }

        [JsonProperty("denizbank")]
        public string denizbank { get; set; }

        [JsonProperty("instutionMoney")]
        public string instutionMoney { get; set; }

        [JsonProperty("instutionName")]
        public string instutionName { get; set; }

        [JsonProperty("merkez")]
        public string merkez { get; set; }

        [JsonProperty("moneyCode")]
        public string moneyCode { get; set; }

        [JsonProperty("transactionDate")]
        public string transactionDate { get; set; }

        [JsonProperty("receivedAmount")]
        public string receivedAmount { get; set; }

        [JsonProperty("operationTypeId")]
        public int operationTypeId { get; set; }

        [JsonProperty("operationTypeName")]
        public string operationTypeName { get; set; }
    }
}
