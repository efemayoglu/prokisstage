﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserListAccountTransactionsProkis
    {
        [JsonProperty("errorCode")]
        public int errorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string errorDescription { get; set; }

        [JsonProperty("totalTransferAmount")]
        public string totalTransferAmount { get; set; }

        [JsonProperty("totalEarnedAmount")]
        public string totalEarnedAmount { get; set; }

        [JsonProperty("recordCount")]
        public int recordCount { get; set; }

        [JsonProperty("pageCount")]
        public int pageCount { get; set; }

        [JsonProperty("transactions")]
        public List<ParserAccountTransactionProkis> Transactions { get; set; }
    }
}
