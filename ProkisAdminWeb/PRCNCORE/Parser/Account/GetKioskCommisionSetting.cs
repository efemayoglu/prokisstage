﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Account
{
    public class GetKioskCommisionSetting
    {
        //[JsonProperty("GetCommisionSetting")]
        //public ParserGetCommisionSetting CommisionSetting { get; set; }

        [JsonProperty("creditCommision")]
        public int creditCommision { get; set; }

        [JsonProperty("mobileCommision")]
        public int mobileCommision { get; set; }

        [JsonProperty("cashCommision")]
        public int cashCommision { get; set; }

        [JsonProperty("yellowAlarm")]
        public int yellowAlarm { get; set; }

        [JsonProperty("redAlarm")]
        public int redAlarm { get; set; }

        [JsonProperty("instutionName")]
        public string instutionName { get; set; }
    }
}
