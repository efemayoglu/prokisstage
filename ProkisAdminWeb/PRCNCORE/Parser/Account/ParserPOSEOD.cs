﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Account
{
    public class ParserPOSEOD
    {
        [JsonProperty("resultCount")]
        public int ResultCount { get; set; }


        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("kioskName")]
        public string KioskName { get; set; }

        [JsonProperty("transactionDate")]
        public string TransactionDate { get; set; }

        [JsonProperty("transactionCount")]
        public string TransactionCount { get; set; }

        [JsonProperty("POSTransactionCount")]
        public string POSTransactionCount { get; set; }

        [JsonProperty("distinctionTransaction")]
        public string DistinctionTransaction { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("POSAmount")]
        public string POSAmount { get; set; }

        [JsonProperty("distinctionAmount")]
        public string DistinctionAmount { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("explanation")]
        public string Explanation { get; set; }

        [JsonProperty("user")]
        public string User { get; set; }
        
    }
}
