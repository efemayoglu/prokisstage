﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserCancelTnx
    {
        [JsonProperty("errorCode")]
        public int errorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string errorDescription { get; set; }

        [JsonProperty("soap:Envelope")]
        public SoapEnvelopeCancelTnx SoapEnvelopeCancelTnx { get; set; }

        [JsonProperty("cancelResult")]
        public int cancelResult { get; set; }

        [JsonProperty("opResult")]
        public int opResult { get; set; }
    }

    public class SatisIptalResult
    {
        [JsonProperty("satisIptalResult")]
        public int satisIptalResult { get; set; }
    }

    public class SoapEnvelopeCancelTnx
    {
        [JsonProperty("soap:Body")]
        public SoapBodyParserCancelTnx SoapBodyCancelTnx { get; set; }
    }

    public class SoapBodyParserCancelTnx
    {
        [JsonProperty("satisIptalResponse")]
        public SatisIptalResult satisIptalResponse { get; set; }
    }

    public class ParserCancelTnxLog
    {
        [JsonProperty("errorCode")]
        public int errorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string errorDescription { get; set; }

        [JsonProperty("returnCode")]
        public string returnCode { get; set; }
    }
}
