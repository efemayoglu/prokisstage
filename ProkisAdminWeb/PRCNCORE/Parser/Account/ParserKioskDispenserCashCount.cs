﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Account
{
    public class ParserKioskDispenserCashCount
    {

        [JsonProperty("KioskName")]
        public string kioskId { get; set; }

        [JsonProperty("KioskId")]
        public string kioskID { get; set; }

        [JsonProperty("InstitutionName")]
        public string institutionId { get; set; }

        [JsonProperty("InsertionDate")]
        public string insertionDate { get; set; }


        [JsonProperty("FirstCassetteValue")]
        public string firstDenomValue { get; set; }

        [JsonProperty("FirstCassetteNumber")]
        public string firstDenomNumber { get; set; }

        [JsonProperty("SecondCassetteValue")]
        public string secondDenomValue { get; set; }

        [JsonProperty("SecondCassetteNumber")]
        public string secondDenomNumber { get; set; }

        [JsonProperty("ThirdCassetteValue")]
        public string thirdDenomValue { get; set; }

        [JsonProperty("ThirdCassetteNumber")]
        public string thirdDenomNumber { get; set; }

        [JsonProperty("FourthCassetteValue")]
        public string fourthDenomValue { get; set; }

        [JsonProperty("FourthCassetteNumber")]
        public string fourthDenomNumber { get; set; }

        [JsonProperty("FifthCassetteValue")]
        public string fifthDenomValue { get; set; }

        [JsonProperty("FifthCassetteNumber")]
        public string fifthDenomNumber { get; set; }

        [JsonProperty("SixthCassetteValue")]
        public string sixthDenomValue { get; set; }

        [JsonProperty("SixthCassetteNumber")]
        public string sixthDenomNumber { get; set; }

        [JsonProperty("SeventhCassetteValue")]
        public string seventhDenomValue { get; set; }

        [JsonProperty("SeventhCassetteNumber")]
        public string seventhDenomNumber { get; set; }

        [JsonProperty("EighthCassetteValue")]
        public string eighthDenomValue { get; set; }

        [JsonProperty("EighthCassetteNumber")]
        public string eighthDenomNumber { get; set; }

        [JsonProperty("Casette1Reject")]
        public string Casette1Reject { get; set; }

        [JsonProperty("Casette2Reject")]
        public string Casette2Reject { get; set; }

        [JsonProperty("Casette3Reject")]
        public string Casette3Reject { get; set; }

        [JsonProperty("Casette4Reject")]
        public string Casette4Reject { get; set; }

        /*
        [JsonProperty("NinthDenomValue")]
        public string ninthDenomValue { get; set; }

        [JsonProperty("NinthDenomNumber")]
        public string ninthDenomNumber { get; set; }

        [JsonProperty("TenthDenomValue")]
        public string tenthDenomValue { get; set; }

        [JsonProperty("TenthDenomNumber")]
        public string tenthDenomNumber { get; set; }

        [JsonProperty("EleventhDenomValue")]
        public string eleventhDenomValue { get; set; }

        [JsonProperty("EleventhDenomNumber")]
        public string eleventhDenomNumber { get; set; }
         * 
         * */
    }
}
