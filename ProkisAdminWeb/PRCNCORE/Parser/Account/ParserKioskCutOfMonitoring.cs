﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Account
{
    public class ParserKioskCutOfMonitoring
    {
        [JsonProperty("StartDate")]
        public string startDate { get; set; }

        [JsonProperty("EndDate")]
        public string endDate { get; set; }

        [JsonProperty("CutDuration")]
        public string cutDuration { get; set; }


        [JsonProperty("KioskName")]
        public string kioskName { get; set; }

        [JsonProperty("Institution")]
        public string institution { get; set; }

        [JsonProperty("CutReason")]
        public string cutReason { get; set; }

        [JsonProperty("EnteredUser")]
        public string enteredUser { get; set; }

        [JsonProperty("EnteredDate")]
        public string enteredDate { get; set; }

        [JsonProperty("CutId")]
        public string cutId { get; set; }

        [JsonProperty("KioskId")]
        public string kioskId { get; set; }

        [JsonProperty("Inst")]
        public string Inst { get; set; }
    }
}
