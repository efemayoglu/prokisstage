﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserAccount
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("accountRangeId")]
        public int accountRangeId { get; set; }

        [JsonProperty("accountRangeName")]
        public string accountRangeName { get; set; }

        [JsonProperty("denizbank")]
        public string denizbank { get; set; }

        [JsonProperty("instutionMoney")]
        public string instutionMoney { get; set; }

        [JsonProperty("merkez")]
        public string merkez { get; set; }

        [JsonProperty("instutionName")]
        public string instutionName { get; set; }

        [JsonProperty("moneyCode")]
        public string moneyCode { get; set; }

        [JsonProperty("instutionId")]
        public string instutionId { get; set; }
    }

    public class ParserListCodeReport
    {
        [JsonProperty("errorCode")]
        public int errorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string errorDescription { get; set; }

        [JsonProperty("recordCount")]
        public int recordCount { get; set; }

        [JsonProperty("pageCount")]
        public int pageCount { get; set; }

        [JsonProperty("TotalTxnCount")]
        public string TotalTxnCount { get; set; }

        [JsonProperty("TotalGeneratedAskiAmount")]
        public string TotalGeneratedAskiAmount { get; set; }

        [JsonProperty("TotalUsedAskiAmount")]
        public string TotalUsedAskiAmount { get; set; }

        [JsonProperty("TotalGeneratedAskiCount")]
        public string TotalGeneratedAskiCount { get; set; }

        [JsonProperty("TotalUsedAskiCount")]
        public string TotalUsedAskiCount { get; set; }

        [JsonProperty("TotalReceivedAmount")]
        public string TotalReceivedAmount { get; set; }

        [JsonProperty("TotalKioskEmptyAmount")]
        public string TotalKioskEmptyAmount { get; set; }

        [JsonProperty("TotalEarnedAmount")]
        public string TotalEarnedAmount { get; set; }

        [JsonProperty("TotalParaUstu")]
        public string TotalParaUstu { get; set; }

        [JsonProperty("TotalSuccesTxnCount")]
        public string TotalSuccesTxnCount { get; set; }

        [JsonProperty("TotalInstitutionAmount")]
        public string TotalInstitutionAmount { get; set; }

        [JsonProperty("TotalCreditCardAmount")]
        public string TotalCreditCardAmount { get; set; }

        [JsonProperty("TotalMobilPaymentAmount")]
        public string TotalMobilPaymentAmount { get; set; }

        [JsonProperty("kioskReports")]
        public List<ParserCashReport> KioskReports { get; set; }
    }

    public class ParserCashReport
    {
        [JsonProperty("KioskName")]
        public string KioskName { get; set; }

        [JsonProperty("GeneratedTotal")]
        public string GeneratedTotal { get; set; }

        [JsonProperty("GeneratedTodayUsableToday")]
        public string GeneratedTodayUsableToday { get; set; }

        [JsonProperty("UsedTotalToday")]
        public string UsedTotalToday { get; set; }

        [JsonProperty("GeneratedTodayUsedToday")]
        public string GeneratedTodayUsedToday { get; set; }

        [JsonProperty("GeneratedYesterdayUsedToday")]
        public string GeneratedYesterdayUsedToday { get; set; }

        [JsonProperty("GeneratedYesterdayUsedTodayCount")]
        public string GeneratedYesterdayUsedTodayCount { get; set; }

        [JsonProperty("RemainUsableCode")]
        public string RemainUsableCode { get; set; }

        [JsonProperty("ResultCount")]
        public string ResultCount { get; set; }

        [JsonProperty("CashAmount")]
        public string CashAmount { get; set; }

        [JsonProperty("SuccessTxnCount")]
        public string SuccessTxnCount { get; set; }

        [JsonProperty("InstitutionAmount")]
        public string InstitutionAmount { get; set; }

        [JsonProperty("UsageFee")]
        public string UsageFee { get; set; }

        [JsonProperty("ParaUstu")]
        public string ParaUstu { get; set; }

        [JsonProperty("Mutabakat")]
        public string Mutabakat { get; set; }

        [JsonProperty("CreditCardAmount")]
        public string CreditCardAmount { get; set; }

        [JsonProperty("CreditCardCommisionAmount")]
        public string CreditCardCommisionAmount { get; set; }

        [JsonProperty("MobilePaymentCommisionAmount")]
        public string MobilePaymentCommisionAmount { get; set; }

        [JsonProperty("MobilePaymentAmount")]
        public string MobilePaymentAmount { get; set; }

        [JsonProperty("ManeullyGeneratedCodeAmount")]
        public string ManeullyGeneratedCodeAmount { get; set; }

        [JsonProperty("AskidaCode")]
        public string AskidaCode { get; set; }

        [JsonProperty("ErasedCode")]
        public string ErasedCode { get; set; }

        [JsonProperty("ReverseRecCount")]
        public string ReverseRecCount { get; set; }

        [JsonProperty("ReverseRecTotal")]
        public string ReverseRecTotal { get; set; }

        [JsonProperty("NonReverseRecCount")]
        public string NonReverseRecCount { get; set; }

        [JsonProperty("NonReverseRecTotal")]
        public string NonReverseRecTotal { get; set; }

        [JsonProperty("RecCount")]
        public string RecCount { get; set; }

        [JsonProperty("RecTotal")]
        public string RecTotal { get; set; }
    }
}
