﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Account
{
    public class ParserBankAccountAction
    {
        [JsonProperty("InsertionDate")]
        public string insertionDate { get; set; }

        [JsonProperty("AskiBalance")]
        public string askiBalance { get; set; }

        [JsonProperty("AskiFeeBalance")]
        public string askiFeeBalance { get; set; }

        [JsonProperty("BgazBalance")]
        public string bgazBalance { get; set; }

        [JsonProperty("BgazFeeBalance")]
        public string bgazFeeBalance { get; set; }

        [JsonProperty("DesmerBalance")]
        public string desmerBalance { get; set; }
    }
}
