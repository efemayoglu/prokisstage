﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserDailyReportProkis
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("insertionDate")]
        public string InsertionDate { get; set; }

        [JsonProperty("totalBalance")]
        public string TotalBalance { get; set; }

        [JsonProperty("activeAskiCode")]
        public string ActiveAskiCode { get; set; }

        [JsonProperty("pasifAskiCode")]
        public string PasifAskiCode { get; set; }

        [JsonProperty("pasifCodeBalance")]
        public string PasifCodeBalance { get; set; }

        [JsonProperty("pasifCodeCount")]
        public string PasifCodeCount { get; set; }

        [JsonProperty("usableCodeBeforeExpiring")]
        public string UsableCodeBeforeExpiring { get; set; }
    }
}
