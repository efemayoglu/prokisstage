﻿using MakeMutabakatParserNS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Account
{
    class ParserListInstutionMutabakat
    {
        [JsonProperty("Durumu")]
        public string Durumu { get; set; }

        [JsonProperty("Mesaj")]
        public string Mesaj { get; set; }

        [JsonProperty("Adet")]
        public int Adet { get; set; }

        [JsonProperty("Faturalar")]
        public Faturalar[] Faturalar { get; set; }

        [JsonProperty("DbDatas")]
        public DbDatas DbDatas { get; set; }

        [JsonProperty("errorCode")]
        public int ErrorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string ErrorDescription { get; set; }
    }
}
