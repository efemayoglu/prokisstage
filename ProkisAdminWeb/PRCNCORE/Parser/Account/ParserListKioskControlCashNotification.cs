﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser.Account
{
    public class ParserListKioskControlCashNotification
    {


        [JsonProperty("kioskcontrolcashnotification")]
        public List<ParserKioskControlCashNotification> ControlCashNotifications { get; set; }

        [JsonProperty("totalfirstDenomNumber")]
        public string totalfirstDenomNumber { get; set; }

        [JsonProperty("totalsecondDenomNumber")]
        public string totalsecondDenomNumber { get; set; }

        [JsonProperty("totalthirdDenomNumber")]
        public string totalthirdDenomNumber { get; set; }

        [JsonProperty("totalfourthDenomNumber")]
        public string totalfourthDenomNumber { get; set; }

        [JsonProperty("totalfifthDenomNumber")]
        public string totalfifthDenomNumber { get; set; }

        [JsonProperty("totalsixthDenomNumber")]
        public string totalsixthDenomNumber { get; set; }

        [JsonProperty("totalseventhDenomNumber")]
        public string totalseventhDenomNumber { get; set; }

        [JsonProperty("totaleighthDenomNumber")]
        public string totaleighthDenomNumber { get; set; }

        [JsonProperty("totalninthDenomNumber")]
        public string totalninthDenomNumber { get; set; }

        [JsonProperty("totaltenthDenomNumber")]
        public string totaltenthDenomNumber { get; set; }

        [JsonProperty("totaleleventhDenomNumber")]
        public string totaleleventhDenomNumber { get; set; }

        [JsonProperty("totalAmount")]
        public string totalAmount { get; set; }


        [JsonProperty("errorCode")]
        public int errorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string errorDescription { get; set; }

        [JsonProperty("recordCount")]
        public int recordCount { get; set; }

        [JsonProperty("pageCount")]
        public int pageCount { get; set; }

       
    }
}
