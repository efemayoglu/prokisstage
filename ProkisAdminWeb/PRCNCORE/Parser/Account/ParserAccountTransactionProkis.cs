﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserAccountTransactionProkis
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("TransactionDate")]
        public string TransactionDate { get; set; }

        [JsonProperty("instutionName")]
        public string instutionName { get; set; }

        [JsonProperty("ReceivedAmount")]
        public string ReceivedAmount { get; set; }

        [JsonProperty("CustomerName")]
        public string CustomerName { get; set; }

        [JsonProperty("AboneNo")]
        public string AboneNo { get; set; }

    }
}
