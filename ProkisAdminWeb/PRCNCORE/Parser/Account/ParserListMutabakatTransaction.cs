﻿using MakeMutabakatParserNS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserListMutabakatTransaction
    {

        [JsonProperty("Durumu")]
        public string Durumu { get; set; }

        [JsonProperty("Mesaj")]
        public string Mesaj { get; set; }

        [JsonProperty("Adet")]
        public int Adet { get; set; }

        [JsonProperty("Faturalar")]
        public Faturalar[] Faturalar { get; set; }

        [JsonProperty("DbDatasList")]
        public DbDatasList[] DbDatasList { get; set; }

        [JsonProperty("soap:Envelope")]
        public SoapEnvelope SoapEnvelope { get; set; }

        [JsonProperty("errorCode")]
        public int ErrorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string ErrorDescription { get; set; }

        [JsonProperty("ES_TOPLAM")]
        public ES_TOPLAM ES_TOPLAM { get; set; }

        [JsonProperty("ET_DETAY")]
        public ET_DETAY[] ET_DETAY { get; set; }

        [JsonProperty("Response")]
        public List<PRCNCORE.BankActions.DBAskiActionsFromServiceDetail> Response { get; set; }
        
    }

    public class ParserListMutabakatMaskiTransaction
    {

        [JsonProperty("Durumu")]
        public string Durumu { get; set; }

        [JsonProperty("Mesaj")]
        public string Mesaj { get; set; }

        [JsonProperty("Adet")]
        public int Adet { get; set; }

        [JsonProperty("DbDatasList")]
        public DbDatasList[] DbDatasList { get; set; }

        [JsonProperty("soap:Envelope")]
        public SoapEnvelopeMaskiList SoapEnvelope { get; set; }

        [JsonProperty("errorCode")]
        public int ErrorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string ErrorDescription { get; set; }
    }

    public class DetayMutabakatResultModel2
    {

        [JsonProperty("AboneNo")]
        public string AboneNo { get; set; }

        [JsonProperty("MakbuzNo")]
        public string MakbuzNo { get; set; }

        [JsonProperty("MakbuzSeri")]
        public string MakbuzSeri { get; set; }

        [JsonProperty("Tutar")]
        public string Tutar { get; set; }

        [JsonProperty("IptalTutar")]
        public string IptalTutar { get; set; }

        [JsonProperty("Donem")]
        public string Donem { get; set; }

        [JsonProperty("Yil")]
        public string Yil { get; set; }

        [JsonProperty("HataKodu")]
        public string HataKodu { get; set; }

        [JsonProperty("TransactionCode")]
        public string TransactionCode { get; set; }
    }

    public class BankaDetayMutabakatResult
    {

        [JsonProperty("DetayMutabakatResultModel2")]
        public DetayMutabakatResultModel2[] DetayMutabakatResultModel2 { get; set; }
    }

    public class BankaDetayMutabakatResponse
    {
        [JsonProperty("BankaDetayMutabakatResult")]
        public BankaDetayMutabakatResult BankaDetayMutabakatResult { get; set; }
    }

    public class SoapBody
    {

        [JsonProperty("BankaDetayMutabakatResponse")]
        public BankaDetayMutabakatResponse BankaDetayMutabakatResponse { get; set; }
    }

    public class SoapBodyMaskiList
    {
        [JsonProperty("BankaDetayMutabakatResponse")]
        public BankaDetayMutabakatResponse BankaDetayMutabakatResponse { get; set; }
    }

    public class SoapEnvelopeMaskiList
    {
        [JsonProperty("soap:Body")]
        public SoapBody SoapBody { get; set; }
    }

}
