﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PRCNCORE.Parser.User
{
    public class ParserUserLogDetails
    {
        [JsonProperty("UserName")]
        public string userName { get; set; }

        [JsonProperty("TransactionDate")]
        public string transactionDate { get; set; }

        [JsonProperty("Transaction")]
        public string transaction { get; set; }

        [JsonProperty("Pages")]
        public string pages { get; set; }

        [JsonProperty("Status")]
        public string status { get; set; }

        [JsonProperty("Explanation")]
        public string explanation { get; set; }

        [JsonProperty("NewValue")]
        public string newValue { get; set; }

        [JsonProperty("OldValue")]
        public string oldValue { get; set; }

      
    }
}
