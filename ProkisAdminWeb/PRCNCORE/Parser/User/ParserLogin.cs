﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserLogin
    {
        [JsonProperty("errorCode")]
        public int errorCode { get; set; }

        [JsonProperty("errorDescription")]
        public string errorDescription { get; set; }

        [JsonProperty("User")]
        public ParserUser User { get; set; }

        [JsonProperty("UserRoles")]
        public List<ParserLoginUserRoles> UserRoles { get; set; }

        [JsonProperty("accessToken")]
        public string AccessToken { get; set; }
    }
}
