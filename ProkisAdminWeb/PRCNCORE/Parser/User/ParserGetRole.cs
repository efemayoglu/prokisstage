﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserGetRole
    {
        [JsonProperty("Role")]
        public ParserRole Role { get; set; }

        [JsonProperty("RoleSubMenus")]
        public List<ParserRoleSubMenus> RoleSubMenus { get; set; }
    }
}
