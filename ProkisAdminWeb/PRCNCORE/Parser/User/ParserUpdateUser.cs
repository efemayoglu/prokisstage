﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserUpdateUser
    {
        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("userName")]
        public string userName { get; set; }

        [JsonProperty("email")]
        public string email { get; set; }

        [JsonProperty("cellphone")]
        public string cellphone { get; set; }

        [JsonProperty("telephone")]
        public string telephone { get; set; }

        [JsonProperty("roleId")]
        public int roleId { get; set; }

        [JsonProperty("status")]
        public int status { get; set; }

        [JsonProperty("updatedUserId")]
        public int updatedUserId { get; set; }
    }
}
