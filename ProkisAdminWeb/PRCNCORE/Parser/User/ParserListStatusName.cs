﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserListStatusName
    {
        [JsonProperty("StatusNames")]
        public List<ParserStatusName> StatusNames { get; set; }
    }
}
