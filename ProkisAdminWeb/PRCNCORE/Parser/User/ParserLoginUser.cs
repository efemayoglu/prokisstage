﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserLoginUser
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("UserName")]
        public string UserName { get; set; }

        [JsonProperty("EMail")]
        public string EMail { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("Telephone")]
        public string Telephone { get; set; }

        [JsonProperty("CellPhone")]
        public string CellPhone { get; set; }

        [JsonProperty("RoleId")]
        public int RoleId { get; set; }

        [JsonProperty("RoleName")]
        public string RoleName { get; set; }
    }
}
