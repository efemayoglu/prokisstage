﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserRoleSubMenus
    {
        [JsonProperty("MenuId")]
        public int MenuId { get; set; }

        [JsonProperty("MenuName")]
        public string MenuName { get; set; }

        [JsonProperty("SubMenuId")]
        public int SubMenuId { get; set; }

        [JsonProperty("SubMenuName")]
        public string Email { get; set; }

        [JsonProperty("SubMenuURL")]
        public string SubMenuURL { get; set; }

        [JsonProperty("CanAdd")]
        public int CanAdd { get; set; }

        [JsonProperty("CanEdit")]
        public int CanEdit { get; set; }

        [JsonProperty("CanDelete")]
        public int CanDelete { get; set; }
    }
}
