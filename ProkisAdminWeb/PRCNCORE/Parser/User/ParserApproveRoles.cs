﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserApproveRoles
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("UpdatedDate")]
        public string UpdatedDate { get; set; }

        [JsonProperty("UpdatedUser")]
        public string UpdatedUser { get; set; }

        [JsonProperty("OldName")]
        public string OldName { get; set; }

        [JsonProperty("OldDescription")]
        public string OldDescription { get; set; }

        [JsonProperty("OldUpdatedDate")]
        public string OldUpdatedDate { get; set; }

        [JsonProperty("OldUpdatedUser")]
        public string OldUpdatedUser { get; set; }
    }
}
