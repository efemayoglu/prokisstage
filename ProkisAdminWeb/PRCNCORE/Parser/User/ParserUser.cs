﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserUser
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Username")]
        public string Username { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Cellphone")]
        public string Cellphone { get; set; }

        [JsonProperty("Telephone")]
        public string Telephone { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("UserStatusId")]
        public int UserStatusId { get; set; }

        [JsonProperty("StatusName")]
        public string StatusName { get; set; }

        [JsonProperty("RoleId")]
        public int RoleId { get; set; }

        [JsonProperty("RoleName")]
        public string RoleName { get; set; }

        [JsonProperty("CreatedDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("LastLoginDate")]
        public DateTime LastLoginDate { get; set; }

        [JsonProperty("PasswordMustChangeId")]
        public int PasswordMustChangeId { get; set; }

        [JsonProperty("LastPasswordChangeDate")]
        public DateTime LastPasswordChangeDate { get; set; }

        [JsonProperty("LastPasswords")]
        public string LastPasswords { get; set; }

        [JsonProperty("WrongAccessTryCount")]
        public int WrongAccessTryCount { get; set; }

        [JsonProperty("LastWrongAccessTryDate")]
        public DateTime LastWrongAccessTryDate { get; set; }

    }
}
