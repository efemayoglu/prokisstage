﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserGetUser
    {
        [JsonProperty("Users")]
        public List<ParserUser> User { get; set; }
    }
}
