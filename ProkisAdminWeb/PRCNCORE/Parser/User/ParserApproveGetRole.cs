﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserApproveGetRole
    {
        [JsonProperty("OldName")]
        public string OldName { get; set; }

        [JsonProperty("OldDescription")]
        public string OldDescription { get; set; }

        [JsonProperty("OldRoleSubMenus")]
        public List<ParserRoleSubMenus> OldRoleSubMenus { get; set; }

        [JsonProperty("NewName")]
        public string NewName { get; set; }

        [JsonProperty("NewDescription")]
        public string NewDescription { get; set; }

        [JsonProperty("NewRoleSubMenus")]
        public List<ParserRoleSubMenus> NewRoleSubMenus { get; set; }
    }
}
