﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserLoginUserRoles
    {
        [JsonProperty("MenuId")]
        public int MenuId { get; set; }

        [JsonProperty("MenuName")]
        public string MenuName { get; set; }

        [JsonProperty("SubMenuId")]
        public int SubMenuId { get; set; }

        [JsonProperty("SubMenuName")]
        public string SubMenuName { get; set; }

        [JsonProperty("SubMenuURL")]
        public string SubMenuURL { get; set; }

        [JsonProperty("CanAdd")]
        public string CanAdd { get; set; }

        [JsonProperty("CanDelete")]
        public string CanDelete { get; set; }

        [JsonProperty("CanEdit")]
        public string CanEdit { get; set; }
    }
}
