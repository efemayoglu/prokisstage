﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Color
{
    public class ColorByValues
    {
        /*
         red-flat
        red-circual
        yellow-flat
        yellow-circual
        green-flat
        green-circual
      */

        public string ColorByValue(string value)
        {
            try
            {
                switch (value)
                {
                    case "1": return "green-circual";
                    case "0": return "red-circual";
                    default: return "red-circual";
                }

            }
            catch (Exception)
            {
                return "";
            }
        } 
    }
}
