﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Mutabakat
{
    public class MutabakatCompare
    {
        public string BOSInvoiceNumber { get; set; }
        public string BOSInvoiceTotal { get; set; }
        public string BOSPrepaidNumber { get; set; }
        public string BOSPrepaidTotal { get; set; }
        public string BOSNumber { get; set; }
        public string BOSTotal { get; set; }
        public string BOSCancelNumber { get; set; }
        public string BOSCancelTotal { get; set; }

        public string InstitutionInvoiceNumber { get; set; }
        public string InstitutionInvoiceTotal { get; set; }
        public string InstitutionPrepaidNumber { get; set; }
        public string InstitutionPrepaidTotal { get; set; }
        public string InstitutionTotalNumber { get; set; }
        public string InstitutionTotalAmount { get; set; }
        public string InstitutionCancelNumber { get; set; }
        public string InstitutionCancelTotal { get; set; }

        public string TotalDifferenceNumber { get; set; }
        public string TotalDifferenceTotal { get; set; }

        public List<MutabakatCompareDetails> BOSSuccessedTxn { get; set; }
        public List<MutabakatCompareDetails> BOSFailedTxn { get; set; }
        public List<MutabakatCompareDetails> InstitutionSuccessedTxn { get; set; }
        public List<MutabakatCompareDetails> InstitutionFailedTxn { get; set; }
    }
    public class MutabakatCompareDetails
    {
        public string Number { get; set; }
        public string TransactionId { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Amount { get; set; }
        public string ReferenceId { get; set; }
        public string ProcessResult { get; set; }
        public string Status { get; set; }
        public string InsertionDate { get; set; }
        public string InvoiceNumber { get; set; }
        public string ReverseRecord { get; set; }
        public string CompareResult { get; set; }
    }
}
