﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using PRCNCORE.Utilities;

namespace PRCNCORE.Backend
{
    public abstract class DBOperator : IDisposable
    {
        private SqlConnection theConnection;

        protected DBOperator()
        {
        }

        protected DBOperator(SqlConnection theConnection)
        {
            this.theConnection = theConnection;
        }

        protected SqlConnection GetConnection()
        {
            if (this.theConnection == null || this.theConnection.State == ConnectionState.Closed)
            {
                this.theConnection = new SqlConnection(this.GetDBConnectionString());
                try
                {
                    this.theConnection.Open();
                }
                catch (Exception exp)
                {
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetConnection");

                }
            }

            return this.theConnection;
        }

        private string GetDBConnectionString()
        {
            return Utility.DecryptString(Utility.GetConfigValue("DBConnectionText"));
        }

        private void Terminate()
        {
            if (this.theConnection != null)
            {
                this.theConnection.Close();
                this.theConnection.Dispose();
                this.theConnection = null;
            }
        }

        #region IDisposable Members

        public virtual void Dispose()
        {
            GC.SuppressFinalize(this);
            this.Terminate();
        }

        #endregion
    }
}
