﻿using Microsoft.ApplicationBlocks.Data;
using PRCNCORE.Desmer;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using PRCNCORE.Constants;
using PRCNCORE.Parser.Other;

namespace PRCNCORE.Backend
{
    public class DesmerOpr : DBOperator
    {
        public List<Talep> GetTaleps(DateTime startDate, DateTime endDate)
        {
            var taleps = new List<Talep>();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@startDate", SqlDbType.DateTime),
                    new SqlParameter("@endDate", SqlDbType.DateTime),
                    new SqlParameter("@returnCode", 0)
                };

                sqlParameters[0].Value = startDate;
                sqlParameters[1].Value = endDate;
                sqlParameters[2].Direction = ParameterDirection.InputOutput;
                var resp = Convert.ToInt32(sqlParameters[2].Value);

                dataSet = SqlHelper.ExecuteDataset(
                    base.GetConnection(),
                    CommandType.StoredProcedure,
                    "pk.get_desmer_talep",
                    Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")),
                    sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    if (dataRow == null)
                        continue;

                    var status =
                        dataRow["Status"] == null || dataRow["Status"].ToString() == ""
                            ? 0
                            : (int) dataRow["Status"];

                    string statusColor;
                    switch (status)
                    {
                        case 0:
                            statusColor = "";
                            break;
                        case 1:
                            statusColor = "success";
                            break;
                        case 2://Eşleşme gerekli
                            statusColor = "info";
                            break;
                        case 3://Bos Eksik
                            statusColor = "warning";
                            break;
                        case 4://Desmer hata
                            statusColor = "danger";
                            break;
                        default:
                            statusColor = "";
                            break;
                    }

                    var talepTarih = dataRow["TalepTarih"];
                    DateTime? talepTarihParsed;
                    if (talepTarih != null)
                    {
                        talepTarihParsed = DateTime.Parse(talepTarih.ToString());
                    }
                    else
                        talepTarihParsed = null;

                    var islemTipi =
                        dataRow["IslemTipi"] == null || dataRow["IslemTipi"].ToString() == ""
                            ? 0
                            : (int) dataRow["IslemTipi"];
                    var talepDurum =
                        dataRow["TalepDurum"] == null || dataRow["TalepDurum"].ToString() == ""
                            ? 0
                            : (int) dataRow["TalepDurum"];
                    var tutar =
                        dataRow["Tutar"] == null || dataRow["Tutar"].ToString() == ""
                            ? 0
                            : (decimal) dataRow["Tutar"];

                    taleps.Add(
                        new Talep
                        {
                            Status = status,
                            StatusColor = statusColor,
                            IslemTipi = islemTipi,
                            TalepDurum = talepDurum,
                            ReferansId = dataRow["ReferansId"] == null ? "" : dataRow["ReferansId"].ToString(),
                            HizmetNoktaKod = dataRow["KioskId"] == null ? "" : dataRow["KioskId"].ToString(),
                            Tutar = tutar,
                            TalepTipi = dataRow["TalepTipi"] == null ? "" : dataRow["TalepTipi"].ToString(),
                            TalepTarih = talepTarihParsed,
                            Aciklama = dataRow["Aciklama"] == null ? "" : dataRow["Aciklama"].ToString(),
                            FulfilId = dataRow["FulfilId"] == null ? "" : dataRow["FulfilId"].ToString()
                        });
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetTaleps");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return taleps;
        }



        public bool InsertTalep(Talep talep, string fulfilId, out int refId)
        {
            refId = 0;
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@FulfilId", fulfilId),
                    new SqlParameter("@KioskId", talep.HizmetNoktaKod),
                    new SqlParameter("@IslemTipi", talep.IslemTipi),
                    new SqlParameter("@TalepTipi", talep.TalepTipi),
                    new SqlParameter("@TalepTarih", SqlDbType.DateTime),
                    new SqlParameter("@Tutar", SqlDbType.Decimal),
                    new SqlParameter("@Aciklama", talep.Aciklama),
                    new SqlParameter("@ReferansId", 0),
                    new SqlParameter("@returnCode", 0)
                };

                sqlParameters[4].IsNullable = true;
                if (talep.TalepTarih == DateTime.MinValue)
                    sqlParameters[4].Value = null;
                else
                    sqlParameters[4].Value = talep.TalepTarih;


                sqlParameters[5].Value = talep.Tutar;
                sqlParameters[7].Direction = ParameterDirection.InputOutput;
                refId = Convert.ToInt32(sqlParameters[7].Value);

                sqlParameters[8].Direction = ParameterDirection.InputOutput;
                var resp = Convert.ToInt32(sqlParameters[8].Value);

                dataSet = SqlHelper.ExecuteDataset(
                    base.GetConnection(),
                    CommandType.StoredProcedure,
                    "pk.insert_desmer_talep",
                    sqlParameters);

                refId = Convert.ToInt32(sqlParameters[7].Value);
                resp = Convert.ToInt32(sqlParameters[8].Value);


                if (dataSet != null)
                    return true;
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREInsertTalep");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return false;
        }

        public bool EditTalep(
            string referansId,
            string hizmetNoktaKod,
            int islemTipi,
            int talepDurum,
            string talepTipi,
            DateTime? talepTarih,
            decimal tutar,
            string aciklama,
            string personel
        )
        {
            DataSet dataSet = null;
            try
            {
                var sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@ReferansId", int.Parse(referansId)),
                    new SqlParameter("@FulfilId", null),
                    new SqlParameter("@KioskId", hizmetNoktaKod),
                    new SqlParameter("@IslemTipi", islemTipi),
                    new SqlParameter("@TalepDurum", SqlDbType.Int),
                    new SqlParameter("@TalepTipi", talepTipi),
                    new SqlParameter("@TalepTarih", SqlDbType.DateTime),
                    new SqlParameter("@Tutar", SqlDbType.Decimal),
                    new SqlParameter("@Aciklama", aciklama),
                    new SqlParameter("@Personel", personel),
                    new SqlParameter("@returnCode", 0),
                    new SqlParameter("@Status", null)
                };

                sqlParameters[4].IsNullable = true;
                if (talepDurum == 0)
                    sqlParameters[4].Value = null;
                else
                    sqlParameters[4].Value = talepDurum;

                sqlParameters[6].IsNullable = true;
                if (talepTarih == DateTime.MinValue)
                    sqlParameters[6].Value = null;
                else
                    sqlParameters[6].Value = talepTarih;

                sqlParameters[7].Value = tutar;

                sqlParameters[10].Direction = ParameterDirection.InputOutput;
                var resp = Convert.ToInt32(sqlParameters[10].Value);

                dataSet = SqlHelper.ExecuteDataset(
                    base.GetConnection(),
                    CommandType.StoredProcedure,
                    "pk.update_desmer_talep",
                    sqlParameters);

                resp = Convert.ToInt32(sqlParameters[10].Value);


                if (dataSet != null && resp == 1)
                    return true;
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREEditTalep");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return false;
        }

        public bool EditTalepStatus(string referansId, int status)
        {
            DataSet dataSet = null;
            try
            {
                var sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@Status", SqlDbType.Int),
                    new SqlParameter("@ReferansId", int.Parse(referansId)),
                    new SqlParameter("@FulfilId", null),
                    new SqlParameter("@KioskId", null),
                    new SqlParameter("@IslemTipi", null),
                    new SqlParameter("@TalepDurum", null),
                    new SqlParameter("@TalepTipi", null),
                    new SqlParameter("@TalepTarih", null),
                    new SqlParameter("@Tutar", null),
                    new SqlParameter("@Aciklama", null),
                    new SqlParameter("@Personel", null),
                    new SqlParameter("@returnCode", 0)
                };

                sqlParameters[0].IsNullable = true;
                sqlParameters[0].Value = status;

                sqlParameters[11].Direction = ParameterDirection.InputOutput;
                var resp = Convert.ToInt32(sqlParameters[11].Value);

                dataSet = SqlHelper.ExecuteDataset(
                    base.GetConnection(),
                    CommandType.StoredProcedure,
                    "pk.update_desmer_talep",
                    sqlParameters);

                resp = Convert.ToInt32(sqlParameters[11].Value);


                if (dataSet != null && resp == 1)
                    return true;
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREEditTalepStatus");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return false;
        }

        public bool DeleteTalep(int refId)
        {
            refId = 0;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@ReferansId", refId),
                    new SqlParameter("@returnCode", 0)
                };

                sqlParameters[1].Direction = ParameterDirection.InputOutput;
                var resp = Convert.ToInt32(sqlParameters[1].Value);

                int a = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure,
                    "[pk].[delete_desmer_talep]", sqlParameters);

                resp = Convert.ToInt32(sqlParameters[1].Value);

                return resp == 1;
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREDeleteTalep");
            }
            finally
            {
                base.Dispose();
            }

            return false;
        }


        public int EditControlCashNotification(
            int expertUserId,
            string firstDenomNumber,
            string secondDenomNumber,
            string thirdDenomNumber,
            string fourthDenomNumber,
            string fifthDenomNumber,
            string sixthDenomNumber,
            string seventhDenomNumber,
            string eighthDenomNumber,
            string ninthDenomNumber,
            string tenthDenomNumber,
            string eleventhDenomNumber,
            decimal totalAmount,
            string desmerId
            //, string approvedCodeapprovedCode
        )
        {
            int resp = 0;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@desmerId", desmerId),
                    new SqlParameter("@expertUserId", expertUserId),
                    new SqlParameter("@returnCode", 0),
                    new SqlParameter("@firstDenomNumber", firstDenomNumber),
                    new SqlParameter("@secondDenomNumber", secondDenomNumber),
                    new SqlParameter("@thirdDenomNumber", thirdDenomNumber),
                    new SqlParameter("@fourthDenomNumber", fourthDenomNumber),
                    new SqlParameter("@fifthDenomNumber", fifthDenomNumber),
                    new SqlParameter("@sixthDenomNumber", sixthDenomNumber),
                    new SqlParameter("@seventhDenomNumber", seventhDenomNumber),
                    new SqlParameter("@eighthDenomNumber", eighthDenomNumber),
                    new SqlParameter("@ninthDenomNumber", ninthDenomNumber),
                    new SqlParameter("@tenthDenomNumber", tenthDenomNumber),
                    new SqlParameter("@eleventhDenomNumber", eleventhDenomNumber),
                    new SqlParameter("@timing", DateTime.Now),
                    new SqlParameter("@totalAmount", totalAmount)
                };

                sqlParameters[2].Direction = ParameterDirection.InputOutput;
                resp = Convert.ToInt32(sqlParameters[2].Value);

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure,
                    "[pk].[update_kiosk_cash_control_notification_desmer]", sqlParameters);

                resp = Convert.ToInt32(sqlParameters[2].Value);
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREEditControlCashNotification");
                resp = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
            }
            finally
            {
                base.Dispose();
            }

            return resp;
        }

        public int EditKioskReferenceSystemOrder(
            int adminUserId,
            string referenceNo,
            string kioskId,
            string explanation,
            string desmerId)
        {
            int success = 0;

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@desmerId", desmerId),
                    new SqlParameter("@returnCode", success),
                    new SqlParameter("@adminUserId", adminUserId),
                    new SqlParameter("@referenceNo", referenceNo),
                    new SqlParameter("@kioskId", kioskId),
                    new SqlParameter("@explanation", explanation)
                };

                sqlParameters[1].Direction = ParameterDirection.Output;

                int a = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure,
                    "[pk].[update_kiosk_reference_system_desmer]", sqlParameters);

                success = Convert.ToInt32(sqlParameters[1].Value);
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveSettings");
            }
            finally
            {
                base.Dispose();
            }

            return success;
        }
    }
}