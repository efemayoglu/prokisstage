﻿using Microsoft.ApplicationBlocks.Data;
using PRCNCORE.Cities;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Backend
{
    public class CityOpr : DBOperator
    {
        public CityOpr()
        {

        }

        internal CityOpr(SqlConnection sqlConnection)
            : base(sqlConnection)
        {

        }

        private City GetCities(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new City(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }

        private Capacity GetCapacities(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new Capacity(Convert.ToInt32(dataRow["Id"])
            , dataRow["Capacity"].ToString());
        }


        private Region GetRegions(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new Region(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }

        public RegionCollection GetRegions(int cityId)
        {
            RegionCollection regions = new RegionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@cityId", cityId)};
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_region_names", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    regions.Add(this.GetRegions(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetRegions");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return regions;
        }
        public RegionCollection GetDistricts(int cityId)
        {
            RegionCollection regions = new RegionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@cityId", cityId) };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_district_names", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    regions.Add(this.GetRegions(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetRegions");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return regions;
        }

        public RegionCollection GetNeighborhoods(int districtId)
        {
            RegionCollection regions = new RegionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@districtId", districtId) };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_neighborhood_names", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    regions.Add(this.GetRegions(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetRegions");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return regions;
        }



        public CityCollection GetCities()
        {
            CityCollection cities = new CityCollection();
            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_city_names");

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    cities.Add(this.GetCities(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetCities");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return cities;
        }


        public CapacityCollection GetCapacities()
        {
            CapacityCollection capacities = new CapacityCollection();
            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_acceptor_cashbox_type");

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    capacities.Add(this.GetCapacities(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetCapacities");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return capacities;
        }

    }
}
