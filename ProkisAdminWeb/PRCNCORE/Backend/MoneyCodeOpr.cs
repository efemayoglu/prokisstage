﻿using Microsoft.ApplicationBlocks.Data;
using PRCNCORE.Kiosks;
using PRCNCORE.MoneyCodes;
using PRCNCORE.Roles;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Backend
{
    public class MoneyCodeOpr : DBOperator
    {
        public MoneyCodeOpr()
        {

        }

        internal MoneyCodeOpr(SqlConnection sqlConnection)
            : base(sqlConnection)
        {

        }
        private MoneyCodeSweepBack GetMoneyCodeSweepBack(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new MoneyCodeSweepBack(Convert.ToInt64(dataRow["Id"])
            , Convert.ToInt32(dataRow["StatusId"])
            , dataRow["StatusName"].ToString()
            , dataRow["InsertionDate"].ToString()
            , dataRow["CustomerName"].ToString()
            , dataRow["AboneNo"].ToString()
            , Convert.ToInt64(dataRow["CardId"].ToString())
            , Convert.ToInt64(dataRow["TransactionId"].ToString())
            , dataRow["CodeAmount"].ToString()
            , dataRow["CodeNumber"].ToString()
            , dataRow["CodeGenerationCase"].ToString()
            , dataRow["UseDate"].ToString()
            , dataRow["GeneratedUserId"].ToString()
            , dataRow["GeneratedUserName"].ToString()
            , dataRow["InstutionName"].ToString()
            , ""
            );
        }
        private MoneyCode GetMoneyCode(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new MoneyCode(Convert.ToInt64(dataRow["Id"])
            , Convert.ToInt32(dataRow["StatusId"])
            , dataRow["StatusName"].ToString()
            , dataRow["InsertionDate"].ToString()
            , dataRow["CustomerName"].ToString()
            , dataRow["AboneNo"].ToString()
            , Convert.ToInt64(dataRow["CardId"].ToString())
            , Convert.ToInt64(dataRow["TransactionId"].ToString())
            , dataRow["CodeAmount"].ToString()
            , dataRow["CodeNumber"].ToString()
            , dataRow["CodeGenerationCase"].ToString()
            , dataRow["UseDate"].ToString()
            , dataRow["GeneratedUserId"].ToString()
            , dataRow["GeneratedUserName"].ToString()
            , dataRow["InstutionName"].ToString()
            , dataRow["generatedKioskName"].ToString()
            , dataRow["usedKioskName"].ToString()
            , dataRow["deletingReason"].ToString()
            , dataRow["SendingSMS"].ToString()
            , dataRow["ApprovalSMS"].ToString()
            , dataRow["CodeDeleteUser"].ToString()
            , dataRow["CountSMSCell"].ToString()
            , dataRow["ApprovedUser"].ToString()
            , dataRow["ApprovedDate"].ToString()
            , ""
            );
        }

        public MoneyCodeCollection SearchMoneyCodesOrder(string searchText,
                                                       DateTime startDate,
                                                       DateTime endDate,
                                                       int numberOfItemsPerPage,
                                                       int currentPageNum,
                                                       ref int recordCount,
                                                       ref int pageCount,
                                                       int orderSelectionColumn,
                                                       int descAsc,
                                                       int codeStatus,
                                                       int generatedType,
                                                       ref string totalAmount,
                                                       int instutionId,
                                                       string generatedKioskIds,
                                                       string usedKioskIds,
                                                       int paymentType,
                                                       int orderType)
        {
            MoneyCodeCollection items = new MoneyCodeCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@codeStatus", codeStatus),
                                                                    new SqlParameter("@generatedType", generatedType),
                                                                    new SqlParameter("@totalAmount", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@instutionId",instutionId),
                                                                    new SqlParameter("@generatedKioskIds",generatedKioskIds),
                                                                    new SqlParameter("@paymentType",paymentType),       
                                                                    new SqlParameter("@orderType",orderType)        
                                                                    };

                sqlParameters[3].Value = Convert.ToDateTime(startDate);
                sqlParameters[4].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                sqlParameters[11].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_money_codes_order_temp", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);
                totalAmount = sqlParameters[11].Value.ToString();

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetMoneyCode(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

                items.Add(new MoneyCode(0, 0, "", "", "TOPLAM:", "", 0, 0, totalAmount, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "info"));

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchMoneyCodesOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        //public MoneyCodeSweepBackCollection SearchMoneyCodesOrderSweepBackForExcel(string searchText,
        //                                                      DateTime startDate,
        //                                                      DateTime endDate,
        //                                                      int numberOfItemsPerPage,
        //                                                      int currentPageNum,
        //                                                      ref int recordCount,
        //                                                      ref int pageCount,
        //                                                      int orderSelectionColumn,
        //                                                      int descAsc,
        //                                                      int codeStatus,
        //                                                      int generatedType,
        //                                                      ref string totalAmount,
        //                                                      int instutionId)
        //{
        //    MoneyCodeSweepBackCollection items = new MoneyCodeSweepBackCollection();
        //    DataSet dataSet = null;
        //    try
        //    {
        //        SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
        //                                                            new SqlParameter("@pageCount", pageCount), 
        //                                                            new SqlParameter("@searchText", searchText),
        //                                                            new SqlParameter("@startDate", SqlDbType.DateTime),  
        //                                                            new SqlParameter("@endDate", SqlDbType.DateTime),  
        //                                                            new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
        //                                                            new SqlParameter("@currentPageNum", currentPageNum),
        //                                                            new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
        //                                                            new SqlParameter("@orderSelectionDescAsc", descAsc),
        //                                                            new SqlParameter("@codeStatus", codeStatus),
        //                                                            new SqlParameter("@generatedType", generatedType),
        //                                                            new SqlParameter("@totalAmount", SqlDbType.VarChar,20),
        //                                                            new SqlParameter("@instutionId",instutionId)};


        //        sqlParameters[3].Value = Convert.ToDateTime(startDate);
        //        sqlParameters[4].Value = Convert.ToDateTime(endDate);
        //        sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
        //        sqlParameters[11].Direction = ParameterDirection.Output;
        //        dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_money_codes_order_sweep_back", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

        //        recordCount = Convert.ToInt32(sqlParameters[0].Value);
        //        pageCount = Convert.ToInt32(sqlParameters[1].Value);
        //        totalAmount = sqlParameters[11].Value.ToString();

        //        for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
        //        {
        //            items.Add(this.GetMoneyCodeSweepBack(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
        //        }
        //    }
        //    catch (Exception exp)
        //    {
        //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchMoneyCodesOrder");
        //    }
        //    finally
        //    {
        //        base.Dispose();

        //        if (dataSet != null)
        //        {
        //            dataSet.Dispose();
        //            dataSet = null;
        //        }
        //    }
        //    return items;
        //}

        public MoneyCodeSweepBackCollection SearchMoneyCodesOrderSweepBack(string searchText,
                                                      DateTime startDate,
                                                      DateTime endDate,
                                                      int numberOfItemsPerPage,
                                                      int currentPageNum,
                                                      ref int recordCount,
                                                      ref int pageCount,
                                                      int orderSelectionColumn,
                                                      int descAsc,
                                                      int codeStatus,
                                                      int generatedType,
                                                      ref string totalAmount,
                                                      int instutionId,
                                                      int orderType)
        {
            MoneyCodeSweepBackCollection items = new MoneyCodeSweepBackCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@codeStatus", codeStatus),
                                                                    new SqlParameter("@generatedType", generatedType),
                                                                    new SqlParameter("@totalAmount", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@instutionId",instutionId),
                                                                    new SqlParameter("@orderType",orderType)};

                sqlParameters[3].Value = Convert.ToDateTime(startDate);
                sqlParameters[4].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                sqlParameters[11].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_money_codes_order_sweep_back", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);
                totalAmount = sqlParameters[11].Value.ToString();

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetMoneyCodeSweepBack(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

                items.Add(new MoneyCodeSweepBack(0, 0, "", "", "TOPLAM:", "", 0, 0, totalAmount, "", "", "", "", "", "", "info"));
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchMoneyCodesOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        public MoneyCodeCollection SearchMoneyCodesOrderForExcel(string searchText,
                                                        DateTime startDate,
                                                        DateTime endDate,
                                                        int numberOfItemsPerPage,
                                                        int currentPageNum,
                                                        ref int recordCount,
                                                        ref int pageCount,
                                                        int orderSelectionColumn,
                                                        int descAsc,
                                                        int codeStatus,
                                                        int generatedType,
                                                        ref string totalAmount,
                                                        int instutionId,
                                                        string generatedKioskIds,
                                                        string usedKioskIds)
        {
            MoneyCodeCollection items = new MoneyCodeCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@codeStatus", codeStatus),
                                                                    new SqlParameter("@generatedType", generatedType),
                                                                    new SqlParameter("@totalAmount", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@instutionId",instutionId),
                                                                    new SqlParameter("@generatedKioskIds",generatedKioskIds)};
                sqlParameters[3].Value = Convert.ToDateTime(startDate);
                sqlParameters[4].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                sqlParameters[11].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_money_codes_order_temp", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);
                totalAmount = sqlParameters[11].Value.ToString();

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetMoneyCode(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchMoneyCodesOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }


        public StatusNameCollection GetMoneyCodeStatusNames()
        {
            DataSet dataSet = null;
            StatusNameCollection userStatusNames = new StatusNameCollection();

            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[get_money_code_status]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    userStatusNames.Add(this.GetStatusName(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetMoneyCodeStatusNames");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return userStatusNames;
        }

        private StatusName GetStatusName(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new StatusName(Convert.ToInt32(dataRow["Id"]),
                        dataRow["Name"].ToString()
                        );
        }
        public MoneyCodeCollection ListDailyMoneyCodesOrderForExcel(Int64 itemId,
                                  int isUsed,
                                  int orderSelectionColumn,
                                  int descAsc)
        {
            MoneyCodeCollection transactions = new MoneyCodeCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@itemId", itemId),
                                                                    //new SqlParameter("@isUsed", isUsed),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                 };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_daily_money_code_report_detail_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetMoneyCode(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchMoneyCodeExcel");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }

        public MoneyCodeCollection ListDailyMoneyCodes(Int64 itemId,
                                 int isUsed,
                                 int numberOfItemsPerPage,
                                 int currentPageNum,
                                 ref int recordCount,
                                 ref int pageCount,
                                 int orderSelectionColumn,
                                 int descAsc,
                                 ref string TotalAmount)
        {
            MoneyCodeCollection transactions = new MoneyCodeCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@itemId", itemId),  
                                                                    new SqlParameter("@isUsed", isUsed),
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@totalAmount", SqlDbType.VarChar,20)
                                                                 };
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                sqlParameters[8].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_daily_money_code_report_detail", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);
                TotalAmount = sqlParameters[8].Value.ToString();

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetMoneyCode(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREMoneyCode");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }

        public int SaveMoneyCode(int transactionId,
            int userId,
            string codeAmount,
            ref string moneyCode,
            string cellPhone)
        {
            int moneyCodeStatus = 0;
            string customerName = "";
            DataSet dataSet = null;
            int returnCode = 0;
            decimal amount = 0;
             int basamak=0;

            if (!String.IsNullOrEmpty(codeAmount))
            {
                int position = codeAmount.IndexOf('.');
                if (position != -1)
                {
                    amount = Convert.ToDecimal(codeAmount.Substring(0, position));
                    basamak = codeAmount.Length - position - 1;
                    if (basamak == 1)
                    {
                        amount += Convert.ToDecimal(codeAmount.Substring((position + 1), basamak)) / 10;
                    }
                    else if (basamak == 2)
                    {
                        amount += Convert.ToDecimal(codeAmount.Substring((position + 1), basamak)) / 100;
                    }
                }
                else
                {
                    position = codeAmount.IndexOf(',');
                    if (position != -1)
                    {
                        amount = Convert.ToDecimal(codeAmount.Substring(0, position));
                        basamak = codeAmount.Length - position - 1;
                        if (basamak == 1)
                        {
                            amount += Convert.ToDecimal(codeAmount.Substring((position + 1), basamak)) / 10;
                        }
                        else if (basamak == 2)
                        {
                            amount += Convert.ToDecimal(codeAmount.Substring((position + 1), basamak)) / 100;
                        }
                    }
                }
                if (basamak==0)
                {
                    amount = Convert.ToDecimal(codeAmount);
                }
            }
            try
            {
                Int64 onlysend = 1;
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@transactionId", transactionId), 
                                                                    new SqlParameter("@userId", userId), 
                                                                    new SqlParameter("@amount", amount),
                                                                    new SqlParameter("@timing", SqlDbType.DateTime),  
                                                                    new SqlParameter("@returnCode", returnCode),
                                                                    new SqlParameter("@moneyCodeId", onlysend),
                                                                    new SqlParameter("@customerName", SqlDbType.VarChar,200),
                                                                    new SqlParameter("@moneyCodeStatus", moneyCodeStatus),
                                                                    new SqlParameter("@cellPhone", cellPhone)};

                sqlParameters[3].Value = DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss");

                sqlParameters[4].Direction =sqlParameters[5].Direction = ParameterDirection.InputOutput;
                sqlParameters[6].Direction = sqlParameters[7].Direction  = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.generate_money_code_temp", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                moneyCode = sqlParameters[5].Value.ToString();
                returnCode = Convert.ToInt32(sqlParameters[4].Value);
                customerName = sqlParameters[6].Value.ToString();
                moneyCodeStatus = Convert.ToInt32(sqlParameters[7].Value);

                if (moneyCodeStatus == 2 || moneyCodeStatus == 3 || moneyCodeStatus == 6 )
                {
                    string webServiceUrl = Utilities.Utility.GetConfigValue("SMSWebServiceUrl");
                    string mesaj = Utilities.Utility.GetConfigValue("ParaKodMesaj");
                    mesaj = mesaj.Replace("XXX", customerName);
                    mesaj = mesaj.Replace("YYY", moneyCode);
                    mesaj = mesaj.Replace("ZZZ", amount.ToString());
                    Utilities.Utility.SendSMS(webServiceUrl, mesaj, cellPhone);
                    //Utilities.Utility.SendSMS(webServiceUrl, "Sn. " + customerName + ", Pratik Noktada kullanabileceğiniz para kodunuz :" + moneyCode + " \r\nİyi Günler Dileriz.", cellPhone);
                }

            }
            catch (Exception exp)
            {
                returnCode = -1;
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveMoneyCodes");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return returnCode;
        }

        public int UpdateMoneyCodeSMSNumber(Int64 transactionId, Int64 moneyCodeId, int adminUserId)
        {
            int returnCode = -1;
            DataSet dataSet = null;

            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@moneyCodeId", moneyCodeId), 
                                                                    new SqlParameter("@returnCode", returnCode),  
                                                                    new SqlParameter("@adminUserId", adminUserId),
                                                                    new SqlParameter("@datetimeNow", DateTime.Now),
                };

                sqlParameters[1].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.update_money_codes_order_smscount", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);


                returnCode = Convert.ToInt32(sqlParameters[1].Value);
                
            }
            catch (Exception exp)
            {
                returnCode = -1;
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESendMoneyCodes");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return returnCode;
        }

        public int SendMoneyCode(Int64 transactionId, Int64 moneyCodeId, string cellPhone, int adminUserId)
        {
            int returnCode = -1;
            int moneyCodeStatus = 0;
            string customerName = "";
            DataSet dataSet = null;
            string codeAmount = "";
            string moneyCode = "";

            int success = 0;

            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@transactionId", transactionId), 
                                                                    new SqlParameter("@moneyCodeId", moneyCodeId), 
                                                                    new SqlParameter("@codeAmount", SqlDbType.VarChar,200),  
                                                                    new SqlParameter("@customerName", SqlDbType.VarChar,200),
                                                                    new SqlParameter("@moneyCode", SqlDbType.VarChar,200),
                };

                sqlParameters[2].Direction = sqlParameters[3].Direction = sqlParameters[4].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_money_code_sms_info", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                codeAmount = sqlParameters[2].Value.ToString();
                customerName = sqlParameters[3].Value.ToString();
                moneyCode = sqlParameters[4].Value.ToString();
                
                if (codeAmount!="" && customerName!="" && moneyCode!="")
                {
                    string webServiceUrl = Utilities.Utility.GetConfigValue("SMSWebServiceUrl");
                    string mesaj = Utilities.Utility.GetConfigValue("ParaKodMesaj");
                    mesaj = mesaj.Replace("XXX", customerName);
                    mesaj = mesaj.Replace("YYY", moneyCode);
                    mesaj = mesaj.Replace("ZZZ", codeAmount);
                    Utilities.Utility.SendSMS(webServiceUrl, mesaj, cellPhone);
                    //Utilities.Utility.SendSMS(webServiceUrl, "Sn. " + customerName + ", Pratik Noktada kullanabileceğiniz para kodunuz :" + moneyCode + " \r\nİyi Günler Dileriz.", cellPhone);
                    returnCode = 1;
                }

            }
            catch (Exception exp)
            {
                returnCode = -1;
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESendMoneyCodes");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return returnCode;
        }




        public MoneyCodeSMSNumberCollection SearchMoneyCodeSMSNumberOrder(Int64 moneyCodeId,
                                                     int adminUserId)
        {
            MoneyCodeSMSNumberCollection items = new MoneyCodeSMSNumberCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@moneyCodeId", moneyCodeId),
                                                                    new SqlParameter("@adminUserId", adminUserId)};

                //sqlParameters[1].Value = Convert.ToDateTime(startDate);
                //sqlParameters[2].Value = Convert.ToDateTime(endDate);

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_money_code_sms_number", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetMoneyCodeSMSNumber(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchMoneyCodeSMSNumberOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        private MoneyCodeSMSNumber GetMoneyCodeSMSNumber(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new MoneyCodeSMSNumber((dataRow["OperationId"]).ToString()
            , (dataRow["UserName"]).ToString()
            , (dataRow["InsertionDate"]).ToString()
     
            );
        }
      

        private PhoneNumber GetPhoneNumber(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new PhoneNumber(dataRow["phoneNumber"].ToString()
            );
        }

        public string SearchPhoneNumber(Int64 customerId, int operationType)
        {
            
            string pn = "";
            DataSet dataSet = null;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@customerId", customerId), 
                                                                    new SqlParameter("@operationType", operationType),
                                                                    new SqlParameter("@phoneNumber", SqlDbType.VarChar,20)};

                sqlParameters[2].Direction= ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_sms_info_from_customer", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                pn = sqlParameters[2].Value.ToString();
                

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetPhoneNumberFromCustommer");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return pn;
        }
    }
}