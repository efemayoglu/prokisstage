﻿using Microsoft.ApplicationBlocks.Data;
using PRCNCORE.Constants;
using PRCNCORE.KioskConditions;
using PRCNCORE.KioskCommands;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using PRCNCORE.Kiosks;
using PRCNCORE.Transactions;
using PRCNCORE.Parser.Other;
using Newtonsoft.Json;
using PRCNCORE.Input.Source;
 

namespace PRCNCORE.Backend
{
    public class KioskOpr : DBOperator
    {
        public KioskOpr()
        {

        }

        internal KioskOpr(SqlConnection sqlConnection)
            : base(sqlConnection)
        {

        }

        public int AddNewKiosk(string kioskName
                             , string kioskAddress
                             , string kioskIp
                             , int kioskStatus
                             , int updatedUser
                             , string latitude
                             , string longitude
                             , int regionId
                             , string usageFee
                             , int kioskTypeId
                             , int kioskCity
                             , string insertionDate
                             , int acceptorTypeId
                             , string explanation
                             , int connectionType
                             , int cardReaderType
                             , int instutionId
                             , int riskClass
                             , int isCredit
                             , int isMobile
                             , int bankNameId
                             , bool IsInvoiceActive
                             , bool IsPrepaidActive
                             , bool IsExtendedScreenActive
                             , int districtId
                             , int neighborhoodId
            )
        {
            int updateStatus = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[]{  new SqlParameter("@kioskName",kioskName),
                                                                    new SqlParameter("@kioskAddress",kioskAddress),
                                                                    new SqlParameter("@kioskIp",kioskIp),
                                                                    new SqlParameter("@kioskStatus",kioskStatus),
                                                                    new SqlParameter("@updatedUser",updatedUser),
                                                                    new SqlParameter("@latitude",latitude),
                                                                    new SqlParameter("@longitude",longitude),
                                                                    new SqlParameter("@regionId",regionId),
                                                                    new SqlParameter("@usageFee",usageFee),
                                                                    new SqlParameter("@kioskTypeId",kioskTypeId),
                                                                    new SqlParameter("@kioskCity",kioskCity),
                                                                    new SqlParameter("@insertionDate", Convert.ToDateTime(insertionDate)),                                                                    new SqlParameter("@acceptorTypeId",acceptorTypeId),
                                                                    new SqlParameter("@explanation",explanation),
                                                                    new SqlParameter("@connectionTypeId",connectionType),
                                                                    new SqlParameter("@cardReaderTypeId",cardReaderType),
                                                                    new SqlParameter("@instutionId",instutionId),
                                                                    new SqlParameter("@riskClass",riskClass),
                                                                    new SqlParameter("@isCredit",isCredit),
                                                                    new SqlParameter("@isMobile",isMobile),
                                                                    new SqlParameter("@bankNameId",bankNameId),
                                                                    new SqlParameter("@isInvoiceActive",IsInvoiceActive),
                                                                    new SqlParameter("@isPrepaidActive",IsPrepaidActive),
                                                                    new SqlParameter("@isExtendedScreenActive",IsExtendedScreenActive),
                                                                    new SqlParameter("@districtId",districtId),
                                                                    new SqlParameter("@neighborhoodId",neighborhoodId)


                                                                    
                                                                    
                };
                
                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "pk.add_kiosk_24112019", sqlParameters);
                //SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "pk.add_kiosk", sqlParameters);

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREAddNewKiosk");
                updateStatus = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
            }
            finally
            {
                base.Dispose();
            }
            return updateStatus;
        }

        private KioskReferenceNumber GetKioskReferenceNumber(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskReferenceNumber(dataRow["Id"].ToString()
            , dataRow["ReferenceNumber"].ToString()
            );
        }

        public KioskReferenceNumber GetKioskReferenceNumberOrder()
        {
            KioskReferenceNumber items = null;
            DataSet dataSet = null;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { /*new SqlParameter("@errorId", errorId)*/ };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_reference_number", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                if (dataSet.Tables[0].Rows.Count == 1)
                {
                    items = this.GetKioskReferenceNumber(dataSet.Tables[0].Rows[0], dataSet.Tables[0].Columns);
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKioskReferenceNumberOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return items;
        }

        public int AddNewKioskDevice(int kioskId
                            , int deviceId
                            , string portNumber
                            , int updatedUser)
        {
            int updateStatus = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[]{  new SqlParameter("@kioskId",kioskId),
                                                                    new SqlParameter("@deviceId",deviceId),
                                                                    new SqlParameter("@portNumber",portNumber),
                                                                    new SqlParameter("@updatedUser",updatedUser)};

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "pk.add_kiosk_device", sqlParameters);

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREAddNewKioskDevice");
                updateStatus = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
            }
            finally
            {
                base.Dispose();
            }
            return updateStatus;
        }

        public LocalLogCollection SearchLocalLogs(string searchText,
                                      DateTime startDate,
                                      DateTime endDate,
                                      int numberOfItemsPerPage,
                                      int currentPageNum,
                                      ref int recordCount,
                                      ref int pageCount,
                                      int orderSelectionColumn,
                                      int descAsc,
                                      string kioskId,
                                      int processType,
                                      int orderType)
        {
            LocalLogCollection localLogs = new LocalLogCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@processType", processType),
                                                                    new SqlParameter("@orderType", orderType),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@descAsc", descAsc)
                                                                 };

                sqlParameters[3].Value = Convert.ToDateTime(startDate);
                sqlParameters[4].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_local_logs", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    localLogs.Add(this.GetLocalLog(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchLocalLogs");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return localLogs;
        }

        public ErrorLogCollection SearchErrorLogs(string searchText,
                              DateTime startDate,
                              DateTime endDate,
                              int numberOfItemsPerPage,
                              int currentPageNum,
                              ref int recordCount,
                              ref int pageCount,
                              string kioskId,
                              int processType,
                              int orderSelectionColumn,
                              int descAsc,
                              int orderType)
        {
            ErrorLogCollection localLogs = new ErrorLogCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@processType", processType),
                                                                     new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectiondescAsc", descAsc),
                                                                    new SqlParameter("@orderType", orderType)
                                                                 };
                sqlParameters[3].Value = Convert.ToDateTime(startDate);
                sqlParameters[4].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_error_logs", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    localLogs.Add(this.GetErrorLog(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchLocalLogs");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return localLogs;
        }

        //GetCutOffMonitoringOrder
        public ErrorLog GetKioskErrorLogsOrder(int errorId)
        {
            ErrorLog items = null;
            DataSet dataSet = null;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@errorId", errorId) };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_error_logs", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                if (dataSet.Tables[0].Rows.Count == 1)
                {
                    items = this.GetErrorLog(dataSet.Tables[0].Rows[0], dataSet.Tables[0].Columns);
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREKioskCutofMonitoringOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return items;
        }

        //UpdateErrorLogsOrder
        public int UpdateErrorLogsOrder(int errorId,
                                        string explanation,
                                        int expertUserId)
        {
            int resp = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@explanation", explanation),       
                                                                    new SqlParameter("@userId", expertUserId),                                                                  
                                                                    new SqlParameter("@returnCode", 0),
                                                                    new SqlParameter("@errorId", errorId)

                                                                
                                                                    };

                sqlParameters[2].Direction = ParameterDirection.InputOutput;
                resp = Convert.ToInt32(sqlParameters[2].Value);

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[update_error_logs]", sqlParameters);
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREUpdateErrorLogs");
                resp = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
            }
            finally
            {
                base.Dispose();
            }

            return resp;
        }

        public MaintenanceCollection SearchKioskMaintenance(string searchText,
                              DateTime startDate,
                              DateTime endDate,
                              int numberOfItemsPerPage,
                              int currentPageNum,
                              ref int recordCount,
                              ref int pageCount,
                              int orderSelectionColumn,
                              int descAsc,
                              string kioskId,
                              int orderType)
        {
            MaintenanceCollection maintenance = new MaintenanceCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                     new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@orderType", orderType)
                                                                 };

                sqlParameters[3].Value = Convert.ToDateTime(startDate);
                sqlParameters[4].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_kiosk_maintenance", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    maintenance.Add(this.GetMaintenance(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskMaintenance");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return maintenance;
        }

        //public MaintenanceCollection SearchKioskMaintenanceForExcel(string searchText,
        //              DateTime startDate,
        //              DateTime endDate,
        //              string kioskId)
        //{
        //    MaintenanceCollection maintenance = new MaintenanceCollection();
        //    DataSet dataSet = null;
        //    try
        //    {
        //        SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@searchText", searchText),
        //                                                            new SqlParameter("@startDate", SqlDbType.DateTime),  
        //                                                            new SqlParameter("@endDate", SqlDbType.DateTime),  
        //                                                            new SqlParameter("@kioskId", kioskId)
        //                                                         };
        //        sqlParameters[1].Value = Convert.ToDateTime(startDate);
        //        sqlParameters[2].Value = Convert.ToDateTime(endDate);
        //        dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_kiosk_maintenance_for_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

        //        for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
        //        {
        //            maintenance.Add(this.GetMaintenance(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
        //        }
        //    }
        //    catch (Exception exp)
        //    {
        //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskMaintenanceForExcel");
        //    }
        //    finally
        //    {
        //        base.Dispose();

        //        if (dataSet != null)
        //        {
        //            dataSet.Dispose();
        //            dataSet = null;
        //        }
        //    }
        //    return maintenance;
        //}


        public HardwareDeviceCollection SearchKioskDevices(string searchText,
                                                           string kioskId,
                                                           int deviceKindId,
                                                           int numberOfItemsPerPage,
                                                           int currentPageNum,
                                                           ref int recordCount,
                                                           ref int pageCount)
        {
            HardwareDeviceCollection devices = new HardwareDeviceCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@deviceKindId", deviceKindId),  
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                 };
                sqlParameters[3].Direction = sqlParameters[4].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_kiosk_devices", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[3].Value);
                pageCount = Convert.ToInt32(sqlParameters[4].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    devices.Add(this.GetHardwareDevice(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskDevices");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return devices;
        }

        private LocalLog GetLocalLog(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new LocalLog(dataRow["LogId"].ToString()
            , dataRow["LogDate"].ToString()
            , dataRow["LogDetail"].ToString()
            , dataRow["KioskName"].ToString()
            );
        }

        private ErrorLog GetErrorLog(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new ErrorLog(dataRow["InsertionDate"].ToString()
            , dataRow["ProcessDate"].ToString()
            , dataRow["DeviceType"].ToString()
            , dataRow["ErrorType"].ToString()
            , dataRow["ErrorId"].ToString()
            , dataRow["State"].ToString()
            , dataRow["RepairTime"].ToString()
            , dataRow["RepairOperatorName"].ToString()
            , dataRow["RepairOperatorName2"].ToString()
            , dataRow["Explanation"].ToString()
            , dataRow["KioskId"].ToString()
            , dataRow["Id"].ToString()
            , dataRow["IsNotified"].ToString()
            , dataRow["NotificationDate"].ToString()
            , dataRow["NotificationType"].ToString()
            , dataRow["ApprovedUser"].ToString()
            , dataRow["ApprovedDate"].ToString()
            , dataRow["CalculatedTime"].ToString()
                //, dataRow["ErrorLogId"].ToString()
                //, Convert.ToInt32(dataRow["orderSelectionDescAsc"])
                //, Convert.ToInt32(dataRow["orderSelectionColumn"])
            );
        }

        private Maintenance GetMaintenance(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new Maintenance(dataRow["MaintenanceId"].ToString()
            , dataRow["MaintenanceDate"].ToString()
            , dataRow["MaintenanceUser"].ToString()
            , dataRow["KioskName"].ToString()
            , dataRow["Message"].ToString()
            );
        }

        private HardwareDevice GetHardwareDevice(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new HardwareDevice(Convert.ToInt64(dataRow["DeviceId"].ToString())
            , dataRow["DeviceName"].ToString()
            , dataRow["Port"].ToString()
            , dataRow["KioskName"].ToString()
            );
        }

        public int SaveKioskCommand(int kioskId, int institutionId, int createdUserId, int commandTypeId, string commandString, int commandReasonId, string explanation)
        {
            int updateStatus = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[]{  new SqlParameter("@kioskId",kioskId),
                                                                    new SqlParameter("@createdUserId",createdUserId),
                                                                    new SqlParameter("@commandTypeId",commandTypeId),
                                                                    new SqlParameter("@commandString",commandString),
                                                                    new SqlParameter("@commandReasonId",commandReasonId),
                                                                    new SqlParameter("@explanation",explanation),
                                                                     new SqlParameter("@institutionId",institutionId)
                
                };

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "pk.add_kiosk_command", sqlParameters);

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveKioskCommand");
                updateStatus = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
            }
            finally
            {
                base.Dispose();

            }
            return updateStatus;
        }

        //UpdateKioskCommandOrder
        public int UpdateKioskCommandOrder(int commandId, int createdUserId, int commandTypeId, string commandString, int commandReasonId, string explanation)
        {
            int resp = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[]{  new SqlParameter("@returnCode", 0),
                                                                   // new SqlParameter("@kioskId",kioskId),
                                                                    new SqlParameter("@createdUserId",createdUserId),
                                                                    new SqlParameter("@commandTypeId",commandTypeId),
                                                                    new SqlParameter("@commandString",commandString),
                                                                    new SqlParameter("@commandReasonId",commandReasonId),
                                                                    new SqlParameter("@explanation",explanation),
                                                                    new SqlParameter("@commandId",commandId),
                                                                  //  new SqlParameter("@institutionId",institutionId)
                                                                     
                
                };

                sqlParameters[0].Direction = ParameterDirection.InputOutput;
                resp = Convert.ToInt32(sqlParameters[0].Value);

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "pk.update_kiosk_command", sqlParameters);

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREUpdateErrorLogs");
                resp = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
            }
            finally
            {
                base.Dispose();

            }
            return resp;
        }

        public int UpdateKiosk(int kioskId
                             , string kioskName
                             , string kioskAddress
                             , string kioskIp
                             , int kioskStatus
                             , int updatedUser
                             , string latitude
                             , string longitude
                             , int regionId
                             , string usageFee
                             , int kioskTypeId
                             , int kioskCity
                             , string insertionDate
                             , int acceptorTypeId
                             , string explanation
                             , int connectionType
                             , int cardReaderType
                             , int instutionId
                             , int riskClass
                             , int isCredit
                             , int isMobile
                             , int bankNameId
                             , bool IsInvoiceActive
                             , bool IsPrepaidActive
                             , bool IsExtendedScreenActive
                             , bool isCreditCard
                             , bool isMobilePayment
                             , string cameraIp
                             , byte channelNumber
                             , string userName
                             , string password
                             , int districtId
                             , int neighboorhoodId


            )
        
        {
            int updateStatus = 0;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]{  new SqlParameter("@kioskId",kioskId),
                                                                    new SqlParameter("@kioskName",kioskName),
                                                                    new SqlParameter("@kioskAddress",kioskAddress),
                                                                    new SqlParameter("@kioskIp",kioskIp),
                                                                    new SqlParameter("@kioskStatus",kioskStatus),
                                                                    new SqlParameter("@updatedUser",updatedUser),
                                                                    new SqlParameter("@latitude",latitude),
                                                                    new SqlParameter("@longitude",longitude),
                                                                    new SqlParameter("@regionId",regionId),
                                                                    new SqlParameter("@usageFee",usageFee),
                                                                    new SqlParameter("@kioskTypeId",kioskTypeId),
                                                                    new SqlParameter("@kioskCity",kioskCity),
                                                                    new SqlParameter("@insertionDate", Convert.ToDateTime(insertionDate)),
                                                                    new SqlParameter("@acceptorTypeId",acceptorTypeId),
                                                                    new SqlParameter("@explanation",explanation),
                                                                    new SqlParameter("@connectionTypeId",connectionType),
                                                                    new SqlParameter("@cardReaderTypeId",cardReaderType),
                                                                    new SqlParameter("@instutionId",instutionId),
                                                                    new SqlParameter("@riskClass",riskClass),
                                                                    new SqlParameter("@isCredit",isCreditCard),
                                                                    new SqlParameter("@isMobile",isMobilePayment),
                                                                    new SqlParameter("@bankNameId",bankNameId),
                                                                    new SqlParameter("@IsInvoiceActive",IsInvoiceActive),
                                                                    new SqlParameter("@IsPrepaidActive",IsPrepaidActive),
                                                                    new SqlParameter("@IsExtendedScreenActive",IsExtendedScreenActive),
                                                                    new SqlParameter("@cameraIp",cameraIp),
                                                                    new SqlParameter("@channelNumber",channelNumber),
                                                                    new SqlParameter("@userName",userName),
                                                                    new SqlParameter("@password",password),
                                                                    new SqlParameter("@districtId",districtId),
                                                                    new SqlParameter("@neighborhoodId",neighboorhoodId)
                                                                    };
                
                //SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "pk.update_kiosk", sqlParameters);
                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "pk.update_kiosk_24112019", sqlParameters);
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREUpdateKiosk");
            }
            finally
            {
                base.Dispose();

            }
            return updateStatus;
        }

        public int UpdateKioskDevice(Int64 itemId
                             , string portNumber
                             , int updatedUser)
        {
            int updateStatus = 0;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]{  new SqlParameter("@itemId",itemId),
                                                                    new SqlParameter("@portNumber",portNumber),
                                                                    new SqlParameter("@updatedUser",updatedUser)};

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "pk.update_kiosk_device", sqlParameters);
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREUpdateKioskDevice");
            }
            finally
            {
                base.Dispose();

            }
            return updateStatus;
        }


        public bool ManuelKioskEmpty(int expertUserId, int kioskId)
        {
            bool response = false;
            int returnCode = 0;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@expertUserId", expertUserId),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@returnCode", returnCode)
                                                                 };

                sqlParameters[2].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[dbo].[ManuelEmptyKiosk]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                returnCode = Convert.ToInt32(sqlParameters[2].Value);
                if (returnCode == 1)
                    response = true;

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskConditionOrder");

            }
            finally
            {
                base.Dispose();

            }
            return response;
        }

        private KioskDispenserError GetKioskDispenserError(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskDispenserError(Convert.ToInt32(dataRow["KioskIdError"])
            , dataRow["Name"].ToString()
            , dataRow["InstitutionName"].ToString()
            , dataRow["Error"].ToString()
            , dataRow["FailPeriod"].ToString()
            , dataRow["SuccessInsertionDate"].ToString()
            , dataRow["ErrorInsertionDate"].ToString()
            );
        }


        private Sources GetSourceCategoryOrType(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new Sources { 
             Id= Convert.ToInt32(dataRow["Id"])
              ,Explanation = dataRow["Explanation"].ToString()
            };
        }

        private Sources GetSource(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new Sources(
              Convert.ToInt32(dataRow["Id"])
            , dataRow["Explanation"].ToString()
            , Convert.ToDateTime(dataRow["CreatedDate"].ToString())
            , Convert.ToInt32(dataRow["CreatedUserId"].ToString())
            , dataRow["CreatedUserName"].ToString()
            , dataRow["Link"].ToString()
            , Convert.ToInt32(dataRow["SourceCategoryId"].ToString())
      
            , dataRow["SourceCategoryExplanation"].ToString()
            , Convert.ToInt32(dataRow["SourceCategoryTypeId"].ToString())
            
            
            , dataRow["SourceTypeExplanation"].ToString()
            );
        }


        private Input.EmployeeScheduling GetEmployeeScheduling(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new Input.EmployeeScheduling(
                   Convert.ToInt32(dataRow["Id"]),
                    dataRow["Title"].ToString(),
                    Convert.ToDateTime(dataRow["StartDate"]),
                    Convert.ToDateTime(dataRow["EndDate"]),
                    Convert.ToBoolean(dataRow["AllDay"]),
                    dataRow["BackgroundColor"].ToString(),
                    dataRow["BorderColor"].ToString()
                );
            
        }

        private Input.OutputListKiosksReportsNew GetKioskReportNew(DataRow dataRow, DataColumnCollection dataColumns)

        {



            return new Input.OutputListKiosksReportsNew

            {

                CashAmount = Convert.ToString(dataRow["CashAmount"]),

                CreditCardAmount = Convert.ToString(dataRow["CreditCardAmount"]),

                CreditCardCommisionAmount = Convert.ToString(dataRow["CreditCardCommisionAmount"]),

                InstitutionAmount = Convert.ToString(dataRow["InstitutionAmount"]),

                KioskName = Convert.ToString(dataRow["KioskName"]),

                MobilePaymentAmount = Convert.ToString(dataRow["MobilePaymentAmount"]),

                MobilePaymentCommisionAmount = Convert.ToString(dataRow["MobilePaymentCommisionAmount"]),

                NonReverseRecCount = Convert.ToString(dataRow["NonReverseRecCount"]),

                ParaUstu = Convert.ToString(dataRow["ParaUstu"]),

                RecCount = Convert.ToString(dataRow["RecCount"]),

                RecTotal = Convert.ToString(dataRow["RecTotal"]),

                ResultCount = Convert.ToString(dataRow["ResultCount"]),

                NonReverseRecTotal = Convert.ToString(dataRow["NonReverseRecTotal"]),

                ReverseRecCount = Convert.ToString(dataRow["ReverseRecCount"]),

                ReverseRecTotal = Convert.ToString(dataRow["ReverseRecTotal"]),

                SuccessTxnCount = Convert.ToString(dataRow["SuccessTxnCount"]),

                UsageFee = Convert.ToString(dataRow["UsageFee"]),

            };





            /*

             OldValueName,

             END As ApprovedStatusName,

               public string OldValueName { get; set; }

        public string ApprovedStatusName { get; set; }

             */

        }
        private Input.SystemSetting GetSystemSetting(DataRow dataRow, DataColumnCollection dataColumns)
        {
            int approvedUserId = 0;

            int.TryParse(dataRow["ApprovedUserId"].ToString(), out approvedUserId);

            return new Input.SystemSetting(
                   Convert.ToInt32(dataRow["Id"]),
                   Convert.ToString(dataRow["CreatedUserName"]),
                   Convert.ToInt32(dataRow["CreatedUserId"]),
                   Convert.ToString(dataRow["ApprovedUserName"]),
                    approvedUserId,
                   Convert.ToInt32(dataRow["ApprovedStatus"]),
                  Convert.ToInt32(dataRow["OldValue"]),
                   //Convert.ToInt32(dataRow["NewStatus"]),
                   Convert.ToString(dataRow["InstutionName"]),
                   Convert.ToDateTime(dataRow["InsertionDate"]),
                   Convert.ToDateTime(dataRow["ApprovedDate"]),
                   Convert.ToString(dataRow["OldValueName"]),
                  Convert.ToString(dataRow["ApprovedStatusName"])
                );
            /*
             OldValueName,
             END As ApprovedStatusName,
               public string OldValueName { get; set; }
        public string ApprovedStatusName { get; set; }
             */
        }

        //private Kiosk GetKiosk(DataRow dataRow, DataColumnCollection dataColumns)
        //{
        //    //var x = dataRow["BankName"].ToString();
        //    return new Kiosk(Convert.ToInt32(dataRow["KioskId"])
        //    , dataRow["KioskName"].ToString()
        //    , dataRow["KioskAddress"].ToString()
        //    , dataRow["KioskIp"].ToString()
        //    , Convert.ToInt32(dataRow["KioskStatus"].ToString())
        //    , dataRow["StatusName"].ToString()
        //        //, Convert.ToDateTime(dataRow["KioskInsertionDate"].ToString())
        //    , dataRow["KioskInsertionDate"].ToString()
        //    , dataRow["Latitude"].ToString()
        //    , dataRow["Longitude"].ToString()
        //    , Convert.ToInt32(dataRow["CityId"].ToString())
        //    , dataRow["CityName"].ToString()
        //    , Convert.ToInt32(dataRow["RegionId"].ToString())
        //    , dataRow["RegionName"].ToString()
        //    , Convert.ToInt32(dataRow["KioskTypeId"].ToString())
        //    , dataRow["KioskTypeName"].ToString()
        //    , Convert.ToInt32(dataRow["InstutionId"].ToString())
        //    , (dataRow["InstutionName"].ToString())
        //    , Convert.ToInt32(dataRow["ConnectionTypeId"].ToString())
        //    , dataRow["ConnectionType"].ToString()
        //    , Convert.ToInt32(dataRow["CardReaderTypeId"].ToString())
        //    , dataRow["CardReaderType"].ToString()
        //    , dataRow["RiskClass"].ToString()
        //    , dataRow["UsageFee"].ToString()
        //    , dataRow["AppVersion"].ToString()
        //    , dataRow["LastAppAliveTime"].ToString()
        //    , dataRow["Explanation"].ToString()
        //    , Convert.ToInt32(dataRow["Capacity"])
        //    , dataRow["CashBoxAcceptorTypeName"].ToString()
        //    , dataRow["CashBoxAcceptorTypeId"].ToString()
        //    , dataRow["AccomplishedTransactions"].ToString()
        //    , dataRow["IsCreditCard"].ToString()
        //    , dataRow["IsMobilePayment"].ToString()
        //    , dataRow["AppAliveDurationTime"].ToString()
        //    , dataRow["MakbuzStatus"].ToString()
        //    , dataRow["JCMStatus"].ToString()
        //    , dataRow["ClassName"].ToString()
        //    //, Convert.ToInt32(dataRow["BankNameId"].ToString())
        //    //, dataRow["BankName"].ToString()
        //    );
        //}









        private Kiosk GetKioskWithOutStackRatio(DataRow dataRow, DataColumnCollection dataColumns)
        {

            var ex1 = Convert.ToInt32(dataRow["KioskId"]);
            var ex2 = dataRow["KioskName"].ToString();
            var ex3 = dataRow["KioskAddress"].ToString();
            var ex4 = dataRow["KioskIp"].ToString();
            var ex5 = Convert.ToInt32(dataRow["KioskStatus"].ToString());
            var ex6 = dataRow["StatusName"].ToString();
            var ex7 = Convert.ToDateTime(dataRow["KioskInsertionDate"].ToString());
            var ex8 = dataRow["KioskInsertionDate"].ToString();
            var ex9 = dataRow["Latitude"].ToString();
            var ex10 = dataRow["Longitude"].ToString();
            var ex11 = Convert.ToInt32(dataRow["CityId"].ToString());
            var ex12 = dataRow["CityName"].ToString();
            var ex13 = Convert.ToInt32(dataRow["RegionId"].ToString());
            var ex14 = dataRow["RegionName"].ToString();
            var ex15 = Convert.ToInt32(dataRow["KioskTypeId"].ToString());
            var ex16 = dataRow["KioskTypeName"].ToString();
            var ex17 = Convert.ToInt32(dataRow["InstutionId"].ToString());
            var ex18 = (dataRow["InstutionName"].ToString());

            var connectionTypeId = Convert.ToInt32(string.IsNullOrEmpty(dataRow["ConnectionTypeId"].ToString()) ? "0" : dataRow["ConnectionTypeId"].ToString());



            var ex20 = dataRow["ConnectionType"].ToString();
            var ex21 = Convert.ToInt32(dataRow["CardReaderTypeId"].ToString());
            var ex22 = dataRow["CardReaderType"].ToString();
            var ex23 = dataRow["RiskClass"].ToString();
            var ex24 = dataRow["UsageFee"].ToString();
            var ex25 = dataRow["AppVersion"].ToString();
            var ex26 = dataRow["LastAppAliveTime"].ToString();
            var ex27 = dataRow["Explanation"].ToString();
            var ex28 = Convert.ToInt32(dataRow["Capacity"]);
            var ex29 = dataRow["CashBoxAcceptorTypeName"].ToString();
            var ex30 = dataRow["CashBoxAcceptorTypeId"].ToString();
            var ex31 = dataRow["AccomplishedTransactions"].ToString();
            var ex32 = dataRow["IsCreditCard"].ToString();
            var ex33 = dataRow["IsMobilePayment"].ToString();
            var ex34 = dataRow["AppAliveDurationTime"].ToString();
            var ex35 = dataRow["MakbuzStatus"].ToString();
            var ex36 = dataRow["JCMStatus"].ToString();
            var ex37 = dataRow["ClassName"].ToString();
            var ex38 = Convert.ToInt32(dataRow["BankNameId"].ToString());
            var ex39 = dataRow["BankName"].ToString();
       

            var ex40 = dataRow["CameraIp"].ToString();

            var ex42 = dataRow["UserName"].ToString();
            var ex43 = dataRow["Password"].ToString();



            var a1 = dataRow["IsCreditCard"].ToString();
            var a2 = dataRow["IsMobilePayment"].ToString();
            int BankNameId = 0;
            bool IsInvoiceActive, IsPrepaidActive, IsExtendedScreenActive;

            int.TryParse(dataRow["BankNameId"].ToString(), out BankNameId);
            bool.TryParse(dataRow["IsInvoiceActive"].ToString(), out IsInvoiceActive);
            bool.TryParse(dataRow["IsPrepaidActive"].ToString(), out IsPrepaidActive);
            bool.TryParse(dataRow["IsExtendedScreenActive"].ToString(), out IsExtendedScreenActive);

            int creditCard = 0, mobilePayment = 0;
            //var mobilePayment = dataRow["IsMobilePayment"].ToString();
            int.TryParse(dataRow["IsCreditCard"].ToString(), out creditCard);
            int.TryParse(dataRow["IsMobilePayment"].ToString(), out mobilePayment);


            int.TryParse(dataRow["DistrictId"].ToString(), out int out_districtId);
            int.TryParse(dataRow["NeighborhoodId"].ToString(), out int out_neighborhoodId);


            byte.TryParse(dataRow["ChannelNumber"].ToString(), out byte channelNumber);
            if (creditCard == 0)
            {
                if (dataRow["IsCreditCard"].ToString() == "Tanımsız")
                    creditCard = 0;
                else if (dataRow["IsCreditCard"].ToString() == "Aktif")
                    creditCard = 1;
            }
            if (mobilePayment == 0)
            {
                if (dataRow["IsMobilePayment"].ToString() == "Tanımsız")
                    mobilePayment = 0;
                else if (dataRow["IsMobilePayment"].ToString() == "Aktif")
                    mobilePayment = 1;
            }


            return new Kiosk(
              Convert.ToInt32(dataRow["KioskId"])
            , dataRow["KioskName"].ToString()
            , dataRow["KioskAddress"].ToString()
            , dataRow["KioskIp"].ToString()
            , Convert.ToInt32(dataRow["KioskStatus"].ToString())
            , dataRow["StatusName"].ToString()
            //, Convert.ToDateTime(dataRow["KioskInsertionDate"].ToString())
            , dataRow["KioskInsertionDate"].ToString()
            , dataRow["Latitude"].ToString()
            , dataRow["Longitude"].ToString()
            , Convert.ToInt32(dataRow["CityId"].ToString())
            , dataRow["CityName"].ToString()
            , Convert.ToInt32(dataRow["RegionId"].ToString())
            , dataRow["RegionName"].ToString()
            , Convert.ToInt32(dataRow["KioskTypeId"].ToString())
            , dataRow["KioskTypeName"].ToString()
            , Convert.ToInt32(dataRow["InstutionId"].ToString())
            , (dataRow["InstutionName"].ToString())
            , connectionTypeId
            , dataRow["ConnectionType"].ToString()
            , Convert.ToInt32(dataRow["CardReaderTypeId"].ToString())
            , dataRow["CardReaderType"].ToString()
            , dataRow["RiskClass"].ToString()
            , dataRow["UsageFee"].ToString()
            , dataRow["AppVersion"].ToString()
            , dataRow["LastAppAliveTime"].ToString()
            , dataRow["Explanation"].ToString()
            , Convert.ToInt32(dataRow["Capacity"])
            , dataRow["CashBoxAcceptorTypeName"].ToString()
            , dataRow["CashBoxAcceptorTypeId"].ToString()
            , dataRow["AccomplishedTransactions"].ToString()
            , creditCard == 1
            , mobilePayment == 1
            , dataRow["AppAliveDurationTime"].ToString()
            , dataRow["MakbuzStatus"].ToString()
            , dataRow["JCMStatus"].ToString()
            , dataRow["ClassName"].ToString()
            , BankNameId
            , dataRow["BankName"].ToString()
            , IsInvoiceActive, IsPrepaidActive, IsExtendedScreenActive

            , dataRow["CameraIp"].ToString()
            , channelNumber
            , dataRow["UserName"].ToString()
            , dataRow["Password"].ToString(), 
              string.Empty, //stackration
             dataRow["DistrictName"].ToString() ??string.Empty,
             dataRow["NeighborhoodName"].ToString() ?? string.Empty,
             out_districtId, out_neighborhoodId


            //, string.IsNullOrEmpty(dataRow["IsInvoiceActive"].ToString()) ? false : Convert.ToBoolean(dataRow["IsInvoiceActive"].ToString())
            //, string.IsNullOrEmpty(dataRow["IsPrepaidActive"].ToString()) ? false: Convert.ToBoolean(dataRow["IsPrepaidActive"].ToString())
            //, string.IsNullOrEmpty(dataRow["IsExtendedScreenActive"].ToString()) ? false: Convert.ToBoolean(dataRow["IsExtendedScreenActive"].ToString())
            //, Convert.ToBoolean(dataRow["IsInvoiceActive"].ToString())
            //, Convert.ToBoolean(dataRow["IsPrepaidActive"].ToString())
            //, Convert.ToBoolean(dataRow["IsExtendedScreenActive"].ToString())
            );
            //IsInvoiceActive
            //IsPrepaidActive
            //IsExtendedScreenActive

        }





        private Kiosk GetKiosk(DataRow dataRow, DataColumnCollection dataColumns)
        {
            try
            {

                var ex1 = Convert.ToInt32(dataRow["KioskId"]);
                var ex2 = dataRow["KioskName"].ToString();
                var ex3 = dataRow["KioskAddress"].ToString();
                var ex4 = dataRow["KioskIp"].ToString();
                var ex5 = Convert.ToInt32(dataRow["KioskStatus"].ToString());
                var ex6 = dataRow["StatusName"].ToString();
                var ex7 = Convert.ToDateTime(dataRow["KioskInsertionDate"].ToString());
                var ex8 = dataRow["KioskInsertionDate"].ToString();
                var ex9 = dataRow["Latitude"].ToString();
                var ex10 = dataRow["Longitude"].ToString();
                var ex11 = Convert.ToInt32(dataRow["CityId"].ToString());
                var ex12 = dataRow["CityName"].ToString();
                var ex13 = Convert.ToInt32(dataRow["RegionId"].ToString());
                var ex14 = dataRow["RegionName"].ToString();
                var ex15 = Convert.ToInt32(dataRow["KioskTypeId"].ToString());
                var ex16 = dataRow["KioskTypeName"].ToString();
                var ex17 = Convert.ToInt32(dataRow["InstutionId"].ToString());
                var ex18 = (dataRow["InstutionName"].ToString());
                var connectionTypeId = Convert.ToInt32(string.IsNullOrEmpty(dataRow["ConnectionTypeId"].ToString())?"0": dataRow["ConnectionTypeId"].ToString());
                var ex20 = dataRow["ConnectionType"].ToString();
                var ex21 = Convert.ToInt32(dataRow["CardReaderTypeId"].ToString());
                var ex22 = dataRow["CardReaderType"].ToString();
                var ex23 = dataRow["RiskClass"].ToString();
                var ex24 = dataRow["UsageFee"].ToString();
                var ex25 = dataRow["AppVersion"].ToString();
                var ex26 = dataRow["LastAppAliveTime"].ToString();
                var ex27 = dataRow["Explanation"].ToString();
                var ex28 = Convert.ToInt32(dataRow["Capacity"]);
                var ex29 = dataRow["CashBoxAcceptorTypeName"].ToString();
                var ex30 = dataRow["CashBoxAcceptorTypeId"].ToString();
                var ex31 = dataRow["AccomplishedTransactions"].ToString();
                var ex32 = dataRow["IsCreditCard"].ToString();
                var ex33 = dataRow["IsMobilePayment"].ToString();
                var ex34 = dataRow["AppAliveDurationTime"].ToString();
                var ex35 = dataRow["MakbuzStatus"].ToString();
                var ex36 = dataRow["JCMStatus"].ToString();
                var ex37 = dataRow["ClassName"].ToString();
                var ex38 = Convert.ToInt32(dataRow["BankNameId"].ToString());
                var ex39 = dataRow["BankName"].ToString();
                var ex555 = dataRow["StackRatio"].ToString();
                var ex40 = dataRow["CameraIp"].ToString();

                var ex42 = dataRow["UserName"].ToString();
                var ex43 = dataRow["Password"].ToString();

                var a1 = dataRow["IsCreditCard"].ToString();
                var a2 = dataRow["IsMobilePayment"].ToString();
                int BankNameId = 0;
                bool IsInvoiceActive, IsPrepaidActive, IsExtendedScreenActive;

                int.TryParse(dataRow["BankNameId"].ToString(), out BankNameId);
                bool.TryParse(dataRow["IsInvoiceActive"].ToString(), out IsInvoiceActive);
                bool.TryParse(dataRow["IsPrepaidActive"].ToString(), out IsPrepaidActive);
                bool.TryParse(dataRow["IsExtendedScreenActive"].ToString(), out IsExtendedScreenActive);

                int creditCard = 0, mobilePayment = 0;
                //var mobilePayment = dataRow["IsMobilePayment"].ToString();
                int.TryParse(dataRow["IsCreditCard"].ToString(), out creditCard);
                int.TryParse(dataRow["IsMobilePayment"].ToString(), out mobilePayment);


                int.TryParse(dataRow["DistrictId"].ToString(), out int out_districtId);
                int.TryParse(dataRow["NeighborhoodId"].ToString(), out int out_neighborhoodId);


                byte.TryParse(dataRow["ChannelNumber"].ToString(), out byte channelNumber);
                if (creditCard == 0)
                {
                    if (dataRow["IsCreditCard"].ToString() == "Tanımsız")
                        creditCard = 0;
                    else if (dataRow["IsCreditCard"].ToString() == "Aktif")
                        creditCard = 1;
                }
                if (mobilePayment == 0)
                {
                    if (dataRow["IsMobilePayment"].ToString() == "Tanımsız")
                        mobilePayment = 0;
                    else if (dataRow["IsMobilePayment"].ToString() == "Aktif")
                        mobilePayment = 1;
                }

                var k = new Kiosk(
             Convert.ToInt32(dataRow["KioskId"])
           , dataRow["KioskName"].ToString()
           , dataRow["KioskAddress"].ToString()
           , dataRow["KioskIp"].ToString()
           , Convert.ToInt32(dataRow["KioskStatus"].ToString())
           , dataRow["StatusName"].ToString()
           //, Convert.ToDateTime(dataRow["KioskInsertionDate"].ToString())
           , dataRow["KioskInsertionDate"].ToString()
           , dataRow["Latitude"].ToString()
           , dataRow["Longitude"].ToString()
           , Convert.ToInt32(dataRow["CityId"].ToString())
           , dataRow["CityName"].ToString()
           , Convert.ToInt32(dataRow["RegionId"].ToString())
           , dataRow["RegionName"].ToString()
           , Convert.ToInt32(dataRow["KioskTypeId"].ToString())
           , dataRow["KioskTypeName"].ToString()
           , Convert.ToInt32(dataRow["InstutionId"].ToString())
           , (dataRow["InstutionName"].ToString()),
             connectionTypeId
           //, Convert.ToInt32(dataRow["ConnectionTypeId"].ToString())
           , dataRow["ConnectionType"].ToString()
           , Convert.ToInt32(dataRow["CardReaderTypeId"].ToString())
           , dataRow["CardReaderType"].ToString()
           , dataRow["RiskClass"].ToString()
           , dataRow["UsageFee"].ToString()
           , dataRow["AppVersion"].ToString()
           , dataRow["LastAppAliveTime"].ToString()
           , dataRow["Explanation"].ToString()
           , Convert.ToInt32(dataRow["Capacity"])
           , dataRow["CashBoxAcceptorTypeName"].ToString()
           , dataRow["CashBoxAcceptorTypeId"].ToString()
           , dataRow["AccomplishedTransactions"].ToString()
           , creditCard == 1
           , mobilePayment == 1
           , dataRow["AppAliveDurationTime"].ToString()
           , dataRow["MakbuzStatus"].ToString()
           , dataRow["JCMStatus"].ToString()
           , dataRow["ClassName"].ToString()
           , BankNameId
           , dataRow["BankName"].ToString()
           , IsInvoiceActive, IsPrepaidActive, IsExtendedScreenActive

           , dataRow["CameraIp"].ToString()
           , channelNumber
           , dataRow["UserName"].ToString()
           , dataRow["Password"].ToString()
           , dataRow["StackRatio"].ToString()
           , dataRow["DistrictName"].ToString()??string.Empty
           , dataRow["NeighborhoodName"].ToString() ?? string.Empty
           , out_districtId, out_neighborhoodId

           //, string.IsNullOrEmpty(dataRow["IsInvoiceActive"].ToString()) ? false : Convert.ToBoolean(dataRow["IsInvoiceActive"].ToString())
           //, string.IsNullOrEmpty(dataRow["IsPrepaidActive"].ToString()) ? false: Convert.ToBoolean(dataRow["IsPrepaidActive"].ToString())
           //, string.IsNullOrEmpty(dataRow["IsExtendedScreenActive"].ToString()) ? false: Convert.ToBoolean(dataRow["IsExtendedScreenActive"].ToString())
           //, Convert.ToBoolean(dataRow["IsInvoiceActive"].ToString())
           //, Convert.ToBoolean(dataRow["IsPrepaidActive"].ToString())
           //, Convert.ToBoolean(dataRow["IsExtendedScreenActive"].ToString())
           );
                return k;
            }
            catch (Exception ex)
            {

                return null;
            }


            return null;
            //IsInvoiceActive
            //IsPrepaidActive
            //IsExtendedScreenActive

        }

        private KioskConditionDetail GetKioskCashDetail(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskConditionDetail(Convert.ToInt32(dataRow["Id"])
            , Convert.ToInt32(dataRow["KioskId"])
            , dataRow["KioskName"].ToString()
            , dataRow["UpdatedDate"].ToString()
            , dataRow["Amount"].ToString()
            , Convert.ToInt32(dataRow["CashCount"])
            , Convert.ToInt32(dataRow["CoinCount"])
            , Convert.ToInt32(dataRow["RibbonRate"])
            , dataRow["ReceivedAmount"].ToString()
            , Convert.ToInt32(dataRow["CashTypeId"])
            , dataRow["CashTypeName"].ToString()
            , dataRow["AboneNo"].ToString()
            , Convert.ToInt32(dataRow["TransactionId"].ToString())

            );
        }

        private KioskCondition GetKioskCondition(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskCondition(Convert.ToInt32(dataRow["ResultCount"])
            , Convert.ToInt32(dataRow["Id"])
            , Convert.ToInt32(dataRow["KioskId"])
            , dataRow["KioskName"].ToString()
            , dataRow["KioskAddress"].ToString()
                //, dataRow["UpdatedDate"].ToString()
            , dataRow["Amount"].ToString()
            , Convert.ToInt32(dataRow["CashCount"])
            , Convert.ToInt32(dataRow["CoinCount"])
            , Convert.ToInt32(dataRow["RibbonRate"])
            , dataRow["Capacity"].ToString()
            , dataRow["CashBoxAcceptorTypeName"].ToString()
            , dataRow["Explanation"].ToString()
            , dataRow["DispenserCount"].ToString()
            , dataRow["DispenserAmount"].ToString()
            , ""
            );
        }

        private KioskMonitoring GetKioskMonitoring(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskMonitoring(dataRow["KioskName"].ToString()
            , dataRow["LastSuccessTime"].ToString()
            , dataRow["LastAppAliveTime"].ToString()
            , dataRow["SuccessTxnCount"].ToString()
            , dataRow["SuccessTxnAmount"].ToString()
            , dataRow["Address"].ToString()
            , dataRow["Latitude"].ToString()
            , dataRow["Longitude"].ToString()
            , dataRow["ResultCount"].ToString()
            , ""
            );
        }

        private KioskCashEmpty GetKioskCashEmpty(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskCashEmpty(Convert.ToInt32(dataRow["Id"])
            , Convert.ToInt32(dataRow["KioskId"])
            , dataRow["KioskName"].ToString()
            , dataRow["ExpertUserId"].ToString()
            , dataRow["ExpertUserName"].ToString()
            , dataRow["UpdatedDate"].ToString()
            , dataRow["Amount"].ToString()
            , Convert.ToInt32(dataRow["CashCount"])
            , Convert.ToInt32(dataRow["RibbonRate"])
            , Convert.ToInt32(dataRow["CoinCount"])
            , dataRow["SecurityCode"].ToString()
            , dataRow["ReferenceNo"].ToString()
            , ""
            );
        }

        private KioskFillCashBox GetKioskFillCashBox(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskFillCashBox(Convert.ToInt32(dataRow["Id"])
            , Convert.ToInt32(dataRow["KioskId"])
            , dataRow["KioskName"].ToString()
            , dataRow["ExpertUserId"].ToString()
            , dataRow["ExpertUserName"].ToString()
            , dataRow["UpdatedDate"].ToString()
            , dataRow["Amount"].ToString()
            , ""
            );
        }


        private KioskCommandTypeName GetKioskCommandTypeName(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskCommandTypeName(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }

        private KioskCommandReasonName GetKioskCommandReasonName(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskCommandReasonName(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }

        private KioskName GetKioskName(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskName(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }

        private ReferenceStatus GetReferenceStatus(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new ReferenceStatus(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }

        private KioskDetail GetKioskDetail(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskDetail(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString()
            , dataRow["Latitude"].ToString()
            , dataRow["Longitude"].ToString()
            , dataRow["Address"].ToString()
            );
        }

        private InstutionName GetInstutionName(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new InstutionName(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }


        private BankName GetBankName(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new BankName(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }
        private CompleteStatusName GetCompleteStatusName(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new CompleteStatusName(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }

        private RiskClass GetRiskClass(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new RiskClass(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }


        private ProcessType GetProcessType(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new ProcessType(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }

        private TxnStatusName GetTxnStatusName(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new TxnStatusName(dataRow["Id"].ToString()
            , dataRow["Name"].ToString());
        }

        private InstutionOperationName GetInstutionOperationName(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new InstutionOperationName(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }

        private CardTypeName GetCardTypeName(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new CardTypeName(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }

        private TransactionConditionName GetTransactionConditionName(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new TransactionConditionName(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }


        private KioskTypeName GetKioskTypeName(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskTypeName(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }

        private EmptyKioskName GetEmptyKioskName(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new EmptyKioskName(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString()
            , Convert.ToInt32(dataRow["EmptyKioskId"]));
        }

        private KioskCommand GetKioskCommand(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskCommand(Convert.ToInt32(dataRow["CommandId"])
            , dataRow["KioskName"].ToString()
            , dataRow["CreatedUserName"].ToString()
            , dataRow["CreatedDate"].ToString()
            , dataRow["CommandType"].ToString()
            , dataRow["CommandString"].ToString()
            , dataRow["ExecutedDate"].ToString()
            , dataRow["Status"].ToString()
            , dataRow["CommandReason"].ToString()
            , dataRow["Explanation"].ToString()
            , dataRow["CommandReasonId"].ToString()
            , dataRow["CommandTypeId"].ToString()
            , dataRow["KioskId"].ToString()
            , dataRow["InstitutionName"].ToString()
            , dataRow["StatusId"].ToString()
            );
        }

        private KioskCashCount GetKioskCashCount(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskCashCount(Convert.ToInt32(dataRow["KioskId"])
            , dataRow["KioskName"].ToString()
            , dataRow["CashTypeName"].ToString()
            , Convert.ToInt32(dataRow["CashCount"])
            , (dataRow["CashTypeValue"].ToString())
            , dataRow["CashSum"].ToString()
        );
        }

        private KioskTotalCashCount GetKioskTotalCashCount(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskTotalCashCount(
                dataRow["CashTypeName"].ToString()
                , Convert.ToInt32(dataRow["CashCount"])
                // , (dataRow["CashTypeValue"].ToString())
        );
        }

        private string ColorByValue(string value, string category)
        {
            try
            {

                DateTime valueTime = Convert.ToDateTime(value);
                if (category == "HOUR")
                {
                    switch ((DateTime.Now - valueTime).Hours)
                    {
                        case 0: return "success";
                        case 1: return "warning";
                        case 2: return "danger";
                        default: return "danger";
                    }
                }
                else if(category == "MINUTE")
                {
                    int res=(DateTime.Now - valueTime).Minutes;
                    switch (res)
                    {
                        case 0: return "success";
                        case 1: return "success";
                        case 2: return "success";
                        case 3: return "success";
                        default: return "danger";
                    }
                }

                return "";
            }
            catch (Exception)
            {
                return "";
            }
        } 

        private KioskCashBoxCount GetKioskCashBoxCount(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskCashBoxCount(Convert.ToInt32(dataRow["KioskId"])
            , dataRow["KioskName"].ToString()
            , dataRow["CashTypeName"].ToString()
            , Convert.ToInt32(dataRow["CashCount"]));
        }

        public Kiosk GetKiosk(int kioskId)
        {
            Kiosk kiosk = null;
            DataSet dataSet = null;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@kioskId", kioskId) };
                //[get_kiosk_24112019]
        dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_24112019", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);
        //dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                dataSet.Tables[0].Columns.Add("ClassName", typeof(string));

                if (dataSet.Tables[0].Rows.Count == 1)
                {
                    kiosk = this.GetKioskWithOutStackRatio(dataSet.Tables[0].Rows[0], dataSet.Tables[0].Columns);
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKiosk");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return kiosk;
        }

        public KioskDispenserErrorCollection SearchKioskDispenserErrorsOrder(string searchText,
                                              int numberOfItemsPerPage,
                                              int currentPageNum,
                                              ref int recordCount,
                                              ref int pageCount,
                                              int orderSelectionColumn,
                                              int descAsc,
                                              string kioskId,
                                              int orderType,
                                              int instutionId,
                                              int errorId
                                                )
        {
            KioskDispenserErrorCollection kiosks = new KioskDispenserErrorCollection();
            DataSet dataSet = null;
            try
            {
                int returnCode = 0;
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@returnCode", returnCode)

                                                                 };

                sqlParameters[0].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_lcdm_errors_by_kiosk", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                //recordCount = Convert.ToInt32(sqlParameters[0].Value);
                //pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    kiosks.Add(this.GetKioskDispenserError(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));                 
                }

               
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKiosksOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return kiosks;
        }

        //public KioskCollection SearchKiosksOrder(string searchText,
        //                                      int numberOfItemsPerPage,
        //                                      int currentPageNum,
        //                                      ref int recordCount,
        //                                      ref int pageCount,
        //                                      int orderSelectionColumn,
        //                                      int descAsc,
        //                                      string kioskId,
        //                                      int kioskTypeId,
        //                                      ref DateTime serverTime,
        //                                      int orderType,
        //                                      int instutionId,
        //                                      int connectionId,
        //                                      int cardReaderId,
        //                                      int statusId,
        //                                      int regionId,
        //                                      int cityId,
        //                                      int riskId
        //                                        )
        //{
        //    KioskCollection kiosks = new KioskCollection();
        //    DataSet dataSet = null;
        //    try
        //    {
        //        SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
        //                                                            new SqlParameter("@pageCount", pageCount), 
        //                                                            new SqlParameter("@searchText", searchText),  
        //                                                            new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
        //                                                            new SqlParameter("@currentPageNum", currentPageNum),
        //                                                            new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
        //                                                            new SqlParameter("@orderSelectionDescAsc", descAsc),
        //                                                            new SqlParameter("@kioskTypeId", kioskTypeId),
        //                                                            new SqlParameter("@serverTime", serverTime),
        //                                                            new SqlParameter("@orderType",orderType ),
        //                                                            new SqlParameter("@instutionId",instutionId),
        //                                                            new SqlParameter("@connectionId",connectionId),
        //                                                            new SqlParameter("@cardReaderId",cardReaderId),
        //                                                            new SqlParameter("@statusId",statusId),
        //                                                            new SqlParameter("@regionId",regionId),
        //                                                            new SqlParameter("@cityId",cityId),
        //                                                            new SqlParameter("@riskId",riskId),
        //                                                             //new SqlParameter("@bankNamesId",0),
        //                                                            new SqlParameter("@searchKioskId",kioskId)

        //                                                         };
        //        sqlParameters[8].Direction = ParameterDirection.Output;
        //        sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
        //        dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_kiosks_order_temp", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

        //        recordCount = Convert.ToInt32(sqlParameters[0].Value);
        //        pageCount = Convert.ToInt32(sqlParameters[1].Value);
        //        serverTime = Convert.ToDateTime(sqlParameters[8].Value);


        //        dataSet.Tables[0].Columns.Add("ClassName", typeof(string));

        //        //var data126 = dataSet.Tables[0].Rows[126];
        //        //var data127 = dataSet.Tables[0].Rows[127];
        //        var exe = 0;
        //        for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
        //        {
        //            ClassNameParsers parser = new ClassNameParsers();
        //            List<ClassName> classNameList = new List<ClassName>();
        //            classNameList.Add(new ClassName("LastAppAliveTime", ColorByValue(dataSet.Tables[0].Rows[i][20].ToString(), "MINUTE")));
        //            classNameList.Add(new ClassName("AccomplishedTransactions", ColorByValue(dataSet.Tables[0].Rows[i][25].ToString(), "HOUR")));
        //            parser.ClassName = classNameList;
        //            exe++;

        //            var obj = dataSet.Tables[0].Rows[i];
        //            var obj2 = dataSet.Tables[0].Columns;
        //            var obj3 = JsonConvert.SerializeObject(parser);

        //            dataSet.Tables[0].Rows[i][36] = JsonConvert.SerializeObject(parser);//ClassName Prop
        //            kiosks.Add(this.GetKiosk(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
        //        }
        //    }
        //    catch (Exception exp)
        //    {
        //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKiosksOrder");

        //    }
        //    finally
        //    {
        //        base.Dispose();

        //        if (dataSet != null)
        //        {
        //            dataSet.Dispose();
        //            dataSet = null;
        //        }
        //    }
        //    return kiosks;
        //}


        public List<Sources> SearchSourceTypeOrder(int id = 0)
        {
            //KioskCollection kiosks = new KioskCollection();
            List<Sources> sources = new List<Sources>();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {  new SqlParameter("@Id", id)  };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(),
                CommandType.StoredProcedure, "dbo.get_source_type",
                Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    sources.Add(this.GetSourceCategoryOrType(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKiosksOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return sources;
        }




        public List<Sources> SearchSourceCategoryTypeOrder(int id = 0)
        {
            //KioskCollection kiosks = new KioskCollection();
            List<Sources> sources = new List<Sources>();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {  new SqlParameter("@Id", id)  };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(),
                CommandType.StoredProcedure, "dbo.get_source_categoryType",
                Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    sources.Add(this.GetSourceCategoryOrType(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKiosksOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return sources;
        }



        public List<Sources> SearchSourceCategoryOrder(int id = 0)
        {
            //KioskCollection kiosks = new KioskCollection();
            List<Sources> sources = new List<Sources>();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {  new SqlParameter("@Id", id)  };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(),
                CommandType.StoredProcedure, "dbo.get_source_category",
                Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    sources.Add(this.GetSourceCategoryOrType(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKiosksOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return sources;
        }

        //SearchEmployeeScheduling



        public bool SaveSystemSettingOrder(int id,long adminUserId, int instutionId, int newStatus, int approvedUserId, string screenMessage)
        {
            DataSet dataSet = null;
            var con = false;
            try
            {
                SqlParameter[] sqlParameters1 = new SqlParameter[]
                {
                    new SqlParameter("@ApprovedUserId", adminUserId),
                    new SqlParameter("@InstutionId", instutionId),
                    new SqlParameter("@State", newStatus),
                    new SqlParameter("@ScreenMessage", screenMessage),
                };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(),
                  CommandType.StoredProcedure, "dbo.save_SystemSetting",
                  Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters1);
 
                con = true;

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKiosksOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return con;
        }
        

        public bool DeleteSystemSettingOrder(int id, long adminUserId)
        {
            DataSet dataSet = null;
            var con = false;

            try
            {
                SqlParameter[] sqlParameters1 = new SqlParameter[]
                {
                    new SqlParameter("@Id", id),
                    new SqlParameter("@UserId", adminUserId),
                };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(),
                  CommandType.StoredProcedure, "dbo.delete_SystemSetting",
                  Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters1);

                con = true;
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKiosksOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return con;
        }
        public bool UpdateSystemSettingOrder(int id, long adminUserId)
        {
            //KioskCollection kiosks = new KioskCollection();
            DataSet dataSet = null;
            var con = false;

            try
            {
                SqlParameter[] sqlParameters1 = new SqlParameter[]
                {
                    new SqlParameter("@Id", id),
                    new SqlParameter("@ApprovedUserId", adminUserId)
                };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(),
                  CommandType.StoredProcedure, "dbo.update_SystemSetting",
                  Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters1);

                con = true;
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKiosksOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return con;
        }

        public List<Input.OutputListKiosksReportsNew> SearchListKiosksReportsNewsOrder(Input.InputListKiosksReportsNew input, ref int recordCound, ref int pageCount)

        {
            var data = new List<Input.OutputListKiosksReportsNew>();
            DataSet dataSet = null;
            recordCound = 0;
            pageCount = 0;
            try

            {
      
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@StartDate", SqlDbType.DateTime),
                    new SqlParameter("@EndDate", SqlDbType.DateTime),

                    new SqlParameter("@searchText", input.SearchText),

                    new SqlParameter("@searchKioskId ", input.KioskId),

                    new SqlParameter("@numberOfItemsPerPage", input.NumberOfItemsPerPage),

                    new SqlParameter("@orderSelectionColumn", input.OrderSelectionColumn),

                    new SqlParameter("@orderSelectionDescAsc", input.DescAsc),

                    new SqlParameter("@instutionId", input.InstutionId),

                    new SqlParameter("@recordCount", recordCound),

                    new SqlParameter("@pageCount",  pageCount),

                    new SqlParameter("@orderType", input.OrderType),

                    new SqlParameter("@paymentType",input.PaymentType),

                    new SqlParameter("@processType ",input.ProcessType),

                    new SqlParameter("@bankType", input.BankNameId),

                    new SqlParameter("@currentPageNum", input.CurrentPageNum),

                };

                sqlParameters[0].Value = input.StartDate;

                sqlParameters[1].Value = input.EndDate;

           

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(),
                    CommandType.StoredProcedure, "pk.get_code_report_wo_code",
                    Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);


                //efee
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)

                {

                    data.Add(this.GetKioskReportNew(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));

                }

            }

            catch (Exception exp)

            {

                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKiosksOrder");

            }

            finally

            {

                base.Dispose();



                if (dataSet != null)

                {

                    dataSet.Dispose();

                    dataSet = null;

                }

            }

            return data;

        }


        public bool SaveEmployeeSchedulingOrder(int id, string title, bool allDay, string backgrounColor, string borderColor, DateTime startDate, DateTime endDate,bool isDeleted)
        {
            //KioskCollection kiosks = new KioskCollection();
            List<Input.EmployeeScheduling> employess = new List<Input.EmployeeScheduling>();
            DataSet dataSet = null;
            var con = false;

            try
            {


                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@Id", id),
                    new SqlParameter("@Title", title),
                    new SqlParameter("@Allday", allDay),
                    new SqlParameter("@BackgroundColor", backgrounColor),
                    new SqlParameter("@BorderColor", borderColor),
                    new SqlParameter("@StartDate", startDate),
                    new SqlParameter("@EndDate", endDate),
                    new SqlParameter("@isDeleted", isDeleted)

                };
 
                    dataSet = SqlHelper.ExecuteDataset(base.GetConnection(),
                      CommandType.StoredProcedure, "dbo.save_EmployeeScheduling",
                      Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);


                con = true;

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKiosksOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return con;
        }

        public List<Input.SystemSetting> SearchSystemSettingOrder(DateTime startDate, DateTime endDate)
        {
            List<Input.SystemSetting> employess = new List<Input.SystemSetting>();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@StartDate", SqlDbType.DateTime),
                    new SqlParameter("@EndDate", SqlDbType.DateTime),
                };
                sqlParameters[0].Value = startDate;
                sqlParameters[1].Value = endDate;

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(),
                    CommandType.StoredProcedure, "dbo.get_SystemSetting",
                    Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    employess.Add(this.GetSystemSetting(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKiosksOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return employess;
        }

        public List<Input.EmployeeScheduling> SearchEmployeeSchedulingOrder(int id, DateTime startDate, DateTime endDate)
        {
            //KioskCollection kiosks = new KioskCollection();
            List<Input.EmployeeScheduling> employess = new List<Input.EmployeeScheduling>();
            DataSet dataSet = null;

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@StartDate", SqlDbType.DateTime),
                    new SqlParameter("@EndDate", SqlDbType.DateTime),
                    new SqlParameter("@CityId", 0),
                    new SqlParameter("@Id", id)
                };
                sqlParameters[0].Value = startDate;
                sqlParameters[1].Value = endDate;

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(),
                    CommandType.StoredProcedure, "dbo.get_EmployeeScheduling",
                    Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {

                    employess.Add(this.GetEmployeeScheduling(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }


            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKiosksOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return employess;
        }

        public List<Sources> SearchSourceOrder(int categoryId, int id=-1)
        {
            //KioskCollection kiosks = new KioskCollection();
            List<Sources> sources = new List<Sources>(); 
            DataSet dataSet = null;

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] 
                { new SqlParameter("@categoryId", categoryId),
                new SqlParameter("@Id", id)};

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(),
                    CommandType.StoredProcedure, "dbo.get_sources_by_categoryIdOrId", 
                    Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {

                    sources.Add(this.GetSource(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }


            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKiosksOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return sources;
        }

        public bool UpdateSourceOrder(int id,string explanation, string link, int sourceCategoryId, int sourceCategoryTypeId, int sourceTypeId)
        {
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@Id",id),

                    new SqlParameter("@Explanation", explanation),
                    new SqlParameter("@Link", link),
                    new SqlParameter("@SourceCategoryId", sourceCategoryId),
                    new SqlParameter("@SourceCategoryTypeId", sourceCategoryTypeId),
                    new SqlParameter("@SourceTypeId", sourceTypeId),
                 
                };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(),
                    CommandType.StoredProcedure, "dbo.update_sources",
                    Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKiosksOrder");
                return false;
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return true;

        }

        public bool AddSourceOrder(string explanation, int createdUserId, string createdUserName, string link, int sourceCategoryId, int sourceCategoryTypeId, int sourceTypeId)
        {
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@Explanation", explanation),
                    new SqlParameter("@CreatedUserId", createdUserId),
                    new SqlParameter("@CreatedUserName", createdUserName),
                    new SqlParameter("@Link", link),
                    new SqlParameter("@SourceCategoryId", sourceCategoryId),
                    new SqlParameter("@SourceCategoryTypeId", sourceCategoryTypeId),
                    new SqlParameter("@SourceTypeId", sourceTypeId),

                };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(),
                    CommandType.StoredProcedure, "dbo.add_sources",
                    Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKiosksOrder");
                return false;
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return true;

        }












        public KioskCollection SearchKiosksOrder(string searchText,
                                        int numberOfItemsPerPage,
                                        int currentPageNum,
                                        ref int recordCount,
                                        ref int pageCount,
                                        int orderSelectionColumn,
                                        int descAsc,
                                        string kioskId,
                                        int kioskTypeId,
                                        ref DateTime serverTime,
                                        int orderType,
                                        int instutionId,
                                        int connectionId,
                                        int cardReaderId,
                                        int statusId,
                                        int regionId,
                                        int cityId,
                                        int riskId,
                                        int bankNameId
        , string cameraIp, byte channelNumber, string userName, string password

                                          )
        {
            KioskCollection kiosks = new KioskCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount),
                                                                    new SqlParameter("@pageCount", pageCount),
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage),
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@kioskTypeId", kioskTypeId),
                                                                    new SqlParameter("@serverTime", serverTime),
                                                                    new SqlParameter("@orderType",orderType ),
                                                                    new SqlParameter("@instutionId",instutionId),
                                                                    new SqlParameter("@connectionId",connectionId),
                                                                    new SqlParameter("@cardReaderId",cardReaderId),
                                                                    new SqlParameter("@statusId",statusId),
                                                                    new SqlParameter("@regionId",regionId),
                                                                    new SqlParameter("@cityId",cityId),
                                                                    new SqlParameter("@riskId",riskId),
                                                                    new SqlParameter("@searchKioskId",kioskId),
                                                                    new SqlParameter("@bankNamesId",bankNameId),

                                                                    //new SqlParameter("@cameraIp",cameraIp),
                                                                    //new SqlParameter("@channelNumber",channelNumber),
                                                                    //new SqlParameter("@userName",userName),
                                                                    //new SqlParameter("@password",password),
                                                                 };

                sqlParameters[8].Direction = ParameterDirection.Output;
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_kiosks_order_temp_24112019", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);
                //dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_kiosks_order_temp", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);
                serverTime = Convert.ToDateTime(sqlParameters[8].Value);


                dataSet.Tables[0].Columns.Add("ClassName", typeof(string));


                 

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    ClassNameParsers parser = new ClassNameParsers();
                    List<ClassName> classNameList = new List<ClassName>();
                    classNameList.Add(new ClassName("LastAppAliveTime", ColorByValue(dataSet.Tables[0].Rows[i][20].ToString(), "MINUTE")));
                    classNameList.Add(new ClassName("AccomplishedTransactions", ColorByValue(dataSet.Tables[0].Rows[i][25].ToString(), "HOUR")));
                    parser.ClassName = classNameList;
                    var columnCOUNT = dataSet.Tables[0].Columns.Count;

                    dataSet.Tables[0].Rows[i][columnCOUNT-1] = JsonConvert.SerializeObject(parser);
                    //dataSet.Tables[0].Rows[i][41] = JsonConvert.SerializeObject(parser);
                    kiosks.Add(this.GetKiosk(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }


            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKiosksOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return kiosks;
        }


        //public KioskCollection SearchKiosksOrderForExcel(string searchText,
        //                                      int orderSelectionColumn,
        //                                      int descAsc,
        //                                      int kioskTypeId)
        //{
        //    KioskCollection kiosks = new KioskCollection();
        //    DataSet dataSet = null;
        //    try
        //    {
        //        SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@searchText", searchText),  
        //                                                            new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
        //                                                            new SqlParameter("@orderSelectionDescAsc", descAsc),
        //                                                            new SqlParameter("@kioskTypeId", kioskTypeId)
        //                                                         };

        //        dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_kiosks_order_for_excel_temp", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

        //        for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
        //        {
        //            kiosks.Add(this.GetKiosk(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
        //        }
        //    }
        //    catch (Exception exp)
        //    {
        //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKiosksOrderForExcel");

        //    }
        //    finally
        //    {
        //        base.Dispose();

        //        if (dataSet != null)
        //        {
        //            dataSet.Dispose();
        //            dataSet = null;
        //        }
        //    }
        //    return kiosks;
        //}

        public KioskConditionCollection SearchKioskConditionOrder(string kioskId,
                                              string searchText,
                                              int numberOfItemsPerPage,
                                              int currentPageNum,
                                              ref int recordCount,
                                              ref int pageCount,
                                              ref string TotalAmount,
                                              ref string TotalCashCount,
                                              ref string TotalCoinCount,
                                              ref string TotalDispenserAmount,
                                              ref string TotalDispenserCount,
                                              int orderSelectionColumn,
                                              int descAsc,
                                              int kioskType,
                                              int orderType)
        {
            KioskConditionCollection kiosks = new KioskConditionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@TotalAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalCashCount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalCoinCount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@kioskType", kioskType),
                                                                    new SqlParameter("@orderType", orderType),
                                                                    new SqlParameter("@TotalDispenserAmount", SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalDispenserCount", SqlDbType.VarChar,200),
                                                                 };

                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                sqlParameters[7].Direction = sqlParameters[8].Direction = sqlParameters[9].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[search_kiosk_condition_order]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);
                TotalAmount = sqlParameters[7].Value.ToString();
                TotalCashCount = sqlParameters[8].Value.ToString();
                TotalCoinCount = sqlParameters[9].Value.ToString();

                //TotalDispenserAmount = sqlParameters[13].Value.ToString();
                //TotalDispenserCount = sqlParameters[14].Value.ToString();

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    kiosks.Add(this.GetKioskCondition(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

                kiosks.Add(new KioskCondition(kiosks.Count + 1, kiosks.Count + 1, 0, "TOPLAM", "", TotalAmount, Convert.ToInt32(TotalCashCount), Convert.ToInt32(TotalCoinCount), 0, "", "", "", "", "","info"));
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskConditionOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return kiosks;
        }

        public int UpdateKioskReferenceSystemOrder(int id, int referenceNo, long adminUserId)
        {
            //KioskGetReferenceSystem item = new KioskGetReferenceSystem();
            DataSet dataSet = null;
            int success = 0;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] {
                                                                    new SqlParameter("@returnCode", success),
                                                                    new SqlParameter("@Id", id),
                                                                    new SqlParameter("@referenceNo", referenceNo),
                                                                    new SqlParameter("@UserId", adminUserId)
                                                                 };

                sqlParameters[0].Direction = ParameterDirection.Output;

                int a = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[update_kiosk_reference_system]", sqlParameters);

                success = Convert.ToInt32(sqlParameters[0].Value);
            }


            catch (Exception exp)
            {
              
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREListKioskReferenceSystemOrder");
                return success;
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return success;

        }

        public KioskGetReferenceSystem GetKioskReferenceSystemOrder(int referenceNo)
        {
            KioskGetReferenceSystem item = new KioskGetReferenceSystem();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] {
                                                                    new SqlParameter("@referenceNo", referenceNo)
                                                                 };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), 
                    CommandType.StoredProcedure, "pk.get_kiosk_reference_system", 
                    Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);
 

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    item = GetKioskReferenceSystemWithoutInstution(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns);
                }
            }


            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREListKioskReferenceSystemOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return item;

        }

        public KioskReferenceSystemCollection SearchKioskReferenceSystemOrder(string searchText,
                                                                                      DateTime startDate,
                                                                                      DateTime endDate,
                                                                                      int referenceStatus,
                                                                                      int numberOfItemsPerPage,
                                                                                      int currentPageNum,
                                                                                      ref int recordCount,
                                                                                      ref int pageCount,
                                                                                      int orderSelectionColumn,
                                                                                      int descAsc,
                                                                                      int institutionId,
                                                                                      string kioskId,
                                                                                     
                                                                                      int orderType)
        {
            KioskReferenceSystemCollection items = new KioskReferenceSystemCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] {
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@searchText", searchText),  
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@institutionId", institutionId),
                                                                    new SqlParameter("@searchKioskId", kioskId),
                                                                    new SqlParameter("@orderType", orderType),
                                                                    new SqlParameter("@referenceStatus", referenceStatus)
                                                                    
                                                                 };


                sqlParameters[0].Value = Convert.ToDateTime(startDate);
                sqlParameters[1].Value = Convert.ToDateTime(endDate);
                sqlParameters[3].Direction = sqlParameters[4].Direction = ParameterDirection.InputOutput;


                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_kiosk_reference_system", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[4].Value);
                pageCount = Convert.ToInt32(sqlParameters[3].Value);
 

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetListKioskReferenceSystem(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }


            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREListKioskReferenceSystemOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }


        private KioskReferenceSystem GetListKioskReferenceSystem(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskReferenceSystem(
            dataRow["Id"].ToString(),
            dataRow["KioskName"].ToString()
            //, dataRow["InstitutionName"].ToString()
            , dataRow["InsertionDate"].ToString()
            , dataRow["Status"].ToString()  
            , dataRow["Explanation"].ToString()   
            , dataRow["CreatedUser"].ToString()
            , dataRow["ReferenceNo"].ToString()   
            );
        }



        private KioskGetReferenceSystem GetKioskReferenceSystem(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskGetReferenceSystem(
              dataRow["Id"].ToString()
            , dataRow["KioskId"].ToString()
            , dataRow["InstitutionId"].ToString()
            , dataRow["InsertionDate"].ToString()
            , dataRow["Status"].ToString()
            , dataRow["Explanation"].ToString()
            , dataRow["UserId"].ToString()
            , dataRow["ReferenceNo"].ToString()
            , dataRow["DesmerId"].ToString());
        }

        private KioskGetReferenceSystem GetKioskReferenceSystemWithoutInstution(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskGetReferenceSystem(
              dataRow["Id"].ToString()
            , dataRow["KioskId"].ToString()
            , dataRow["InsertionDate"].ToString()
            , dataRow["Status"].ToString()
            , dataRow["Explanation"].ToString()
            , dataRow["UserId"].ToString()
            , dataRow["ReferenceNo"].ToString()
            , dataRow["DesmerId"].ToString());
        }


        public int SaveKioskReferenceSystemOrder(int adminUserId,
                                          string referenceNo,
                                          string kioskId,
                                           string explanation,
                                           string desmerId )
        {
            int success = 0;
            try
            {



                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@result",success),
                                                                    new SqlParameter("@adminUserId", adminUserId),
                                                                    new SqlParameter("@referenceNo", referenceNo),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@explanation", explanation),
                                                                    new SqlParameter("@desmerId", desmerId)


                };

                sqlParameters[0].Direction = ParameterDirection.Output;

                int a = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[add_kiosk_reference_system]", sqlParameters);

                success = Convert.ToInt32(sqlParameters[0].Value);
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveSettings");

            }
            finally
            {
                base.Dispose();
            }
            return success;

        }


        //UpdateKioskConditionDispenserCashCountOrder
        public int UpdateKioskConditionDispenserCashCountOrder(string kioskId,
                               string dispenser,
                               string dispenserAmount)
        {
            int resp = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@kioskId", kioskId),       
                                                                    //new SqlParameter("@userId", expertUserId),                                                                  
                                                                    new SqlParameter("@returnCode", 0),
                                                                    new SqlParameter("@dispenser", dispenser),
                                                                    new SqlParameter("@dispenserAmount", dispenserAmount)

                                                                
                                                                    };

                sqlParameters[1].Direction = ParameterDirection.InputOutput;
                resp = Convert.ToInt32(sqlParameters[1].Value);

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[update_kiosk_condition_order]", sqlParameters);
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREUpdateKioskConditionDispenserCashCount");
                resp = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
            }
            finally
            {
                base.Dispose();
            }

            return resp;
        }


        public KioskMonitoringCollection SearchKioskMonitoringOrder(string kioskId,
                                              string searchText,
                                              int numberOfItemsPerPage,
                                              int currentPageNum,
                                              ref int recordCount,
                                              ref int pageCount,
            //ref string TotalAmount,
            //ref string TotalCashCount,
            //ref string TotalCoinCount,
                                              int orderSelectionColumn,
                                              int descAsc,
            //int kioskType,
                                              int orderType, DateTime startDate, DateTime endDate, int instutionId)
        {
            KioskMonitoringCollection kioskMonitoring = new KioskMonitoringCollection();
            DataSet dataSet = null;

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@orderType", orderType),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),
                                                                    new SqlParameter("@instutionId", instutionId),
                                                                 };

                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;

                sqlParameters[9].Value = Convert.ToDateTime(startDate);
                sqlParameters[10].Value = Convert.ToDateTime(endDate);
                //sqlParameters[7].Direction = sqlParameters[8].Direction = sqlParameters[9].Direction  = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[get_kiosk_details_depends_on_institution]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);


                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    kioskMonitoring.Add(this.GetKioskMonitoring(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

                List<KioskMonitoring> modifiedList = new List<KioskMonitoring>(kioskMonitoring);
                modifiedList[modifiedList.Count-1].KioskName = "TOPLAM";
                modifiedList[modifiedList.Count - 1].ClassName = "info";
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskCMonitoringOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return kioskMonitoring;
        }


        public KioskCashEmptyCollection SearchKioskCashEmptyOrder(string searchText,
                                      DateTime startDate,
                                      DateTime endDate,
                                      int numberOfItemsPerPage,
                                      int currentPageNum,
                                      ref int recordCount,
                                      ref int pageCount,
                                      ref string TotalAmount,
                                      ref string TotalCashCount,
                                      ref string TotalCoinCount,
                                      int orderSelectionColumn,
                                      int descAsc,
                                      //int kioskType,
                                      int instutionId,
                                      string kioskId,
                                      int orderType)
        {
            KioskCashEmptyCollection items = new KioskCashEmptyCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime), 
                                                                    new SqlParameter("@TotalAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalCashCount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalCoinCount",SqlDbType.VarChar,200),
                                                                    //new SqlParameter("@kioskType", kioskType),
                                                                    new SqlParameter("@orderType", orderType),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@instutionId", instutionId)

                                                                 };
                sqlParameters[7].Value = Convert.ToDateTime(startDate);
                sqlParameters[8].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                sqlParameters[9].Direction = sqlParameters[10].Direction = sqlParameters[11].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[search_kiosk_cash_empty_order]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);
                TotalAmount = sqlParameters[9].Value.ToString();
                TotalCashCount = sqlParameters[10].Value.ToString();
                TotalCoinCount = sqlParameters[11].Value.ToString();

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskCashEmpty(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

                items.Add(new KioskCashEmpty(items.Count + 1, 0, "TOPLAM", "", "", "", TotalAmount, Convert.ToInt32(TotalCashCount), 0, Convert.ToInt32(TotalCoinCount), "", "", "info"));

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskCashEmptyOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        //public KioskCashEmptyCollection SearchKioskCashEmptyOrderForExcel(string searchText,
        //                                                                  DateTime startDate,
        //                                                                  DateTime endDate,
        //                                                                  int orderSelectionColumn,
        //                                                                  int descAsc,
        //                                                                  int kioskType)
        //{
        //    KioskCashEmptyCollection items = new KioskCashEmptyCollection();
        //    DataSet dataSet = null;
        //    try
        //    {
        //        SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@searchText", searchText),  
        //                                                            new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
        //                                                            new SqlParameter("@orderSelectionDescAsc", descAsc),
        //                                                             new SqlParameter("@startDate", SqlDbType.DateTime),  
        //                                                            new SqlParameter("@endDate", SqlDbType.DateTime),
        //                                                            new SqlParameter("@kioskType", kioskType)
        //                                                         };
        //        sqlParameters[3].Value = Convert.ToDateTime(startDate);
        //        sqlParameters[4].Value = Convert.ToDateTime(endDate);
        //        dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[search_kiosk_cash_empty_order_for_excel]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

        //        for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
        //        {
        //            items.Add(this.GetKioskCashEmpty(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
        //        }
        //    }
        //    catch (Exception exp)
        //    {
        //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskCashEmptyOrderForExcel");

        //    }
        //    finally
        //    {
        //        base.Dispose();

        //        if (dataSet != null)
        //        {
        //            dataSet.Dispose();
        //            dataSet = null;
        //        }
        //    }
        //    return items;
        //}

        public KioskFillCashBoxCollection SearchKioskFillCashBoxOrder(string kioskId,
                              DateTime startDate,
                              DateTime endDate,
                              int institution,
                              int numberOfItemsPerPage,
                              int currentPageNum,
                              ref int recordCount,
                              ref int pageCount,
                              int orderSelectionColumn,
                              int descAsc,
                              ref string totalAmount,
                              int orderType)
        {
            KioskFillCashBoxCollection items = new KioskFillCashBoxCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@kioskId", kioskId),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@totalAmount", SqlDbType.VarChar,200),
                                                                    new SqlParameter("@orderType", orderType),
                                                                    new SqlParameter("@institutionId", institution)
                                                                 };
                sqlParameters[7].Value = Convert.ToDateTime(startDate);
                sqlParameters[8].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                sqlParameters[9].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[search_kiosk_fill_cashbox_order_temp]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);
                totalAmount = sqlParameters[9].Value.ToString();

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskFillCashBox(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

                items.Add(new KioskFillCashBox(0,0,"","","","",totalAmount,"info"));

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskFillCashOrderEmptyOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        //public KioskFillCashBoxCollection SearchKioskFillCashBoxOrderForExcel(string kioskId,
        //                                                                  DateTime startDate,
        //                                                                  DateTime endDate,
        //                                                                  int orderSelectionColumn,
        //                                                                  int descAsc)
        //{
        //    KioskFillCashBoxCollection items = new KioskFillCashBoxCollection();
        //    DataSet dataSet = null;
        //    try
        //    {
        //        SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@kioskId", kioskId),  
        //                                                            new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
        //                                                            new SqlParameter("@orderSelectionDescAsc", descAsc),
        //                                                            new SqlParameter("@startDate", SqlDbType.DateTime),  
        //                                                            new SqlParameter("@endDate", SqlDbType.DateTime)
        //                                                         };
        //        sqlParameters[3].Value = Convert.ToDateTime(startDate);
        //        sqlParameters[4].Value = Convert.ToDateTime(endDate);
        //        dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[search_kiosk_fill_cashbox_order_for_excel]",Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

        //        for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
        //        {
        //            items.Add(this.GetKioskFillCashBox(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
        //        }
        //    }
        //    catch (Exception exp)
        //    {
        //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskCashEmptyOrderForExcel");

        //    }
        //    finally
        //    {
        //        base.Dispose();

        //        if (dataSet != null)
        //        {
        //            dataSet.Dispose();
        //            dataSet = null;
        //        }
        //    }
        //    return items;
        //}

        public KioskConditionDetailCollection GetKioskConditionDetailOrder(int kioskId,
                                      int numberOfItemsPerPage,
                                      int currentPageNum,
                                      ref int recordCount,
                                      ref int pageCount)
        {
            KioskConditionDetailCollection kiosks = new KioskConditionDetailCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@kioskId", kioskId),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum)
                                                                 };

                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_condition_detail_order_temp", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    kiosks.Add(this.GetKioskCashDetail(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKioskConditionDetailOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return kiosks;
        }

        public KioskConditionDetailCollection GetKioskConditionDetailOrderForExcel(int kioskId)
        {
            KioskConditionDetailCollection kiosks = new KioskConditionDetailCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@kioskId", kioskId) };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_condition_detail_order_for_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    kiosks.Add(this.GetKioskCashDetail(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKioskConditionDetailOrderForExcel");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return kiosks;
        }

        public KioskCommandTypeNameCollection GetKioskCommandTypeName()
        {
            KioskCommandTypeNameCollection kioskCommandTypeNames = new KioskCommandTypeNameCollection();
            DataSet dataSet = null;
            try
            {


                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_command_type_names", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    kioskCommandTypeNames.Add(this.GetKioskCommandTypeName(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKioskCommandTypeName");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return kioskCommandTypeNames;
        }

        public KioskCommandReasonCollection GetKioskCommandReasonName()
        {
            KioskCommandReasonCollection kioskCommandReasonNames = new KioskCommandReasonCollection();
            DataSet dataSet = null;
            try
            {


                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_command_reason_names", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    kioskCommandReasonNames.Add(this.GetKioskCommandReasonName(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKioskCommandReasonName");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return kioskCommandReasonNames;
        }

        public KioskNameCollection GetKioskName()
        {
            KioskNameCollection kioskNames = new KioskNameCollection();

            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_names", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    kioskNames.Add(this.GetKioskName(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKioskName");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return kioskNames;
        }


        //GetReferenceStatusOrder
        public ReferenceStatusCollection GetReferenceStatusOrder()
        {
            ReferenceStatusCollection kioskNames = new ReferenceStatusCollection();

            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_reference_status", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    kioskNames.Add(this.GetReferenceStatus(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetReferenceStatusOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return kioskNames;
        }


        public KioskDetailCollection GetKioskDetails()
        {
            KioskDetailCollection kioskNames = new KioskDetailCollection();

            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_details", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    kioskNames.Add(this.GetKioskDetail(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKioskName");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return kioskNames;
        }

        public TxnStatusNameCollection GetTxnStatusNames()
        {
            TxnStatusNameCollection names = new TxnStatusNameCollection();
            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_txn_status_names", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    names.Add(this.GetTxnStatusName(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetTxnStatusNames");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return names;
        }


        public InstutionNameCollection GetInstutionNames()
        {
            InstutionNameCollection names = new InstutionNameCollection();
            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_foundation_names", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    names.Add(this.GetInstutionName(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetInstutionNames");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return names;
        }

        public BankNameCollection GetBankNames()
        {
            BankNameCollection names = new BankNameCollection();
            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_bank_names", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    names.Add(this.GetBankName(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetInstutionNames");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return names;
        }

        public CompleteStatusNameCollection GetCompleteStatusNames()
        {
            CompleteStatusNameCollection names = new CompleteStatusNameCollection();
            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_completestatus_names", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    names.Add(this.GetCompleteStatusName(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetInstutionNames");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return names;
        }

        public RiskClassCollection GetRiskClass()
        {
            RiskClassCollection risk = new RiskClassCollection();
            DataSet dataSet = null;
            try
            {
                //dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_foundation_names", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_risk_class");
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    risk.Add(this.GetRiskClass(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetRiskClass");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return risk;
        }


        public InstutionNameCollection GetInstutionNamesMutabakat()
        {
            InstutionNameCollection names = new InstutionNameCollection();
            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_foundation_names_mutabakat", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    names.Add(this.GetInstutionName(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetInstutionNames");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return names;
        }

        public ProcessTypeCollection GetProcessType()
        {
            ProcessTypeCollection process = new ProcessTypeCollection();
            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_process_types", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    process.Add(this.GetProcessType(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetProcessTypes");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return process;
        }

        public InstutionNameCollection GetDeviceNames()
        {
            InstutionNameCollection names = new InstutionNameCollection();
            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_device_names", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    names.Add(this.GetInstutionName(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetDeviceNames");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return names;
        }


        public InstutionOperationNameCollection GetInstutionOperationNames()
        {
            InstutionOperationNameCollection names = new InstutionOperationNameCollection();
            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_instution_operation_names", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    names.Add(this.GetInstutionOperationName(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetInstutionNames");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return names;
        }

        public CardTypeNameColection GetCardTypeNames()
        {
            CardTypeNameColection names = new CardTypeNameColection();
            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_card_type_names", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    names.Add(this.GetCardTypeName(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetCardTypeNames");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return names;
        }
        
        public ConnectionTypeCollection GetDistrictAndneighborhoods()
        {


            ConnectionTypeCollection connectionType = new ConnectionTypeCollection();
            DataSet dataSet = null;
            try
            {
                //SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@connectionTypeId", connectionTypeId) };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_district_and_neighborhood");

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    connectionType.Add(this.GetConnectionType(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetConnectionType");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return connectionType;
        }
        public ConnectionTypeCollection GetConnectionType()
        {


            ConnectionTypeCollection connectionType = new ConnectionTypeCollection();
            DataSet dataSet = null;
            try
            {
                //SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@connectionTypeId", connectionTypeId) };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_connection_type");

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    connectionType.Add(this.GetConnectionType(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetConnectionType");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return connectionType;
        }



        private ConnectionType GetConnectionType(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new ConnectionType(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }


        public CardReaderTypeCollection GetCardReaderType()
        {


            CardReaderTypeCollection cardReaderType = new CardReaderTypeCollection();
            DataSet dataSet = null;
            try
            {
                // SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@connectionTypeId", connectionTypeId) };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_cardreader_type");

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    cardReaderType.Add(this.GetCardReaderType(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetCardReaderType");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return cardReaderType;
        }



        private CardReaderType GetCardReaderType(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new CardReaderType(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }



        public TransactionConditionNameCollection GetTransactionConditionNames()
        {
            TransactionConditionNameCollection names = new TransactionConditionNameCollection();
            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_transaction_condition_names", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    names.Add(this.GetTransactionConditionName(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetTransactionConditionNames");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return names;
        }

        public KioskTypeNameCollection GetKioskTypeNames()
        {
            KioskTypeNameCollection names = new KioskTypeNameCollection();
            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_type_names", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    names.Add(this.GetKioskTypeName(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetInstutionNames");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return names;
        }

        public EmptyKioskNameCollection GetEmptyKioskName(int expertUserId)
        {
            EmptyKioskNameCollection kioskNames = new EmptyKioskNameCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@expertUserId", expertUserId) };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[get_empty_kiosk_names]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    kioskNames.Add(this.GetEmptyKioskName(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetEmptyKioskName");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return kioskNames;
        }

        public EmptyKioskNameCollection GetEmptyKioskName()
        {
            EmptyKioskNameCollection kioskNames = new EmptyKioskNameCollection();
            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_empty_kiosk_names_for_mutabakat", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    kioskNames.Add(this.GetEmptyKioskName(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetEmptyKioskNameMutabakat");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return kioskNames;
        }

        public KioskCommandCollection SearchKioskCommandsOrder(string searchText,
                                             DateTime startDate,
                                             DateTime endDate,
                                             string kioskId,
                                             int instutionId,
                                             int numberOfItemsPerPage,
                                             int currentPageNum,
                                             ref int recordCount,
                                             ref int pageCount,
                                             int orderSelectionColumn,
                                             int descAsc,
                                             int orderType)
        {
            KioskCommandCollection kioskCommands = new KioskCommandCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@orderType", orderType),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@institutionId", instutionId),
                                                                 };
                sqlParameters[3].Value = Convert.ToDateTime(startDate);
                sqlParameters[4].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_kiosk_commands_order", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    kioskCommands.Add(this.GetKioskCommand(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskCommandsOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return kioskCommands;
        }

        
        //GetKioskCommandOrder
        public KioskCommand GetKioskCommandOrder(int commandId)
        {
            KioskCommand items = null;
            DataSet dataSet = null;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@commandId", commandId) };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_commands_order", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                if (dataSet.Tables[0].Rows.Count == 1)
                {

                    items = this.GetKioskCommand(dataSet.Tables[0].Rows[0], dataSet.Tables[0].Columns);
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKioskCommandOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return items;
        }	
		
		

        public KioskCashCountCollection SearchKiosksCashCountOrder(int operationType,
                                                                   int itemId,
                                                                   int numberOfItemsPerPage,
                                                                   int currentPageNum,
                                                                   ref int recordCount,
                                                                   ref int pageCount,
                                                                   int orderSelectionColumn,
                                                                   int descAsc)
        {
            KioskCashCountCollection items = new KioskCashCountCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@itemId", itemId),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn), 
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@operationType", operationType)
                                                                 };

                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                sqlParameters[7].Direction = ParameterDirection.Input;

                if (operationType == 1)
                {
                    dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_cash_count_order", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);
                }
                else
                {
                    dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.[get_kiosk_empty_cash_count_order]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);
                }
                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskCashCount(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKioskConditionDetailOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }


        public KioskTotalCashCountCollection SearchKiosksTotalCashCountOrder(DateTime startDate,
                                                                   DateTime endDate,
                                                                   int operationType,
            // int itemId,
                                                                   int numberOfItemsPerPage,
                                                                   int currentPageNum,
                                                                   ref int recordCount,
                                                                   ref int pageCount,
                                                                   int orderSelectionColumn,
                                                                   int descAsc)
        {
            KioskTotalCashCountCollection items = new KioskTotalCashCountCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    //new SqlParameter("@itemId", itemId),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn), 
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@operationType", operationType),
                                                                    new SqlParameter("@startDate", startDate),
                                                                    new SqlParameter("@endDate", endDate)
                                                                 };

                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                sqlParameters[6].Direction = ParameterDirection.Input;

                if (operationType == 1)
                {
                    dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_total_cash_count_order", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);
                }
                else
                {
                    dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.[get_kiosk_total_empty_cash_count_order]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);
                }
                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskTotalCashCount(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKioskConditionDetailOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }




        public KioskCashCountCollection SearchKiosksCashCountOrderForExcel(int operationType,
                                                                   int itemId,
                                                                   int orderSelectionColumn,
                                                                   int descAsc)
        {
            KioskCashCountCollection items = new KioskCashCountCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@itemId", itemId),   
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn), 
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc)
                                                                 };

                if (operationType == 1)
                {
                    dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_cash_count_order_for_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);
                }
                else
                {
                    dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.[get_kiosk_empty_cash_count_order_for_excel]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);
                }

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskCashCount(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKioskConditionDetailOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        public KioskCashBoxCountTotalCollection SearchKiosksCashBoxCountOrder(int operationType,
                                                                   int itemId,
                                                                   int numberOfItemsPerPage,
                                                                   int currentPageNum,
                                                                   ref int recordCount,
                                                                   ref int pageCount,
                                                                   int orderSelectionColumn,
                                                                   int descAsc)
        {
            KioskCashBoxCountTotalCollection items = new KioskCashBoxCountTotalCollection();
            DataSet dataSet = null;
            try
            {

                if (operationType == 1)
                {
                    SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@itemId", itemId),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn), 
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@cassette1Value", SqlDbType.VarChar,10),
                                                                    new SqlParameter("@cassette1Count", 0),
                                                                    new SqlParameter("@cassette1Reject", 0),
                                                                    new SqlParameter("@cassette2Value", SqlDbType.VarChar,10),
                                                                    new SqlParameter("@cassette2Count", 0),
                                                                    new SqlParameter("@cassette2Reject", 0),
                                                                    new SqlParameter("@cassette3Value", SqlDbType.VarChar,10),
                                                                    new SqlParameter("@cassette3Count", 0),
                                                                    new SqlParameter("@cassette3Reject", 0),
                                                                    new SqlParameter("@cassette4Value", SqlDbType.VarChar,10),
                                                                    new SqlParameter("@cassette4Count", 0),
                                                                    new SqlParameter("@cassette4Reject", 0),
                                                                    new SqlParameter("@hopper1Value", SqlDbType.VarChar,10),
                                                                    new SqlParameter("@hopper1Count", 0),
                                                                    new SqlParameter("@hopper2Value", SqlDbType.VarChar,10),
                                                                    new SqlParameter("@hopper2Count", 0),
                                                                    new SqlParameter("@hopper3Value", SqlDbType.VarChar,10),
                                                                    new SqlParameter("@hopper3Count", 0),
                                                                    new SqlParameter("@hopper4Value", SqlDbType.VarChar,10),
                                                                    new SqlParameter("@hopper4Count", 0),

                                                                 };

                    sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;


                    //sqlParameters[7].Direction = sqlParameters[8].Direction = sqlParameters[9].Direction = sqlParameters[10].Direction = ParameterDirection.InputOutput;
                    //sqlParameters[11].Direction = sqlParameters[12].Direction = sqlParameters[13].Direction = sqlParameters[14].Direction = ParameterDirection.InputOutput;
                    //sqlParameters[15].Direction = sqlParameters[16].Direction = sqlParameters[17].Direction = sqlParameters[18].Direction = ParameterDirection.InputOutput;
                    //sqlParameters[19].Direction = sqlParameters[20].Direction = sqlParameters[21].Direction = sqlParameters[22].Direction = ParameterDirection.InputOutput;
                    //sqlParameters[23].Direction = sqlParameters[24].Direction = sqlParameters[25].Direction = sqlParameters[26].Direction = ParameterDirection.InputOutput;

                    sqlParameters[7].Direction  = sqlParameters[8].Direction  = sqlParameters[9].Direction = ParameterDirection.InputOutput;
                    sqlParameters[10].Direction = sqlParameters[11].Direction = sqlParameters[12].Direction = ParameterDirection.InputOutput;
                    sqlParameters[13].Direction = sqlParameters[14].Direction = sqlParameters[15].Direction = ParameterDirection.InputOutput;
                    sqlParameters[16].Direction = sqlParameters[17].Direction = sqlParameters[18].Direction = ParameterDirection.InputOutput;

                    sqlParameters[19].Direction = sqlParameters[20].Direction = ParameterDirection.InputOutput;
                    sqlParameters[21].Direction = sqlParameters[22].Direction = ParameterDirection.InputOutput; 
                    sqlParameters[23].Direction = sqlParameters[24].Direction = ParameterDirection.InputOutput; 
                    sqlParameters[25].Direction = sqlParameters[26].Direction = ParameterDirection.InputOutput;


                    dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_cashbox_count_order", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                    recordCount = Convert.ToInt32(sqlParameters[0].Value);
                    pageCount = Convert.ToInt32(sqlParameters[1].Value);

                    string casette1Value = sqlParameters[7].Value.ToString();
                    int cassette1Count = Convert.ToInt32(sqlParameters[8].Value);
                    int cassette1Reject = Convert.ToInt32(sqlParameters[9].Value);

                    string casette2Value = sqlParameters[10].Value.ToString();
                    int cassette2Count = Convert.ToInt32(sqlParameters[11].Value);
                    int cassette2Reject = Convert.ToInt32(sqlParameters[12].Value);

                    string casette3Value = sqlParameters[13].Value.ToString();
                    int cassette3Count = Convert.ToInt32(sqlParameters[14].Value);
                    int cassette3Reject = Convert.ToInt32(sqlParameters[15].Value);

                    string casette4Value = sqlParameters[16].Value.ToString();
                    int cassette4Count = Convert.ToInt32(sqlParameters[17].Value);
                    int cassette4Reject = Convert.ToInt32(sqlParameters[18].Value);

                    string hopper1Value = sqlParameters[19].Value.ToString();
                    int hopper1Count = Convert.ToInt32(sqlParameters[20].Value);

                    string hopper2Value = sqlParameters[21].Value.ToString();
                    int hopper2Count = Convert.ToInt32(sqlParameters[22].Value);

                    string hopper3Value = sqlParameters[23].Value.ToString();
                    int hopper3Count = Convert.ToInt32(sqlParameters[24].Value);

                    string hopper4Value = sqlParameters[25].Value.ToString();
                    int hopper4Count = Convert.ToInt32(sqlParameters[26].Value);

                    /*
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        items.Add(this.GetKioskCashBoxCount(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                    }
                    */
                    items.Add(new KioskCashBoxCountTotal(itemId, "BankNote:", Convert.ToString(casette1Value), cassette1Count, (Convert.ToDouble(casette1Value) * cassette1Count).ToString()));
                    items.Add(new KioskCashBoxCountTotal(itemId, "BankNote:", Convert.ToString(casette2Value), cassette2Count, (Convert.ToDouble(casette2Value) * cassette2Count).ToString()));
                    items.Add(new KioskCashBoxCountTotal(itemId, "BankNote:", Convert.ToString(casette3Value), cassette3Count, (Convert.ToDouble(casette3Value) * cassette3Count).ToString()));
                    items.Add(new KioskCashBoxCountTotal(itemId, "BankNote:", Convert.ToString(casette4Value), cassette4Count, (Convert.ToDouble(casette4Value) * cassette4Count).ToString()));


                    items.Add(new KioskCashBoxCountTotal(itemId, "Hopper:", Convert.ToString(hopper1Value), hopper1Count, (Convert.ToDouble(hopper1Value) * hopper1Count).ToString()));
                    items.Add(new KioskCashBoxCountTotal(itemId, "Hopper:", Convert.ToString(hopper2Value), hopper2Count, (Convert.ToDouble(hopper2Value) * hopper2Count).ToString()));
                    items.Add(new KioskCashBoxCountTotal(itemId, "Hopper:", Convert.ToString(hopper3Value), hopper3Count, (Convert.ToDouble(hopper3Value) * hopper3Count).ToString()));
                    items.Add(new KioskCashBoxCountTotal(itemId, "Hopper:", Convert.ToString(hopper4Value), hopper4Count, (Convert.ToDouble(hopper4Value) * hopper4Count).ToString()));

                    items.Add(new KioskCashBoxCountTotal(itemId, "Reject:", Convert.ToString(casette1Value), cassette1Reject, (Convert.ToDouble(casette1Value) * cassette1Reject).ToString()));
                    items.Add(new KioskCashBoxCountTotal(itemId, "Reject:", Convert.ToString(casette2Value), cassette2Reject, (Convert.ToDouble(casette2Value) * cassette2Reject).ToString()));
                    items.Add(new KioskCashBoxCountTotal(itemId, "Reject:", Convert.ToString(casette3Value), cassette3Reject, (Convert.ToDouble(casette3Value) * cassette3Reject).ToString()));
                    items.Add(new KioskCashBoxCountTotal(itemId, "Reject:", Convert.ToString(casette4Value), cassette4Reject, (Convert.ToDouble(casette4Value) * cassette4Reject).ToString()));

                }
                else
                {
                    SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@itemId", itemId),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn), 
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc)
                                                                 };

                    sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;

                    dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.[get_kiosk_fulfill_cashbox_count_order]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                    recordCount = Convert.ToInt32(sqlParameters[0].Value);
                    pageCount = Convert.ToInt32(sqlParameters[1].Value);

                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        KioskCashBoxCount kioskCount = this.GetKioskCashBoxCount(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns);
                        items.Add(new KioskCashBoxCountTotal(kioskCount.KioskId, kioskCount.KioskName, kioskCount.CashTypeName, kioskCount.CashCount, (Convert.ToInt32(kioskCount.CashTypeName) * kioskCount.CashCount).ToString()));
                    }

                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKioskConditionDetailOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        public KioskCashBoxCountCollection SearchKiosksCashBoxCountOrderForExcel(int operationType,
                                                                   int itemId,
                                                                   int orderSelectionColumn,
                                                                   int descAsc)
        {
            KioskCashBoxCountCollection items = new KioskCashBoxCountCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@itemId", itemId),   
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn), 
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc)
                                                                 };

                if (operationType == 1)
                {
                    dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_cashbox_count_order_for_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);
                }
                else
                {
                    dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.[get_kiosk_fulfill_cashbox_count_order_for_excel]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);
                }

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskCashBoxCount(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKioskConditionDetailOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        public KioskReportCollections SearchKioskReportsOrder(string searchText,
                                              DateTime startDate,
                                              DateTime endDate,
                                              int numberOfItemsPerPage,
                                              int currentPageNum,
                                              ref int recordCount,
                                              ref int pageCount,
                                              int orderSelectionColumn,
                                              int descAsc,
                                              string kioskId,
                                              int instutionId,
                                              ref Int64 totalTxnCount,
                                              ref string totalGeneratedAskiAmount,
                                              ref string totalUsedAskiAmount,
                                              ref Int64 totalGeneratedAskiCount,
                                              ref Int64 totalUsedAskiCount,
                                              ref string totalReceivedAmount,
                                              ref string totalKioskEmptyAmount,
                                              ref string totalEarnedAmount,
                                              ref string totalParaUstu,
                                              ref string totalSuccessTxnCount,
                                              ref string TotalInstitutionAmount,
                                              ref string TotalTotalAmount,
                                              ref string TotalCreditCardAmount,
                                              ref string TotalCommisionAmount, 
                                              int orderType)
        {
            KioskReportCollections items = new KioskReportCollections();
            DataSet dataSet = null;


            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@searchKioskId", kioskId),
                                                                    new SqlParameter("@TotalTxnCount", totalTxnCount),
                                                                    new SqlParameter("@TotalGeneratedAskiAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalUsedAskiAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalGeneratedAskiCount", totalGeneratedAskiCount),
                                                                    new SqlParameter("@TotalUsedAskiCount", totalUsedAskiCount),
                                                                    new SqlParameter("@TotalReceivedAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalKioskEmptyAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalEarnedAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalParaUstu",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalSuccessTxnCount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalInstitutionAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalTotalAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalCreditCardAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalCommisionAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@instutionId",instutionId),
                                                                    new SqlParameter("@orderType",orderType)
                };

                sqlParameters[3].Value = Convert.ToDateTime(startDate);
                sqlParameters[4].Value = Convert.ToDateTime(endDate);
                //sqlParameters[11].Value = Convert.ToDecimal(totalGeneratedAskiAmount);
                //sqlParameters[12].Value = Convert.ToDecimal(totalUsedAskiAmount);
                //sqlParameters[15].Value = Convert.ToDecimal(totalReceivedAmount);
                //sqlParameters[16].Value = Convert.ToDecimal(totalKioskEmptyAmount);
                //sqlParameters[17].Value = Convert.ToDecimal(totalEarnedAmount);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                sqlParameters[10].Direction = sqlParameters[11].Direction = sqlParameters[12].Direction = sqlParameters[13].Direction = ParameterDirection.Output;
                sqlParameters[14].Direction = sqlParameters[15].Direction = sqlParameters[16].Direction = sqlParameters[17].Direction = ParameterDirection.Output;
                sqlParameters[18].Direction = sqlParameters[19].Direction = ParameterDirection.Output;
                sqlParameters[20].Direction = sqlParameters[21].Direction = ParameterDirection.Output;
                sqlParameters[22].Direction = sqlParameters[23].Direction = ParameterDirection.Output;

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_report_V2", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                totalTxnCount = Convert.ToInt32(sqlParameters[10].Value);
                totalGeneratedAskiAmount = sqlParameters[11].Value.ToString();
                totalUsedAskiAmount = sqlParameters[12].Value.ToString();
                totalGeneratedAskiCount = Convert.ToInt32(sqlParameters[13].Value);
                totalUsedAskiCount = Convert.ToInt32(sqlParameters[14].Value);
                totalReceivedAmount = sqlParameters[15].Value.ToString();
                totalKioskEmptyAmount = sqlParameters[16].Value.ToString();
                totalEarnedAmount = sqlParameters[17].Value.ToString();
                totalParaUstu = sqlParameters[18].Value.ToString();
                totalSuccessTxnCount = sqlParameters[19].Value.ToString();
                TotalInstitutionAmount = sqlParameters[20].Value.ToString();
                TotalTotalAmount = sqlParameters[21].Value.ToString();
                TotalCreditCardAmount = sqlParameters[22].Value.ToString();
                TotalCommisionAmount = sqlParameters[23].Value.ToString();
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskReports(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

                items.Add(new KioskReports(0
                                          ,"TOPLAM"
                                          , totalTxnCount.ToString()
                                          , totalGeneratedAskiAmount.ToString()
                                          , totalUsedAskiAmount.ToString()
                                          , totalGeneratedAskiCount.ToString()
                                          , totalUsedAskiCount.ToString()
                                          , totalReceivedAmount.ToString()
                                          , totalEarnedAmount.ToString()
                                          , totalParaUstu.ToString()
                                          , totalSuccessTxnCount.ToString()
                                          , TotalTotalAmount.ToString()
                                          , TotalInstitutionAmount.ToString()
                                          , TotalCreditCardAmount.ToString()
                                          , TotalCommisionAmount.ToString()
                                          , "info"));
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskReportOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        public KioskReportCollection SearchKioskReportOrder(string searchText,
                                                      DateTime startDate,
                                                      DateTime endDate,
                                                      int numberOfItemsPerPage,
                                                      int currentPageNum,
                                                      ref int recordCount,
                                                      ref int pageCount,
                                                      int orderSelectionColumn,
                                                      int descAsc,
                                                      string kioskId,
                                                      int instutionId,
                                                      ref Int64 totalTxnCount,
                                                      ref string totalGeneratedAskiAmount,
                                                      ref string totalUsedAskiAmount,
                                                      ref Int64 totalGeneratedAskiCount,
                                                      ref Int64 totalUsedAskiCount,
                                                      ref string totalReceivedAmount,
            //ref string totalKioskEmptyAmount,
                                                      ref string totalEarnedAmount,
                                                      ref string totalParaUstu,
                                                      ref string totalSuccesTxnCount)
        {
            KioskReportCollection items = new KioskReportCollection();
            DataSet dataSet = null;


            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@searchKioskId", kioskId),
                                                                    new SqlParameter("@TotalTxnCount", totalTxnCount),
                                                                    new SqlParameter("@TotalGeneratedAskiAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalUsedAskiAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalGeneratedAskiCount", totalGeneratedAskiCount),
                                                                    new SqlParameter("@TotalUsedAskiCount", totalUsedAskiCount),
                                                                    new SqlParameter("@TotalReceivedAmount",SqlDbType.VarChar,200),
                                                                    //new SqlParameter("@TotalKioskEmptyAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalEarnedAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalParaUstu",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@instutionId",instutionId),
                                                                    new SqlParameter("@TotalSuccesTxnCount",SqlDbType.VarChar,200)};

                sqlParameters[3].Value = Convert.ToDateTime(startDate);
                sqlParameters[4].Value = Convert.ToDateTime(endDate);
                //sqlParameters[11].Value = Convert.ToDecimal(totalGeneratedAskiAmount);
                //sqlParameters[12].Value = Convert.ToDecimal(totalUsedAskiAmount);
                //sqlParameters[15].Value = Convert.ToDecimal(totalReceivedAmount);
                //sqlParameters[16].Value = Convert.ToDecimal(totalKioskEmptyAmount);
                //sqlParameters[17].Value = Convert.ToDecimal(totalEarnedAmount);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                sqlParameters[10].Direction = sqlParameters[11].Direction = sqlParameters[12].Direction = sqlParameters[13].Direction = ParameterDirection.Output;
                sqlParameters[14].Direction = sqlParameters[15].Direction = sqlParameters[16].Direction = sqlParameters[17].Direction = ParameterDirection.Output;
                sqlParameters[19].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_report_temp", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                totalTxnCount = Convert.ToInt32(sqlParameters[10].Value);
                totalGeneratedAskiAmount = sqlParameters[11].Value.ToString();
                totalUsedAskiAmount = sqlParameters[12].Value.ToString();
                totalGeneratedAskiCount = Convert.ToInt32(sqlParameters[13].Value);
                totalUsedAskiCount = Convert.ToInt32(sqlParameters[14].Value);
                totalReceivedAmount = sqlParameters[15].Value.ToString();
                //totalKioskEmptyAmount = sqlParameters[16].Value.ToString();
                totalEarnedAmount = sqlParameters[16].Value.ToString();
                totalParaUstu = sqlParameters[17].Value.ToString();
                totalSuccesTxnCount = sqlParameters[19].Value.ToString();

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskReport(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskReportOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        /*
        //SearchKioskCashBoxRejectCountOrder
        public KioskReportCollection SearchKioskCashBoxRejectCountOrder(string kioskId,
                                              
                                              ref Int64 totalTxnCount,
                                              ref string totalGeneratedAskiAmount,
                                              ref string totalUsedAskiAmount,
                                              ref Int64 totalGeneratedAskiCount,
                                              ref Int64 totalUsedAskiCount,
                                              ref string totalReceivedAmount,
            //ref string totalKioskEmptyAmount,
                                              ref string totalEarnedAmount,
                                              ref string totalParaUstu,
                                              ref string totalSuccesTxnCount)
        {
            KioskReportCollection items = new KioskReportCollection();
            DataSet dataSet = null;


            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] {
                                                                    new SqlParameter("@searchKioskId", kioskId),
                                                                    new SqlParameter("@TotalTxnCount", totalTxnCount),
                                                                    new SqlParameter("@TotalGeneratedAskiAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalUsedAskiAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalGeneratedAskiCount", totalGeneratedAskiCount),
                                                                    new SqlParameter("@TotalUsedAskiCount", totalUsedAskiCount),
                                                                    new SqlParameter("@TotalReceivedAmount",SqlDbType.VarChar,200),
                                                                    //new SqlParameter("@TotalKioskEmptyAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalEarnedAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalParaUstu",SqlDbType.VarChar,200),
                                                                  
                                                                    new SqlParameter("@TotalSuccesTxnCount",SqlDbType.VarChar,200)};

                sqlParameters[3].Value = Convert.ToDateTime(startDate);
                sqlParameters[4].Value = Convert.ToDateTime(endDate);
                //sqlParameters[11].Value = Convert.ToDecimal(totalGeneratedAskiAmount);
                //sqlParameters[12].Value = Convert.ToDecimal(totalUsedAskiAmount);
                //sqlParameters[15].Value = Convert.ToDecimal(totalReceivedAmount);
                //sqlParameters[16].Value = Convert.ToDecimal(totalKioskEmptyAmount);
                //sqlParameters[17].Value = Convert.ToDecimal(totalEarnedAmount);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                sqlParameters[10].Direction = sqlParameters[11].Direction = sqlParameters[12].Direction = sqlParameters[13].Direction = ParameterDirection.Output;
                sqlParameters[14].Direction = sqlParameters[15].Direction = sqlParameters[16].Direction = sqlParameters[17].Direction = ParameterDirection.Output;
                sqlParameters[19].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_report_temp", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                totalTxnCount = Convert.ToInt32(sqlParameters[10].Value);
                totalGeneratedAskiAmount = sqlParameters[11].Value.ToString();
                totalUsedAskiAmount = sqlParameters[12].Value.ToString();
                totalGeneratedAskiCount = Convert.ToInt32(sqlParameters[13].Value);
                totalUsedAskiCount = Convert.ToInt32(sqlParameters[14].Value);
                totalReceivedAmount = sqlParameters[15].Value.ToString();
                //totalKioskEmptyAmount = sqlParameters[16].Value.ToString();
                totalEarnedAmount = sqlParameters[16].Value.ToString();
                totalParaUstu = sqlParameters[17].Value.ToString();
                totalSuccesTxnCount = sqlParameters[19].Value.ToString();

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskReport(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskReportOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }
        */


        public KioskReportCollection SearchKioskReportForExcelOrder(string searchText,
                                                     DateTime startDate,
                                                     DateTime endDate,
                                                     int orderSelectionColumn,
                                                     int descAsc,
                                                     string kioskId,
                                                     int instutionId)
        {
            KioskReportCollection items = new KioskReportCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@searchKioskId", kioskId),
                                                                    new SqlParameter("@instutionId", instutionId)};

                sqlParameters[1].Value = Convert.ToDateTime(startDate);
                sqlParameters[2].Value = Convert.ToDateTime(endDate);
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_report_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskReport(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskReportOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        public KioskReportCollections SearchKioskReportForExcelOrderNew(string searchText,
                                             DateTime startDate,
                                             DateTime endDate,
                                             int orderSelectionColumn,
                                             int descAsc,
                                             string kioskId,
                                             int instutionId)
        {
            KioskReportCollections items = new KioskReportCollections();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@searchKioskId", kioskId),
                                                                    new SqlParameter("@instutionId", instutionId)};

                sqlParameters[1].Value = Convert.ToDateTime(startDate);
                sqlParameters[2].Value = Convert.ToDateTime(endDate);
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "get_kiosk_acceptor_logs", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskReports(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskReportOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        private KioskReport GetKioskReport(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskReport(Convert.ToInt32(dataRow["KioskId"])
            , dataRow["KioskName"].ToString()
            , dataRow["TxnCount"].ToString()
            , dataRow["GeneratedAskiAmount"].ToString()
            , dataRow["UsedAskiAmount"].ToString()
            , dataRow["GeneratedAskiCount"].ToString()
            , dataRow["UsedAskiCount"].ToString()
            , dataRow["ReceivedAmount"].ToString()
                //, dataRow["KioskEmptyAmount"].ToString()
            , dataRow["EarnedMoney"].ToString()
            , dataRow["ParaUstu"].ToString()
            , dataRow["SuccessTxnCount"].ToString());
        }

        private KioskReports GetKioskReports(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskReports(Convert.ToInt32(dataRow["KioskId"])
            , dataRow["KioskName"].ToString()
            , (dataRow["TxnCount"].ToString())
            , (dataRow["GeneratedAskiAmount"].ToString())
            , (dataRow["UsedAskiAmount"].ToString())
            , (dataRow["GeneratedAskiCount"].ToString())
            , (dataRow["UsedAskiCount"].ToString())
            , (dataRow["ReceivedAmount"].ToString())
            , (dataRow["EarnedMoney"].ToString())
            , (dataRow["ParaUstu"].ToString())
            , (dataRow["SuccessTxnCount"].ToString())
            , (dataRow["TotalAmount"].ToString())
            , (dataRow["InstutionAmount"].ToString())
            , (dataRow["CreditCardAmount"].ToString())
            , (dataRow["CommisionAmount"].ToString())
            , ""
            );
        }
    }
}