﻿//using CORE.Utilities;
//using log4net;
using Microsoft.ApplicationBlocks.Data;
using PRCNCORE.Backend;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Backend
{
    public class EmailOpr : DBOperator
    {
        //private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        
        public DekontFields GetDekontFieldsByTransactionId(long transactionId)
        {
            try
            {
                DekontFields dekontfields = new DekontFields();
                DataSet dataSet = null;
                int returnCode = 0;
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@returnCode", returnCode),
                                                                    new SqlParameter("@transactionId", transactionId)};

                sqlParameters[0].Direction = ParameterDirection.InputOutput;

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[get_transaction_details_for_dekont]", /*AppUtilities.timeoutValue*/ Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    dekontfields = new DekontFields
                    {
                        InsertionDate = dataRow["InsertionDate"].ToString(),
                        TransactionId = dataRow["TransactionId"].ToString(),
                        CustomerId = dataRow["CustomerId"].ToString(),
                        CustomerName = dataRow["CustomerName"].ToString(),
                        KioskName = dataRow["KioskName"].ToString(),
                        PaymentType = dataRow["PaymentType"].ToString(),
                        CreditCardCustomer = dataRow["CreditCardCustomer"].ToString(),
                        CreditCardNumber = dataRow["CreditCardNumber"].ToString(),
                        InstitutionAmount = dataRow["InstitutionAmount"].ToString(),
                        UsageFee = dataRow["UsageFee"].ToString(),
                        CommisionFee = dataRow["CommisionFee"].ToString(),
                        InstitutionId = dataRow["InstitutionId"].ToString(),
                        Institution = dataRow["Institution"].ToString(),
                        TotalAmount = dataRow["TotalAmount"].ToString()
                    };

                }

                return dekontfields;

            }
            catch (Exception ex)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), ex, "OPR14 - GetPhoneNumberByCustomerId Exception Occured");

                //Log.Error("OPR14 - GetPhoneNumberByCustomerId Exception Occured", ex);
                return null;
            }

        }
        public string GetSettingNameByLabel(string labelName)
        {
            try
            {
                DataSet dataSet = null;
                string settingValue = "";

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@keyword", labelName),
                                                                    new SqlParameter("@settingValue", SqlDbType. NVarChar,100)};
                sqlParameters[1].Direction = ParameterDirection.InputOutput;

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[get_settings_parameters_by_label]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")) /*AppUtilities.timeoutValue*/, sqlParameters);

                settingValue = sqlParameters[1].Value.ToString();

                return settingValue;

            }
            catch (Exception ex)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), ex, "OPR15 - GetSettingNameByLabel Exception Occured");
                //Log.Error("OPR15 - GetSettingNameByLabel Exception Occured", ex);
                return null;
            }

        }
    }
}
