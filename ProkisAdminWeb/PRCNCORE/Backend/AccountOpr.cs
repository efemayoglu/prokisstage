﻿using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Accounts;
using PRCNCORE.Color;
using PRCNCORE.Constants;
using PRCNCORE.Kiosks;
using PRCNCORE.Parser.Other;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Backend
{
    public class AccountOpr : DBOperator
    {
        public AccountOpr()
        {

        }

        internal AccountOpr(SqlConnection sqlConnection)
            : base(sqlConnection)
        {

        }
        //



        public bool UpdateKioskEmptyStatus(int kioskEmptyId, int statusId, string explanation)
        {
            DataSet dataSet = null;
            var kioskEmpty = false;
            int resp = 0;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                { new SqlParameter("@returnCode", 0),
                  new SqlParameter("@id", kioskEmptyId),
                  new SqlParameter("@newStatusValue", statusId),
                  new SqlParameter("@newExplanation", explanation), };
                sqlParameters[0].Direction =    ParameterDirection.Output;
                //dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kioskempty_status", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                resp = Convert.ToInt32(sqlParameters[0].Value);

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.update_kiosk_empty_kioskEmptyStatusId", sqlParameters);

                if (resp == 0)
                {
                    kioskEmpty = true;
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetAccounts");
            }
            finally
            {

                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return kioskEmpty;
        }


        public List<UsageFeeDetails> GetUsageFeeDetails(string kioskId, int instutionId, int isKioskListed, DateTime startDate, DateTime endDate)
        {
            DataSet dataSet = null;
            var kioskEmpty = new List<UsageFeeDetails>();

            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] 
                {
                    new SqlParameter("@startDate", SqlDbType.DateTime),
                    new SqlParameter("@endDate", SqlDbType.DateTime),
                    new SqlParameter("@kioskId", kioskId),
                    new SqlParameter("@institutionId", instutionId),
                    new SqlParameter("@isKioskListed", isKioskListed),
                };

                sqlParameters[0].Value = Convert.ToDateTime(startDate);
                sqlParameters[1].Value = Convert.ToDateTime(endDate);

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "dbo.get_kiosk_usageFee_detail", sqlParameters);
                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                   
                        kioskEmpty.Add(this.GetUsageFeeParser(dataRow, dataSet.Tables[0].Columns));
                }
                kioskEmpty.LastOrDefault().className = "info";
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetAccounts");
            }
            finally
            {

                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return kioskEmpty;
        }



        public List<KioskEmptyStatus> GetKioskEmptyStatus(int id )
        {
            DataSet dataSet = null;
            var kioskEmpty = new List<KioskEmptyStatus>();

            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@id", id)};
                //sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                //dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kioskempty_status", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);
               

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kioskempty_status", sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    kioskEmpty.Add(this.GetKioskEmpty(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetAccounts");
            }
            finally
            {

                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return kioskEmpty;
        }



        public AccountCollection GetAccounts(int instutionId
                                           , int numberOfItemsPerPage
                                           , int currentPageNum
                                           , ref int recordCount
                                           , ref int pageCount
                                             , int orderSelectionColumn,
                                              int descAsc
                                           , int orderType)
        {
            DataSet dataSet = null;
            AccountCollection account = new AccountCollection();

            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount),
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                     new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@instutionId", instutionId),
                                                                    new SqlParameter("@orderType", orderType)
                                                                 };
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_accounts", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    account.Add(this.GetAccount(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetAccounts");
            }
            finally
            {

                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return account;
        }

        public MutabakatListCollection GetMutabakatListValues(DateTime mutabakatDate, Int64 kioskId, int instutionId, Int64 adminUserId)
        {
            MutabakatListCollection items = new MutabakatListCollection();
            JObject data = new JObject();
            DataSet dataSet = null;

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@mutabakatDate",  SqlDbType.DateTime), 
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@instutionId", instutionId)
                                                                 };
                sqlParameters[0].Value = Convert.ToDateTime(mutabakatDate);
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.[get_mutabakat_values_list]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);


                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetMutabakatList(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchMutabakatOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return items;
        }
        /*get_mutabakat_values_list_invoice*/
        public MutabakatListInvoiceCollection GetMutabakatListValuesInvoice(DateTime mutabakatDate, Int64 kioskId, int instutionId, Int64 adminUserId, int processTypeId, int paymentTypeId, int bankNameId)
        {
            MutabakatListInvoiceCollection items = new MutabakatListInvoiceCollection();
            JObject data = new JObject();
            DataSet dataSet = null;

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@mutabakatDate",  SqlDbType.DateTime),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@instutionId", instutionId),
                                                                    new SqlParameter("@processTypeId", processTypeId),
                                                                    new SqlParameter("@paymentTypeId", paymentTypeId),
                                                                    new SqlParameter("@bankNameId", bankNameId)
                                                                 };
                sqlParameters[0].Value = Convert.ToDateTime(mutabakatDate);
                //sqlParameters[6].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_mutabakat_values_list_invoice", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);


                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetMutabakatListInvoice(
                        dataSet.Tables[0].Rows[i],
                        dataSet.Tables[0].Columns, instutionId!=3? 1:0));
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchMutabakatOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return items;
        }


        private MutabakatList GetMutabakatList(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new MutabakatList(Convert.ToInt64(dataRow["TransactionId"])
            , dataRow["AboneNo"].ToString()
            , dataRow["Amount"].ToString()
            , dataRow["InsertionDate"].ToString()
            , dataRow["FaturaIds"].ToString()
            , Convert.ToInt32(dataRow["Status"])
            , dataRow["CompleteStatus"].ToString());
        }

        private MutabakatListInvoice GetMutabakatListInvoice(DataRow dataRow, DataColumnCollection dataColumns,int type)
        {
           
            if (type==0)
            {
                try
                {
                    return new MutabakatListInvoice(
                        Convert.ToInt64(dataRow["TransactionId"])
                        , dataRow["AboneNo"].ToString()
                        , dataRow["Amount"].ToString()
                        , dataRow["InsertionDate"].ToString()
                        , dataRow["FaturaIds"].ToString()
                        , Convert.ToInt32(dataRow["Status"])
                        , dataRow["CompleteStatus"].ToString()
                        , dataRow["BankReferenceNumber"].ToString(), 0
                    //, Convert.ToInt32(dataRow["Category"])
                    );
                }
                catch (Exception)
                {
                    return new MutabakatListInvoice(
                        Convert.ToInt64(dataRow["TransactionId"])
                        , dataRow["AboneNo"].ToString()
                        , dataRow["Amount"].ToString()
                        , dataRow["InsertionDate"].ToString()
                        , dataRow["FaturaIds"].ToString()
                        , Convert.ToInt32(dataRow["Status"])
                        , ""
                        , dataRow["BankReferenceNumber"].ToString()
                        , Convert.ToInt32(dataRow["Category"])
                    );
                }

                
            }
            else  
            {
                try
                {
                    return new MutabakatListInvoice(
                         Convert.ToInt64(dataRow["TransactionId"])
                        , dataRow["AboneNo"].ToString()
                        , dataRow["Amount"].ToString()
                        , dataRow["InsertionDate"].ToString()
                        , dataRow["FaturaIds"].ToString()
                        , Convert.ToInt32(dataRow["Status"])
                        , ""
                        , dataRow["BankReferenceNumber"].ToString()
                        , Convert.ToInt32(dataRow["Category"])
                    );
                }
                catch (Exception)
                {
                    return new MutabakatListInvoice(
                        Convert.ToInt64(dataRow["TransactionId"])
                        , dataRow["AboneNo"].ToString()
                        , dataRow["Amount"].ToString()
                        , dataRow["InsertionDate"].ToString()
                        , dataRow["FaturaIds"].ToString()
                        , Convert.ToInt32(dataRow["Status"])
                        , dataRow["CompleteStatus"].ToString()
                        , dataRow["BankReferenceNumber"].ToString(), 0
                        //, Convert.ToInt32(dataRow["Category"])
                        );
                }
            }
        }

        public AccountCollection GetAccountsForExcel(int instutionId)
        {
            DataSet dataSet = null;
            AccountCollection account = new AccountCollection();

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@instutionId", instutionId) };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_accounts_for_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    account.Add(this.GetAccount(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetAccountsForExcel");
            }
            finally
            {

                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return account;
        }

        public int SaveEmptyKioskMutabakat(int userId, int kioskEmptyId, string inputAmount, string outputAmount, string explanation)
        {
            int success = 0;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@Id",success),
                                                                    new SqlParameter("@userId", userId),
                                                                    new SqlParameter("@kioskEmptyId", kioskEmptyId),
                                                                    new SqlParameter("@inputAmount",inputAmount),
                                                                    new SqlParameter("@outputAmount",outputAmount),
                                                                    new SqlParameter("@explanation",explanation),
                };

                sqlParameters[0].Direction = ParameterDirection.Output;

                int a = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[save_kiosk_empty_mutabakat]", sqlParameters);

                success = Convert.ToInt32(sqlParameters[0].Value);

            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveRole");

            }
            finally
            {
                base.Dispose();
            }
            return success;
        }


        public CodeReportCollections SearchCodeReportForExcelOrderNew(string searchText,
                                       DateTime startDate,
                                       DateTime endDate,
                                       int orderSelectionColumn,
                                       int descAsc,
                                       string kioskId,
                                       int instutionId)
        {
            CodeReportCollections items = new CodeReportCollections();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@searchKioskId", kioskId),
                                                                    new SqlParameter("@instutionId", instutionId)};

                sqlParameters[1].Value = Convert.ToDateTime(startDate);
                sqlParameters[2].Value = Convert.ToDateTime(endDate);
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_code_report_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetCodeReports(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskReportOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }


        public MutabakatCashCountCollection GetMutabakatCashCount(int whichSide, int kioskEmptyId)
        {
            DataSet dataSet = null;
            MutabakatCashCountCollection items = new MutabakatCashCountCollection();

            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@whichSide", whichSide)
                                                                  , new SqlParameter("@kioskEmptyId", kioskEmptyId) };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_mutabakat_cash_count", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);


                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    items.Add(this.GetMutabakatCashCount(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetMutabakatCashCount");
            }
            finally
            {

                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        public AccountDetailCollection GetAccountDetails(int itemId
                                                       , int instutionId
                                                       , int numberOfItemsPerPage
                                                       , int currentPageNum
                                                       , ref int recordCount
                                                       , ref int pageCount)
        {
            DataSet dataSet = null;
            AccountDetailCollection accountDetails = new AccountDetailCollection();

            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount),
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@accountRangeId", itemId),
                                                                    new SqlParameter("@instutionId", instutionId)
                                                                 };
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_account_details", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    accountDetails.Add(this.GetAccountDetail(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetAccountDetails");
            }
            finally
            {

                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return accountDetails;
        }


        public KioskDispenserCashCountCollection SearchKioskDispenserCashCountOrder(string kioskId,
                                                                           string searchText,
                                                                           int instutionId,
                                                                           int numberOfItemsPerPage,
                                                                           int currentPageNum,
                                                                           ref int recordCount,
                                                                           ref int pageCount,
                                                                           int orderSelectionColumn,
                                                                           int descAsc,
                                                                         
                                                                           Int64 adminUserId,
                                                                           int orderType)
        {
            KioskDispenserCashCountCollection items = new KioskDispenserCashCountCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn), 
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    //new SqlParameter("@adminUserId ", adminUserId),
                                                                    new SqlParameter("@institutionId",instutionId),
                                                                    new SqlParameter("@orderType",orderType)
                                                                   
                                                                 };

                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.[search_kiosk_dispenser_cashcount]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                dataSet.Tables[0].Columns.Add("ClassName", typeof(string));
                ColorByValues colorVlaues = new ColorByValues();

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    ClassNameParsers parser = new ClassNameParsers();
                    List<ClassName> classNameList = new List<ClassName>();
                    classNameList.Add(new ClassName("FirstCassetteValue", colorVlaues.ColorByValue(dataSet.Tables[0].Rows[i]["Cassette1Status"].ToString())));
                    classNameList.Add(new ClassName("SecondCassetteValue", colorVlaues.ColorByValue(dataSet.Tables[0].Rows[i]["Cassette2Status"].ToString())));
                    classNameList.Add(new ClassName("ThirdCassetteValue", colorVlaues.ColorByValue(dataSet.Tables[0].Rows[i]["Cassette3Status"].ToString())));
                    classNameList.Add(new ClassName("FourthCassetteValue", colorVlaues.ColorByValue(dataSet.Tables[0].Rows[i]["Cassette4Status"].ToString())));
                    classNameList.Add(new ClassName("FifthCassetteValue", colorVlaues.ColorByValue(dataSet.Tables[0].Rows[i]["Hopper1Status"].ToString())));
                    classNameList.Add(new ClassName("SixthCassetteValue", colorVlaues.ColorByValue(dataSet.Tables[0].Rows[i]["Hopper2Status"].ToString())));
                    classNameList.Add(new ClassName("SeventhCassetteValue", colorVlaues.ColorByValue(dataSet.Tables[0].Rows[i]["Hopper3Status"].ToString())));
                    classNameList.Add(new ClassName("EighthCassetteValue", colorVlaues.ColorByValue(dataSet.Tables[0].Rows[i]["Hopper4Status"].ToString())));
                    parser.ClassName = classNameList;

                    dataSet.Tables[0].Rows[i]["ClassName"] = JsonConvert.SerializeObject(parser);

                    items.Add(this.GetKioskDispenserCashCount2(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
                if (items.Count > 0)
                    items[items.Count - 1].ClassName = "info";

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskDispenserCashCountOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }


        //GetKioskDispenserCashCountOrder
        public CashDispenser GetKioskDispenserCashCountOrder(string Id)
        {
            CashDispenser kiosk = null;
            DataSet dataSet = null;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@Id", Id) };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_dispenser_cashcount", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                if (dataSet.Tables[0].Rows.Count == 1)
                {
                    kiosk = this.GetCashDispenser(dataSet.Tables[0].Rows[0], dataSet.Tables[0].Columns);
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKioskCashDispenser");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return kiosk;
        }



        //**//
        public KioskCashBoxCashCountErrorCollection SearchKioskCashBoxCashCountErrorOrder(string kioskId,
                                                                          string searchText,
                                                                          DateTime startDate,
                                                                          DateTime endDate,
                                                                          int instutionId,
                                                                          int numberOfItemsPerPage,
                                                                          int currentPageNum,
                                                                          ref int recordCount,
                                                                          ref int pageCount,
                                                                          int orderSelectionColumn,
                                                                          int descAsc,

                                                                          Int64 adminUserId,
                                                                          int orderType)
        {
            KioskCashBoxCashCountErrorCollection items = new KioskCashBoxCashCountErrorCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", startDate),  
                                                                    new SqlParameter("@endDate", endDate),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn), 
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    //new SqlParameter("@adminUserId ", adminUserId),
                                                                    new SqlParameter("@institutionId",instutionId),
                                                                    new SqlParameter("@orderType",orderType)
                                                                   
                                                                 };

                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.[search_kiosk_cashbox_cashcount_error]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskCashBoxCashCountError(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

                if (items.Count > 0)
                    items[items.Count - 1].ClassName = "info";

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREKioskCashBoxCashCountErrorOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }


        //GetKioskDispenserMonitoring
        private KioskCashBoxCashCountError GetKioskCashBoxCashCountError(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskCashBoxCashCountError((dataRow["TransactionId"]).ToString()
            , (dataRow["KioskName"]).ToString()
            , dataRow["InsertionDate"].ToString()
            , dataRow["Reject1"].ToString()
            , dataRow["Reject2"].ToString()
            , dataRow["Reject3"].ToString()
            , dataRow["Reject4"].ToString()
            , dataRow["Value1"].ToString()
            , dataRow["Value2"].ToString()
            , dataRow["Value3"].ToString()
            , dataRow["Value4"].ToString()
            , dataRow["Exit1"].ToString()
            , dataRow["Exit2"].ToString()
            , dataRow["Exit3"].ToString()
            , dataRow["Exit4"].ToString()
            , dataRow["Error"].ToString()
            , dataRow["ErrorStatus"].ToString()
            , dataRow["Exception"].ToString()
            , dataRow["TotalLog"].ToString()
            , dataRow["TotalDispensed"].ToString()
            , dataRow["TotalDifference"].ToString()
            , ""
            );
        }


        //SearchKioskDispenserCashCountOrder
        public KioskDispenserMonitoringCollection SearchKioskDispenserMonitoringOrder(string searchText,
                                                                                      int instutionId,
                                                                                      int numberOfItemsPerPage,
                                                                                      int currentPageNum,
                                                                                      ref int recordCount,
                                                                                      ref int pageCount,
                                                                                      int orderSelectionColumn,
                                                                                      int descAsc,
                                                                                      DateTime startDate,
                                                                                      DateTime endDate,
                                                                                      Int64 adminUserId,
                                                                                      int orderType)
        {
            KioskDispenserMonitoringCollection items = new KioskDispenserMonitoringCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] {  new SqlParameter("@recordCount", recordCount), 
                                                                     new SqlParameter("@pageCount", pageCount),
                                                                     new SqlParameter("@searchText", searchText),
                                                                     new SqlParameter("@startDate", startDate), 
                                                                     new SqlParameter("@endDate", endDate), 
                                                                     new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                     new SqlParameter("@currentPageNum", currentPageNum),  
                                                                     new SqlParameter("@orderSelectionColumn", orderSelectionColumn), 
                                                                     new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                     //new SqlParameter("@adminUserId ", adminUserId),
                                                                     new SqlParameter("@institutionId",instutionId),
                                                                     new SqlParameter("@orderType",orderType)
                                                                   
                                                                 };

                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;


                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.[search_kiosk_dispenser_monitoring]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskDispenserMonitoring(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
                if (dataSet.Tables[0].Rows.Count>0)
                {
                    //try
                    //{
                    //    var sumOfFirstDenomNumber = items.Sum(x => Convert.ToDouble(x.FirstDenomNumber));
                    //    var sumOfSecondDenomNumber = items.Sum(x => Convert.ToDouble(x.SecondDenomNumber));
                    //    var sumOfThirdDenomNumber = items.Sum(x => Convert.ToDouble(x.ThirdDenomNumber));
                    //    var sumOfFourthDenomNumber = items.Sum(x => Convert.ToDouble(x.FourthDenomNumber));
                    //    var sumOfFifthDenomNumber = items.Sum(x => Convert.ToDouble(x.FifthDenomNumber));
                    //    var sumOfSixthDenomNumber = items.Sum(x => Convert.ToDouble(x.SixthDenomNumber));
                    //    var sumOfSeventhDenomNumber = items.Sum(x => Convert.ToDouble(x.SeventhDenomNumber));
                    //    var sumOfEighthDenomNumber = items.Sum(x => Convert.ToDouble(x.EighthDenomNumber));
                    //    var sumOfCasette1Reject = items.Sum(x => Convert.ToDouble(x.Casette1Reject));
                    //    var sumOfCasette2Reject = items.Sum(x => Convert.ToDouble(x.Casette2Reject));
                    //    var sumOfCasette3Reject = items.Sum(x => Convert.ToDouble(x.Casette3Reject));
                    //    var sumOfCasette4Reject = items.Sum(x => Convert.ToDouble(x.Casette4Reject));
                    //    var sumOfFirstDenomNumberOld = items.Sum(x => Convert.ToDouble(x.FirstDenomNumberOld));
                    //    var sumOfSecondDenomNumberOld = items.Sum(x => Convert.ToDouble(x.SecondDenomNumberOld));
                    //    var sumOfThirdDenomNumberOld = items.Sum(x => Convert.ToDouble(x.ThirdDenomNumberOld));
                    //    var sumOfFourthDenomNumberOld = items.Sum(x => Convert.ToDouble(x.FourthDenomNumberOld));
                    //    var sumOfFifthDenomNumberOld = items.Sum(x => Convert.ToDouble(x.FifthDenomNumberOld));
                    //    var sumOfSixthDenomNumberOld = items.Sum(x => Convert.ToDouble(x.SixthDenomNumberOld));
                    //    var sumOfSeventhDenomNumberOld = items.Sum(x => Convert.ToDouble(x.SeventhDenomNumberOld));
                    //    var sumOfEighthDenomNumberOld = items.Sum(x => Convert.ToDouble(x.EighthDenomNumberOld));
                    //    var sumOfCasette1RejectOld = items.Sum(x => Convert.ToDouble(x.Casette1RejectOld));
                    //    var sumOfCasette2RejectOld = items.Sum(x => Convert.ToDouble(x.Casette2RejectOld));
                    //    var sumOfCasette3RejectOld = items.Sum(x => Convert.ToDouble(x.Casette3RejectOld));
                    //    var sumOfCasette4RejectOld = items.Sum(x => Convert.ToDouble(x.Casette4RejectOld));


                    //    items.Add(new KioskDispenserMonitoring(0, "", "", ""
                    //        , sumOfFirstDenomNumber.ToString()          
                    //        , sumOfSecondDenomNumber.ToString()
                    //        , sumOfThirdDenomNumber.ToString()
                    //        , sumOfFourthDenomNumber.ToString()
                    //        , sumOfFifthDenomNumber.ToString()
                    //        , sumOfSixthDenomNumber.ToString()
                    //        , sumOfSeventhDenomNumber.ToString()
                    //        , sumOfEighthDenomNumber.ToString()
                    //        , sumOfCasette1Reject.ToString()
                    //        , sumOfCasette2Reject.ToString()
                    //        , sumOfCasette3Reject.ToString()
                    //        , sumOfCasette4Reject.ToString()
                    //        , sumOfFirstDenomNumberOld.ToString()
                    //        , sumOfSecondDenomNumberOld.ToString()
                    //        , sumOfThirdDenomNumberOld.ToString()
                    //        , sumOfFourthDenomNumberOld.ToString()
                    //        , sumOfFifthDenomNumberOld.ToString()
                    //        , sumOfSixthDenomNumberOld.ToString()
                    //        , sumOfSeventhDenomNumberOld.ToString()
                    //        , sumOfEighthDenomNumberOld.ToString()
                    //        , sumOfCasette1RejectOld.ToString()
                    //        , sumOfCasette2RejectOld.ToString()
                    //        , sumOfCasette3RejectOld.ToString()
                    //        , sumOfCasette4RejectOld.ToString()
                    //        , "info"));
                    //}
                    //catch (Exception e)
                    //{

                    //    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), e, "CORESearchKioskDispenserMonitoringOrder");

                    //}


                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskDispenserMonitoringOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        //GetKioskDispenserMonitoring
        private KioskDispenserMonitoring GetKioskDispenserMonitoring(DataRow dataRow, DataColumnCollection dataColumns,string className="")
        {
            return new KioskDispenserMonitoring(Convert.ToInt32(dataRow["Id"])
            , (dataRow["CreatedUser"]).ToString()
            , dataRow["InsertionDate"].ToString()
            , (dataRow["KioskName"]).ToString()
            , dataRow["FirstDenomNumber"].ToString()
            , dataRow["SecondDenomNumber"].ToString()
            , dataRow["ThirdDenomNumber"].ToString()
            , dataRow["FourthDenomNumber"].ToString()
            , dataRow["FifthDenomNumber"].ToString()
            , dataRow["SixthDenomNumber"].ToString()
            , dataRow["SeventhDenomNumber"].ToString()
            , dataRow["EighthDenomNumber"].ToString()
            , dataRow["Casette1Reject"].ToString()
            , dataRow["Casette2Reject"].ToString()
            , dataRow["Casette3Reject"].ToString()
            , dataRow["Casette4Reject"].ToString()
            , dataRow["FirstDenomNumberOld"].ToString()
            , dataRow["SecondDenomNumberOld"].ToString()
            , dataRow["ThirdDenomNumberOld"].ToString()
            , dataRow["FourthDenomNumberOld"].ToString()
            , dataRow["FifthDenomNumberOld"].ToString()
            , dataRow["SixthDenomNumberOld"].ToString()
            , dataRow["SeventhDenomNumberOld"].ToString()
            , dataRow["EighthDenomNumberOld"].ToString()
            , dataRow["Casette1RejectOld"].ToString()
            , dataRow["Casette2RejectOld"].ToString()
            , dataRow["Casette3RejectOld"].ToString()
            , dataRow["Casette4RejectOld"].ToString()
            , className
            );
        }


        public CodeReportCollections SearchCodeDetailsOrder(string searchText,
                                             DateTime startDate,
                                             DateTime endDate,
                                             int numberOfItemsPerPage,
                                             int currentPageNum,
                                             ref int recordCount,
                                             ref int pageCount,
                                             int orderSelectionColumn,
                                             int descAsc,
                                             string kioskId,
                                             int instutionId,
                                             ref Int64 totalTxnCount,
                                             ref string totalGeneratedAskiAmount,
                                             ref string totalUsedAskiAmount,
                                             ref Int64 totalGeneratedAskiCount,
                                             ref Int64 totalUsedAskiCount,
                                             ref string totalReceivedAmount,
                                             ref string totalKioskEmptyAmount,
                                             ref string totalEarnedAmount,
                                             ref string totalParaUstu,
                                             ref string totalSuccessTxnCount,
                                             ref string TotalInstitutionAmount,
                                             ref string TotalCreditCardAmount,
                                             ref string TotalMobilPaymentAmount,
                                             int paymentType,
                                             int orderType,
                                             int processType,
                                             int bankType)
        {
            CodeReportCollections items = new CodeReportCollections();
            DataSet dataSet = null;


            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@searchKioskId", kioskId),
                                                                    new SqlParameter("@TotalTxnCount", totalTxnCount),
                                                                    new SqlParameter("@TotalGeneratedAskiAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalUsedAskiAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalGeneratedAskiCount", totalGeneratedAskiCount),
                                                                    new SqlParameter("@TotalUsedAskiCount", totalUsedAskiCount),
                                                                    new SqlParameter("@TotalReceivedAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalKioskEmptyAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalEarnedAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalParaUstu",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalSuccessTxnCount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@TotalInstitutionAmount",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@instutionId",instutionId),
                                                                    // new SqlParameter("@TotalCreditCardAmount",TotalCreditCardAmount),
                                                                    // new SqlParameter("@TotalMobilPaymentAmount",TotalMobilPaymentAmount),
                                                                    new SqlParameter("@orderType",orderType),
                                                                    new SqlParameter("@paymentType",paymentType),
                                                                    new SqlParameter("@processType",processType),
                                                                    new SqlParameter("@bankType",bankType)
                };

                sqlParameters[3].Value = Convert.ToDateTime(startDate);
                sqlParameters[4].Value = Convert.ToDateTime(endDate);
                //sqlParameters[11].Value = Convert.ToDecimal(totalGeneratedAskiAmount);
                //sqlParameters[12].Value = Convert.ToDecimal(totalUsedAskiAmount);
                //sqlParameters[15].Value = Convert.ToDecimal(totalReceivedAmount);
                //sqlParameters[16].Value = Convert.ToDecimal(totalKioskEmptyAmount);
                //sqlParameters[17].Value = Convert.ToDecimal(totalEarnedAmount);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                sqlParameters[10].Direction = sqlParameters[11].Direction = sqlParameters[12].Direction = sqlParameters[13].Direction = ParameterDirection.Output;
                sqlParameters[14].Direction = sqlParameters[15].Direction = sqlParameters[16].Direction = sqlParameters[17].Direction = ParameterDirection.Output;
                sqlParameters[18].Direction = sqlParameters[19].Direction = ParameterDirection.Output;
                sqlParameters[20].Direction = ParameterDirection.Output;

                //sqlParameters[22].Direction = sqlParameters[23].Direction = ParameterDirection.Output;

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_code_report", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);
                /*
                totalTxnCount = Convert.ToInt32(sqlParameters[10].Value);
                totalGeneratedAskiAmount = sqlParameters[11].Value.ToString();
                totalUsedAskiAmount = sqlParameters[12].Value.ToString();
                totalGeneratedAskiCount = Convert.ToInt32(sqlParameters[13].Value);
                totalUsedAskiCount = Convert.ToInt32(sqlParameters[14].Value);
                totalReceivedAmount = sqlParameters[15].Value.ToString();
                totalKioskEmptyAmount = sqlParameters[16].Value.ToString();
                totalEarnedAmount = sqlParameters[17].Value.ToString();
                totalParaUstu = sqlParameters[18].Value.ToString();
                totalSuccessTxnCount = sqlParameters[19].Value.ToString();
                TotalInstitutionAmount = sqlParameters[20].Value.ToString();
                 * */
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetCodeReports(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

                if (items.Count > 0)
                    items[items.Count - 1].ClassName = "info";

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskReportOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        private Account GetAccount(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new Account(Convert.ToInt32(dataRow["Id"])
            , Convert.ToInt32(dataRow["AccountRangeId"])
            , dataRow["AccountRangeName"].ToString()
            , dataRow["Denizbank"].ToString()
            , dataRow["InstutionMoney"].ToString()
            , dataRow["Merkez"].ToString()
            , dataRow["MoneyCode"].ToString()
            , dataRow["InstutionName"].ToString()
            , Convert.ToInt32(dataRow["InstutionId"]));
        }
        private KioskEmptyStatus GetKioskEmpty(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskEmptyStatus(
                Convert.ToInt32(dataRow["Id"]),
                dataRow["Name"].ToString());

        }


        private UsageFeeDetails GetUsageFeeParser(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new UsageFeeDetails
            {
                KioskName = dataRow["KioskName"].ToString(),
                Total = Convert.ToInt32(dataRow["Total"]),
                TransactionCount = Convert.ToInt32(dataRow["TransactionCount"]),
                UsageFee = Convert.ToDecimal(dataRow["UsageFee"]),
            };

        }

        private UsageFeeDetails GetUsageFeeParser2(DataRow dataRow, DataColumnCollection dataColumns,string className)
        {
            return new UsageFeeDetails
            {
                KioskName = dataRow["KioskName"].ToString(),
                Total = Convert.ToInt32(dataRow["Total"]),
                TransactionCount = Convert.ToInt32(dataRow["TransactionCount"]),
                UsageFee = Convert.ToDecimal(dataRow["UsageFee"]),
                className = className
            };

        }



        private MutabakatCashCount GetMutabakatCashCount(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new MutabakatCashCount(Convert.ToInt32(dataRow["CashTypeId"]),
                        Convert.ToInt32(dataRow["CashCount"].ToString()),
                        (dataRow["CashTypeName"]).ToString(),
                        (dataRow["CashTypeValue"].ToString()),
                        dataRow["CashSum"].ToString() // parser patlıyor ve toplanan para onaylistesi onaylama detayını göremiyoruz.
                        );
        }


        private GetKioskUsersAndPassword GetKioskInfo(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new GetKioskUsersAndPassword(dataRow["BGazUserName"].ToString(),
                        dataRow["BGazUserPass"].ToString(),
                        Convert.ToInt32(dataRow["Id"]),
                        dataRow["Name"].ToString(),
                        dataRow["KartDolumTutar"].ToString(),
                        dataRow["KartDolumSayısı"].ToString(),
                        dataRow["FaturaDolumTutar"].ToString(),
                        dataRow["FaturaSayısı"].ToString()
                        );
        }



        private AccountDetail GetAccountDetail(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new AccountDetail(Convert.ToInt32(dataRow["Id"])
            , Convert.ToInt32(dataRow["TransactionId"])
            , Convert.ToInt32(dataRow["AccountRangeId"])
            , dataRow["AccountRangeName"].ToString()
            , dataRow["Denizbank"].ToString()
            , dataRow["InstutionMoney"].ToString()
            , dataRow["Merkez"].ToString()
            , dataRow["MoneyCode"].ToString()
            , dataRow["TransactionDate"].ToString()
            , dataRow["ReceivedAmount"].ToString()
            , Convert.ToInt32(dataRow["OperationTypeId"])
            , dataRow["OperationTypeName"].ToString()
            , dataRow["InstutionName"].ToString());
        }

        private CodeReports GetCodeReports(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new CodeReports(dataRow["GeneratedTotal"].ToString()
            , dataRow["KioskName"].ToString()
            , dataRow["GeneratedTodayUsableToday"].ToString()
            , dataRow["UsedTotalToday"].ToString()
            , dataRow["GeneratedTodayUsedToday"].ToString()
            , dataRow["GeneratedYesterdayUsedToday"].ToString()
            , dataRow["GeneratedYesterdayUsedTodayCount"].ToString()
            , dataRow["RemainUsableCode"].ToString()
            , dataRow["ResultCount"].ToString()
            , dataRow["CashAmount"].ToString()
            , dataRow["SuccessTxnCount"].ToString()
            , dataRow["InstitutionAmount"].ToString()
            , dataRow["UsageFee"].ToString()
            , dataRow["ParaUstu"].ToString()
            , dataRow["Mutabakat"].ToString()
            , dataRow["CreditCardAmount"].ToString()
            , dataRow["CreditCardCommisionAmount"].ToString()
            , dataRow["MobilePaymentAmount"].ToString()
            , dataRow["MobilePaymentCommisionAmount"].ToString()
            , dataRow["ManuellyGeneratedCodeAmount"].ToString()
            , dataRow["HangedOnMoneyCodeAmount"].ToString()
            , dataRow["DeletedMoneyCodeAmount"].ToString()
            , dataRow["ReverseRecCount"].ToString()
            , dataRow["ReverseRecTotal"].ToString()
            , dataRow["NonReverseRecCount"].ToString()
            , dataRow["NonReverseRecTotal"].ToString()
            , dataRow["RecCount"].ToString()
            , dataRow["RecTotal"].ToString()
            , ""
            );
        }



        public int DeleteControlCashNotification(int id, int status)
        {
            int resp = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] {
                                                                    new SqlParameter("@id", id),
                                                                    new SqlParameter("@status", status),
                                                                
                                                                     //new SqlParameter("@approvedCodeapprovedCode", approvedCodeapprovedCode)
                                                                    };

                //[pk].[delete_kiosk_cash_control_notification] 
                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[delete_kiosk_cash_control_notification]", sqlParameters);
                resp = 1;

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveControlCashNotification");
                resp = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
            }
            finally
            {
                base.Dispose();
            }

            return resp;
        }



        public int SaveControlCashNotification(int kioskId, int expertUserId,
                                                                              string firstDenomNumber,
                                                                              string secondDenomNumber,
                                                                              string thirdDenomNumber,
                                                                              string fourthDenomNumber,
                                                                              string fifthDenomNumber,
                                                                              string sixthDenomNumber,
                                                                              string seventhDenomNumber,
                                                                              string eighthDenomNumber,
                                                                              string ninthDenomNumber,
                                                                              string tenthDenomNumber,
                                                                              string eleventhDenomNumber,
                                                                              decimal totalAmount,
                                                                              string desmerId
                                                                                //, string approvedCodeapprovedCode
                                                                                )
        {
            int resp = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { 
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@expertUserId", expertUserId),                                                                  
                                                                    new SqlParameter("@returnCode", 0),
                                                                    new SqlParameter("@firstDenomNumber", firstDenomNumber),
                                                                    new SqlParameter("@secondDenomNumber", secondDenomNumber),
                                                                    new SqlParameter("@thirdDenomNumber", thirdDenomNumber),
                                                                    new SqlParameter("@fourthDenomNumber", fourthDenomNumber),
                                                                    new SqlParameter("@fifthDenomNumber", fifthDenomNumber),
                                                                    new SqlParameter("@sixthDenomNumber", sixthDenomNumber),
                                                                    new SqlParameter("@seventhDenomNumber", seventhDenomNumber),
                                                                    new SqlParameter("@eighthDenomNumber", eighthDenomNumber),
                                                                    new SqlParameter("@ninthDenomNumber", ninthDenomNumber),
                                                                    new SqlParameter("@tenthDenomNumber", tenthDenomNumber),
                                                                    new SqlParameter("@eleventhDenomNumber", eleventhDenomNumber),
                                                                    new SqlParameter("@timing", DateTime.Now),
                                                                    new SqlParameter("@totalAmount", totalAmount),
                                                                    new SqlParameter("@desmerId", desmerId)
                                                                     //new SqlParameter("@approvedCodeapprovedCode", approvedCodeapprovedCode)
                                                                    };

                sqlParameters[2].Direction = ParameterDirection.InputOutput;
                resp = Convert.ToInt32(sqlParameters[2].Value);

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[insert_kiosk_cash_control_notification]", sqlParameters);
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveControlCashNotification");
                resp = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
            }
            finally
            {
                base.Dispose();
            }

            return resp;
        }


        //inputListUpdateControlCashNotification
        //UpdateCutOffMonitoring
        public int UpdateControlCashNotification(string controlCashId, int expertUserId,
                                                                              string firstDenomNumber,
                                                                              string secondDenomNumber,
                                                                              string thirdDenomNumber,
                                                                              string fourthDenomNumber,
                                                                              string fifthDenomNumber,
                                                                              string sixthDenomNumber,
                                                                              string seventhDenomNumber,
                                                                              string eighthDenomNumber,
                                                                              string ninthDenomNumber,
                                                                              string tenthDenomNumber,
                                                                              string eleventhDenomNumber,
                                                                              decimal totalAmount)
        {
            int resp = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] {  
                                                                    new SqlParameter("@controlCashId", controlCashId),
                                                                    new SqlParameter("@expertUserId", expertUserId),                                                                  
                                                                    new SqlParameter("@returnCode", 0),
                                                                    new SqlParameter("@firstDenomNumber", firstDenomNumber),
                                                                    new SqlParameter("@secondDenomNumber", secondDenomNumber),
                                                                    new SqlParameter("@thirdDenomNumber", thirdDenomNumber),
                                                                    new SqlParameter("@fourthDenomNumber", fourthDenomNumber),
                                                                    new SqlParameter("@fifthDenomNumber", fifthDenomNumber),
                                                                    new SqlParameter("@sixthDenomNumber", sixthDenomNumber),
                                                                    new SqlParameter("@seventhDenomNumber", seventhDenomNumber),
                                                                    new SqlParameter("@eighthDenomNumber", eighthDenomNumber),
                                                                    new SqlParameter("@ninthDenomNumber", ninthDenomNumber),
                                                                    new SqlParameter("@tenthDenomNumber", tenthDenomNumber),
                                                                    new SqlParameter("@eleventhDenomNumber", eleventhDenomNumber),
                                                                    new SqlParameter("@timing", DateTime.Now),
                                                                    new SqlParameter("@totalAmount", totalAmount)
                                                                
                                                                    };

                sqlParameters[2].Direction = ParameterDirection.InputOutput;
                resp = Convert.ToInt32(sqlParameters[2].Value);

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[update_kiosk_cash_control_notification]", sqlParameters);
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREUpdateControlCashNotification");
                resp = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
            }
            finally
            {
                base.Dispose();
            }

            return resp;
        }


        //UpdateDispenserCashCount
        public int UpdateDispenserCashCountOrder(int kioskId, int expertUserId,
                                                                              string firstDenomNumber,
                                                                              string secondDenomNumber,
                                                                              string thirdDenomNumber,
                                                                              string fourthDenomNumber,
                                                                              string fifthDenomNumber,
                                                                              string sixthDenomNumber,
                                                                              string seventhDenomNumber,
                                                                              string eighthDenomNumber,
                                                                              string ninthDenomNumber,
                                                                              string tenthDenomNumber,
                                                                              string eleventhDenomNumber,
                                                                              string twelfthDenomNumber,
                                                                              int cassette1Status,
                                                                              int cassette2Status,
                                                                              int cassette3Status,
                                                                              int cassette4Status,
                                                                              int hopper1Status,
                                                                              int hopper2Status,
                                                                              int hopper3Status,
                                                                              int hopper4Status,
                                                                              decimal totalAmount
            //, string approvedCodeapprovedCode
                                                                                )
        {
            int resp = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { 
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    //new SqlParameter("@expertUserId", expertUserId),                                                                  
                                                                    new SqlParameter("@returnCode", 0),
                                                                    new SqlParameter("@firstDenomNumber", firstDenomNumber),
                                                                    new SqlParameter("@secondDenomNumber", secondDenomNumber),
                                                                    new SqlParameter("@thirdDenomNumber", thirdDenomNumber),
                                                                    new SqlParameter("@fourthDenomNumber", fourthDenomNumber),
                                                                    new SqlParameter("@fifthDenomNumber", fifthDenomNumber),
                                                                    new SqlParameter("@sixthDenomNumber", sixthDenomNumber),
                                                                    new SqlParameter("@seventhDenomNumber", seventhDenomNumber),
                                                                    new SqlParameter("@eighthDenomNumber", eighthDenomNumber),
                                                                    new SqlParameter("@ninthDenomNumber", ninthDenomNumber),
                                                                    new SqlParameter("@tenthDenomNumber", tenthDenomNumber),
                                                                    new SqlParameter("@eleventhDenomNumber", eleventhDenomNumber),
                                                                    new SqlParameter("@twelfthDenomNumber", twelfthDenomNumber),
                                                                    new SqlParameter("@cassette1Status", cassette1Status),
                                                                    new SqlParameter("@cassette2Status", cassette2Status),
                                                                    new SqlParameter("@cassette3Status", cassette3Status),
                                                                    new SqlParameter("@cassette4Status", cassette4Status),
                                                                    new SqlParameter("@hopper1Status", hopper1Status),
                                                                    new SqlParameter("@hopper2Status", hopper2Status),
                                                                    new SqlParameter("@hopper3Status", hopper3Status),
                                                                    new SqlParameter("@hopper4Status", hopper4Status),
                                                                    ///new SqlParameter("@timing", DateTime.Now),
                                                                    //new SqlParameter("@totalAmount", totalAmount)
                                                                    //new SqlParameter("@approvedCodeapprovedCode", approvedCodeapprovedCode)
                                                                    };

                sqlParameters[1].Direction = ParameterDirection.InputOutput;
                resp = Convert.ToInt32(sqlParameters[1].Value);

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[update_kiosk_dispenser_cashcount_with_status]", sqlParameters);
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREUpdateDispenserCashCountOrder");
                resp = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
            }
            finally
            {
                base.Dispose();
            }

            return resp;
        }

        //SaveDispenserCashCountOrder
        public int SaveDispenserCashCountOrder(int kioskId,
                                              Int64 adminUserId,
                                              string firstDenomNumber,
                                              string firstDenomNumberOld,
                                              string secondDenomNumber,
                                              string secondDenomNumberOld,
                                              string thirdDenomNumber, string thirdDenomNumberOld,
                                              string fourthDenomNumber, string fourthDenomNumberOld,
                                              string fifthDenomNumber, string fifthDenomNumberOld,
                                              string sixthDenomNumber, string sixthDenomNumberOld,
                                              string seventhDenomNumber, string seventhDenomNumberOld,
                                              string eighthDenomNumber, string eighthDenomNumberOld,
                                              string ninthDenomNumber, string ninthDenomNumberOld,
                                              string tenthDenomNumber, string tenthDenomNumberOld,
                                              string eleventhDenomNumber, string eleventhDenomNumberOld,
                                              string twelfthDenomNumber, string twelfthDenomNumberOld  
                                                )
        {
            int updateStatus = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { 
                                                                    new SqlParameter("@kioskId", kioskId),                                           
                                                                    new SqlParameter("@returnCode", 0),
                                                                    new SqlParameter("@expertUserId", adminUserId),  
                                                                    new SqlParameter("@insertionDate", DateTime.Now),  
                                                                    new SqlParameter("@firstDenomNumber", firstDenomNumber),
                                                                    new SqlParameter("@secondDenomNumber", secondDenomNumber),
                                                                    new SqlParameter("@thirdDenomNumber", thirdDenomNumber),
                                                                    new SqlParameter("@fourthDenomNumber", fourthDenomNumber),
                                                                    new SqlParameter("@fifthDenomNumber", fifthDenomNumber),
                                                                    new SqlParameter("@sixthDenomNumber", sixthDenomNumber),
                                                                    new SqlParameter("@seventhDenomNumber", seventhDenomNumber),
                                                                    new SqlParameter("@eighthDenomNumber", eighthDenomNumber),
                                                                    new SqlParameter("@ninthDenomNumber", ninthDenomNumber),
                                                                    new SqlParameter("@tenthDenomNumber", tenthDenomNumber),
                                                                    new SqlParameter("@eleventhDenomNumber", eleventhDenomNumber),
                                                                    new SqlParameter("@twelfthDenomNumber", twelfthDenomNumber),
                                                                    new SqlParameter("@firstDenomNumberOld", firstDenomNumberOld),
                                                                    new SqlParameter("@secondDenomNumberOld", secondDenomNumberOld),
                                                                    new SqlParameter("@thirdDenomNumberOld", thirdDenomNumberOld),
                                                                    new SqlParameter("@fourthDenomNumberOld", fourthDenomNumberOld),
                                                                    new SqlParameter("@fifthDenomNumberOld", fifthDenomNumberOld),
                                                                    new SqlParameter("@sixthDenomNumberOld", sixthDenomNumberOld),
                                                                    new SqlParameter("@seventhDenomNumberOld", seventhDenomNumberOld),
                                                                    new SqlParameter("@eighthDenomNumberOld", eighthDenomNumberOld),
                                                                    new SqlParameter("@ninthDenomNumberOld", ninthDenomNumberOld),
                                                                    new SqlParameter("@tenthDenomNumberOld", tenthDenomNumberOld),
                                                                    new SqlParameter("@eleventhDenomNumberOld", eleventhDenomNumberOld),
                                                                    new SqlParameter("@twelfthDenomNumberOld", twelfthDenomNumberOld),
                                                                    ///new SqlParameter("@timing", DateTime.Today),
                                                                    //new SqlParameter("@totalAmount", totalAmount)
                                                                    //new SqlParameter("@approvedCodeapprovedCode", approvedCodeapprovedCode)
                                                                    };

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "pk.add_kiosk_dispenser_cashcount", sqlParameters);

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveDispenserCashCountOrder");
                updateStatus = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
            }
            finally
            {
                base.Dispose();
            }
            return updateStatus;
        }

        //SearchKioskDispenserMonitoringOrder
        public DailyAccountReportCollection SearchKioskDispenserMonitoringOrder(DateTime startDate,
                                             DateTime endDate,
                                             int numberOfItemsPerPage,
                                             int currentPageNum,
                                             ref int recordCount,
                                             ref int pageCount,
                                             int orderSelectionColumn,
                                             int descAsc,
                                             int orderType)
        {
            DailyAccountReportCollection items = new DailyAccountReportCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@orderType", orderType)
                                                                 };
                sqlParameters[2].Value = Convert.ToDateTime(startDate);
                sqlParameters[3].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_daily_account_report_order", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetDailyAccountReport(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchDailyAccountReportOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }



        public KioskCommisionSettingCollection GetKioskCommisionSettingsOrder(int expertUserId
                                                    //ref int creditAski,
                                                    //     ref int mobileAski,
                                                    //     ref int cashAski,
                                                    //     ref int yellowAski,
                                                    //     ref int redAski,
                                                    //     ref int creditBaskent,
                                                    //     ref int mobileBaskent,
                                                    //     ref int cashBaskent,
                                                    //     ref int yellowBaskent,
                                                    //     ref int redBaskent,
                                                    //     ref int creditAksaray,
                                                    //     ref int mobileAksaray,
                                                    //     ref int cashAksaray,
                                                    //     ref int yellowAksaray,
                                                    //     ref int redAksaray
                                                        )
        {
            
            KioskCommisionSettingCollection commisions = new KioskCommisionSettingCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { 
                                                                    new SqlParameter("@expertUserId", expertUserId)                                                                  
                                                                    //new SqlParameter("@returnCode", 0)
                                                                     //new SqlParameter("@creditAski"     , 0),
                                                                     //new SqlParameter("@mobileAski"     , 0),
                                                                     //new SqlParameter("@cashAski"       , 0),
                                                                     //new SqlParameter("@yellowAski"     , 0),
                                                                     //new SqlParameter("@redAski"        , 0),
                                                                     //new SqlParameter("@creditBaskent"  , 0),
                                                                     //new SqlParameter("@mobileBaskent"  , 0),
                                                                     //new SqlParameter("@cashBaskent"    , 0),
                                                                     //new SqlParameter("@yellowBaskent"  , 0),
                                                                     //new SqlParameter("@redBaskent"     , 0),
                                                                     //new SqlParameter("@creditAksaray"  , 0),
                                                                     //new SqlParameter("@mobileAksaray"  , 0),
                                                                     //new SqlParameter("@cashAksaray"    , 0),
                                                                     //new SqlParameter("@yellowAksaray"  , 0),
                                                                     //new SqlParameter("@redAksaray"     , 0)
                                                                     
                                                                    };



                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[get_commision_settings_parameters]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                //creditAski = Convert.ToInt32((sqlParameters[1].Value));
                //mobileAski = Convert.ToInt32((sqlParameters[2].Value));
                //cashAski = Convert.ToInt32((sqlParameters[3].Value));
                //yellowAski = Convert.ToInt32((sqlParameters[4].Value));
                //redAski = Convert.ToInt32((sqlParameters[5].Value));
                //
                //
                //creditBaskent = Convert.ToInt32((sqlParameters[6].Value));
                //mobileBaskent = Convert.ToInt32((sqlParameters[7].Value));
                //cashBaskent = Convert.ToInt32((sqlParameters[8].Value));
                //yellowBaskent = Convert.ToInt32((sqlParameters[9].Value));
                //redBaskent = Convert.ToInt32((sqlParameters[10].Value));
                //
                //
                //creditAksaray = Convert.ToInt32((sqlParameters[11].Value));
                //mobileAksaray = Convert.ToInt32((sqlParameters[12].Value));
                //cashAksaray = Convert.ToInt32((sqlParameters[13].Value));
                //yellowAksaray = Convert.ToInt32((sqlParameters[14].Value));
                //redAksaray = Convert.ToInt32((sqlParameters[15].Value));
                
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    commisions.Add(this.GetKioskCommisionSetting(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKioskCommisionSettings");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return commisions;
        }

        private KioskCommisionSetting GetKioskCommisionSetting(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskCommisionSetting( 
             dataRow["CreditCommission"].ToString()
            , dataRow["MobileCommission"].ToString()
            , dataRow["CashCommission"].ToString()
            , dataRow["YellowAlarm"].ToString() 
            , dataRow["RedAlarm"].ToString()
            , dataRow["InsName"].ToString()
            , dataRow["InstutionId"].ToString()
            );
        }

   
        public int KioskCommisionSettingsOrder(int instutionId,
                                               ref int creditCommision,
                                               ref int mobileCommision,
                                               ref int cashCommision,
                                               ref int yellowAlarm,
                                               ref int redAlarm,
                                               ref string instutionName)
        {
            int resp = 0;

            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { 
                                                                    //new SqlParameter("@expertUserId", expertUserId),                                                                  
                                                                    //new SqlParameter("@returnCode", 0)
                                                                     new SqlParameter("@creditCommision", 0),
                                                                     new SqlParameter("@mobileCommision", 0),
                                                                     new SqlParameter("@cashCommision"  , 0),
                                                                     new SqlParameter("@yellowAlarm"    , 0),
                                                                     new SqlParameter("@redAlarm"       , 0),
                                                                     new SqlParameter("@instutionId"    , instutionId),
                                                                     new SqlParameter("@instutionName"    , SqlDbType.VarChar,200)
                                                                     
                                                                    };


                sqlParameters[0].Direction = sqlParameters[1].Direction = sqlParameters[2].Direction =
                sqlParameters[3].Direction = sqlParameters[4].Direction = sqlParameters[6].Direction = ParameterDirection.Output;

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[commision_settings_parameters]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);



                creditCommision = Convert.ToInt32((sqlParameters[0].Value));
                mobileCommision = Convert.ToInt32((sqlParameters[1].Value));
                cashCommision = Convert.ToInt32((sqlParameters[2].Value));
                yellowAlarm = Convert.ToInt32((sqlParameters[3].Value));
                redAlarm = Convert.ToInt32((sqlParameters[4].Value));
                instutionName = (sqlParameters[6].Value.ToString());


            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREKioskCommisionSettingsOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return resp;
        }


        //UpdateKioskCommisionSettingsOrder
        public int UpdateKioskCommisionSettingsOrder(int instutionId
                                                          , string creditCommision
                                                          , string mobileCommision
                                                          , string cashCommision
                                                          , string yellowAlarm
                                                          , string redAlarm
                                                          , Int64 expertUserId
                                                         )
        {
            int resp = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@instutionId", instutionId),       
                                                                    new SqlParameter("@expertUserId", expertUserId),                                                                  
                                                                    new SqlParameter("@result", 0),
                                                                     new SqlParameter("@creditCommision", creditCommision),
                                                                    new SqlParameter("@mobileCommision", mobileCommision),
                                                                    new SqlParameter("@cashCommision", cashCommision),
                                                                   new SqlParameter("@yellowAlarm", yellowAlarm),
                                                                    new SqlParameter("@redAlarm", redAlarm)
                                                                
                                                               
                                                                
                                                                    };

                sqlParameters[2].Direction = ParameterDirection.InputOutput;
                resp = Convert.ToInt32(sqlParameters[2].Value);

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[update_commision_settings_parameters]", sqlParameters);
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREUpdateKioskCommisionSettings");
                resp = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
            }
            finally
            {
                base.Dispose();
            }

            return resp;
        }


        public int GetKioskSettings(int expertUserId,
                                                         ref string billFieldNotificationLimit,
                                                         ref int isNotificationLimitReceiptActive,
                                                         ref string cashBoxNotificationLimit,
                                                         ref int isNotificationCashBoxLimitActive,
                                                         ref int activeYellowAlarmBox,
                                                         ref int activeRedAlarmBox,
                                                         ref string moneyCodeGenerationLimit,
                                                         ref string moneyCodeGenerationCountLimit,
                                                         ref string oneyCodeGenerationCountLimitPeriod,
                                                         ref string bGazUpperLoadingLimit
                   )
        {
            int resp = 0;

            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { 
                                                                    new SqlParameter("@expertUserId", expertUserId),                                                                  
                                                                    //new SqlParameter("@returnCode", 0)
                                                                    new SqlParameter("@billFieldNotificationLimits", 0),
                                                                    new SqlParameter("@isNotificationLimitReceiptActive", isNotificationLimitReceiptActive),
                                                                    new SqlParameter("@cashBoxNotificationLimit", 0),
                                                                    new SqlParameter("@isNotificationCashBoxLimitActive", isNotificationCashBoxLimitActive),
                                                                    new SqlParameter("@isYellowAlertActive", activeYellowAlarmBox),
                                                                    new SqlParameter("@isRedAlertActive", activeRedAlarmBox),
                                                                    new SqlParameter("@moneyCodeGenerationLimit", 0),
                                                                    new SqlParameter("@moneyCodeGenerationCountLimit", 0 ),
                                                                    new SqlParameter("@oneyCodeGenerationCountLimitPeriod", 0 ),
                                                                    new SqlParameter("@bGazUpperLoadingLimit", 0 )
                                                                     
                                                                    };


                sqlParameters[1].Direction = sqlParameters[2].Direction = sqlParameters[3].Direction =
                sqlParameters[4].Direction = sqlParameters[5].Direction = sqlParameters[6].Direction =
                sqlParameters[7].Direction = sqlParameters[8].Direction = sqlParameters[9].Direction =
                sqlParameters[10].Direction = ParameterDirection.Output;

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[get_settings_parameters]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);



                billFieldNotificationLimit = ((sqlParameters[1].Value)).ToString();
                isNotificationLimitReceiptActive = Convert.ToInt32((sqlParameters[2].Value));
                cashBoxNotificationLimit = ((sqlParameters[3].Value)).ToString();
                isNotificationCashBoxLimitActive = Convert.ToInt32((sqlParameters[4].Value));
                activeYellowAlarmBox = Convert.ToInt32((sqlParameters[5].Value));
                activeRedAlarmBox = Convert.ToInt32((sqlParameters[6].Value));

                decimal moneyCodeGenerationLimit2 = (Convert.ToDecimal(sqlParameters[7].Value));
                moneyCodeGenerationLimit = moneyCodeGenerationLimit2.ToString();

                decimal moneyCodeGenerationCountLimit2 = (Convert.ToDecimal(sqlParameters[8].Value));
                moneyCodeGenerationCountLimit = moneyCodeGenerationCountLimit2.ToString();

                decimal oneyCodeGenerationCountLimitPeriod2 = (Convert.ToDecimal(sqlParameters[9].Value));
                oneyCodeGenerationCountLimitPeriod = oneyCodeGenerationCountLimitPeriod2.ToString();

                decimal bGazUpperLoadingLimit2 = (Convert.ToDecimal(sqlParameters[10].Value));
                bGazUpperLoadingLimit = bGazUpperLoadingLimit2.ToString();


            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetStatus");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return resp;
        }


        public int SaveKioskSettings(int adminUserId,
                                           string billFieldNotificationLimit,
                                           int isNotificationLimitReceiptActive,
                                           string cashBoxNotificationLimit,
                                           int isNotificationCashBoxLimitActive,
                                           int activeYellowAlarmBox,
                                           int activeRedAlarmBox,
                                           string moneyCodeGenerationLimit,
                                           string moneyCodeGenerationCountLimit,
                                           string oneyCodeGenerationCountLimitPeriod,
                                           string bGazUpperLoadingLimit)
        {
            int success = 0;

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@result",success),
                                                                    new SqlParameter("@adminUserId", adminUserId),
                                                                    new SqlParameter("@billFieldNotificationLimits", Convert.ToInt32(billFieldNotificationLimit)),
                                                                    new SqlParameter("@isNotificationLimitReceiptActive", isNotificationLimitReceiptActive),
                                                                    new SqlParameter("@cashBoxNotificationLimit", Convert.ToInt32(cashBoxNotificationLimit)),
                                                                    new SqlParameter("@isNotificationCashBoxLimitActive", isNotificationCashBoxLimitActive),
                                                                    new SqlParameter("@isYellowAlertActive", activeYellowAlarmBox),
                                                                    new SqlParameter("@isRedAlertActive", activeRedAlarmBox),
                                                                    new SqlParameter("@moneyCodeGenerationLimit", Convert.ToInt32(moneyCodeGenerationLimit)),
                                                                    new SqlParameter("@moneyCodeGenerationCountLimit", Convert.ToInt32(moneyCodeGenerationCountLimit) ),
                                                                    new SqlParameter("@oneyCodeGenerationCountLimitPeriod", Convert.ToInt32(oneyCodeGenerationCountLimitPeriod) ),
                                                                    new SqlParameter("@bGazUpperLoadingLimit", Convert.ToInt32(bGazUpperLoadingLimit) )
                                                                     
                };

                sqlParameters[0].Direction = ParameterDirection.Output;

                int a = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[set_settings_parameters]", sqlParameters);

                success = Convert.ToInt32(sqlParameters[0].Value);

            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveSettings");

            }
            finally
            {
                base.Dispose();
            }
            return success;

        }


        //SaveCommisionSettingsOrder

        public int SaveCommisionSettingsOrder(int adminUserId,
                                           string creditAski,
                                           string mobileAski,
                                           string cashAski,
                                           string yellowAski,
                                           string redAski,
                                           string creditBaskent,
                                           string mobileBaskent,
                                           string cashBaskent,
                                           string yellowBaskent,
                                           string redBaskent,
                                           string creditAksaray,
                                           string mobileAksaray,
                                           string cashAksaray,
                                           string yellowAksaray,
                                           string redAksaray)
        {
            int success = 0;

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@result",success),
                                                                    new SqlParameter("@adminUserId", adminUserId),
                                                                    new SqlParameter("@creditAski", creditAski),
                                                                    new SqlParameter("@mobileAski", mobileAski),
                                                                    new SqlParameter("@cashAski", cashAski),
                                                                    new SqlParameter("@yellowAski", yellowAski),
                                                                    new SqlParameter("@redAski", redAski),
                                                                    new SqlParameter("@creditBaskent", creditBaskent),
                                                                    new SqlParameter("@mobileBaskent",  mobileBaskent),
                                                                    new SqlParameter("@cashBaskent", cashBaskent ),
                                                                    new SqlParameter("@yellowBaskent", yellowBaskent),
                                                                    new SqlParameter("@redBaskent", redBaskent ),
                                                                    new SqlParameter("@creditAksaray", creditAksaray),
                                                                    new SqlParameter("@mobileAksaray",  mobileAksaray),
                                                                    new SqlParameter("@cashAksaray", cashAksaray ),
                                                                    new SqlParameter("@yellowAksaray", yellowAksaray),
                                                                    new SqlParameter("@redAksaray", redAksaray ),
                                                                     
                };

                sqlParameters[0].Direction = ParameterDirection.Output;

                //int a = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[set_commision_settings_parameters]", sqlParameters);

                success = Convert.ToInt32(sqlParameters[0].Value);

            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveSettings");

            }
            finally
            {
                base.Dispose();
            }
            return success;

        }




        public StatusCollection GetStatusOrder()
        {
            StatusCollection status = new StatusCollection();
            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_mutabakat_status_names", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    status.Add(this.GetStatus(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetStatus");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return status;
        }

        private Status GetStatus(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new Status(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }


        public int SaveKioskEmptyCashCounts(DataTable dt, int kioskId, int expertUserId, string code, int emptyKioskId, int desmerId)
        {
            int resp = 0;
            try
            {
                DataSet dataSet = null;

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@code", code), 
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@expertUserId", expertUserId), 
                                                                    new SqlParameter("@kioskEmptyId", emptyKioskId), 
                                                                    new SqlParameter("@recordCount", 0),
                                                                    new SqlParameter("@desmerId", desmerId) };

                sqlParameters[4].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[check_empty_kiosk_code]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                int recordCount = Convert.ToInt32(sqlParameters[4].Value);

                sqlParameters = null;

                if (recordCount > 0)
                {
                    sqlParameters = new SqlParameter[] {new SqlParameter("@kioskId", kioskId),
                                                        new SqlParameter("@expertUserId", expertUserId), 
                                                        new SqlParameter("@emptyKioskId", emptyKioskId),
                                                        new SqlParameter("@recordCount", 0) ,
                                                        new SqlParameter("@desmerId", desmerId) 
                    };

                    sqlParameters[3].Direction = ParameterDirection.InputOutput;

                    dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[delete_empty_kiosk_cash_count]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                    SqlDataAdapter pSqlda = null;
                    SqlCommand sqlCmd = null;

                    pSqlda = new SqlDataAdapter(sqlCmd);

                    sqlCmd = new SqlCommand
                    {
                        Connection = base.GetConnection(),
                        CommandText = "[pk].[save_kiosk_empty_cash_count]",
                        CommandType = CommandType.StoredProcedure,
                        UpdatedRowSource = UpdateRowSource.None
                    };


                    sqlCmd.Parameters.Add("@KioskId", SqlDbType.Int).SourceColumn = "KioskId";
                    sqlCmd.Parameters.Add("@ExpertUserId", SqlDbType.Int).SourceColumn = "ExpertUserId";
                    sqlCmd.Parameters.Add("@CashTypeId", SqlDbType.Int).SourceColumn = "CashTypeId";
                    sqlCmd.Parameters.Add("@Count", SqlDbType.Int).SourceColumn = "Count";
                    sqlCmd.Parameters.Add("@KioskEmptyId", SqlDbType.Int).SourceColumn = "KioskEmptyId";
                    sqlCmd.Parameters.Add("@DesmerId", SqlDbType.Int).SourceColumn = "DesmerId";

                    pSqlda.InsertCommand = sqlCmd;
                    pSqlda.UpdateBatchSize = 0; // 0 alabileceği max kadar alıyor. 300- 500 gibi degerlerde verilebilir.

                    int records = pSqlda.Update(dt);

                    if (records >= 0)
                    {
                        resp = 0;
                    }
                    else
                    {
                        resp = -2;
                    }
                }
                else
                {
                    resp = -1;
                }
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveKioskEmptyCashCounts");
                resp = -3;
            }
            finally
            {
                base.Dispose();

            }
            return resp;
        }



        public DailyAccountReportCollection SearchDailyAccountReportOrder(DateTime startDate,
                                             DateTime endDate,
                                             int numberOfItemsPerPage,
                                             int currentPageNum,
                                             ref int recordCount,
                                             ref int pageCount,
                                             int orderSelectionColumn,
                                             int descAsc,
                                             int orderType)
        {
            DailyAccountReportCollection items = new DailyAccountReportCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@orderType", orderType)
                                                                 };
                sqlParameters[2].Value = Convert.ToDateTime(startDate);
                sqlParameters[3].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_daily_account_report_order", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetDailyAccountReport(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchDailyAccountReportOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        public AutoMutabakatCollection SearchAutoMutabakatOrder(string searchText,
                                     DateTime startDate,
                                     DateTime endDate,
                                     int numberOfItemsPerPage,
                                     int currentPageNum,
                                     ref int recordCount,
                                     ref int pageCount,
                                     int orderSelectionColumn,
                                     int descAsc,
                                     int UserId,
                                     string AdminUserId,
                                     int instutionId,
                                     int State
                                    , int orderType)
        {
            AutoMutabakatCollection items = new AutoMutabakatCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@UserId", UserId),
                                                                    new SqlParameter("@AdminUserId", AdminUserId),
                                                                    new SqlParameter("@instutionId", instutionId),
                                                                    new SqlParameter("@State", State),
                                                                    new SqlParameter("@orderType", orderType)
                                                                 };
                sqlParameters[3].Value = Convert.ToDateTime(startDate);
                sqlParameters[4].Value = Convert.ToDateTime(endDate);
                sqlParameters[1].Direction = sqlParameters[2].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_auto_mutabakat_report_order", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[1].Value);
                pageCount = Convert.ToInt32(sqlParameters[2].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetAutoMutabakatReport(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchDailyAccountReportOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }


        public DailyAccountReportProkisCollection SearchDailyAccountReportProkisOrder(DateTime startDate,
                                            DateTime endDate,
                                            int numberOfItemsPerPage,
                                            int currentPageNum,
                                            ref int recordCount,
                                            ref int pageCount,
                                            int orderSelectionColumn,
                                            int descAsc,
                                            int orderType)
        {
            DailyAccountReportProkisCollection items = new DailyAccountReportProkisCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@orderType", orderType)
                                                                 };
                sqlParameters[2].Value = Convert.ToDateTime(startDate);
                sqlParameters[3].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_daily_account_report_order_prokis", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetDailyAccountReportProkis(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchDailyAccountReportOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        public KioskCashAcceptorCollection SearchKioskCashAcceptorOrder(DateTime startDate,
                                         DateTime endDate,
                                         int numberOfItemsPerPage,
                                         int currentPageNum,
                                         ref int recordCount,
                                         ref int pageCount,
                                         int orderSelectionColumn,
                                         int descAsc,
                                         int institutionId,
                                         string kioskId,
                                         int orderType)
        {
            KioskCashAcceptorCollection items = new KioskCashAcceptorCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@institutionId", institutionId),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@orderType", orderType)
                                                                 };
                sqlParameters[2].Value = Convert.ToDateTime(startDate);
                sqlParameters[3].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_acceptor_logs", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    if( i == dataSet.Tables[0].Rows.Count - 1)
                    { //alttaki toplam alanı için yapıldı
                        items.Add(this.GetKioskCashAcceptor(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns,"info"));

                    }
                    else
                    {
                        items.Add(this.GetKioskCashAcceptor(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));

                    }
                }


             

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREKioskCashAcceptorOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        public KioskCashDispenserCollection SearchKioskCashDispenserOrder(DateTime startDate,
                                        DateTime endDate,
                                        int numberOfItemsPerPage,
                                        int currentPageNum,
                                        ref int recordCount,
                                        ref int pageCount,
                                        int orderSelectionColumn,
                                        int descAsc,
                                        int institutionId,
                                        string kioskId,
                                        int orderType)
        {
            KioskCashDispenserCollection items = new KioskCashDispenserCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@institutionId", institutionId),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@orderType", orderType)
                                                                 };
                sqlParameters[2].Value = Convert.ToDateTime(startDate);
                sqlParameters[3].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_dispenser_logs", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskCashDispenser(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREKioskCashDispenserOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }


        public KioskCashDispenserCollection SearchKioskCashDispenserNewOrder(DateTime startDate,
                                        DateTime endDate,
                                        int numberOfItemsPerPage,
                                        int currentPageNum,
                                        ref int recordCount,
                                        ref int pageCount,
                                        int orderSelectionColumn,
                                        int descAsc,
                                        int institutionId,
                                        string kioskId,
                                        int orderType)
        {
            KioskCashDispenserCollection items = new KioskCashDispenserCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@institutionId", institutionId),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@orderType", orderType)
                                                                 };
                sqlParameters[2].Value = Convert.ToDateTime(startDate);
                sqlParameters[3].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_dispenser_logs_new", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskCashDispenser(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREKioskCashDispenserNewOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }


        public KioskCashDispenserLCDMHopperCollection SearchKioskCashDispenserLCDMHopperOrder(DateTime startDate,
                                        DateTime endDate,
                                        int numberOfItemsPerPage,
                                        int currentPageNum,
                                        ref int recordCount,
                                        ref int pageCount,
                                        int orderSelectionColumn,
                                        int descAsc,
                                        int institutionId,
                                        string kioskId,
                                        int orderType)
        {
            KioskCashDispenserLCDMHopperCollection items = new KioskCashDispenserLCDMHopperCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@institutionId", institutionId),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@orderType", orderType)
                                                                 };
                sqlParameters[2].Value = Convert.ToDateTime(startDate);
                sqlParameters[3].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_dispenser_LCDM_hopper_logs", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {//hoppereod.
                    items.Add(this.GetKioskCashDispenserLCDMHopper(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns, i == dataSet.Tables[0].Rows.Count - 1 ? "info" : ""));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREKioskCashDispenserLCDMHopperOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }


        public POSEODCollection SearchPOSEODOrder(string kioskId,
                            DateTime startDate,
                            DateTime endDate,
                            int numberOfItemsPerPage,
                            int currentPageNum,
                            ref int recordCount,
                            ref int pageCount,
                            int orderSelectionColumn,
                            int descAsc,
                            ref string totalAmount,
                            int orderType)
        {
            POSEODCollection items = new POSEODCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@kioskId", kioskId),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@totalAmount", SqlDbType.VarChar,200),
                                                                     new SqlParameter("@orderType", orderType)
                                                                 };
                sqlParameters[7].Value = Convert.ToDateTime(startDate);
                sqlParameters[8].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                sqlParameters[9].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[search_pos_eod_order_temp]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                totalAmount = sqlParameters[9].Value.ToString();

                dataSet.Tables[0].Columns.Add("ClassName", typeof(string));
                ColorByValues colorVlaues = new ColorByValues();
              
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    ClassNameParsers parser = new ClassNameParsers();
                    List<ClassName> classNameList = new List<ClassName>();
                    classNameList.Add(new ClassName("Status", colorVlaues.ColorByValue(dataSet.Tables[0].Rows[i]["StatusId"].ToString())));
                    parser.ClassName = classNameList;

                    dataSet.Tables[0].Rows[i]["ClassName"] = JsonConvert.SerializeObject(parser);

                    items.Add(this.GetPOSEOD(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchKioskFillCashOrderEmptyOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        private POSEOD GetPOSEOD(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new POSEOD(Convert.ToInt32(dataRow["ResultCount"])
            , Convert.ToInt32(dataRow["Id"])
            , dataRow["KioskName"].ToString()
            , dataRow["TransactionDate"].ToString()
            , dataRow["TransactionCount"].ToString()
            , dataRow["POSTransactionCount"].ToString()
            , dataRow["DistinctionTransaction"].ToString()
            , dataRow["Amount"].ToString()
            , dataRow["POSAmount"].ToString()
            , dataRow["DistinctionAmount"].ToString()
            , dataRow["Status"].ToString()
            , dataRow["Explanation"].ToString()
            , dataRow["Users"].ToString()
            , dataRow["StatusId"].ToString()
            , dataRow["ClassName"].ToString()
        );
        }


        //private KioskFillCashBox GetPOSEOD(DataRow dataRow, DataColumnCollection dataColumns)
        //{
        //    return new KioskFillCashBox(Convert.ToInt32(dataRow["Id"])
        //    , Convert.ToInt32(dataRow["KioskId"])
        //    , dataRow["KioskName"].ToString()
        //    , dataRow["ExpertUserId"].ToString()
        //    , dataRow["ExpertUserName"].ToString()
        //    , dataRow["UpdatedDate"].ToString()
        //    , dataRow["Amount"].ToString());
        //}

        //GetKioskCashDispenserOrder
        public KioskCashDispenser GetKioskCashDispenserOrder(int kioskId)
        {
            KioskCashDispenser kiosk = null;
            DataSet dataSet = null;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@kioskId", kioskId) };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_cashdispenser", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                if (dataSet.Tables[0].Rows.Count == 1)
                {
                    kiosk = this.GetKioskCashDispenser(dataSet.Tables[0].Rows[0], dataSet.Tables[0].Columns);
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKioskCashDispenserO");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return kiosk;
        }

        public KioskControlCashNotificationCollection ControlCashNotificationOrder(string searchText,
                                                                                     DateTime startDate,
                                                                                     DateTime endDate,
                                                                                     int numberOfItemsPerPage,
                                                                                     int currentPageNum,
                                                                                     ref int recordCount,
                                                                                     ref int pageCount,
                                                                                     int orderSelectionColumn,
                                                                                     int descAsc,
                                                                                     int institutionId,
                                                                                     string kioskId,
                                                                                     ref string totalfirstDenomNumber,
                                                                                     ref string totalsecondDenomNumber,
                                                                                     ref string totalthirdDenomNumber,
                                                                                     ref string totalfourthDenomNumber,
                                                                                     ref string totalfifthDenomNumber,
                                                                                     ref string totalsixthDenomNumber,
                                                                                     ref string totalseventhDenomNumber,
                                                                                     ref string totaleighthDenomNumber,
                                                                                     ref string totalninthDenomNumber,
                                                                                     ref string totaltenthDenomNumber,
                                                                                     ref string totaleleventhDenomNumber,
                                                                                     ref string totalAmount,
                                                                                     int orderType,
                                                                                     int statusId,
                                                                                     int kioskFullFillId)
        {
            KioskControlCashNotificationCollection items = new KioskControlCashNotificationCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] {
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@searchText", searchText),  
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@institutionId", institutionId),
                                                                    new SqlParameter("@searchKioskId", kioskId),
                                                                    new SqlParameter("@orderType", orderType),
                                                                    new SqlParameter("@totalfirstDenomNumber",  SqlDbType.VarChar,20),
                                                                    new SqlParameter("@totalsecondDenomNumber", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@totalthirdDenomNumber",  SqlDbType.VarChar,20), 
                                                                    new SqlParameter("@totalfourthDenomNumber", SqlDbType.VarChar,20), 
                                                                    new SqlParameter("@totalfifthDenomNumber",  SqlDbType.VarChar,20), 
                                                                    new SqlParameter("@totalsixthDenomNumber",  SqlDbType.VarChar,20),  
                                                                    new SqlParameter("@totalseventhDenomNumber",SqlDbType.VarChar,20),
                                                                    new SqlParameter("@totaleighthDenomNumber", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@totalninthDenomNumber",  SqlDbType.VarChar,20),
                                                                    new SqlParameter("@totaltenthDenomNumber", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@totaleleventhDenomNumber", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@totalAmount", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@statusId", statusId),
                                                                    new SqlParameter("@kioskFullFillId", kioskFullFillId)
                                                                    
                                                                    
                                                                 };


                sqlParameters[0].Value = Convert.ToDateTime(startDate);
                sqlParameters[1].Value = Convert.ToDateTime(endDate);
                sqlParameters[3].Direction = sqlParameters[4].Direction = ParameterDirection.InputOutput;
                sqlParameters[12].Direction = sqlParameters[13].Direction = ParameterDirection.InputOutput;
                sqlParameters[14].Direction = sqlParameters[15].Direction = ParameterDirection.InputOutput;
                sqlParameters[16].Direction = sqlParameters[17].Direction = ParameterDirection.InputOutput;
                sqlParameters[18].Direction = sqlParameters[19].Direction = ParameterDirection.InputOutput;
                sqlParameters[20].Direction = sqlParameters[21].Direction = ParameterDirection.InputOutput;
                sqlParameters[22].Direction = sqlParameters[23].Direction = ParameterDirection.InputOutput;

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_kiosk_control_cash_notification_list", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[4].Value);
                pageCount = Convert.ToInt32(sqlParameters[3].Value);


                totalfirstDenomNumber = sqlParameters[12].Value.ToString();
                totalsecondDenomNumber = sqlParameters[13].Value.ToString();
                totalthirdDenomNumber = sqlParameters[14].Value.ToString();
                totalfourthDenomNumber = sqlParameters[15].Value.ToString();
                totalfifthDenomNumber = sqlParameters[16].Value.ToString();
                totalsixthDenomNumber = sqlParameters[17].Value.ToString();
                totalseventhDenomNumber = sqlParameters[18].Value.ToString();
                totaleighthDenomNumber = sqlParameters[19].Value.ToString();
                totalninthDenomNumber = sqlParameters[20].Value.ToString();
                totaltenthDenomNumber = sqlParameters[21].Value.ToString();
                totaleleventhDenomNumber = sqlParameters[22].Value.ToString();
                totalAmount = sqlParameters[23].Value.ToString();


                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskControlCashNotificationList(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
                items.Add(new KioskControlCashNotification("", "", "", totalfirstDenomNumber, 
                    totalsecondDenomNumber, totalthirdDenomNumber, totalfourthDenomNumber
                    , totalfifthDenomNumber, totalsixthDenomNumber, totalseventhDenomNumber, totaleighthDenomNumber, totalninthDenomNumber, totaltenthDenomNumber,
                    totaleleventhDenomNumber, "", "", "", totalAmount, "", "", "", 0, "info"));
                //if (items.Count > 0)
                //    items[items.Count - 1].ClassName = "info";

            }


            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREKioskControlCashNotificationOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        //ControlCashNotification

        public KioskControlCashNotification GetControlCashNotificationOrder(int controlCashId)
        {
            KioskControlCashNotification items = null;
            DataSet dataSet = null;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@controlCashId", controlCashId) };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_control_cash_notification_list", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                if (dataSet.Tables[0].Rows.Count == 1)
                {

                    items = this.GetKioskControlCashNotificationList(dataSet.Tables[0].Rows[0], dataSet.Tables[0].Columns);
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKioskControlCashNotificationListOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return items;
        }



        public KioskCutofMonitoringCollection CutOffMonitoringOrder(string searchText,
                                                                                    DateTime startDate,
                                                                                    DateTime endDate,
                                                                                    int numberOfItemsPerPage,
                                                                                    int currentPageNum,
                                                                                    ref int recordCount,
                                                                                    ref int pageCount,
                                                                                    int orderSelectionColumn,
                                                                                    int descAsc,
                                                                                    int institutionId,
                                                                                    string kioskId,
                                                                                    int orderType)
        {
            KioskCutofMonitoringCollection items = new KioskCutofMonitoringCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] {
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@searchText", searchText),  
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@institutionId", institutionId),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@orderType", orderType)
                                                                    
                                                                 };

                sqlParameters[0].Value = Convert.ToDateTime(startDate);
                sqlParameters[1].Value = Convert.ToDateTime(endDate);
                sqlParameters[3].Direction = sqlParameters[4].Direction = ParameterDirection.InputOutput;

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_kiosk_cut_off_monitoring", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[4].Value);
                pageCount = Convert.ToInt32(sqlParameters[3].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskCutOffMonitoringList(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }


            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREKioskCutofMonitoringOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        //GetCutOffMonitoringOrder
        public KioskCutofMonitoring GetCutOffMonitoringOrder(int cutId)
        {
            KioskCutofMonitoring items = null;
            DataSet dataSet = null;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@cutId", cutId) };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_cut_off_monitoring", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                if (dataSet.Tables[0].Rows.Count == 1)
                {

                    items = this.GetKioskCutOffMonitoringList(dataSet.Tables[0].Rows[0], dataSet.Tables[0].Columns);
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREKioskCutofMonitoringOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return items;
        }

        public int SaveCutOffMonitoring(int expertUserId,
                                        DateTime startDate,
                                        DateTime endDate,
                                        string cutDurationText,
                                        int instutionId,
                                        string cutReasonText,
                                        string kioskId,
                                        DateTime insertDate)
        {
            int resp = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@userId", expertUserId),                                                                  
                                                                    new SqlParameter("@returnCode", 0),
                                                                    new SqlParameter("@startDate", startDate),
                                                                    new SqlParameter("@endDate", endDate),
                                                                    new SqlParameter("@cutOffTime", (cutDurationText).ToString()),
                                                                    new SqlParameter("@institutionId", instutionId),
                                                                    new SqlParameter("@explanation", cutReasonText),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@insertionDate", insertDate)
                                                                
                                                                    };

                sqlParameters[1].Direction = ParameterDirection.InputOutput;
                resp = Convert.ToInt32(sqlParameters[1].Value);

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[insert_kiosk_cut_off_monitoring]", sqlParameters);
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveControlCashNotification");
                resp = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
            }
            finally
            {
                base.Dispose();
            }

            return resp;
        }

        //UpdateCutOffMonitoring
        public int UpdateCutOffMonitoring(int cutId,
                                int expertUserId,
                                DateTime startDate,
                                DateTime endDate,
                                string cutDurationText,
                                int instutionId,
                                string cutReasonText,
                                string kioskId,
                                DateTime insertDate)
        {
            int resp = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@cutId", cutId),       
                                                                    new SqlParameter("@userId", expertUserId),                                                                  
                                                                    new SqlParameter("@returnCode", 0),
                                                                    new SqlParameter("@startDate", startDate),
                                                                    new SqlParameter("@endDate", endDate),
                                                                    new SqlParameter("@cutOffTime", (cutDurationText).ToString()),
                                                                    new SqlParameter("@institutionId", instutionId),
                                                                    new SqlParameter("@explanation", cutReasonText),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@insertionDate", insertDate)
                                                                
                                                                    };

                sqlParameters[2].Direction = ParameterDirection.InputOutput;
                resp = Convert.ToInt32(sqlParameters[2].Value);

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[update_kiosk_cut_off_monitoring]", sqlParameters);
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREUpdateControlCashNotification");
                resp = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
            }
            finally
            {
                base.Dispose();
            }

            return resp;
        }


        private KioskCutofMonitoring GetKioskCutOffMonitoringList(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskCutofMonitoring(dataRow["StartDate"].ToString()
            , dataRow["EndDate"].ToString()
            , dataRow["CutOffTime"].ToString()
            , dataRow["KioskName"].ToString()
            , dataRow["InstitutionId"].ToString()
            , dataRow["Explanation"].ToString()
            , dataRow["UserId"].ToString()
            , dataRow["InsertionDate"].ToString()
            , dataRow["CutId"].ToString()
            , dataRow["KioskId"].ToString()
            , dataRow["Inst"].ToString()
            )
            ;
        }


        public KioskBankAccountActionCollection BankAccountActionOrder(string searchText,
                                                                      DateTime startDate,
                                                                      DateTime endDate,
                                                                      int numberOfItemsPerPage,
                                                                      int currentPageNum,
                                                                      ref int recordCount,
                                                                      ref int pageCount,
                                                                      int orderSelectionColumn,
                                                                      int descAsc,
                                                                      int orderType)
        {
            KioskBankAccountActionCollection items = new KioskBankAccountActionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount),
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@searchText", searchText),  
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@orderType", orderType)
                                                                    
                                                                 };

                sqlParameters[2].Value = Convert.ToDateTime(startDate);
                sqlParameters[3].Value = Convert.ToDateTime(endDate);

                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_bank_account_actions", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);


                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskBankAccountActionList(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }


            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREKioskBankAccountActionOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }



        public EntereKioskEmptyCashCountCollection EnteredKioskEmptyCashCountOrder(string searchText,
                                                                                      DateTime startDate,
                                                                                      DateTime endDate,
                                                                                      int numberOfItemsPerPage,
                                                                                      int currentPageNum,
                                                                                      ref int recordCount,
                                                                                      ref int pageCount,
                                                                                      int orderSelectionColumn,
                                                                                      int descAsc,
            // int institutionId,
                                                                                      string kioskId,
                                                                                      string enteredUser,
                                                                                      string operatorUser,
                                                                                      string status)
        {
            EntereKioskEmptyCashCountCollection items = new EntereKioskEmptyCashCountCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] {
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@searchText", searchText),  
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    //new SqlParameter("@institutionId", institutionId),
                                                                    new SqlParameter("@searchKioskId", kioskId),
                                                                    new SqlParameter("@searchEnteredUser", enteredUser),
                                                                    new SqlParameter("@searchOperatorUser", operatorUser),
                                                                    new SqlParameter("@searchStatus", status)
                                                                    
                                                                 };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_control_cash_notification_list", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                //recordCount = Convert.ToInt32(sqlParameters[0].Value);
                //pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetEntereKioskEmptyCashCountList(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }


            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREEntereKioskEmptyCashCountOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        public CodeReportCollections SearchKioskCashAcceptorOrderForExcel(DateTime startDate,
                                      DateTime endDate,
                                      int numberOfItemsPerPage,
                                      int currentPageNum,
                                      ref int recordCount,
                                      ref int pageCount,
                                      int orderSelectionColumn,
                                      int descAsc,
                                      int institutionId,
                                       string kioskId)
        {
            CodeReportCollections items = new CodeReportCollections();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@institutionId", institutionId),
                                                                    new SqlParameter("@kioskId", kioskId)
                                                                 };
                sqlParameters[2].Value = Convert.ToDateTime(startDate);
                sqlParameters[3].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_acceptor_logs_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetCodeReports(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREKioskCashAcceptorOrderForExcel");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        public KioskCashDispenserCollection SearchKioskCashDispenserOrderForExcel(DateTime startDate,
                                     DateTime endDate,
                                     int numberOfItemsPerPage,
                                     int currentPageNum,
                                     ref int recordCount,
                                     ref int pageCount,
                                     int orderSelectionColumn,
                                     int descAsc,
                                     int institutionId,
                                     string kioskId)
        {
            KioskCashDispenserCollection items = new KioskCashDispenserCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@institutionId", institutionId),
                                                                    new SqlParameter("@kioskId", kioskId)
                                                                 };
                sqlParameters[2].Value = Convert.ToDateTime(startDate);
                sqlParameters[3].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk_dispenser_logs", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetKioskCashDispenser(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREKioskCashDispenserOrderForExcel");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }


        public MutabakatCollection SearchMutabakatOrder(string searchText,
                                             DateTime startDate,
                                             DateTime endDate,
                                             int numberOfItemsPerPage,
                                             int currentPageNum,
                                             ref int recordCount,
                                             ref int pageCount,
                                             int orderSelectionColumn,
                                             int descAsc,
                                             string kioskId,
                                             int institutionId,
                                             int statusId,
                                             int kioskEmptyStatus,
                                             int orderType)
        {
            MutabakatCollection items = new MutabakatCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount),
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@institutionId", institutionId),
                                                                    new SqlParameter("@statusId", statusId),
                                                                    new SqlParameter("@kioskEmptyStatusId", kioskEmptyStatus),
                                                                    new SqlParameter("@orderType", orderType),
                                                                    new SqlParameter("@kioskId", kioskId)
                                                                 };
                sqlParameters[3].Value = Convert.ToDateTime(startDate);
                sqlParameters[4].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_mutabakat_order", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetMutabakat2(i == dataSet.Tables[0].Rows.Count-1 ? "info":"",dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchMutabakatOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        public MutabakatReportCollection SearchMutabakatOrderForReport(string searchText,
                                     DateTime startDate,
                                     DateTime endDate,
                                     int numberOfItemsPerPage,
                                     int currentPageNum,
                                     ref int recordCount,
                                     ref int pageCount,
                                     int orderSelectionColumn,
                                     int descAsc,
                                     int orderType)
        {
            MutabakatReportCollection items = new MutabakatReportCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@orderType", orderType)
                                                                 };
                sqlParameters[3].Value = Convert.ToDateTime(startDate);
                sqlParameters[4].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_mutabakat_order_as_report", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetMutabakatReport(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

                if (items.Count>0)
                    items[items.Count - 1].ClassName = "info";
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchMutabakatOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        public JObject GetMutabakatValues(DateTime mutabakatDate, Int64 kioskId, int instutionId, Int64 adminUserId, int bankTypeId)
        {
            JObject data = new JObject();
            DataSet dataSet = null;
            Int64 faturaCount = 0;
            Int64 kartDolumCount = 0;
            Int64 totalDolumCount = 0;
            string faturaAmount = "0";
            string kartDolumAmount = "0";
            string totalDolumAmount = "0";
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@mutabakatDate",  SqlDbType.DateTime), 
                                                                    new SqlParameter("@faturaCount", faturaCount),
                                                                    new SqlParameter("@faturaAmount", SqlDbType.VarChar,20),  
                                                                    new SqlParameter("@kartDolumCount", kartDolumCount),  
                                                                    new SqlParameter("@kartDolumAmount", SqlDbType.VarChar,20), 
                                                                    new SqlParameter("@totalDolumAmount", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@totalDolumCount", totalDolumCount),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@instutionId", instutionId),
                                                                    new SqlParameter("@bankTypeId", bankTypeId)
                                                                 };
                sqlParameters[0].Value = Convert.ToDateTime(mutabakatDate);
                sqlParameters[1].Direction = sqlParameters[2].Direction = sqlParameters[3].Direction = sqlParameters[4].Direction = sqlParameters[5].Direction = sqlParameters[6].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.[get_mutabakat_values]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                faturaCount = Convert.ToInt64(sqlParameters[1].Value);
                faturaAmount = sqlParameters[2].Value.ToString();

                kartDolumCount = Convert.ToInt64(sqlParameters[3].Value);
                kartDolumAmount = sqlParameters[4].Value.ToString();

                totalDolumCount = Convert.ToInt64(sqlParameters[6].Value);
                totalDolumAmount = sqlParameters[5].Value.ToString();



                data.Add("faturaCount", faturaCount);
                data.Add("faturaAmount", faturaAmount);

                data.Add("kartDolumCount", kartDolumCount);
                data.Add("kartDolumAmount", kartDolumAmount);

                data.Add("totalDolumCount", totalDolumCount);
                data.Add("totalDolumAmount", totalDolumAmount);

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchMutabakatOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return data;
        }

        public GetKioskUsersAndPasswordCollection GetKioskUserPassWithId(Int64 kioskId, DateTime startDate, DateTime endDate)
        {
            bool resp = false;


            GetKioskUsersAndPasswordCollection items = new GetKioskUsersAndPasswordCollection();

            try
            {
                DataSet dataSet = null;

                SqlParameter[] sqlParameters = new SqlParameter[] {new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@startDate", startDate),
                                                                     new SqlParameter("@endDate", endDate)
                                                                 };
                sqlParameters[1].Direction = sqlParameters[2].Direction = ParameterDirection.Input;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.[get_kiosk_user_pass_with_id]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    items.Add(this.GetKioskInfo(dataRow, dataSet.Tables[0].Columns));
                }



            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchMutabakatOrder");
            }
            finally
            {
                base.Dispose();
            }
            return items;
        }

        public JObject SaveMutabakatResult(Int64 userId
                                        , string mutabakatDate
                                        , Int64 kioskId
                                        , int instutionId
                                        , int faturaCount
                                        , string faturaAmount
                                        , int kartDolumCount
                                        , string kartDolumAmount
                                        , int totalCount
                                        , string totalAmount
                                        , int instutionCount
                                        , string instutionAmount
                                        , string whichSide
                                        , string mutabakatResult
                                        , out int returnCode)
        {
            JObject data = new JObject();
            DataSet dataSet = null;
            Int64 kioskInstutionMutabakatId = 0;
            Int64 kioskInstutionMutabakatLogId = 0;
            returnCode = -1;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@userId",  userId), 
                                                                    new SqlParameter("@mutabakatDate",  mutabakatDate), 
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@InstutionId",instutionId),  
                                                                    new SqlParameter("@faturaCount", faturaCount),  
                                                                    new SqlParameter("@faturaAmount",faturaAmount), 
                                                                    new SqlParameter("@kartDolumCount", kartDolumCount),
                                                                    new SqlParameter("@kartDolumAmount", kartDolumAmount),
                                                                    new SqlParameter("@totalCount", totalCount),
                                                                    new SqlParameter("@totalAmount", totalAmount),
                                                                    new SqlParameter("@instutionCount", instutionCount),
                                                                    new SqlParameter("@instutionAmount", instutionAmount),
                                                                    new SqlParameter("@returnCode", returnCode),
                                                                    new SqlParameter("@kioskInstutionMutabakatId", kioskInstutionMutabakatId),
                                                                    new SqlParameter("@kioskInstutionMutabakatLogId", kioskInstutionMutabakatLogId),
                                                                    new SqlParameter("@mutabakatResult", mutabakatResult),
                                                                    new SqlParameter("@whichSide", whichSide)
                                                                 };
                sqlParameters[12].Direction = sqlParameters[13].Direction = sqlParameters[14].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "dbo.[SaveKioskInstutionMutabakat]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                data.Add("kioskInstutionMutabakatId", kioskInstutionMutabakatId);
                data.Add("kioskInstutionMutabakatLogId", kioskInstutionMutabakatLogId);
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchMutabakatOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return data;
        }

        public DailyAccountReportCollection SearchDailyAccounReportsOrderForExcel(DateTime startDate,
                                             DateTime endDate,
                                             int orderSelectionColumn,
                                             int descAsc)
        {
            DailyAccountReportCollection items = new DailyAccountReportCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc)
                                                                 };
                sqlParameters[0].Value = Convert.ToDateTime(startDate);
                sqlParameters[1].Value = Convert.ToDateTime(endDate);
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_daily_account_report_for_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);


                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetDailyAccountReport(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchDailyAccounReportsOrderForExcel");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        public DailyAccountReportProkisCollection SearchDailyAccounReportsProkisOrderForExcel(DateTime startDate,
                                            DateTime endDate,
                                            int orderSelectionColumn,
                                            int descAsc)
        {
            DailyAccountReportProkisCollection items = new DailyAccountReportProkisCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc)
                                                                 };
                sqlParameters[0].Value = Convert.ToDateTime(startDate);
                sqlParameters[1].Value = Convert.ToDateTime(endDate);
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_daily_account_report_for_excel_prokis", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);


                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetDailyAccountReportProkis(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchDailyAccounReportsOrderForExcel");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        public MutabakatCollection SearchMutabakatOrderForExcel(string searchText,
                                             DateTime startDate,
                                             DateTime endDate,
                                             int orderSelectionColumn,
                                             int descAsc)
        {
            MutabakatCollection items = new MutabakatCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc)
                                                                 };
                sqlParameters[1].Value = Convert.ToDateTime(startDate);
                sqlParameters[2].Value = Convert.ToDateTime(endDate);
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_mutabakat_order_for_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);


                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    items.Add(this.GetMutabakat(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchMutabakatOrderForExcel");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return items;
        }

        private PRCNCORE.Accounts.Mutabakat GetMutabakat(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new PRCNCORE.Accounts.Mutabakat(Convert.ToInt32(dataRow["Id"])
            , dataRow["InsertionDate"].ToString()
            , dataRow["Status"].ToString()
            , dataRow["StatusId"].ToString()
            , dataRow["MutabakatDate"].ToString()
            , dataRow["MutabakatUser"].ToString()
            , dataRow["Explanation"].ToString()
            , dataRow["InputAmount"].ToString()
            , dataRow["OutputAmount"].ToString()
            , dataRow["AmountDifference"].ToString()
            , dataRow["Institution"].ToString()
                //, Convert.ToDecimal(dataRow["CashInFlow"].ToString())
                //, dataRow["CodeInFlow"].ToString()
                //, Convert.ToDecimal(dataRow["InstitutionAmount"].ToString())
                //, Convert.ToDecimal(dataRow["ActiveCode"].ToString())
                //, Convert.ToDecimal(dataRow["UsedCode"].ToString()) 
            , dataRow["AccountOwner"].ToString()
            , dataRow["KioskName"].ToString()
            , dataRow["ResultCount"].ToString()
            , dataRow["KioskEmptyId"].ToString()
            , dataRow["AccountDate"].ToString()
            , dataRow["OperatorName"].ToString()
            , dataRow["ReferenceNo"].ToString()
            ,     ""
            );
        }

        private PRCNCORE.Accounts.Mutabakat GetMutabakat(string className, DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new PRCNCORE.Accounts.Mutabakat(Convert.ToInt32(dataRow["Id"])
            , dataRow["InsertionDate"].ToString()
            , dataRow["Status"].ToString()
            , dataRow["StatusId"].ToString()
            , dataRow["MutabakatDate"].ToString()
            , dataRow["MutabakatUser"].ToString()
            , dataRow["Explanation"].ToString()
            , dataRow["InputAmount"].ToString()
            , dataRow["OutputAmount"].ToString()
            , dataRow["AmountDifference"].ToString()
            , dataRow["Institution"].ToString()
            //, Convert.ToDecimal(dataRow["CashInFlow"].ToString())
            //, dataRow["CodeInFlow"].ToString()
            //, Convert.ToDecimal(dataRow["InstitutionAmount"].ToString())
            //, Convert.ToDecimal(dataRow["ActiveCode"].ToString())
            //, Convert.ToDecimal(dataRow["UsedCode"].ToString()) 
            , dataRow["AccountOwner"].ToString()
            , dataRow["KioskName"].ToString()
            , dataRow["ResultCount"].ToString()
            , dataRow["KioskEmptyId"].ToString()
            , dataRow["AccountDate"].ToString()
            , dataRow["OperatorName"].ToString()
            , dataRow["ReferenceNo"].ToString()

            , className
            );
        }

        private PRCNCORE.Accounts.Mutabakat GetMutabakat2(string className, DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new PRCNCORE.Accounts.Mutabakat(Convert.ToInt32(dataRow["Id"])
            , dataRow["InsertionDate"].ToString()
            , dataRow["Status"].ToString()
            , dataRow["StatusId"].ToString()
            , dataRow["MutabakatDate"].ToString()
            , dataRow["MutabakatUser"].ToString()
            , dataRow["Explanation"].ToString()
            , dataRow["InputAmount"].ToString()
            , dataRow["OutputAmount"].ToString()
            , dataRow["AmountDifference"].ToString()
            , dataRow["Institution"].ToString()
            //, Convert.ToDecimal(dataRow["CashInFlow"].ToString())
            //, dataRow["CodeInFlow"].ToString()
            //, Convert.ToDecimal(dataRow["InstitutionAmount"].ToString())
            //, Convert.ToDecimal(dataRow["ActiveCode"].ToString())
            //, Convert.ToDecimal(dataRow["UsedCode"].ToString()) 
            , dataRow["AccountOwner"].ToString()
            , dataRow["KioskName"].ToString()
            , dataRow["ResultCount"].ToString()
            , dataRow["KioskEmptyId"].ToString()
            , dataRow["AccountDate"].ToString()
            , dataRow["OperatorName"].ToString()
            , dataRow["ReferenceNo"].ToString()
            , className

            , dataRow["KioskEmptyStatusId"].ToString()
            , dataRow["KioskEmptyStatusName"].ToString()

            );
        }

        private MutabakatReport GetMutabakatReport(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new MutabakatReport(Convert.ToInt32(dataRow["ResultCount"])
            , dataRow["InsertionDate"].ToString()
            , dataRow["KioskName"].ToString()
            , dataRow["UpdatedDate"].ToString()
            , dataRow["LastCashOutDate"].ToString()
            , dataRow["OutputAmount"].ToString()
            , dataRow["InputAmount"].ToString()
            , dataRow["AmountDifference"].ToString()
            , dataRow["Institution"].ToString()
            , dataRow["CashAmount"].ToString()
            , dataRow["CodeAmount"].ToString()
            , dataRow["InstitutionAmount"].ToString()
            , dataRow["UsageAmount"].ToString()
            , dataRow["CalculatedChangeAmount"].ToString()
            , dataRow["GivenChangeAmount"].ToString()
            , dataRow["ActiveCode"].ToString()
            , dataRow["UsedCode"].ToString()
            , dataRow["KioskGeneratedActiveCode"].ToString()
            , dataRow["KioskGeneratedKioskUsedCode"].ToString()
            , dataRow["ManuelGeneratedActiveCode"].ToString()
            , dataRow["ManuelGeneratedUsedCode"].ToString()
            , dataRow["AnotherKioskGeneratedUsedCode"].ToString()
            , "");
        }

        private DailyAccountReport GetDailyAccountReport(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new DailyAccountReport(Convert.ToInt32(dataRow["Id"])
            , dataRow["InsertionDate"].ToString()
            , dataRow["TotalBalance"].ToString()
            , dataRow["ActiveBalance"].ToString()
            , dataRow["PasifBalance"].ToString());
        }

        private AutoMutabakatReport GetAutoMutabakatReport(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new AutoMutabakatReport(dataRow["InstitutionNumber"].ToString()
            , dataRow["InstitutionAmount"].ToString()
            , dataRow["InstitiutionCancelNumber"].ToString()
            , dataRow["InstitutionCancelAmount"].ToString()
            , dataRow["BOSNumber"].ToString()
            , dataRow["BOSAmount"].ToString()
            , dataRow["BOSCancelNumber"].ToString()
            , dataRow["BOSCancelAmount"].ToString()
            , dataRow["Account"].ToString()
            , dataRow["InsertionDate"].ToString()
            , dataRow["IsSuccess"].ToString()
            , dataRow["Message"].ToString()
            , dataRow["UserId"].ToString()
            , dataRow["ManuelApproveDate"].ToString()
            , dataRow["InsMif"].ToString()
            , dataRow["InsMakbuz"].ToString()
            , dataRow["InsSaat"].ToString()
            , dataRow["Explanation"].ToString());
        }


        private DailyAccountReportProkis GetDailyAccountReportProkis(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new DailyAccountReportProkis(Convert.ToInt32(dataRow["Id"])
            , dataRow["InsertionDate"].ToString()
            , dataRow["TotalBalance"].ToString()
            , dataRow["ActiveAskiCode"].ToString()
            , dataRow["PasifAskiCode"].ToString()
            , dataRow["PasifCodeBalance"].ToString()
            , dataRow["PasifCodeCount"].ToString()
            , dataRow["UsableCodeBeforeExpiring"].ToString()
            );
        }



        private KioskControlCashNotification GetKioskControlCashNotificationList(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskControlCashNotification(dataRow["KioskName"].ToString()
            , dataRow["InstitutionName"].ToString()
            , dataRow["InsertionDate"].ToString()
            , dataRow["FirstDenomNumber"].ToString()
            , dataRow["SecondDenomNumber"].ToString()
            , dataRow["ThirdDenomNumber"].ToString()
            , dataRow["FourthDenomNumber"].ToString()
            , dataRow["FifthDenomNumber"].ToString()
            , dataRow["SixthDenomNumber"].ToString()
            , dataRow["SeventhDenomNumber"].ToString()
            , dataRow["EighthDenomNumber"].ToString()
            , dataRow["NinthDenomNumber"].ToString()
            , dataRow["TenthDenomNumber"].ToString()
            , dataRow["EleventhDenomNumber"].ToString()
            , dataRow["CreatedUser"].ToString()
            , dataRow["UserCashOut"].ToString()
            , dataRow["CashOutDate"].ToString()
            , dataRow["TotalAmount"].ToString()
            , dataRow["Status"].ToString()
            , (dataRow["ControlCashId"]).ToString()
            , (dataRow["KioskId"]).ToString()
            , Convert.ToInt32(dataRow["StatusId"])
            , ""
            )
            ;
        }


        private KioskBankAccountAction GetKioskBankAccountActionList(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskBankAccountAction(dataRow["InsertionDate"].ToString()
            , dataRow["AskiBalance"].ToString()
            , dataRow["AskiFeeBalance"].ToString()
            , dataRow["BgazBalance"].ToString()
            , dataRow["BgazFeeBalance"].ToString()
            , dataRow["DesmerBalance"].ToString()

            )
            ;
        }

        private EntereKioskEmptyCashCount GetEntereKioskEmptyCashCountList(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new EntereKioskEmptyCashCount(dataRow["KioskName"].ToString()
                //, dataRow["InstitutionName"].ToString()
            , dataRow["InsertionDate"].ToString()
            , dataRow["FirstDenomNumber"].ToString()
            , dataRow["SecondDenomNumber"].ToString()
            , dataRow["ThirdDenomNumber"].ToString()
            , dataRow["FourthDenomNumber"].ToString()
            , dataRow["FifthDenomNumber"].ToString()
            , dataRow["SixthDenomNumber"].ToString()
            , dataRow["SeventhDenomNumber"].ToString()
            , dataRow["EighthDenomNumber"].ToString()
            , dataRow["NinthDenomNumber"].ToString()
            , dataRow["TenthDenomNumber"].ToString()
            , dataRow["EleventhDenomNumber"].ToString()
            , dataRow["CreatedUser"].ToString()
            , dataRow["UserCashOut"].ToString()
            , dataRow["CashOutDate"].ToString()
            , dataRow["TotalAmount"].ToString()
            )
            ;
        }


        private KioskCashAcceptor GetKioskCashAcceptor(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskCashAcceptor(dataRow["KioskName"].ToString()
            , dataRow["InstitutionName"].ToString()
            , dataRow["InsertionDate"].ToString()
            , dataRow["FirstDenomValue"].ToString()
            , dataRow["FirstDenomNumber"].ToString()
            , dataRow["SecondDenomValue"].ToString()
            , dataRow["SecondDenomNumber"].ToString()
            , dataRow["ThirdDenomValue"].ToString()
            , dataRow["ThirdDenomNumber"].ToString()
            , dataRow["FourthDenomValue"].ToString()
            , dataRow["FourthDenomNumber"].ToString()
            , dataRow["FifthDenomValue"].ToString()
            , dataRow["FifthDenomNumber"].ToString()
            , dataRow["SixthDenomValue"].ToString()
            , dataRow["SixthDenomNumber"].ToString()
            , dataRow["SeventhDenomValue"].ToString()
            , dataRow["SeventhDenomNumber"].ToString()
            , dataRow["EighthDenomValue"].ToString()
            , dataRow["EighthDenomNumber"].ToString()
            , dataRow["NinthDenomValue"].ToString()
            , dataRow["NinthDenomNumber"].ToString()
            , dataRow["TenthDenomValue"].ToString()
            , dataRow["TenthDenomNumber"].ToString()
            , dataRow["EleventhDenomValue"].ToString()
            , dataRow["EleventhDenomNumber"].ToString()
            , "",0
                //, dataRow["girisyapankull"].ToString()
                //, dataRow["giristarihi"].ToString()
                //, dataRow["toplam"].ToString()
            )
            ;
        }
        private KioskCashAcceptor GetKioskCashAcceptor(DataRow dataRow, DataColumnCollection dataColumns,string className)
        {
            return new KioskCashAcceptor(dataRow["KioskName"].ToString()
            , dataRow["InstitutionName"].ToString()
            , dataRow["InsertionDate"].ToString()
            , dataRow["FirstDenomValue"].ToString()
            , dataRow["FirstDenomNumber"].ToString()
            , dataRow["SecondDenomValue"].ToString()
            , dataRow["SecondDenomNumber"].ToString()
            , dataRow["ThirdDenomValue"].ToString()
            , dataRow["ThirdDenomNumber"].ToString()
            , dataRow["FourthDenomValue"].ToString()
            , dataRow["FourthDenomNumber"].ToString()
            , dataRow["FifthDenomValue"].ToString()
            , dataRow["FifthDenomNumber"].ToString()
            , dataRow["SixthDenomValue"].ToString()
            , dataRow["SixthDenomNumber"].ToString()
            , dataRow["SeventhDenomValue"].ToString()
            , dataRow["SeventhDenomNumber"].ToString()
            , dataRow["EighthDenomValue"].ToString()
            , dataRow["EighthDenomNumber"].ToString()
            , dataRow["NinthDenomValue"].ToString()
            , dataRow["NinthDenomNumber"].ToString()
            , dataRow["TenthDenomValue"].ToString()
            , dataRow["TenthDenomNumber"].ToString()
            , dataRow["EleventhDenomValue"].ToString()
            , dataRow["EleventhDenomNumber"].ToString()
            , className,0
            //, dataRow["girisyapankull"].ToString()
            //, dataRow["giristarihi"].ToString()
            //, dataRow["toplam"].ToString()
            )
            ;
        }


        private KioskCashDispenser GetKioskCashDispenser(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskCashDispenser(dataRow["KioskName"].ToString()
            , dataRow["InstitutionName"].ToString()
            , dataRow["InsertionDate"].ToString()
            , dataRow["KioskId"].ToString()
            , dataRow["FirstDenomValue"].ToString()
            , dataRow["FirstDenomNumber"].ToString()
            , dataRow["SecondDenomValue"].ToString()
            , dataRow["SecondDenomNumber"].ToString()
            , dataRow["ThirdDenomValue"].ToString()
            , dataRow["ThirdDenomNumber"].ToString()
            , dataRow["FourthDenomValue"].ToString()
            , dataRow["FourthDenomNumber"].ToString()
            , dataRow["FifthDenomValue"].ToString()
            , dataRow["FifthDenomNumber"].ToString()
            , dataRow["SixthDenomValue"].ToString()
            , dataRow["SixthDenomNumber"].ToString()
            , dataRow["SeventhDenomValue"].ToString()
            , dataRow["SeventhDenomNumber"].ToString()
            , dataRow["EighthDenomValue"].ToString()
            , dataRow["EighthDenomNumber"].ToString()
            , dataRow["Casette1Reject"].ToString()
            , dataRow["Casette2Reject"].ToString()
            , dataRow["Casette3Reject"].ToString()
            , dataRow["Casette4Reject"].ToString()
            , dataRow["Id"].ToString()
            //, dataRow["EleventhDenomNumber"].ToString()
            //, dataRow["Id"].ToString()
            );
        }


        private KioskCashDispenserLCDMHopper GetKioskCashDispenserLCDMHopper(DataRow dataRow, DataColumnCollection dataColumns,string className="")
        {
            return new KioskCashDispenserLCDMHopper(dataRow["KioskName"].ToString()
            , dataRow["InstitutionName"].ToString()
            , dataRow["InsertionDate"].ToString()
            , dataRow["KioskId"].ToString()
            , dataRow["FirstDenomValue"].ToString()
            , dataRow["FirstDenomNumber"].ToString()
            , dataRow["SecondDenomValue"].ToString()
            , dataRow["SecondDenomNumber"].ToString()
            , dataRow["ThirdDenomValue"].ToString()
            , dataRow["ThirdDenomNumber"].ToString()
            , dataRow["FourthDenomValue"].ToString()
            , dataRow["FourthDenomNumber"].ToString()
            , dataRow["FifthDenomValue"].ToString()
            , dataRow["FifthDenomNumber"].ToString()
            , dataRow["SixthDenomValue"].ToString()
            , dataRow["SixthDenomNumber"].ToString()
            , dataRow["SeventhDenomValue"].ToString()
            , dataRow["SeventhDenomNumber"].ToString()
            , dataRow["EighthDenomValue"].ToString()
            , dataRow["EighthDenomNumber"].ToString()
            , dataRow["NinthDenomValue"].ToString()
            , dataRow["NinthDenomNumber"].ToString()
            , dataRow["TenthDenomValue"].ToString()
            , dataRow["TenthDenomNumber"].ToString()
            ,className
            //, dataRow["EleventhDenomValue"].ToString()
            //, dataRow["EleventhDenomNumber"].ToString()
                //, dataRow["Id"].ToString()
            );
        }

        //GetCashDispenser
        private CashDispenser GetCashDispenser(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new CashDispenser(dataRow["KioskName"].ToString(),
             dataRow["FirstDenomNumber"].ToString()
            , dataRow["SecondDenomNumber"].ToString()
            , dataRow["ThirdDenomNumber"].ToString()
            , dataRow["FourthDenomNumber"].ToString()
            , dataRow["FifthDenomNumber"].ToString()
            , dataRow["SixthDenomNumber"].ToString()
            , dataRow["SeventhDenomNumber"].ToString()
            , dataRow["EighthDenomNumber"].ToString()
            , dataRow["Casette1Reject"].ToString()
            , dataRow["Casette2Reject"].ToString()
            , dataRow["Casette3Reject"].ToString()
            , dataRow["Casette4Reject"].ToString()
            , dataRow["FirstDenomValue"].ToString()
            , dataRow["SecondDenomValue"].ToString()
            , dataRow["ThirdDenomValue"].ToString()
            , dataRow["FourthDenomValue"].ToString()
            , dataRow["FifthDenomValue"].ToString()
            , dataRow["SixthDenomValue"].ToString()
            , dataRow["SeventhDenomValue"].ToString()
            , dataRow["EighthDenomValue"].ToString()
            , dataRow["Cassette1Status"].ToString()
            , dataRow["Cassette2Status"].ToString()
            , dataRow["Cassette3Status"].ToString()
            , dataRow["Cassette4Status"].ToString()
            , dataRow["Hopper1Status"].ToString()
            , dataRow["Hopper2Status"].ToString()
            , dataRow["Hopper3Status"].ToString()
            , dataRow["Hopper4Status"].ToString()
           );
        }


        private KioskDispenserCashCount GetKioskDispenserCashCount(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskDispenserCashCount(dataRow["KioskName"].ToString()
            , dataRow["InstitutionName"].ToString()
            //, dataRow["InsertionDate"].ToString()
            , dataRow["KioskId"].ToString()
            , dataRow["FirstDenomValue"].ToString()
            , dataRow["FirstDenomNumber"].ToString()
            , dataRow["SecondDenomValue"].ToString()
            , dataRow["SecondDenomNumber"].ToString()
            , dataRow["ThirdDenomValue"].ToString()
            , dataRow["ThirdDenomNumber"].ToString()
            , dataRow["FourthDenomValue"].ToString()
            , dataRow["FourthDenomNumber"].ToString()
            , dataRow["FifthDenomValue"].ToString()
            , dataRow["FifthDenomNumber"].ToString()
            , dataRow["SixthDenomValue"].ToString()
            , dataRow["SixthDenomNumber"].ToString()
            , dataRow["SeventhDenomValue"].ToString()
            , dataRow["SeventhDenomNumber"].ToString()
            , dataRow["EighthDenomValue"].ToString()
            , dataRow["EighthDenomNumber"].ToString()
            , dataRow["Casette1Reject"].ToString()
            , dataRow["Casette2Reject"].ToString()
            , dataRow["Casette3Reject"].ToString()
            , dataRow["Casette4Reject"].ToString()
            , dataRow["ClassName"].ToString()
    
            );
        }

        private KioskDispenserCashCount GetKioskDispenserCashCount2(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new KioskDispenserCashCount(dataRow["KioskName"].ToString()
            , dataRow["InstitutionName"].ToString()
            //, dataRow["InsertionDate"].ToString()
            , dataRow["KioskId"].ToString()
            , dataRow["FirstDenomValue"].ToString()
            , dataRow["FirstDenomNumber"].ToString()
            , dataRow["SecondDenomValue"].ToString()
            , dataRow["SecondDenomNumber"].ToString()
            , dataRow["ThirdDenomValue"].ToString()
            , dataRow["ThirdDenomNumber"].ToString()
            , dataRow["FourthDenomValue"].ToString()
            , dataRow["FourthDenomNumber"].ToString()
            , dataRow["FifthDenomValue"].ToString()
            , dataRow["FifthDenomNumber"].ToString()
            , dataRow["SixthDenomValue"].ToString()
            , dataRow["SixthDenomNumber"].ToString()
            , dataRow["SeventhDenomValue"].ToString()
            , dataRow["SeventhDenomNumber"].ToString()
            , dataRow["EighthDenomValue"].ToString()
            , dataRow["EighthDenomNumber"].ToString()
            , dataRow["Casette1Reject"].ToString()
            , dataRow["Casette2Reject"].ToString()
            , dataRow["Casette3Reject"].ToString()
            , dataRow["Casette4Reject"].ToString()
            , "sum"
            , dataRow["ClassName"].ToString()

            );
        }
    }
}
