﻿using Microsoft.ApplicationBlocks.Data;
using PRCNCORE.Input;
using PRCNCORE.Menus;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Backend
{
    public class MenuOpr : DBOperator
    {
        public MenuOpr()
        {

        }

        internal MenuOpr(SqlConnection sqlConnection)
            : base(sqlConnection)
        {

        }

        public MenuCollection GetMenus(int roleId)
        {
            DataSet dataSet = null;
            MenuCollection menus = new MenuCollection();

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@RoleId", roleId) };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[dbo].[get_role_menu]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    menus.Add(this.GetMenu(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetMenus");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return menus;
        }

        public MenuCollection GetMenus()
        {
            DataSet dataSet = null;
            MenuCollection menus = new MenuCollection();

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@SearchText", null) };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[dbo].[get_product_types]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    menus.Add(this.GetMenu(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetMenus1");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return menus;
        }

        private Menu GetMenu(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new Menu(Convert.ToInt32(dataRow["Id"]),
                        dataRow["Name"].ToString()
                        );
        }

        public bool UpdateMenu(UpdateMenu inputData)
        {
            bool success = false;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]{  new SqlParameter("@Id",inputData.Id),
                                                                    new SqlParameter("@Name",inputData.Name)};

                int a = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[dbo].[update_product_type]", sqlParameters);

                if (a >= -1)
                    success = true;
                else
                    success = false;
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREUpdateMenu");
            }
            finally
            {
                base.Dispose();

            }
            return success;
        }

        public bool DeleteMenu(DeleteMenu inputData)
        {
            bool success = false;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@Id", inputData.Id) };

                int a = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[dbo].[delete_product_type]", sqlParameters);

                if (a >= -1)
                    success = true;
                else
                    success = false;
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREDeleteMenu");

            }
            finally
            {
                base.Dispose();

            }
            return success;
        }

        public bool SaveMenu(SaveMenu inputData)
        {
            bool success = false;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@Name", inputData.Name) };

                int a = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[dbo].[save_product_type]", sqlParameters);

                if (a >= -1)
                    success = true;
                else
                    success = false;
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveMenu");

            }
            finally
            {
                base.Dispose();

            }
            return success;
        }
    }
}


