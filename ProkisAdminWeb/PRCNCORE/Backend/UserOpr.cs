﻿using Microsoft.ApplicationBlocks.Data;
using PRCNCORE.Constants;
using PRCNCORE.Input;
using PRCNCORE.Users;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Backend
{
    public class UserOpr : DBOperator
    {
        public UserOpr()
        {

        }

        internal UserOpr(SqlConnection sqlConnection)
            : base(sqlConnection)
        {

        }



        private RoleAction GetRoleAction(DataRow dataRow)
        {
            return new RoleAction(Convert.ToInt32(dataRow["Id"].ToString()),
                                  dataRow["RoleName"].ToString(),
                                  Convert.ToBoolean( dataRow["Status"].ToString()),
                                  dataRow["RoleLabel"].ToString()
                                );
        }



        private WebUser GetUser(DataRow dataRow)
        {
            return new WebUser(Convert.ToInt32(dataRow["Id"].ToString()),
                                  dataRow["Name"].ToString(),
                                  dataRow["Username"].ToString(),
                                  dataRow["Email"].ToString(),
                                  dataRow["Password"].ToString(),
                                  dataRow["Telephone"].ToString(),
                                  dataRow["Cellphone"].ToString(),
                                  Convert.ToInt16(dataRow["UserStatusId"]),
                                  dataRow["StatusName"].ToString(),
                                  Convert.ToInt32(dataRow["RoleId"].ToString()),
                                  dataRow["RoleName"].ToString(),
                                  Convert.ToDateTime(dataRow["CreatedDate"].ToString()),
                                  Convert.ToDateTime(dataRow["LastLoginDate"].ToString()),
                                  Convert.ToInt16(dataRow["PaswordMustChangeId"].ToString()),
                                  Convert.ToDateTime(dataRow["LastPasswordChangeDate"].ToString()),
                                  dataRow["LastPasswords"].ToString(),
                                  Convert.ToInt16(dataRow["WrongAccessTryCount"].ToString()),
                                  Convert.ToDateTime(dataRow["LastWrongAccessTryDate"].ToString()),
                                  dataRow["ProfileImagePath"].ToString(),
                                  dataRow["ProfileFilePath"].ToString(),
                                  dataRow["ProfessionTitle"].ToString(),
                                   dataRow["MainPage"].ToString()
                                  );
        }

        private BlackList GetBlackList(DataRow dataRow)
        {
            return new BlackList(Convert.ToInt32(dataRow["Id"].ToString()),
                                  dataRow["Name"].ToString(),
                                  dataRow["Tckn"].ToString(),
                                  dataRow["BirthYear"].ToString(),
                                  dataRow["Description"].ToString(),
                                  dataRow["InsertionDate"].ToString(),
                                  dataRow["CreateUser"].ToString(),
                                  Convert.ToInt32(dataRow["Status"].ToString()));
        }


        private ApproveUser GetApproveUser(DataRow dataRow)
        {
            return new ApproveUser(Convert.ToInt32(dataRow["Id"].ToString()),
                                  dataRow["Name"].ToString(),
                                  dataRow["Username"].ToString(),
                                  dataRow["Email"].ToString(),
                                  dataRow["Password"].ToString(),
                                  dataRow["Telephone"].ToString(),
                                  dataRow["Cellphone"].ToString(),
                                  dataRow["StatusName"].ToString(),
                                  dataRow["RoleName"].ToString(),
                                  dataRow["UpdatedDate"].ToString(),
                                  dataRow["UpdatedUser"].ToString());
        }

        public WebUser GetUser(int userId)
        {
            DataSet dataSet = null;
            WebUser user = null;

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@userId", userId) };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[get_user]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    user = this.GetUser(dataRow);
                }
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetUser");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return user;
        }

        public WebUser GetApproveUser(int userId)
        {
            DataSet dataSet = null;
            WebUser user = null;

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@userId", userId) };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[get_approve_user]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    user = this.GetUser(dataRow);
                }
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetUser");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return user;
        }

        public WebUser GetLoginUser(string username, string userpassword, int minute, int tryLimit, ref int operationResult, int expireDay, ref string accessToken)
        {
            WebUser theUser = null;
            DataSet theDataSet = null;

            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[]{  new SqlParameter("@username", username),
                                                                    new SqlParameter("@userPassword", userpassword),
                                                                    new SqlParameter("@minute", minute),
                                                                    new SqlParameter("@tryLimit", tryLimit),
                                                                    new SqlParameter("@loginResult", operationResult),
                                                                    new SqlParameter("@expireDay", expireDay),
                                                                    new SqlParameter("@accessToken", SqlDbType.VarChar, 200)};

                sqlParameters[4].Direction = ParameterDirection.InputOutput;
                sqlParameters[6].Direction = ParameterDirection.InputOutput;

                theDataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[get_login_user]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);
                operationResult = Convert.ToInt32(sqlParameters[4].Value);
                accessToken = sqlParameters[6].Value.ToString();

                if (theDataSet.Tables.Count != 0)
                {
                    if (theDataSet.Tables[0].Rows.Count != 0)
                        theUser = this.GetUser(theDataSet.Tables[0].Rows[0]);
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetLoginUser");

            }
            finally
            {
                base.Dispose();

                if (theDataSet != null)
                {
                    theDataSet.Dispose();
                    theDataSet = null;
                }
            }
            return theUser;
        }

        public bool ChangeUserPassword(int userId, string newPassword, string oldPassword, int updatedUser)
        {
            try
            {
                DateTime updatedDate = DateTime.Now;

                SqlParameter[] sqlParameters = new SqlParameter[]{  new SqlParameter("@UserId", userId),
                                                                    new SqlParameter("@newPassword", newPassword),
                                                                    new SqlParameter("@oldPassword", oldPassword),
                                                                    new SqlParameter("@updatedUser", updatedUser)};

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[change_user_password]", sqlParameters);

                return true;
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREChangeUserPassword");

                return false;
            }
            finally
            {
                base.Dispose();
            }
        }

        public bool ChangeUserPasswordAdmin(int userId, string newPassword, int updatedUser, out string telephone, out string email, out string name)
        {
            int returnCode = 0;
            telephone = "";
            email = "";
            name = "";

            try
            {
                DateTime updatedDate = DateTime.Now;

                SqlParameter[] sqlParameters = new SqlParameter[]{  new SqlParameter("@UserId", userId),
                                                                    new SqlParameter("@newPassword", newPassword),
                                                                    new SqlParameter("@updatedUser", updatedUser),
                                                                    new SqlParameter("@name", SqlDbType.VarChar,200),
                                                                    new SqlParameter("@telephone", SqlDbType.VarChar,200),
                                                                    new SqlParameter("@email", SqlDbType.VarChar,200),
                                                                    new SqlParameter("@returnCode", returnCode)};

                sqlParameters[3].Direction = ParameterDirection.InputOutput;
                sqlParameters[4].Direction = ParameterDirection.InputOutput;
                sqlParameters[5].Direction = ParameterDirection.InputOutput;
                sqlParameters[6].Direction = ParameterDirection.InputOutput;

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[change_user_password_admin]", sqlParameters);

                returnCode = Convert.ToInt32(sqlParameters[6].Value);

                if (returnCode == 1)
                {
                    name = sqlParameters[3].Value.ToString();
                    telephone = sqlParameters[4].Value.ToString();
                    email = sqlParameters[5].Value.ToString();

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREChangeUserPassword");

                return false;
            }
            finally
            {
                base.Dispose();
            }
        }

        public bool UpdateUser(int id,
                              string name,
                              string userName,
                              string email,
                              string cellphone,
                              string telephone,
                              int roleId,
                              int status,
                              int updatedUser)
        {
            bool response = true;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]{  new SqlParameter("@id",id),
                                                                    new SqlParameter("@name",name),
                                                                    new SqlParameter("@userName",userName),
                                                                    new SqlParameter("@email",email),
                                                                    new SqlParameter("@cellphone",cellphone),
                                                                    new SqlParameter("@telephone",telephone),
                                                                    new SqlParameter("@roleId",roleId),
                                                                    new SqlParameter("@status",status),
                                                                    new SqlParameter("@updatedUser",updatedUser)};

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[update_user]", sqlParameters);

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREUpdateUser");

                response = false;
            }
            finally
            {
                base.Dispose();

            }
            return response;
        }
        public int SaveProject(string name 
                           )
        {
            int userId = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] {  
                                                                    new SqlParameter("@name", name),
                                                                   };

          
                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[dbo].[add_project]", sqlParameters);

                userId =1;
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveUser");

                userId = (int)ManagementScreenErrorCodes.SystemError;
            }
            finally
            {
                base.Dispose();

            }
            return userId;
        }


        public int UpdateRoleConnection(int userId, string roleId)
        {
            int userId2 = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] {
                                                                    new SqlParameter("@roleId", roleId),
                                                                    new SqlParameter("@userId", userId),
                                                                   };

             
                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[dbo].[add_or_update_userrole_connection]", sqlParameters);

                userId = 1;
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveUser");

                userId2 = (int)ManagementScreenErrorCodes.SystemError;
            }
            finally
            {
                base.Dispose();

            }
            return userId2;
        }

        public int UpdateRoleProject(int roleId, int projectId  
                           )
        {
            int userId = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] {
                                                                    new SqlParameter("@Id", roleId),
                                                                    new SqlParameter("@projectId", projectId),
                                                                   };

               
                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[dbo].[update_project_by_roleid]", sqlParameters);

                userId = 1;
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveUser");

                userId = (int)ManagementScreenErrorCodes.SystemError;
            }
            finally
            {
                base.Dispose();

            }
            return userId;
        }

        public int SaveUser(string name,
                            string userName,
                            string email,
                            string password,
                            string telephone,
                            string cellphone,
                            int status,
                            int roleId,
                            int updatedUser)
        {
            int userId = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@userId", userId),
                                                                    new SqlParameter("@name", name),
                                                                    new SqlParameter("@userName", userName),
                                                                    new SqlParameter("@email", email),
                                                                    new SqlParameter("@password", password),
                                                                    new SqlParameter("@telephone", telephone),
                                                                    new SqlParameter("@cellphone", cellphone),
                                                                    new SqlParameter("@status", status),
                                                                    new SqlParameter("@roleId", roleId),
                                                                    new SqlParameter("@updatedUser", updatedUser)};

                sqlParameters[0].Direction = ParameterDirection.InputOutput;
                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[add_user]", sqlParameters);

                userId = Convert.ToInt32(sqlParameters[0].Value);
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveUser");

                userId = (int)ManagementScreenErrorCodes.SystemError;
            }
            finally
            {
                base.Dispose();

            }
            return userId;
        }


        public int SaveBlackList(string name,
                           string tckn,
                           string birthYear,
                           string description,
                           int updatedUser)
        {
            int userId = 0;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@userId", userId),
                                                                    new SqlParameter("@name", name),
                                                                    new SqlParameter("@tckn", tckn),
                                                                    new SqlParameter("@birthYear", birthYear),
                                                                    new SqlParameter("@description", description),
                                                                    new SqlParameter("@updatedUser", updatedUser)};

                sqlParameters[0].Direction = ParameterDirection.InputOutput;
                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[add_black_list]", sqlParameters);

                userId = Convert.ToInt32(sqlParameters[0].Value);
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveUBlackList");

                userId = (int)ManagementScreenErrorCodes.SystemError;
            }
            finally
            {
                base.Dispose();

            }
            return userId;
        }
        //
        public int DeleteRoleAction(int roleActionId)
        {
            RoleAction[] users = null;
            //DataSet dataSet = null;
            var response = 0;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] {
                    new SqlParameter("@returnCode", 0),
                    new SqlParameter("@roleActionId", roleActionId)};

                //sqlParameters[0].Direction = sqlParameters[0].Direction = ParameterDirection.Output;
                //sqlParameters[1].Direction = sqlParameters[0].Direction = ParameterDirection.Input;

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[delete_roleaction_by_id]", sqlParameters);


                //SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[delete_roleaction_by_id]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                //recordCount = Convert.ToInt32(sqlParameters[0].Value);
                //pageCount = Convert.ToInt32(sqlParameters[1].Value);

                //users = new RoleAction[dataSet.Tables[0].Rows.Count];

                //for (int i = 0; i < users.Length; i++)
                //{
                //    users[i] = this.GetRoleAction(dataSet.Tables[0].Rows[i]);
                //}
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchRoleActionsOrder");

            }
            finally
            {

                base.Dispose();
                //if (dataSet != null)
                //{
                //    dataSet.Dispose();
                //    dataSet = null;
                //}
            }
            return response;
        }
        //


        public int AddRoleAction(string roleName, int status, string roleLabel)
        {
            //RoleAction[] users = null;
            var response = 0;
            //DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] {
                    new SqlParameter("@returnCode", 0),
                    //new SqlParameter("@Id", roleActionId),
                    new SqlParameter("@roleName", roleName),
                    new SqlParameter("@status", status),
                    new SqlParameter("@roleLabel", roleLabel),
                };

                //sqlParameters[0].Direction = sqlParameters[0].Direction = ParameterDirection.InputOutput;
                //sqlParameters[1].Direction = sqlParameters[1].Direction = ParameterDirection.Input;
                //sqlParameters[2].Direction = sqlParameters[2].Direction = ParameterDirection.Input;
                //sqlParameters[3].Direction = sqlParameters[3].Direction = ParameterDirection.Input;
                //sqlParameters[4].Direction = sqlParameters[4].Direction = ParameterDirection.Input;



                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[insert_role_action]", sqlParameters);

                //dataSet = SqlHelper.ExecuteDataset(
                //    base.GetConnection(), CommandType.StoredProcedure, "", 
                //    Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                //recordCount = Convert.ToInt32(sqlParameters[0].Value);
                //pageCount = Convert.ToInt32(sqlParameters[1].Value);
                //var d = dataSet.Tables[0];
                //users = new RoleAction[dataSet.Tables[0].Rows.Count];

                //for (int i = 0; i < users.Length; i++)
                //{
                //    users[i] = this.GetRoleAction(dataSet.Tables[0].Rows[i]);
                //}
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchRoleActionsOrder");

            }
            finally
            {

                base.Dispose();

                //if (dataSet != null)
                //{
                //    dataSet.Dispose();
                //    dataSet = null;
                //}
            }
            return response;
        }




        public int UpdateRoleAction(int roleActionId, string roleName, int status, string roleLabel)
        {
            //RoleAction[] users = null;
            var response = 0;
            //DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] {
                    new SqlParameter("@returnCode", 0),
                    new SqlParameter("@Id", roleActionId),
                    new SqlParameter("@roleName", roleName),
                    new SqlParameter("@status", status),
                    new SqlParameter("@roleLabel", roleLabel),
                };

                //sqlParameters[0].Direction = sqlParameters[0].Direction = ParameterDirection.InputOutput;
                //sqlParameters[1].Direction = sqlParameters[1].Direction = ParameterDirection.Input;
                //sqlParameters[2].Direction = sqlParameters[2].Direction = ParameterDirection.Input;
                //sqlParameters[3].Direction = sqlParameters[3].Direction = ParameterDirection.Input;
                //sqlParameters[4].Direction = sqlParameters[4].Direction = ParameterDirection.Input;


             
                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[update_roleaction]", sqlParameters);

                //dataSet = SqlHelper.ExecuteDataset(
                //    base.GetConnection(), CommandType.StoredProcedure, "", 
                //    Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                //recordCount = Convert.ToInt32(sqlParameters[0].Value);
                //pageCount = Convert.ToInt32(sqlParameters[1].Value);
                //var d = dataSet.Tables[0];
                //users = new RoleAction[dataSet.Tables[0].Rows.Count];

                //for (int i = 0; i < users.Length; i++)
                //{
                //    users[i] = this.GetRoleAction(dataSet.Tables[0].Rows[i]);
                //}
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchRoleActionsOrder");

            }
            finally
            {

                base.Dispose();

                //if (dataSet != null)
                //{
                //    dataSet.Dispose();
                //    dataSet = null;
                //}
            }
            return response;
        }
        //
        public RoleAction[] SearchRoleActionsOrder()
        {
            RoleAction[] users = null;
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@returnCode", 0) };

                sqlParameters[0].Direction = sqlParameters[0].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[get_all_roleactions]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                //recordCount = Convert.ToInt32(sqlParameters[0].Value);
                //pageCount = Convert.ToInt32(sqlParameters[1].Value);

                users = new RoleAction[dataSet.Tables[0].Rows.Count];

                for (int i = 0; i < users.Length; i++)
                {
                    users[i] = this.GetRoleAction(dataSet.Tables[0].Rows[i]);
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchRoleActionsOrder");

            }
            finally
            {

                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return users;
        }
        //

        public WebUser[] SearchUsersOrder(string searchText,
                                      int numberOfItemsPerPage,
                                      int currentPageNum,
                                      ref int recordCount,
                                      ref int pageCount,
                                      int orderSelectionColumn,
                                      int descAsc)
        {
            WebUser[] users = null;
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc)
                                                                 };

                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[search_users_order]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                users = new WebUser[dataSet.Tables[0].Rows.Count];

                for (int i = 0; i < users.Length; i++)
                {
                    users[i] = this.GetUser(dataSet.Tables[0].Rows[i]);
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchUsersOrder");

            }
            finally
            {

                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return users;
        }

        public WebUser[] SearchUsersOrderByRoles(string searchText,
                              int numberOfItemsPerPage,
                              int currentPageNum,
                              ref int recordCount,
                              ref int pageCount,
                              int orderSelectionColumn,
                              int descAsc)
        {
            WebUser[] users = null;
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc)
                                                                 };

                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[search_users_order_by_role]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                users = new WebUser[dataSet.Tables[0].Rows.Count];

                for (int i = 0; i < users.Length; i++)
                {
                    users[i] = this.GetUser(dataSet.Tables[0].Rows[i]);
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchUsersOrder");

            }
            finally
            {

                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return users;
        }

        public BlackList[] SearchBlackListOrder(string searchText,
                                      int numberOfItemsPerPage,
                                      int currentPageNum,
                                      ref int recordCount,
                                      ref int pageCount,
                                      int orderSelectionColumn,
                                      int descAsc)
        {
            BlackList[] users = null;
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc)
                                                                 };

                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[search_black_list_order]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                users = new BlackList[dataSet.Tables[0].Rows.Count];

                for (int i = 0; i < users.Length; i++)
                {
                    users[i] = this.GetBlackList(dataSet.Tables[0].Rows[i]);
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchBlackListsOrder");

            }
            finally
            {

                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return users;
        }

        public ApproveUser[] SearchApproveUsersOrder(string searchText,
                                      int numberOfItemsPerPage,
                                      int currentPageNum,
                                      ref int recordCount,
                                      ref int pageCount)
        {
            ApproveUser[] users = null;
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum)
                                                                 };

                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[search_approve_users]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                users = new ApproveUser[dataSet.Tables[0].Rows.Count];

                for (int i = 0; i < users.Length; i++)
                {
                    users[i] = this.GetApproveUser(dataSet.Tables[0].Rows[i]);
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchApproveUsersOrder");

            }
            finally
            {

                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return users;
        }

        public void DeleteOperation(int userId, string storedProcedure, int itemId, ref int returnCode)
        {
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@returnCode", returnCode),
                                                                    new SqlParameter("@userId", userId),
                                                                    new SqlParameter("@itemId", itemId)};
                sqlParameters[0].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, storedProcedure, sqlParameters);
                returnCode = Convert.ToInt32(sqlParameters[0].Value);
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREDeleteOperationUser");

            }
            finally
            {
                base.Dispose();

            }
        }
    }
}