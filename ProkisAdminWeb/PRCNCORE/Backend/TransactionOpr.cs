﻿using Microsoft.ApplicationBlocks.Data;
using PRCNCORE.Accounts;
using PRCNCORE.Constants;
using PRCNCORE.Transactions;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Backend
{
    public class TransactionOpr : DBOperator
    {
        public TransactionOpr()
        {

        }

        internal TransactionOpr(SqlConnection sqlConnection)
            : base(sqlConnection)
        {

        }

        public TransactionCollection GetTransactions()
        {
            DataSet dataSet = null;
            TransactionCollection transaction = new TransactionCollection();

            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_transactions", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    transaction.Add(this.GetTransaction(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetTransactions");
            }
            finally
            {

                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transaction;
        }

        public bool CheckCustomerId(string customerNumber, int institutionId)
        {

            DataSet dataSet = null;
            try
            {
                int returnCode = 0;

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@customerId", customerNumber),
                                                                    new SqlParameter("@returnCode", returnCode),
                                                                    new SqlParameter("@institutionId", institutionId)
                };

                sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.check_customer_id_if_exist", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                returnCode = Convert.ToInt32(sqlParameters[1].Value);

                if (returnCode == 1) return true; else return false;

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CheckCustomerId");

                return false;
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

        }
        public TransactionDetailCollection GetTransactionDetails(int transactionId)
        {
            DataSet dataSet = null;
            TransactionDetailCollection transactionDetails = new TransactionDetailCollection();

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@transactionId", transactionId) };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_transaction_details_temp", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    transactionDetails.Add(this.GetTransactionDetail(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetTransactionDetails");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactionDetails;
        }

        public TransactionDetailCollection GetPurseTransactionDetails(int transactionId)
        {
            DataSet dataSet = null;
            TransactionDetailCollection transactionDetails = new TransactionDetailCollection();

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@transactionId", transactionId) };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_purse_transaction_details",Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    transactionDetails.Add(this.GetTransactionDetail(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetPurseTransactionDetails");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactionDetails;
        }


        public int ChangeTransactionStatus(int transactionId, int userId, int adminUserId, string newStatus, string explanation)
        {
            DataSet dataSet = null;
            string returnCode = "";
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@transactionId", transactionId),
                                                                    new SqlParameter("@userId", userId),
                                                                    new SqlParameter("@adminUserId", adminUserId),
                                                                    new SqlParameter("@newStatus", Convert.ToInt32(newStatus)),
                                                                    new SqlParameter("@explanation", explanation),
                                                                    new SqlParameter("@insertionDate", DateTime.Now),
                                                                    new SqlParameter("@returnCode", SqlDbType.VarChar,20)};

                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.Input;
                sqlParameters[2].Direction = sqlParameters[3].Direction = ParameterDirection.Input;
                sqlParameters[4].Direction = sqlParameters[5].Direction = ParameterDirection.Input;
                sqlParameters[6].Direction = ParameterDirection.Output;



                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.update_txn_status", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                returnCode = sqlParameters[6].Value.ToString();

                //foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                //{
                //    transactionDetails.Add(this.GetTransactionDetail(dataRow, dataSet.Tables[0].Columns));
                //}
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetPurseTransactionDetails");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return Convert.ToInt32(returnCode);
        }

        private TransactionDetail GetTransactionDetail(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new TransactionDetail(Convert.ToInt32(dataRow["TransactionId"])
            , dataRow["ProcessStatus"].ToString()
            , dataRow["ProcessDescription"].ToString()
            , dataRow["TransactionDate"].ToString()
            , dataRow["KioskName"].ToString());
        }

        private Transaction GetTransaction(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new Transaction(Convert.ToInt32(dataRow["TransactionId"])
            , Convert.ToInt32(dataRow["KioskId"])
            , dataRow["KioskName"].ToString()
            , dataRow["TransactionDate"].ToString()
            , dataRow["CustomerName"].ToString()
            , dataRow["AboneNo"].ToString()
            //--------------------------------------------------------------, Convert.ToInt32(dataRow["CardId"])
            , dataRow["CardId"].ToString()
            , dataRow["ProcessStatus"].ToString()
            , dataRow["InstutionName"].ToString()
            , dataRow["OperationTypeName"].ToString()
            );
        }

        private TransactionDetails GetTransactionDetails(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new TransactionDetails(Convert.ToInt32(dataRow["TransactionId"]).ToString()
            , dataRow["KioskId"].ToString()
            , dataRow["TotalAmount"].ToString()
            , dataRow["CashAmount"].ToString()
            , dataRow["CodeAmount"].ToString()
            , dataRow["CreditCardAmount"].ToString()
            , dataRow["InstitutionAmount"].ToString()
            , dataRow["UsageAmount"].ToString()
            , dataRow["ProcessType"].ToString()
            , dataRow["Cassette1Value"].ToString()
            , dataRow["Cassette2Value"].ToString()
            , dataRow["Cassette3Value"].ToString()
            , dataRow["Cassette4Value"].ToString()
            , dataRow["Cassette1Reject"].ToString()
            , dataRow["Cassette2Reject"].ToString()
            , dataRow["Cassette3Reject"].ToString()
            , dataRow["Cassette4Reject"].ToString()
            , dataRow["Cassette1Exit"].ToString()
            , dataRow["Cassette2Exit"].ToString()
            , dataRow["Cassette3Exit"].ToString()
            , dataRow["Cassette4Exit"].ToString()
            , dataRow["Cassette1Total"].ToString()
            , dataRow["Cassette2Total"].ToString()
            , dataRow["Cassette3Total"].ToString()
            , dataRow["Cassette4Total"].ToString()
            , dataRow["CassetteAllTotal"].ToString()
            , dataRow["ReverseRecord"].ToString()
            );
        }

        private CreditCardTransaction GetCreditCardTransaction(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new CreditCardTransaction(Convert.ToInt32(dataRow["TransactionId"])
           // , Convert.ToInt32(dataRow["KioskId"]) //iptal
            , dataRow["KioskName"].ToString()
            , dataRow["TransactionDate"].ToString()
            , dataRow["CustomerName"].ToString()
            //, dataRow["AboneNo"].ToString()
            , dataRow["CreditCardNo"].ToString()
            , dataRow["CreditCardOwner"].ToString()
            , dataRow["InstutionName"].ToString()
            , dataRow["RRN"].ToString()
            , dataRow["TransactionAmount"].ToString()
            , dataRow["MuhasebeStatus"].ToString() 
            , dataRow["ApprovedCode"].ToString()
            , dataRow["Batch"].ToString() 
            , ""
            );
        }

        private CustomerContacts GetCustomerContacts(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new CustomerContacts(dataRow["InsertionDate"].ToString()
            , dataRow["PhoneNumber"].ToString()
            , dataRow["AboneNo"].ToString()
            , dataRow["CustomerName"].ToString()
            , dataRow["InstitutionName"].ToString()
            , ""
            );
        }

        //MobilePaymentTransaction
        private MobilePaymentTransaction GetMobilePaymentTransaction(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new MobilePaymentTransaction(Convert.ToInt32(dataRow["TransactionId"])
                // , Convert.ToInt32(dataRow["KioskId"]) //iptal
            , dataRow["KioskName"].ToString()
            , dataRow["TransactionDate"].ToString()
            , dataRow["CustomerName"].ToString()
                //, dataRow["AboneNo"].ToString()
            , dataRow["MobileNo"].ToString()
            , dataRow["Status"].ToString()
            , dataRow["InstitutionName"].ToString()
            , dataRow["TransactionAmount"].ToString()
            //, dataRow["RRN"].ToString()
            //, dataRow["TransactionAmount"].ToString()
            ,dataRow["MuhasebeStatus"].ToString()
            , dataRow["AboneNo"].ToString()
            , dataRow["Rnd"].ToString()
            , ""
            );
        }

        //GetRepairCard
        private RepairCard GetRepairCard(DataRow dataRow, DataColumnCollection dataColumns)
        {
            

            return new RepairCard(dataRow["CustomerName"].ToString()  
            , Convert.ToInt32(dataRow["TransactionId"])
            , dataRow["KioskName"].ToString()
            , dataRow["AnaKredi"].ToString()
            , dataRow["YedekKredi"].ToString()
            , dataRow["InsertionDate"].ToString()
            , dataRow["TransactionDate"].ToString()
            , dataRow["UserName"].ToString()
            , dataRow["Status"].ToString()
            , dataRow["IsDeleted"].ToString()
            , dataRow["RepairDate"].ToString()
            , (dataRow["RepairedTransactionId"]).ToString()
            , (dataRow["RepairedId"]).ToString()
            );
        }

        private FailedTransaction GetFailedTransaction(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new FailedTransaction(Convert.ToInt32(dataRow["TransactionId"])
            , Convert.ToInt32(dataRow["KioskId"])
            , dataRow["KioskName"].ToString()
            , dataRow["TransactionDate"].ToString()
            , dataRow["CustomerName"].ToString()
            , dataRow["AboneNo"].ToString()
                //--------------------------------------------------------------, Convert.ToInt32(dataRow["CardId"])
            , dataRow["CardId"].ToString()
            , dataRow["ProcessStatus"].ToString()
            , dataRow["InstutionName"].ToString()
            , dataRow["OperationTypeName"].ToString()
            );
        }

        private PurseTransaction GetPurseTransaction(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new PurseTransaction(Convert.ToInt32(dataRow["TransactionId"])
                                     , dataRow["CustomerName"].ToString()
                                     , dataRow["CustomerNo"].ToString()
                                     , dataRow["TransactionDate"].ToString()
                                     , dataRow["KioskName"].ToString()
                                     , dataRow["IsKiosk"].ToString());
        }

        private AccountTransaction GetAccountTransaction(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new AccountTransaction(Convert.ToInt32(dataRow["Id"])
                                     , dataRow["TransactionDate"].ToString()
                                     , dataRow["Description"].ToString()
                                     , dataRow["TransactionType"].ToString()
                                     , dataRow["Amount"].ToString()
                                     , dataRow["SenderNo"].ToString()
                                     , dataRow["SenderBalance"].ToString()
                                     , dataRow["RecieverNo"].ToString()
                //, dataRow["RecieverBalance"].ToString()
                                     , dataRow["TransactionPrice"].ToString()
                                     , dataRow["TransactionPlatform"].ToString()
                                     , Convert.ToInt32(dataRow["ActionStatusId"])
                                     , dataRow["ActionStatus"].ToString()
                                     , Convert.ToInt32(dataRow["FraudTypeId"])
                                     , dataRow["FraudType"].ToString()
                                     , dataRow["FraudApproveTime"].ToString()
                                     , dataRow["FraudApproveUser"].ToString()
                                     , "");

        }


        private CustomerAccount GetCustomerAccount(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new CustomerAccount(Convert.ToInt32(dataRow["Id"])
                                     , dataRow["CustomerNo"].ToString()
                                     , dataRow["CustomerName"].ToString()
                                     , dataRow["Amount"].ToString()
                                     , dataRow["Date"].ToString());
        }

        private CancelTransaction GetCancelTransaction(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new CancelTransaction(Convert.ToInt32(dataRow["Id"])
                                     , dataRow["CustomerNo"].ToString()
                                     , dataRow["CustomerName"].ToString()
                                     , dataRow["BillDate"].ToString()
                                     , dataRow["BillNumber"].ToString());
        }

        public CustomerAccountCollection ListDailyCustomerAccounts(Int64 itemId,
                                 int isActive,
                                 int numberOfItemsPerPage,
                                 int currentPageNum,
                                 ref int recordCount,
                                 ref int pageCount,
                                 int orderSelectionColumn,
                                 int descAsc,
                                 ref string TotalAmount)
        {
            CustomerAccountCollection transactions = new CustomerAccountCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@itemId", itemId),  
                                                                    new SqlParameter("@isActive", isActive),
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@totalAmount", SqlDbType.VarChar,20)
                                                                 };
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                sqlParameters[8].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pp.get_daily_customer_account_report_detail", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);
                TotalAmount = sqlParameters[8].Value.ToString();

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetCustomerAccount(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORECustomerAccount");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }

        public TransactionConditionCollection SearchTransactionConditionsOrder(string searchText,
                                DateTime startDate,
                                DateTime endDate,
                                int numberOfItemsPerPage,
                                int currentPageNum,
                                ref int recordCount,
                                ref int pageCount,
                                int orderSelectionColumn,
                                int descAsc,
                                string kioskId,
                                int instutionId,
                                int paymentTypeId,
                                int cardTypeId,
                                int completeStatusId,
                                int paymentType,
                                ref string totalTotalAmount,
                                ref string totalCashAmount,
                                ref string totalCodeAmount,
                                ref string totalInsttutionAmount,
                                ref string totalKioskUsageFee,
                                ref string totalCalculatedChangeAmount,
                                ref string totalGivenChangeAmount,
                                ref string totalPaidFaturaCount,
                                ref string totalCreditCardAmount,
                                ref string totalCommisionAmount,
                                int orderType)
        {
            TransactionConditionCollection transactions = new TransactionConditionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@instutionId", instutionId),
                                                                    new SqlParameter("@paymentTypeId", paymentTypeId),
                                                                    new SqlParameter("@cardTypeId", cardTypeId),
                                                                    new SqlParameter("@completeStatusId", completeStatusId),
                                                                    new SqlParameter("@totalTotalAmount", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@totalCashAmount", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@totalCodeAmount", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@totalInsttutionAmount", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@totalKioskUsageFee", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@totalCalculatedChangeAmount", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@totalGivenChangeAmount", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@totalPaidFaturaCount", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@totalCreditCardAmount", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@totalCommisionAmount", SqlDbType.VarChar,20),  
                                                                    new SqlParameter("@orderType", orderType) ,
                                                                    new SqlParameter("@paymentType", paymentType)  
                                                                 };

                sqlParameters[3].Value = Convert.ToDateTime(startDate);
                sqlParameters[4].Value = Convert.ToDateTime(endDate);
                sqlParameters[14].Direction = sqlParameters[15].Direction = sqlParameters[16].Direction = sqlParameters[17].Direction = sqlParameters[18].Direction = sqlParameters[19].Direction = sqlParameters[20].Direction = sqlParameters[21].Direction = sqlParameters[22].Direction = sqlParameters[23].Direction = ParameterDirection.Output;
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_transaction_conditions_order", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                totalTotalAmount = sqlParameters[14].Value.ToString();
                totalCashAmount = sqlParameters[15].Value.ToString();
                totalCodeAmount = sqlParameters[16].Value.ToString();
                totalInsttutionAmount = sqlParameters[17].Value.ToString();
                totalKioskUsageFee = sqlParameters[18].Value.ToString();
                totalCalculatedChangeAmount = sqlParameters[19].Value.ToString();
                totalGivenChangeAmount = sqlParameters[20].Value.ToString();
                totalPaidFaturaCount = sqlParameters[21].Value.ToString();
                totalCreditCardAmount = sqlParameters[22].Value.ToString();
                totalCommisionAmount = sqlParameters[23].Value.ToString();

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetTransactionCondition(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
                if (transactions != null)
                {
                    transactions.Add(
                     new TransactionCondition(0, 0, "", "", "", totalTotalAmount.ToString() , totalCashAmount.ToString(), totalCodeAmount.ToString(),
                     totalCreditCardAmount.ToString(), totalCommisionAmount.ToString(),
                     totalInsttutionAmount.ToString(), totalCalculatedChangeAmount.ToString(), totalGivenChangeAmount.ToString(), totalKioskUsageFee.ToString(), totalPaidFaturaCount.ToString(), "", "", "", "", "info"));
                }

                //if (false)//todo toplam alanı nasıl olmalı.
                //{
                //    decimal totalAmount = 0, cashAmount = 0, codeAmount = 0,
                //        creditCardAmount = 0, instutionAmount = 0, usageFee = 0,
                //        calculatedChange = 0, givenChange = 0, paidFaturaCount=0, 
                //        commisionAmount=0;
                //    foreach (var transaction in transactions)
                //    {
                //        decimal.TryParse(transaction.TotalAmount, out decimal r1);
                //        totalAmount += r1;
                //        decimal.TryParse(transaction.CashAmount, out decimal r2);
                //        cashAmount += r2;
                //        decimal.TryParse(transaction.CodeAmount, out decimal r3);
                //        codeAmount += r3;
                //        decimal.TryParse(transaction.CreditCardAmount, out decimal r4);
                //        creditCardAmount += r4;
                //        decimal.TryParse(transaction.InstutionAmount, out decimal r5);
                //        instutionAmount += r5;
                //        decimal.TryParse(transaction.UsageFee, out decimal r6);
                //        usageFee += r6;
                //        decimal.TryParse(transaction.CalculatedChange, out decimal r7);
                //        calculatedChange += r7;
                //        decimal.TryParse(transaction.GivenChange, out decimal r8);
                //        givenChange += r8;
                //        decimal.TryParse(transaction.PaidFaturaCount, out decimal r9);
                //        paidFaturaCount += r9;
                //        decimal.TryParse(transaction.CommisionAmount, out decimal r10);
                //        commisionAmount += r10;
                //    }
                //    transactions.Add(
                //        new TransactionCondition(0, 0, "", "", "", totalAmount.ToString()+ "₺", cashAmount.ToString(), codeAmount.ToString(),
                //        creditCardAmount.ToString(), commisionAmount.ToString(),
                //        instutionAmount.ToString(), calculatedChange.ToString(), givenChange.ToString(), usageFee.ToString(), paidFaturaCount.ToString(), "", "", "", "", "info"));

                //}

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchTransactionConditionsOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;

        }


        //GetPaymentTypeOrder
        public PaymentTypeCollection GetPaymentTypeOrder()
        {
            PaymentTypeCollection names = new PaymentTypeCollection();
            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_payment_type", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    names.Add(this.GetPaymentType(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetPaymentType");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return names;
        }

        //CardRepairType
        public CardRepairTypeCollection GetCardRepairStatusOrder()
        {
            CardRepairTypeCollection names = new CardRepairTypeCollection();
            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_card_repair_status", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    names.Add(this.GetCardRepairStatus(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetCardRepairTypeOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return names;
        }

        private PaymentType GetPaymentType(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new PaymentType(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }

        //GetCardRepairType
        private CardRepairType GetCardRepairStatus(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new CardRepairType(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }


        //GetIsCardDeletedOrder
        public IsCardDeletedCollection GetIsCardDeletedOrder()
        {
            IsCardDeletedCollection names = new IsCardDeletedCollection();
            DataSet dataSet = null;
            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_is_card_deleted", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    names.Add(this.GetIsCardDeletedOrder(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetCardRepairTypeOrder");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return names;
        }

        //GetCardRepairType
        private IsCardDeleted GetIsCardDeletedOrder(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new IsCardDeleted(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString());
        }



        public TransactionConditionCollection SearchTransactionConditionExcelOrder(string searchText,
                               DateTime startDate,
                               DateTime endDate,
                               int orderSelectionColumn,
                               int descAsc,
                               string kioskId,
                               int instutionId,
                               int paymentTypeId,
                               int cardTypeId,
                               int completeStatusId)
        {
            TransactionConditionCollection transactions = new TransactionConditionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@instutionId", instutionId),
                                                                    new SqlParameter("@paymentTypeId", paymentTypeId),
                                                                    new SqlParameter("@cardTypeId", cardTypeId),
                                                                    new SqlParameter("@completeStatusId", completeStatusId)    
                                                                 };

                sqlParameters[1].Value = Convert.ToDateTime(startDate);
                sqlParameters[2].Value = Convert.ToDateTime(endDate);

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_transaction_condition_excel_order", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetTransactionCondition(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchTransactionConditionExcelOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;

        }


        private TransactionCondition GetTransactionCondition(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new TransactionCondition(Convert.ToInt64(dataRow["Id"])
            , Convert.ToInt64(dataRow["TransactionId"])
            , dataRow["KioskName"].ToString()
            , dataRow["TransactionDate"].ToString()
            , dataRow["InstutionName"].ToString()
            , dataRow["TotalAmount"].ToString()
            , dataRow["CashAmount"].ToString()
            , dataRow["CodeAmount"].ToString()
            , dataRow["CreditCardAmount"].ToString()
            , dataRow["CommisionAmount"].ToString()
            , dataRow["InstutionAmount"].ToString()
            , dataRow["CalculatedChangeAmount"].ToString()
            , dataRow["GivenChangeAmount"].ToString()
            , dataRow["KioskUsageFee"].ToString()
            , dataRow["PaidFaturaCount"].ToString()
            , dataRow["CompleteStatusName"].ToString()
            , dataRow["CustomerName"].ToString()
            , dataRow["PayTypeName"].ToString()
            , dataRow["ReverseRecord"].ToString()
            , ""
            );
        }
 

        public CancelTransactionCollection GetApprovedSaleCancelParameters(Int64 itemId)
        {
            CancelTransactionCollection transactions = new CancelTransactionCollection();
            DataSet dataSet = null;
            
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@itemId", itemId),
                                                                    new SqlParameter("@billDate", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@billNumber", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@customerNumber", SqlDbType.VarChar,20)
                                                                 };

                sqlParameters[0].Direction = ParameterDirection.Input;
                sqlParameters[1].Direction = ParameterDirection.Output;
                sqlParameters[2].Direction = ParameterDirection.Output;
                sqlParameters[3].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_transaction_cancel_params",Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                string makbuzTarihi = sqlParameters[1].Value.ToString();
                string makbuzNo = sqlParameters[2].Value.ToString();
                string aboneNo = sqlParameters[3].Value.ToString();

                transactions.Add(new CancelTransaction(itemId,aboneNo,"",makbuzTarihi,makbuzNo));
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchCustomerAccountExcel");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }

        public string LogCancelTnxOperation(int transactionId
                                            , int userId
                                            , int adminUserId
                                            , string customerNo
                                            , string billDate
                                            , string billNumber
                                            , string serviceResult)
        {
            DataSet dataSet = null;
            string returnCode = "";
            DateTime insertionDate = DateTime.Now;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@transactionId", transactionId),
                                                                    new SqlParameter("@userId", userId),
                                                                    new SqlParameter("@adminUserId", adminUserId),
                                                                    new SqlParameter("@customerNo", customerNo),
                                                                    new SqlParameter("@billDate", billDate),
                                                                    new SqlParameter("@billNumber", billNumber),
                                                                    new SqlParameter("@insertionDate", insertionDate),
                                                                    new SqlParameter("@returnCode", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@serviceResult", serviceResult)
                                                                 };

                sqlParameters[0].Direction = ParameterDirection.Input;
                sqlParameters[1].Direction = ParameterDirection.Input;
                sqlParameters[2].Direction = ParameterDirection.Input;
                sqlParameters[3].Direction = ParameterDirection.Input;
                sqlParameters[4].Direction = ParameterDirection.Input;
                sqlParameters[5].Direction = ParameterDirection.Input;
                sqlParameters[6].Direction = ParameterDirection.Input;
                sqlParameters[7].Direction = ParameterDirection.Output;
                sqlParameters[8].Direction = ParameterDirection.Input;

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.update_transaction_cancel_logs", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                returnCode = sqlParameters[7].Value.ToString();

                //transactions.Add(new CancelTransaction(itemId, aboneNo, "", makbuzTarihi, makbuzNo));
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchCustomerAccountExcel");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return returnCode;
        }

        public CustomerAccountCollection ListDailyCustomerAccountsOrderForExcel(Int64 itemId,int isActive,int orderSelectionColumn,int descAsc)
        {
            CustomerAccountCollection transactions = new CustomerAccountCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@transactionId", itemId),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                 };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pp.get_daily_customer_account_report_detail_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetCustomerAccount(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchCustomerAccountExcel");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }


        private AccountTransaction GetDailyAccountTransaction(DataRow dataRow, DataColumnCollection dataColumns)
        {

            return new AccountTransaction(Convert.ToInt32(dataRow["Id"])
                                     , dataRow["TransactionDate"].ToString()
                                     , dataRow["Description"].ToString()
                                     , dataRow["TransactionType"].ToString()
                                     , dataRow["Amount"].ToString()
                                     , dataRow["SenderNo"].ToString()
                                     , dataRow["SenderName"].ToString()
                                     , dataRow["SenderBalance"].ToString()
                                     , dataRow["ReceiverNo"].ToString()
                                     , dataRow["ReceiverName"].ToString()
                                     , dataRow["ReceiverBalance"].ToString()
                                     , dataRow["TransactionPrice"].ToString()
                                     , dataRow["TransactionPlatform"].ToString());

        }

        private AccountTransactionProkis GetDailyAccountTransactionProkis(DataRow dataRow, DataColumnCollection dataColumns)
        {

            return new AccountTransactionProkis(Convert.ToInt32(dataRow["Id"])
                                     , dataRow["TransactionDate"].ToString()
                                     , dataRow["ReceivedAmount"].ToString()
                                     , dataRow["CustomerName"].ToString()
                                     , dataRow["AboneNo"].ToString());

        }

        public Transaction GetTransaction(int transactionId)
        {
            Transaction transaction = null;
            DataSet dataSet = null;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@transactionId", transactionId) };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_transaction", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                if (dataSet.Tables[0].Rows.Count == 1)
                {
                    transaction = this.GetTransaction(dataSet.Tables[0].Rows[0], dataSet.Tables[0].Columns);
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetTransaction");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return transaction;
        }

         public TransactionCollection SearchTransactionsOrder(string searchText,
                                              DateTime startDate,
                                              DateTime endDate,
                                              int numberOfItemsPerPage,
                                              int currentPageNum,
                                              ref int recordCount,
                                              ref int pageCount,
                                              int orderSelectionColumn,
                                              int descAsc,
                                              string kioskId,
                                              int instutionId,
                                              int operationTypeId,
                                              string txnStatusId, 
                                              int orderType)
        {
            TransactionCollection transactions = new TransactionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@instutionId", instutionId),
                                                                    new SqlParameter("@operationTypeId", operationTypeId),
                                                                    new SqlParameter("@txnStatusId", txnStatusId),
                                                                    new SqlParameter("@orderType", orderType)
                                                                 };
                sqlParameters[3].Value = Convert.ToDateTime(startDate);
                sqlParameters[4].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_transactions_order_temp", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetTransaction(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchTransactionsOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }

         public TransactionDetailCollectionGroup SearchTransactionsDetailsOrderDet(long transactionId)
         {
             TransactionDetailCollectionGroup transactions = new TransactionDetailCollectionGroup();
             DataSet dataSet = null;
             try
             {
                 SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@transactionId", transactionId) };
                 dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_transactions_details_with_pages", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);


                 for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                 {
                     transactions.Add(this.GetTransactionDetails(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                 }
             }
             catch (Exception exp)
             {
                 Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchTransactionsOrder");
             }
             finally
             {
                 base.Dispose();

                 if (dataSet != null)
                 {
                     dataSet.Dispose();
                     dataSet = null;
                 }
             }
             return transactions;
         }

         public KioskLocalLogCollection SearchKioskLocalLogOrder(string searchText,
                                                                  DateTime startDate,
                                                                  DateTime endDate,
                                                                  int logStatus,
                                                                  int numberOfItemsPerPage,
                                                                  int currentPageNum,
                                                                  ref int recordCount,
                                                                  ref int pageCount,
                                                                  int orderSelectionColumn,
                                                                  int descAsc,
                                                                   // int institutionId,
                                                                  string kioskId,
                                                                  
                                                                  int orderType)
         {
             KioskLocalLogCollection items = new KioskLocalLogCollection();
             DataSet dataSet = null;
             try
             {
                 SqlParameter[] sqlParameters = new SqlParameter[] {
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@searchText", searchText),  
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    //new SqlParameter("@institutionId", institutionId),
                                                                    new SqlParameter("@searchKioskId", kioskId),
                                                                    new SqlParameter("@orderType", orderType),
                                                                    new SqlParameter("@logStatus", logStatus)
                                                                    
                                                                 };


                 sqlParameters[0].Value = Convert.ToDateTime(startDate);
                 sqlParameters[1].Value = Convert.ToDateTime(endDate);
                 sqlParameters[3].Direction = sqlParameters[4].Direction = ParameterDirection.InputOutput;


                 dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_kiosk_local_log", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                 recordCount = Convert.ToInt32(sqlParameters[4].Value);
                 pageCount = Convert.ToInt32(sqlParameters[3].Value);


                 for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                 {
                     items.Add(this.GetListKioskLocalLog(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                 }
             }


             catch (Exception exp)
             {
                 Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREKioskLocalLogOrder");
             }
             finally
             {
                 base.Dispose();

                 if (dataSet != null)
                 {
                     dataSet.Dispose();
                     dataSet = null;
                 }
             }
             return items;
         }


         public KioskLocalLogDetailCollection SearchKioskLocalLogDetailOrder(string localLogDetail)
         {
             KioskLocalLogDetailCollection items = new KioskLocalLogDetailCollection();
             DataSet dataSet = null;
             try
             {
                 SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@localLogId", Convert.ToInt32(localLogDetail)) };


                 dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_kiosk_local_log_details", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);


                 for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                 {
                     items.Add(this.GetListKioskLocalLogDetail(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                 }
             }


             catch (Exception exp)
             {
                 Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREKioskLocalLogOrder");
             }
             finally
             {
                 base.Dispose();

                 if (dataSet != null)
                 {
                     dataSet.Dispose();
                     dataSet = null;
                 }
             }
             return items;
         }
         private KioskLocalLog GetListKioskLocalLog(DataRow dataRow, DataColumnCollection dataColumns)
         {
             return new KioskLocalLog(
             dataRow["TransactionId"].ToString()
             , dataRow["KioskName"].ToString()
                 //, dataRow["InstitutionName"].ToString()
             , dataRow["InsertionDate"].ToString()
             , dataRow["Name"].ToString()
             , dataRow["CustomerNo"].ToString()
             , dataRow["UserName"].ToString()
             , dataRow["LogStatus"].ToString()
             , dataRow["Status"].ToString()
             , dataRow["Id"].ToString()
             , dataRow["LogStatusText"].ToString()
             
             );
         }

         private KioskLocalLogDetail GetListKioskLocalLogDetail(DataRow dataRow, DataColumnCollection dataColumns)
         {
             return new KioskLocalLogDetail(
               dataRow["Id"].ToString()
             , dataRow["InsertionDate"].ToString()
             , dataRow["RecordInsertionDate"].ToString()
             , dataRow["Detail"].ToString()
             , dataRow["TotalAmount"].ToString()
             , dataRow["Status"].ToString()
             );
         }


         //SaveKioskLocalLogOrder
         public int SaveKioskLocalLogOrder(int transactionId, Int64 adminUserId)
         {
             int success = 0;

             try
             {
                 SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@result",success),
                                                                     new SqlParameter("@adminUserId", adminUserId),
                                                                     new SqlParameter("@transactionId", transactionId)
                                                                     
                };

                 sqlParameters[0].Direction = ParameterDirection.Output;

                 int a = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[add_kiosk_local_log]", sqlParameters);

                 success = Convert.ToInt32(sqlParameters[0].Value);

             }

             catch (Exception exp)
             {
                 Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveKioskLocalLog");

             }
             finally
             {
                 base.Dispose();
             }
             return success;

         }


         public GetCustomerContactsCollection SearchCustomerContactsOrder(string searchText,
                              DateTime startDate,
                              DateTime endDate,
                              int numberOfItemsPerPage,
                              int currentPageNum,
                              int recordCount,
                              int pageCount,
                              int orderSelectionColumn,
                              int descAsc,
                              int instutionId,
                              long transactionId,
                              int orderType
                              )
         {
             GetCustomerContactsCollection transactions = new GetCustomerContactsCollection();
             DataSet dataSet = null;
             try
             {
                 SqlParameter[] sqlParameters = new SqlParameter[] {new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@institutionId", instutionId),
                                                                    new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@transactionId", transactionId), 
                                                                    new SqlParameter("@orderType", orderType),
                                                                 };
                 sqlParameters[0].Value = Convert.ToDateTime(startDate);
                 sqlParameters[1].Value = Convert.ToDateTime(endDate);
                 sqlParameters[8].Direction = sqlParameters[9].Direction = ParameterDirection.InputOutput;

                 dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[search_customer_contacts_order]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                 recordCount = Convert.ToInt32(sqlParameters[8].Value);
                 pageCount = Convert.ToInt32(sqlParameters[9].Value);


                 for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                 {   //creditcartcustomer tablosu
                     transactions.Add(this.GetCustomerContacts(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                 }

             }
             catch (Exception exp)
             {
                 Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchCreditCardTransactionsOrder");
             }
             finally
             {
                 base.Dispose();

                 if (dataSet != null)
                 {
                     dataSet.Dispose();
                     dataSet = null;
                 }
             }
             return transactions;
         }


         //SearchCreditCardTransactionsOrder
         public CreditCardTransactionCollection SearchCreditCardTransactionsOrder(string searchText,
                                      DateTime startDate,
                                      DateTime endDate,
                                      int numberOfItemsPerPage,
                                      int currentPageNum,
                                      ref int recordCount,
                                      ref int pageCount,
                                      int orderSelectionColumn,
                                      int descAsc,
                                      string kioskId,
                                      int instutionId,
                                      int muhasebeStatusId,
                                      int orderType,
                                      ref string totalTransactionAmount
                                      )
         {
             CreditCardTransactionCollection transactions = new CreditCardTransactionCollection();
             DataSet dataSet = null;
             try
             {
                 SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@instutionId", instutionId),
                                                                    new SqlParameter("@orderType", orderType),
                                                                    new SqlParameter("@totalTransactionAmount", SqlDbType.VarChar,200),
                                                                    new SqlParameter("@muhasebeStatusId", muhasebeStatusId), 
                                                                 };
                 sqlParameters[3].Value = Convert.ToDateTime(startDate);
                 sqlParameters[4].Value = Convert.ToDateTime(endDate);
                 sqlParameters[0].Direction = sqlParameters[1].Direction = sqlParameters[12].Direction = ParameterDirection.InputOutput;
                 
                 dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[search_creditcard_transactions_order]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                 recordCount = Convert.ToInt32(sqlParameters[0].Value);
                 pageCount = Convert.ToInt32(sqlParameters[1].Value);

                 totalTransactionAmount = sqlParameters[12].Value.ToString();

                 for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                 {   //creditcartcustomer tablosu
                     transactions.Add(this.GetCreditCardTransaction(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                 }

                 transactions.Add(new CreditCardTransaction(0, "", "", "", "", "", "", "", totalTransactionAmount, "", "", "", "info"));
             }
             catch (Exception exp)
             {
                 Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchCreditCardTransactionsOrder");
             }
             finally
             {
                 base.Dispose();

                 if (dataSet != null)
                 {
                     dataSet.Dispose();
                     dataSet = null;
                 }
             }
             return transactions;
         }


         //SearchMobilePaymentTransactionsOrder
         public MobilePaymentTransactionCollection SearchMobilePaymentTransactionsOrder(string searchText,
                                      DateTime startDate,
                                      DateTime endDate,
                                      int numberOfItemsPerPage,
                                      int currentPageNum,
                                      ref int recordCount,
                                      ref int pageCount,
                                      int orderSelectionColumn,
                                      int descAsc,
                                      string kioskId,
                                      int instutionId,
                                      int muhasebeStatusId,
                                      int orderType,
                                      ref string totalTransactionAmount
                                      )
         {
             MobilePaymentTransactionCollection transactions = new MobilePaymentTransactionCollection();
             DataSet dataSet = null;
             try
             {
                 SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@institutionId", instutionId),
                                                                    new SqlParameter("@orderType", orderType),
                                                                    new SqlParameter("@totalTransactionAmount", SqlDbType.VarChar,200),
                                                                    new SqlParameter("@muhasebeStatusId",muhasebeStatusId),  
                                                                 };
                 sqlParameters[3].Value = Convert.ToDateTime(startDate);
                 sqlParameters[4].Value = Convert.ToDateTime(endDate);
                 sqlParameters[0].Direction = sqlParameters[1].Direction = sqlParameters[12].Direction = ParameterDirection.InputOutput;

                 dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[search_mobile_payment_transactions_order]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                 recordCount = Convert.ToInt32(sqlParameters[0].Value);
                 pageCount = Convert.ToInt32(sqlParameters[1].Value);

                 totalTransactionAmount = sqlParameters[12].Value.ToString();

                 for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                 {   //creditcartcustomer tablosu
                     transactions.Add(this.GetMobilePaymentTransaction(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                 }

                 transactions[transactions.Count - 1].ClassName = "info";

             }
             catch (Exception exp)
             {
                 Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchCreditCardTransactionsOrder");
             }
             finally
             {
                 base.Dispose();

                 if (dataSet != null)
                 {
                     dataSet.Dispose();
                     dataSet = null;
                 }
             }
             return transactions;
         }

         // SearchRepairCardOrder
         public RepairCardCollection SearchRepairCardOrder(string searchText,
                              DateTime startDate,
                              DateTime endDate,
                              int numberOfItemsPerPage,
                              int currentPageNum,
                              ref int recordCount,
                              ref int pageCount,
                              int orderSelectionColumn,
                              int descAsc,
                              string kioskId,
                              int instutionId,
                              int cardRepairStatus,
                              int isCardDeleted,
                              int orderType)
         {
             RepairCardCollection transactions = new RepairCardCollection();
             DataSet dataSet = null;
             try
             {
                 SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@orderType", orderType),
                                                                    new SqlParameter("@instutionId", instutionId),
                                                                    new SqlParameter("@cardRepairStatus", cardRepairStatus),
                                                                    new SqlParameter("@isCardDeleted", isCardDeleted)
                                                                 };

                 sqlParameters[3].Value = Convert.ToDateTime(startDate);
                 sqlParameters[4].Value = Convert.ToDateTime(endDate);
                 sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                 dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[search_card_repair_order]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                 recordCount = Convert.ToInt32(sqlParameters[0].Value);
                 pageCount = Convert.ToInt32(sqlParameters[1].Value);

                 for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                 {   //creditcartcustomer tablosı
                     transactions.Add(this.GetRepairCard(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                 }
             }
             catch (Exception exp)
             {
                 Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchCreditCardTransactionsOrder");
             }
             finally
             {
                 base.Dispose();

                 if (dataSet != null)
                 {
                     dataSet.Dispose();
                     dataSet = null;
                 }
             }
             return transactions;
         }


         //GetCutOffMonitoringOrder
         public RepairCard GetRepairedCardOrder(int repairId)
         {
             RepairCard items = null;
             DataSet dataSet = null;
             try
             {

                 SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@repairId", repairId) };

                 dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_card_repair_order", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                 if (dataSet.Tables[0].Rows.Count == 1)
                 {

                     items = this.GetRepairCard(dataSet.Tables[0].Rows[0], dataSet.Tables[0].Columns);
                 }

             }
             catch (Exception exp)
             {
                 Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREKioskRepairedCardOrderOrder");

             }
             finally
             {
                 base.Dispose();

                 if (dataSet != null)
                 {
                     dataSet.Dispose();
                     dataSet = null;
                 }
             }

             return items;
         }



         //AddCardRepairOrder
         public int AddCardRepairOrder(int transactionId, string anaKredi, string yedekKredi, int adminUserId)
         {
             int updateStatus = 0;
             try
             {

                 SqlParameter[] sqlParameters = new SqlParameter[]{ new SqlParameter("@transactionId",transactionId),
                                                                    new SqlParameter("@anaKredi",anaKredi),
                                                                    new SqlParameter("@yedekKredi",yedekKredi),
                                                                    new SqlParameter("@insertionDate", DateTime.Now),
                                                                    new SqlParameter("@userId",adminUserId),
                                                                    new SqlParameter("@returnCode",0)
                                                                 
                };

                 sqlParameters[5].Direction = ParameterDirection.InputOutput;
                 updateStatus = Convert.ToInt32(sqlParameters[5].Value);

                 SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "pk.add_card_repair", sqlParameters);

             }
             catch (Exception exp)
             {
                 Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREAddCardRepairOrder");
                 updateStatus = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
             }
             finally
             {
                 base.Dispose();
             }
             return updateStatus;
         }


         public int UpdateRepairedCardOrder(int repairId,
                                     string anaKredi,
                                     string yedekKredi,   
                                     int expertUserId)
         {
             int resp = 0;
             try
             {

                 SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@repairId", repairId),       
                                                                    new SqlParameter("@anaKredi", anaKredi),
                                                                    new SqlParameter("@yedekKredi", yedekKredi),
                                                                    new SqlParameter("@returnCode", 0),
                                                                    new SqlParameter("@expertUserId", expertUserId)

                                                                
                                                                    };

                 sqlParameters[3].Direction = ParameterDirection.InputOutput;
                 resp = Convert.ToInt32(sqlParameters[2].Value);
               
                 SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[update_card_repair_order]", sqlParameters);
             }
             catch (Exception exp)
             {
                 Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREUpdateErrorLogs");
                 resp = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
             }
             finally
             {
                 base.Dispose();
             }

             return resp;
         }


         //MuhasebeStatusCollection
         public MuhasebeStatusCollection GetMuhasebeStatusOrder()
         {
             MuhasebeStatusCollection names = new MuhasebeStatusCollection();
             DataSet dataSet = null;
             try
             {
                 dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_muhasebe_status", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                 for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                 {
                     names.Add(this.GetMuhasebeStatus(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                 }
             }
             catch (Exception exp)
             {
                 Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREMuhasebeStatus");

             }
             finally
             {
                 base.Dispose();

                 if (dataSet != null)
                 {
                     dataSet.Dispose();
                     dataSet = null;
                 }
             }
             return names;
         }

         //GetMuhasebeStatus
         private MuhasebeStatus GetMuhasebeStatus(DataRow dataRow, DataColumnCollection dataColumns)
         {
             return new MuhasebeStatus(Convert.ToInt32(dataRow["Id"])
             , dataRow["Name"].ToString());
         }


         public FailedTransactionCollection SearchFailedTransactionsOrder(string searchText,
                                      DateTime startDate,
                                      DateTime endDate,
                                      int numberOfItemsPerPage,
                                      int currentPageNum,
                                      ref int recordCount,
                                      ref int pageCount,
                                      int orderSelectionColumn,
                                      int descAsc,
                                      string kioskId,
                                      int instutionId,
                                      int operationTypeId,
                                      string txnStatusId,
                                      int orderType)
         {
             FailedTransactionCollection failedtransactions = new FailedTransactionCollection();
             DataSet dataSet = null;
             try
             {
                 SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@instutionId", instutionId),
                                                                    new SqlParameter("@operationTypeId", operationTypeId),
                                                                    new SqlParameter("@txnStatusId", txnStatusId),
                                                                    new SqlParameter("@orderType", orderType)
                                                                 };
                 sqlParameters[3].Value = Convert.ToDateTime(startDate);
                 sqlParameters[4].Value = Convert.ToDateTime(endDate);
                 sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                 dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_failed_transactions_order_temp", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                 recordCount = Convert.ToInt32(sqlParameters[0].Value);
                 pageCount = Convert.ToInt32(sqlParameters[1].Value);

                 for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                 {
                     failedtransactions.Add(this.GetFailedTransaction(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                 }
             }
             catch (Exception exp)
             {
                 Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchFailedTransactionsOrder");
             }
             finally
             {
                 base.Dispose();

                 if (dataSet != null)
                 {
                     dataSet.Dispose();
                     dataSet = null;
                 }
             }
             return failedtransactions;
         }

        public PurseTransactionCollection SearchPurseTransactionsOrder(string searchText,
                                             DateTime startDate,
                                             DateTime endDate,
                                             int numberOfItemsPerPage,
                                             int currentPageNum,
                                             ref int recordCount,
                                             ref int pageCount,
                                             int orderSelectionColumn,
                                             int descAsc)
        {
            PurseTransactionCollection transactions = new PurseTransactionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc)
                                                                 };
                sqlParameters[3].Value = Convert.ToDateTime(startDate);
                sqlParameters[4].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_purse_transactions_order", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetPurseTransaction(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchTransactionsOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }

        public AccountTransactionCollection SearchAccountTransactionsOrder(string searchText,
                                            DateTime startDate,
                                            DateTime endDate,
                                            int numberOfItemsPerPage,
                                            int currentPageNum,
                                            ref int recordCount,
                                            ref int pageCount,
                                            int orderSelectionColumn,
                                            int descAsc,
                                            ref string TotalTransferAmount,
                                            ref string TotalEarnedAmount,
                                            string platform,
                                            int whichAction)
        {
            AccountTransactionCollection transactions = new AccountTransactionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@TotalTransferAmount", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@TotalEarnedAmount", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@platform", platform),
                                                                    new SqlParameter("@whichAction", whichAction)
                                                                 };
                sqlParameters[3].Value = Convert.ToDateTime(startDate);
                sqlParameters[4].Value = Convert.ToDateTime(endDate);
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                sqlParameters[9].Direction = sqlParameters[10].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_account_transactions_order", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);
                TotalEarnedAmount = sqlParameters[10].Value.ToString();
                TotalTransferAmount = sqlParameters[9].Value.ToString();

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetAccountTransaction(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }

                transactions.Add(new AccountTransaction(0, "", "", "", TotalTransferAmount, "", "", "", "", "", 0, TotalEarnedAmount, 0, "", "", "", "info"));

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchTransactionsOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }


        public AccountTransactionCollection ListDailyAccountTransactionsOrder(Int64 itemId,
                                    int numberOfItemsPerPage,
                                    int currentPageNum,
                                    ref int recordCount,
                                    ref int pageCount,
                                    int orderSelectionColumn,
                                    int descAsc,
                                    ref string TotalTransferAmount,
                                    ref string TotalEarnedAmount)
        {
            AccountTransactionCollection transactions = new AccountTransactionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@itemId", itemId),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@TotalTransferAmount", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@TotalEarnedAmount", SqlDbType.VarChar,20)
                                                                 };
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                sqlParameters[7].Direction = sqlParameters[8].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_daily_account_report_detail", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);
                TotalEarnedAmount = sqlParameters[8].Value.ToString();
                TotalTransferAmount = sqlParameters[7].Value.ToString();

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetDailyAccountTransaction(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchTransactionsOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }

        public AccountTransactionProkisCollection ListDailyAccountTransactionsProkisOrder(Int64 itemId,
                                   int numberOfItemsPerPage,
                                   int currentPageNum,
                                   ref int recordCount,
                                   ref int pageCount,
                                   int orderSelectionColumn,
                                   int descAsc,
                                   ref string TotalTransferAmount,
                                   ref string TotalEarnedAmount)
        {
            AccountTransactionProkisCollection transactions = new AccountTransactionProkisCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@itemId", itemId),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@TotalTransferAmount", SqlDbType.VarChar,20),
                                                                    new SqlParameter("@TotalEarnedAmount", SqlDbType.VarChar,20)
                                                                 };
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                sqlParameters[7].Direction = sqlParameters[8].Direction = ParameterDirection.Output;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_daily_account_report_detail_prokis", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);
                TotalEarnedAmount = sqlParameters[8].Value.ToString();
                TotalTransferAmount = sqlParameters[7].Value.ToString();

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetDailyAccountTransactionProkis(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchTransactionsOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }

        public TransactionCollection SearchTransactionsOrderForExcel(string searchText,
                                             DateTime startDate,
                                             DateTime endDate,
                                             int orderSelectionColumn,
                                             int descAsc,
                                             string kioskId,
                                             int instutionId,
                                             int operationTypeId,
                                             string txnStatusId)
        {
            TransactionCollection transactions = new TransactionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@instutionId", instutionId),
                                                                    new SqlParameter("@operationTypeId", operationTypeId),
                                                                    new SqlParameter("@txnStatusId", txnStatusId)
                                                                 };
                sqlParameters[1].Value = Convert.ToDateTime(startDate);
                sqlParameters[2].Value = Convert.ToDateTime(endDate);
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_transactions_order_for_excel_temp", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetTransaction(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchTransactionsOrderForExcel");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }

        public PurseTransactionCollection SearchPurseTransactionsOrderForExcel(string searchText,
                                            DateTime startDate,
                                            DateTime endDate,
                                            int orderSelectionColumn,
                                            int descAsc)
        {
            PurseTransactionCollection transactions = new PurseTransactionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc)
                                                                 };
                sqlParameters[1].Value = Convert.ToDateTime(startDate);
                sqlParameters[2].Value = Convert.ToDateTime(endDate);
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_purse_transactions_order_for_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetPurseTransaction(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchPurseTransactionsOrderForExcel");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }

        public AccountTransactionCollection SearchAccountTransactionsOrderForExcel(string searchText,
                                    DateTime startDate,
                                    DateTime endDate,
                                    int orderSelectionColumn,
                                    int descAsc,
                                    string platform,
                                    int whichAction)
        {
            AccountTransactionCollection transactions = new AccountTransactionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@startDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@endDate", SqlDbType.DateTime),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@platform", platform),
                                                                    new SqlParameter("@whichAction", whichAction)
                                                                 };
                sqlParameters[1].Value = Convert.ToDateTime(startDate);
                sqlParameters[2].Value = Convert.ToDateTime(endDate);
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_account_transactions_order_for_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetAccountTransaction(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchAccountTransactionsOrderForExcel");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }

        public AccountTransactionCollection ListDailyAccountTransactionsOrderForExcel(Int64 itemId,
                                   int orderSelectionColumn,
                                   int descAsc)
        {
            AccountTransactionCollection transactions = new AccountTransactionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@itemId", itemId),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                 };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_daily_account_report_detail_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetDailyAccountTransaction(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchAccountTransactionsOrderForExcel");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }

        public AccountTransactionProkisCollection ListDailyAccountTransactionsProkisOrderForExcel(Int64 itemId,
                                  int orderSelectionColumn,
                                  int descAsc)
        {
            AccountTransactionProkisCollection transactions = new AccountTransactionProkisCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@itemId", itemId),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                 };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_daily_account_report_detail_excel_prokis", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetDailyAccountTransactionProkis(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchAccountTransactionsOrderForExcel");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }

        public TransactionCollection SearchCustomerTransactionsOrder(string searchText,
                                      int numberOfItemsPerPage,
                                      int currentPageNum,
                                      ref int recordCount,
                                      ref int pageCount,
                                      int orderSelectionColumn,
                                      int descAsc,
                                      int customerId,
                                      string kioskId,
                                      int instutionId,
                                      int operationTypeId,
                                      string txnStatusId)
        {
            TransactionCollection transactions = new TransactionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { 
                                                                    
                                                                    new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@customerId", customerId),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@instutionId", instutionId),
                                                                    new SqlParameter("@operationTypeId", operationTypeId),
                                                                    new SqlParameter("@txnStatusId", txnStatusId)
                                                                 };
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_customer_transactions_order_temp", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);
                //dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.deneme2", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetTransaction(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchCustomerTransactionsOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }

        public TransactionCollection SearchCustomerPurseTransactionsOrder(string searchText,
                                      int numberOfItemsPerPage,
                                      int currentPageNum,
                                      ref int recordCount,
                                      ref int pageCount,
                                      int orderSelectionColumn,
                                      int descAsc,
                                      int customerId)
        {
            TransactionCollection transactions = new TransactionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),  
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@customerId", customerId)
                                                                 };
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_customer_purse_transactions_order", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetTransaction(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchCustomerTransactionsOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }

        public TransactionCollection SearchCustomerTransactionsOrderForExcel(string searchText,
                                     int orderSelectionColumn,
                                     int descAsc,
                                     int customerId,
                                     string kioskId,
                                     int instutionId,
                                     int operationTypeId)
        {
            TransactionCollection transactions = new TransactionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@searchText", searchText),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@customerId", customerId),
                                                                    new SqlParameter("@kioskId", kioskId),
                                                                    new SqlParameter("@instutionId", instutionId),
                                                                    new SqlParameter("@operationTypeId", operationTypeId)
                                                                 };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_customer_transactions_order_for_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetTransaction(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchCustomerTransactionsOrderForExcel");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }

        public TransactionCollection SearchCustomerPurseTransactionsOrderForExcel(string searchText,
                                    int orderSelectionColumn,
                                    int descAsc,
                                    int customerId)
        {
            TransactionCollection transactions = new TransactionCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@searchText", searchText),  
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@customerId", customerId)};

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_customer_purse_transactions_order_for_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    transactions.Add(this.GetTransaction(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchCustomerPurseTransactionsOrderForExcel");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return transactions;
        }

        public void DeleteOperation(int userId, string storedProcedure, int itemId, ref int returnCode)
        {
            try
            {
                if (storedProcedure == "[pk].[approve_money_code]")
                {
                    string moneyCode = "";
                    int moneyCodeStatus = 0;
                    string customerName = "";
                    DataSet dataSet = null;
                    string cellPhone = "",codeAmount="";
                    SqlParameter[] sqlParameters1 = new SqlParameter[] { new SqlParameter("@itemId", itemId), 
                                                                         new SqlParameter("@userId", userId),  
                                                                         new SqlParameter("@returnCode", returnCode),
                                                                         new SqlParameter("@moneyCode", SqlDbType.VarChar,200),
                                                                         new SqlParameter("@customerName", SqlDbType.VarChar,200),
                                                                         new SqlParameter("@moneyCodeStatus", moneyCodeStatus),
                                                                         new SqlParameter("@cellPhone", SqlDbType.VarChar,200),
                                                                         new SqlParameter("@codeAmount", SqlDbType.VarChar,200)
                    };

                    sqlParameters1[2].Direction = ParameterDirection.InputOutput;
                    sqlParameters1[3].Direction = sqlParameters1[4].Direction = sqlParameters1[5].Direction = sqlParameters1[6].Direction = ParameterDirection.Output;
                    sqlParameters1[7].Direction = ParameterDirection.Output;
                    dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.approve_money_code", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters1);

                    moneyCode = sqlParameters1[3].Value.ToString();
                    returnCode = Convert.ToInt32(sqlParameters1[2].Value);
                    customerName = sqlParameters1[4].Value.ToString();
                    cellPhone = sqlParameters1[6].Value.ToString();
                    moneyCodeStatus = Convert.ToInt32(sqlParameters1[5].Value);
                    codeAmount = sqlParameters1[7].Value.ToString();

                    if (moneyCodeStatus == 1 )
                    {
                        string webServiceUrl = Utilities.Utility.GetConfigValue("SMSWebServiceUrl");
                        string mesaj = Utilities.Utility.GetConfigValue("ParaKodMesaj");
                        mesaj = mesaj.Replace("XXX", customerName);
                        mesaj = mesaj.Replace("YYY", moneyCode);
                        mesaj = mesaj.Replace("ZZZ", codeAmount);
                        Utilities.Utility.SendSMS(webServiceUrl, mesaj, cellPhone);
                        //Utilities.Utility.SendSMS(webServiceUrl, "Sn. " + customerName + ", Pratik Noktada kullanabileceğiniz para kodunuz :" + moneyCode + " \r\nİyi Günler Dileriz.", cellPhone);
                    }


                }
                else
                {
                    SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@returnCode", returnCode),
                                                                    new SqlParameter("@userId", userId),
                                                                    new SqlParameter("@itemId", itemId)};
                    sqlParameters[0].Direction = ParameterDirection.Output;
                    SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, storedProcedure, sqlParameters);
                    returnCode = Convert.ToInt32(sqlParameters[0].Value);
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREDeleteOperation");

            }
            finally
            {
                base.Dispose();

            }
        }

        public void OperationInform(int userId, string storedProcedure, int itemId, ref int returnCode, int type)
        {
            try
            {
                string senderName, recieverName, senderTelephone, recieverTelephone, amountTxn = "";

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@returnCode", returnCode),
                                                                    new SqlParameter("@userId", userId),
                                                                    new SqlParameter("@itemId", itemId),
                                                                    new SqlParameter("@senderName",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@receiverName",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@senderTelephone",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@receiverTelephone",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@amountTxn",SqlDbType.VarChar,200)
                };

                sqlParameters[0].Direction = ParameterDirection.Output;
                sqlParameters[3].Direction = ParameterDirection.Output;
                sqlParameters[4].Direction = ParameterDirection.Output;
                sqlParameters[5].Direction = ParameterDirection.Output;
                sqlParameters[6].Direction = ParameterDirection.Output;
                sqlParameters[7].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, storedProcedure, sqlParameters);
                returnCode = Convert.ToInt32(sqlParameters[0].Value);
                senderName = sqlParameters[3].Value.ToString();
                recieverName = sqlParameters[4].Value.ToString();
                senderTelephone = sqlParameters[5].Value.ToString();
                recieverTelephone = sqlParameters[6].Value.ToString();
                amountTxn = sqlParameters[7].Value.ToString();

                if (returnCode == 1)
                {
                    string webServiceUrl = Utility.GetConfigValue("SMSWebServiceUrl");
                    if (type == 1)
                    {
                        Utility.SendSMS(webServiceUrl, "Sn. " + senderName + ", hesabınızdan " + recieverName + " adına " + amountTxn + " TL transfer edilmiştir. \r\nİyi Günler Dileriz.", senderTelephone);
                        Utility.SendSMS(webServiceUrl, "Sn. " + recieverName + ", hesabınıza " + senderName + " tarafından " + amountTxn + " TL gönderilmiştir. \r\nİyi Günler Dileriz.", recieverTelephone);
                    }
                    else if (type == 0)
                    {
                        Utility.SendSMS(webServiceUrl, "Sn. " + senderName + ",  " + recieverName + " adına " + amountTxn + " TL tutarındaki transfer isteğiniz iptal edilmiştir. \r\nİyi Günler Dileriz.", senderTelephone);
                    }

                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREDeleteOperation");

            }
            finally
            {
                base.Dispose();

            }
        }

        public void DeleteMoneyCode(int userId, string detailText, string vitelTicketNo, int itemId, ref int returnCode)
        {
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@returnCode", returnCode),
                                                                    new SqlParameter("@userId", userId),
                                                                    new SqlParameter("@itemId", itemId),
                                                                    new SqlParameter("@vitelTicketNo", vitelTicketNo),
                                                                    new SqlParameter("@detailText",detailText),

                };

                sqlParameters[0].Direction = ParameterDirection.Output;

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[delete_money_code]", sqlParameters);
                returnCode = Convert.ToInt32(sqlParameters[0].Value);

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREDeleteMoneyCoden");

            }
            finally
            {
                base.Dispose();

            }
        }
    }
}