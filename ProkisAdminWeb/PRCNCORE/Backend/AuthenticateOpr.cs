﻿using Microsoft.ApplicationBlocks.Data;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Backend
{
    public class AuthenticateOpr : DBOperator
    {
        public AuthenticateOpr()
        {

        }

        internal AuthenticateOpr(SqlConnection sqlConnection)
            : base(sqlConnection)
        {

        }

        public bool ControlAccessToken(string accessToken, Int64 adminUserId)
        {
            bool validApiKeyApiPass = false;
            int returnCode = 0;

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]{
                                                                new SqlParameter("@AccessToken",accessToken),
                                                                new SqlParameter("@timing",DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")),
                                                                new SqlParameter("@returnCode",returnCode),
                                                                new SqlParameter("@adminUserId",adminUserId)
                };

                sqlParameters[2].Direction = ParameterDirection.InputOutput;

                SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.ControlAccessToken",  sqlParameters);

                if (Convert.ToInt32(sqlParameters[2].Value) == 1)
                    validApiKeyApiPass = true;

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREControlAccessToken");
            }
            finally
            {
                base.Dispose();
            }

            return validApiKeyApiPass;
        }


        public bool ControlExpiredAccessToken(string accessToken, Int64 adminUserId)
        {
            bool validApiKeyApiPass = false;
            int returnCode = 0;

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]{
                                                                new SqlParameter("@AccessToken",accessToken),
                                                                new SqlParameter("@timing",DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss")),
                                                                new SqlParameter("@returnCode",returnCode),
                                                                new SqlParameter("@adminUserId",adminUserId)
                };

                sqlParameters[2].Direction = ParameterDirection.InputOutput;

                SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.ControlExpiredAccessToken", sqlParameters);

                if (Convert.ToInt32(sqlParameters[2].Value) == 1)
                    validApiKeyApiPass = true;

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREControlExpiredAccessToken");
            }
            finally
            {
                base.Dispose();
            }

            return validApiKeyApiPass;
        }

       public bool ControlApiKeyApiPass(string apiKey, string apiPass)
        {
            bool validApiKeyApiPass = false;

            DataSet dataSet = null;

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]{
                                                                new SqlParameter("@apiKey",apiKey),
                                                                new SqlParameter("@apiPass",apiPass)  
                                                              };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[dbo].[SP_CONTROL_APIKEY_APIPASS]", sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    validApiKeyApiPass = Convert.ToBoolean(dataRow["cnt"]);
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREControlApiKeyApiPass");

            }
            finally
            {
                base.Dispose();
                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return validApiKeyApiPass;   
        }
    }
}
