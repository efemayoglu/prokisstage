﻿using Microsoft.ApplicationBlocks.Data;
using PRCNCORE.Customers;
using PRCNCORE.Input;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace PRCNCORE.Backend
{
    public class CustomerOpr : DBOperator
    {
        public CustomerOpr()
        {

        }

        internal CustomerOpr(SqlConnection sqlConnection)
            : base(sqlConnection)
        {

        }

        private PRCNCORE.Customers.Customer GetCustomer(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new PRCNCORE.Customers.Customer(Convert.ToInt32(dataRow["CustomerId"])
            , dataRow["CustomerName"].ToString()
            , dataRow["AboneNo"].ToString()
            , dataRow["InsertionDate"].ToString()
            , dataRow["LastTransactionDate"].ToString()
            , dataRow["TransactionCount"].ToString()
            , dataRow["InstutionName"].ToString()
            , Convert.ToInt32(dataRow["InstutionId"])
            );
        }

        private WebCustomer GetWebCustomer(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new WebCustomer(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString()
            , dataRow["CustomerNo"].ToString()
            , dataRow["Tckn"].ToString()
            , dataRow["Email"].ToString()
            , dataRow["CellPhone"].ToString()
            , dataRow["Status"].ToString());
        }

        private WebCustomer GetApproveBLWebCustomer(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new WebCustomer(Convert.ToInt32(dataRow["Id"])
            , dataRow["Name"].ToString()
            , dataRow["CustomerNo"].ToString()
            , dataRow["Tckn"].ToString()
            , dataRow["Email"].ToString()
            , dataRow["CellPhone"].ToString()
            , dataRow["Status"].ToString()
            , dataRow["ApproveUser"].ToString()
            , dataRow["ApproveTime"].ToString()
            , Convert.ToInt32(dataRow["ApproveStatus"]));
        }

        private InputCustomerEmailAdress GetEmail(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new InputCustomerEmailAdress(dataRow["EmailAddress"].ToString());
        }



        public ParserOperation UpdateCustomerEmailStatus(string status, string aboneno)//aboneno)
        {
            ParserOperation parserOperation = new ParserOperation();
            parserOperation.errorCode = 1;
            DataSet dataSet = null;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] {
                    new SqlParameter("@returnCode", 0),
                    new SqlParameter("@aboneno",  aboneno),
                    new SqlParameter("@status", status)
                };
                sqlParameters[0].Direction = ParameterDirection.Output;
                

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.update_customer_email_status", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                var respStatus = Convert.ToString(sqlParameters[2].Value);
                if (respStatus==status)
                {
                    parserOperation.errorCode = 0;
                    parserOperation.errorDescription= "İşlem Başarılı";
                }
         
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKiosk");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return parserOperation;
        }




        public List<InputCustomerEmailAdress> GetCustomerEmail(long aboneno)//aboneno)
        {
            var emails = new List<InputCustomerEmailAdress>();
            DataSet dataSet = null;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] {
                    new SqlParameter("@returnCode", 0),
                    new SqlParameter("@aboneno",  aboneno),
                    new SqlParameter("@emailAddress", SqlDbType.VarChar,50)
                };
                sqlParameters[0].Direction = ParameterDirection.Output;
                sqlParameters[2].Direction = ParameterDirection.Output;

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_customer_email_by_aboneno", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                var email = Convert.ToString(sqlParameters[2].Value);
                emails.Add(new InputCustomerEmailAdress(aboneno, email)); /*GetEmail(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns)*/

                //emails.Add(GetEmail(dataSet.Tables[0].Rows[0], dataSet.Tables[0].Columns));
                //var ex = dataSet.Tables[0].Rows[0][1];
                //var ex2 = dataSet.Tables[0].Columns;

                //if (dataSet.Tables!=null)
                //{
                //    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                //    {
                //        emails.Add(GetEmail(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));

                //    }
                //}

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKiosk");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return emails;
        }



        public CustomerCollection SearchCustomersOrder(string searchText,
                                                     int numberOfItemsPerPage,
                                                     int currentPageNum,
                                                     ref int recordCount,
                                                     ref int pageCount,
                                                     int orderSelectionColumn,
                                                     int descAsc,
                                                     int instutionId)
        {
            CustomerCollection customers = new CustomerCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                                                                    new SqlParameter("@instutionId", instutionId)
                                                                 };
                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_customers_order", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    customers.Add(this.GetCustomer(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchCustomersOrder");
            }
            finally
            {
                base.Dispose();
                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return customers;
        }

        public WebCustomerCollection SearchWebCustomersOrder(string searchNameText,
                                                          string searchCustNoText,
                                                          string searchTcknText,
                                                          int numberOfItemsPerPage,
                                                          int currentPageNum,
                                                          ref int recordCount,
                                                          ref int pageCount,
                                                          int tableType)
        {
            WebCustomerCollection customers = new WebCustomerCollection();
            DataSet dataSet = null;
            try
            {
                Int64 custNo = 0;
                if (String.IsNullOrEmpty(searchCustNoText))
                    custNo = 0;
                else
                    custNo = Convert.ToInt64(searchCustNoText);

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@recordCount", recordCount), 
                                                                    new SqlParameter("@pageCount", pageCount), 
                                                                    new SqlParameter("@searchNameText", searchNameText),
                                                                    new SqlParameter("@searchCustNoText", custNo),
                                                                    new SqlParameter("@searchTcknText", searchTcknText),
                                                                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage), 
                                                                    new SqlParameter("@currentPageNum", currentPageNum),
                                                                    new SqlParameter("@tableType", tableType)
                                                                 };

                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_web_customers", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                if (tableType == 0)
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        customers.Add(this.GetWebCustomer(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                    }
                else if (tableType == 1)
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        customers.Add(this.GetApproveBLWebCustomer(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                    }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchWebCustomersOrder");
            }
            finally
            {
                base.Dispose();
                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return customers;
        }

        public void GetSecretKey(int customerId, int updatedUser, ref string secretKey)
        {
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@customerId", customerId), 
                                                                    new SqlParameter("@updatedUser", updatedUser), 
                                                                    new SqlParameter("@secretKey",SqlDbType.VarChar,200)
                                                                 };
                sqlParameters[2].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "pk.get_web_customer_secretkey", sqlParameters);

                secretKey = sqlParameters[2].Value.ToString();

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetSecretKey");
            }
            finally
            {

            }
        }

        public void GetCustomerInfoChange(int customerId, int updatedUser, ref string email, ref string cellphone, ref string name)
        {
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@customerId", customerId), 
                                                                    new SqlParameter("@updatedUser", updatedUser), 
                                                                    new SqlParameter("@email",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@cellphone",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@name",SqlDbType.VarChar,200)
                                                                 };
                sqlParameters[2].Direction = sqlParameters[3].Direction = sqlParameters[4].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "pk.get_web_customer_info", sqlParameters);

                email = sqlParameters[2].Value.ToString();
                cellphone = sqlParameters[3].Value.ToString();
                name = sqlParameters[4].Value.ToString();

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetCustomerInfo");
            }
            finally
            {

            }
        }

        public void UpdateCustomerInfo(ref int customerId, int updatedUser, string email, string cellphone)
        {
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@customerId", customerId), 
                                                                    new SqlParameter("@updatedUser", updatedUser), 
                                                                    new SqlParameter("@email",email),
                                                                    new SqlParameter("@cellphone",cellphone)
                                                                 };

                sqlParameters[0].Direction = ParameterDirection.InputOutput;
                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "pk.update_web_customer_info", sqlParameters);

                customerId = Convert.ToInt16(sqlParameters[0].Value.ToString());

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetCustomerInfoChange");
            }
            finally
            {

            }
        }

        public void GenerateCustomerPin(int customerId, int updatedUser, string newPin, ref int opertionResult)
        {
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@customerId", customerId), 
                                                                    new SqlParameter("@updatedUser", updatedUser), 
                                                                    new SqlParameter("@newPin",newPin),
                                                                    new SqlParameter("@opertionResult",SqlDbType.Int)
                                                                 };
                sqlParameters[3].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "pk.generate_customer_pin", sqlParameters);

                opertionResult = Convert.ToInt16(sqlParameters[3].Value);

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGenerateCustomerPin");
            }
            finally
            {

            }
        }

        public void GetCustomerInfo(int customerId, int updatedUser, ref string cellphone, ref string email, ref string name)
        {
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@customerId", customerId), 
                                                                    new SqlParameter("@updatedUser", updatedUser), 
                                                                    new SqlParameter("@cellphone",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@email",SqlDbType.VarChar,200),
                                                                    new SqlParameter("@name",SqlDbType.VarChar,200)
                                                                 };
                sqlParameters[2].Direction = sqlParameters[3].Direction = sqlParameters[4].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "pk.get_web_customer_info", sqlParameters);

                cellphone = sqlParameters[2].Value.ToString();
                email = sqlParameters[3].Value.ToString();
                name = sqlParameters[4].Value.ToString();

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetCustomerInfo");
            }
            finally
            {

            }
        }


        public CustomerCollection SearchCustomersOrderForExcel(string searchText,
                                                     int orderSelectionColumn,
                                                     int descAsc)
        {
            CustomerCollection customers = new CustomerCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@searchText", searchText),
                                                                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                                                                    new SqlParameter("@orderSelectionDescAsc", descAsc)
                                                                 };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_customers_order_for_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    customers.Add(this.GetCustomer(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchCustomersOrderForExcel");
            }
            finally
            {
                base.Dispose();
                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return customers;
        }

        public WebCustomerCollection SearchWebCustomersOrderForExcel(string searchNameText,
                                                          string searchCustNoText,
                                                          string searchTcknText)
        {
            WebCustomerCollection customers = new WebCustomerCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@searchNameText", searchNameText),
                                                                    new SqlParameter("@searchCustNoText", searchCustNoText),
                                                                    new SqlParameter("@searchTcknText", searchTcknText)
                                                                 };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.search_web_customers_for_excel", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    customers.Add(this.GetWebCustomer(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchWebCustomersOrderForExcel");
            }
            finally
            {
                base.Dispose();
                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return customers;
        }




        public void CloseCustomerAccountFirstStep(Int64 customerId, Int64 userId, out int returnCode)
        {
            returnCode = 0;

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@customerId", customerId), 
                                                                    new SqlParameter("@userId", userId), 
                                                                    new SqlParameter("@returnCode",returnCode)
                                                                 };

                sqlParameters[2].Direction = ParameterDirection.InputOutput;

                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "pk.update_web_customer_info", sqlParameters);

                returnCode = Convert.ToInt32(sqlParameters[2].Value.ToString());

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CloseCustomerAccountFirstStep");
            }
            finally
            {

            }
        }

    }
}
