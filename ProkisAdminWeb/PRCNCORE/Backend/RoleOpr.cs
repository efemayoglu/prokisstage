﻿using Microsoft.ApplicationBlocks.Data;
using PRCNCORE.Constants;
using PRCNCORE.Input;
using PRCNCORE.Roles;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Backend
{
    public class RoleOpr : DBOperator
    {
        public RoleOpr()
        {
        }

        internal RoleOpr(SqlConnection sqlConnection)
            : base(sqlConnection)
        {
        }

        public RoleCollection SearchRolesOrder(string searchText,
            int numberOfItemsPerPage,
            int currentPageNum,
            ref int recordCount,
            ref int pageCount,
            int orderSelectionColumn,
            int descAsc)
        {
            RoleCollection roles = new RoleCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@recordCount", recordCount),
                    new SqlParameter("@pageCount", pageCount),
                    new SqlParameter("@searchText", searchText),
                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage),
                    new SqlParameter("@currentPageNum", currentPageNum),
                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                    new SqlParameter("@orderSelectionDescAsc", descAsc)
                };

                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure,
                    "pk.search_roles_order", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    roles.Add(this.GetRole(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchRolesOrder");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return roles;
        }

        public ApproveRoleCollection SearchApproveRoles(string searchText,
            int numberOfItemsPerPage,
            int currentPageNum,
            ref int recordCount,
            ref int pageCount)
        {
            ApproveRoleCollection roles = new ApproveRoleCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@recordCount", recordCount),
                    new SqlParameter("@pageCount", pageCount),
                    new SqlParameter("@searchText", searchText),
                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage),
                    new SqlParameter("@currentPageNum", currentPageNum)
                };

                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure,
                    "pk.search_approve_roles", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    roles.Add(this.GetApproveRole(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchApproveRoles");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return roles;
        }


        public UserLogDetailCollection SearchUserLogDetail(string searchText,
            DateTime startDate,
            DateTime endDate,
            int numberOfItemsPerPage,
            int currentPageNum,
            ref int recordCount,
            ref int pageCount,
            int orderSelectionColumn,
            int descAsc,
            int orderType)
        {
            UserLogDetailCollection roles = new UserLogDetailCollection();
            DataSet dataSet = null;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@recordCount", recordCount),
                    new SqlParameter("@pageCount", pageCount),
                    new SqlParameter("@searchText", searchText),
                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage),
                    new SqlParameter("@currentPageNum", currentPageNum),
                    new SqlParameter("@startDate", startDate),
                    new SqlParameter("@endDate", endDate),
                    //new SqlParameter("@kioskId", kioskId),  
                    new SqlParameter("@orderSelectionColumn", orderSelectionColumn),
                    new SqlParameter("@orderSelectionDescAsc", descAsc),
                    new SqlParameter("@orderType", orderType)
                };

                sqlParameters[0].Direction = sqlParameters[1].Direction = ParameterDirection.InputOutput;
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure,
                    "pk.search_user_log_details", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[0].Value);
                pageCount = Convert.ToInt32(sqlParameters[1].Value);

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    roles.Add(this.GetUserLogDetail(dataSet.Tables[0].Rows[i], dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESearchApproveRoles");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return roles;
        }


        //GetUserLogDetail
        private UserLogDetail GetUserLogDetail(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new UserLogDetail(Convert.ToInt32(dataRow["Id"]),
                dataRow["UserName"].ToString(),
                dataRow["TransactionDate"].ToString(),
                dataRow["Transaction"].ToString(),
                dataRow["Pages"].ToString(),
                dataRow["Status"].ToString(),
                dataRow["Explanation"].ToString()
            );
        }


        public RoleCollection GetRoles(int hospitalId, int userType, string searchText,
            int numberOfItemsPerPage,
            int currentPageNum,
            ref int recordCount,
            ref int pageCount)
        {
            DataSet dataSet = null;
            RoleCollection userRoles = new RoleCollection();

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@searchText", searchText),
                    new SqlParameter("@numberOfItemsPerPage", numberOfItemsPerPage),
                    new SqlParameter("@currentPageNum", currentPageNum),
                    new SqlParameter("@recordCount", recordCount),
                    new SqlParameter("@pageCount", pageCount),
                    new SqlParameter("@UserType", userType),
                    new SqlParameter("@HospitalId", hospitalId)
                };


                sqlParameters[3].Direction = ParameterDirection.InputOutput;
                sqlParameters[4].Direction = ParameterDirection.InputOutput;

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure,
                    "[dbo].[get_all_roles]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                recordCount = Convert.ToInt32(sqlParameters[3].Value);
                pageCount = Convert.ToInt32(sqlParameters[4].Value);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    userRoles.Add(this.GetRole(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetRoles");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return userRoles;
        }

        public RoleNameCollection GetAdminRoleNames()
        {
            DataSet dataSet = null;
            RoleNameCollection userRoleNames = new RoleNameCollection();

            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure,
                    "[dbo].[get_roles_by_hospital_id]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    userRoleNames.Add(this.GetRoleName(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetAdminRoleNames");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return userRoleNames;
        }

        public StatusNameCollection GetKioskStatusNames()
        {
            DataSet dataSet = null;
            StatusNameCollection userStatusNames = new StatusNameCollection();

            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure,
                    "[pk].[get_kiosk_status_names]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    userStatusNames.Add(this.GetStatusName(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKioskStatusNames");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return userStatusNames;
        }

        public StatusNameCollection GetStatusNames()
        {
            DataSet dataSet = null;
            StatusNameCollection userStatusNames = new StatusNameCollection();

            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure,
                    "[pk].[get_status_names]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    userStatusNames.Add(this.GetStatusName(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetStatusNames");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return userStatusNames;
        }

        public RoleCollection GetRoles()
        {
            DataSet dataSet = null;
            RoleCollection roles = new RoleCollection();

            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure,
                    "[dbo].[get_role_names]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    roles.Add(this.GetRole(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetRoles");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return roles;
        }

        public Role GetRole(int roleId)
        {
            DataSet dataSet = null;
            Role role = null;

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] {new SqlParameter("@roleId", roleId)};

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[get_role]",
                    Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    role = this.GetRole(dataRow, dataSet.Tables[0].Columns);
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetRole");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return role;
        }

        public int SaveRole(ref int roleId, string name, string description, int hospitalId)
        {
            int success = 0;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@Id", roleId),
                    new SqlParameter("@Name", name),
                    new SqlParameter("@Description", description),
                    new SqlParameter("@HospitalId", hospitalId)
                };

                sqlParameters[0].Direction = ParameterDirection.Output;

                int a = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure,
                    "[dbo].[save_role]", sqlParameters);

                roleId = Convert.ToInt32(sqlParameters[0].Value);
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveRole");
            }
            finally
            {
                base.Dispose();
            }

            return success;
        }

        public int AddNewRole(ref int roleId, string name, string description, int creationUserId)
        {
            int success = 0;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@Id", roleId),
                    new SqlParameter("@Name", name),
                    new SqlParameter("@Description", description),
                    new SqlParameter("@CreationUserId", creationUserId)
                };

                sqlParameters[0].Direction = ParameterDirection.Output;

                int a = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[add_role]",
                    sqlParameters);

                roleId = Convert.ToInt32(sqlParameters[0].Value);
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREAddNewRole");

                success = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
            }
            finally
            {
                base.Dispose();
            }

            return success;
        }

        public RoleNameCollection GetRoleProjectNames(int roleId)
        {
            DataSet dataSet = null;
            RoleNameCollection roleNames = new RoleNameCollection();
            SqlParameter[] sqlParameters = new SqlParameter[]
               {
                    new SqlParameter("@roleId", roleId),
               };

            try
            {
                //dataSet = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure,
                //    "[dbo].[get_projects_by_roleid]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                dataSet= SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[dbo].[get_projects_by_roleid]",
                   sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    roleNames.Add(this.GetRoleName(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetRoleNames");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return roleNames;
        }
        public RoleNameCollection GetProjectNames()
        {
            DataSet dataSet = null;
            RoleNameCollection roleNames = new RoleNameCollection();

            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure,
                    "[dbo].[get_projects]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    roleNames.Add(this.GetRoleName(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetRoleNames");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return roleNames;
        }

        public RoleNameCollection GetUserRoleConnection(int userId)
        {
            DataSet dataSet = null;
            RoleNameCollection roleNames = new RoleNameCollection();
            SqlParameter[] sqlParameters = new SqlParameter[]
                         {
                    new SqlParameter("@userId", userId),
                         };

            try
            {
                //dataSet = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure,
                //    "[dbo].[get_projects_by_roleid]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[dbo].[get_roles_by_userid]",
                   sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    roleNames.Add(this.GetUserRoleConnection(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetRoleNames");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return roleNames;
        }




        public RoleNameCollection GetRoleNames()
        {
            DataSet dataSet = null;
            RoleNameCollection roleNames = new RoleNameCollection();

            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure,
                    "[pk].[get_role_names]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    roleNames.Add(this.GetRoleName(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetRoleNames");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return roleNames;
        }


        public bool ApproveRoleContext(int roleId,  long approvedUserId)
        {
            bool success = false;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                      new SqlParameter("@returnCode", 0),
                    new SqlParameter("@RoleId", roleId),
                    new SqlParameter("@ApprovedUserId", approvedUserId)
                };
               var a =  SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure,
                    "[pk].[Approve_Role_Context]", sqlParameters);

                if (a >= -1)
                    success = true;
                else
                    success = false;
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREUpdateRole");
            }
            finally
            {
                base.Dispose();
            }

            return success;
        }

        public bool CancelRoleContext(int roleId, long approvedUserId)
        {
            bool success = false;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    
                    new SqlParameter("@returnCode", 0),
                    new SqlParameter("@roleId", roleId),
                    new SqlParameter("@approvedUserId", approvedUserId)
                };
                var a = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure,
                     "[pk].[Cancel_Role_Context]", sqlParameters);

                if (a >= -1)
                    success = true;
                else
                    success = false;
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREUpdateRole");
            }
            finally
            {
                base.Dispose();
            }

            return success;
        }




        public bool UpdateRole(int roleId, string roleName, string description, int updatedUserId)
        {
            bool success = false;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@Id", roleId),
                    new SqlParameter("@Name", roleName),
                    new SqlParameter("@Description", description),
                    new SqlParameter("@UpdatedUserId", updatedUserId)
                };

                int a = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure,
                    "[pk].[update_role]", sqlParameters);

                if (a >= -1)
                    success = true;
                else
                    success = false;
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREUpdateRole");
            }
            finally
            {
                base.Dispose();
            }

            return success;
        }

        private Role GetRole(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new Role(Convert.ToInt32(dataRow["Id"]),
                dataRow["Name"].ToString(),
                dataRow["Description"].ToString(),
                dataRow["CreationDate"].ToString()
            );
        }

        private ApproveRole GetApproveRole(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new ApproveRole(Convert.ToInt32(dataRow["Id"]),
                dataRow["Name"].ToString(),
                dataRow["Description"].ToString(),
                dataRow["UpdatedDate"].ToString(),
                dataRow["UpdatedUser"].ToString()
            );
        }

        private UserRoleConnection GetRoleName(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new UserRoleConnection(Convert.ToInt32(dataRow["Id"]),
                dataRow["Name"].ToString()
            );
        }


        private UserRoleConnection GetUserRoleConnection(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new UserRoleConnection(Convert.ToInt32(dataRow["Id"])
            );
        }


        private StatusName GetStatusName(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new StatusName(Convert.ToInt32(dataRow["Id"]),
                dataRow["Name"].ToString()
            );
        }

        public bool DeleteRole(int roleId)
        {
            bool success = false;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] {new SqlParameter("@Id", roleId)};

                int a = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure,
                    "[dbo].[delete_role]", sqlParameters);

                if (a >= -1)
                    success = true;
                else
                    success = false;
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREDeleteRole");
            }
            finally
            {
                base.Dispose();
            }

            return success;
        }

        public bool DeleteRoleSubMenus(int roleId)
        {
            bool success = false;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] {new SqlParameter("@Id", roleId)};

                int a = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure,
                    "[dbo].[delete_role_submenu]", sqlParameters);

                if (a >= -1)
                    success = true;
                else
                    success = false;
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREDeleteRoleSubMenus");
            }
            finally
            {
                base.Dispose();
            }

            return success;
        }

        public List<RoleN> RoleActionList()
        {
            var roles = new List<RoleN>();
            DataSet dataSet = null;
            try
            {
                var sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@returnCode", 0)
                };

                sqlParameters[0].Direction = ParameterDirection.InputOutput;

                dataSet = SqlHelper.ExecuteDataset(
                    base.GetConnection(),
                    CommandType.StoredProcedure,
                    "dbo.RoleActionList",
                    Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")),
                    sqlParameters);


                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    if (dataRow == null)
                        continue;

                    roles.Add(new RoleN
                    {
                        Id = int.Parse(dataRow["Id"].ToString()),
                        RoleLabel = dataRow["RoleLabel"].ToString(),
                        RoleName = dataRow["RoleName"].ToString(),
                        RoleExplain = dataRow["RoleExplain"].ToString(),
                        Status =
                            dataRow["Status"] == null || dataRow["Status"].ToString() == ""
                                ? 0
                                : (bool) dataRow["Status"]
                                    ? 1
                                    : 0
                    });
                }
            }
            catch (Exception ex)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), ex, "CORERoleActionList");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return roles;
        }

        public List<RoleContext> RoleSubContextMenuList(int roleId)
        {
            var subContextMenuList = new List<RoleContext>();
            DataSet dataSet = null;
            try
            {
                var sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@returnCode", 0),
                    new SqlParameter("@roleId", roleId)
                };

                sqlParameters[0].Direction = ParameterDirection.InputOutput;
              

                dataSet = SqlHelper.ExecuteDataset(
                    base.GetConnection(),
                    CommandType.StoredProcedure,
                    "pk.RoleSubContextMenuList",
                    Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")),
                    sqlParameters);


                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    if (dataRow == null)
                        continue;

                    subContextMenuList.Add(new RoleContext
                    {
                        Id = int.Parse(dataRow["Id"].ToString()),
                        RoleId = int.Parse(dataRow["RoleId"].ToString()),
                        RoleActionId = int.Parse(dataRow["RoleActionId"].ToString()),
                        RoleActionValue = int.Parse(dataRow["RoleActionValue"].ToString()),
                        SubMenuId = int.Parse(dataRow["SubMenuId"].ToString()),
                        OldRoleActionValue = int.Parse(dataRow["OldRoleActionValue"].ToString()),
                        //UniqueId = dataRow["UniqueId"] as string,

                        Status =
                            dataRow["Status"] == null || dataRow["Status"].ToString() == ""
                                ? 0
                                : int.Parse(dataRow["Status"].ToString()),
                    });
                }
            }
            catch (Exception ex)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), ex, "CORERoleSubContextMenuList");
            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return subContextMenuList;
        }
    }

    public class RoleContext
    {
        public int Id;
        public int RoleId;
        public int RoleActionId;
        public int RoleActionValue;
        public int OldRoleActionValue;
        public int SubMenuId;
        public int Status;
        public string UniqueId;
    }

    public class RoleN
    {
        public int Id;
        public string RoleName;
        public int Status;
        public string RoleLabel;
        public string RoleExplain;

    }
}