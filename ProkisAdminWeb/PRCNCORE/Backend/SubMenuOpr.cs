﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ApplicationBlocks.Data;

using System.Data;
using System.Data.SqlClient;
using PRCNCORE.Constants;
using PRCNCORE.Input;
using PRCNCORE.RoleSubMenus;
using PRCNCORE.SubMenus;
using PRCNCORE.Utilities;


namespace PRCNCORE.Backend
{
    public class SubMenuOpr : DBOperator
    {
        public SubMenuOpr()
        {

        }

        internal SubMenuOpr(SqlConnection sqlConnection)
            : base(sqlConnection)
        {

        }

        public RoleSubMenuCollection GetRoleMenus(int roleId)
        {
            DataSet dataSet = null;
            RoleSubMenuCollection subMenus = new RoleSubMenuCollection();

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@RoleId", roleId) };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[get_all_menus_by_roleid]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    subMenus.Add(this.GetRoleSubMenu(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetRoleMenus");

            }
            finally
            {

                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return subMenus;
        }

        public RoleSubMenuContextCollection GetRoleMenusContext(int roleId)
        {
            DataSet dataSet = null;
            RoleSubMenuContextCollection subMenus = new RoleSubMenuContextCollection();

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@RoleId", roleId) };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[get_all_menus_by_roleid_context2]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    subMenus.Add(this.GetRoleSubMenuContext(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetRoleMenus");

            }
            finally
            {

                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return subMenus;
        }

        public void GetRoleNameDesc(int roleId,int type, ref string name, ref string description)
        {
           

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@RoleId", roleId),
                                                                    new SqlParameter("@Name", SqlDbType.VarChar,200) ,
                                                                    new SqlParameter("@Description", SqlDbType.VarChar,200),
                                                                    new SqlParameter("@Type", type)};

                sqlParameters[1].Direction = sqlParameters[2].Direction = ParameterDirection.Output;
                SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[get_role_name_desc]", sqlParameters);
                name = sqlParameters[1].Value.ToString();
                description = sqlParameters[2].Value.ToString();
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetRoleNameDesc");

            }
            finally
            {

                base.Dispose();

            }
        }

        public RoleSubMenuCollection GetNewRoleMenus(int roleId)
        {
            DataSet dataSet = null;
            RoleSubMenuCollection subMenus = new RoleSubMenuCollection();

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@RoleId", roleId) };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[pk].[get_all_new_menus_by_roleid]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    subMenus.Add(this.GetRoleSubMenu(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetNewRoleMenus");

            }
            finally
            {

                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return subMenus;
        }

        public RoleSubMenuCollection GetAdminMenus()
        {
            DataSet dataSet = null;
            RoleSubMenuCollection subMenus = new RoleSubMenuCollection();

            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[dbo].[get_admin_menus]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    subMenus.Add(this.GetRoleSubMenu(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetAdminMenus");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return subMenus;
        }

        public SubMenuCollection GetSubMenus(int menuId)
        {
            DataSet dataSet = null;
            SubMenuCollection subMenus = new SubMenuCollection();

            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@MenuId", menuId) };
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[dbo].[get_sub_menus]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    subMenus.Add(this.GetSubMenu(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetSubMenus");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return subMenus;
        }

        public SubMenuCollection GetSubMenus()
        {
            DataSet dataSet = null;
            SubMenuCollection subMenus = new SubMenuCollection();

            try
            {
                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "[dbo].[get_all_sub_menus]", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")));

                foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                {
                    subMenus.Add(this.GetSubMenu(dataRow, dataSet.Tables[0].Columns));
                }
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetSubMenus");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }
            return subMenus;
        }

        private SubMenu GetSubMenu(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new SubMenu(Convert.ToInt32(dataRow["Id"]),
                        dataRow["Name"].ToString(), Convert.ToInt32(dataRow["MenuId"])
                        );
        }

        private RoleSubMenu GetRoleSubMenu(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new RoleSubMenu(Convert.ToInt32(dataRow["MenuId"]),
                        dataRow["MenuName"].ToString(),
                        Convert.ToInt32(dataRow["SubMenuId"]),
                        dataRow["SubMenuName"].ToString(),
                        dataRow["SubMenuUrl"].ToString(),
                        Convert.ToInt32(dataRow["canAdd"]),
                        Convert.ToInt32(dataRow["canDelete"]),
                        Convert.ToInt32(dataRow["canEdit"]),
                        dataRow["ClassName"].ToString(),
                        dataRow["SmallClassName"].ToString(),
                        dataRow["SmallClassColor"].ToString()
                        );
        }


        private RoleSubMenuContext GetRoleSubMenuContext(DataRow dataRow, DataColumnCollection dataColumns)
        {
            return new RoleSubMenuContext(Convert.ToInt32(dataRow["MenuId"]),
                        dataRow["MenuName"].ToString(),
                        Convert.ToInt32(dataRow["SubMenuId"]),
                        dataRow["SubMenuName"].ToString(),
                        dataRow["SubMenuUrl"].ToString(),
                        dataRow["RoleActionName"].ToString(),
                        dataRow["UniqueId"].ToString(),
                        Convert.ToInt32(dataRow["RoleActionValue"]),
                        dataRow["ClassName"].ToString(),
                        dataRow["SmallClassName"].ToString(),
                        dataRow["SmallClassColor"].ToString(),
                        dataRow["RoleLabel"].ToString(),
                        Convert.ToInt16(dataRow["Status"]),
                        Convert.ToInt16(dataRow["OldRoleActionValue"])
                        );
        }
        public int DeleteRoleSubMenus(int roleId)
        {
            int success = 0;
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@RoleId", roleId) };

                int a = SqlHelper.ExecuteNonQuery(base.GetConnection(), CommandType.StoredProcedure, "[pk].[delete_role_submenus]", sqlParameters);

            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREDeleteRoleSubMenus");

                success = Convert.ToInt32(ManagementScreenErrorCodes.SystemError);
            }
            finally
            {
                base.Dispose();
            }
            return success;
        }

        public bool SaveRoleSubMenus(DataTable dt)
        {
            bool success = false;
            try
            {
                SqlDataAdapter pSqlda = null;
                SqlCommand sqlCmd = null;

                pSqlda = new SqlDataAdapter(sqlCmd);

                sqlCmd = new SqlCommand();
                sqlCmd.Connection = base.GetConnection();
                sqlCmd.CommandText = "[pk].[save_role_submenu]";
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.UpdatedRowSource = UpdateRowSource.None;

                sqlCmd.Parameters.Add("@RoleId", SqlDbType.Int).SourceColumn = "RoleId";
                sqlCmd.Parameters.Add("@canEdit", SqlDbType.Int).SourceColumn = "Edit";
                sqlCmd.Parameters.Add("@canDelete", SqlDbType.Int).SourceColumn = "Delete";
                sqlCmd.Parameters.Add("@canAdd", SqlDbType.Int).SourceColumn = "Add";
                sqlCmd.Parameters.Add("@SubMenuId", SqlDbType.Int).SourceColumn = "SubMenuId";
                sqlCmd.Parameters.Add("@createdserId", SqlDbType.Int).SourceColumn = "CreatedserId";

                pSqlda.InsertCommand = sqlCmd;
                pSqlda.UpdateBatchSize = 0; // 0 alabileceği max kadar alıyor. 300- 500 gibi degerlerde verilebilir.

                int records = pSqlda.Update(dt);
                success = true;
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveRoleSubMenus");

            }
            finally
            {
                base.Dispose();

            }
            return success;
        }

        public bool SaveRoleSubMenusContext(DataTable dt)
        {
            bool success = false;
            try
            {
                SqlDataAdapter pSqlda = null;
                SqlCommand sqlCmd = null;

                pSqlda = new SqlDataAdapter(sqlCmd);

                sqlCmd = new SqlCommand();
                sqlCmd.Connection = base.GetConnection();
                sqlCmd.CommandText = "[pk].[save_role_submenu_context]";
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.UpdatedRowSource = UpdateRowSource.None;

                sqlCmd.Parameters.Add("@RoleId", SqlDbType.Int).SourceColumn = "RoleId";
                sqlCmd.Parameters.Add("@SubMenuId", SqlDbType.Int).SourceColumn = "SubMenuId";
                sqlCmd.Parameters.Add("@RoleActionId", SqlDbType.Int).SourceColumn = "RoleActionId";
                sqlCmd.Parameters.Add("@RoleActionValue", SqlDbType.Int).SourceColumn = "RoleActionValue";

                pSqlda.InsertCommand = sqlCmd;
                pSqlda.UpdateBatchSize = 0; // 0 alabileceği max kadar alıyor. 300- 500 gibi degerlerde verilebilir.

                int records = pSqlda.Update(dt);
                success = true;
            }

            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CORESaveRoleSubMenus");

            }
            finally
            {
                base.Dispose();

            }
            return success;
        }
    }
}

