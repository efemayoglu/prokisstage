﻿using PRCNCORE.Backend;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Roles
{
    public class RoleCollection: ReadOnlyCollection<Role>
    {
        internal RoleCollection()
             : base(new List<Role>())
        {

        }

        internal void Add(Role role)
        {
            if (role != null && !this.Contains(role))
            {
                this.Items.Add(role);
            }
        }

        public JObject GetJSONJObject()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (Role item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("Roles", jArray);

            return jObjectDis;
        }

        public JArray GetJSONJArray()
        {
            JArray jArray = new JArray();

            foreach (Role item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            return jArray;
        }
    }
}

