﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PRCNCORE.Roles
{
    public class UserRoleConnection
    {
         private int id;

        public int Id
        {
            get { return this.id; }
        }

        private string name;

        public string Name
        {
            get { return this.name; }
        }

        internal UserRoleConnection(int id, string name)
        {
            this.id = id;
            this.name = name;
        }

        internal UserRoleConnection(int id )
        {
            this.id = id;
        }
    }



}
