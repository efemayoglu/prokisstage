﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Roles
{
    public class UserLogDetail
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private string userName;
        public string UserName
        {
            get { return userName; }
        }

        private string transactionDate;
        public string TransactionDate
        {
            get { return transactionDate; }
        }

        private string transaction;
        public string Transaction
        {
            get { return transaction; }
        }

         private string pages;
         public string Pages
         {
             get { return pages; }
         }

        private string status;
        public string Status
        {
            get { return status; }
        }

        private string explanation;
        public string Explanation
        {
            get { return explanation; }
        }


        internal UserLogDetail(int id
            , string userName
            , string transactionDate
            , string transaction

            , string pages
            , string status
            , string explanation
 
      
            )
        {
            this.id = id;
            this.userName = userName;
            this.transactionDate = transactionDate;
            this.transaction = transaction;
            this.pages = pages;
            this.status = status;
            this.explanation = explanation;
        
        }

    }
}
