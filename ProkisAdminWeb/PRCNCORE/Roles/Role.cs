﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Roles
{
    public class Role
    {
        private int id;

        public int Id
        {
            get { return this.id; }
        }

        private string name;

        public string Name
        {
            get { return this.name; }
        }

        private string description;

        public string Description
        {
            get { return this.description; }
        }

        private string creationDate;

        public string CreationDate
        {
            get { return this.creationDate; }
        }

        internal Role(int id, string name, string description, string creationDate)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.creationDate = creationDate;
        }
    }
}
