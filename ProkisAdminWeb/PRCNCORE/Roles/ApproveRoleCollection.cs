﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Roles
{
   public  class ApproveRoleCollection: ReadOnlyCollection<ApproveRole>
    {
       internal ApproveRoleCollection()
             : base(new List<ApproveRole>())
        {

        }

       internal void Add(ApproveRole role)
        {
            if (role != null && !this.Contains(role))
            {
                this.Items.Add(role);
            }
        }

        public JObject GetJSONJObject()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (ApproveRole item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("Roles", jArray);

            return jObjectDis;
        }

        public JArray GetJSONJArray()
        {
            JArray jArray = new JArray();

            foreach (ApproveRole item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            return jArray;
        }
    }
}
