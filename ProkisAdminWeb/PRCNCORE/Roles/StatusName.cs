﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Roles
{
    public class StatusName
    {
        private int id;

        public int Id
        {
            get { return this.id; }
        }

        private string name;

        public string Name
        {
            get { return this.name; }
        }

        internal StatusName(int id, string name)
        {
            this.id = id;
            this.name = name;
        }
    }
}
