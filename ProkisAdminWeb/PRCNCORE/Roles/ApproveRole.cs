﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Roles
{
    public class ApproveRole
    {
        private int id;

        public int Id
        {
            get { return this.id; }
        }

        private string name;

        public string Name
        {
            get { return this.name; }
        }

        private string description;

        public string Description
        {
            get { return this.description; }
        }

        private string updatedDate;

        public string UpdatedDate
        {
            get { return this.updatedDate; }
        }

        private string updatedUser;

        public string UpdatedUser
        {
            get { return this.updatedUser; }
        }

        internal ApproveRole(int id, string name, string description, string updatedDate, string updatedUser)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.updatedDate = updatedDate;
            this.updatedUser = updatedUser;
            
        }
    }
}
