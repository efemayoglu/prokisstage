﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.MoneyCodes
{
    public class MoneyCode
    {
        private Int64 id;
        public Int64 Id
        {
            get { return id; }
        }

        private int statusId;
        public int StatusId
        {
            get { return statusId; }
        }

        private string statusName;
        public string StatusName
        {
            get { return statusName; }
        }

        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
        }

        private string aboneNo;
        public string AboneNo
        {
            get { return aboneNo; }
        }

        private Int64 cardId;
        public Int64 CardId
        {
            get { return cardId; }
        }

        private Int64 transactionId;
        public Int64 TransactionId
        {
            get { return transactionId; }
        }

        private string codeAmount;
        public string CodeAmount
        {
            get { return codeAmount; }
        }

        private string codeNumber;
        public string CodeNumber
        {
            get { return codeNumber; }
        }

        private string codeGenerationCase;
        public string CodeGenerationCase
        {
            get { return codeGenerationCase; }
        }

        private string useDate;
        public string UseDate
        {
            get { return useDate; }
        }

        private string generatedUserId;
        public string GeneratedUserId
        {
            get { return generatedUserId; }
        }

        private string generatedUserName;
        public string GeneratedUserName
        {
            get { return generatedUserName; }
        }

        private string instutionName;
        public string InstutionName
        {
            get { return instutionName; }
        }

        private string generatedKioskName;
        public string GeneratedKioskName
        {
            get { return generatedKioskName; }
        }

        private string usedKioskName;
         public string UsedKioskName
        {
            get { return usedKioskName; }
        }

         private string deletingReason;
         public string DeletingReason
         {
             get { return deletingReason; }

         }

         private string sendingSMS;
         public string SendingSMS
         {
             get { return sendingSMS; }

         }

         private string approvalSMS;
         public string ApprovalSMS
         {
             get { return approvalSMS; }

         }

         private string codeDeleteUser;
         public string CodeDeleteUser
         {
             get { return codeDeleteUser; }

         }

         private string countSMSCell;
         public string CountSMSCell
         {
             get { return countSMSCell; }

         }
         private string approvedUser;
         public string ApprovedUser
         {
             get { return approvedUser; }

         }

         private string approvedDate;
         public string ApprovedDate
         {
             get { return approvedDate; }

         }

         private string className;
         public string ClassName
         {
             get { return className; }

         }

         internal MoneyCode(Int64 id,
                           int statusId,
                           string statusName,
                           string insertionDate,
                           string customerName,
                           string aboneNo,
                           Int64 cardId,
                           Int64 transactionId,
                           string codeAmount,
                           string codeNumber,
                           string codeGenerationCase,
                           string useDate,
                           string generatedUserId,
                           string generatedUserName,
                           string instutionName,
                           string generatedKioskName,
                           string usedKioskName,
                           string deletingReason,
                           string sendingSMS,
                           string approvalSMS,
                           string codeDeleteUser,
                           string countSMSCell, 
                           string approvedUser,
                           string approvedDate,
                           string className)
        {
            this.id = id;
            this.statusId = statusId;
            this.statusName = statusName;
            this.insertionDate = insertionDate;
            this.customerName = customerName;
            this.aboneNo = aboneNo;
            this.cardId = cardId;
            this.transactionId = transactionId;
            this.codeAmount = codeAmount;
            this.codeNumber = codeNumber;
            this.codeGenerationCase = codeGenerationCase;
            this.useDate = useDate;
            this.generatedUserId = generatedUserId;
            this.generatedUserName = generatedUserName;
            this.instutionName = instutionName;
            this.generatedKioskName = generatedKioskName;
            this.usedKioskName = usedKioskName;
            this.deletingReason = deletingReason;
            this.sendingSMS = sendingSMS;
            this.approvalSMS = approvalSMS;
            this.codeDeleteUser = codeDeleteUser;
            this.countSMSCell = countSMSCell;
            this.approvedUser = approvedUser;
            this.approvedDate = approvedDate;
            this.className = className;
        }
    }
    public class MoneyCodeSweepBack
    {
        private Int64 id;
        public Int64 Id
        {
            get { return id; }
        }

        private int statusId;
        public int StatusId
        {
            get { return statusId; }
        }

        private string statusName;
        public string StatusName
        {
            get { return statusName; }
        }

        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
        }

        private string aboneNo;
        public string AboneNo
        {
            get { return aboneNo; }
        }

        private Int64 cardId;
        public Int64 CardId
        {
            get { return cardId; }
        }

        private Int64 transactionId;
        public Int64 TransactionId
        {
            get { return transactionId; }
        }

        private string codeAmount;
        public string CodeAmount
        {
            get { return codeAmount; }
        }

        private string codeNumber;
        public string CodeNumber
        {
            get { return codeNumber; }
        }

        private string codeGenerationCase;
        public string CodeGenerationCase
        {
            get { return codeGenerationCase; }
        }

        private string useDate;
        public string UseDate
        {
            get { return useDate; }
        }

        private string generatedUserId;
        public string GeneratedUserId
        {
            get { return generatedUserId; }
        }

        private string generatedUserName;
        public string GeneratedUserName
        {
            get { return generatedUserName; }
        }

        private string instutionName;
        public string InstutionName
        {
            get { return instutionName; }
        }

        private string generatedKioskName;
        public string GeneratedKioskName
        {
            get { return generatedKioskName; }
        }

        private string usedKioskName;
        public string UsedKioskName
        {
            get { return usedKioskName; }
        }

        private string deletingReason;
        public string DeletingReason
        {
            get { return deletingReason; }

        }

        private string className;
        public string ClassName
        {
            get { return className; }

        }

        internal MoneyCodeSweepBack(Int64 id,
                          int statusId,
                          string statusName,
                          string insertionDate,
                          string customerName,
                          string aboneNo,
                          Int64 cardId,
                          Int64 transactionId,
                          string codeAmount,
                          string codeNumber,
                          string codeGenerationCase,
                          string useDate,
                          string generatedUserId,
                          string generatedUserName,
                          string instutionName,
                          string className)
        {
            this.id = id;
            this.statusId = statusId;
            this.statusName = statusName;
            this.insertionDate = insertionDate;
            this.customerName = customerName;
            this.aboneNo = aboneNo;
            this.cardId = cardId;
            this.transactionId = transactionId;
            this.codeAmount = codeAmount;
            this.codeNumber = codeNumber;
            this.codeGenerationCase = codeGenerationCase;
            this.useDate = useDate;
            this.generatedUserId = generatedUserId;
            this.generatedUserName = generatedUserName;
            this.instutionName = instutionName;
            this.className = className;
            //this.generatedKioskName = generatedKioskName;
            //this.usedKioskName = usedKioskName;
            //this.deletingReason = deletingReason;
        }
    }
}