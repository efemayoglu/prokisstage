﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.MoneyCodes
{
    public class MoneyCodeCollection : ReadOnlyCollection<MoneyCode>
    {
        internal MoneyCodeCollection()
            : base(new List<MoneyCode>())
        {

        }

        internal void Add(MoneyCode transaction)
        {
            if (transaction != null && !this.Contains(transaction))
            {
                this.Items.Add(transaction);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (MoneyCode item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("MoneyCodes", jArray);

            return jObjectDis;
        }

    }
    public class MoneyCodeSweepBackCollection : ReadOnlyCollection<MoneyCodeSweepBack>
    {
        internal MoneyCodeSweepBackCollection()
            : base(new List<MoneyCodeSweepBack>())
        {

        }

        internal void Add(MoneyCodeSweepBack transaction)
        {
            if (transaction != null && !this.Contains(transaction))
            {
                this.Items.Add(transaction);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (MoneyCodeSweepBack item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("MoneyCodes", jArray);

            return jObjectDis;
        }
    }
    
}
