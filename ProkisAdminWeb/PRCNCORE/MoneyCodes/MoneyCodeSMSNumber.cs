﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.MoneyCodes
{
    public class MoneyCodeSMSNumber
    {

        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

        private string userName;
        public string UserName
        {
            get { return userName; }
        }

        private string operationId;
        public string OperationId
        {
            get { return operationId; }

        }

        internal MoneyCodeSMSNumber(string operationId,
                          string userName,
                          string insertionDate)
        {
            this.operationId = operationId;
            this.userName = userName;
            this.insertionDate = insertionDate;
           
        }
    }
}
