﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class KioskReferenceNumber
    {
        private string id;
        public string ID
        {
            get { return id; }
        }

        private string referenceNumber;
        public string ReferenceNumber
        {
            get { return referenceNumber; }
        }

        internal KioskReferenceNumber(string id
            , string referenceNumber)
        {
            this.id = id;
            this.referenceNumber = referenceNumber;
        }
    }
}
