﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class KioskMonitoring
    {
        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
            set { kioskName = value; }
        }

        private string kioskAddress;
        public string Address
        {
            get { return kioskAddress; }
        }
 

        private string lastTransactionTime;
        public string LastSuccessTime
        {
            get { return lastTransactionTime; }
        }

        private string onlineStatus;
        public string LastAppAliveTime
        {
            get { return onlineStatus; }
        }

        private string transactionCount;
        public string SuccessTxnCount
        {
            get { return transactionCount; }
        }

        private string totalTransactionAmout;
        public string SuccessTxnAmount
        {
            get { return totalTransactionAmout; }
        }

        private string latitude;
        public string Latitude
        {
            get { return latitude; }
        }

        private string longitude;
        public string Longitude
        {
            get { return longitude; }
        }

        private string resultCount;
        public string ResultCount
        {
            get { return resultCount; }
        }

        private string className;
        public string ClassName
        {
            get { return className; }
            set { className = value; }
        }

        internal KioskMonitoring(string kioskName
            , string lastTransactionTime
            , string onlineStatus
            , string transactionCount
            , string totalTransactionAmout
            , string kioskAddress
            , string latitude
            , string longitude
            , string resultCount
            , string className
            )
        {
            
            this.kioskName = kioskName;
            this.kioskAddress = kioskAddress;
            this.lastTransactionTime = lastTransactionTime;
            this.onlineStatus = onlineStatus;
            this.transactionCount = transactionCount;
            this.totalTransactionAmout = totalTransactionAmout;
            this.latitude = latitude;
            this.longitude = longitude;
            this.resultCount = resultCount;
            this.className = className;
        }

    }
}
