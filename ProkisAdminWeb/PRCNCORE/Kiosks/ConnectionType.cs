﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
  
    public class ConnectionType
    {
        private int id;

        public int Id
        {
            get { return this.id; }
        }

        private string name;

        public string Name
        {
            get { return this.name; }
        }

        internal ConnectionType(int id, string name)
        {
            this.id = id;
            this.name = name;
        }
    }


    public class DistrictAndNeighborhood
    {
        private int id;

        public int Id
        {
            get { return this.id; }
        }

        private string name;

        public string Name
        {
            get { return this.name; }
        }

        private int cityId;

        public int CityId
        {
            get { return this.cityId; }
        }

        private string cityName;

        public string CityName
        {
            get { return this.cityName; }
        }


        private int districtId;

        public int DistrictId
        {
            get { return this.districtId; }
        }

        private string districtName;

        public string DistrictName
        {
            get { return this.districtName; }
        }

        internal DistrictAndNeighborhood(int id, string name,
            int districtId, string districtName,
            int cityId, string cityName)
        {
            this.id = id;
            this.name = name;

            this.districtId = districtId;
            this.districtName = districtName;

            this.cityId = cityId;
            this.cityName = cityName;

        }
    }
}
