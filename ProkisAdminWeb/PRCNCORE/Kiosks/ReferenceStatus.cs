﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class ReferenceStatus
    {
        	private int id;
        public int Id
        {
            get { return id; }
        }

        private string name;
        public string Name
        {
            get { return name; }
        }

        internal ReferenceStatus(int id
            , string name)
        {
            this.id = id;
            this.name = name;
        }
    }
}
