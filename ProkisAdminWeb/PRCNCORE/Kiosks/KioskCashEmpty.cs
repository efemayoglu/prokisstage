﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class KioskCashEmpty
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private int kioskId;
        public int KioskId
        {
            get { return kioskId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string expertUserId;
        public string ExpertUserId
        {
            get { return expertUserId; }
        }

        private string expertUserName;
        public string ExpertUserName
        {
            get { return expertUserName; }
        }


        private string updatedDate;
        public string UpdatedDate
        {
            get { return updatedDate; }
        }

        private string amount;
        public string Amount
        {
            get { return amount; }
        }

        private int cashCount;
        public int CashCount
        {
            get { return cashCount; }
        }

        private int ribbonRate;
        public int RibbonRate
        {
            get { return ribbonRate; }
        }

        private int coinCount;
        public int CoinCount
        {
            get { return coinCount; }
        }

        private string securityCode;
        public string SecurityCode
        {
            get { return securityCode; }
        }

        private string referenceNo;
        public string ReferenceNo
        {
            get { return referenceNo; }
        }

        private string className;
        public string ClassName
        {
            get { return className; }
        }

        internal KioskCashEmpty(int id
                              , int kioskId
                              , string kioskName
                              , string expertUserId
                              , string expertUserName
                              , string updatedDate
                              , string amount
                              , int cashCount
                              , int ribbonRate
                              , int coinCount
                              , string securityCode
                              , string referenceNo
                              , string className
                            )
        {
            this.id = id;
            this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.expertUserId = expertUserId;
            this.expertUserName = expertUserName;
            this.updatedDate = updatedDate;
            this.amount = amount;
            this.cashCount = cashCount;
            this.ribbonRate = ribbonRate;
            this.coinCount = coinCount;
            this.securityCode = securityCode;
            this.referenceNo = referenceNo;
            this.className = className;
        }
    }
}
