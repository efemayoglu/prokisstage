﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class LocalLog
    {
        private string logId;
        public string LogId
        {
            get { return logId; }
        }
        private string logDate;
        public string LogDate
        {
            get { return logDate; }
        }
        private string logDetail;
        public string LogDetail
        {
            get { return logDetail; }
        }
        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        internal LocalLog(string logId
            , string logDate
            , string logDetail
            , string kioskName)
        {
            this.logId = logId;
            this.kioskName = kioskName;
            this.logDate = logDate;
            this.logDetail = logDetail;
        }
    }

    public class ErrorLog
    {
        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }
        private string processDate;
        public string ProcessDate
        {
            get { return processDate; }
        }
        private string deviceType;
        public string DeviceType
        {
            get { return deviceType; }
        }
        private string errorType;
        public string ErrorType
        {
            get { return errorType; }
        }
        private string errorId;
        public string ErrorId
        {
            get { return errorId; }
        }
        private string repairState;
        public string RepairState
        {
            get { return repairState; }
        }
        private string repairTime;
        public string RepairTime
        {
            get { return repairTime; }
        }
        private string repairOperatorName;
        public string RepairOperatorName
        {
            get { return repairOperatorName; }
        }
        private string repairOperatorName2;
        public string RepairOperatorName2
        {
            get { return repairOperatorName2; }
        }
        private string explanation;
        public string Explanation
        {
            get { return explanation; }
        }
        private string kioskId;
        public string KioskId
        {
            get { return kioskId; }
        }
        private string id;
        public string Id
        {
            get { return id; }
        }
        private string isNotified;
        public string IsNotified
        {
            get { return isNotified; }
        }
        private string notificationDate;
        public string NotificationDate
        {
            get { return notificationDate; }
        }
        private string notificationType;
        public string NotificationType
        {
            get { return notificationType; }
        }

        private string approvedUser;
        public string ApprovedUser
        {
            get { return approvedUser; }
        }
        private string approvedDate;
        public string ApprovedDate
        {
            get { return approvedDate; }
        }
        private string calculatedTime;
        public string CalculatedTime
        {
            get { return calculatedTime; }
        }

        //private int orderSelectionAsc;
        //public int OrderSelectionAsc
        //{
        //    get { return orderSelectionAsc; }
        //}
        //private int orderColumn;
        //public int OrderColumn
        //{
        //    get { return orderColumn; }
        //}

        //private string errorLogId;
        //public string ErrorLogId
        //{
        //    get { return errorLogId; }
        //}

        internal ErrorLog(string insertionDate
            , string processDate
            , string deviceType
            , string errorType
            , string errorId
            , string repairState
            , string repairTime
            , string repairOperatorName
            , string repairOperatorName2
            , string explanation
            , string kioskId
            , string id
            , string isNotified
            , string notificationDate
            , string notificationType
            , string approvedUser
            , string approvedDate
            , string calculatedTime
            //, int orderSelectionAsc
            //, int orderColumn
            //, string errorLogId

            )
        {
            this.insertionDate = insertionDate;
            this.processDate = processDate;
            this.deviceType = deviceType;
            this.errorType = errorType;
            this.errorId = errorId;
            this.repairState = repairState;
            this.repairTime = repairTime;
            this.repairOperatorName = repairOperatorName;
            this.repairOperatorName2 = repairOperatorName2;
            this.explanation = explanation;
            this.kioskId = kioskId;
            this.id = id;
            this.isNotified = isNotified;
            this.notificationDate = notificationDate;
            this.notificationType = notificationType;
            this.approvedUser = approvedUser;
            this.approvedDate = approvedDate;
            this.calculatedTime = calculatedTime;
            //this.orderSelectionAsc = orderSelectionAsc;
            //this.orderColumn = orderColumn;
            //this.errorLogId = errorLogId;
          
        }
    }
}
