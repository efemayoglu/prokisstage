﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class LocalLogCollection : ReadOnlyCollection<LocalLog>
    {
        internal LocalLogCollection()
            : base(new List<LocalLog>())
        {

        }

        internal void Add(LocalLog item)
        {
            if (item != null && !this.Contains(item))
            {
                this.Items.Add(item);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (LocalLog item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("LocalLogs", jArray);

            return jObjectDis;
        }

    }

    public class ErrorLogCollection : ReadOnlyCollection<ErrorLog>
    {
        internal ErrorLogCollection()
            : base(new List<ErrorLog>())
        {

        }

        internal void Add(ErrorLog item)
        {
            if (item != null && !this.Contains(item))
            {
                this.Items.Add(item);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (ErrorLog item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("ErrorLogs", jArray);

            return jObjectDis;
        }

    }
}

