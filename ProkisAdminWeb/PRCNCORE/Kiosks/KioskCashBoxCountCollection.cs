﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class KioskCashBoxCountCollection : ReadOnlyCollection<KioskCashBoxCount>
    {
        internal KioskCashBoxCountCollection()
            : base(new List<KioskCashBoxCount>())
        {

        }

        internal void Add(KioskCashBoxCount kiosk)
        {
            if (kiosk != null && !this.Contains(kiosk))
            {
                this.Items.Add(kiosk);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (KioskCashBoxCount item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("CashCount", jArray);

            return jObjectDis;
        }

    }

    public class KioskCashBoxCountTotalCollection : ReadOnlyCollection<KioskCashBoxCountTotal>
    {
        internal KioskCashBoxCountTotalCollection()
            : base(new List<KioskCashBoxCountTotal>())
        {

        }

        internal void Add(KioskCashBoxCountTotal kiosk)
        {
            if (kiosk != null && !this.Contains(kiosk))
            {
                this.Items.Add(kiosk);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (KioskCashBoxCountTotal item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("CashCount", jArray);

            return jObjectDis;
        }

    }
}

