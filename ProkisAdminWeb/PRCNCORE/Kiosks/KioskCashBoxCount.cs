﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class KioskCashBoxCount
    {
        private int kioskId;
        public int KioskId
        {
            get { return kioskId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string cashTypeName;
        public string CashTypeName
        {
            get { return cashTypeName; }
        }

        private int cashCount;
        public int CashCount
        {
            get { return cashCount; }
        }

        private string cassetteTotal;
        public string CassetteTotal
        {
            get { return cassetteTotal; }
        }

        internal KioskCashBoxCount(int kioskId
                              , string kioskName
                              , string cashTypeName
                              , int cashCount)
        {
            this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.cashTypeName = cashTypeName;
            this.cashCount = cashCount;
        }
    }

    public class KioskCashBoxCountTotal
    {
        private int kioskId;
        public int KioskId
        {
            get { return kioskId; }
        }

        private string explanation;
        public string Explanation
        {
            get { return explanation; }
        }

        private string cashTypeValue;
        public string CashTypeValue
        {
            get { return cashTypeValue; }
        }

        private int cashCount;
        public int CashCount
        {
            get { return cashCount; }
        }

        private string cashSum;
        public string CashSum
        {
            get { return cashSum; }
        }

        internal KioskCashBoxCountTotal(int kioskId
                              , string explanation
                              , string cashTypeValue
                              , int cashCount
                              , string cashSum)
        {
            this.kioskId = kioskId;
            this.explanation = explanation;
            this.cashTypeValue = cashTypeValue;
            this.cashCount = cashCount;
            this.cashSum = cashSum;
        }
    }
}