﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class KioskReferenceSystem
    {
        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string institutionName;
        public string InstitutionName
        {
            get { return institutionName; }
        }

        private string referenceNo;
        public string ReferenceNo
        {
            get { return referenceNo; }
        }


        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

        private string status;
        public string Status
        {
            get { return status; }
        }

        private string explanation;
        public string Explanation
        {
            get { return explanation; }
        }

        private string createdUser;
        public string CreatedUser
        {
            get { return createdUser; }
        }

        private string id;
        public string ID
        {
            get { return id; }
        }


        internal KioskReferenceSystem(
            string id
            ,string kioskName
           // , string institutionName
            , string insertionDate
            , string status
            , string explanation
            , string createdUser
            , string referenceNo

           )
        {
            this.id = id;
            this.kioskName = kioskName;
           // this.institutionName = institutionName;
            this.insertionDate = insertionDate;
            this.createdUser = createdUser;
            this.status = status;
            this.explanation = explanation;
            this.referenceNo = referenceNo;
        }
    }






    public class KioskGetReferenceSystem
    {
        private string kioskId;
        public string KioskId
        {
            get { return kioskId; }
        }

        private string institutionId;
        public string InstitutionId
        {
            get { return institutionId; }
        }

        private string referenceNo;
        public string ReferenceNo
        {
            get { return referenceNo; }
        }


        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

        private string status;
        public string Status
        {
            get { return status; }
        }

        private string explanation;
        public string Explanation
        {
            get { return explanation; }
        }

        private string userId;
        public string UserId
        {
            get { return userId; }
        }

        private string id;
        public string Id
        {
            get { return id; }
        }

        private string desmerId;
        public string DesmerId
        {
            get { return desmerId; }
        }


        public KioskGetReferenceSystem()
        {

        }
        internal KioskGetReferenceSystem(
              string id
            , string kioskId
            , string institutionId
            , string insertionDate
            , string status
            , string explanation
            , string userId
            , string referenceNo
            , string desmerId
           )
        {
            this.id = id;
            this.kioskId = kioskId;
            this.institutionId = institutionId;
            this.insertionDate = insertionDate;
            this.status = status;
            this.explanation = explanation;
            this.userId = userId;
            this.referenceNo = referenceNo;
            this.desmerId = desmerId;
        }

        internal KioskGetReferenceSystem(
             string id
           , string kioskId
           , string insertionDate
           , string status
           , string explanation
           , string userId
           , string referenceNo
           , string desmerId
          )
        {
            this.id = id;
            this.kioskId = kioskId;
            this.insertionDate = insertionDate;
            this.status = status;
            this.explanation = explanation;
            this.userId = userId;
            this.referenceNo = referenceNo;
            this.desmerId = desmerId;
        }
    }






}
