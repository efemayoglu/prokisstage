﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class EmptyKioskName
    {
        private int emptyKioskId;
        public int EmptyKioskId
        {
            get { return emptyKioskId; }
        }

        private int id;
        public int Id
        {
            get { return id; }
        }

        private string name;
        public string Name
        {
            get { return name; }
        }

        internal EmptyKioskName(int id
            , string name
            , int emptyKioskId)
        {
            this.id = id;
            this.name = name;
            this.emptyKioskId = emptyKioskId;
        }

    }
}
