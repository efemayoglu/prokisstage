﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class KioskReport
    {
        private int kioskId;
        public int KioskId
        {
            get { return kioskId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }


        private string txnCount;
        public string TxnCount
        {
            get { return txnCount; }
        }

        private string generatedAskiAmount;
        public string GeneratedAskiAmount
        {
            get { return generatedAskiAmount; }
        }

        private string usedAskiAmount;
        public string UsedAskiAmount
        {
            get { return usedAskiAmount; }
        }

        private string generatedAskiCount;
        public string GeneratedAskiCount
        {
            get { return generatedAskiCount; }
        }

        private string usedAskiCount;
        public string UsedAskiCount
        {
            get { return usedAskiCount; }
        }

        private string receivedAmount;
        public string ReceivedAmount
        {
            get { return receivedAmount; }
        }

        private string kioskEmptyAmount;
        public string KioskEmptyAmount
        {
            get { return kioskEmptyAmount; }
        }

        private string earnedMoney;
        public string EarnedMoney
        {
            get { return earnedMoney; }
        }

        private string paraUstu;
        public string ParaUstu
        {
            get { return paraUstu; }
        }

        private string successTxnCount;
        public string SuccessTxnCount
        {
            get { return successTxnCount; }
        }

        internal KioskReport(int kioskId
                           , string kioskName
                           , string txnCount
                           , string generatedAskiAmount
                           , string usedAskiAmount
                           , string generatedAskiCount
                           , string usedAskiCount
                           , string receivedAmount
                           //, string kioskEmptyAmount
                           , string earnedMoney
                           , string paraUstu
                           , string successTxnCount)
        {
            this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.txnCount = txnCount;
            this.generatedAskiAmount = generatedAskiAmount;
            this.usedAskiAmount = usedAskiAmount;
            this.generatedAskiCount = generatedAskiCount;
            this.usedAskiCount = usedAskiCount;
            this.receivedAmount = receivedAmount;
            //this.kioskEmptyAmount = kioskEmptyAmount;
            this.earnedMoney = earnedMoney;
            this.paraUstu = paraUstu;
            this.successTxnCount = successTxnCount;
        }

        internal KioskReport(int kioskId
                         , string kioskName
                         , string txnCount
                         , string generatedAskiAmount
                         , string usedAskiAmount
                         , string generatedAskiCount
                         , string usedAskiCount
                         , string receivedAmount
                         , string kioskEmptyAmount
                         , string earnedMoney
                         , string paraUstu
                         , string successTxnCount)
        {
            this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.txnCount = txnCount;
            this.generatedAskiAmount = generatedAskiAmount;
            this.usedAskiAmount = usedAskiAmount;
            this.generatedAskiCount = generatedAskiCount;
            this.usedAskiCount = usedAskiCount;
            this.receivedAmount = receivedAmount;
            this.kioskEmptyAmount = kioskEmptyAmount;
            this.earnedMoney = earnedMoney;
            this.paraUstu = paraUstu;
            this.successTxnCount = successTxnCount;
        }
    }
    public class KioskReports
    {
        private int kioskId;
        public int KioskId
        {
            get { return kioskId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string totalAmount;
        public string TotalAmount
        {
            get { return totalAmount; }
        }

        private string txnCount;
        public string TxnCount
        {
            get { return txnCount; }
        }

        private string generatedAskiAmount;
        public string GeneratedAskiAmount
        {
            get { return generatedAskiAmount; }
        }

        private string usedAskiAmount;
        public string UsedAskiAmount
        {
            get { return usedAskiAmount; }
        }

        private string generatedAskiCount;
        public string GeneratedAskiCount
        {
            get { return generatedAskiCount; }
        }

        private string usedAskiCount;
        public string UsedAskiCount
        {
            get { return usedAskiCount; }
        }

        private string receivedAmount;
        public string ReceivedAmount
        {
            get { return receivedAmount; }
        }

        //private decimal kioskEmptyAmount;
        //public decimal KioskEmptyAmount
        //{
        //    get { return kioskEmptyAmount; }
        //}

        private string earnedMoney;
        public string EarnedMoney
        {
            get { return earnedMoney; }
        }

        private string paraUstu;
        public string ParaUstu
        {
            get { return paraUstu; }
        }

        private string successTxnCount;
        public string SuccessTxnCount
        {
            get { return successTxnCount; }
        }

        private string instutionAmount;
        public string InstutionAmount
        {
            get { return instutionAmount; }
        }

        private string totalAmountCreditCard;
        public string TotalAmountCreditCard
        {
            get { return totalAmountCreditCard; }
        }

        private string creditCardAmount;
        public string CreditCardAmount
        {
            get { return creditCardAmount; }
        }

        private string commisionAmount;
        public string CommisionAmount
        {
            get { return commisionAmount; }
        }

        private string className;
        public string ClassName
        {
            get { return className; }
        }

        internal KioskReports(int kioskId
                           , string kioskName
                           , string txnCount
                           , string generatedAskiAmount
                           , string usedAskiAmount
                           , string generatedAskiCount
                           , string usedAskiCount
                           , string receivedAmount
                            //, string kioskEmptyAmount
                           , string earnedMoney
                           , string paraUstu
                           , string successTxnCount
                           , string totalAmount
                           , string instutionAmount
                           , string creditCardAmount
                           , string commisionAmount
                           , string className
                           )
        {
            this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.txnCount = txnCount;
            this.generatedAskiAmount = generatedAskiAmount;
            this.usedAskiAmount = usedAskiAmount;
            this.generatedAskiCount = generatedAskiCount;
            this.usedAskiCount = usedAskiCount;
            this.receivedAmount = receivedAmount;
            //this.kioskEmptyAmount = kioskEmptyAmount;
            this.earnedMoney = earnedMoney;
            this.paraUstu = paraUstu;
            this.successTxnCount = successTxnCount;
            this.totalAmount = totalAmount;
            this.instutionAmount = instutionAmount;
            this.creditCardAmount = creditCardAmount;
            this.commisionAmount = commisionAmount;
            this.className = className;
            
        }

       
    }
}