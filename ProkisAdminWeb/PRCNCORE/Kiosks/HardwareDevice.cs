﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class HardwareDevice
    { 
        private Int64 deviceId;
        public Int64 DeviceId
        {
            get { return deviceId; }
        }
        private string deviceName;
        public string DeviceName
        {
            get { return deviceName; }
        }

        private string port;
        public string Port
        {
            get { return port; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        internal HardwareDevice(Int64 deviceId
            , string deviceName
            , string port
            , string kioskName)
        {
            this.deviceId = deviceId;
            this.kioskName = kioskName;
            this.deviceName = deviceName;
            this.port = port;
        }
    }
}
