﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class HardwareDeviceCollection : ReadOnlyCollection<HardwareDevice>
    {
        internal HardwareDeviceCollection()
            : base(new List<HardwareDevice>())
        {

        }

        internal void Add(HardwareDevice item)
        {
            if (item != null && !this.Contains(item))
            {
                this.Items.Add(item);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (HardwareDevice item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("Devices", jArray);

            return jObjectDis;
        }

    }
}

