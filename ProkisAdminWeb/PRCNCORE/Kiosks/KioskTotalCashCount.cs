﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class KioskTotalCashCount
    {
        private string cashTypeName;
        public string CashTypeName
        {
            get { return cashTypeName; }
        }

        private int cashCount;
        public int CashCount
        {
            get { return cashCount; }
        }

        internal KioskTotalCashCount(string cashTypeName
                              , int cashCount
		                    //, string cashTypeValue
                                )
        {
           
            this.cashTypeName = cashTypeName;
            this.cashCount = cashCount;
	        //this.cashTypeValue = cashTypeValue;
        }
    }
}
