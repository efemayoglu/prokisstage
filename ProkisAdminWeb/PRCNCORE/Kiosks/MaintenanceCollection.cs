﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class PhoneNumberCollection : ReadOnlyCollection<PhoneNumber>
    {
        internal PhoneNumberCollection()
            : base(new List<PhoneNumber>())
        {

        }

        internal void Add(PhoneNumber item)
        {
            if (item != null && !this.Contains(item))
            {
                this.Items.Add(item);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (PhoneNumber item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("PhoneNumber", jArray);

            return jObjectDis;
        }

    }
    public class MaintenanceCollection : ReadOnlyCollection<Maintenance>
    {
        internal MaintenanceCollection()
            : base(new List<Maintenance>())
        {

        }

        internal void Add(Maintenance item)
        {
            if (item != null && !this.Contains(item))
            {
                this.Items.Add(item);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (Maintenance item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("Maintenance", jArray);

            return jObjectDis;
        }

    }

    
}

