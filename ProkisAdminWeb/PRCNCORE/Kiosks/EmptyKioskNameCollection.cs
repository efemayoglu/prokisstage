﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class EmptyKioskNameCollection : ReadOnlyCollection<EmptyKioskName>
    {
        internal EmptyKioskNameCollection()
            : base(new List<EmptyKioskName>())
        {

        }

        internal void Add(EmptyKioskName kioskName)
        {
            if (kioskName != null && !this.Contains(kioskName))
            {
                this.Items.Add(kioskName);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (EmptyKioskName item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("EmptyKioskNames", jArray);

            return jObjectDis;
        }

    }
}

