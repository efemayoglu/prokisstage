﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class KioskFillCashBox
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private int kioskId;
        public int KioskId
        {
            get { return kioskId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string expertUserId;
        public string ExpertUserId
        {
            get { return expertUserId; }
        }

        private string expertUserName;
        public string ExpertUserName
        {
            get { return expertUserName; }
        }


        private string updatedDate;
        public string UpdatedDate
        {
            get { return updatedDate; }
        }

        private string amount;
        public string Amount
        {
            get { return amount; }
        }

        private string className;
        public string ClassName
        {
            get { return className; }
        }

        internal KioskFillCashBox(int id
                              , int kioskId
                              , string kioskName
                              , string expertUserId
                              , string expertUserName
                              , string updatedDate
                              , string amount
                              , string className)
        {
            this.id = id;
            this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.expertUserId = expertUserId;
            this.expertUserName = expertUserName;
            this.updatedDate = updatedDate;
            this.amount = amount;
            this.className = className;
        }
    }
}
