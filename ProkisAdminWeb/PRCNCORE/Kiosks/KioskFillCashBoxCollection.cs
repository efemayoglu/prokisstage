﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class KioskFillCashBoxCollection : ReadOnlyCollection<KioskFillCashBox>
    {
        internal KioskFillCashBoxCollection()
            : base(new List<KioskFillCashBox>())
        {

        }

        internal void Add(KioskFillCashBox kiosk)
        {
            if (kiosk != null && !this.Contains(kiosk))
            {
                this.Items.Add(kiosk);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (KioskFillCashBox item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("KioskFillCashBox", jArray);

            return jObjectDis;
        }

    }
}

