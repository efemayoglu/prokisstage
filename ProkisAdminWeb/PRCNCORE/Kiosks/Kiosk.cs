﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class Kiosk
    {
        private int kioskId;
        public int KioskId
        {
            get { return kioskId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string kioskAddress;
        public string KioskAddress
        {
            get { return kioskAddress; }
        }

        private string kioskIp;
        public string KioskIp
        {
            get { return kioskIp; }
        }

        private int kioskStatusId;
        public int KioskStatusId
        {
            get { return kioskStatusId; }
        }

        private string kioskStatusName;
        public string KioskStatusName
        {
            get { return kioskStatusName; }
        }

        private string kioskInsertionDate;
        public string KioskInsertionDate
        {
            get { return kioskInsertionDate; }
        }

        private string latitude;
        public string Latitude
        {
            get { return latitude; }
        }
        private int bankNameId;
        public int BankNameId
        {
            get { return bankNameId; }
        }

        private string bankName;
        public string BankName
        {
            get { return bankName; }
        }

        private string longitude;
        public string Longitude
        {
            get { return longitude; }
        }

        private int cityId;
        public int CityId
        {
            get { return cityId; }
        }

        private string cityName;
        public string CityName
        {
            get { return cityName; }
        }


        private string districtName;
        public string DistrictName
        {
            get { return districtName; }
        }



        private int districtId;
        public int DistrictId
        {
            get { return districtId; }
        }



        private string neighborhoodName;
        public string NeighborhoodName
        {
            get { return neighborhoodName; }
        }


        private int neighborhoodId;
        public int NeighborhoodId
        {
            get { return neighborhoodId; }
        }

        private int regionId;
        public int RegionId
        {
            get { return regionId; }
        }

        private string regionName;
        public string RegionName
        {
            get { return regionName; }
        }

        private string kioskTypeName;
        public string KioskTypeName
        {
            get { return kioskTypeName; }
        }

        private int kioskTypeId;
        public int KioskTypeId
        {
            get { return kioskTypeId; }
        }

        private string usageFee;
        public string UsageFee
        {
            get { return usageFee; }
        }

        private string appVersion;
        public string AppVersion
        {
            get { return appVersion; }
        }

        private string lastAppAliveTime;
        public string LastAppAliveTime
        {
            get { return lastAppAliveTime; }
        }

        private int capacity;
        public int Capacity
        {
            get { return capacity; }
        }

        private string cashBoxAcceptorTypeName;
        public string CashBoxAcceptorTypeName
        {
            get { return cashBoxAcceptorTypeName; }
        }

        private string explanation;
        public string Explanation
        {
            get { return explanation; }
        }

        private string cashBoxAcceptorTypeId;
        public string CashBoxAcceptorTypeId
        {
            get { return cashBoxAcceptorTypeId; }
        }

        private string accomplishedTransactions;
        public string AccomplishedTransactions
        {
            get { return accomplishedTransactions; }
        }


        private string connectionType;
        public string ConnectionType
        {
            get { return connectionType; }
        }

        private string cardReaderType;
        public string CardReaderType
        {
            get { return cardReaderType; }
        }

        private int connectionTypeId;
        public int ConnectionTypeId
        {
            get { return connectionTypeId; }
        }

        private int cardReaderTypeId;
        public int CardReaderTypeId
        {
            get { return cardReaderTypeId; }
        }

        private string institutionName;
        public string InstitutionName
        {
            get { return institutionName; }
        }


        private int institutionId;
        public int InstitutionId
        {
            get { return institutionId; }
        }

        private string riskClass;
        public string RiskClass
        {
            get { return riskClass; }
        }

        private bool isCreditCard;
        public bool IsCreditCard
        {
            get { return isCreditCard; }
        }

        private bool isMobilePayment;
        public bool IsMobilePayment
        {
            get { return isMobilePayment; }
        }

        private string appAliveDurationTime;
        public string AppAliveDurationTime
        {
            get { return appAliveDurationTime; }
        }
        private bool isInvoiceActive;
        public bool IsInvoiceActive
        {
            get { return isInvoiceActive; }
        }


        private bool isPrepaidActive;
        public bool IsPrepaidActive
        {
            get { return isPrepaidActive; }
        }


        private bool isExtendedScreenActive;
        public bool IsExtendedScreenActive
        {
            get { return isExtendedScreenActive; }
        }
        /*
         IsInvoiceActive       
         IsPrepaidActive       
         IsExtendedScreenActive
             */

        private string makbuzStatus;
        public string MakbuzStatus
        {
            get { return makbuzStatus; }
        }

        private string jcmStatus;
        public string JCMStatus
        {
            get { return jcmStatus; }
        }

        private string className;
        public string ClassName
        {
            get { return className; }
            set { className = value; }
        }

           //  ,      string cameraIp, byte channelNumber, string userName, string password
        //

        private string cameraIp;
        public string CameraIp
        {
            get { return cameraIp; }
            set { cameraIp = value; }
        }

        private byte channelNumber;
        public byte ChannelNumber
        {
            get { return channelNumber; }
            set { channelNumber = value; }
        }

        private string userName;
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        private string password;
        public string Password
        {
            get { return password; }
            set { password = value; }
        }


        private string stackRatio;
        public string StackRatio
        {
            get { return stackRatio; }
            set { stackRatio = value; }
        }



        internal Kiosk(int kioskId
            , string kioskName
            , string kioskAddress
            , string kioskIp
            , int kioskStatusId
            , string kioskStatusName
            , string kioskInsertionDate
            , string latitude
            , string longitude
            , int cityId
            , string cityName
            , int regionId
            , string regionName
            , int kioskTypeId
           
            , string kioskTypeName
            , int institutionId
            , string institutionName
            , int connectionTypeId
            , string connectionType
            , int cardReaderTypeId
            , string cardReaderType
            , string riskClass
            , string usageFee
            , string appVersion
            , string lastAppAliveTime
            , string explanation
            , int capacity
            , string cashBoxAcceptorTypeName
            , string cashBoxAcceptorTypeId
            , string accomplishedTransactions
            , bool isCreditCard
            , bool isMobilePayment
            , string appAliveDurationTime
            , string makbuzStatus
            , string jcmStatus
            , string className
            , int bankNameId
            , string bankName
            , bool isInvoiceActive
            , bool isPrepaidActive
            , bool isExtendedScreenActive
             ,      string cameraIp, byte channelNumber, string userName, string password,
            string stackRatio,string districtName,
            string neighborhoodName,
            int districtId, int neighborhoodId
            /*
                   , dataRow["IsInvoiceActive"].ToString()
            , dataRow["IsPrepaidActive"].ToString()
            , dataRow["IsExtendedScreenActive"].ToString()
             */

            )
        {
            this.districtId = districtId;
            this.neighborhoodId = neighborhoodId;
            this.districtName = districtName;
            this.neighborhoodName = neighborhoodName;
            this.stackRatio = stackRatio;
            this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.kioskAddress = kioskAddress;
            this.kioskIp = kioskIp;
            this.kioskStatusId = kioskStatusId;
            this.kioskStatusName = kioskStatusName;
            this.kioskInsertionDate = kioskInsertionDate;
            this.latitude = latitude;
            this.longitude = longitude;
            this.cityId = cityId;
            this.cityName = cityName;
            this.regionId = regionId;
            this.regionName = regionName;
            this.kioskTypeName = kioskTypeName;
            this.kioskTypeId = kioskTypeId;
            this.institutionName = institutionName;
            this.usageFee = usageFee;
            this.appVersion = appVersion;
            this.lastAppAliveTime = lastAppAliveTime;
            this.capacity = capacity;
            this.cashBoxAcceptorTypeName = cashBoxAcceptorTypeName;
            this.explanation = explanation;
            this.cashBoxAcceptorTypeId = cashBoxAcceptorTypeId;
            this.accomplishedTransactions = accomplishedTransactions;
            this.connectionType = connectionType;
            this.cardReaderType = cardReaderType;
            this.connectionTypeId = connectionTypeId;
            this.cardReaderTypeId = cardReaderTypeId;
            this.institutionId = institutionId;
            this.riskClass = riskClass;
            this.isCreditCard = isCreditCard;
            this.isMobilePayment = isMobilePayment;
            this.appAliveDurationTime = appAliveDurationTime;
            this.makbuzStatus = makbuzStatus;
            this.jcmStatus = jcmStatus;
            this.bankNameId = bankNameId;
            this.bankName = bankName;
            this.className = className;
            this.isInvoiceActive = isInvoiceActive;
            this.isPrepaidActive = isPrepaidActive ;
            this.isExtendedScreenActive = isExtendedScreenActive;

            this.cameraIp = cameraIp;
            this.channelNumber = channelNumber;
            this.userName = userName;
            this.password = password;

        }

        internal Kiosk(int kioskId
            , string kioskName
            , string kioskAddress
            , string kioskIp
            , int kioskStatusId
            , string kioskStatusName
            , string kioskInsertionDate
            , string latitude
            , string longitude
            , int cityId
            , string cityName
            , int regionId
            , string regionName
            , int kioskTypeId

            , string kioskTypeName
            , int institutionId
            , string institutionName
            , int connectionTypeId
            , string connectionType
            , int cardReaderTypeId
            , string cardReaderType
            , string riskClass
            , string usageFee
            , string appVersion
            , string lastAppAliveTime
            , string explanation
            , int capacity
            , string cashBoxAcceptorTypeName
            , string cashBoxAcceptorTypeId
            , string accomplishedTransactions
            , bool isCreditCard
            , bool isMobilePayment
            , string appAliveDurationTime
            , string makbuzStatus
            , string jcmStatus
            , string className
            , int bankNameId
            , string bankName
            , bool isInvoiceActive
            , bool isPrepaidActive
            , bool isExtendedScreenActive
             , string cameraIp, byte channelNumber, string userName, string password 
    
            /*
                   , dataRow["IsInvoiceActive"].ToString()
            , dataRow["IsPrepaidActive"].ToString()
            , dataRow["IsExtendedScreenActive"].ToString()
             */

            )
        {
       
            this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.kioskAddress = kioskAddress;
            this.kioskIp = kioskIp;
            this.kioskStatusId = kioskStatusId;
            this.kioskStatusName = kioskStatusName;
            this.kioskInsertionDate = kioskInsertionDate;
            this.latitude = latitude;
            this.longitude = longitude;
            this.cityId = cityId;
            this.cityName = cityName;
            this.regionId = regionId;
            this.regionName = regionName;
            this.kioskTypeName = kioskTypeName;
            this.kioskTypeId = kioskTypeId;
            this.institutionName = institutionName;
            this.usageFee = usageFee;
            this.appVersion = appVersion;
            this.lastAppAliveTime = lastAppAliveTime;
            this.capacity = capacity;
            this.cashBoxAcceptorTypeName = cashBoxAcceptorTypeName;
            this.explanation = explanation;
            this.cashBoxAcceptorTypeId = cashBoxAcceptorTypeId;
            this.accomplishedTransactions = accomplishedTransactions;
            this.connectionType = connectionType;
            this.cardReaderType = cardReaderType;
            this.connectionTypeId = connectionTypeId;
            this.cardReaderTypeId = cardReaderTypeId;
            this.institutionId = institutionId;
            this.riskClass = riskClass;
            this.isCreditCard = isCreditCard;
            this.isMobilePayment = isMobilePayment;
            this.appAliveDurationTime = appAliveDurationTime;
            this.makbuzStatus = makbuzStatus;
            this.jcmStatus = jcmStatus;
            this.bankNameId = bankNameId;
            this.bankName = bankName;
            this.className = className;
            this.isInvoiceActive = isInvoiceActive;
            this.isPrepaidActive = isPrepaidActive;
            this.isExtendedScreenActive = isExtendedScreenActive;

            this.cameraIp = cameraIp;
            this.channelNumber = channelNumber;
            this.userName = userName;
            this.password = password;

        }
    }

    public class KioskDispenserError
    {
        private int kioskId;
        public int KioskId
        {
            get { return kioskId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }


        private string error;
        public string Error
        {
            get { return error; }
        }

        private string failPeriod;
        public string FailPeriod
        {
            get { return failPeriod; }
        }

        private string lastSuccessDate;
        public string LastSuccessDate
        {
            get { return lastSuccessDate; }
        }

        private string lastFailDate;
        public string LastFailDate
        {
            get { return lastFailDate; }
        }

        private string institutionName;
        public string InstitutionName
        {
            get { return institutionName; }
        }


        internal KioskDispenserError(int kioskId
            , string kioskName
            , string institutionName
            , string error
            , string failPeriod
            , string lastSuccessDate
            , string lastFailDate
            )
        {
            this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.institutionName = institutionName;
            this.error = error;
            this.failPeriod = failPeriod;
            this.lastSuccessDate = lastSuccessDate;
            this.lastFailDate = lastFailDate;
        }
    }
}