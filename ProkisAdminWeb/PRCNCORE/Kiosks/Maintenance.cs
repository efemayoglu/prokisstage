﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class Maintenance
    {
        private string maintenanceId;
        public string MaintenanceId
        {
            get { return maintenanceId; }
        }
        private string maintenanceDate;
        public string MaintenanceDate
        {
            get { return maintenanceDate; }
        }
        private string message;
        public string Message
        {
            get { return message; }
        }
        private string maintenanceUser;
        public string MaintenanceUser
        {
            get { return maintenanceUser; }
        }
        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        internal Maintenance(string maintenanceId
            , string maintenanceDate
            , string maintenanceUser
            , string kioskName
            , string message)
        {
            this.maintenanceId = maintenanceId;
            this.kioskName = kioskName;
            this.maintenanceDate = maintenanceDate;
            this.maintenanceUser = maintenanceUser;
            this.message = message;
        }
    }

    public class PhoneNumber
    {
        private string phoneNumber;
        public string GetPhoneNumber
        {
            get { return phoneNumber; }

        }
        internal PhoneNumber(string phoneNumber)
        {
            this.phoneNumber = phoneNumber;
        }
    }
}
