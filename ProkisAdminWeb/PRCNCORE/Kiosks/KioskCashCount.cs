﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Kiosks
{
    public class KioskCashCount
    {
        private int kioskId;
        public int KioskId
        {
            get { return kioskId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string cashTypeName;
        public string CashTypeName
        {
            get { return cashTypeName; }
        }

        private int cashCount;
        public int CashCount
        {
            get { return cashCount; }
        }

        private string cashTypeValue;
        public string CashTypeValue
        {
            get { return cashTypeValue; }
        }

        private string cashSum;
        public string CashSum
        {
            get { return cashSum; }
        }


        internal KioskCashCount(int kioskId
                              , string kioskName
                              , string cashTypeName
                              , int cashCount
		                      , string cashTypeValue
                              , string cashSum
                                )
        {
            this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.cashTypeName = cashTypeName;
            this.cashCount = cashCount;
	        this.cashTypeValue = cashTypeValue;
            this.cashSum = cashSum;
        }
    }
}