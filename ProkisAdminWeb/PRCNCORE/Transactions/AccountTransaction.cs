﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Transactions
{
    public class AccountTransaction
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private string transactionDate;
        public string TransactionDate
        {
            get { return transactionDate; }
        }

        private string description;
        public string Description
        {
            get { return description; }
        }

        private string transactionType;
        public string TransactionType
        {
            get { return transactionType; }
        }

        private string amount;
        public string Amount
        {
            get { return amount; }
        }

        private string senderNo;
        public string SenderNo
        {
            get { return senderNo; }
        }

        private string senderName;
        public string SenderName
        {
            get { return senderName; }
        }

        private string senderBalance;
        public string SenderBalance
        {
            get { return senderBalance; }
        }

        private string recieverNo;
        public string RecieverNo
        {
            get { return recieverNo; }
        }

        private string recieverName;
        public string RecieverName
        {
            get { return recieverName; }
        }

        private string recieverBalance;
        public string RecieverBalance
        {
            get { return recieverBalance; }
        }

        private string transactionPrice;
        public string TransactionPrice
        {
            get { return transactionPrice; }
        }

        private string transactionPlatform;
        public string TransactionPlatform
        {
            get { return transactionPlatform; }
        }

        private int actionSatusId;
        public int ActionSatusId
        {
            get { return actionSatusId; }
        }

        private string actionSatus;
        public string ActionSatus
        {
            get { return actionSatus; }
        }

        private int fraudTypeId;
        public int FraudTypeId
        {
            get { return fraudTypeId; }
        }

        private string fraudType;
        public string FraudType
        {
            get { return fraudType; }
        }

        private string fraudApproveTime;
        public string FraudApproveTime
        {
            get { return fraudApproveTime; }
        }

        private string fraudApproveUser;
        public string FraudApproveUser
        {
            get { return fraudApproveUser; }
        }

        private string className;
        public string ClassName
        {
            get { return className; }
        }

        internal AccountTransaction(int id
                                  , string transactionDate
                                  , string description
                                  , string transactionType
                                  , string amount
                                  , string senderNo
                                  , string senderBalance
                                  , string recieverNo
                                  //, string recieverBalance
                                  , string transactionPrice
                                  , string transactionPlatform
                                  , int actionSatusId
                                  , string actionSatus
                                  , int fraudTypeId
                                  , string fraudType
                                  , string fraudApproveTime
                                  , string fraudApproveUser
                                  , string className)
        {
            this.id = id;
            this.transactionDate = transactionDate;
            this.description = description;
            this.transactionType = transactionType;
            this.amount = amount;
            this.senderNo = senderNo;
            this.senderBalance = senderBalance;
            this.recieverNo = recieverNo;
            //this.recieverBalance = recieverBalance;
            this.transactionPrice = transactionPrice;
            this.transactionPlatform = transactionPlatform;
            this.actionSatusId = actionSatusId;
            this.actionSatus = actionSatus;
            this.fraudTypeId = fraudTypeId;
            this.fraudType = fraudType;
            this.fraudApproveTime = fraudApproveTime;
            this.fraudApproveUser = fraudApproveUser;
            this.className = className;

        }

        internal AccountTransaction(int id
                                  , string transactionDate
                                  , string description
                                  , string transactionType
                                  , string amount
                                  , string senderNo
                                  , string senderName
                                  , string senderBalance
                                  , string recieverNo
                                  , string recieverName
                                  , string recieverBalance
                                  , string transactionPrice
                                  , string transactionPlatform)
        {
            this.id = id;
            this.transactionDate = transactionDate;
            this.description = description;
            this.transactionType = transactionType;
            this.amount = amount;
            this.senderNo = senderNo;
            this.senderName = senderName;
            this.senderBalance = senderBalance;
            this.recieverNo = recieverNo;
            this.recieverName = recieverName;
            this.recieverBalance = recieverBalance;
            this.transactionPrice = transactionPrice;
            this.transactionPlatform = transactionPlatform;
        }
    }
    public class LastTransactionDetails
    {
        private Int64 id;
        public Int64 Id
        {
            get { return id; }
        }

        private string transactionDate;
        public string TransactionDate
        {
            get { return transactionDate; }
        }

        private string description;
        public string Description
        {
            get { return description; }
        }

        private string amount;
        public string Amount
        {
            get { return amount; }
        }

        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
        }

        private string completeStatus;
        public string CompleteStatus
        {
            get { return completeStatus; }
        }

        private string creditCardNumber;
        public string CreditCardNumber
        {
            get { return creditCardNumber; }
        }

        private string creditCardOwner;
        public string CreditCardOwner
        {
            get { return creditCardOwner; }
        }

        internal LastTransactionDetails(Int64 id
                                  , string transactionDate
                                  , string description
                                  , string amount
                                  , string customerName
                                  , string completeStatus
                                  , string creditCardNumber
                                  , string creditCardOwner)
        {
            this.id = id;
            this.transactionDate = transactionDate;
            this.description = description;
            this.amount = amount;
            this.customerName = customerName;
            this.completeStatus = completeStatus;
            this.creditCardNumber = creditCardNumber;
            this.creditCardOwner = creditCardOwner;


        }
    }
}
