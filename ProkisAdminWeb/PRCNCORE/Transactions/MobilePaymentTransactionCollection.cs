﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Backend;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Transactions
{
    public class MobilePaymentTransactionCollection : ReadOnlyCollection<MobilePaymentTransaction>
    {
        internal MobilePaymentTransactionCollection()
            : base(new List<MobilePaymentTransaction>())
        {

        }

        internal void Add(MobilePaymentTransaction transaction)
        {
            if (transaction != null && !this.Contains(transaction))
            {
                this.Items.Add(transaction);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (MobilePaymentTransaction item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("MobilePaymentTransaction", jArray);

            return jObjectDis;
        }
    }
}
