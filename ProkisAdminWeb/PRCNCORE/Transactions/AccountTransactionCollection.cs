﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Transactions
{
    public class AccountTransactionCollection: ReadOnlyCollection<AccountTransaction>
    {
        internal AccountTransactionCollection()
            : base(new List<AccountTransaction>())
        {

        }

        internal void Add(AccountTransaction transaction)
        {
            if (transaction != null && !this.Contains(transaction))
            {
                this.Items.Add(transaction);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (AccountTransaction item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("Transactions", jArray);

            return jObjectDis;
        }

    }
    public class LastTransactionDetailsCollection : ReadOnlyCollection<LastTransactionDetails>
    {
        internal LastTransactionDetailsCollection()
            : base(new List<LastTransactionDetails>())
        {

        }

        internal void Add(LastTransactionDetails transaction)
        {
            if (transaction != null && !this.Contains(transaction))
            {
                this.Items.Add(transaction);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (LastTransactionDetails item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("Transactions", jArray);

            return jObjectDis;
        }

    }
}
