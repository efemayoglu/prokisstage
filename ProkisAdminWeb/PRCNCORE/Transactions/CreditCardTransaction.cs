﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Transactions
{
    public class CreditCardTransaction
    {
        private int transactionId;
        public int TransactionId
        {
            get { return transactionId; }
        }

        private int kioskId;
        public int KioskId
        {
            get { return kioskId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string transactionDate;
        public string TransactionDate
        {
            get { return transactionDate; }
        }

        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
        }

        private string aboneNo;
        public string AboneNo
        {
            get { return aboneNo; }
        }

        private string creditCardNo;
        public string CreditCardNo
        {
            get { return creditCardNo; }
        }

        private string creditCardOwner;
        public string CreditCardOwner
        {
            get { return creditCardOwner; }
        }

        private string instutionName;
        public string InstutionName
        {
            get { return instutionName; }
        }

        private string rrn;
        public string RRN
        {
            get { return rrn; }
        }

        private string transactionAmountCell;
        public string TransactionAmountCell
        {
            get { return transactionAmountCell; }
        }

        private string muhasebeStatus;
        public string MuhasebeStatus
        {
            get { return muhasebeStatus; }
        }

        private string approvedCode;
        public string ApprovedCode
        {
            get { return approvedCode; }
        }

        private string batch;
        public string Batch
        {
            get { return batch; }
        }

        private string className;
        public string ClassName
        {
            get { return className; }
        }

        internal CreditCardTransaction(int transactionId
            //, int kioskId
            , string kioskName
            , string transactionDate
            , string customerName
            //, string aboneNo
            , string creditCardNo
            , string creditCardOwner
            , string instutionName
            , string rrn
            , string transactionAmountCell
            , string muhasebeStatus
            , string approvedCode
            , string batch
            , string className
            )
        {
            this.transactionId = transactionId;
            //this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.transactionDate = transactionDate;
            this.customerName = customerName;
            //this.aboneNo = aboneNo;
            this.creditCardNo = creditCardNo;
            this.creditCardOwner = creditCardOwner;
            this.instutionName = instutionName;
            this.rrn = rrn;
            this.transactionAmountCell = transactionAmountCell;
            this.muhasebeStatus = muhasebeStatus;
            this.approvedCode = approvedCode;
            this.batch = batch;
            this.className = className;
        }
    }

    public class CustomerContacts
    {
        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

        private string phoneNumber;
        public string PhoneNumber
        {
            get { return phoneNumber; }
        }

        private string aboneNo;
        public string AboneNo
        {
            get { return aboneNo; }
        }

        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
        }

        private string institutionName;
        public string InstitutionName
        {
            get { return institutionName; }
        }

        private string className;
        public string ClassName
        {
            get { return className; }
        }

       

        internal CustomerContacts(string insertionDate
            , string phoneNumber
            , string aboneNo
            , string customerName
            , string institutionName
            , string className
            )
        {
            this.insertionDate = insertionDate;
            this.phoneNumber = phoneNumber;
            this.aboneNo = aboneNo;
            this.customerName = customerName;
            this.institutionName = institutionName;
            this.className = className;
        }
    }
}
