﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE
{
    public class FailedTransaction
    {
        private int transactionId;
        public int TransactionId
        {
            get { return transactionId; }
        }

        private int kioskId;
        public int KioskId
        {
            get { return kioskId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string transactionDate;
        public string TransactionDate
        {
            get { return transactionDate; }
        }

        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
        }

        private string aboneNo;
        public string AboneNo
        {
            get { return aboneNo; }
        }

        private string processStatus;
        public string ProcessStatus
        {
            get { return processStatus; }
        }

        private string cardId;
        public string CardId
        {
            get { return cardId; }
        }

        private string instutionName;
        public string InstutionName
        {
            get { return instutionName; }
        }

        private string operationTypeName;
        public string OperationTypeName
        {
            get { return operationTypeName; }
        }

        internal FailedTransaction(int transactionId
            , int kioskId
            , string kioskName
            , string transactionDate
            , string customerName
            , string aboneNo
            , string cardId
            , string processStatus
            , string instutionName
            , string operationTypeName)
        {
            this.transactionId = transactionId;
            this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.transactionDate = transactionDate;
            this.customerName = customerName;
            this.aboneNo = aboneNo;
            this.cardId = cardId;
            this.processStatus = processStatus;
            this.instutionName = instutionName;
            this.operationTypeName = operationTypeName;
        }
    }
}