﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Transactions
{
    public class AccountTransactionProkisCollection : ReadOnlyCollection<AccountTransactionProkis>
    {
        internal AccountTransactionProkisCollection()
            : base(new List<AccountTransactionProkis>())
        {

        }

        internal void Add(AccountTransactionProkis transaction)
        {
            if (transaction != null && !this.Contains(transaction))
            {
                this.Items.Add(transaction);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (AccountTransactionProkis item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("Transactions", jArray);

            return jObjectDis;
        }

    }
}
