﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Transactions
{
    public class TransactionDetail
    {
        private int transactionId;
        public int TransactionId
        {
            get { return transactionId; }
        }

        private string processStatus;
        public string ProcessStatus
        {
            get { return processStatus; }
        }

        private string processDescription;
        public string ProcessDescription
        {
            get { return processDescription; }
        }


        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string transactionDate;
        public string TransactionDate
        {
            get { return transactionDate; }
        }


        internal TransactionDetail(int transactionId
            , string processStatus
            , string processDescription
            , string transactionDate
            , string kioskName)
        {
            this.transactionId = transactionId;
            this.processStatus = processStatus;
            this.processDescription = processDescription;
            this.transactionDate = transactionDate;
            this.kioskName = kioskName;
        }
    }
}