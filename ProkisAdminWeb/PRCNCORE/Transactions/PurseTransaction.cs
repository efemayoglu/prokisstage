﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Transactions
{
    public class PurseTransaction
    {        
        private int transactionId;
        public int TransactionId
        {
            get { return transactionId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string transactionDate;
        public string TransactionDate
        {
            get { return transactionDate; }
        }

        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
        }

        private string customerNo;
        public string CustomerNo
        {
            get { return customerNo; }
        }

        private string transactionKind;
        public string TransactionKind
        {
            get { return transactionKind; }
        }

        internal PurseTransaction(int transactionId
                                , string customerName
                                , string customerNo
                                , string transactionDate
                                , string kioskName
                                , string transactionKind)
        {
            this.transactionId = transactionId;
            this.customerName = customerName;
            this.kioskName = kioskName;
            this.transactionDate = transactionDate;
            this.customerNo = customerNo;
            this.transactionKind = transactionKind;
        }
    }
}
