﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Transactions
{
     public class TransactionConditionCollection: ReadOnlyCollection<TransactionCondition>
    {
         internal TransactionConditionCollection()
             : base(new List<TransactionCondition>())
        {

        }

         internal void Add(TransactionCondition transaction)
        {
            if (transaction != null && !this.Contains(transaction))
            {
                this.Items.Add(transaction);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (TransactionCondition item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("TransactionConditions", jArray);

            return jObjectDis;
        }

    }

     public class CancelTransactionCollection : ReadOnlyCollection<CancelTransaction>
     {
         internal CancelTransactionCollection()
             : base(new List<CancelTransaction>())
         {

         }

         internal void Add(CancelTransaction transaction)
         {
             if (transaction != null && !this.Contains(transaction))
             {
                 this.Items.Add(transaction);
             }
         }

         public JObject GetJSON()
         {
             JObject jObjectDis = new JObject();
             JArray jArray = new JArray();

             foreach (CancelTransaction item in this)
             {
                 JObject jo = new JObject();

                 string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                 JObject x = JObject.Parse(a);
                 jArray.Add(x);
             }

             jObjectDis.Add("CancelTransaction", jArray);

             return jObjectDis;
         }

     }
}