﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Transactions
{
    public class MobilePaymentTransaction
    {
         private int transactionId;
        public int TransactionId
        {
            get { return transactionId; }
        }

        private int kioskId;
        public int KioskId
        {
            get { return kioskId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string transactionDate;
        public string TransactionDate
        {
            get { return transactionDate; }
        }

        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
        }

        private string aboneNo;
        public string AboneNo
        {
            get { return aboneNo; }
        }

        private string mobileNo;
        public string MobileNo
        {
            get { return mobileNo; }
        }

        private string status;
        public string Status
        {
            get { return status; }
        }

        private string instutionName;
        public string InstutionName
        {
            get { return instutionName; }
        }

        private string rrn;
        public string RRN
        {
            get { return rrn; }
        }

        private string transactionAmount;
        public string TransactionAmount
        {
            get { return transactionAmount; }
        }


        private string muhasebeStatus;
        public string MuhasebeStatus
        {
            get { return muhasebeStatus; }
        }

        private string className;
        public string ClassName
        {
            get { return className; }
            set { className = value; }
        }


        private string rnd;
        public string Rnd
        {
            get { return rnd; }
            set { rnd = value; }
        }


        internal MobilePaymentTransaction(int transactionId
            //, int kioskId
            , string kioskName
            , string transactionDate
            , string customerName
            //, string aboneNo
            , string mobileNo
            , string status
            , string instutionName
            //, string rrn
            , string transactionAmount
            , string muhasebeStatus
            , string aboneNo
            , string rnd
            , string className
            )
        {
            this.transactionId = transactionId;
            //this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.transactionDate = transactionDate;
            this.customerName = customerName;
            //this.aboneNo = aboneNo;
            this.mobileNo = mobileNo;
            this.status = status;
            this.instutionName = instutionName;
            //this.rrn = rrn;
            this.transactionAmount = transactionAmount;
            this.muhasebeStatus = muhasebeStatus;
            this.className = className;
            this.aboneNo = aboneNo;
            this.rnd = rnd;

        }
    }
}
