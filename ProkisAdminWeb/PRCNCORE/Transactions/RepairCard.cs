﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Transactions
{
    public class RepairCard
    {
        private int transactionId;
        public int TransactionId
        {
            get { return transactionId; }
        }

        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
        }

        private string anaKredi;
        public string AnaKredi
        {
            get { return anaKredi; }
        }


        private string yedekKredi;
        public string YedekKredi
        {
            get { return yedekKredi; }
        }

        private string transactionDate;
        public string TransactionDate
        {
            get { return transactionDate; }
        }

        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

        private string userName;
        public string UserName
        {
            get { return userName; }
        }

        private string status;
        public string Status
        {
            get { return status; }
        }

        private string isDeleted;
        public string IsDeleted
        {
            get { return isDeleted; }
        }

        private string repairDate;
        public string RepairDate
        {
            get { return repairDate; }
        }

        private string repairedTransactionId;
        public string RepairedTransactionId
        {
            get { return repairedTransactionId; }
        }


        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string repairedId;
        public string RepairedId
        {
            get { return repairedId; }
        }


        internal RepairCard(string customerName
            , int transactionId
            , string kioskName
            , string anaKredi
            , string yedekKredi
            , string insertionDate
            , string transactionDate
            , string userName
            , string status
            , string isDeleted
            , string repairDate
            , string repairedTransactionId
            , string repairedId)
        {
            this.customerName = customerName;
            this.transactionId = transactionId;
            this.kioskName = kioskName;
            this.anaKredi = anaKredi;
            this.yedekKredi = yedekKredi;
            this.insertionDate = insertionDate;
            this.transactionDate = transactionDate;
            this.userName = userName;
            this.status = status;
            this.isDeleted = isDeleted;
            this.repairDate = repairDate;
            this.repairedTransactionId = repairedTransactionId;
            this.repairedId = repairedId;
        }
    }
}
