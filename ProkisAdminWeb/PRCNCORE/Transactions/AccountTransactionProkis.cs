﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Transactions
{
    public class AccountTransactionProkis
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private string transactionDate;
        public string TransactionDate
        {
            get { return transactionDate; }
        }

        private string receivedAmount;
        public string ReceivedAmount
        {
            get { return receivedAmount; }
        }

        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
        }

        private string aboneNo;
        public string AboneNo
        {
            get { return aboneNo; }
        }

        internal AccountTransactionProkis(int id
                                  , string transactionDate
                                  , string receivedAmount
                                  , string customerName
                                  , string aboneNo)
        {
            this.id = id;
            this.transactionDate = transactionDate;
            this.receivedAmount = receivedAmount;
            this.customerName = customerName;
            this.aboneNo = aboneNo;
        }
    }
}

