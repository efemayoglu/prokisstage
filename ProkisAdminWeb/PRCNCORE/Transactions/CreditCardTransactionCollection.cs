﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Backend;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Transactions
{
    public class CreditCardTransactionCollection : ReadOnlyCollection<CreditCardTransaction>
    {
        internal CreditCardTransactionCollection()
            : base(new List<CreditCardTransaction>())
        {

        }

        internal void Add(CreditCardTransaction transaction)
        {
            if (transaction != null && !this.Contains(transaction))
            {
                this.Items.Add(transaction);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (CreditCardTransaction item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("CreditCardTransactions", jArray);

            return jObjectDis;
        }

    }

    public class GetCustomerContactsCollection : ReadOnlyCollection<CustomerContacts>
    {
        internal GetCustomerContactsCollection()
            : base(new List<CustomerContacts>())
        {

        }

        internal void Add(CustomerContacts transaction)
        {
            if (transaction != null && !this.Contains(transaction))
            {
                this.Items.Add(transaction);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (CustomerContacts item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("CustomerContacts", jArray);

            return jObjectDis;
        }

    }

}
