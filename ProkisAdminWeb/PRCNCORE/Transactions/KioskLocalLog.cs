﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Transactions
{
    public class KioskLocalLog
    {
        private string transactionId;
        public string TransactionId
        {
            get { return transactionId; }
        }

        private int kioskId;
        public int KioskId
        {
            get { return kioskId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

        private string name;
        public string Name
        {
            get { return name; }
        }

        private string customerNo;
        public string CustomerNo
        {
            get { return customerNo; }
        }

        private string logStatus;
        public string LogStatus
        {
            get { return logStatus; }
        }

        private string status;
        public string Status
        {
            get { return status; }
        }

        private string instutionName;
        public string InstutionName
        {
            get { return instutionName; }
        }

        private string operationTypeName;
        public string OperationTypeName
        {
            get { return operationTypeName; }
        }

        private string userName;
        public string UserName
        {
            get { return userName; }
        }

        private string id;
        public string Id
        {
            get { return id; }
        }

        private string logStatusText;
        public string LogStatusText
        {
            get { return logStatusText; }
        }

        internal KioskLocalLog(string transactionId
           // , int kioskId
            , string kioskName
            , string insertionDate
            , string name
            , string customerNo
            , string userName
            , string logStatus
            , string status
            , string id
            , string logStatusText)
        {
            this.transactionId = transactionId;
            //this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.insertionDate = insertionDate;
            this.name = name;
            this.customerNo = customerNo;
            this.logStatus = logStatus;
            this.status = status;
            this.userName = userName;
            this.id = id;
            this.logStatusText = logStatusText;
        }
    }

    public class KioskLocalLogDetail
    {
        private string id;
        public string Id
        {
            get { return id; }
        }

        private string recordInsertionDate;
        public string RecordInsertionDate
        {
            get { return recordInsertionDate; }
        }

        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

        private string detail;
        public string Detail
        {
            get { return detail; }
        }

        private string totalAmount;
        public string TotalAmount
        {
            get { return totalAmount; }
        }

        private string status;
        public string Status
        {
            get { return status; }
        }


        internal KioskLocalLogDetail(string id
            , string insertionDate
            , string recordInsertionDate
            , string detail
            , string totalAmount
            , string status)
        {
            this.id = id;
            this.insertionDate = insertionDate;
            this.recordInsertionDate = recordInsertionDate;
            this.detail = detail;
            this.totalAmount = totalAmount;
            this.status = status;
        }
    }
}
