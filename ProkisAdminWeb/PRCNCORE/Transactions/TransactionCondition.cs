﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Transactions
{
    public class TransactionCondition
    {
        private Int64 id;
        public Int64 Id
        {
            get { return id; }
        }

        private Int64 transactionId;
        public Int64 TransactionId
        {
            get { return transactionId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string transactionDate;
        public string TransactionDate
        {
            get { return transactionDate; }
        }

        private string instutionName;
        public string InstutionName
        {
            get { return instutionName; }
        }

        private string totalAmount;
        public string TotalAmount
        {
            get { return totalAmount; }
        }

        private string cashAmount;
        public string CashAmount
        {
            get { return cashAmount; }
        }

        private string codeAmount;
        public string CodeAmount
        {
            get { return codeAmount; }
        }

        private string instutionAmount;
        public string InstutionAmount
        {
            get { return instutionAmount; }
        }

        private string calculatedChange;
        public string CalculatedChange
        {
            get { return calculatedChange; }
        }

        private string givenChange;
        public string GivenChange
        {
            get { return givenChange; }
        }

        private string usageFee;
        public string UsageFee
        {
            get { return usageFee; }
        }

        private string paidFaturaCount;
        public string PaidFaturaCount
        {
            get { return paidFaturaCount; }
        }
        private string transationStatus;
        public string TransationStatus
        {
            get { return transationStatus; }
        }

        private string creditCardAmount;
        public string CreditCardAmount
        {
            get { return creditCardAmount; }
        }

        private string commisionAmount;
        public string CommisionAmount
        {
            get { return commisionAmount; }
        }


        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
        }

        //PayTypeName
        private string payTypeName;
        public string PayTypeName
        {
            get { return payTypeName; }
        }

        private string reverseRecord;
        public string ReverseRecord
        {
            get { return reverseRecord; }
        }


        private string className;
        public string ClassName
        {
            get { return className; }
        }


        internal TransactionCondition(Int64 id
            , Int64 transactionId
            , string kioskName
            , string transactionDate
            , string instutionName
            , string totalAmount
            , string cashAmount
            , string codeAmount
            , string creditCardAmount
            , string commisionAmount
            , string instutionAmount
            , string calculatedChange
            , string givenChange
            , string usageFee
            , string paidFaturaCount
            , string transationStatus
            , string customerName
            , string payTypeName
            , string reverseRecord
            , string className)
        {
            this.id = id;
            this.transactionId = transactionId;
            this.kioskName = kioskName;
            this.transactionDate = transactionDate;
            this.instutionName = instutionName;
            this.totalAmount = totalAmount;
            this.cashAmount = cashAmount;
            this.codeAmount = codeAmount;
            this.creditCardAmount = creditCardAmount;
            this.instutionAmount = instutionAmount;
            this.commisionAmount = commisionAmount;
            this.calculatedChange = calculatedChange;
            this.givenChange = givenChange;
            this.usageFee = usageFee;
            this.paidFaturaCount = paidFaturaCount;
            this.transationStatus = transationStatus;
            this.customerName = customerName;
            this.payTypeName = payTypeName;
            this.reverseRecord = reverseRecord;
            this.className = className;
        }
    }

    public class CancelTransaction
    {
        private Int64 id;
        public Int64 Id
        {
            get { return id; }
        }
        private string customerNo;
        public string CustomerNo
        {
            get { return customerNo; }
        }

        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
        }

        private string billDate;
        public string BillDate
        {
            get { return billDate; }
        }

        private string billNumber;
        public string BillNumber
        {
            get { return billNumber; }
        }


        internal CancelTransaction(Int64 Id
            , string CustomerNo
            , string CustomerName
            , string BillDate
            , string BillNumber)
        {
            this.id = id;
            this.customerNo = CustomerNo;
            this.customerName = CustomerName;
            this.billDate = BillDate;
            this.billNumber = BillNumber;
        }
    }
}
