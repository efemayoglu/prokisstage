﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Transactions
{
    public class KioskLocalLogCollection: ReadOnlyCollection<KioskLocalLog>
    {
        internal KioskLocalLogCollection()
            : base(new List<KioskLocalLog>())
        {

        }

        internal void Add(KioskLocalLog transaction)
        {
            if (transaction != null && !this.Contains(transaction))
            {
                this.Items.Add(transaction);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (KioskLocalLog item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("KioskLocalLog", jArray);

            return jObjectDis;
        }

    }

    public class KioskLocalLogDetailCollection : ReadOnlyCollection<KioskLocalLogDetail>
    {
        internal KioskLocalLogDetailCollection()
            : base(new List<KioskLocalLogDetail>())
        {

        }

        internal void Add(KioskLocalLogDetail transaction)
        {
            if (transaction != null && !this.Contains(transaction))
            {
                this.Items.Add(transaction);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (KioskLocalLogDetail item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("KioskLocalLogDetail", jArray);

            return jObjectDis;
        }

    }
}
