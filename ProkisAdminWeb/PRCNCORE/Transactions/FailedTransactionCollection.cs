﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Backend;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Transactions
{
    public class FailedTransactionCollection : ReadOnlyCollection<FailedTransaction>
    {
        internal FailedTransactionCollection()
            : base(new List<FailedTransaction>())
        {

        }

        internal void Add(FailedTransaction transaction)
        {
            if (transaction != null && !this.Contains(transaction))
            {
                this.Items.Add(transaction);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (FailedTransaction item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("FailedTransactions", jArray);

            return jObjectDis;
        }

    }
}
