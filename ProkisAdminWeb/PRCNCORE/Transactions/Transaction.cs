﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE
{
    public class Transaction
    {
        private int transactionId;
        public int TransactionId
        {
            get { return transactionId; }
        }

        private int kioskId;
        public int KioskId
        {
            get { return kioskId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string transactionDate;
        public string TransactionDate
        {
            get { return transactionDate; }
        }

        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
        }

        private string aboneNo;
        public string AboneNo
        {
            get { return aboneNo; }
        }

        private string processStatus;
        public string ProcessStatus
        {
            get { return processStatus; }
        }

        private string cardId;
        public string CardId
        {
            get { return cardId; }
        }

        private string instutionName;
        public string InstutionName
        {
            get { return instutionName; }
        }

        private string operationTypeName;
        public string OperationTypeName
        {
            get { return operationTypeName; }
        }

        internal Transaction(int transactionId
            , int kioskId
            , string kioskName
            , string transactionDate
            , string customerName
            , string aboneNo
            , string cardId
            , string processStatus
            , string instutionName
            , string operationTypeName)
        {
            this.transactionId = transactionId;
            this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.transactionDate = transactionDate;
            this.customerName = customerName;
            this.aboneNo = aboneNo;
            this.cardId = cardId;
            this.processStatus = processStatus;
            this.instutionName = instutionName;
            this.operationTypeName = operationTypeName;
        }
    }

    public class TransactionDetails
    {
        public string transactionId { get; set; }
        public string kioskId { get; set; }
        public string kioskName { get; set; }
        public string customerName { get; set; }
        public string aboneNo { get; set; }
        public string totalAmount { get; set; }
        public string cashAmount { get; set; }
        public string codeAmount { get; set; }
        public string creditCardAmount { get; set; }
        public string institutionAmount { get; set; }
        public string usageAmount { get; set; }
        public string processType { get; set; }
        public string cassette1Value { get; set; }
        public string cassette2Value { get; set; }
        public string cassette3Value { get; set; }
        public string cassette4Value { get; set; }
        public string cassette1Reject { get; set; }
        public string cassette2Reject { get; set; }
        public string cassette3Reject { get; set; }
        public string cassette4Reject { get; set; }
        public string cassette1Exit { get; set; }
        public string cassette2Exit { get; set; }
        public string cassette3Exit { get; set; }
        public string cassette4Exit { get; set; }
        public string cassette1Total { get; set; }
        public string cassette2Total { get; set; }
        public string cassette3Total { get; set; }
        public string cassette4Total { get; set; }
        public string cassetteAllTotal { get; set; }
        public string reverseRecord { get; set; }

        public TransactionDetails(string transactionId
                                , string kioskId 
                                , string totalAmount 
                                , string cashAmount 
                                , string codeAmount 
                                , string creditCardAmount 
                                , string institutionAmount 
                                , string usageAmount 
                                , string processType 
                                , string cassette1Value 
                                , string cassette2Value
                                , string cassette3Value 
                                , string cassette4Value 
                                , string cassette1Reject
                                , string cassette2Reject
                                , string cassette3Reject
                                , string cassette4Reject
                                , string cassette1Exit 
                                , string cassette2Exit 
                                , string cassette3Exit 
                                , string cassette4Exit 
                                , string cassette1Total 
                                , string cassette2Total 
                                , string cassette3Total 
                                , string cassette4Total 
                                , string cassetteAllTotal,string reverseRecord)

        {
            this.kioskId =kioskId;
            this.kioskName =kioskName;
            this.customerName=customerName;
            this.aboneNo =aboneNo;
            this.totalAmount =totalAmount;
            this.cashAmount =cashAmount;
            this.codeAmount =codeAmount;
            this.creditCardAmount=creditCardAmount;
            this.institutionAmount=institutionAmount;
            this.usageAmount=usageAmount;
            this.processType =processType;
            this.cassette1Value =cassette1Value;
            this.cassette2Value=cassette2Value;
            this.cassette3Value =cassette3Value;
            this.cassette4Value =cassette4Value;
            this.cassette1Reject=cassette1Reject;
            this.cassette2Reject=cassette2Reject;
            this.cassette3Reject=cassette3Reject;
            this.cassette4Reject=cassette4Reject;
            this.cassette1Exit =cassette1Exit;
            this.cassette2Exit =cassette2Exit;
            this.cassette3Exit =cassette3Exit;
            this.cassette4Exit =cassette4Exit;
            this.cassette1Total =cassette1Total;
            this.cassette2Total =cassette2Total;
            this.cassette3Total =cassette3Total;
            this.cassette4Total =cassette4Total;
            this.cassetteAllTotal = cassetteAllTotal;
            this.reverseRecord = reverseRecord;
        }
    }

    /*
    public class TransactionDetails2
    {
        private int transactionId;
        public int TransactionId
        {
            get { return transactionId; }
        }

        private int kioskId;
        public int KioskId
        {
            get { return kioskId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string transactionDate;
        public string TransactionDate
        {
            get { return transactionDate; }
        }

        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
        }

        private string aboneNo;
        public string AboneNo
        {
            get { return aboneNo; }
        }

        private string totalAmount;
        public string TotalAmount
        {
            get { return totalAmount; }
        }

        private string cashAmount;
        public string CashAmount
        {
            get { return cashAmount; }
        }

        private string codeAmount;
        public string CodeAmount
        {
            get { return codeAmount; }
        }

        private string creditCardAmount;
        public string CreditCardAmount
        {
            get { return creditCardAmount; }
        }

        private string institutionAmount;
        public string InstitutionAmount
        {
            get { return institutionAmount; }
        }

        private string usageAmount;
        public string UsageAmount
        {
            get { return usageAmount; }
        }

        private string processType;
        public string ProcessType
        {
            get { return processType; }
        }

        private string cassette1Value;
        public string Cassette1Value
        {
            get { return cassette1Value; }
        }

        private string cassette2Value;
        public string Cassette2Value
        {
            get { return cassette2Value; }
        }

        private string cassette3Value;
        public string Cassette3Value
        {
            get { return cassette3Value; }
        }

        private string cassette4Value;
        public string Cassette4Value
        {
            get { return cassette4Value; }
        }

        private string cassette1Reject;
        public string Cassette1Reject
        {
            get { return cassette1Reject; }
        }

        private string cassette2Reject;
        public string Cassette2Reject
        {
            get { return cassette2Reject; }
        }

        private string cassette3Reject;
        public string Cassette3Reject
        {
            get { return cassette3Reject; }
        }

        internal TransactionDetails2(int transactionId
            , int kioskId
            , string kioskName
            , string transactionDate
            , string customerName
            , string aboneNo
            , string cardId
            , string processStatus
            , string instutionName
            , string operationTypeName)
        {
            this.transactionId = transactionId;
            this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.transactionDate = transactionDate;
            this.customerName = customerName;
            this.aboneNo = aboneNo;
            this.cardId = cardId;
            this.processStatus = processStatus;
            this.instutionName = instutionName;
            this.operationTypeName = operationTypeName;
        }
    
    }*/
}