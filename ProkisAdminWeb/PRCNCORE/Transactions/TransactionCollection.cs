﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Backend;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Transactions
{
    public class TransactionCollection: ReadOnlyCollection<Transaction>
    {
        internal TransactionCollection()
            : base(new List<Transaction>())
        {

        }

        internal void Add(Transaction transaction)
        {
            if (transaction != null && !this.Contains(transaction))
            {
                this.Items.Add(transaction);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (Transaction item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("Transactions", jArray);

            return jObjectDis;
        }

    }

    public class TransactionDetailCollectionGroup : ReadOnlyCollection<TransactionDetails>
    {
        internal TransactionDetailCollectionGroup()
            : base(new List<TransactionDetails>())
        {

        }

        internal void Add(TransactionDetails transaction)
        {
            if (transaction != null && !this.Contains(transaction))
            {
                this.Items.Add(transaction);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (TransactionDetails item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("TransactionsDetails", jArray);

            return jObjectDis;
        }

    }
}
