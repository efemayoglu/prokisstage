﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class KioskCommisionSetting
    {       
        private int id;
        public int Id
        {
            get { return id; }
        }



        private string cashCommission;
        public string CashCommission
        {
            get { return cashCommission; }
        }

        private string mobileCommission;
        public string MobileCommission
        {
            get { return mobileCommission; }
        }

        private string creditCommission;
        public string CreditCommission
        {
            get { return creditCommission; }
        }

        private string yellowAlarm;
        public string YellowAlarm
        {
            get { return yellowAlarm; }
        }

        private string redAlarm;
        public string RedAlarm
        {
            get { return redAlarm; }
        }

        private string instutionId;
        public string InstutionId
        {
            get { return instutionId; }
        }


        private string insName;
        public string InsName
        {
            get { return insName; }
        }

  

        internal KioskCommisionSetting( 
            
             string creditCommission
            , string mobileCommission
            , string cashCommission
            , string yellowAlarm
            , string redAlarm
            , string insName
            , string instutionId
          
            )
        {
            this.creditCommission = creditCommission;
            this.mobileCommission = mobileCommission;
            this.cashCommission = cashCommission;
            this.yellowAlarm = yellowAlarm;
            this.redAlarm = redAlarm;
            this.insName = insName;
            this.instutionId = instutionId;
           
            
        }
    }
}
