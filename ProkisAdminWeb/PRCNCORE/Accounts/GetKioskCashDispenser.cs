﻿using Newtonsoft.Json;
using PRCNCORE.Parser.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class GetKioskCashDispenser
    {
        [JsonProperty("KioskCashDispenser")]
        public   ParserKioskCashDispenser  ParserKioskCashDispenser { get; set; }
    }
}
