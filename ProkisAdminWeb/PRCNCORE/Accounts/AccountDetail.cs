﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class AccountDetail
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private int transactionId;
        public int TransactionId
        {
            get { return transactionId; }
        }

        private int accountRangeId;
        public int AccountRangeId
        {
            get { return accountRangeId; }
        }

        private string accountRangeName;
        public string AccountRangeName
        {
            get { return accountRangeName; }
        }

        private string denizbank;
        public string Denizbank
        {
            get { return denizbank; }
        }

        private string instutionMoney;
        public string InstutionMoney
        {
            get { return instutionMoney; }
        }

        private string merkez;
        public string Merkez
        {
            get { return merkez; }
        }

        private string askiCode;
        public string AskiCode
        {
            get { return askiCode; }
        }

        private string transactionDate;
        public string TransactionDate
        {
            get { return transactionDate; }
        }

        private string receivedAmount;
        public string ReceivedAmount
        {
            get { return receivedAmount; }
        }

        private int operationTypeId;
        public int OperationTypeId
        {
            get { return operationTypeId; }
        }

        private string operationTypeName;
        public string OperationTypeName
        {
            get { return operationTypeName; }
        }

        private string instutionName;
        public string InstutionName
        {
            get { return instutionName; }
        }

        internal AccountDetail(int id
            , int transactionId
            , int accountRangeId
            , string accountRangeName
            , string denizbank
            , string instutionMoney
            , string merkez
            , string moneyCode
            , string transactionDate
            , string receivedAmount
            , int operationTypeId
            , string operationTypeName
            , string instutionName)
        {
            this.id = id;
            this.transactionId = transactionId;
            this.accountRangeId = accountRangeId;
            this.accountRangeName = accountRangeName;
            this.denizbank = denizbank;
            this.instutionMoney = instutionMoney;
            this.merkez = merkez;
            this.askiCode = moneyCode;
            this.transactionDate = transactionDate;
            this.receivedAmount = receivedAmount;
            this.operationTypeId = operationTypeId;
            this.operationTypeName = operationTypeName;
            this.instutionName = instutionName;
        }

    }
}
