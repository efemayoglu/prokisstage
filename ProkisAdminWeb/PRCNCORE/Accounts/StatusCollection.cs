﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class StatusCollection : ReadOnlyCollection<Status>
    {
        internal StatusCollection()
            : base(new List<Status>())
        {

        }

        internal void Add(Status name)
        {
            if (name != null && !this.Contains(name))
            {
                this.Items.Add(name);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (Status item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("Status", jArray);

            return jObjectDis;
        }

    }

    
}

