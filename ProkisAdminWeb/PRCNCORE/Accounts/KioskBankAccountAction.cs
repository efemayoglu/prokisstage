﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class KioskBankAccountAction
    {
        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

        private string askiBalance;
        public string AskiBalance
        {
            get { return askiBalance; }
        }

        private string askiFeeBalance;
        public string AskiFeeBalance
        {
            get { return askiFeeBalance; }
        }

        private string bgazBalance;
        public string BgazBalance
        {
            get { return bgazBalance; }
        }

        private string bgazFeeBalance;
        public string BgazFeeBalance
        {
            get { return bgazFeeBalance; }
        }

        private string desmerBalance;
        public string DesmerBalance
        {
            get { return desmerBalance; }
        }



        internal KioskBankAccountAction(string insertionDate
            , string askiBalance
            , string askiFeeBalance
             , string bgazBalance
             , string bgazFeeBalance
             , string desmerBalance
            

           )
        {
            this.insertionDate = insertionDate;
            this.askiBalance = askiBalance;
            this.askiFeeBalance = askiFeeBalance;
            this.bgazBalance = bgazBalance;
            this.bgazFeeBalance = bgazFeeBalance;
            this.desmerBalance = desmerBalance;
                
        }
    }
}
