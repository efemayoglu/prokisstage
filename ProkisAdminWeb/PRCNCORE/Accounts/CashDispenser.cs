﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class CashDispenser
    {
        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }
 

        private string firstDenomNumber;
        public string FirstDenomNumber
        {
            get { return firstDenomNumber; }
        }

 
        private string secondDenomNumber;
        public string SecondDenomNumber
        {
            get { return secondDenomNumber; }
        }

 
        private string thirdDenomNumber;
        public string ThirdDenomNumber
        {
            get { return thirdDenomNumber; }
        }
 
        private string fourthDenomNumber;
        public string FourthDenomNumber
        {
            get { return fourthDenomNumber; }
        }
 
        private string fifthDenomNumber;
        public string FifthDenomNumber
        {
            get { return fifthDenomNumber; }
        }
 
        private string sixthDenomNumber;
        public string SixthDenomNumber
        {
            get { return sixthDenomNumber; }
        }
 
        private string seventhDenomNumber;
        public string SeventhDenomNumber
        {
            get { return seventhDenomNumber; }
        }
 
        private string eighthDenomNumber;
        public string EighthDenomNumber
        {
            get { return eighthDenomNumber; }
        }
 
        private string ninthDenomNumber;
        public string NinthDenomNumber
        {
            get { return ninthDenomNumber; }
        }
 
        private string tenthDenomNumber;
        public string TenthDenomNumber
        {
            get { return tenthDenomNumber; }
        }
 
        private string eleventhDenomNumber;
        public string EleventhDenomNumber
        {
            get { return eleventhDenomNumber; }
        }

        private string casette1Reject;
        public string Casette1Reject
        {
            get { return casette1Reject; }
        }

        private string casette2Reject;
        public string Casette2Reject
        {
            get { return casette2Reject; }
        }

        private string casette3Reject;
        public string Casette3Reject
        {
            get { return casette3Reject; }
        }

        private string casette4Reject;
        public string Casette4Reject
        {
            get { return casette4Reject; }
        }


        private string firstDenomValue;
        public string FirstDenomValue
        {
            get { return firstDenomValue; }
        }


        private string secondDenomValue;
        public string SecondDenomValue
        {
            get { return secondDenomValue; }
        }


        private string thirdDenomValue;
        public string ThirdDenomValue
        {
            get { return thirdDenomValue; }
        }

        private string fourthDenomValue;
        public string FourthDenomValue
        {
            get { return fourthDenomValue; }
        }

        private string fifthDenomValue;
        public string FifthDenomValue
        {
            get { return fifthDenomValue; }
        }

        private string sixthDenomValue;
        public string SixthDenomValue
        {
            get { return sixthDenomValue; }
        }

        private string seventhDenomValue;
        public string SeventhDenomValue
        {
            get { return seventhDenomValue; }
        }

        private string eighthDenomValue;
        public string EighthDenomValue
        {
            get { return eighthDenomValue; }
        }

        private string cassette1status;
        public string Cassette1Status
        {
            get { return cassette1status; }
        }

        private string cassette2status;
        public string Cassette2Status
        {
            get { return cassette2status; }
        }

        private string cassette3status;
        public string Cassette3Status
        {
            get { return cassette3status; }
        }

        private string cassette4status;
        public string Cassette4Status
        {
            get { return cassette4status; }
        }

        private string hopper1status;
        public string Hopper1Status
        {
            get { return hopper1status; }
        }

        private string hopper2status;
        public string Hopper2Status
        {
            get { return hopper2status; }
        }

        private string hopper3status;
        public string Hopper3Status
        {
            get { return hopper3status; }
        }

        private string hopper4status;
        public string Hopper4Status
        {
            get { return hopper4status; }
        }

        internal CashDispenser( 
            string kioskName
            , string firstDenomNumber
            , string secondDenomNumber
            , string thirdDenomNumber
            , string fourthDenomNumber
            , string fifthDenomNumber
            , string sixthDenomNumber
            , string seventhDenomNumber
            , string eighthDenomNumber
            , string casette1Reject
            , string casette2Reject
            , string casette3Reject
            , string casette4Reject
            , string firstDenomValue
            , string secondDenomValue
            , string thirdDenomValue
            , string fourthDenomValue
            , string fifthDenomValue
            , string sixthDenomValue
            , string seventhDenomValue
            , string eighthDenomValue
            , string cassette1Status
            , string cassette2Status
            , string cassette3Status
            , string cassette4Status
            , string hopper1Status
            , string hopper2Status
            , string hopper3Status
            , string hopper4Status
            
           )
        {
            this.kioskName = kioskName;
            this.firstDenomNumber = firstDenomNumber;
            this.secondDenomNumber = secondDenomNumber;
            this.thirdDenomNumber = thirdDenomNumber;
            this.fourthDenomNumber = fourthDenomNumber;
            this.fifthDenomNumber = fifthDenomNumber;
            this.sixthDenomNumber = sixthDenomNumber;
            this.seventhDenomNumber = seventhDenomNumber;
            this.eighthDenomNumber = eighthDenomNumber;
            this.casette1Reject = casette1Reject;
            this.casette2Reject = casette2Reject;
            this.casette3Reject = casette3Reject;
            this.casette4Reject = casette4Reject;
            this.firstDenomValue = firstDenomValue;
            this.secondDenomValue = secondDenomValue;
            this.thirdDenomValue = thirdDenomValue;
            this.fourthDenomValue = fourthDenomValue;
            this.fifthDenomValue = fifthDenomValue;
            this.sixthDenomValue = sixthDenomValue;
            this.seventhDenomValue = seventhDenomValue;
            this.eighthDenomValue = eighthDenomValue;
            this.cassette1status = cassette1Status;
            this.cassette2status = cassette2Status;
            this.cassette3status = cassette3Status;
            this.cassette4status = cassette4Status;
            this.hopper1status = hopper1Status;
            this.hopper2status = hopper2Status;
            this.hopper3status = hopper3Status;
            this.hopper4status = hopper4Status;
        }
    }
}
