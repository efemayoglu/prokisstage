﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class KioskControlCashNotification
    {
        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string institutionName;
        public string InstitutionName
        {
            get { return institutionName; }
        }

        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

        private string firstDenomNumber;
        public string FirstDenomNumber
        {
            get { return firstDenomNumber; }
        }

        private string secondDenomNumber;
        public string SecondDenomNumber
        {
            get { return secondDenomNumber; }
        }

        private string thirdDenomNumber;
        public string ThirdDenomNumber
        {
            get { return thirdDenomNumber; }
        }

        private string fourthDenomNumber;
        public string FourthDenomNumber
        {
            get { return fourthDenomNumber; }
        }


        private string fifthDenomNumber;
        public string FifthDenomNumber
        {
            get { return fifthDenomNumber; }
        }


        private string sixthDenomNumber;
        public string SixthDenomNumber
        {
            get { return sixthDenomNumber; }
        }


        private string seventhDenomNumber;
        public string SeventhDenomNumber
        {
            get { return seventhDenomNumber; }
        }

        private string eighthDenomValue;
 
        private string eighthDenomNumber;
        public string EighthDenomNumber
        {
            get { return eighthDenomNumber; }
        }

 
        private string ninthDenomNumber;
        public string NinthDenomNumber
        {
            get { return ninthDenomNumber; }
        }

 

        private string tenthDenomNumber;
        public string TenthDenomNumber
        {
            get { return tenthDenomNumber; }
        }

 
        private string eleventhDenomNumber;
        public string EleventhDenomNumber
        {
            get { return eleventhDenomNumber; }
        }

        private string createdrUser;
        public string CreatedUser
        {
            get { return createdrUser; }
        }


        private string userCashOut;
        public string UserCashOut
        {
            get { return userCashOut; }
        }


        private string cashOutDate;
        public string CashOutDate
        {
            get { return cashOutDate; }
        }

        private string sum;
        public string Sum
        {
            get { return sum; }
        }

        private string status;
        public string Status
        {
            get { return status; }
        }

        private int statusId;
        public int StatusId
        {
            get { return statusId; }
        }

        private string controlCashId;
        public string ControlCashId
        {
            get { return controlCashId; }
        }


        private string kioskId;
        public string KioskId
        {
            get { return kioskId; }
        }

        private string className;
        public string ClassName
        {
            get { return className; }
            set { className = value; }
        }

        internal KioskControlCashNotification(string kioskName
            , string institutionName
            , string insertionDate
             , string firstDenomNumber
             , string secondDenomNumber
             , string thirdDenomNumber
             , string fourthDenomNumber
             , string fifthDenomNumber
             , string sixthDenomNumber
             , string seventhDenomNumber
             , string eighthDenomNumber
             , string ninthDenomNumber
             , string tenthDenomNumber
             , string eleventhDenomNumber
             , string CreatedrUser
             , string UserCashOut
             , string CashOutDate
             , string Sum
             , string status
             , string controlCashId
             , string kioskId
             , int statusId
             , string className
           )
        {
            this.kioskName = kioskName;
            this.institutionName = institutionName;
            this.insertionDate = insertionDate;            
            this.firstDenomNumber = firstDenomNumber;          
            this.secondDenomNumber = secondDenomNumber;         
            this.thirdDenomNumber = thirdDenomNumber;
            this.fourthDenomNumber = fourthDenomNumber;
            this.fifthDenomNumber = fifthDenomNumber; 
            this.sixthDenomNumber = sixthDenomNumber;
            this.seventhDenomNumber = seventhDenomNumber;
            this.eighthDenomNumber = eighthDenomNumber;
            this.ninthDenomNumber = ninthDenomNumber;
            this.tenthDenomNumber = tenthDenomNumber;
            this.eleventhDenomNumber = eleventhDenomNumber;
            this.createdrUser = CreatedrUser;
            this.userCashOut = UserCashOut;
            this.cashOutDate = CashOutDate;
            this.sum = Sum;
            this.status = status;
            this.controlCashId = controlCashId;
            this.kioskId = kioskId;
            this.statusId = statusId;
            this.className = className;
        }
    }
}
