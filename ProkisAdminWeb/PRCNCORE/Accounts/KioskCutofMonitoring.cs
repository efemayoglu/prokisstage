﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class KioskCutofMonitoring
    {

        private string startDate;
        public string StartDate
        {
            get { return startDate; }
        }

        private string endDate;
        public string EndDate
        {
            get { return endDate; }
        }


        private string cutDuration;
        public string CutDuration
        {
            get { return cutDuration; }
        }

        private string institution;
        public string Institution
        {
            get { return institution; }
        }

        private string cutReason;
        public string CutReason
        {
            get { return cutReason; }
        }

        private string enteredUser;
        public string EnteredUser
        {
            get { return enteredUser; }
        }

        private string enteredDate;
        public string EnteredDate
        {
            get { return enteredDate; }
        }


        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string cutId;
        public string CutId
        {
            get { return cutId; }
        }

        private string kioskId;
        public string KioskId
        {
            get { return kioskId; }
        }

        private string institutionId;
        public string InstitutionId
        {
            get { return institutionId; }
        }

        internal KioskCutofMonitoring(string startDate
                                      , string endDate
                                      , string cutDuration
                                      , string kioskName
                                      , string institution
                                      , string cutReason
                                      , string enteredUser
                                      , string enteredDate
                                      , string cutId
                                      , string kioskId
                                      , string institutionId


           )
        {
            this.startDate = startDate;
            this.endDate = endDate;
            this.cutDuration = cutDuration;
            this.kioskName = kioskName;
            this.institution = institution;
            this.cutReason = cutReason;
            this.enteredUser = enteredUser;
            this.enteredDate = enteredDate;
            this.cutId = cutId;
            this.kioskId = kioskId;
            this.institutionId = institutionId;
        }
    }
}
