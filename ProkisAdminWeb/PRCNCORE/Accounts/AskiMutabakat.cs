﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class AskiSoapMutabakat
    {
        public string ISLEM_KODU { get; set; }
        public string MAKBUZ_NO { get; set; }
        public string SYSTARIH { get; set; }
        public string SATIS_TUTARI_YTL { get; set; }
    }
}
