﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class Mutabakat
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private string mutabakatType;
        public string MutabakatType
        {
            get { return mutabakatType; }
        }

        private string mutabakatDate;
        public string MutabakatDate
        {
            get { return mutabakatDate; }
        }

        private string mutabakatUser;
        public string MutabakatUser
        {
            get { return mutabakatUser; }
        }

        private string inputAmount;
        public string InputAmount
        {
            get { return inputAmount; }
        }

        private string outputAmount;
        public string OutputAmount
        {
            get { return outputAmount; }
        }


        private string institution;
        public string Institution
        {
            get { return institution; }
        }

        private decimal cashInflow;
        public decimal CashInflow
        {
            get { return cashInflow; }
        }

        private decimal institutionAmount;
        public decimal InstitutionAmount
        {
            get { return institutionAmount; }
        }


        private decimal activeCode;
        public decimal ActiveCode
        {
            get { return activeCode; }
        }

        private decimal usedCode;
        public decimal UsedCode
        {
            get { return usedCode; }
        }

        private string explanation;
        public string Explanation
        {
            get { return explanation; }
        }

        private string accountOwner;
        public string AccountOwner
        {
            get { return accountOwner; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string resultCount;
        public string ResultCount
        {
            get { return resultCount; }
        }

        private string kioskEmptyId;
        public string KioskEmptyId
        {
            get { return kioskEmptyId; }
        }

        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }
        private string amountDifference;
        public string AmountDifference
        {
            get { return amountDifference; }
        }

        private string codeInflow;
        public string CodeInflow
        {
            get { return codeInflow; }
        }
        //@kioskEmptyStatusId

        private string kioskEmptyStatusId;
        public string KioskEmptyStatusId
        {
            get { return kioskEmptyStatusId; }
        }

        private string status;
        public string Status
        {
            get { return status; }
        }

        private string statusId;
        public string StatusId
        {
            get { return statusId; }
        }

        private string accountDate;
        public string AccountDate
        {
            get { return accountDate; }
        }

        private string operatorName;
        public string OperatorName
        {
            get { return operatorName; }
        }
        private string referenceNo;
        public string ReferenceNo
        {
            get { return referenceNo; }
        }

        private string className;
        public string ClassName
        {
            get { return className; }
        }
    
        private string kioskEmptyStatusName;
        public string KioskEmptyStatusName
        {
            get { return kioskEmptyStatusName; }
        }

        internal Mutabakat(
            int id
            , string insertionDate
            , string status
            , string statusId
            , string mutabakatDate
            , string mutabakatUser
            , string explanation
            , string inputAmount
            , string outputAmount
            , string amountDifference
            , string institution
           // , decimal cashInflow
           // , string codeInflow
            //, decimal institutionAmount
            //, decimal activeCode
            //, decimal usedCode
            , string accountOwner
            , string kioskName
            , string resultCount
            , string kioskEmptyId
            , string accountDate
            , string operatorName
            , string referenceNo
            , string className
            )
        {
            this.id = id;
            this.insertionDate = insertionDate;
            this.status = status;
            this.statusId = statusId;
            this.mutabakatDate = mutabakatDate;
            this.mutabakatUser = mutabakatUser;
            this.explanation = explanation;
            this.inputAmount = inputAmount;
            this.outputAmount = outputAmount;
            this.amountDifference = amountDifference;
            this.institution = institution;
            //this.cashInflow = cashInflow;
            //this.codeInflow = codeInflow;
            //this.institutionAmount = institutionAmount;
            //this.activeCode = activeCode;
            //this.usedCode = usedCode;
            this.explanation = explanation;
            this.accountOwner = accountOwner;
            this.resultCount = resultCount;
            this.kioskName = kioskName;
            this.kioskEmptyId = kioskEmptyId;
            this.accountDate = accountDate;
            this.operatorName = operatorName;
            this.referenceNo = referenceNo;
            this.className = className;
        }



        internal Mutabakat(
         int id
         , string insertionDate
         , string status
         , string statusId
         , string mutabakatDate
         , string mutabakatUser
         , string explanation
         , string inputAmount
         , string outputAmount
         , string amountDifference
         , string institution
         // , decimal cashInflow
         // , string codeInflow
         //, decimal institutionAmount
         //, decimal activeCode
         //, decimal usedCode
         , string accountOwner
         , string kioskName
         , string resultCount
         , string kioskEmptyId
         , string accountDate
         , string operatorName
         , string referenceNo
         , string className
         , string kioskEmptyStatusId
         , string kioskEmptyStatusName

         )
        {
            this.id = id;
            this.insertionDate = insertionDate;
            this.status = status;
            this.statusId = statusId;
            this.mutabakatDate = mutabakatDate;
            this.mutabakatUser = mutabakatUser;
            this.explanation = explanation;
            this.inputAmount = inputAmount;
            this.outputAmount = outputAmount;
            this.amountDifference = amountDifference;
            this.institution = institution;
            //this.cashInflow = cashInflow;
            //this.codeInflow = codeInflow;
            //this.institutionAmount = institutionAmount;
            //this.activeCode = activeCode;
            //this.usedCode = usedCode;
            this.explanation = explanation;
            this.accountOwner = accountOwner;
            this.resultCount = resultCount;
            this.kioskName = kioskName;
            this.kioskEmptyId = kioskEmptyId;
            this.accountDate = accountDate;
            this.operatorName = operatorName;
            this.referenceNo = referenceNo;
            this.className = className;

            this.kioskEmptyStatusId = kioskEmptyStatusId;
            this.kioskEmptyStatusName = kioskEmptyStatusName;

        }
    }


    public class MutabakatReport
    {
        private int resultCount;
        public int ResultCount
        {
            get { return resultCount; }
        }

        private string manuelGeneratedUsedCode;
        public string ManuelGeneratedUsedCode
        {
            get { return manuelGeneratedUsedCode; }
        }

        private string manuelGeneratedActiveCode;
        public string ManuelGeneratedActiveCode
        {
            get { return manuelGeneratedActiveCode; }
        }

        private string kioskGeneratedKioskUsedCode;
        public string KioskGeneratedKioskUsedCode
        {
            get { return kioskGeneratedKioskUsedCode; }
        }

        private string inputAmount;
        public string InputAmount
        {
            get { return inputAmount; }
        }

        private string outputAmount;
        public string OutputAmount
        {
            get { return outputAmount; }
        }


        private string institution;
        public string Institution
        {
            get { return institution; }
        }

        private string cashAmount;
        public string CashAmount
        {
            get { return cashAmount; }
        }

        private string institutionAmount;
        public string InstitutionAmount
        {
            get { return institutionAmount; }
        }


        private string activeCode;
        public string ActiveCode
        {
            get { return activeCode; }
        }

        private string usedCode;
        public string UsedCode
        {
            get { return usedCode; }
        }

        private string explanation;
        public string Explanation
        {
            get { return explanation; }
        }

        private string kioskGeneratedActiveCode;
        public string KioskGeneratedActiveCode
        {
            get { return kioskGeneratedActiveCode; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }


        private string givenChangeAmount;
        public string GivenChangeAmount
        {
            get { return givenChangeAmount; }
        }

        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }
        private string amountDifference;
        public string AmountDifference
        {
            get { return amountDifference; }
        }

        private string codeAmount;
        public string CodeAmount
        {
            get { return codeAmount; }
        }


        private string calculatedChangeAmount;
        public string CalculatedChangeAmount
        {
            get { return calculatedChangeAmount; }
        }

        private string usageAmount;
        public string UsageAmount
        {
            get { return usageAmount; }
        }

        private string lastCashOutDate;
        public string LastCashOutDate
        {
            get { return lastCashOutDate; }
        }

        private string updatedDate;
        public string UpdatedDate
        {
            get { return updatedDate; }
        }

        private string anotherKioskGeneratedUsedCode;
        public string AnotherKioskGeneratedUsedCode
        {
            get { return anotherKioskGeneratedUsedCode; }
        }
        
        private string className;
        public string ClassName
        {
            get { return className; }
            set { className = value; }
        }


        internal MutabakatReport(
            int resultCount
            , string insertionDate
            , string kioskName
            , string updatedDate
            , string lastCashOutDate
            , string outputAmount
            , string inputAmount
            , string amountDifference
            , string institution
            , string cashAmount
            , string codeAmount
            , string institutionAmount
            , string usageAmount
            , string calculatedChangeAmount
            , string givenChangeAmount
            , string activeCode
            , string usedCode
            , string kioskGeneratedActiveCode
            , string kioskGeneratedKioskUsedCode
            , string manuelGeneratedActiveCode
            , string manuelGeneratedUsedCode
            , string anotherKioskGeneratedUsedCode
            , string className
            )
        {
            this.resultCount = resultCount;
            this.kioskName = kioskName;
            this.insertionDate = insertionDate;
            this.updatedDate = updatedDate;
            this.lastCashOutDate = lastCashOutDate;
            this.outputAmount = outputAmount;
            this.inputAmount = inputAmount;
            this.amountDifference = amountDifference;
            this.institution = institution;
            this.cashAmount = cashAmount;
            this.codeAmount = codeAmount;
            this.institutionAmount = institutionAmount;
            this.usageAmount = usageAmount;
            this.calculatedChangeAmount = calculatedChangeAmount;
            this.givenChangeAmount = givenChangeAmount;
            this.activeCode = activeCode;
            this.usedCode = usedCode;
            this.kioskGeneratedActiveCode = kioskGeneratedActiveCode;
            this.kioskGeneratedKioskUsedCode = kioskGeneratedKioskUsedCode;
            this.manuelGeneratedActiveCode = manuelGeneratedActiveCode;
            this.manuelGeneratedUsedCode = manuelGeneratedUsedCode;
            this.anotherKioskGeneratedUsedCode = anotherKioskGeneratedUsedCode;
            this.className=className;
        }
    }

}
