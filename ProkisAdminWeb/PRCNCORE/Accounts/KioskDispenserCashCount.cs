﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class KioskDispenserCashCount
    {
         private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string institutionName;
        public string InstitutionName
        {
            get { return institutionName; }
        }

      

        private string firstCassetteValue;
        public string FirstCassetteValue
        {
            get { return firstCassetteValue; }
        }

        private string firstCassetteNumber;
        public string FirstCassetteNumber
        {
            get { return firstCassetteNumber; }
        }

        private string secondCassetteValue;
        public string SecondCassetteValue
        {
            get { return secondCassetteValue; }
        }

        private string secondCassetteNumber;
        public string SecondCassetteNumber
        {
            get { return secondCassetteNumber; }
        }

        private string thirdCassetteValue;
        public string ThirdCassetteValue
        {
            get { return thirdCassetteValue; }
        }

        private string thirdCassetteNumber;
        public string ThirdCassetteNumber
        {
            get { return thirdCassetteNumber; }
        }

        private string fourthCassetteValue;
        public string FourthCassetteValue
        {
            get { return fourthCassetteValue; }
        }

        private string fourthCassetteNumber;
        public string FourthCassetteNumber
        {
            get { return fourthCassetteNumber; }
        }

        private string fifthCassetteValue;
        public string FifthCassetteValue
        {
            get { return fifthCassetteValue; }
        }

        private string fifthCassetteNumber;
        public string FifthCassetteNumber
        {
            get { return fifthCassetteNumber; }
        }


        private string sixthCassetteValue;
        public string SixthCassetteValue
        {
            get { return sixthCassetteValue; }
        }

        private string sixthCassetteNumber;
        public string SixthCassetteNumber
        {
            get { return sixthCassetteNumber; }
        }


        private string seventhCassetteValue;
        public string SeventhCassetteValue
        {
            get { return seventhCassetteValue; }
        }

        private string seventhCassetteNumber;
        public string SeventhCassetteNumber
        {
            get { return seventhCassetteNumber; }
        }

        private string eighthCassetteValue;
        public string EighthCassetteValue
        {
            get { return eighthCassetteValue; }
        }

        private string eighthCassetteNumber;
        public string EighthCassetteNumber
        {
            get { return eighthCassetteNumber; }
        }

        private string ninthCassetteValue;
        public string NinthCassetteValue
        {
            get { return ninthCassetteValue; }
        }

        private string ninthCassetteNumber;
        public string NinthCassetteNumber
        {
            get { return ninthCassetteNumber; }
        }

        private string tenthCassetteValue;
        public string TenthCassetteValue
        {
            get { return tenthCassetteValue; }
        }

        private string tenthCassetteNumber;
        public string TenthCassetteNumber
        {
            get { return tenthCassetteNumber; }
        }

        private string eleventhCassetteValue;
        public string EleventhCassetteValue
        {
            get { return eleventhCassetteValue; }
        }

        private string eleventhCassetteNumber;
        public string EleventhCassetteNumber
        {
            get { return eleventhCassetteNumber; }
        }

        private string kioskId;
        public string KioskId
        {
            get { return kioskId; }
        }

        private string casette1Reject;
        public string Casette1Reject
        {
            get { return casette1Reject; }
        }

        private string casette2Reject;
        public string Casette2Reject
        {
            get { return casette2Reject; }
        }

        private string casette3Reject;
        public string Casette3Reject
        {
            get { return casette3Reject; }
        }

        private string casette4Reject;
        public string Casette4Reject
        {
            get { return casette4Reject; }
        }

        private string className;
        public string ClassName
        {
            get { return className; }
            set { className = value; }
        }


        private string sum;
        public string Sum
        {
            get { return sum; }
        }
        internal KioskDispenserCashCount(string kioskName
            , string institutionName
            , string kioskId
            , string firstCassetteValue
            , string firstCassetteNumber
            , string secondCassetteValue
            , string secondCassetteNumber
            , string thirdCassetteValue
            , string thirdCassetteNumber
            , string fourthCassetteValue
            , string fourthCassetteNumber
            , string fifthCassetteValue
            , string fifthCassetteNumber
             , string sixthCassetteValue
            , string sixthCassetteNumber
             , string seventhCassetteValue
            , string seventhCassetteNumber
             , string eighthCassetteValue
            , string eighthCassetteNumber
            , string casette1Reject
            , string casette2Reject
            , string casette3Reject
            , string casette4Reject
            , string className
            //, string ninthCassetteNumber
             //, string tenthCassetteValue
            //, string tenthCassetteNumber
            // , string eleventhCassetteValue
           // , string eleventhCassetteNumber
            

           )
        {
            this.kioskName = kioskName;
            this.institutionName = institutionName;
          
            this.firstCassetteValue = firstCassetteValue;
            this.firstCassetteNumber = firstCassetteNumber;
            this.secondCassetteValue = secondCassetteValue;
            this.secondCassetteNumber = secondCassetteNumber;
            this.thirdCassetteValue = thirdCassetteValue;
            this.thirdCassetteNumber = thirdCassetteNumber;
            this.fourthCassetteValue = fourthCassetteValue;
            this.fourthCassetteNumber = fourthCassetteNumber;
            this.fifthCassetteValue = fifthCassetteValue;
            this.fifthCassetteNumber = fifthCassetteNumber;
            this.sixthCassetteValue = sixthCassetteValue;
            this.sixthCassetteNumber = sixthCassetteNumber;
            this.seventhCassetteValue = seventhCassetteValue;
            this.seventhCassetteNumber = seventhCassetteNumber;
            this.eighthCassetteValue = eighthCassetteValue;
            this.eighthCassetteNumber = eighthCassetteNumber;
            this.kioskId = kioskId;
            this.casette1Reject = casette1Reject;
            this.casette2Reject = casette2Reject;
            this.casette3Reject = casette3Reject;
            this.casette4Reject = casette4Reject;
            this.className = className;
        }


        internal KioskDispenserCashCount(string kioskName
            , string institutionName
            , string kioskId
            , string firstCassetteValue
            , string firstCassetteNumber
            , string secondCassetteValue
            , string secondCassetteNumber
            , string thirdCassetteValue
            , string thirdCassetteNumber
            , string fourthCassetteValue
            , string fourthCassetteNumber
            , string fifthCassetteValue
            , string fifthCassetteNumber
            , string sixthCassetteValue
            , string sixthCassetteNumber
            , string seventhCassetteValue
            , string seventhCassetteNumber
            , string eighthCassetteValue
            , string eighthCassetteNumber
            , string casette1Reject
            , string casette2Reject
            , string casette3Reject
            , string casette4Reject
            , string sum
            , string className
           //, string ninthCassetteNumber
           //, string tenthCassetteValue
           //, string tenthCassetteNumber
           // , string eleventhCassetteValue
           // , string eleventhCassetteNumber


           )
        {
            this.kioskName = kioskName;
            this.institutionName = institutionName;

            this.firstCassetteValue = firstCassetteValue;
            this.firstCassetteNumber = firstCassetteNumber;
            this.secondCassetteValue = secondCassetteValue;
            this.secondCassetteNumber = secondCassetteNumber;
            this.thirdCassetteValue = thirdCassetteValue;
            this.thirdCassetteNumber = thirdCassetteNumber;
            this.fourthCassetteValue = fourthCassetteValue;
            this.fourthCassetteNumber = fourthCassetteNumber;
            this.fifthCassetteValue = fifthCassetteValue;
            this.fifthCassetteNumber = fifthCassetteNumber;
            this.sixthCassetteValue = sixthCassetteValue;
            this.sixthCassetteNumber = sixthCassetteNumber;
            this.seventhCassetteValue = seventhCassetteValue;
            this.seventhCassetteNumber = seventhCassetteNumber;
            this.eighthCassetteValue = eighthCassetteValue;
            this.eighthCassetteNumber = eighthCassetteNumber;
            this.kioskId = kioskId;
            this.casette1Reject = casette1Reject;
            this.casette2Reject = casette2Reject;
            this.casette3Reject = casette3Reject;
            this.casette4Reject = casette4Reject;
            this.className = className;


  this.sum = ((Convert.ToDouble(firstCassetteValue.ToString().Replace(',', '.')) * Convert.ToDouble(firstCassetteNumber.ToString().Replace(',', '.'))) +
                             (Convert.ToDouble(secondCassetteValue.ToString().Replace(',', '.')) * Convert.ToDouble(secondCassetteNumber.ToString().Replace(',', '.'))) +
                             (Convert.ToDouble(thirdCassetteValue.ToString().Replace(',', '.')) * Convert.ToDouble(thirdCassetteNumber.ToString().Replace(',', '.'))) +
                             (Convert.ToDouble(fourthCassetteValue.ToString().Replace(',', '.')) * Convert.ToDouble(fourthCassetteNumber.ToString().Replace(',', '.'))) +
                             (Convert.ToDouble(fifthCassetteValue.ToString().Replace(',', '.')) * Convert.ToDouble(fifthCassetteNumber.ToString().Replace(',', '.'))) +
                             (Convert.ToDouble(sixthCassetteValue.ToString().Replace(',', '.')) * Convert.ToDouble(sixthCassetteNumber.ToString().Replace(',', '.'))) +
                             (Convert.ToDouble(seventhCassetteValue.ToString().Replace(',', '.')) * Convert.ToDouble(seventhCassetteNumber.ToString().Replace(',', '.'))) +
                             (Convert.ToDouble(eighthCassetteValue.ToString().Replace(',', '.')) * Convert.ToDouble(eighthCassetteNumber.ToString().Replace(',', '.')))
                             //(Convert.ToDouble(value9Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount9Cell.Text.Replace(',', '.'))) +
                             //(Convert.ToDouble(value10Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount10Cell.Text.Replace(',', '.'))) +
                             // (Convert.ToDouble(value11Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount11Cell.Text.Replace(',', '.')))
                             ).ToString() + " ₺";
        }
    }
}
