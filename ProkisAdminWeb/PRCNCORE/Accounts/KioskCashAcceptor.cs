﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class KioskCashAcceptor
    {
        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string institutionName;
        public string InstitutionName
        {
            get { return institutionName; }
        }

        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

        private string firstDenomValue;
        public string FirstDenomValue
        {
            get { return firstDenomValue; }
        }

        private string firstDenomNumber;
        public string FirstDenomNumber
        {
            get { return firstDenomNumber; }
        }

        private string secondDenomValue;
        public string SecondDenomValue
        {
            get { return secondDenomValue; }
        }

        private string secondDenomNumber;
        public string SecondDenomNumber
        {
            get { return secondDenomNumber; }
        }

        private string thirdDenomValue;
        public string ThirdDenomValue
        {
            get { return thirdDenomValue; }
        }

        private string thirdDenomNumber;
        public string ThirdDenomNumber
        {
            get { return thirdDenomNumber; }
        }

        private string fourthDenomValue;
        public string FourthDenomValue
        {
            get { return fourthDenomValue; }
        }

        private string fourthDenomNumber;
        public string FourthDenomNumber
        {
            get { return fourthDenomNumber; }
        }

        private string fifthDenomValue;
        public string FifthDenomValue
        {
            get { return fifthDenomValue; }
        }

        private string fifthDenomNumber;
        public string FifthDenomNumber
        {
            get { return fifthDenomNumber; }
        }


        private string sixthDenomValue;
        public string SixthDenomValue
        {
            get { return sixthDenomValue; }
        }

        private string sixthDenomNumber;
        public string SixthDenomNumber
        {
            get { return sixthDenomNumber; }
        }


        private string seventhDenomValue;
        public string SeventhDenomValue
        {
            get { return seventhDenomValue; }
        }

        private string seventhDenomNumber;
        public string SeventhDenomNumber
        {
            get { return seventhDenomNumber; }
        }

        private string eighthDenomValue;
        public string EighthDenomValue
        {
            get { return eighthDenomValue; }
        }

        private string eighthDenomNumber;
        public string EighthDenomNumber
        {
            get { return eighthDenomNumber; }
        }

        private string ninthDenomValue;
        public string NinthDenomValue
        {
            get { return ninthDenomValue; }
        }

        private string ninthDenomNumber;
        public string NinthDenomNumber
        {
            get { return ninthDenomNumber; }
        }

        private string tenthDenomValue;
        public string TenthDenomValue
        {
            get { return tenthDenomValue; }
        }

        private string tenthDenomNumber;
        public string TenthDenomNumber
        {
            get { return tenthDenomNumber; }
        }

        private string eleventhDenomValue;
        public string EleventhDenomValue
        {
            get { return eleventhDenomValue; }
        }

        private string eleventhDenomNumber;
        public string EleventhDenomNumber
        {
            get { return eleventhDenomNumber; }
        }

        private string className;
        public string ClassName
        {
            get { return className; }
            set { className = value; }
        }

        internal KioskCashAcceptor(string kioskName
            , string institutionName
            , string insertionDate
            , string firstDenomValue
            , string firstDenomNumber
            , string secondDenomValue
            , string secondDenomNumber
            , string thirdDenomValue
            , string thirdDenomNumber
            , string fourthDenomValue
            , string fourthDenomNumber
            , string fifthDenomValue
            , string fifthDenomNumber
             , string sixthDenomValue
            , string sixthDenomNumber
             , string seventhDenomValue
            , string seventhDenomNumber
             , string eighthDenomValue
            , string eighthDenomNumber
             , string ninthDenomValue
            , string ninthDenomNumber
             , string tenthDenomValue
            , string tenthDenomNumber
             , string eleventhDenomValue
            , string eleventhDenomNumber
            , string className
           )
        {
            this.kioskName = kioskName;
            this.institutionName = institutionName;
            this.insertionDate = insertionDate;
            this.firstDenomValue = firstDenomValue;
            this.firstDenomNumber = firstDenomNumber;
            this.secondDenomValue = secondDenomValue;
            this.secondDenomNumber = secondDenomNumber;
            this.thirdDenomValue = thirdDenomValue;
            this.thirdDenomNumber = thirdDenomNumber;
            this.fourthDenomValue = fourthDenomValue;
            this.fourthDenomNumber = fourthDenomNumber;
            this.fifthDenomValue = fifthDenomValue;
            this.fifthDenomNumber = fifthDenomNumber;
            this.sixthDenomValue = sixthDenomValue;
            this.sixthDenomNumber = sixthDenomNumber;
            this.seventhDenomValue = seventhDenomValue;
            this.seventhDenomNumber = seventhDenomNumber;
            this.eighthDenomValue = eighthDenomValue;
            this.eighthDenomNumber = eighthDenomNumber;
            this.ninthDenomValue = ninthDenomValue;
            this.ninthDenomNumber = ninthDenomNumber;
            this.tenthDenomValue = tenthDenomValue;
            this.tenthDenomNumber = tenthDenomNumber;
            this.eleventhDenomValue = eleventhDenomValue;
            this.eleventhDenomNumber = eleventhDenomNumber;
            this.className = className;
        }




        internal KioskCashAcceptor(string kioskName
           , string institutionName
           , string insertionDate
           , string firstDenomValue
           , string firstDenomNumber
           , string secondDenomValue
           , string secondDenomNumber
           , string thirdDenomValue
           , string thirdDenomNumber
           , string fourthDenomValue
           , string fourthDenomNumber
           , string fifthDenomValue
           , string fifthDenomNumber
            , string sixthDenomValue
           , string sixthDenomNumber
            , string seventhDenomValue
           , string seventhDenomNumber
            , string eighthDenomValue
           , string eighthDenomNumber
            , string ninthDenomValue
           , string ninthDenomNumber
            , string tenthDenomValue
           , string tenthDenomNumber
            , string eleventhDenomValue
           , string eleventhDenomNumber
           , string className,
            double sum
          )
        {
            
            this.kioskName = kioskName;
            this.institutionName = institutionName;
            this.insertionDate = insertionDate;
            this.firstDenomValue = firstDenomValue;
            this.firstDenomNumber = firstDenomNumber;
            this.secondDenomValue = secondDenomValue;
            this.secondDenomNumber = secondDenomNumber;
            this.thirdDenomValue = thirdDenomValue;
            this.thirdDenomNumber = thirdDenomNumber;
            this.fourthDenomValue = fourthDenomValue;
            this.fourthDenomNumber = fourthDenomNumber;
            this.fifthDenomValue = fifthDenomValue;
            this.fifthDenomNumber = fifthDenomNumber;
            this.sixthDenomValue = sixthDenomValue;
            this.sixthDenomNumber = sixthDenomNumber;
            this.seventhDenomValue = seventhDenomValue;
            this.seventhDenomNumber = seventhDenomNumber;
            this.eighthDenomValue = eighthDenomValue;
            this.eighthDenomNumber = eighthDenomNumber;
            this.ninthDenomValue = ninthDenomValue;
            this.ninthDenomNumber = ninthDenomNumber;
            this.tenthDenomValue = tenthDenomValue;
            this.tenthDenomNumber = tenthDenomNumber;
            this.eleventhDenomValue = eleventhDenomValue;
            this.eleventhDenomNumber = eleventhDenomNumber;
            this.className = className;
            this.sum =
                Convert.ToDouble(firstDenomNumber) *  Convert.ToDouble(firstDenomValue) +
                Convert.ToDouble(secondDenomNumber) * Convert.ToDouble(secondDenomValue) +
                Convert.ToDouble(thirdDenomNumber) *  Convert.ToDouble(thirdDenomValue) +
                Convert.ToDouble(fourthDenomNumber) * Convert.ToDouble(fourthDenomValue) +
                Convert.ToDouble(fifthDenomNumber) *  Convert.ToDouble(fifthDenomValue) +
                Convert.ToDouble(sixthDenomNumber) *  Convert.ToDouble(sixthDenomValue) +
                Convert.ToDouble(seventhDenomNumber)* Convert.ToDouble(seventhDenomValue) +
                Convert.ToDouble(eighthDenomNumber) * Convert.ToDouble(eighthDenomValue) +
                Convert.ToDouble(ninthDenomNumber) *  Convert.ToDouble(ninthDenomValue) +
                Convert.ToDouble(tenthDenomNumber) *  Convert.ToDouble(tenthDenomValue) +
                Convert.ToDouble(eleventhDenomNumber)*Convert.ToDouble(eleventhDenomValue)+ "₺";
        }
        private string sum;
        public string Sum
        {
            get { return sum; }
        }
    }

}
