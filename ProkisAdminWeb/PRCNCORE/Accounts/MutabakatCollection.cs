﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class MutabakatCollection : ReadOnlyCollection<Mutabakat>
    {
        internal MutabakatCollection()
            : base(new List<Mutabakat>())
        {

        }

        internal void Add(Mutabakat item)
        {
            if (item != null && !this.Contains(item))
            {
                this.Items.Add(item);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (Mutabakat item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("Mutabakat", jArray);

            return jObjectDis;
        }

    }
    public class MutabakatReportCollection : ReadOnlyCollection<MutabakatReport>
    {
        internal MutabakatReportCollection()
            : base(new List<MutabakatReport>())
        {

        }

        internal void Add(MutabakatReport item)
        {
            if (item != null && !this.Contains(item))
            {
                this.Items.Add(item);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (MutabakatReport item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("MutabakatReport", jArray);

            return jObjectDis;
        }

    }


}