﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class DailyAccountReportCollection : ReadOnlyCollection<DailyAccountReport>
    {
        internal DailyAccountReportCollection()
            : base(new List<DailyAccountReport>())
        {

        }

        internal void Add(DailyAccountReport item)
        {
            if (item != null && !this.Contains(item))
            {
                this.Items.Add(item);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (DailyAccountReport item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("DailyReports", jArray);

            return jObjectDis;
        }

    }


    public class CodeReportCollections : ReadOnlyCollection<CodeReports>
    {
        internal CodeReportCollections()
            : base(new List<CodeReports>())
        {

        }

        internal void Add(CodeReports kiosk)
        {
            if (kiosk != null && !this.Contains(kiosk))
            {
                this.Items.Add(kiosk);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (CodeReports item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("kioskReports", jArray);

            return jObjectDis;
        }

    }

    public class AutoMutabakatCollection : ReadOnlyCollection<AutoMutabakatReport>
    {
        internal AutoMutabakatCollection()
            : base(new List<AutoMutabakatReport>())
        {

        }

        internal void Add(AutoMutabakatReport item)
        {
            if (item != null && !this.Contains(item))
            {
                this.Items.Add(item);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (AutoMutabakatReport item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("Transactions", jArray);

            return jObjectDis;
        }

    }
}