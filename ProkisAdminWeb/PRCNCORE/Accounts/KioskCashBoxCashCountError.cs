﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class KioskCashBoxCashCountError
    {
        private string transactionId;
        public string TransactionId
        {
            get { return transactionId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

        private string reject1;
        public string Reject1
        {
            get { return reject1; }
        }

        private string reject2;
        public string Reject2
        {
            get { return reject2; }
        }

        private string reject3;
        public string Reject3
        {
            get { return reject3; }
        }

        private string reject4;
        public string Reject4
        {
            get { return reject4; }
        }


        private string value1;
        public string Value1
        {
            get { return value1; }
        }

        private string value2;
        public string Value2
        {
            get { return value2; }
        }

        private string value3;
        public string Value3
        {
            get { return value3; }
        }

        private string value4;
        public string Value4
        {
            get { return value4; }
        }


        private string exit1;
        public string Exit1
        {
            get { return exit1; }
        }

        private string exit2;
        public string Exit2
        {
            get { return exit2; }
        }

        private string exit3;
        public string Exit3
        {
            get { return exit3; }
        }

        private string exit4;
        public string Exit4
        {
            get { return exit4; }
        }


        private string error;
        public string Error
        {
            get { return error; }
        }

        private string errorStatus;
        public string ErrorStatus
        {
            get { return errorStatus; }
        }


        private string exception;
        public string Exception
        {
            get { return exception; }
        }

        private string totalLog;
        public string TotalLog
        {
            get { return totalLog; }
        }

        private string totalDispensed;
        public string TotalDispensed
        {
            get { return totalDispensed; }
        }

        private string totalDifference;
        public string TotalDifference
        {
            get { return totalDifference; }
        }

        private string className;
        public string ClassName
        {
            get { return className; }
            set { className = value; }
        }


        internal KioskCashBoxCashCountError(string transactionId
            , string kioskName
            , string insertionDate
            , string reject1
            , string reject2
            , string reject3
            , string reject4
            , string value1
            , string value2
            , string value3
            , string value4
            , string exit1
            , string exit2
            , string exit3
            , string exit4
            , string error
            , string errorStatus
            , string exception
            , string totalLog
            , string totalDispensed
            , string totalDifference
            , string className
            // , string ninthDenomValue
            //, string ninthDenomNumber
             //, string tenthDenomValue
            //, string tenthDenomNumber
            // , string eleventhDenomValue
           // , string eleventhDenomNumber
            

           )
        {
            this.transactionId = transactionId;
            this.kioskName = kioskName;
            this.insertionDate = insertionDate;

            this.reject1 = reject1;
            this.reject2 = reject2;
            this.reject3 = reject3;
            this.reject4 = reject4;

            this.value1 = value1;
            this.value2 = value2;
            this.value3 = value3;
            this.value4 = value4;

            this.exit1 = exit1;
            this.exit2 = exit2;
            this.exit3 = exit3;
            this.exit4 = exit4;

            this.error = error;
            this.errorStatus = errorStatus;
            this.exception = exception;
            this.totalLog = totalLog;
            this.totalDispensed = totalDispensed;
            this.totalDifference = totalDifference;
            this.className = className;
        }
    }
}
