﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class Account
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private int accountRangeId;
        public int AccountRangeId
        {
            get { return accountRangeId; }
        }

        private string accountRangeName;
        public string AccountRangeName
        {
            get { return accountRangeName; }
        }

        private string denizbank;
        public string Denizbank
        {
            get { return denizbank; }
        }

        private string instutionMoney;
        public string InstutionMoney
        {
            get { return instutionMoney; }
        }

        private string merkez;
        public string Merkez
        {
            get { return merkez; }
        }

        private string moneyCode;
        public string MoneyCode
        {
            get { return moneyCode; }
        }

        private string instutionName;
        public string InstutionName
        {
            get { return instutionName; }
        }

        private int instutionId;
        public int InstutionId
        {
            get { return instutionId; }
        }

        internal Account(int id
            , int accountRangeId
            , string accountRangeName
            , string denizbank
            , string instutionMoney
            , string merkez
            , string moneyCode
            , string instutionName
            , int instutionId)
        {
            this.id = id;
            this.accountRangeId = accountRangeId;
            this.accountRangeName = accountRangeName;
            this.denizbank = denizbank;
            this.instutionMoney = instutionMoney;
            this.merkez = merkez;
            this.moneyCode = moneyCode;
            this.instutionName = instutionName;
            this.instutionId = instutionId;
        }
    }
}
