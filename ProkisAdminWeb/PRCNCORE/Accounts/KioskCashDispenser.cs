﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class KioskCashDispenser
    {
        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string institutionName;
        public string InstitutionName
        {
            get { return institutionName; }
        }

        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }


        private string firstDenomValue;
        public string FirstDenomValue
        {
            get { return firstDenomValue; }
        }

        private string firstDenomNumber;
        public string FirstDenomNumber
        {
            get { return firstDenomNumber; }
        }

        private string secondDenomValue;
        public string SecondDenomValue
        {
            get { return secondDenomValue; }
        }

        private string secondDenomNumber;
        public string SecondDenomNumber
        {
            get { return secondDenomNumber; }
        }

        private string thirdDenomValue;
        public string ThirdDenomValue
        {
            get { return thirdDenomValue; }
        }

        private string thirdDenomNumber;
        public string ThirdDenomNumber
        {
            get { return thirdDenomNumber; }
        }

        private string fourthDenomValue;
        public string FourthDenomValue
        {
            get { return fourthDenomValue; }
        }

        private string fourthDenomNumber;
        public string FourthDenomNumber
        {
            get { return fourthDenomNumber; }
        }

        private string fifthDenomValue;
        public string FifthDenomValue
        {
            get { return fifthDenomValue; }
        }

        private string fifthDenomNumber;
        public string FifthDenomNumber
        {
            get { return fifthDenomNumber; }
        }


        private string sixthDenomValue;
        public string SixthDenomValue
        {
            get { return sixthDenomValue; }
        }

        private string sixthDenomNumber;
        public string SixthDenomNumber
        {
            get { return sixthDenomNumber; }
        }


        private string seventhDenomValue;
        public string SeventhDenomValue
        {
            get { return seventhDenomValue; }
        }

        private string seventhDenomNumber;
        public string SeventhDenomNumber
        {
            get { return seventhDenomNumber; }
        }

        private string eighthDenomValue;
        public string EighthDenomValue
        {
            get { return eighthDenomValue; }
        }

        private string eighthDenomNumber;
        public string EighthDenomNumber
        {
            get { return eighthDenomNumber; }
        }

        private string ninthDenomValue;
        public string NinthDenomValue
        {
            get { return ninthDenomValue; }
        }

        private string cassette1reject;
        public string Casette1Reject
        {
            get { return cassette1reject; }
        }

        private string cassette2reject;
        public string Casette2Reject
        {
            get { return cassette2reject; }
        }

        private string cassette3reject;
        public string Casette3Reject
        {
            get { return cassette3reject; }
        }

        private string cassette4reject;
        public string Casette4Reject
        {
            get { return cassette4reject; }
        }

        private string eleventhDenomNumber;
        public string EleventhDenomNumber
        {
            get { return eleventhDenomNumber; }
        }

        private string kioskId;
        public string KioskId
        {
            get { return kioskId; }
        }

        private string id;
        public string Id
        {
            get { return id; }
        }

        internal KioskCashDispenser(string kioskName
            , string institutionName
            , string insertionDate
            , string kioskId
            , string firstDenomValue
            , string firstDenomNumber
            , string secondDenomValue
            , string secondDenomNumber
            , string thirdDenomValue
            , string thirdDenomNumber
            , string fourthDenomValue
            , string fourthDenomNumber
            , string fifthDenomValue
            , string fifthDenomNumber
             , string sixthDenomValue
            , string sixthDenomNumber
             , string seventhDenomValue
            , string seventhDenomNumber
             , string eighthDenomValue
            , string eighthDenomNumber
             , string cassette1reject
            , string cassette2reject
             , string cassette3reject
            , string cassette4reject
             , string id
            //, string eleventhDenomNumber
            //, string id

           )
        {
            this.kioskName = kioskName;
            this.institutionName = institutionName;
            this.insertionDate = insertionDate;
            this.firstDenomValue = firstDenomValue;
            this.firstDenomNumber = firstDenomNumber;
            this.secondDenomValue = secondDenomValue;
            this.secondDenomNumber = secondDenomNumber;
            this.thirdDenomValue = thirdDenomValue;
            this.thirdDenomNumber = thirdDenomNumber;
            this.fourthDenomValue = fourthDenomValue;
            this.fourthDenomNumber = fourthDenomNumber;
            this.fifthDenomValue = fifthDenomValue;
            this.fifthDenomNumber = fifthDenomNumber;
            this.sixthDenomValue = sixthDenomValue;
            this.sixthDenomNumber = sixthDenomNumber;
            this.seventhDenomValue = seventhDenomValue;
            this.seventhDenomNumber = seventhDenomNumber;
            this.eighthDenomValue = eighthDenomValue;
            this.eighthDenomNumber = eighthDenomNumber;
            this.cassette1reject = cassette1reject;
            this.cassette2reject = cassette2reject;
            this.cassette3reject = cassette3reject;
            this.cassette4reject = cassette4reject;
            this.id = id;
            //this.eleventhDenomNumber = eleventhDenomNumber;
            this.kioskId = kioskId;
            //this.id = id;
        }
    }
}
