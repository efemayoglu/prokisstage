﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class KioskDispenserMonitoringCollection : ReadOnlyCollection<KioskDispenserMonitoring>
    {
        internal KioskDispenserMonitoringCollection()
            : base(new List<KioskDispenserMonitoring>())
        {

        }

        internal void Add(KioskDispenserMonitoring item)
        {
            if (item != null && !this.Contains(item))
            {
                this.Items.Add(item);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (KioskDispenserMonitoring item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("KioskDispenserMonitoring", jArray);

            return jObjectDis;
        }
    }
}
