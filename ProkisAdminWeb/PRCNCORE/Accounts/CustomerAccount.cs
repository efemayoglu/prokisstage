﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class CustomerAccount
    {
        private Int64 id;
        public Int64 Id
        {
            get { return id; }
        }

        private string customerNo;
        public string CustomerNo
        {
            get { return customerNo; }
        }

        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
        }

        private string amount;
        public string Amount
        {
            get { return amount; }
        }

        private string date;
        public string Date
        {
            get { return date; }
        }


        internal CustomerAccount(Int64 id,
                          string customerNo,
                          string customerName,
                          string amount,
                          string date)
        {
            this.id = id;
            this.customerNo = customerNo;
            this.customerName = customerName;
            this.amount = amount;
            this.date = date;
        }
    }
}