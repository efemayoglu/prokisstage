﻿using System;
using System.Collections.Generic;

namespace PRCNCORE.Accounts
{

   
    public class KioskEmptyStatus
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Explanation { get; set; }

        public int KioskEmptyId { get; set; }

        public KioskEmptyStatus()
        {

        }

        public  KioskEmptyStatus(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }
        public KioskEmptyStatus(int id, string name, string explanation)
        {
            this.Id = id;
            this.Name = name;
            this.Explanation = explanation;
        }
        public KioskEmptyStatus(int id, string name, string explanation, int kioskEmptyId)
        {
            this.Id = id;
            this.Name = name;
            this.Explanation = explanation;
            this.KioskEmptyId = KioskEmptyId;
        }
    }
    public class UsageFeeDetail
    {

        public string kioskId { get; set; }
        public int instutionId { get; set; }
        public int isKioskListed { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }

    public class UsageFeeDetails
    {
        public string KioskName { get; set; }
        public decimal UsageFee { get; set; }
        public int TransactionCount { get; set; }
        public int Total { get; set; }
        public string className { get; set; }
    }
    public class UsageFeeDetailsModel
    {
        public List<UsageFeeDetails> Datas { get; set; }
    }
}


/*
 
        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (Account item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("Account", jArray);

            return jObjectDis;
        }
     */
