﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class MutabakatList
    {
        private Int64 transactionId;
        public Int64 TransactionId
        {
            get { return transactionId; }
        }

        private string aboneNo;
        public string AboneNo
        {
            get { return aboneNo; }
        }

        private string amount;
        public string Amount
        {
            get { return amount; }
        }

        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

        private string faturaIds;
        public string FaturaIds
        {
            get { return faturaIds; }
        }

        private string completeStatus;
        public string CompleteStatus
        {
            get { return completeStatus; }
        }

        private int status;
        public int Status
        {
            get { return status; }
        }

        internal MutabakatList(Int64 transactionId
            , string aboneNo
            , string amount
            , string insertionDate
            , string faturaIds
            , int status
            , string completeStatus)
        {
            this.transactionId = transactionId;
            this.aboneNo = aboneNo;
            this.insertionDate = insertionDate;
            this.faturaIds = faturaIds;
            this.amount = amount;
            this.status = status;
            this.completeStatus = completeStatus;
        }
    }
}
