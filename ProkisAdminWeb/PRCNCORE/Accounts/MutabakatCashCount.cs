﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class MutabakatCashCount
    {
        private int cashTypeId;
        public int CashTypeId
        {
            get { return this.cashTypeId; }
        }

        private int cashCount;
        public int CashCount
        {
            get { return this.cashCount; }
        }

        private string cashTypeName;
        public string CashTypeName
        {
            get { return this.cashTypeName; }
        }

        private string cashTypeValue;
        public string CashTypeValue
        {
            get { return this.cashTypeValue; }
        }

        private string cashSum;
        public string CashSum
        {
            get { return this.cashSum; }
        }

        internal MutabakatCashCount(int cashTypeId, int cashCount, string cashTypeName, string cashTypeValue, string cashSum)
        {
            this.cashTypeId = cashTypeId;
            this.cashCount = cashCount;
            this.cashTypeName = cashTypeName;
            this.cashTypeValue = cashTypeValue;
            this.cashSum = cashSum;
        }
        //Toplanan para onaylistesi onay detayını görmek için ve parserda hata aldıgı ıcın 2. yapıcı metod oluşturdum.
        internal MutabakatCashCount(int cashTypeId, int cashCount, string cashTypeName, string cashTypeValue)
        {
            this.cashTypeId = cashTypeId;
            this.cashCount = cashCount;
            this.cashTypeName = cashTypeName;
            this.cashTypeValue = cashTypeValue;
        }
    }

    public class GetKioskUsersAndPassword
    {
        private string userName;
        public string UserName
        {
            get { return this.userName; }
        }

        private string passWord;
        public string PassWord
        {
            get { return this.passWord; }
        }

        private int kioskId;
        public int KioskId
        {
            get { return this.kioskId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return this.kioskName; }
        }

        private string kartDolumTutar;
        public string KartDolumTutar
        {
            get { return this.kartDolumTutar; }
        }

        private string kartDolumSayısı;
        public string KartDolumSayısı
        {
            get { return this.kartDolumSayısı; }
        }

        private string faturaDolumTutar;
        public string FaturaDolumTutar
        {
            get { return this.faturaDolumTutar; }
        }

        private string faturaDolumSayısı;
        public string FaturaDolumSayısı
        {
            get { return this.faturaDolumSayısı; }
        }

        internal GetKioskUsersAndPassword(string userName, string passWord, int kioskId, string kioskName, string kartDolumTutar, string kartDolumSayısı, string faturaDolumTutar, string faturaDolumSayısı)
        {
            this.userName = userName;
            this.passWord = passWord;
            this.kioskId = kioskId;
            this.kioskName = kioskName;
            this.kartDolumTutar = kartDolumTutar;
            this.kartDolumSayısı = kartDolumSayısı;
            this.faturaDolumTutar = faturaDolumTutar;
            this.faturaDolumSayısı = faturaDolumSayısı;
        }
    }
}
