﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class CustomerAccountCollection : ReadOnlyCollection<CustomerAccount>
    {
        internal CustomerAccountCollection()
            : base(new List<CustomerAccount>())
        {

        }

        internal void Add(CustomerAccount transaction)
        {
            if (transaction != null && !this.Contains(transaction))
            {
                this.Items.Add(transaction);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (CustomerAccount item in this)
            {
                JObject jo = new JObject();
                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);
                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("customerAccounts", jArray);

            return jObjectDis;
        }

    }
}