﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class DailyAccountReportProkis
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

        private string totalBalance;
        public string TotalBalance
        {
            get { return totalBalance; }
        }

        private string activeAskiCode;
        public string ActiveAskiCode
        {
            get { return activeAskiCode; }
        }

        private string pasifAskiCode;
        public string PasifAskiCode
        {
            get { return pasifAskiCode; }
        }

        private string pasifCodeBalance;
        public string PasifCodeBalance
        {
            get { return pasifCodeBalance; }
        }

        private string pasifCodeCount;
        public string PasifCodeCount
        {
            get { return pasifCodeCount; }
        }

        private string usableCodeBeforeExpiring;
        public string UsableCodeBeforeExpiring
        {
            get { return usableCodeBeforeExpiring; }
        }

        internal DailyAccountReportProkis(int id
            , string insertionDate
            , string totalBalance
            , string activeAskiCode
            , string pasifAskiCode
            , string pasifCodeBalance
            , string pasifCodeCount
            , string usableCodeBeforeExpiring)
        {
            this.id = id;
            this.insertionDate = insertionDate;
            this.totalBalance = totalBalance;
            this.activeAskiCode = activeAskiCode;
            this.pasifAskiCode = pasifAskiCode;
            this.pasifCodeBalance = pasifCodeBalance;
            this.pasifCodeCount = pasifCodeCount;
            this.usableCodeBeforeExpiring = usableCodeBeforeExpiring;
        }
    }
}
