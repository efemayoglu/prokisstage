﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class MutabakatCashCountCollection : ReadOnlyCollection<MutabakatCashCount>
    {
        internal MutabakatCashCountCollection()
            : base(new List<MutabakatCashCount>())
        {

        }

        internal void Add(MutabakatCashCount role)
        {
            if (role != null && !this.Contains(role))
            {
                this.Items.Add(role);
            }
        }

        public JObject GetJSONJObject()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (MutabakatCashCount item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("CashCounts", jArray);

            return jObjectDis;
        }

        public JArray GetJSONJArray()
        {
            JArray jArray = new JArray();

            foreach (MutabakatCashCount item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            return jArray;
        }
    }

    public class GetKioskUsersAndPasswordCollection : ReadOnlyCollection<GetKioskUsersAndPassword>
    {
        internal GetKioskUsersAndPasswordCollection()
            : base(new List<GetKioskUsersAndPassword>())
        {

        }

        internal void Add(GetKioskUsersAndPassword role)
        {
            if (role != null && !this.Contains(role))
            {
                this.Items.Add(role);
            }
        }

        public JObject GetJSONJObject()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (GetKioskUsersAndPassword item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("KioskInfo", jArray);

            return jObjectDis;
        }

        public JArray GetJSONJArray()
        {
            JArray jArray = new JArray();

            foreach (GetKioskUsersAndPassword item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            return jArray;
        }
    }
}