﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class POSEOD
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string transactionDate;
        public string TransactionDate
        {
            get { return transactionDate; }
        }

        private string transactionCount;
        public string TransactionCount
        {
            get { return transactionCount; }
        }

        private string posTransactionCount;
        public string POSTransactionCount
        {
            get { return posTransactionCount; }
        }

        private string distinctionTransaction;
        public string DistinctionTransaction
        {
            get { return distinctionTransaction; }
        }

        private string amount;
        public string Amount
        {
            get { return amount; }
        }

        private string posAmount;
        public string POSAmount
        {
            get { return posAmount; }
        }

        private string distinctionAmount;
        public string DistinctionAmount
        {
            get { return distinctionAmount; }
        }

        private string status;
        public string Status
        {
            get { return status; }
        }

        private string explanation;
        public string Explanation
        {
            get { return explanation; }
        }

        private string user;
        public string User
        {
            get { return user; }
        }

        private int resultCount;
        public int ResultCount
        {
            get { return resultCount; }
        }

        private string statusId;
        public string StatusId
        {
            get { return statusId; }
        }

        private string className;
        public string ClassName
        {
            get { return className; }
        }

        internal POSEOD(int resultCount
             , int id
             , string kioskName
             , string transactionDate
             , string transactionCount
            , string posTransactionCount
            , string distinctionTransaction
            , string amount
            , string posAmount
            , string distinctionAmount
            , string status
            , string explanation
            , string user
            , string statusId
            , string className)
        {
            this.id = id;
            this.kioskName = kioskName;
            this.transactionDate = transactionDate;
            this.transactionCount = transactionCount;
            this.posTransactionCount = posTransactionCount;
            this.distinctionTransaction = distinctionTransaction;
            this.amount = amount;
            this.posAmount = posAmount;
            this.distinctionAmount = distinctionAmount;
            this.status = status;
            this.explanation = explanation;
            this.user = user;
            this.resultCount = resultCount;
            this.statusId = statusId;
            this.className = className;
        }

    }
}
