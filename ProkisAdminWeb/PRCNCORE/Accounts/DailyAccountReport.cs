﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class DailyAccountReport
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

        private string totalBalance;
        public string TotalBalance
        {
            get { return totalBalance; }
        }

        private string activeBalance;
        public string ActiveBalance
        {
            get { return activeBalance; }
        }

        private string pasifBalance;
        public string PasifBalance
        {
            get { return pasifBalance; }
        }

        internal DailyAccountReport(int id
            , string insertionDate
            , string totalBalance
            , string activeBalance
            , string pasifBalance)
        {
            this.id = id;
            this.insertionDate = insertionDate;
            this.totalBalance = totalBalance;
            this.activeBalance = activeBalance;
            this.pasifBalance = pasifBalance;
        }
    }


    public class AutoMutabakatReport
    {
        private string institutionNumber;
        public string InstitutionNumber
        {
            get { return institutionNumber; }
        }

        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

        private string institutionAmount;
        public string InstitutionAmount
        {
            get { return institutionAmount; }
        }

        private string institutionCancelNumber;
        public string InstitutionCancelNumber
        {
            get { return institutionCancelNumber; }
        }

        private string institutionCancelAmount;
        public string InstitutionCancelAmount
        {
            get { return institutionCancelAmount; }
        }

        private string bosNumber;
        public string BosNumber
        {
            get { return bosNumber; }
        }

        private string bosAmount;
        public string BosAmount
        {
            get { return bosAmount; }
        }

        private string bosCancelNumber;
        public string BosCancelNumber
        {
            get { return bosCancelNumber; }
        }

        private string bosCancelAmount;
        public string BosCancelAmount
        {
            get { return bosCancelAmount; }
        }

        private string account;
        public string Account
        {
            get { return account; }
        }

        private string userId;
        public string UserId
        {
            get { return userId; }
        }

        private string isSucess;
        public string IsSucess
        {
            get { return isSucess; }
        }

        private string message;
        public string Message
        {
            get { return message; }
        }

        private string manualApproveDate;
        public string ManualApproveDate
        {
            get { return manualApproveDate; }
        }

        private string insMif;
        public string InsMif
        {
            get { return insMif; }
        }

        private string insMakbuz;
        public string InsMakbuz
        {
            get { return insMakbuz; }
        }
        private string insDate;
        public string InsDate
        {
            get { return insDate; }
        }

        private string explanation;
        public string Explanation
        {
            get { return explanation; }
        }

        internal AutoMutabakatReport(string institutionNumber
            , string institutionAmount
            , string institutionCancelNumber
            , string institutionCancelAmount
            , string bosNumber
            , string bosAmount
            , string bosCancelNumber
            , string bosCancelAmount
            , string account
            , string insertionDate
            , string isSucess
            , string message
            , string userId
            , string manualApproveDate
            , string insMif
            , string insMakbuz
            , string insDate
            , string explanation
            )
        {
            this.institutionNumber = institutionNumber;
            this.institutionAmount = institutionAmount;
            this.institutionCancelNumber = institutionCancelNumber;
            this.institutionCancelAmount = institutionCancelAmount;
            this.bosNumber = bosNumber;
            this.bosAmount = bosAmount;
            this.bosCancelNumber = bosCancelNumber;
            this.bosCancelAmount = bosCancelAmount;
            this.account = account;
            this.insertionDate = insertionDate;
            this.isSucess=isSucess;
            this.message=message;
            this.userId=userId;
            this.manualApproveDate=manualApproveDate;
            this.insMif=insMif;
            this.insMakbuz=insMakbuz;
            this.insDate=insDate;
            this.explanation = explanation;
        }
    }
    public class CodeReports
    {
        private string generatedTotal;
        public string GeneratedTotal
        {
            get { return generatedTotal; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string generatedTodayUsableToday;
        public string GeneratedTodayUsableToday
        {
            get { return generatedTodayUsableToday; }
        }

        private string usedTotalToday;
        public string UsedTotalToday
        {
            get { return usedTotalToday; }
        }

        private string generatedTodayUsedToday;
        public string GeneratedTodayUsedToday
        {
            get { return generatedTodayUsedToday; }
        }

        private string generatedYesterdayUsedToday;
        public string GeneratedYesterdayUsedToday
        {
            get { return generatedYesterdayUsedToday; }
        }

        private string generatedYesterdayUsedTodayCount;
        public string GeneratedYesterdayUsedTodayCount
        {
            get { return generatedYesterdayUsedTodayCount; }
        }

        private string remainUsableCode;
        public string RemainUsableCode
        {
            get { return remainUsableCode; }
        }

        private string resultCount;
        public string ResultCount
        {
            get { return resultCount; }
        }

        private string cashAmount;
        public string CashAmount
        {
            get { return cashAmount; }
        }

        private string institutionAmount;
        public string InstitutionAmount
        {
            get { return institutionAmount; }
        }

        private string paraUstu;
        public string ParaUstu
        {
            get { return paraUstu; }
        }

        private string successTxnCount;
        public string SuccessTxnCount
        {
            get { return successTxnCount; }
        }

        private string instutionAmount;
        public string InstutionAmount
        {
            get { return instutionAmount; }
        }

        private string usageFee;
        public string UsageFee
        {
            get { return usageFee; }
        }

        private string mutabakat;
        public string Mutabakat
        {
            get { return mutabakat; }
        }

        private string creditCardAmount;
        public string CreditCardAmount
        {
            get { return creditCardAmount; }
        }

        private string creditCardCommisionAmount;
        public string CreditCardCommisionAmount
        {
            get { return creditCardCommisionAmount; }
        }

        private string mobilePaymentAmount;
        public string MobilePaymentAmount
        {
            get { return mobilePaymentAmount; }
        }

        private string mobilePaymentCommisionAmount;
        public string MobilePaymentCommisionAmount
        {
            get { return mobilePaymentCommisionAmount; }
        }

        private string maneullyGeneratedCodeAmount;
        public string ManeullyGeneratedCodeAmount
        {
            get { return maneullyGeneratedCodeAmount; }
        }

        private string askidaCode;
        public string AskidaCode
        {
            get { return askidaCode; }
        }


        private string erasedCode;
        public string ErasedCode
        {
            get { return erasedCode; }
        }

        private string reverseRecCount;
        public string ReverseRecCount
        {
            get { return reverseRecCount; }
        }

        private string reverseRecTotal;
        public string ReverseRecTotal
        {
            get { return reverseRecTotal; }
        }


        private string nonReverseRecCount;
        public string NonReverseRecCount
        {
            get { return nonReverseRecCount; }
        }

        private string nonReverseRecTotal;
        public string NonReverseRecTotal
        {
            get { return nonReverseRecTotal; }
        }

        private string recCount;
        public string RecCount
        {
            get { return recCount; }
        }

        private string recTotal;
        public string RecTotal
        {
            get { return recTotal; }
        }

        private string className;
        public string ClassName
        {
            get { return className; }
            set { className = value; }
        }

        internal CodeReports(string generatedTotal
                           , string kioskName
                           , string generatedTodayUsableToday
                           , string usedTotalToday
                           , string generatedTodayUsedToday
                           , string generatedYesterdayUsedToday
                           , string generatedYesterdayUsedTodayCount
                           , string remainUsableCode
                           , string resultCount
                           , string cashAmount
                           , string successTxnCount
                           , string institutionAmount
                           , string usageFee
                           , string paraUstu
                           , string mutabakat
                           , string creditCardAmount
                           , string creditCardCommisionAmount
                           , string mobilePaymentAmount
                           , string mobilePaymentCommisionAmount
                           , string maneullyGeneratedCodeAmount
                           , string askidaCode
                           , string erasedCode
                           , string reverseRecCount
                           , string reverseRecTotal
                           , string nonReverseRecCount
                           , string nonReverseRecTotal
                           , string recCount
                           , string recTotal
                           , string className
            )
        {
            this.generatedTotal = generatedTotal;
            this.kioskName = kioskName;
            this.generatedTodayUsableToday = generatedTodayUsableToday;
            this.usedTotalToday = usedTotalToday;
            this.generatedTodayUsedToday = generatedTodayUsedToday;
            this.generatedYesterdayUsedToday = generatedYesterdayUsedToday;
            this.generatedYesterdayUsedTodayCount = generatedYesterdayUsedTodayCount;
            this.remainUsableCode = remainUsableCode;
            this.resultCount = resultCount;
            this.cashAmount = cashAmount;
            this.successTxnCount = successTxnCount;
            this.institutionAmount = institutionAmount;
            this.usageFee = usageFee;
            this.paraUstu = paraUstu;
            this.mutabakat = mutabakat;
            this.creditCardAmount = creditCardAmount;
            this.creditCardCommisionAmount = creditCardCommisionAmount;
            this.mobilePaymentAmount = mobilePaymentAmount;
            this.mobilePaymentCommisionAmount = mobilePaymentCommisionAmount;
            this.maneullyGeneratedCodeAmount = maneullyGeneratedCodeAmount;
            this.askidaCode = askidaCode;
            this.erasedCode = erasedCode;
            this.className = className;
            this.reverseRecCount = reverseRecCount;
            this.reverseRecTotal = reverseRecTotal;
            this.nonReverseRecCount = nonReverseRecCount;
            this.nonReverseRecTotal = nonReverseRecTotal;
            this.recCount = recCount;
            this.recTotal = recTotal;

        }


    }
}
