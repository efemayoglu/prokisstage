﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PRCNCORE.Accounts
{
  public class KioskCashDispenserLCDMHopper
  {
    private string kioskName;
    [JsonProperty(Order = 1)]
    public string KioskName { get { return kioskName; } }

    private string institutionName;
    [JsonProperty(Order = 2)]
    public string InstitutionName { get { return institutionName; } }

    private string insertionDate;
    [JsonProperty(Order = 3)]
    public string InsertionDate { get { return insertionDate; } }


    private string firstDenomValue;
    [JsonProperty(Order = 4)]
    public string FirstDenomValue { get { return firstDenomValue; } }

    private string firstDenomNumber;
    [JsonProperty(Order = 5)]
    public string FirstDenomNumber { get { return firstDenomNumber; } }

    private string secondDenomValue;
    [JsonProperty(Order = 7)]
    public string SecondDenomValue { get { return secondDenomValue; } }

    private string secondDenomNumber;
    [JsonProperty(Order = 8)]
    public string SecondDenomNumber { get { return secondDenomNumber; } }

    private string thirdDenomValue;
    [JsonProperty(Order = 10)]
    public string ThirdDenomValue { get { return thirdDenomValue; } }

    private string thirdDenomNumber;
    [JsonProperty(Order = 11)]
    public string ThirdDenomNumber { get { return thirdDenomNumber; } }

    private string fourthDenomValue;
    [JsonProperty(Order = 13)]
    public string FourthDenomValue { get { return fourthDenomValue; } }

    private string fourthDenomNumber;
    [JsonProperty(Order = 14)]
    public string FourthDenomNumber { get { return fourthDenomNumber; } }

    private string fifthDenomValue;
    [JsonProperty(Order = 16)]
    public string FifthDenomValue { get { return fifthDenomValue; } }

    private string fifthDenomNumber;
    [JsonProperty(Order = 17)]
    public string FifthDenomNumber { get { return fifthDenomNumber; } }


    private string sixthDenomValue;
    [JsonProperty(Order = 18)]
    public string SixthDenomValue { get { return sixthDenomValue; } }

    private string sixthDenomNumber;
    [JsonProperty(Order = 19)]
    public string SixthDenomNumber { get { return sixthDenomNumber; } }


    private string seventhDenomValue;
    [JsonProperty(Order = 20)]
    public string SeventhDenomValue { get { return seventhDenomValue; } }

    private string seventhDenomNumber;
    [JsonProperty(Order = 21)]
    public string SeventhDenomNumber { get { return seventhDenomNumber; } }

    private string eighthDenomValue;
    [JsonProperty(Order = 22)]
    public string EighthDenomValue { get { return eighthDenomValue; } }

    private string eighthDenomNumber;
    [JsonProperty(Order = 23)]
    public string EighthDenomNumber { get { return eighthDenomNumber; } }

    private string ninthDenomValue;
    [JsonProperty(Order = 6)]
    public string NinthDenomValue { get { return ninthDenomValue; } }

    private string ninthDenomNumber;
    [JsonProperty(Order = 9)]
    public string NinthDenomNumber { get { return ninthDenomNumber; } }

    private string tenthDenomValue;
    [JsonProperty(Order = 12)]
    public string TenthDenomValue { get { return tenthDenomValue; } }

    private string tenthDenomNumber;
    [JsonProperty(Order = 15)]
    public string TenthDenomNumber { get { return tenthDenomNumber; } }

    //private string eleventhDenomValue;
    //public string EleventhDenomValue
    //{
    //    get { return eleventhDenomValue; }
    //}
    //
    //private string eleventhDenomNumber;
    //public string EleventhDenomNumber
    //{
    //    get { return eleventhDenomNumber; }
    //}

    private string kioskId;
    [JsonProperty(Order = 1)]
    public string KioskId { get { return kioskId; } }

    private string id;
    [JsonProperty(Order = 0)]
    public string Id { get { return id; } }




        private string className;
        public string ClassName { get { return className; } }


        internal KioskCashDispenserLCDMHopper(string kioskName
      , string institutionName
      , string insertionDate
      , string kioskId
      , string firstDenomValue
      , string firstDenomNumber
      , string secondDenomValue
      , string secondDenomNumber
      , string thirdDenomValue
      , string thirdDenomNumber
      , string fourthDenomValue
      , string fourthDenomNumber
      , string fifthDenomValue
      , string fifthDenomNumber
      , string sixthDenomValue
      , string sixthDenomNumber
      , string seventhDenomValue
      , string seventhDenomNumber
      , string eighthDenomValue
      , string eighthDenomNumber
      , string ninthDenomValue
      , string ninthDenomNumber
      , string tenthDenomValue
      , string tenthDenomNumber
      // , string eleventhDenomValue
      //, string eleventhDenomNumber
      //, string id
    )
    {
      this.kioskName = kioskName;
      this.institutionName = institutionName;
      this.insertionDate = insertionDate;
      this.firstDenomValue = firstDenomValue;
      this.firstDenomNumber = firstDenomNumber;
      this.secondDenomValue = secondDenomValue;
      this.secondDenomNumber = secondDenomNumber;
      this.thirdDenomValue = thirdDenomValue;
      this.thirdDenomNumber = thirdDenomNumber;
      this.fourthDenomValue = fourthDenomValue;
      this.fourthDenomNumber = fourthDenomNumber;
      this.fifthDenomValue = fifthDenomValue;
      this.fifthDenomNumber = fifthDenomNumber;
      this.sixthDenomValue = sixthDenomValue;
      this.sixthDenomNumber = sixthDenomNumber;
      this.seventhDenomValue = seventhDenomValue;
      this.seventhDenomNumber = seventhDenomNumber;
      this.eighthDenomValue = eighthDenomValue;
      this.eighthDenomNumber = eighthDenomNumber;
      this.ninthDenomValue = ninthDenomValue;
      this.ninthDenomNumber = ninthDenomNumber;
      this.tenthDenomValue = tenthDenomValue;
      this.tenthDenomNumber = tenthDenomNumber;
      //this.eleventhDenomValue = eleventhDenomValue;
      //this.eleventhDenomNumber = eleventhDenomNumber;
      //this.kioskId = kioskId;
      //this.id = id;
    }





        internal KioskCashDispenserLCDMHopper(string kioskName
      , string institutionName
      , string insertionDate
      , string kioskId
      , string firstDenomValue
      , string firstDenomNumber
      , string secondDenomValue
      , string secondDenomNumber
      , string thirdDenomValue
      , string thirdDenomNumber
      , string fourthDenomValue
      , string fourthDenomNumber
      , string fifthDenomValue
      , string fifthDenomNumber
      , string sixthDenomValue
      , string sixthDenomNumber
      , string seventhDenomValue
      , string seventhDenomNumber
      , string eighthDenomValue
      , string eighthDenomNumber
      , string ninthDenomValue
      , string ninthDenomNumber
      , string tenthDenomValue
      , string tenthDenomNumber
            ,string className
    // , string eleventhDenomValue
    //, string eleventhDenomNumber
    //, string id
    )
        {
            this.kioskName = kioskName;
            this.institutionName = institutionName;
            this.insertionDate = insertionDate;
            this.firstDenomValue = firstDenomValue;
            this.firstDenomNumber = firstDenomNumber;
            this.secondDenomValue = secondDenomValue;
            this.secondDenomNumber = secondDenomNumber;
            this.thirdDenomValue = thirdDenomValue;
            this.thirdDenomNumber = thirdDenomNumber;
            this.fourthDenomValue = fourthDenomValue;
            this.fourthDenomNumber = fourthDenomNumber;
            this.fifthDenomValue = fifthDenomValue;
            this.fifthDenomNumber = fifthDenomNumber;
            this.sixthDenomValue = sixthDenomValue;
            this.sixthDenomNumber = sixthDenomNumber;
            this.seventhDenomValue = seventhDenomValue;
            this.seventhDenomNumber = seventhDenomNumber;
            this.eighthDenomValue = eighthDenomValue;
            this.eighthDenomNumber = eighthDenomNumber;
            this.ninthDenomValue = ninthDenomValue;
            this.ninthDenomNumber = ninthDenomNumber;
            this.tenthDenomValue = tenthDenomValue;
            this.tenthDenomNumber = tenthDenomNumber;
            this.className = className;
            //this.eleventhDenomValue = eleventhDenomValue;
            //this.eleventhDenomNumber = eleventhDenomNumber;
            //this.kioskId = kioskId;
            //this.id = id;
        }
    }
}