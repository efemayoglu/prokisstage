﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Kiosks;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class POSEODCollection : ReadOnlyCollection<POSEOD>
    {

        internal POSEODCollection()
            : base(new List<POSEOD>())
        {

        }

        internal void Add(POSEOD kiosk)
        {
            if (kiosk != null && !this.Contains(kiosk))
            {
                this.Items.Add(kiosk);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (POSEOD item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("POSEOD", jArray);

            return jObjectDis;
        }

    }
}
