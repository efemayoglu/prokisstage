﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Accounts
{
    public class KioskDispenserMonitoring
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private string createruser;
        public string Createruser
        {
            get { return createruser; }
        }

        private string insertionDate;
        public string InsertionDate
        {
            get { return insertionDate; }
        }

  
        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }



        private string firstDenomNumber;
        public string FirstDenomNumber
        {
            get { return firstDenomNumber; }
        }
 
        private string secondDenomNumber;
        public string SecondDenomNumber
        {
            get { return secondDenomNumber; }
        }
 
        private string thirdDenomNumber;
        public string ThirdDenomNumber
        {
            get { return thirdDenomNumber; }
        }
 

        private string fourthDenomNumber;
        public string FourthDenomNumber
        {
            get { return fourthDenomNumber; }
        }
 

        private string fifthDenomNumber;
        public string FifthDenomNumber
        {
            get { return fifthDenomNumber; }
        } 

        private string sixthDenomNumber;
        public string SixthDenomNumber
        {
            get { return sixthDenomNumber; }
        }
 
        private string seventhDenomNumber;
        public string SeventhDenomNumber
        {
            get { return seventhDenomNumber; }
        }
         

        private string eighthDenomNumber;
        public string EighthDenomNumber
        {
            get { return eighthDenomNumber; }
        }

 
        private string ninthDenomNumber;
        public string NinthDenomNumber
        {
            get { return ninthDenomNumber; }
        }
 

        private string tenthDenomNumber;
        public string TenthDenomNumber
        {
            get { return tenthDenomNumber; }
        }
 
        private string eleventhDenomNumber;
        public string EleventhDenomNumber
        {
            get { return eleventhDenomNumber; }
        }

        /// <summary>
        /// /////
        /// </summary>
        private string firstDenomNumberOld;
        public string FirstDenomNumberOld
        {
            get { return firstDenomNumberOld; }
        }

        private string secondDenomNumberOld;
        public string SecondDenomNumberOld
        {
            get { return secondDenomNumberOld; }
        }

        private string thirdDenomNumberOld;
        public string ThirdDenomNumberOld
        {
            get { return thirdDenomNumberOld; }
        }


        private string fourthDenomNumberOld;
        public string FourthDenomNumberOld
        {
            get { return fourthDenomNumberOld; }
        }


        private string fifthDenomNumberOld;
        public string FifthDenomNumberOld
        {
            get { return fifthDenomNumberOld; }
        }

        private string sixthDenomNumberOld;
        public string SixthDenomNumberOld
        {
            get { return sixthDenomNumberOld; }
        }

        private string seventhDenomNumberOld;
        public string SeventhDenomNumberOld
        {
            get { return seventhDenomNumberOld; }
        }


        private string eighthDenomNumberOld;
        public string EighthDenomNumberOld
        {
            get { return eighthDenomNumberOld; }
        }


        private string ninthDenomNumberOld;
        public string NinthDenomNumberOld
        {
            get { return ninthDenomNumberOld; }
        }


        private string tenthDenomNumberOld;
        public string TenthDenomNumberOld
        {
            get { return tenthDenomNumberOld; }
        }

        private string eleventhDenomNumberOld;
        public string EleventhDenomNumberOld
        {
            get { return eleventhDenomNumberOld; }
        }

        private string casette1Reject;
        public string Casette1Reject
        {
            get { return casette1Reject; }
        }

        private string casette2Reject;
        public string Casette2Reject
        {
            get { return casette2Reject; }
        }

        private string casette3Reject;
        public string Casette3Reject
        {
            get { return casette3Reject; }
        }

        private string casette4Reject;
        public string Casette4Reject
        {
            get { return casette4Reject; }
        }

        private string casette1RejectOld;
        public string Casette1RejectOld
        {
            get { return casette1RejectOld; }
        }

        private string casette2RejectOld;
        public string Casette2RejectOld
        {
            get { return casette2RejectOld; }
        }

        private string casette3RejectOld;
        public string Casette3RejectOld
        {
            get { return casette3RejectOld; }
        }

        private string casette4RejectOld;
        public string Casette4RejectOld
        {
            get { return casette4RejectOld; }
        }
        private string className;
        public string ClassName
        {
            get { return className; }
        }

        internal KioskDispenserMonitoring(int id
            , string createruser
            , string insertionDate
            , string kioskName
            , string firstDenomNumber
            , string secondDenomNumber
            , string thirdDenomNumber
            , string fourthDenomNumber
            , string fifthDenomNumber
            , string sixthDenomNumber
            , string seventhDenomNumber
            , string eighthDenomNumber
            , string casette1Reject
            , string casette2Reject
            , string casette3Reject
            , string casette4Reject
            , string firstDenomNumberOld
            , string secondDenomNumberOld
            , string thirdDenomNumberOld
            , string fourthDenomNumberOld
            , string fifthDenomNumberOld
            , string sixthDenomNumberOld
            , string seventhDenomNumberOld
            , string eighthDenomNumberOld
            , string casette1RejectOld
            , string casette2RejectOld
            , string casette3RejectOld
            , string casette4RejectOld
            , string className=""
            )
        {
            this.id = id;
            this.createruser = createruser;
            this.insertionDate = insertionDate;
            this.kioskName = kioskName;
            this.firstDenomNumber = firstDenomNumber;
            this.secondDenomNumber = secondDenomNumber;
            this.thirdDenomNumber = thirdDenomNumber;
            this.fourthDenomNumber = fourthDenomNumber;
            this.fifthDenomNumber = fifthDenomNumber;
            this.sixthDenomNumber = sixthDenomNumber;
            this.seventhDenomNumber = seventhDenomNumber;
            this.eighthDenomNumber = eighthDenomNumber;
            this.casette1Reject = casette1Reject;
            this.casette2Reject = casette2Reject;
            this.casette3Reject = casette3Reject;
            this.casette4Reject = casette4Reject;
            this.firstDenomNumberOld = firstDenomNumberOld;
            this.secondDenomNumberOld = secondDenomNumberOld;
            this.thirdDenomNumberOld = thirdDenomNumberOld;
            this.fourthDenomNumberOld = fourthDenomNumberOld;
            this.fifthDenomNumberOld = fifthDenomNumberOld;
            this.sixthDenomNumberOld = sixthDenomNumberOld;
            this.seventhDenomNumberOld = seventhDenomNumberOld;
            this.eighthDenomNumberOld = eighthDenomNumberOld;
            this.casette1RejectOld = casette1RejectOld;
            this.casette2RejectOld = casette2RejectOld;
            this.casette3RejectOld = casette3RejectOld;
            this.casette4RejectOld = casette4RejectOld;
            this.className = className;
        }
    }
}
