﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.BankActions
{
    public class DBAskiActionsFromService
    {
        public int errorCode { get; set; }
        public List<DBAskiActionsFromServiceDetail> actionDetails { get; set; }

        public DBAskiActionsFromService()
        {

        }

        public DBAskiActionsFromService(int errorCode, List<DBAskiActionsFromServiceDetail> actionDetails)
        {
            this.errorCode = errorCode;
            this.actionDetails = actionDetails;
        }
    }
    public class DBAskiActionsFromServiceDetail
    {
        public string bankReferenceNumber { get; set; }
        public string insProcessDate { get; set; }
        public string faturaNumber { get; set; }
        public decimal institutionAmount { get; set; }
        public string status { get; set; }
        public string customerNumber { get; set; }

        public DBAskiActionsFromServiceDetail(string bankReferenceNumber,
                                      string insProcessDate,
                                      decimal institutionAmount,
                                      string status,
                                      string faturaNumber,
                                      string customerNumber)
        {
            this.bankReferenceNumber = bankReferenceNumber;
            this.insProcessDate = insProcessDate;
            this.institutionAmount = institutionAmount;
            this.status = status;
            this.customerNumber = customerNumber;
            this.faturaNumber = faturaNumber;
        }

        public DBAskiActionsFromServiceDetail()
        { }
    }
}
