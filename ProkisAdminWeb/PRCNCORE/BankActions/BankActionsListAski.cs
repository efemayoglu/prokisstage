﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.BankActions
{
    public class BankActionsListAski
    {
        public int errorCode { get; set; }
        public DataTable actionDetails { get; set; }

        public BankActionsListAski()
        {

        }

        public BankActionsListAski(int errorCode, DataTable actionDetails)
        {
            this.errorCode=errorCode;
            this.actionDetails=actionDetails;
        }
    }
    public class BankActionListAskiList
    {
        public string bankReferenceNumber { get; set; }
        public string insProcessDate { get; set; }
        public string kioskName { get; set; }
        public string transactionId { get; set; }
        public int institutionId { get; set; }
        public decimal institutionAmount { get; set; }
        public string status { get; set; }
        public string customerId { get; set; }

        public BankActionListAskiList(string bankReferenceNumber,
                                      string insProcessDate,
                                      string kioskName,
                                      string transactionId,
                                      int institutionId,
                                      decimal institutionAmount,
                                      string status,
                                      string customerId)
        {
            this.bankReferenceNumber=bankReferenceNumber;
            this.insProcessDate=insProcessDate;
            this.kioskName=kioskName;
            this.transactionId=transactionId;
            this.institutionId=institutionId;
            this.institutionAmount=institutionAmount;
            this.status=status;
            this.customerId=customerId;
        }

        public BankActionListAskiList()
        {}
    }

}
