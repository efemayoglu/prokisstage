﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.KioskCommands
{
    public class KioskCommand
    {
        private int commandId;
        public int CommandId
        {
            get { return commandId; }
        }

        private string kioskName;
        public string KioskName
        {
            get { return kioskName; }
        }

        private string createdUserName;
        public string CreatedUserName
        {
            get { return createdUserName; }
        }

        private string createdDate;
        public string CreatedDate
        {
            get { return createdDate; }
        }

        private string commandType;
        public string CommandType
        {
            get { return commandType; }
        }

        private string commandString;
        public string CommandString
        {
            get { return commandString; }
        }

        private string executedDate;
        public string ExecutedDate
        {
            get { return executedDate; }
        }

        private string status;
        public string Status
        {
            get { return status; }
        }

        private string commandReason;
        public string CommandReason
        {
            get { return commandReason; }
        }

        private string explanation;
        public string Explanation
        {
            get { return explanation; }
        }

        private string commandReasonId;
        public string CommandReasonId
        {
            get { return commandReasonId; }
        }

        private string commandTypeId;
        public string CommandTypeId
        {
            get { return commandTypeId; }
        }

         private string kioskId;
         public string KioskId
         {
             get { return kioskId; }
         }

        private string institution;
        public string Institution
        {
            get { return institution; }
        }

        private string statusId;
        public string StatusId
        {
            get { return statusId; }
        }


        internal KioskCommand(int commandId
            , string kioskName
            , string createdUserName
            , string createdDate
            , string commandType
            , string commandString
            , string executedDate
            , string status
            , string commandReason
            , string explanation
            , string commandReasonId
            , string commandTypeId
            , string kioskId
            , string institution
            , string statusId)
        {
            this.commandId = commandId;
            this.kioskName = kioskName;
            this.createdUserName = createdUserName;
            this.createdDate = createdDate;
            this.commandType = commandType;
            this.commandString = commandString;
            this.executedDate = executedDate;
            this.status = status;
            this.commandReason = commandReason;
            this.explanation = explanation;
            this.commandReasonId = commandReasonId;
            this.commandTypeId = commandTypeId;
            this.kioskId = kioskId;
            this.institution = institution;
            this.statusId = statusId;
        }
    }
}
