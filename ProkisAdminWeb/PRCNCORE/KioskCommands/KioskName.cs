﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.KioskCommands
{
    public class KioskName
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private string name;
        public string Name
        {
            get { return name; }
        }

        internal KioskName(int id
            , string name)
        {
            this.id = id;
            this.name = name;
        }

    }

    public class KioskDetail
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private string name;
        public string Name
        {
            get { return name; }
        }

        private string latitude;
        public string Latitude
        {
            get { return latitude; }
        }

        private string longitude;
        public string Longitude
        {
            get { return longitude; }
        }

        private string address;
        public string Address
        {
            get { return address; }
        }

        internal KioskDetail(int id
            , string name
            , string latitude
            , string longitude
            , string address)
        {
            this.id = id;
            this.name = name;
            this.latitude = latitude;
            this.longitude = longitude;
            this.address = address;
        }

    }
}
