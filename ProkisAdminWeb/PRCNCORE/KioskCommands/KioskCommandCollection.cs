﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.KioskCommands
{
    public class KioskCommandCollection: ReadOnlyCollection<KioskCommand>
    {
        internal KioskCommandCollection()
            : base(new List<KioskCommand>())
        {

        }

        internal void Add(KioskCommand kioskCommand)
        {
            if (kioskCommand != null && !this.Contains(kioskCommand))
            {
                this.Items.Add(kioskCommand);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (KioskCommand item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("KioskCommands", jArray);

            return jObjectDis;
        }

    }
}
