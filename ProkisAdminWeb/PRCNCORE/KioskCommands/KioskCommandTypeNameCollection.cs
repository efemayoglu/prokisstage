﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.KioskCommands
{
    public class KioskCommandTypeNameCollection : ReadOnlyCollection<KioskCommandTypeName>
    {
        internal KioskCommandTypeNameCollection()
            : base(new List<KioskCommandTypeName>())
        {

        }

        internal void Add(KioskCommandTypeName kioskCommandType)
        {
            if (kioskCommandType != null && !this.Contains(kioskCommandType))
            {
                this.Items.Add(kioskCommandType);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (KioskCommandTypeName item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("CommandTypeNames", jArray);

            return jObjectDis;
        }

    }

    public class KioskCommandReasonCollection : ReadOnlyCollection<KioskCommandReasonName>
    {
        internal KioskCommandReasonCollection()
            : base(new List<KioskCommandReasonName>())
        {

        }

        internal void Add(KioskCommandReasonName kioskCommandType)
        {
            if (kioskCommandType != null && !this.Contains(kioskCommandType))
            {
                this.Items.Add(kioskCommandType);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (KioskCommandReasonName item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("CommandReasonNames", jArray);

            return jObjectDis;
        }

    }
}
