﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.KioskCommands
{
    public class InstutionNameCollection : ReadOnlyCollection<InstutionName>
    {
        internal InstutionNameCollection()
            : base(new List<InstutionName>())
        {

        }

        internal void Add(InstutionName name)
        {
            if (name != null && !this.Contains(name))
            {
                this.Items.Add(name);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (InstutionName item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("InstutionNames", jArray);

            return jObjectDis;
        }

    }

    public class BankNameCollection : ReadOnlyCollection<BankName>
    {
        internal BankNameCollection()
            : base(new List<BankName>())
        {

        }

        internal void Add(BankName name)
        {
            if (name != null && !this.Contains(name))
            {
                this.Items.Add(name);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (BankName item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("BankNames", jArray);

            return jObjectDis;
        }

    }

    public class CompleteStatusNameCollection : ReadOnlyCollection<CompleteStatusName>
    {
        internal CompleteStatusNameCollection()
            : base(new List<CompleteStatusName>())
        {

        }

        internal void Add(CompleteStatusName name)
        {
            if (name != null && !this.Contains(name))
            {
                this.Items.Add(name);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (CompleteStatusName item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("CompleteStatusName", jArray);

            return jObjectDis;
        }

    }

    public class ProcessTypeCollection : ReadOnlyCollection<ProcessType>
    {
        internal ProcessTypeCollection()
            : base(new List<ProcessType>())
        {

        }

        internal void Add(ProcessType name)
        {
            if (name != null && !this.Contains(name))
            {
                this.Items.Add(name);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (ProcessType item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("ProcessTypes", jArray);

            return jObjectDis;
        }

    }
}
