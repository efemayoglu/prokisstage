﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.KioskCommands
{
    public class InstutionName
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private string name;
        public string Name
        {
            get { return name; }
        }

        internal InstutionName(int id
            , string name)
        {
            this.id = id;
            this.name = name;
        }
    }

    public class BankName
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private string name;
        public string Name
        {
            get { return name; }
        }

        internal BankName(int id, string name)
        {
            this.id = id;
            this.name = name;
        }
    }



    public class CompleteStatusName
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private string name;
        public string Name
        {
            get { return name; }
        }

        internal CompleteStatusName(int id
            , string name)
        {
            this.id = id;
            this.name = name;
        }
    }

    public class ProcessType
    {
        private int id;
        public int Id
        {
            get { return id; }
        }

        private string name;
        public string Name
        {
            get { return name; }
        }

        internal ProcessType(int id
            , string name)
        {
            this.id = id;
            this.name = name;
        }
    }
}
