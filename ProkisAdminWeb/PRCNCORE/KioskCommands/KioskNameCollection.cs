﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.KioskCommands
{
    public class KioskNameCollection : ReadOnlyCollection<KioskName>
    {
        internal KioskNameCollection()
            : base(new List<KioskName>())
        {

        }

        internal void Add(KioskName kioskName)
        {
            if (kioskName != null && !this.Contains(kioskName))
            {
                this.Items.Add(kioskName);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (KioskName item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("KioskNames", jArray);

            return jObjectDis;
        }

    }

    public class KioskDetailCollection : ReadOnlyCollection<KioskDetail>
    {
        internal KioskDetailCollection()
            : base(new List<KioskDetail>())
        {

        }

        internal void Add(KioskDetail kioskName)
        {
            if (kioskName != null && !this.Contains(kioskName))
            {
                this.Items.Add(kioskName);
            }
        }

        public JObject GetJSON()
        {
            JObject jObjectDis = new JObject();
            JArray jArray = new JArray();

            foreach (KioskDetail item in this)
            {
                JObject jo = new JObject();

                string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

                JObject x = JObject.Parse(a);
                jArray.Add(x);
            }

            jObjectDis.Add("KioskDetails", jArray);

            return jObjectDis;
        }

    }
}
