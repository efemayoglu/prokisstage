﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Menus
{
    public class Menu
    {
        private int id;

        public int Id
        {
            get { return this.id; }
        }

        private string name;

        public string Name
        {
            get { return this.name; }
        }

        internal Menu(int id, string name)
        {
            this.id = id;
            this.name = name;
        }
    }
}
