﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PRCNTEST.ReportingWebService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ReportingWebService.DesmerNotificationSoap")]
    public interface DesmerNotificationSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ReportTalepChange", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        string ReportTalepChange(string apiUser, string apiPass, string referansId, int talepDurum, string talepTipi, decimal tutar, PRCNTEST.ReportingWebService.Kupur[] kupurler, PRCNTEST.ReportingWebService.Personnel[] personel, string aciklama);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ReportTalepChange", ReplyAction="*")]
        System.Threading.Tasks.Task<string> ReportTalepChangeAsync(string apiUser, string apiPass, string referansId, int talepDurum, string talepTipi, decimal tutar, PRCNTEST.ReportingWebService.Kupur[] kupurler, PRCNTEST.ReportingWebService.Personnel[] personel, string aciklama);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2556.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class Kupur : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string kupurKodField;
        
        private int adetField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string KupurKod {
            get {
                return this.kupurKodField;
            }
            set {
                this.kupurKodField = value;
                this.RaisePropertyChanged("KupurKod");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public int Adet {
            get {
                return this.adetField;
            }
            set {
                this.adetField = value;
                this.RaisePropertyChanged("Adet");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.2556.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class Personnel : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string tcKimlikField;
        
        private string adiSoyadiField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string TcKimlik {
            get {
                return this.tcKimlikField;
            }
            set {
                this.tcKimlikField = value;
                this.RaisePropertyChanged("TcKimlik");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string AdiSoyadi {
            get {
                return this.adiSoyadiField;
            }
            set {
                this.adiSoyadiField = value;
                this.RaisePropertyChanged("AdiSoyadi");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface DesmerNotificationSoapChannel : PRCNTEST.ReportingWebService.DesmerNotificationSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class DesmerNotificationSoapClient : System.ServiceModel.ClientBase<PRCNTEST.ReportingWebService.DesmerNotificationSoap>, PRCNTEST.ReportingWebService.DesmerNotificationSoap {
        
        public DesmerNotificationSoapClient() {
        }
        
        public DesmerNotificationSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public DesmerNotificationSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DesmerNotificationSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DesmerNotificationSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string ReportTalepChange(string apiUser, string apiPass, string referansId, int talepDurum, string talepTipi, decimal tutar, PRCNTEST.ReportingWebService.Kupur[] kupurler, PRCNTEST.ReportingWebService.Personnel[] personel, string aciklama) {
            return base.Channel.ReportTalepChange(apiUser, apiPass, referansId, talepDurum, talepTipi, tutar, kupurler, personel, aciklama);
        }
        
        public System.Threading.Tasks.Task<string> ReportTalepChangeAsync(string apiUser, string apiPass, string referansId, int talepDurum, string talepTipi, decimal tutar, PRCNTEST.ReportingWebService.Kupur[] kupurler, PRCNTEST.ReportingWebService.Personnel[] personel, string aciklama) {
            return base.Channel.ReportTalepChangeAsync(apiUser, apiPass, referansId, talepDurum, talepTipi, tutar, kupurler, personel, aciklama);
        }
    }
}
