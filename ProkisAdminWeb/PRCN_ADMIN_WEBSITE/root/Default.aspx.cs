﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class root_Default : System.Web.UI.Page 
{
    private ParserLogin LoginDatas = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas == null)
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        ControlAccessToken();
    }

    private void ControlAccessToken()
    {
        CallWebServices callWebServ = new CallWebServices();
        ParserOperation operationResult = callWebServ.CallControlAccessTokenService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

        if (operationResult != null)
        {
            if (operationResult.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
            {
                Session.Abandon();
                Session.RemoveAll();
                Response.Redirect("../root/Login.aspx", true);
            }
        }
    }
}
