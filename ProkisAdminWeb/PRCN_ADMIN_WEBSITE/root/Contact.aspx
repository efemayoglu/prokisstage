﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="root_Contact" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp</div>
        <div align="left" class="windowTitle_container_autox30">
            İletişim Formu<hr style="border-bottom: 1px solid #b2b2b4;" />
        </div>
        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSubmit">
            <p>
                İletişime geçmek için lütfen aşağıdaki alanları doldurunuz.
            </p>
            <p>
                Adınız ve Soyadınız:  
        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"
            ControlToValidate="YourName" ValidationGroup="save" /><br />
                <asp:TextBox ID="YourName" runat="server" Width="250px" /><br />
                E-mail adresiniz:
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
            ControlToValidate="YourEmail" ValidationGroup="save" /><br />
                <asp:TextBox ID="YourEmail" runat="server" Width="250px" />
                <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator23"
                    SetFocusOnError="true" Text="örnek: info@birlesikodeme.com" ControlToValidate="YourEmail"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"
                    ValidationGroup="save" /><br />
                Konu:
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
            ControlToValidate="YourSubject" ValidationGroup="save" /><br />
                <asp:TextBox ID="YourSubject" runat="server" Width="400px" /><br />
                Sorunuz:
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
            ControlToValidate="Comments" ValidationGroup="save" /><br />
                <asp:TextBox ID="Comments" runat="server"
                    TextMode="MultiLine" Rows="10" Width="400px" />
            </p>
            <p>
                <asp:Button ID="btnSubmit" runat="server" Text="Gönder"
                    OnClick="btnSubmit_Click" ValidationGroup="save" />
            </p>
        </asp:Panel>
    </form>
</body>
</html>
