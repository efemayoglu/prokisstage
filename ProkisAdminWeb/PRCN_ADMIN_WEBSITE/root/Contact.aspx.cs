﻿using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class root_Contact : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
 
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string[] recievers = { this.YourEmail.Text };
            Utility.SendEmail("info@enplaka.com", this.YourName.Text, recievers, "Konu: " + this.YourSubject.Text, "Açıklama: " + this.Comments.Text, null);
        }
        catch
        {
            //If the message failed at some point, let the user know
            messageArea.InnerHtml = "Your message failed to send, please try again.";
        }
    }
}
