using System;
using System.Security.Principal;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using PRCNCORE.Utilities;
using System.Net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using PRCNCORE.Parser;

public partial class Login : System.Web.UI.Page
{

    int UserPasswordChangePeriodDay = 0;
    int UserLoginPeriodDay = 0;
    int WrongAccessTryLimit = 0;
    int UnluckWrongAccessTryMinute = 0;

    ParserLogin LoginDatas = null;

    protected void Page_Load(object sender, System.EventArgs e)
    {

        //this.UserPasswordChangePeriodDay = 60;
        //this.UserLoginPeriodDay = 90;
        //this.WrongAccessTryLimit = 6;
        //this.UnluckWrongAccessTryMinute = 30; 

        this.UserPasswordChangePeriodDay = Convert.ToInt32(Utility.GetConfigValue("UserPasswordChangePeriod"));
        this.UserLoginPeriodDay = Convert.ToInt32(Utility.GetConfigValue("UserLoginPeriod"));
        this.WrongAccessTryLimit = Convert.ToInt32(Utility.GetConfigValue("WrongAccessTryLimit"));
        this.UnluckWrongAccessTryMinute = Convert.ToInt32(Utility.GetConfigValue("UnluckWrongAccessTry"));

        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }

    private void BindCtrls()
    {
        this.BuildControls();
        this.SetControls();
    }

    private void BuildControls()
    {

    }

    private void SetControls()
    {
        Session.RemoveAll();
        this.SetTitleLabel();
        this.usernameField.Focus();  //srknpkr
        this.RemoveLogoutButton();
    }

    private void SetTitleLabel()
    {

    }

    private void RemoveLogoutButton()
    {
        ContentPlaceHolder mpContentPlaceHolder;
        ImageButton mpTextBox;
        mpContentPlaceHolder =
          (ContentPlaceHolder)Master.FindControl("pageContent");
        if (mpContentPlaceHolder != null)
        {
            mpTextBox =
                (ImageButton)mpContentPlaceHolder.FindControl("pageContent");
            if (mpTextBox != null)
            {
                //mpTextBox.Text = "TextBox found!";
            }
        }

        ImageButton mpLabel = (ImageButton)Master.FindControl("logoutImage");
        if (mpLabel != null)
        {
            mpLabel.Visible = false;
        }
    }

    private void ProcessLogin()
    {
        try
        {
            CallWebServices callWebServ = new CallWebServices();
            this.LoginDatas = callWebServ.CallLoginService(this.usernameField.Text,
                Utility.CalculateSHA512Pin(this.passwordField.Text),
                this.UnluckWrongAccessTryMinute,
                this.WrongAccessTryLimit,
                this.UserLoginPeriodDay);


            if (LoginDatas != null)
            {
                if (LoginDatas.errorCode == 0)
                {
                    if (LoginDatas.User == null)
                    {
                        this.messageArea.InnerHtml = "Kullanıcı adı veya şifre hatalı.";
                    }
                    else
                    {

                        if (LoginDatas.User.PasswordMustChangeId == 0)
                        {
                            if (LoginDatas.User.LastPasswordChangeDate > DateTime.Now.AddDays(-90))
                            {
                                Session.Add("LoginData", LoginDatas);
                                FormsAuthentication.RedirectFromLoginPage(LoginDatas.User.Name, false);

                            }
                            else
                            {
                                Session.Add("TempLoginData", LoginDatas);
                                FormsAuthentication.RedirectFromLoginPage(LoginDatas.User.Name, false);
                                Response.Redirect("../User/ChangePasswordLogin.aspx");
                            }

                        }
                        else
                        {
                            Session.Add("TempLoginData", LoginDatas);
                            FormsAuthentication.RedirectFromLoginPage(LoginDatas.User.Name, false);
                            Response.Redirect("../User/ChangePasswordLogin.aspx");
                        }
                    }
                }
                else
                {
                    this.messageArea.InnerHtml = LoginDatas.errorDescription;
                }
            }
            else
            {
                this.messageArea.InnerHtml = "Sisteme Erişilemiyor.";
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "LoginPL");
        }
    }

    protected void loginButtonHref_Click(object sender, EventArgs e)
    {
        this.ProcessLogin();
    }

    protected void loginButtonLink_Click(object sender, EventArgs e)
    {
        this.ProcessLogin();
    }
}
