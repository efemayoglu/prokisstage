﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowCommisionSetting.aspx.cs" Inherits="Settings_ShowCommisionSetting" %>

<%@ Register Src="../MS_Control/MultipleSelection.ascx" TagName="MultipleSelection" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <link rel="stylesheet" href="../styles/style.css" />

    <title></title>

</head>
<body>

    <form id="form1" runat="server">

          <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel7" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <div align="left" id="articleDetailsTab" class="windowTitle_container_autox30">
                    Kurum Komisyon Detayları /  <asp:Label   ID="instutionTextField" runat="server" />
                    <hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>


                <asp:Table ID="itemsTable" runat="server" CellPadding="0" CellSpacing="0" BorderWidth="0px"
                                        BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="95%">


                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" Style="height: 30px;">
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty" Text="Kredi Kartı Komisyon" Style="color: black; text-align: left;"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty" Style="width: 10px;"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell>
                                                <asp:TextBox TextMode="Number" ID="creditTextField" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9" min="1" max="1000" step="1" value="0" />
                                            </asp:TableHeaderCell>
                                        </asp:TableRow>
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" Style="height: 30px;">
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty" Text="Mobil Ödeme Komisyon:" Style="color: black; text-align: left;"></asp:TableHeaderCell><asp:TableHeaderCell CssClass="inputTitleCellEmpty" Style="width: 10px;"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell>
                                                <asp:TextBox TextMode="Number" ID="mobileTextField" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9" min="1" max="1000" step="1" value="0" />
                                            </asp:TableHeaderCell>
                                        </asp:TableRow>
 
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" Style="height: 30px;">
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty" Text="Nakit Yükleme Komisyon :" Style="color: black; text-align: left;"></asp:TableHeaderCell><asp:TableHeaderCell CssClass="inputTitleCellEmpty" Style="width: 10px;"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell>
                                                <asp:TextBox TextMode="Number" ID="cashTextField" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9" min="1" max="20000"  step="1" value="0" />
                                            </asp:TableHeaderCell>
                                        </asp:TableRow>
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" Style="height: 30px;">
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty" Text="Sarı Alarm :" Style="color: black; text-align: left; width: 250px"></asp:TableHeaderCell><asp:TableHeaderCell CssClass="inputTitleCellEmpty" Style="width: 10px;"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell>
                                                <asp:TextBox TextMode="Number" ID="yellowTextField" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9" min="1" max="500000"  step="1" value="0" />
                                            </asp:TableHeaderCell>
                                        </asp:TableRow>
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" Style="height: 30px;">
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty" Text="Kırmızı Alarm:" Style="color: black; text-align: left; width: 250px"></asp:TableHeaderCell><asp:TableHeaderCell CssClass="inputTitleCellEmpty" Style="width: 10px;"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell>
                                                <asp:TextBox TextMode="Number" ID="redTextField" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9" min="1" max="500000" step="1" value="0" />
                                            </asp:TableHeaderCell>
                                        </asp:TableRow>
 
                                    </asp:Table>
                <br />
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Kaydet"></asp:Button>

                                <asp:Button ID="cancelButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClientClick="parent.hideModalPopup2_Exit();" Text="Vazgeç"></asp:Button>

                            </td>
                        </tr>
                    </table>
                </td>
            </asp:Panel>
        </div>


    </form>

</body>
</html>
