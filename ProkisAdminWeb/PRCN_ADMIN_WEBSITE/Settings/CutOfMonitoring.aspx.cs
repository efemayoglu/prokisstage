﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Account;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Settings_CutOfMonitoring : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;
    private int ownSubMenuIndex = -1;

    private int pageNum;
    private int numberOfItemsPerPage = 20;


    private int orderSelectionColumn = 2;
    private int orderSelectionDescAsc = 1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Settings/CutOfMonitoring.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }

        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);
        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));


        if (!Page.IsPostBack)
        {

            CallWebServices callWebServ = new CallWebServices();


            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {

                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }

            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {
                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();
            }


            ListItem item = new ListItem();
            item.Text = "Kurum Seçiniz";
            item.Value = "0";
            this.instutionBox.Items.Add(item);
            this.instutionBox.SelectedValue = "0";

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.startDateField.Text = DateTime.Now.AddDays(-6).ToString("yyyy-MM-dd") + " 00:00:00";
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00";

            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;
            this.pageNum = 0;
            
            this.SearchOrder(7, 1); 

        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(7, 1);
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskId = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskId = "0";
            else
                kioskId = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebService = new CallWebServices();
            ParserListKioskCutOfMonitoring items = callWebService.CutOffMonitoring(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                              Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             kioskId,
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id
                                                                             ,1);

            if (items != null)
            {
                if (items.errorCode == 0 || items.errorCode == 2)
                {
                    this.BindListTable(items);
                }
                else if (items.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + items.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskCutOffMonitoringSrch");
        }
    }


    private void BindListTable(ParserListKioskCutOfMonitoring items)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);
            int hour = 0;
            int minute = 0;
            int second = 0;

            if (items.Cuts != null)
            {

                TableRow[] Row = new TableRow[items.Cuts.Count];
                int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
                for (int i = 0; i < Row.Length; i++)
                {

                    Row[i] = new TableRow();

                    TableCell indexCell = new TableCell();
                    TableCell startDateCell = new TableCell();
                    TableCell endDateCell = new TableCell();
                    TableCell durationCutCell = new TableCell();
                    TableCell institutionCell = new TableCell();
                    TableCell cutReasonCell = new TableCell();
                    TableCell enteredUserCell = new TableCell();
                    TableCell enteredDateCell = new TableCell();
                    TableCell kioskNameCell = new TableCell();


                    indexCell.CssClass = "inputTitleCell4";
                    startDateCell.CssClass = "inputTitleCell4";
                    endDateCell.CssClass = "inputTitleCell4";
                    durationCutCell.CssClass = "inputTitleCell4";

                    institutionCell.CssClass = "inputTitleCell4";
                    cutReasonCell.CssClass = "inputTitleCell4";
                    enteredUserCell.CssClass = "inputTitleCell4";
                    enteredDateCell.CssClass = "inputTitleCell4";
                    kioskNameCell.CssClass = "inputTitleCell4";

                    indexCell.Text = (startIndex + i).ToString();
                    //indexCell.Text = items.Cuts[i].cutId;

                    startDateCell.Text  = items.Cuts[i].startDate;
                    endDateCell.Text    = items.Cuts[i].endDate;
                    durationCutCell.Text    = items.Cuts[i].cutDuration;

                    institutionCell.Text  = items.Cuts[i].institution;
                    cutReasonCell.Text      = items.Cuts[i].cutReason;
                    enteredUserCell.Text    = items.Cuts[i].enteredUser;
                    enteredDateCell.Text    = items.Cuts[i].enteredDate;
                    //kioskNameCell.Text = items.Cuts[i].kioskName;
                    kioskNameCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editCutOfMonitoringButtonClicked(" + items.Cuts[i].cutId + ");\" class=\"anylink\">" + items.Cuts[i].kioskName + "</a>";
 

                    //toplam süre hesaplama
                    string[] totaltime = items.Cuts[i].cutDuration.ToString().Split(':');

                    hour   += Convert.ToInt32(totaltime[0]);
                    minute += Convert.ToInt32(totaltime[1]);
                    second += Convert.ToInt32(totaltime[2]);


                    Row[i].Cells.AddRange(new TableCell[]{
                    indexCell,
                    kioskNameCell,
                    startDateCell,
                    endDateCell,
                    durationCutCell,
                    
                    institutionCell,  
                    cutReasonCell,
                    enteredUserCell,                
                    enteredDateCell               

                });


                    //if (i == Row.Length - 1)
                    //{
                    //    Row[i].CssClass = "inputTitleCell99";
                    //    Row[i].Cells[1].Text = "TOPLAM";
                    //    Row[i].Cells[0].Text = "";
                    //}
                    //else
                    //{
                    if (i % 2 == 0)
                        Row[i].CssClass = "listrow";
                    else
                        Row[i].CssClass = "listRowAlternate";
                    //}

                    
                }

                if (second > 59)
                {
                    int secondnew= second;
                    second = secondnew % 60;

                    minute = minute + (secondnew/60);
                }

                if (minute > 59)
                {
                    int minutenew = minute;
                    minute = minutenew % 60;

                    hour = hour + (minutenew / 60);
                }

               



                this.itemsTable.Rows.AddRange(Row);

                TableRow pagingRow = new TableRow();
                TableCell pagingCell = new TableCell();

                int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
                int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

                if (items.recordCount < currentRecordEnd)
                    currentRecordEnd = items.recordCount;

                if (currentRecordEnd > 0)
                {
                    this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + items.recordCount.ToString() + " kayıt bulundu.";
                }





                //items.pageCount = 10;
                //sayfalama
                pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
                pagingCell.HorizontalAlign = HorizontalAlign.Right;
                pagingCell.Text = WebUtilities.GetPagingText(items.pageCount, this.pageNum, items.recordCount);
                pagingRow.Cells.Add(pagingCell);
                this.itemsTable.Rows.AddAt(0, pagingRow);

                this.itemsTable.Visible = true;


                // if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
                //  {
                TableRow addNewRow = new TableRow();

                TableCell addNewCell = new TableCell();
                TableCell spaceCell = new TableCell();
                TableCell spaceCell2 = new TableCell();
                TableCell totalCutofTimeCell = new TableCell();

                spaceCell.CssClass = "inputTitleCell99";
                spaceCell2.CssClass = "inputTitleCell99";
                addNewCell.CssClass = "inputTitleCell99";
                totalCutofTimeCell.CssClass = "inputTitleCell99";

                spaceCell.ColumnSpan = 4;
                totalCutofTimeCell.ColumnSpan = 1;
                spaceCell2.ColumnSpan = 3;

                addNewCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editCutOfMonitoringButtonClicked('0');\" class=\"anylink\"><img src=../images/add.png border=0 title=\"Add New...\" /></a>";

                totalCutofTimeCell.Text = hour.ToString() + ":" + minute.ToString() + ":" + second.ToString();
                
                //addnewRecord.Text = "<a href=\"javascript:void(0);\" onclick=\"editCutOfMonitoringButtonClicked();\" class=\"anylink\"><img src=../images/add.png border=0 title=\"Add New...\" /></a>";

                addNewRow.Cells.Add(spaceCell);
                addNewRow.Cells.Add(totalCutofTimeCell);
                addNewRow.Cells.Add(spaceCell2);
                addNewRow.Cells.Add(addNewCell);
                this.itemsTable.Rows.Add(addNewRow);
                // }
            }
            else
            {
                TableRow addNewRow = new TableRow();
                TableCell addNewCell = new TableCell();
                TableCell spaceCell = new TableCell();
                spaceCell.CssClass = "inputTitleCell4";
                spaceCell.ColumnSpan = 8;

                addNewCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editCutOfMonitoringButtonClicked('0');\" class=\"anylink\"><img src=../images/add.png border=0 title=\"Add New...\" /></a>";

                addNewRow.Cells.Add(spaceCell);
                addNewRow.Cells.Add(addNewCell);
                this.itemsTable.Rows.Add(addNewRow);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListMutabakatBDT");
        }
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskId = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskId = "0";
            else
                kioskId = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebService = new CallWebServices();
            ParserListKioskCutOfMonitoring items = callWebService.CutOffMonitoring(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text).AddDays(0).AddSeconds(-1),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             Convert.ToInt32(ViewState["OrderDesc"]),
                                                                             kioskId,//
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id
                                                                             , 2);

            if (items != null)
            {
                if (items.errorCode == 0)
                {

                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    string[] headerNames = { "Başlangıç Tarih", "Bitiş Tarihi ", "Kesinti Süresi", "Kiosk", "Kurum", "Kesinti Nedeni", "Giriş Yapan Kullanıcı",
                                               "Giriş Yapılma Tarihi", "#5 / 10 TL", "#6 / 5 TL", "#7 / 1 TL", "#8 / 50 Kuruş", 
                                               "#9 / 25 Kuruş", "#10 / 10 Kuruş", "#11 / 5 Kuruş", "Bildiren Kullanıcı", "Giriş Yapan Kullanıcı", "Giriş Yapılma Tarih", "Toplam"};

                     
                    exportExcell.ExportExcellByBlock(items.Cuts, "Kiosk Kesinti Takip Listesi", headerNames);
                }
                else if (items.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + items.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelCutOffMonitoring");
        }
    }


    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "StartDate":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Name.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Name.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Name.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "EndDate":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "DurationCut":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Name.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Name.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Name.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "InstitutionId":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CutReason":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Ip.Text = "<a>Kiosk IP    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Ip.Text = "<a>Kiosk IP    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Ip.Text = "<a>Kiosk IP    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "EnteredUser":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Status.Text = "<a>Kiosk Durum    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Status.Text = "<a>Kiosk Durum    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Status.Text = "<a>Kiosk Durum    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "EnteredDate":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //InsertionDate.Text = "<a>Eklenme Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // InsertionDate.Text = "<a>Eklenme Tarihi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //InsertionDate.Text = "<a>Eklenme Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "KioskName":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //InsertionDate.Text = "<a>Eklenme Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // InsertionDate.Text = "<a>Eklenme Tarihi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //InsertionDate.Text = "<a>Eklenme Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;

            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }


        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

}