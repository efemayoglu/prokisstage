﻿<%@ Page Language="C#" CodeFile="ListSetting.aspx.cs" Inherits="Settings_ListSetting" MasterPageFile="~/webctrls/AdminSiteMPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit5" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit6" %>





<%@ Register Src="../MS_Control/MultipleSelection.ascx" TagName="MultipleSelection" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<%@ Register Namespace="ClickableWebControl" TagPrefix="clkweb" %>
<asp:Content ID="pageContent" ContentPlaceHolderID="mainCPHolder" runat="server">
    <input type="hidden" id="pageNumRefField" runat="server" name="pageNumRefField" value="0" />


    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { setInterval(function () { cache_clear() }, 120000); });
        function cache_clear() { window.location.reload(true); }
    </script>
    <script language="javascript" type="text/javascript">

        function editCommisionSettingButtonClicked(instutionId) {
            var retVal = showCommisionSetting(instutionId);
        }


        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function deleteButtonClicked(storedProcedure, itemId) {
            var retVal = showDeleteWindowKiosk(storedProcedure, itemId);
        }

        function searchButtonClicked(sender) {
            document.getElementById('pageNumRefField').value = "0";
            return true;
        }

        function editButtonClicked(itemID) {
            var retVal = showKioskWindow(itemID);
        }

        function postForPaging(selectedPageNum) {
            document.getElementById('pageNumRefField').value = selectedPageNum;
            document.getElementById('navigateButton').click();
        }

        function validateNo(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

    </script>
    <script type="text/javascript" language="javascript" src="../js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery.tablednd_0_5.js"></script>

    <asp:HiddenField ID="idRefField" runat="server" Value="0" />
    <asp:HiddenField ID="oldOrderNumberField" runat="server" Value="0" />
    <asp:HiddenField ID="newOrderNumberField" runat="server" Value="0" />


    <div align="center">

        <asp:Panel ID="panel1" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto" Height="603px">
            <table>

                <tr>
                    <div align="left" class="windowTitle_container_autox30" style="padding-bottom: 55px;">
                        Kiosk Ayarlar
                        <hr style="border-bottom: 1px solid #b2b2b4;" />
                    </div>
                </tr>

                <tr>

                    <td>
                        <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp;</div>




                        <table border="0" cellpadding="2" cellspacing="0" id="Table1" runat="server">

                            <%--                            <tr>
                                <td align="center" colspan="2" class="staticTextLine_200x20" height="30" style="background: #dcdcdc !important; border-radius: 15px;">AYARLAR</td>
                            </tr>--%>

                            <tr>
                                <td align="center" colspan="2" style="padding-top: 50px;">

                                    <asp:Table ID="itemsTable" runat="server" CellPadding="0" CellSpacing="0" BorderWidth="0px"
                                        BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="95%">


                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" Style="height: 30px;">
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty" Text="Makbuz Limiti" Style="color: black; text-align: left;"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty" Style="width: 10px;"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell>
                                                <asp:TextBox TextMode="Number" ID="billFieldNotificationLimit" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9" min="1" max="1000" step="1" value="0" />
                                            </asp:TableHeaderCell>
                                        </asp:TableRow>
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" Style="height: 30px;">
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty" Text="Makbuz Uyarı Aktif:" Style="color: black; text-align: left;"></asp:TableHeaderCell><asp:TableHeaderCell CssClass="inputTitleCellEmpty" Style="width: 10px;"></asp:TableHeaderCell><asp:TableHeaderCell>
                                                <asp:ListBox ID="isNotificationLimitReceiptActive" runat="server" Height="21" CssClass="inputTitleCell9" Rows="1" SelectionMode="Single" Visible="true"></asp:ListBox>
                                            </asp:TableHeaderCell>
                                        </asp:TableRow>
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" Style="height: 30px;">
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty" Text="Para Kutusu Uyarı Limit Yüzdesi:" Style="color: black; text-align: left;"></asp:TableHeaderCell><asp:TableHeaderCell CssClass="inputTitleCellEmpty" Style="width: 10px;"></asp:TableHeaderCell><asp:TableHeaderCell>
                                                <asp:TextBox TextMode="Number" ID="cashBoxNotificationLimit" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9" min="1" max="1000" step="1" value="0" />
                                            </asp:TableHeaderCell>
                                        </asp:TableRow>
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" Style="height: 30px;">
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty" Text="Para Kutusu Uyarı Aktif:" Style="color: black; text-align: left;"></asp:TableHeaderCell><asp:TableHeaderCell CssClass="inputTitleCellEmpty" Style="width: 10px;"></asp:TableHeaderCell><asp:TableHeaderCell>
                                                <asp:ListBox ID="isNotificationCashBoxLimitActive" runat="server" Height="21" CssClass="inputTitleCell9" Rows="1" SelectionMode="Single" Visible="true"></asp:ListBox>
                                            </asp:TableHeaderCell>
                                        </asp:TableRow>
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" Style="height: 30px;">
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty" Text="Hesap Sarı Alarm Aktif:" Style="color: black; text-align: left; width: 250px"></asp:TableHeaderCell><asp:TableHeaderCell CssClass="inputTitleCellEmpty" Style="width: 10px;"></asp:TableHeaderCell><asp:TableHeaderCell>
                                                <asp:ListBox ID="isYellowAlertActive" runat="server" Height="21" CssClass="inputTitleCell9" Rows="1" SelectionMode="Single" Visible="true"></asp:ListBox>
                                            </asp:TableHeaderCell>
                                        </asp:TableRow>
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" Style="height: 30px;">
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty" Text="Hesap Kırmızı Alarm Aktif:" Style="color: black; text-align: left; width: 250px"></asp:TableHeaderCell><asp:TableHeaderCell CssClass="inputTitleCellEmpty" Style="width: 10px;"></asp:TableHeaderCell><asp:TableHeaderCell>
                                                <asp:ListBox ID="isRedAlertActive" runat="server" Height="21" CssClass="inputTitleCell9" Rows="1" SelectionMode="Single" Visible="true"></asp:ListBox>
                                            </asp:TableHeaderCell>
                                        </asp:TableRow>
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" Style="height: 30px;">
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty" Text="Para Kod Üretme Limiti:" Style="color: black; text-align: left; width: 250px"></asp:TableHeaderCell><asp:TableHeaderCell CssClass="inputTitleCellEmpty" Style="width: 10px;"></asp:TableHeaderCell><asp:TableHeaderCell>
                                                <asp:TextBox TextMode="Number" ID="ProduceMoneyCode" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9" min="1" max="1000" step="1" value="0" />
                                            </asp:TableHeaderCell>
                                        </asp:TableRow>
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" Style="height: 30px;">
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty" Text="Para Sayı Limiti:" Style="color: black; text-align: left; width: 250px"></asp:TableHeaderCell><asp:TableHeaderCell CssClass="inputTitleCellEmpty" Style="width: 10px;"></asp:TableHeaderCell><asp:TableHeaderCell>
                                                <asp:TextBox TextMode="Number" ID="LimitofMoneyCount" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9" min="1" max="1000" step="1" value="0" />
                                            </asp:TableHeaderCell>
                                        </asp:TableRow>
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" Style="height: 30px;">
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty" Text="Para Kod Periyodik Limiti:" Style="color: black; text-align: left; width: 250px"></asp:TableHeaderCell><asp:TableHeaderCell CssClass="inputTitleCellEmpty" Style="width: 10px;"></asp:TableHeaderCell><asp:TableHeaderCell>
                                                <asp:TextBox TextMode="Number" ID="CashCodeWarning" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9" min="1" max="1000" step="1" value="0" />
                                            </asp:TableHeaderCell>
                                        </asp:TableRow>
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" Style="height: 30px;">
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty" Text="BaşkentGaz Maximum Uyarı Limit:" Style="color: black; text-align: left; width: 250px"></asp:TableHeaderCell><asp:TableHeaderCell CssClass="inputTitleCellEmpty" Style="width: 10px;"></asp:TableHeaderCell><asp:TableHeaderCell>
                                                <asp:TextBox TextMode="Number" ID="LimitofBaşkentGazMaximumWarning" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9" min="1" max="1000" step="1" value="0" />
                                            </asp:TableHeaderCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                                    OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Kaydet"></asp:Button>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>


                    </td>

                    <td style="padding-bottom: 153px;">

                        <table border="0" cellpadding="2" cellspacing="10" id="Table2" runat="server">

                            <%--                            <tr>
                                <td align="center" colspan="2" class="staticTextLine_200x20" height="30" style="background: #dcdcdc !important; border-radius: 15px;">KOMİSYONLAR</td>
                            </tr>--%>

                            <tr>
                                <td align="center" colspan="2"  >

                                    <asp:Table ID="itemsTable2" runat="server" CellPadding="0" CellSpacing="0" BorderWidth="0px"
                                        BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="95%">


                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty"  Text="" Style="color: black; text-align: left;"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty"  BorderStyle="Ridge" Style="color: black;  " Text="Kredi Kartı Komisyon    </a>" />
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty"  BorderStyle="Ridge" Style="color: black;  " Text="Mobil Ödeme Komisyon    </a>" />
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty"  BorderStyle="Ridge" Style="color: black;  " Text="Nakit Yükleme Komisyon           </a>" />
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty"  BorderStyle="Ridge" Style="color: black;  " Text="Sarı Alarm                     </a>" />
                                            <asp:TableHeaderCell CssClass="inputTitleCellEmpty"  BorderStyle="Ridge" Style="color: black;  " Text="Kırmızı Alarm                  </a>" />

                                        </asp:TableRow>


                                     <%--   <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" Style="height: 30px;">
                                            <asp:TableHeaderCell Style="color: black; text-align: left;" >
                                                <asp:Label ID="askiTextField" Text="Aski:" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v2" />
                                            </asp:TableHeaderCell>
                                            <asp:TableHeaderCell>
                                                <asp:Label   ID="creditAski" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v2"  />
                                            </asp:TableHeaderCell>
                                            <asp:TableHeaderCell>
                                                <asp:Label  ID="mobileAski" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v2"  />
                                            </asp:TableHeaderCell>

                                            <asp:TableHeaderCell>
                                                <asp:Label   ID="cashAski" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v2"  />
                                            </asp:TableHeaderCell>

                                            <asp:TableHeaderCell>
                                                <asp:Label  ID="yellowAski" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v2"  />
                                            </asp:TableHeaderCell>

                                            <asp:TableHeaderCell>
                                                <asp:Label  ID="redAski" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v2"  />
                                            </asp:TableHeaderCell>

                                        </asp:TableRow>


                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" Style="height: 30px;">
                                            <asp:TableHeaderCell Style="color: black; text-align: left;">
                                                <asp:Label ID="baskentGazTextField" Text="BaşkentGaz:" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v2" />
                                            </asp:TableHeaderCell>
                                            <asp:TableHeaderCell>
                                                <asp:Label   ID="creditBaskent" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v2"   />
                                            </asp:TableHeaderCell>
                                            <asp:TableHeaderCell>
                                                <asp:Label   ID="mobileBaskent" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v2"  />
                                            </asp:TableHeaderCell>

                                            <asp:TableHeaderCell>
                                                <asp:Label   ID="cashBaskent" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v2"   />
                                            </asp:TableHeaderCell>

                                            <asp:TableHeaderCell>
                                                <asp:Label   ID="yellowBaskent" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v2"   />
                                            </asp:TableHeaderCell>

                                            <asp:TableHeaderCell>
                                                <asp:Label   ID="redBaskent" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v2"  />
                                            </asp:TableHeaderCell>

                                        </asp:TableRow>


                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" Style="height: 30px;">
                                            <asp:TableHeaderCell Style="color: black; text-align: left;">
                                                <asp:Label  ID="aksarayTextField" Text="Aksaray:" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v2" />
                                            </asp:TableHeaderCell>
                                            <asp:TableHeaderCell>
                                                <asp:Label   ID="creditAksaray" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v2"  />
                                            </asp:TableHeaderCell>
                                            <asp:TableHeaderCell>
                                                <asp:Label   ID="mobileAksaray" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v2"  />
                                            </asp:TableHeaderCell>

                                            <asp:TableHeaderCell>
                                                <asp:Label   ID="cashAksaray" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v2"   />
                                            </asp:TableHeaderCell>

                                            <asp:TableHeaderCell>
                                                <asp:Label   ID="yellowAksaray" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v2"   />
                                            </asp:TableHeaderCell>

                                            <asp:TableHeaderCell>
                                                <asp:Label  ID="redAksaray" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v2"   />
                                            </asp:TableHeaderCell>

                                        </asp:TableRow>--%>


                                    </asp:Table>
                                </td>
                            </tr>
 

                        </table>

                    </td>

                </tr>
            </table>
        </asp:Panel>
    </div>

    <div>&nbsp;</div>
    <br />
    <table width="95%">
        <tr align="right">
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="width: 80px">
                <asp:Label ID="Label2" runat="server" Text="powered by" CssClass="poweredbyTitle"></asp:Label></td>
            <td style="width: 100px">
                <asp:LinkButton ID="LinkButton1" href="http://www.birlesikodeme.com/" runat="server" Text="BİRLEŞİK ÖDEME" CssClass="procenneTitle"></asp:LinkButton></td>
        </tr>
    </table>
</asp:Content>
