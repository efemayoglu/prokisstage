﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowCutOfMonitoring.aspx.cs" Inherits="Settings_ShowCutOfMonitoring" %>

<%@ Register Src="../MS_Control/MultipleSelection.ascx" TagName="MultipleSelection" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />

    <script language="javascript" type="text/javascript">

        function editEnterKioskEmptyCashButtonClicked() {
            var retVal = showEnterKioskEmptyCash();
        }

        function strtDateChngd() {
            if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                document.getElementById('startDateField').value = document.getElementById('endDateField').value;
            }

            document.getElementById('navigateButton').click();

        }

        function endDateChngd() {
            if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                document.getElementById('endDateField').value = document.getElementById('startDateField').value;
            }

            //document.getElementById('instutionBox').value == "0";
            //document.getElementById('kioskBox').value == "0";

            document.getElementById('navigateButton').click();
        }

        function insertDateChngd() {
            if (document.getElementById('insertDateField').value < document.getElementById('startDateField').value) {
                document.getElementById('insertDateField').value = document.getElementById('startDateField').value;
            }

            document.getElementById('navigateButton').click();
        }


        function checkForm() {
            var status = true;
            var messageText = "";

            if (document.getElementById('instutionBox').value == "0" && document.getElementById('kioskBox').value == "0") {
                status = false;
                messageText = "Kurum  veya Kiosk Seçiniz !";
            }
            
            else {
                messageText = "";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }

         
        function checkKioskBox() {
          
           
            if (document.getElementById('kioskBox').value == 0) {

                document.getElementById('instutionBox').disabled = false;
            }
            else
            {
                document.getElementById('instutionBox').disabled = true;
            }
                
           // document.getElementById('instutionBox').value = "0";
            
          
        }

        
        function checkInstutionBox() {
           

            if (document.getElementById('instutionBox').value == 0) {

                document.getElementById('kioskBox').disabled = false;
            }
            else {
                document.getElementById('kioskBox').disabled = true;
            }

          
        }

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        //function getDailyAccountActionsClicked(itemId) {
        //    var url = "../Account/KiosCashAcceptor.aspx?itemId=" + itemId;
        //    var retValue = openDialoagWindow(url, 1000, 600, "", "Kiosk Para Alımı");
        //}

        //function getDailyCustomerAccountsClicked(itemId) {
        //    var url = "../Account/KiosCashAcceptor.aspx?itemId=" + itemId + "&isActive=1";
        //    var retValue = openDialoagWindow(url, 700, 600, "", "Kiosk Para Alımı");
        //}

        function searchButtonClicked(sender) {
            document.getElementById('pageNumRefField').value = "0";
            return true;
        }

        function showTransactionDetailClicked(transactionID) {
            var retVal = showTransactionDetailWindow(transactionID);
        }

        function postForPaging(selectedPageNum) {
            document.getElementById('pageNumRefField').value = selectedPageNum;
            document.getElementById('navigateButton').click();
        }



        function validateNo(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

    </script>

</head>
<body>

    <form id="form1" runat="server">

        <asp:Button ID="navigateButton" runat="server" OnClick="navigateButton_Click" CssClass="dummy" />
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>

        <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel7" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <div align="left" id="articleDetailsTab" class="windowTitle_container_autox30">
                    Kiosk Kesinti Ekranı Kontrolleri<hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>
                <table border="0" cellpadding="2" cellspacing="0" id="Table1" runat="server">


                    <tr valign="middle" >
                        <td align="left" class="staticTextLine_200x20">Başlangıç Tarihi :
                        </td>

                        <td>
                            <asp:TextBox ID="startDateField" runat="server" CssClass="inputLine_90x20" AutoPostBack="true" OnTextChanged="navigateButton_Click"></asp:TextBox>

                            <ajaxControlToolkit:CalendarExtender ID="startDateCalendar" runat="server" TargetControlID="startDateField"
                                Format="yyyy-MM-dd HH:mm:ss" DaysModeTitleFormat="yyyy-MM-dd HH:mm:ss" Enabled="True" OnClientDateSelectionChanged="strtDateChngd">
                            </ajaxControlToolkit:CalendarExtender>
                        </td>

                    </tr>

                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Bitiş Tarihi :
                        </td>

                        <td>
                            <asp:TextBox ID="endDateField" runat="server" CssClass="inputLine_90x20" AutoPostBack="true" OnTextChanged="navigateButton_Click"></asp:TextBox>

                            <ajaxControlToolkit:CalendarExtender ID="endDateCalendar" runat="server" TargetControlID="endDateField"
                                Format="yyyy-MM-dd HH:mm:ss" DaysModeTitleFormat="yyyy-MM-dd HH:mm:ss" Enabled="True" OnClientDateSelectionChanged="endDateChngd">
                            </ajaxControlToolkit:CalendarExtender>
                        </td>

                    </tr>

                    <tr>
                        <td align="left" class="staticTextLine_200x20" style="padding:5px;">Kesinti Süresi:                                             
                        </td>

                        <td>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="cutDurationField" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" onkeypress='validateNo(event)' Enabled="false"></asp:TextBox>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="startDateField" EventName="TextChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="endDateField"   EventName="TextChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" class="staticTextLine_200x20" >Kiosk Adı :                                             
                        </td>
                        <td>
                             <asp:ListBox ID="kioskBox" CssClass="inputLine_90x20" runat="server" SelectionMode="Single"
                                DataTextField="Name" DataValueField="Id" Rows="1" Visible="true" onchange ="checkKioskBox();"></asp:ListBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" class="staticTextLine_200x20" >Kurum :                                             
                        </td>
                        <td>
                            <asp:ListBox ID="instutionBox" CssClass="inputLine_90x20" runat="server" SelectionMode="Single"
                                DataTextField="Name" DataValueField="Id" Rows="1" Visible="true" onchange ="checkInstutionBox();"></asp:ListBox>
                        </td>
                    </tr>

                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Kesinti Nedeni:                                             
                        </td>
                        <td >
                            <asp:TextBox TextMode="MultiLine"   style="resize:none; text-align:left;" Height="85"  ID="cutReasonField" CssClass="inputLine_90x20" runat="server"  ></asp:TextBox>
                        </td>
                    </tr>

  

                </table>
                <br />
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Kaydet"></asp:Button>

                                <asp:Button ID="cancelButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClientClick="parent.hideModalPopup2_Exit();" Text="Vazgeç"></asp:Button>

                            </td>
                        </tr>
                    </table>
                </td>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
