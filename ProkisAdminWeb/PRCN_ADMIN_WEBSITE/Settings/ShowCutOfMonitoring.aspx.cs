﻿using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Account;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Settings_ShowCutOfMonitoring : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private int ownSubMenuIndex = -1;
    private ParserListKioskName kiosks;
    public int cutId;
    private GetKioskCutofMonitoring cutKiosk;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Settings/CutOfMonitoring.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }

        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }

        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }



        this.cutId = Convert.ToInt32(Request.QueryString["cutID"]);


        if (this.cutId == 0 && ViewState["cutID"] != null)
        {
            this.cutId = Convert.ToInt32(ViewState["cutID"].ToString());
        }


        if (!Page.IsPostBack)
        {
            this.BindCtrls();
        }

    }

    private void BindCtrls()
    {
        CallWebServices callWebServ = new CallWebServices();
        if (this.cutId != 0)
        {
            this.cutKiosk = callWebServ.CallGetCutOffMonitoringService(this.cutId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        }

        this.SetControls();
    }

    private void SetControls()
    {
        try
        {

            if (this.cutKiosk != null)
            {
                this.startDateField.Text = this.cutKiosk.Cuts.startDate;
                this.endDateField.Text = this.cutKiosk.Cuts.endDate;
                this.cutReasonField.Text = this.cutKiosk.Cuts.cutReason;

                //this.userRoleBox.SelectedValue = this.user.User[0].RoleId.ToString();
                //this.userRoleBox.DataBind();
                //this.userStatusBox.SelectedValue = this.user.User[0].UserStatusId.ToString();
                //this.userStatusBox.DataBind();
                
                //this.kioskBox.SelectedValue = this.cutKiosk.Cuts.kioskName;
                //this.kioskBox.DataBind();

                TimeSpan diffrence = Convert.ToDateTime(endDateField.Text) - Convert.ToDateTime(startDateField.Text);
                cutDurationField.Text = diffrence.ToString();



                CallWebServices callWebServ = new CallWebServices();
                ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                if (kiosks != null)
                {

                    this.kioskBox.DataSource = kiosks.KioskNames;
                    this.kioskBox.DataBind();
                }


                ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                if (instutions != null)
                {
                    this.instutionBox.DataSource = instutions.InstutionNames;
                    this.instutionBox.DataBind();
                }

                ListItem item = new ListItem();
                item.Text = "Kurum Seçiniz";
                item.Value = "0";
                this.instutionBox.Items.Add(item);
                this.instutionBox.SelectedValue = "0";


                ListItem item2 = new ListItem();
                item2.Text = "Kiosk Seçiniz";
                item2.Value = "0";
                this.kioskBox.Items.Add(item2);
                this.kioskBox.SelectedValue = "0";

                if (string.IsNullOrEmpty(this.cutKiosk.Cuts.kioskId))
                {
                    this.kioskBox.SelectedValue = "0";
                }
                else
                {
                    this.kioskBox.SelectedValue = this.cutKiosk.Cuts.kioskId.ToString();
                }


                if (string.IsNullOrEmpty(this.cutKiosk.Cuts.Inst))
                {
                    this.instutionBox.SelectedValue = "0";
                }
                else
                {
                    this.instutionBox.SelectedValue = this.cutKiosk.Cuts.Inst.ToString();
                }

                
                //this.instutionBox.SelectedValue = this.cutKiosk.Cuts.Inst.ToString();

            }

            else
            {
                CallWebServices callWebServ = new CallWebServices();

                ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                if (kiosks != null)
                {

                    this.kioskBox.DataSource = kiosks.KioskNames;
                    this.kioskBox.DataBind();
                }


                ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                if (instutions != null)
                {
                    this.instutionBox.DataSource = instutions.InstutionNames;
                    this.instutionBox.DataBind();
                }


                ListItem item = new ListItem();
                item.Text = "Kurum Seçiniz";
                item.Value = "0";
                this.instutionBox.Items.Add(item);
                this.instutionBox.SelectedValue = "0";


                ListItem item2 = new ListItem();
                item2.Text = "Kiosk Seçiniz";
                item2.Value = "0";
                this.kioskBox.Items.Add(item2);
                this.kioskBox.SelectedValue = "0";

                //this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

                this.startDateField.Text = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";
                this.endDateField.Text = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";


                TimeSpan diffrence = Convert.ToDateTime(endDateField.Text) - Convert.ToDateTime(startDateField.Text);


                cutDurationField.Text = diffrence.ToString();

                //int toplamsaat = Convert.ToInt32(diffrence.ToString());
                //insertorField.Text = this.LoginDatas.User.Username;
                //insertDateField.Text = DateTime.Now.ToString();


                //this.cutReasonField.Height = 100;
                this.cutReasonField.Width = 300;

                ViewState["OrderColumn"] = 2;
                ViewState["OrderDesc"] = 1;

            }

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowKioskSC");
        }

    }

    // OnTextChanged: startDateField, endDateField
    protected void navigateButton_Click(object sender, EventArgs e)
    {
        TimeSpan diffrence = Convert.ToDateTime(endDateField.Text) - Convert.ToDateTime(startDateField.Text);


        if (diffrence.TotalDays > 0)
        {

            double daytohours = diffrence.Days * 24;

            string hours;
            string minutes;
            string seconds;

            hours = (diffrence.Hours + daytohours).ToString();

            if (diffrence.Minutes >= 0 && diffrence.Minutes <= 9)
            {
                minutes = "0" + diffrence.Minutes.ToString();
            }
            else
            {
                minutes = diffrence.Minutes.ToString();
            }


            if (diffrence.Seconds >= 0 && diffrence.Seconds <= 9)
            {
                seconds = "0" + diffrence.Seconds.ToString();
            }
            else
            {
                seconds = diffrence.Seconds.ToString();
            }



            cutDurationField.Text = hours + ":" + minutes + ":" + seconds;
        }
        else
        {
            cutDurationField.Text = diffrence.ToString();
        }


        this.instutionBox.SelectedValue = "0";
        this.kioskBox.SelectedValue = "0";

    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        SaveCashNotification();
    }

    private void SaveCashNotification()
    {
        try
        {
            //save 
            if (cutId == 0)
            {

                if (this.instutionBox.SelectedValue == "")
                {
                    this.instutionBox.SelectedValue = "0";
                }

                if (this.kioskBox.SelectedValue == "")
                {
                    this.kioskBox.SelectedValue = "0";
                }


                CallWebServices callWebServ = new CallWebServices();

                ParserOperation parserSaveCutOfMonitoring = callWebServ.CallSaveCutOffMonitoringService(Convert.ToDateTime(this.startDateField.Text),
                                                                                                       Convert.ToDateTime(this.endDateField.Text),
                                                                                                       this.cutDurationField.Text,
                                                                                                       (this.kioskBox.SelectedValue),
                                                                                                       Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                                                       this.cutReasonField.Text,
                                                                                                       DateTime.Now,
                                                                                                       this.LoginDatas.User.Id,
                                                                                                       this.LoginDatas.AccessToken);


                if (parserSaveCutOfMonitoring != null)
                {
                    string scriptText = "";

                    //this.messageArea.InnerHtml = parserSaveControlCashNotification.errorDescription;

                    scriptText = "parent.showMessageWindowCutOffMonitoring('İşlem Başarılı.');";
                    ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);

                }
                else if (parserSaveCutOfMonitoring.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('Sisteme Erişilemiyor !'); ", true);
                }

            }

            //update
            else
            {
                if (this.instutionBox.SelectedValue == "")
                {
                    this.instutionBox.SelectedValue = "0";
                }

                if (this.kioskBox.SelectedValue == "")
                {
                    this.kioskBox.SelectedValue = "0";
                }


                CallWebServices callWebServ = new CallWebServices();

                ParserOperation parserUpdateCutOfMonitoring = callWebServ.CallUpdateCutOffMonitoringService(this.cutId,
                                                                                                        Convert.ToDateTime(this.startDateField.Text),
                                                                                                       Convert.ToDateTime(this.endDateField.Text),
                                                                                                       this.cutDurationField.Text,
                                                                                                       (this.kioskBox.SelectedValue),
                                                                                                       Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                                                       this.cutReasonField.Text,
                                                                                                       DateTime.Now,
                                                                                                       this.LoginDatas.User.Id,
                                                                                                       this.LoginDatas.AccessToken);


                if (parserUpdateCutOfMonitoring != null)
                {
                    string scriptText = "";

                    //this.messageArea.InnerHtml = parserSaveControlCashNotification.errorDescription;

                    scriptText = "parent.showMessageWindowCutOffMonitoring('İşlem Başarılı.');";
                    ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);

                }
                else if (parserUpdateCutOfMonitoring.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('Sisteme Erişilemiyor !'); ", true);
                }

            }

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "Settings");
        }
    }

}