﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE;
using PRCNCORE.Constants;
using PRCNCORE.Input;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Account;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Settings_ListSetting : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;
     
    private int ownSubMenuIndex;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Settings/ListSetting.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }
 


        if (!Page.IsPostBack)
        {


            //this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            //ViewState["OrderColumn"] = 4;
            //ViewState["OrderDesc"] = 1;
            /////////////////////////////////////////////////////////


            //ListItem item = new ListItem();
            //item.Text = "Durum Tipini Seçiniz";
            //item.Value = "2";


            ListItem pasif = new ListItem();
            pasif.Text = "Pasif";
            pasif.Value = "0";


            ListItem aktif = new ListItem();
            aktif.Text = "Aktif";
            aktif.Value = "1";

            //Makbuz uyarı aktif
            
            this.isNotificationLimitReceiptActive.Items.Add(pasif);
            this.isNotificationLimitReceiptActive.Items.Add(aktif);
            //this.isNotificationLimitReceiptActive.Items.Add(item);
            this.isNotificationLimitReceiptActive.SelectedValue = "0";

            ////////////////////////////////////////////

            //ListItem item2 = new ListItem();
            //item2.Text = "Durum Tipini Seçiniz";
            //item2.Value = "2";



            ListItem pasif2 = new ListItem();
            pasif2.Text = "Pasif";
            pasif2.Value = "0";


            ListItem aktif2 = new ListItem();
            aktif2.Text = "Aktif";
            aktif2.Value = "1";


            //Para Kutusu Uyarı Aktif:	
            
            this.isNotificationCashBoxLimitActive.Items.Add(pasif2);
            this.isNotificationCashBoxLimitActive.Items.Add(aktif2);
            //this.isNotificationCashBoxLimitActive.Items.Add(item2);
            this.isNotificationCashBoxLimitActive.SelectedValue = "0";


            ////////////////////////////////////////////

            //ListItem itemYellow = new ListItem();
            //itemYellow.Text = "Durum Tipini Seçiniz";
            //itemYellow.Value = "2";



            ListItem pasifYellow = new ListItem();
            pasifYellow.Text = "Pasif";
            pasifYellow.Value = "0";


            ListItem aktifYellow = new ListItem();
            aktifYellow.Text = "Aktif";
            aktifYellow.Value = "1";

            // Hesap Sarı Alarm Aktif:	

            this.isYellowAlertActive.Items.Add(pasifYellow);
            this.isYellowAlertActive.Items.Add(aktifYellow);
            //this.isYellowAlertActive.Items.Add(itemYellow);
            this.isYellowAlertActive.SelectedValue = "0";

            ////////////////////////////////////////////


            //ListItem itemRed = new ListItem();
            //itemRed.Text = "Durum Tipini Seçiniz";
            //itemRed.Value = "2";
            
            ListItem pasifRed = new ListItem();
            pasifRed.Text = "Pasif";
            pasifRed.Value = "0";


            ListItem aktifRed = new ListItem();
            aktifRed.Text = "Aktif";
            aktifRed.Value = "1";


            // Hesap Sarı Alarm Aktif:	

            this.isRedAlertActive.Items.Add(pasifRed);
            this.isRedAlertActive.Items.Add(aktifRed);
            //this.isRedAlertActive.Items.Add(item4);
            this.isRedAlertActive.SelectedValue = "0";

            ////////////////////////////////////////////


        
            this.GetSetting();
        }
    }


    protected void saveButton_Click(object sender, EventArgs e)
    {
        SaveSetting();
    }


    private void GetSetting()
    {
        try
        {
            CallWebServices callWebServ = new CallWebServices();

            
            ParserListSetting list = callWebServ.CallGetSettingService(this.billFieldNotificationLimit.Text,
                                                 Convert.ToInt32(this.isNotificationLimitReceiptActive.SelectedValue),
                                                 (this.cashBoxNotificationLimit.Text),
                                                 Convert.ToInt32(this.isNotificationCashBoxLimitActive.SelectedValue),
                                                 Convert.ToInt32(this.isYellowAlertActive.SelectedValue),
                                                 Convert.ToInt32(this.isRedAlertActive.SelectedValue),
                                                 this.ProduceMoneyCode.Text,
                                                 this.LimitofMoneyCount.Text,
                                                 this.CashCodeWarning.Text,
                                                 this.LimitofBaşkentGazMaximumWarning.Text,
                                                 this.LoginDatas.User.Id,
                                                 this.LoginDatas.AccessToken);

            ParserGetCommisionSetting list2 = callWebServ.CallCommisionSettingService(this.LoginDatas.User.Id,
                                                 this.LoginDatas.AccessToken);

            BindListTable(list.billFieldNotificationLimit,
                list.isNotificationLimitReceiptActive,
                list.cashBoxNotificationLimit,
                list.isNotificationCashBoxLimitActive,
                list.isYellowAlertActive,
                list.isRedAlertActive,
                list.moneyCodeGenerationLimit,
                list.moneyCodeGenerationCountLimit,
                list.oneyCodeGenerationCountLimitPeriod,
                list.bGazUpperLoadingLimit,
                list2);
        }
        catch (Exception exp)
        {

            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveSettings");
        }

    }

    //protected void searchButton_Click(object sender, System.EventArgs e)
    //{
    //    //this.pageNum = 0;
    //   // this.SearchOrder(4, 1);
    //}

    private void BindListTable(string billFieldNotificationLimitNew,
                                        int isNotificationLimitReceiptActiveNew
                                       , string cashBoxNotificationLimitNew,
                                        int isNotificationCashBoxLimitActiveNew,
                                        int isYellowAlertActiveNew, 
                                        int isRedAlertActiveNew,
                                        string ProduceMoneyCodeNew,
                                        string LimitofMoneyCountNew,
                                        string CashCodeWarningNew,
                                        string LimitofBaşkentGazMaximumWarningNew,
                                        ParserGetCommisionSetting commision)


    {
        try
        {
            billFieldNotificationLimit.Text = billFieldNotificationLimitNew;
            cashBoxNotificationLimit.Text = cashBoxNotificationLimitNew;
            ProduceMoneyCode.Text = ProduceMoneyCodeNew;
            LimitofMoneyCount.Text = LimitofMoneyCountNew;
            CashCodeWarning.Text = CashCodeWarningNew;
            LimitofBaşkentGazMaximumWarning.Text = LimitofBaşkentGazMaximumWarningNew;

            this.isNotificationLimitReceiptActive.SelectedIndex = isNotificationLimitReceiptActiveNew;
            this.isNotificationCashBoxLimitActive.SelectedIndex  = isNotificationCashBoxLimitActiveNew;
            this.isYellowAlertActive.SelectedIndex  = isYellowAlertActiveNew;
            this.isRedAlertActive.SelectedIndex = isRedAlertActiveNew;



            // other setting of commission
            TableRow[] Row = new TableRow[commision.CommisionSetting.Count];

            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                //Row[i].Attributes.Add("kioskId", kiosks.Kiosks[i].KioskId.ToString());

                TableCell indexCell = new TableCell();
                TableCell creditCommissionCell = new TableCell();
                TableCell mobileCommissionCell = new TableCell();
                TableCell cashCommissionCell = new TableCell();
                TableCell yellowAlarmCell = new TableCell();
                TableCell redAlarmCell = new TableCell();
                TableCell insNameCell = new TableCell();

                indexCell.CssClass = "inputTitleCell9_v2";
                creditCommissionCell.CssClass = "inputTitleCell9_v2";
                mobileCommissionCell.CssClass = "inputTitleCell9_v2";
                cashCommissionCell.CssClass = "inputTitleCell9_v2";
                redAlarmCell.CssClass = "inputTitleCell9_v2";
                insNameCell.CssClass = "inputTitleCell9_v3";
                yellowAlarmCell.CssClass = "inputTitleCell9_v2";

                //insNameCell.BorderStyle = Ridge;

               // indexCell.Text = (startIndex + i).ToString();


                //if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1")
                //{
                //    nameCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editButtonClicked(" + kiosks.Kiosks[i].KioskId + ");\" class=\"anylink\">" + kiosks.Kiosks[i].KioskName.ToString() + "</a>";
                //}
                //else
                //{
                //    nameCell.Text = kiosks.Kiosks[i].KioskName.ToString();
                //}

                creditCommissionCell.Text = commision.CommisionSetting[i].CreditCommission;
                mobileCommissionCell.Text = commision.CommisionSetting[i].MobileCommission;
                cashCommissionCell.Text = commision.CommisionSetting[i].CashCommission;
                yellowAlarmCell.Text = commision.CommisionSetting[i].YellowAlarm;
                redAlarmCell.Text = commision.CommisionSetting[i].RedAlarm;
             
                insNameCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editCommisionSettingButtonClicked(" + commision.CommisionSetting[i].InstutionId + ");\" class=\"anylink\">" + commision.CommisionSetting[i].InsName + "</a>";
 
                
                Row[i].Cells.AddRange(new TableCell[]{
						insNameCell,
						creditCommissionCell,
                        mobileCommissionCell,
                        cashCommissionCell,
                        yellowAlarmCell,
                        redAlarmCell 
                                                                       
					});


                //if (i % 2 == 0)
                //    Row[i].CssClass = "listrow";
                //else
                //    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable2.Rows.AddRange(Row);


            // other setting of commission
            //this.creditAski.Text = creditAski.ToString();
            //this.mobileAski.Text = mobileAski.ToString();
            //this.cashAski.Text = cashAski.ToString();
            //this.yellowAski.Text = yellowAski.ToString();
            //this.redAski.Text = redAski.ToString();
            //
            //this.creditBaskent.Text = creditBaskent.ToString();
            //this.mobileBaskent.Text = mobileBaskent.ToString();
            //this.cashBaskent.Text = cashBaskent.ToString();
            //this.yellowBaskent.Text = yellowBaskent.ToString();
            //this.redBaskent.Text = redBaskent.ToString();
            //
            //this.creditAksaray.Text = creditAksaray.ToString();
            //this.mobileAksaray.Text = mobileAksaray.ToString();
            //this.cashAksaray.Text = cashAksaray.ToString();
            //this.yellowAksaray.Text = yellowAksaray.ToString();
            //this.redAksaray.Text = redAksaray.ToString();

            //this.askiTextField.Text = "<a href=\"javascript:void(0);\" onclick=\"editCommisionSettingButtonClicked('1');\" class=\"anylink\">" + "Aski" + "</a>";
            //this.baskentGazTextField.Text = "<a href=\"javascript:void(0);\" onclick=\"editCommisionSettingButtonClicked('2');\" class=\"anylink\">" + "BaşkentGaz" + "</a>";
            //this.aksarayTextField.Text = "<a href=\"javascript:void(0);\" onclick=\"editCommisionSettingButtonClicked('4');\" class=\"anylink\">" + "Aksaray" + "</a>";
             
                   
 
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskBDT");
        }
    }



    private void SaveSetting()
    {
        try
        {

            CallWebServices callWebServ = new CallWebServices();

             

            ParserOperation parserSaveKiosSettings = callWebServ.CallSaveSettingService(this.billFieldNotificationLimit.Text,
                                                                                            Convert.ToInt32(this.isNotificationLimitReceiptActive.SelectedValue),
                                                                                            (this.cashBoxNotificationLimit.Text),
                                                                                            Convert.ToInt32(this.isNotificationCashBoxLimitActive.SelectedValue),
                                                                                            Convert.ToInt32(this.isYellowAlertActive.SelectedValue),
                                                                                            Convert.ToInt32(this.isRedAlertActive.Text),
                                                                                            (this.ProduceMoneyCode.Text),
                                                                                            (this.LimitofMoneyCount.Text),
                                                                                            (this.CashCodeWarning.Text),
                                                                                            (this.LimitofBaşkentGazMaximumWarning.Text), 
                                                                                            this.LoginDatas.AccessToken, 
                                                                                            this.LoginDatas.User.Id);


 
            if (parserSaveKiosSettings != null)
            {
                
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('" + parserSaveKiosSettings.errorDescription + "'); ", true);
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:callOwn(); ", true);
 
                //Response.Redirect("../Account/EmptyKioskMutabakat.aspx");
            }
            else if (parserSaveKiosSettings.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
            {
                Session.Abandon();
                Session.RemoveAll();
                Response.Redirect("../root/Login.aspx", true);
            }
            else
            {
                this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "Settings");
        }
    }

    /*
    protected void saveButton2_Click(object sender, EventArgs e)
    {
        SaveCommisionSetting();
    }

    private void SaveCommisionSetting()
    {
        try
        {

            CallWebServices callWebServ = new CallWebServices();



            ParserOperation parserSaveCommisionSettings = callWebServ.CallSaveCommisionSettingsService(
                                                                                            this.creditAski.Text,
                                                                                            this.mobileAski.Text,
                                                                                            this.cashAski.Text,
                                                                                            this.yellowAski.Text,
                                                                                            this.redAski.Text,
                                                                                            this.creditBaskent.Text,
                                                                                            this.mobileBaskent.Text,
                                                                                            this.cashBaskent.Text,
                                                                                            this.yellowBaskent.Text,
                                                                                            this.redBaskent.Text,
                                                                                            this.creditAksaray.Text,
                                                                                            this.mobileAksaray.Text,
                                                                                            this.cashAksaray.Text,
                                                                                            this.yellowAksaray.Text,
                                                                                            this.redAksaray.Text,
                                                                                            this.LoginDatas.AccessToken,
                                                                                            this.LoginDatas.User.Id);



            if (parserSaveCommisionSettings != null)
            {

                ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('" + parserSaveCommisionSettings.errorDescription + "'); ", true);
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:callOwn(); ", true);

                //Response.Redirect("../Account/EmptyKioskMutabakat.aspx");
            }
            else if (parserSaveCommisionSettings.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
            {
                Session.Abandon();
                Session.RemoveAll();
                Response.Redirect("../root/Login.aspx", true);
            }
            else
            {
                this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "Settings");
        }
    }
     * */

 
}
