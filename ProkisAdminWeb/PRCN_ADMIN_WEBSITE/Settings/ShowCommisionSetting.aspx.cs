﻿using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Account;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Settings_ShowCommisionSetting : System.Web.UI.Page
{

    private ParserLogin LoginDatas;
    private int ownSubMenuIndex = -1;
    private ParserListKioskName kiosks;

    public int instutionId;
    private GetKioskCommisionSetting instutionCommission;


    protected void Page_Load(object sender, EventArgs e)
    {

        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Settings/ListSetting.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }

        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }

        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.instutionId = Convert.ToInt32(Request.QueryString["instutionId"]);


        if (this.instutionId == 0 && ViewState["instutionId"] != null)
        {
            this.instutionId = Convert.ToInt32(ViewState["instutionId"].ToString());
        }


        if (!Page.IsPostBack)
        {
           this.BindCtrls();
        }

    }

    private void BindCtrls()
    {
        CallWebServices callWebServ = new CallWebServices();
        if (this.instutionId != 0)
        {
            this.instutionCommission = callWebServ.CallGetCommisionSettingService(this.instutionId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        }

        this.SetControls();
    }

    private void SetControls()
    {
        try
        {

            this.creditTextField.Text   = this.instutionCommission.creditCommision.ToString();
            this.mobileTextField.Text   = this.instutionCommission.mobileCommision.ToString();
            this.cashTextField.Text     = this.instutionCommission.cashCommision.ToString();
            this.yellowTextField.Text   = this.instutionCommission.yellowAlarm.ToString();
            this.redTextField.Text      = this.instutionCommission.redAlarm.ToString();
            this.instutionTextField.Text = this.instutionCommission.instutionName.ToString();
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowKioskSC");
        }

    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        SaveCommisionSetting();
    }


    private void SaveCommisionSetting()
    {
        try
        {
  
                CallWebServices callWebServ = new CallWebServices();

                ParserOperation parserUpdateCommisionSetting = callWebServ.CallUpdateCommisionSettingService(this.instutionId,
                                                                                                       this.creditTextField.Text,
                                                                                                       this.mobileTextField.Text,
                                                                                                       this.cashTextField.Text,  
                                                                                                       this.yellowTextField.Text,
                                                                                                       this.redTextField.Text,
                                                                                                       this.LoginDatas.User.Id,
                                                                                                       this.LoginDatas.AccessToken);


                if (parserUpdateCommisionSetting != null)
                {
                    string scriptText = "";

                    //this.messageArea.InnerHtml = parserSaveControlCashNotification.errorDescription;

                    scriptText = "parent.showMessageWindowCommisionSetting('İşlem Başarılı.');";
                    ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);

                }
                else if (parserUpdateCommisionSetting.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('Sisteme Erişilemiyor !'); ", true);
                }

            

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "Settings");
        }
    }
}