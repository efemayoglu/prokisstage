﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ListEnterKioskCashCount : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    //private int kioskId, operationType;
    private int operationId;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    //private int orderSelectionColumn = 1;
    //private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    public int kioskId;
    public int emptyKioskId;
    public string insertDate;
    private ParserGetKiosk kiosk;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];


        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Kiosk/ListEnterKioskCashCount.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        //if (this.ownSubMenuIndex == -1)
        //{
        //    Response.Redirect("../Default.aspx", true);
        //}

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        this.kioskId = Convert.ToInt32(Request.QueryString["kioskID"]);
        this.insertDate = ((Request.QueryString["insertDate"])).ToString();
        this.emptyKioskId = Convert.ToInt32(Request.QueryString["emptyKioskId"]);

        if (this.kioskId == 0 && ViewState["kioskID"] != null)
        {
            this.kioskId = Convert.ToInt32(ViewState["kioskID"].ToString());

        }

        this.numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            ViewState["OrderColumn"] = 1;
            ViewState["OrderDesc"] = 1;

            this.BuildControls();
        }
    }


    private void BuildControls()
    {
        CallWebServices callWebServ = new CallWebServices();
        if (this.kioskId != 0)
        {
            this.kiosk = callWebServ.CallGetKioskService(this.kioskId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        }

        this.SetControls();
       
    }

    private void SetControls()
    {
        if (kiosk.Kiosk.KioskName == null)
        {
            lblKioskName.Text = "Kiosk Bulunamadı" + " , Giriş Zamanı: " + "Tarih kaydı yok ";
        }
        else
            lblKioskName.Text = kiosk.Kiosk.KioskName + " , Giriş Zamanı: " + insertDate;

        try
        {
            //ListItem item = new ListItem();
            //item.Text = "Kiosk Seçiniz";
            //item.Value = "0";
            //this.kioskBox.Items.Add(item);
            //this.kioskBox.SelectedValue = "0";

            GetMutabakatCashCount();

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "EnterKioskEmptyCashCountSC");
        }
    }

    protected void GetMutabakatCashCount()
    {


        //int emptyKioskId = 0;
        CallWebServices callWebServ = new CallWebServices();
        if (this.kioskId != 0)
        {
            //if (kiosk.Kiosk == null)
            //{
            //     emptyKioskId = 0;
            //}
            //else
            ////emptyKioskId = Convert.ToInt32((this.kiosk.Kiosk.KioskId));

            ParserListMutabakatCashCounts expertCashCounts = callWebServ.CallGetMutabakatCashCountService(2, emptyKioskId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (expertCashCounts != null)
            {
                if (expertCashCounts.errorCode == 0)
                {
                    this.BindListTable(expertCashCounts);
                }
                else if (expertCashCounts.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    //ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + expertCashCounts.errorDescription + "'); ", true);
                }
            }
            else
            {
                //ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }

        }
    }


    private void BindListTable(ParserListMutabakatCashCounts expertCashCounts)
    {
        try
        {
            double sumAll = 0;

            TableRow[] Row = new TableRow[expertCashCounts.cashCounts.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                //Row[i].Attributes.Add("transactionId", listKioskMoneyDetail.kioskMoneyDetail[i].TransactionId.ToString());

                TableCell indexCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell cashTypeCell = new TableCell();
                TableCell cashCountCell = new TableCell();
                TableCell cashTypeNameCell = new TableCell();
                TableCell sumCell = new TableCell();

                TableCell fakeCell1 = new TableCell();
                TableCell fakeCell2 = new TableCell();

                indexCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                cashTypeCell.CssClass = "inputTitleCell4";
                cashCountCell.CssClass = "inputTitleCell4";
                cashTypeNameCell.CssClass = "inputTitleCell4";
                sumCell.CssClass = "inputTitleCell4";

                fakeCell1.CssClass = "inputTitleCell4";
                fakeCell2.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                //kioskNameCell.Text = merkezCashCounts.cashCounts[i].;
                cashTypeCell.Text = (expertCashCounts.cashCounts[i].cashTypeId).ToString();
                cashCountCell.Text = expertCashCounts.cashCounts[i].cashCount.ToString() + "  Adet";
                cashTypeNameCell.Text = (expertCashCounts.cashCounts[i].cashTypeName);
                
                //cashTypeValueCell.Text = expertCashCounts.cashCounts[i].cashTypeValue;
                sumCell.Text = (Convert.ToDouble(expertCashCounts.cashCounts[i].cashTypeValue) * (expertCashCounts.cashCounts[i].cashCount)).ToString() + " TL";

                sumAll = sumAll + (Convert.ToDouble(expertCashCounts.cashCounts[i].cashTypeValue) * (expertCashCounts.cashCounts[i].cashCount));

                fakeCell1.Visible = false;
                fakeCell2.Visible = false;

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
						 cashTypeNameCell,
                        //cashTypeCell,
                        cashCountCell,
                        //cashTypeValueCell
                        sumCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            
            
            this.itemsTable.Rows.AddRange(Row);

            TableRow addNewRow = new TableRow();
            TableCell totalMoneyTextCell = new TableCell();
            TableCell totalMoneyCell = new TableCell();
            TableCell spaceCell = new TableCell();


            spaceCell.CssClass = "inputTitleCell3";
            spaceCell.ColumnSpan = (2);

            totalMoneyTextCell.CssClass = "inputTitleCell3";
            totalMoneyCell.CssClass = "inputTitleCell3";

           

            spaceCell.Text = " ";


            spaceCell.Text = "Toplam Miktar:";
            totalMoneyCell.Text = sumAll + " TL ";

            addNewRow.Cells.Add(spaceCell);
            addNewRow.Cells.Add(totalMoneyTextCell);
            addNewRow.Cells.Add(totalMoneyCell);
           
            this.itemsTable.Rows.Add(addNewRow);

            //recordInfoLabel.Text = kiosk.Kiosk.KioskName ;
            lblKioskName.Text = "Kiosk Para Giriş Adetleri / " +  kiosk.Kiosk.KioskName;

            //TableRow pagingRow = new TableRow();
            //TableCell pagingCell = new TableCell();

            //pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            //pagingCell.HorizontalAlign = HorizontalAlign.Right;
            //pagingCell.Text = WebUtilities.GetPagingText(merkezCashCounts., this.pageNum, merkezCashCounts.recordCount);
            //pagingRow.Cells.Add(pagingCell);
            //this.itemsTable.Rows.AddAt(0, pagingRow);
            //this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskMoneyDetailBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    { 
    }


    protected void excelButton_Click(object sender, EventArgs e)
    {
      
    }

}