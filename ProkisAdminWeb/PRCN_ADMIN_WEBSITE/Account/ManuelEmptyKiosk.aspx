﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ManuelEmptyKiosk.aspx.cs" Inherits="Account_ManuelEmptyKiosk"   MasterPageFile="~/webctrls/AdminSiteMPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<asp:Content ID="pageContent" ContentPlaceHolderID="mainCPHolder" runat="server">
    <input type="hidden" id="pageNumRefField" runat="server" name="pageNumRefField" value="0" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { setInterval(function () { cache_clear() }, 120000); });
        function cache_clear() { window.location.reload(true); }
    </script>
    <script language="javascript" type="text/javascript">

        function showAlert(message) {
            var retVal = showAlertWindowManuel(message);
        }

        function checkForm() {
            var status = true;
            var messageText = "";

            if (document.getElementById('kioskBox').value == "0") {
                status = false;
                messageText = "Onaylanacak Kiosku Seçiniz !";
            }
            else {
                messageText = "";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }

    </script>
    <script type="text/javascript" language="javascript" src="../js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery.tablednd_0_5.js"></script>

    <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>

    <div align="center">
        <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp;</div>
    </div>
    <div>&nbsp;</div>
    <div align="center">
        <asp:Panel ID="ListPanel" runat="server" CssClass="containerPanel_95Pxauto">
            <div align="left" class="windowTitle_container_autox30">
                Merkezden Kiosk Para Toplama<hr style="border-bottom: 1px solid #b2b2b4;" />
            </div>
            <div align="center">
                <table>
                    <tr>
                        <td align="center" class="staticTextLine_30x20" style="width: 67px">Kiosk:</td>
                        <td align="left" style="width: 58px">
                            <asp:ListBox ID="kioskBox" CssClass="inputLine_120x25" runat="server" SelectionMode="Single" AutoPostBack ="true"
                                DataTextField="Name" DataValueField="Id" Rows="1"></asp:ListBox>
                        </td>
                    </tr>
                </table>
            </div>

            <div align="center">
                <table>
                    <tr>
                        <td colspan="4" align="center" style="padding-top: 3px; padding-bottom: 5px;">
                            <asp:Button ID="saveButton" CssClass="buttonCSSDesign" Style="height: 30px; width: 80px;" runat="server"
                                OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Onayla"></asp:Button>

                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>

    <br />
    <table width="95%">
        <tr align="right">
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="width: 80px">
                <asp:Label ID="Label5" runat="server" Text="powered by" CssClass="poweredbyTitle"></asp:Label>

            </td>
            <td style="width: 100px">
                <asp:LinkButton ID="LinkButton1" href="http://www.birlesikodeme.com/" runat="server" Text="BİRLEŞİK ÖDEME" CssClass="procenneTitle"></asp:LinkButton>

            </td>
        </tr>
    </table>
</asp:Content>