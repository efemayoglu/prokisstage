﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ListAccount : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int ownSubMenuIndex = -1;

    private int orderSelectionColumn = 2;
    private int orderSelectionDescAsc = 1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/ListAccount.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);
        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            CallWebServices callWebServ = new CallWebServices();
            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {

                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();

            }

            //ViewState["OrderColumn"] = 4;
            //ViewState["OrderDesc"] = 1;

            ListItem item = new ListItem();
            item.Text = "Kurum Seçiniz";
            item.Value = "0";
            this.instutionBox.Items.Add(item);
            this.instutionBox.SelectedValue = "0";

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.pageNum = 0;
            this.SearchOrder(4, 1);
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(4, 1);
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "AccountPeriod":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // AccountPeriod.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //AccountPeriod.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // AccountPeriod.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Institution":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // Institution.Text = "<a>Kiosk Adres    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Institution.Text = "<a>Kiosk Adres    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "InstitutionAmount":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // Ip.Text = "<a>Kiosk IP    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Ip.Text = "<a>Kiosk IP    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Ip.Text = "<a>Kiosk IP    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "DenizBankAmount":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Status.Text = "<a>Kiosk Durum    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Status.Text = "<a>Kiosk Durum    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Status.Text = "<a>Kiosk Durum    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "HeadquartersAmount":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // InsertionDate.Text = "<a>Eklenme Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //InsertionDate.Text = "<a>Eklenme Tarihi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // InsertionDate.Text = "<a>Eklenme Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CashCode":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //CashCode.Text = "<a>Şehir    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //City.Text = "<a>Şehir    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //City.Text = "<a>Şehir    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;


            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    private void BindListTable(ParserListAccounts list)
    {
        try
        {
           this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            TableRow[] Row = new TableRow[list.Account.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("accountId", list.Account[i].id.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell accountRangeNameCell = new TableCell();
                TableCell denizbankCell = new TableCell();
                TableCell instutionMoneyCell = new TableCell();
                TableCell merkezCell = new TableCell();
                TableCell moneyCodeCell = new TableCell();
                TableCell instutionNameCell = new TableCell();

                idCell.Visible = false;

                indexCell.CssClass = "inputTitleCell4";
                idCell.CssClass = "inputTitleCell4";
                accountRangeNameCell.CssClass = "inputTitleCell4";
                denizbankCell.CssClass = "inputTitleCell4";
                instutionMoneyCell.CssClass = "inputTitleCell4";
                merkezCell.CssClass = "inputTitleCell4";
                moneyCodeCell.CssClass = "inputTitleCell4";
                instutionNameCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                idCell.Text = list.Account[i].accountRangeId.ToString();
                accountRangeNameCell.Text = "<a href=\"javascript:void(0);\" onclick=\"showAccountDetailClicked(" + list.Account[i].accountRangeId + ", " + list.Account[i].instutionId + ");\" class=\"anylink\">" + list.Account[i].accountRangeName + "</a>";
                denizbankCell.Text = list.Account[i].denizbank + " TL";
                instutionMoneyCell.Text = list.Account[i].instutionMoney + " TL";
                merkezCell.Text = list.Account[i].merkez + " TL";
                moneyCodeCell.Text = list.Account[i].moneyCode + " TL";
                instutionNameCell.Text = list.Account[i].instutionName;

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        idCell,
						accountRangeNameCell,
                        instutionNameCell,
                        denizbankCell,
                        instutionMoneyCell,
                        merkezCell,
                        moneyCodeCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (list.recordCount < currentRecordEnd)
                currentRecordEnd = list.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + list.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(list.pageCount, this.pageNum, list.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListAccountBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebService = new CallWebServices();
            ParserListAccounts listAccounts = callWebService.CallListAccountService(Convert.ToInt32(this.numberOfItemField.Text),
                                                                                    this.pageNum,
                                                                                    recordCount,
                                                                                    pageCount,
                                                                                    orderSelectionColumn,
                                                                                    descAsc,
                                                                                    Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                                    this.LoginDatas.AccessToken,
                                                                                    this.LoginDatas.User.Id,
                                                                                    1);

            if (listAccounts != null)
            {
                if (listAccounts.errorCode == 0)
                {
                    this.BindListTable(listAccounts);
                }
                else if (listAccounts.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + listAccounts.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskCashSrch");
        }
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebService = new CallWebServices();
            ParserListAccounts lists = callWebService.CallListAccountService(Convert.ToInt32(this.numberOfItemField.Text),
                                                                                    this.pageNum,
                                                                                    recordCount,
                                                                                    pageCount,
                                                                                    orderSelectionColumn,
                                                                                    descAsc,
                                                                                    Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                                    this.LoginDatas.AccessToken,
                                                                                    this.LoginDatas.User.Id,
                                                                                    2);

            if (lists != null)
            {
                if (lists.errorCode == 0)
                {
                    for (int i = 0; i < lists.Account.Count; i++)
                    {
                        lists.Account[i].denizbank = lists.Account[i].denizbank.Replace('.', ',');
                        lists.Account[i].instutionMoney = lists.Account[i].instutionMoney.Replace('.', ',');
                        lists.Account[i].merkez = lists.Account[i].merkez.Replace('.', ',');
                        lists.Account[i].moneyCode = lists.Account[i].moneyCode.Replace('.', ',');
                    }


                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    string[] headerNames = { "No", "Hesap Aralığı", "Hesap Aralığı 2", "Kurum Miktar", "DenizBank", "Merkez Miktar",
                                               "Kurum", "Para Kodu", "No 2", " asds"};

                    exportExcell.ExportExcellByBlock(lists.Account, "Kiosk Hesap Raporu", headerNames);
                    //exportExcell.ExportExcell(lists.Account, lists.recordCount, "Hesap Listesi");
                }
                else if (lists.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + lists.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelAccount");
        }
    }
}