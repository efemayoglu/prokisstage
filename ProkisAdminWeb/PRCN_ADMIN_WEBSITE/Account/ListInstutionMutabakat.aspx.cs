﻿using MakeMutabakatParserNS;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ListInstutionMutabakat : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Transaction/ListTransactions.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.startDateField.Text = DateTime.Now.AddDays(-10).ToString("yyyy-MM-dd HH:mm:ss");
            this.endDateField.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {

                this.kioskBox.DataSource = kiosks.KioskNames;
                this.kioskBox.DataBind();

            }

            ListItem item = new ListItem();
            item.Text = "Kiosk Seçiniz";
            item.Value = "0";
            this.kioskBox.Items.Add(item);
            this.kioskBox.SelectedValue = "0";

            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {

                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();

            }

            ListItem item1 = new ListItem();
            item1.Text = "Kurum Seçiniz";
            item1.Value = "0";
            this.instutionBox.Items.Add(item1);
            this.instutionBox.SelectedValue = "0";

            ParserListInstutionOperationName operationType = callWebServ.CallGetOperationTypeNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (operationType != null)
            {

                this.operationTypeBox.DataSource = operationType.ParserListInstutionOperationNames;
                this.operationTypeBox.DataBind();

            }

            ListItem item2 = new ListItem();
            item2.Text = "İşlem Tipi Seçiniz";
            item2.Value = "0";
            this.operationTypeBox.Items.Add(item2);
            this.operationTypeBox.SelectedValue = "0";

            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;
            this.pageNum = 0;
            this.SearchOrder(4, 1);
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(4, 1);
    }

    private void BindListTable(MakeMutabakatParser datas)
    {
        try
        {
            TableRow[] Row = new TableRow[1];
            int startIndex = 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                //Row[i].Attributes.Add("transactionId", datas.[i].TransactionId.ToString());

                TableCell indexCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell instutionNameCell = new TableCell();
                TableCell faturaCountCell = new TableCell();
                TableCell faturaAmountCell = new TableCell();
                TableCell kartDolumCountCell = new TableCell();
                TableCell kartDolumAmountCell = new TableCell();
                TableCell totalCountCell = new TableCell();
                TableCell totalAmountCell = new TableCell();

                TableCell instutionCountCell = new TableCell();
                TableCell instutionAmountCell = new TableCell();

                TableCell detailsCell = new TableCell();

                indexCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                instutionNameCell.CssClass = "inputTitleCell4";
                faturaCountCell.CssClass = "inputTitleCell4";
                faturaAmountCell.CssClass = "inputTitleCell4";
                kartDolumCountCell.CssClass = "inputTitleCell4";
                kartDolumAmountCell.CssClass = "inputTitleCell4";
                totalCountCell.CssClass = "inputTitleCell4";
                totalAmountCell.CssClass = "inputTitleCell4";
                instutionCountCell.CssClass = "inputTitleCell4";
                instutionAmountCell.CssClass = "inputTitleCell4";
                detailsCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                //kioskNameCell.Text = datas.DbDatas..Transactions[i].KioskName;
                //instutionNameCell.Text = transactions.Transactions[i].instutionName;
                faturaCountCell.Text = datas.DbDatas.FaturaCount.ToString();
                faturaAmountCell.Text = datas.DbDatas.FaturaAmount.ToString();
                kartDolumCountCell.Text = datas.DbDatas.KartDolumCount.ToString();
                kartDolumAmountCell.Text = datas.DbDatas.KartDolumAmount.ToString();
                totalCountCell.Text = datas.DbDatas.TotalDolumCount.ToString();
                totalAmountCell.Text = datas.DbDatas.TotalDolumAmount.ToString();

                instutionCountCell.Text = datas.Adet.ToString();
                double totalNum = 0;

                for (int j = 0; j < datas.Faturalar.Length; j++)
                {
                    totalNum += datas.Faturalar[j].Tutar;
                }

                instutionAmountCell.Text = totalNum.ToString().Replace(',', '.');

                detailsCell.Width = Unit.Pixel(65);
                detailsCell.Text = "<a href=# onClick=\"useBOSValuesClicked(" + faturaAmountCell.Text + "," + faturaCountCell.Text + "," + kartDolumAmountCell.Text + "," + kartDolumCountCell.Text + "," + totalCountCell.Text + "," + totalAmountCell.Text + "," + instutionCountCell.Text + "," + instutionAmountCell.Text + ",'" + startDateField.Text + "'," + kioskBox.SelectedValue.ToString() + "," + instutionBox.SelectedValue.ToString() + ");\"><img src=../images/generate_over.png border=0 title=\"BÖS Değerleri ile Yap\" /></a>"
                    + "<a href=# onClick=\"useBGazValuesClicked(" + faturaAmountCell.Text + "," + faturaCountCell.Text + "," + kartDolumAmountCell.Text + "," + kartDolumCountCell.Text + "," + totalCountCell.Text + "," + totalAmountCell.Text + "," + instutionCountCell.Text + "," + instutionAmountCell.Text + ",'" + startDateField.Text + "'," + kioskBox.SelectedValue.ToString() + "," + instutionBox.SelectedValue.ToString() + ");\"><img src=../images/approve.png border=0 title=\"BGaz Değerleri ile Yap\" /></a>";

                Row[i].Cells.AddRange(new TableCell[]{
                        indexCell,
                        faturaCountCell,
                        faturaAmountCell,
                        kartDolumCountCell,
                        kartDolumAmountCell,
                        totalCountCell,
                        totalAmountCell,
                        instutionCountCell,
                        instutionAmountCell,
                        detailsCell
                    });

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            //TableRow pagingRow = new TableRow();
            //TableCell pagingCell = new TableCell();

            //int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            //int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            //pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            //pagingCell.HorizontalAlign = HorizontalAlign.Right;
            //pagingCell.Text = WebUtilities.GetPagingText(transactions.pageCount, this.pageNum, transactions.recordCount);
            //pagingRow.Cells.Add(pagingCell);
            //this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "TransactionId":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "KioskName":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CustomerName":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        CustomerName.Text = "<a>Müşteri Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        CustomerName.Text = "<a>Müşteri Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    CustomerName.Text = "<a>Müşteri Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "TransactionDate":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "TransactionStatus":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        TransactionStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        TransactionStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    TransactionStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Instution":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        Instution.Text = "<a>Kurum    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        Instution.Text = "<a>Kurum    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    Instution.Text = "<a>Kurum    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "AboneNo":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        AboneNo.Text = "<a>Abone No    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        AboneNo.Text = "<a>Abone No    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    AboneNo.Text = "<a>Abone No    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "OperationType":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    OperationType.Text = "<a>İşlem Tipi   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;
            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            //this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            ////ViewState["NumberOfItem"] = this.numberOfItemField.Text;
            //int recordCount = 0;
            //int pageCount = 0;
            //int custId = 0;

            //if (Convert.ToInt32(Request.QueryString["customerId"]) != 0)
            //{
            //    custId = Convert.ToInt32(Request.QueryString["customerId"]);
            //}

            //CallWebServices callWebServ = new CallWebServices();
            //ParserListTransactions transactions = callWebServ.CallListTransactionService(this.searchTextField.Text,
            //                                                                 Convert.ToDateTime(this.startDateField.Text),
            //                                                                 Convert.ToDateTime(this.endDateField.Text).AddDays(1).AddSeconds(-1),
            //                                                                 Convert.ToInt32(this.numberOfItemField.Text),
            //                                                                 this.pageNum,
            //                                                                 recordCount,
            //                                                                 pageCount,
            //                                                                 orderSelectionColumn,
            //                                                                 descAsc,
            //                                                                 custId,
            //                                                                 Convert.ToInt32(this.kioskBox.SelectedValue),
            //                                                                 Convert.ToInt32(this.instutionBox.SelectedValue),
            //                                                                 Convert.ToInt32(this.operationTypeBox.SelectedValue),
            //                                                                 this.LoginDatas.AccessToken,
            //                                                                 this.LoginDatas.User.Id);


            //if (transactions != null)
            //{
            //    if (transactions.errorCode == 0)
            //    {
            //        this.BindListTable(transactions);
            //    }
            //    else if (transactions.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
            //    {
            //        Session.Abandon();
            //        Session.RemoveAll();
            //        Response.Redirect("../root/Login.aspx", true);
            //    }
            //    else
            //    {
            //        ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + transactions.errorDescription + "'); ", true);
            //    }
            //}
            //else
            //{
            //    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            //}
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionSrch");
        }
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    private void ExportToExcel()
    {
        try
        {

            //int custId = 0;

            //if (Convert.ToInt32(Request.QueryString["customerId"]) != 0)
            //{
            //    custId = Convert.ToInt32(Request.QueryString["customerId"]);
            //}

            //CallWebServices callWebServ = new CallWebServices();
            //ParserListTransactions lists = callWebServ.CallListTransactionForExcelService(this.searchTextField.Text,
            //                                                                Convert.ToDateTime(this.startDateField.Text),
            //                                                                Convert.ToDateTime(this.endDateField.Text).AddDays(1).AddSeconds(-1),
            //                                                                Convert.ToInt16(ViewState["OrderColumn"]),
            //                                                                Convert.ToInt16(ViewState["OrderDesc"]),
            //                                                                custId,
            //                                                                Convert.ToInt32(this.kioskBox.SelectedValue), this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            //if (lists != null)
            //{
            //    if (lists.errorCode == 0)
            //    {

            //        ExportExcellDatas exportExcell = new ExportExcellDatas();
            //        exportExcell.ExportExcell(lists.Transactions, lists.recordCount, "İşlemler Listesi");
            //    }
            //    else if (lists.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
            //    {
            //        Session.Abandon();
            //        Session.RemoveAll();
            //        Response.Redirect("../root/Login.aspx", true);
            //    }
            //    else
            //    {
            //        ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + lists.errorDescription + "'); ", true);
            //    }
            //}
            //else
            //{
            //    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            //}
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelTransaction");
        }
    }
}