﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DailyAskiCode.aspx.cs" Inherits="Account_DailyAskiCode" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="../styles/style.css" />
    <script type="text/javascript" language="javascript" src="../js/wdws.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { setInterval(function () { cache_clear() }, 120000); });
        function cache_clear() { window.location.reload(true); }
    </script>
    <script language="javascript" type="text/javascript">

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function searchButtonClicked(sender) {
            document.getElementById('pageNumRefField').value = "0";
            return true;
        }

        function postForPaging(selectedPageNum) {
            document.getElementById('pageNumRefField').value = selectedPageNum;
            document.getElementById('navigateButton').click();
        }

        function validateNo(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
    <script type="text/javascript" language="javascript" src="../js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery.tablednd_0_5.js"></script>

    <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>
</head>
<body>
    <form id="form1" runat="server" defaultbutton="searchButton">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <input type="hidden" id="pageNumRefField" runat="server" name="pageNumRefField" value="0" />
        <asp:Button ID="navigateButton" runat="server" OnClick="navigateButton_Click" CssClass="dummy" />
        <asp:Button ID="searchButton" runat="server" ClientIDMode="Static" OnClick="searchButton_Click" CssClass="dummy" />
        <asp:HiddenField ID="idRefField" runat="server" Value="0" />
        <asp:HiddenField ID="oldOrderNumberField" runat="server" Value="0" />
        <asp:HiddenField ID="newOrderNumberField" runat="server" Value="0" />
        <div align="center">
            <asp:Panel ID="ListPanel" runat="server" CssClass="containerPanel_95Pxauto_noShadow" Width="99%">
                <div align="left" class="windowTitle_container_autox30">
                    Günlük Para Kod Durumları<hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>
                <table cellpadding="0" cellspacing="0" id="upTable" runat="server">
                    <tr>
                        <td style="width: 22px; height: 20px;">
                            <asp:ImageButton ID="excellButton" runat="server" ClientIDMode="Static" OnClick="excelButton_Click" ImageUrl="~/images/excel.jpg" />
                        </td>
                        <td style="width: 8px; height: 20px;"></td>
                        <td style="width: 200px; height: 20px;">
                            <asp:Label ID="recordInfoLabel" runat="server" Style="width: 200px; height: 20px; text-align: left" CssClass="data"></asp:Label>
                        </td>
                        <td style="height: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td style="height: 20px; width: 130px; text-align: right">
                            <asp:Label ID="Label1" Text="Listelenecek Kayıt Sayısı :  " runat="server" Style="width: 200px; height: 20px; text-align: left" CssClass="data"></asp:Label>
                        </td>
                        <td align="right" style="height: 20px; width: 33px">
                            <asp:TextBox ID="numberOfItemField" CssClass="inputLine_30x20_s" MaxLength="3" runat="server" onkeypress='validateNo(event)'></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="center" class="tableStyle1" colspan="6">
                            <asp:UpdatePanel ID="ListUPanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" RenderMode="Block">
                                <ContentTemplate>
                                    <asp:Table ID="itemsTable" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                        BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data">
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="#"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="İşlem No"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Kurum"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Müşteri Adı"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Abone No"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Durumu"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Oluşturulma Tarihi"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Tutarı"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Kod"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Kod Üretilme Adımı"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Kullanılma Tarihi"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Üreten Kullanıcı"></asp:TableHeaderCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="navigateButton" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <br />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
