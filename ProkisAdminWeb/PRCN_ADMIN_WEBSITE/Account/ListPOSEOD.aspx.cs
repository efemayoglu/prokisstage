﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Account;
using PRCNCORE.Parser.Kiosk;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Account_ListPOSEOD : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/ListPOSEOD.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.startDateField.Text = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd 00:00:00");



            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {
                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }

            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;
            this.pageNum = 0;
            
            this.SearchOrder(1, 1);
        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }

    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            int recordCount = 0;
            int pageCount = 0;
            CallWebServices callWebService = new CallWebServices();
            ParserListPOSEOD listPOSEOD = callWebService.CallListPOSEODService(kioskIds,
                                                                                            Convert.ToDateTime(this.startDateField.Text),
                                                                                            Convert.ToDateTime(this.endDateField.Text),
                                                                                            Convert.ToInt32(this.numberOfItemField.Text),
                                                                                            this.pageNum,
                                                                                            recordCount,
                                                                                            pageCount,
                                                                                            orderSelectionColumn,
                                                                                            descAsc,
                                                                                            this.LoginDatas.AccessToken,
                                                                                            this.LoginDatas.User.Id, 1);
            if (listPOSEOD != null)
            {
                if (listPOSEOD.errorCode == 0)
                {
                    this.BindListTable(listPOSEOD);
                }
                else if (listPOSEOD.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + listPOSEOD.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListFullCashBox");
        }
    }

    private void BindListTable(ParserListPOSEOD list)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            TableRow[] Row = new TableRow[list.POSEOD.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                //Row[i].Attributes.Add("cashFullId", list.kioskFullCashBox[i].id.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                
                TableCell kioskNameCell = new TableCell();
                TableCell transactionDateCell = new TableCell();
                TableCell transactionCountCell = new TableCell();
                TableCell POSTransactionCountCell = new TableCell();
                TableCell distinctionTransaction = new TableCell();
                TableCell amountCell = new TableCell();
                TableCell POSAmountCell = new TableCell();
                TableCell distinctionAmountCell = new TableCell();
                TableCell statusCell = new TableCell();
                TableCell explanationCell = new TableCell();
                TableCell userCell = new TableCell();
 
                idCell.Visible = false;

                indexCell.CssClass = "inputTitleCell4";
                idCell.CssClass = "inputTitleCell4";

                kioskNameCell.CssClass = "inputTitleCell4";
                transactionDateCell.CssClass = "inputTitleCell4";
                transactionCountCell.CssClass = "inputTitleCell4";
                POSTransactionCountCell.CssClass = "inputTitleCell4";
                distinctionTransaction.CssClass = "inputTitleCell4";
                amountCell.CssClass = "inputTitleCell4";
                POSAmountCell.CssClass = "inputTitleCell4";
                distinctionAmountCell.CssClass = "inputTitleCell4";
                statusCell.CssClass= "inputTitleCell4";
                explanationCell.CssClass= "inputTitleCell4";
                userCell.CssClass= "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                //idCell.Text = listKioskMoney.kioskMoney[i].kioskId.ToString();
                kioskNameCell.Text = list.POSEOD[i].KioskName;
                transactionDateCell.Text = list.POSEOD[i].TransactionDate;
                transactionCountCell.Text = list.POSEOD[i].TransactionCount;
                POSTransactionCountCell.Text = list.POSEOD[i].POSTransactionCount;
                distinctionTransaction.Text = list.POSEOD[i].DistinctionTransaction;
                amountCell.Text = list.POSEOD[i].Amount;
                POSAmountCell.Text = list.POSEOD[i].POSAmount;
                distinctionAmountCell.Text = list.POSEOD[i].DistinctionAmount;
                statusCell.Text = list.POSEOD[i].Status;
                explanationCell.Text = list.POSEOD[i].Explanation;
                userCell.Text = list.POSEOD[i].User;

                //detailsCell.Width = Unit.Pixel(30);
                //detailsCell.Text = "<a href=# onClick=\"showKioskFullCashBoxCountClicked(" + list.kioskFullCashBox[i].id + ");\" class=\"detailCodeButton\" Title=\"Para Adetleri Göster\"></a>";

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        idCell,
 
						kioskNameCell,
                        transactionDateCell,
                        transactionCountCell,
                        POSTransactionCountCell,
                        distinctionTransaction,
                        amountCell,
                        POSAmountCell,
                        distinctionAmountCell,
                        statusCell,
                        explanationCell,
                        userCell
					});

                if (list.POSEOD[i].ResultCount - 1 == list.recordCount)
                {
                    Row[i].CssClass = "inputTitleCell99";
                   
                    //Row[i].Cells[4].Text = "";
                    Row[i].Cells[2].Text = "TOPLAM";
                    //Row[i].Cells[3].Text = "";
                    //Row[i].Cells[0].Text = "";
                }
                else
                {
                    if (i % 2 == 0)
                        Row[i].CssClass = "listrow";
                    else
                        Row[i].CssClass = "listRowAlternate";
                }
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (list.recordCount < currentRecordEnd)
                currentRecordEnd = list.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + list.recordCount.ToString() + " kayıt bulundu.";
            }

            TableRow addNewRow = new TableRow();
            TableCell totalAmountCell = new TableCell();
            TableCell spaceCell = new TableCell();
            TableCell spaceCell1 = new TableCell();



            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(list.pageCount, this.pageNum, list.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskCashEmptyBDT");
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(1, 1);
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            CallWebServices callWebService = new CallWebServices();
            ParserListPOSEOD listPOSEOD = callWebService.CallListPOSEODService(kioskIds,
                                                                                            Convert.ToDateTime(this.startDateField.Text),
                                                                                            Convert.ToDateTime(this.endDateField.Text),
                                                                                            Convert.ToInt32(this.numberOfItemField.Text),
                                                                                            this.pageNum,
                                                                                            recordCount,
                                                                                            pageCount,
                                                                                            orderSelectionColumn,
                                                                                            descAsc,
                                                                                            this.LoginDatas.AccessToken,
                                                                                            this.LoginDatas.User.Id, 2);


            if (listPOSEOD != null)
            {
                if (listPOSEOD.errorCode == 0)
                {
                    


                    ExportExcellDatas exportExcell = new ExportExcellDatas();

                    string[] headerNames = {"a","b", "Kiosk Adı", "İşlem Zamanı", "İşlem Sayısı", "POS İşlem Sayısı", "İşlem Farkı",	 "Tutar",
                                              "POS Tutarı", "Tutar Farkı","Durumu","Açıklama","Kullanıcı"};

                    exportExcell.ExportExcellByBlock(listPOSEOD.POSEOD, "Kiosk Para Doldurma Listesi", headerNames);

                    // exportExcell.ExportExcell(listFullCashBox.kioskFullCashBox, listFullCashBox.recordCount, "Kiosk Para Doldurma Listesi");
                }
                else if (listPOSEOD.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + listPOSEOD.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelParaDoldurma");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "KioskName":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "TransactionDate":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // ExpertUser.Text = "<a>Yetkili Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // ExpertUser.Text = "<a>Yetkili Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //ExpertUser.Text = "<a>Yetkili Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "TransactionCount":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // TotalAmout.Text = "<a>Para Miktarı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // TotalAmout.Text = "<a>Para Miktarı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // TotalAmout.Text = "<a>Para Miktarı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "POSTransactionCount":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "DistinctionTransaction":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Amount":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "POSAmount":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "DistinctionAmount":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Status":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Explanation":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "User":
                if (orderSelectionColumn == 11)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 11;
                    orderSelectionDescAsc = 1;
                }
                break;


            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }
}