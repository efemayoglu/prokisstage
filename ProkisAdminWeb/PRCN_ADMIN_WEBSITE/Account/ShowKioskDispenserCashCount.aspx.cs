﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Account;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ShowKioskDispenserCashCount : System.Web.UI.Page
{

    private ParserLogin LoginDatas;
    private int ownSubMenuIndex = -1;
    private ParserGetKiosk kiosks;
    public ParserGetKioskCashDispenser kioskcashdispenser;

    public int kioskId;
    //private GetKioskCutofMonitoring cutKiosk;

    //private ParserGetDispenserCashCount dispensercashcount;
    public int Id;

    private string a1;
    private string a2;
    private string a3;
    private string a4;
    private string a5;
    private string a6;
    private string a7;
    private string a8;
    private string a9;
    private string a10;
    private string a11;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/ListKioskDispenserCashCount.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }

        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }

        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.kioskId = Convert.ToInt32(Request.QueryString["kioskID"]);
        //this.Id = Convert.ToInt32(Request.QueryString["Id"]);

        if (this.kioskId == 0 && ViewState["kioskID"] != null)
        {
            this.kioskId = Convert.ToInt32(ViewState["kioskID"].ToString());
        }

        //if (this.Id == 0 && ViewState["Id"] != null)
        //{
        //    this.Id = Convert.ToInt32(ViewState["Id"].ToString());
        //}


        if (!Page.IsPostBack)
        {
            this.BindCtrls();
        }

    }

    private void BindCtrls()
    {
        CallWebServices callWebServ = new CallWebServices();
        if (this.kioskId != 0)
        {
            this.kiosks = callWebServ.CallGetKioskService(this.kioskId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        }

        if (this.kioskId != 0)
        {
            this.kioskcashdispenser = callWebServ.CallGetDispenserCashCountService(this.kioskId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        }

        this.SetControls();
        //this.SetControls();
    }

    private void SetControls()
    {
        try
        {
            if (this.kiosks.Kiosk!=null)
                this.nameField.Text = this.kiosks.Kiosk.KioskName;
            if (this.kioskcashdispenser != null)
                this.nameField.Text = this.kioskcashdispenser.KioskCashDispenser.kioskId;
       
            

            //CashTB1.Text = kioskcashdispenser.KioskCashDispenser.firstDenomValue;

            //a1 = kioskcashdispenser.KioskCashDispenser.firstDenomNumber;
            //a2 = kioskcashdispenser.KioskCashDispenser.secondDenomNumber;
            //a3 = kioskcashdispenser.KioskCashDispenser.thirdDenomNumber;
            //a4 = kioskcashdispenser.KioskCashDispenser.fourthDenomNumber;
            //a5 = kioskcashdispenser.KioskCashDispenser.fifthDenomNumber;
            //a6 = kioskcashdispenser.KioskCashDispenser.sixthDenomNumber;
            //a7 = kioskcashdispenser.KioskCashDispenser.seventhDenomNumber;
            //a8 = kioskcashdispenser.KioskCashDispenser.eighthDenomNumber;
            //a9 = kioskcashdispenser.KioskCashDispenser.ninthDenomNumber;
            //a10 = kioskcashdispenser.KioskCashDispenser.tenthDenomNumber;
            //a11 = kioskcashdispenser.KioskCashDispenser.eleventhDenomNumber;

            this.Label1.Text = kioskcashdispenser.KioskCashDispenser.firstDenomValue;
            this.Label2.Text = kioskcashdispenser.KioskCashDispenser.secondDenomValue;
            this.Label3.Text = kioskcashdispenser.KioskCashDispenser.thirdDenomValue;
            this.Label4.Text = kioskcashdispenser.KioskCashDispenser.fourthDenomValue;
            this.Label5.Text = kioskcashdispenser.KioskCashDispenser.fifthDenomValue;
            this.Label6.Text = kioskcashdispenser.KioskCashDispenser.sixthDenomValue;
            this.Label7.Text = kioskcashdispenser.KioskCashDispenser.seventhDenomValue;
            this.Label8.Text = kioskcashdispenser.KioskCashDispenser.eighthDenomValue;

            this.TotalAmountTb.Text = "0";
            this.CashYpeTB1.Text = kioskcashdispenser.KioskCashDispenser.firstDenomNumber;
            this.CashYpeTB2.Text = kioskcashdispenser.KioskCashDispenser.secondDenomNumber;
            this.CashYpeTB3.Text = kioskcashdispenser.KioskCashDispenser.thirdDenomNumber;
            this.CashYpeTB4.Text = kioskcashdispenser.KioskCashDispenser.fourthDenomNumber;
            this.CashYpeTB5.Text = kioskcashdispenser.KioskCashDispenser.fifthDenomNumber;
            this.CashYpeTB6.Text = kioskcashdispenser.KioskCashDispenser.sixthDenomNumber;
            this.CashYpeTB7.Text = kioskcashdispenser.KioskCashDispenser.seventhDenomNumber;
            this.CashYpeTB8.Text = kioskcashdispenser.KioskCashDispenser.eighthDenomNumber;

            this.CashYpeTB9.Text = kioskcashdispenser.KioskCashDispenser.Casette1Reject;
            this.CashYpeTB10.Text = kioskcashdispenser.KioskCashDispenser.Casette2Reject;
            this.CashYpeTB11.Text = kioskcashdispenser.KioskCashDispenser.Casette3Reject;
            this.CashYpeTB12.Text = kioskcashdispenser.KioskCashDispenser.Casette4Reject;


            Session["firstOld"] = kioskcashdispenser.KioskCashDispenser.firstDenomNumber;
            Session["secondOld"] = kioskcashdispenser.KioskCashDispenser.secondDenomNumber;
            Session["thirdOld"] = kioskcashdispenser.KioskCashDispenser.thirdDenomNumber;
            Session["fourthOld"] = kioskcashdispenser.KioskCashDispenser.fourthDenomNumber;
            Session["fifthOld"] = kioskcashdispenser.KioskCashDispenser.fifthDenomNumber;
            Session["sixthOld"] = kioskcashdispenser.KioskCashDispenser.sixthDenomNumber;
            Session["seventhOld"] = kioskcashdispenser.KioskCashDispenser.seventhDenomNumber;
            Session["eighthOld"] = kioskcashdispenser.KioskCashDispenser.eighthDenomNumber;

            Session["ninethOld"] = kioskcashdispenser.KioskCashDispenser.Casette1Reject;
            Session["tenthOld"] = kioskcashdispenser.KioskCashDispenser.Casette2Reject;
            Session["eleventhOld"] = kioskcashdispenser.KioskCashDispenser.Casette3Reject;
            Session["twelfthOld"] = kioskcashdispenser.KioskCashDispenser.Casette4Reject;




        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "EnterListKioskDispenserCashCountSC");
        }
    }

    protected void CashYpeTB1_TextChanged(object sender, EventArgs e)
    {
        TextBox xsenderTB = (TextBox)sender;

        decimal output;
        bool result = Decimal.TryParse(xsenderTB.Text, out output);

        if (String.IsNullOrEmpty(xsenderTB.Text))
            xsenderTB.Text = "0";

        if (result)
        {
            double totalAmount = (Convert.ToInt32(this.CashYpeTB1.Text) * 50 +
                                  Convert.ToInt32(this.CashYpeTB2.Text) * 20 +
                                  Convert.ToInt32(this.CashYpeTB3.Text) * 10 +
                                  Convert.ToInt32(this.CashYpeTB4.Text) * 5 +
                                  Convert.ToInt32(this.CashYpeTB5.Text) * 1 +
                                  Convert.ToInt32(this.CashYpeTB6.Text) / 4.0 +
                                  Convert.ToInt32(this.CashYpeTB7.Text) / 10.0 +
                                  Convert.ToInt32(this.CashYpeTB8.Text) / 20.0
                                 //Convert.ToInt32(this.CashYpeTB9.Text) / 4.0 +
                                 //Convert.ToInt32(this.CashYpeTB10.Text) / 10.0 +
                                 //Convert.ToInt32(this.CashYpeTB11.Text) / 20.0
                                 );

            this.TotalAmountTb.Text = totalAmount.ToString();
        }
        else
        {
            this.messageArea.InnerHtml = "Lütfen rakam giriniz. !";
        }
    }


    protected void saveButton_Click(object sender, EventArgs e)
    {
        UpdateDispenserCashCount();

    }

    private void UpdateDispenserCashCount()
    {
        try
        {

            CallWebServices callWebServ = new CallWebServices();

            ParserOperation parserUpdateDispenserCashCount = callWebServ.CallUpdateDispenserCashCountService(kioskId,
                                                                               this.LoginDatas.User.Id,
                                                                               this.LoginDatas.AccessToken,
                                                                               CashYpeTB1.Text,
                                                                               CashYpeTB2.Text,
                                                                               CashYpeTB3.Text,
                                                                               CashYpeTB4.Text,
                                                                               CashYpeTB5.Text,
                                                                               CashYpeTB6.Text,
                                                                               CashYpeTB7.Text,
                                                                               CashYpeTB8.Text,
                                                                               CashYpeTB9.Text,
                                                                               CashYpeTB10.Text,
                                                                               CashYpeTB11.Text,
                                                                               CashYpeTB12.Text,
                                                                               1,1,1,1,1,1,1,1,
                                                                               Convert.ToDecimal(this.TotalAmountTb.Text));

            
            if (parserUpdateDispenserCashCount != null)
            {
                string scriptText = "";

                //this.messageArea.InnerHtml = parserSaveControlCashNotification.errorDescription;

                scriptText = "parent.showMessageWindowKioskDispenserCashCount('İşlem Başarılı.');";
                ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);




               //if (this.kioskId != 0)
               //{
               //    this.kioskcashdispenser = callWebServ.CallGetKioskCashDispenserService(this.kioskId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
               //}
               //
                SaveDispenserMonitoring();

            }
            else if (parserUpdateDispenserCashCount.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
            {
                Session.Abandon();
                Session.RemoveAll();
                Response.Redirect("../root/Login.aspx", true);
            }
            else
            {
                this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('Sisteme Erişilemiyor !'); ", true);
            }
            
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "Settings");
        }

    }


    private void SaveDispenserMonitoring()
    {
        try
        {

            string a = Session["firstOld"].ToString();
            string a2 = Session["secondOld"].ToString();
            string a3 = Session["thirdOld"].ToString();
            string a4 = Session["fourthOld"].ToString();
            string a5 = Session["fifthOld"].ToString();
            string a6 = Session["sixthOld"].ToString();
            string a7 = Session["seventhOld"].ToString();
            string a8 = Session["eighthOld"].ToString();
            
             string r1 = Session["ninethOld"].ToString();
             string r2 = Session["tenthOld"].ToString();
             string r3 = Session["eleventhOld"].ToString();
             string r4 = Session["twelfthOld"].ToString();

            CallWebServices callWebServ = new CallWebServices();

            
            ParserOperation parserUpdateDispenserCashCount = callWebServ.CallSaveDispenserMonitoringService(kioskId.ToString(),
                                                                       this.LoginDatas.AccessToken,
                                                                       this.LoginDatas.User.Id,
                                                                       CashYpeTB1.Text,//Convert.ToInt32(kioskcashdispenser.KioskCashDispenser.firstDenomNumber).ToString(),        //CashYpeTB1.Text,
                                                                       CashYpeTB2.Text,//Convert.ToInt32(kioskcashdispenser.KioskCashDispenser.secondDenomNumber).ToString(),       //CashYpeTB2.Text,
                                                                       CashYpeTB3.Text,//Convert.ToInt32(kioskcashdispenser.KioskCashDispenser.thirdDenomNumber).ToString(),        //CashYpeTB3.Text,
                                                                       CashYpeTB4.Text,//Convert.ToInt32(kioskcashdispenser.KioskCashDispenser.fourthDenomNumber).ToString(),       //CashYpeTB4.Text,
                                                                       CashYpeTB5.Text,//Convert.ToInt32(kioskcashdispenser.KioskCashDispenser.fifthDenomNumber).ToString(),        //CashYpeTB5.Text,
                                                                       CashYpeTB6.Text,//Convert.ToInt32(kioskcashdispenser.KioskCashDispenser.sixthDenomNumber).ToString(),        //CashYpeTB6.Text,
                                                                       CashYpeTB7.Text,//Convert.ToInt32(kioskcashdispenser.KioskCashDispenser.seventhDenomNumber).ToString(),      //CashYpeTB7.Text,
                                                                       CashYpeTB8.Text,//Convert.ToInt32(kioskcashdispenser.KioskCashDispenser.eighthDenomNumber).ToString(),       //CashYpeTB8.Text,
                                                                       CashYpeTB9.Text,//Convert.ToInt32(kioskcashdispenser.KioskCashDispenser.ninthDenomNumber).ToString(),        //CashYpeTB9.Text,
                                                                       CashYpeTB10.Text,//Convert.ToInt32(kioskcashdispenser.KioskCashDispenser.tenthDenomNumber).ToString(),        //CashYpeTB10.Text,
                                                                       CashYpeTB11.Text,//Convert.ToInt32(kioskcashdispenser.KioskCashDispenser.eleventhDenomNumber).ToString(),     //CashYpeTB11.Text,
                                                                       CashYpeTB12.Text,
                                                                      a  ,
                                                                      a2 ,
                                                                      a3 ,
                                                                      a4 ,
                                                                      a5 ,
                                                                      a6 ,
                                                                      a7 ,
                                                                      a8 ,
                                                                      r1,
                                                                      r2,
                                                                      r3,
                                                                      r4
                                                                      //a9 ,
                                                                      //a10,
                                                                      //a11
                                                                      );


            if (parserUpdateDispenserCashCount != null)
            {
                string scriptText = "";

                //this.messageArea.InnerHtml = parserSaveControlCashNotification.errorDescription;

                scriptText = "parent.showMessageWindowKioskDispenserCashCount('İşlem Kaydı Alındı.');";
                ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);

            }
            else if (parserUpdateDispenserCashCount.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
            {
                Session.Abandon();
                Session.RemoveAll();
                Response.Redirect("../root/Login.aspx", true);
            }
            else
            {
                this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('Sisteme Erişilemiyor !'); ", true);
            }
            
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "Settings");
        }

    }



}