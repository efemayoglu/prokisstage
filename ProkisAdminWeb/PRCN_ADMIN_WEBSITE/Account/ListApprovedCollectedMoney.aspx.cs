﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Other;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ListApprovedCollectedMoney : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 3;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;
    string kioskIds = "";
    string kioskIndex = "";

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/ListApprovedCollectedMoney.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        if (Session["numberOfItemField"] == null)
            numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));
        else
            numberOfItemsPerPage = Convert.ToInt16(Session["numberOfItemField"]);


        if (!Page.IsPostBack)
        {
            CallWebServices callWebServ = new CallWebServices();

            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {

                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);
                
            }

            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {

                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();

            }

            ParserListStatus status = callWebServ.CallGetStatusService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (status != null)
            {

                this.statusBox.DataSource = status.StatusNames;
                this.statusBox.DataBind();

            }


            ListItem item = new ListItem();
            item.Text = "Kurum Seçiniz";
            item.Value = "0";

            this.instutionBox.Items.Add(item);
            this.instutionBox.SelectedValue = "0";


            ListItem itemstatus = new ListItem();
            itemstatus.Text = "Durum Seçiniz";
            itemstatus.Value = "0";


            this.statusBox.Items.Add(itemstatus);

            this.statusBox.SelectedValue = "0";

            if (Session["startDate"] == null)
                this.startDateField.Text = DateTime.Now.ToString("yyyy-MM-dd 00:00:00");
            else
                this.startDateField.Text = Convert.ToString(Session["startDate"]);

            if (Session["endDate"] == null)
                this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd 00:00:00");
            else
                this.endDateField.Text = Convert.ToString(Session["endDate"]);



            if (Session["MultipleSelection1"] == null)
            {
                kioskIds = "0";
                this.MultipleSelection1.selectedIndex = "";
                kioskIndex = "";
            }
            else
            {
                kioskIds = Convert.ToString(Session["MultipleSelection1"]);
                kioskIndex = Convert.ToString(Session["MultipleSelection1Index"]);
                this.MultipleSelection1.selectedIndex = kioskIndex;
            }

            if (Session["instutionBox"] == null)
                instutionBox.SelectedValue = "0";
            else
                instutionBox.SelectedValue = Convert.ToString(Session["instutionBox"]);

            if (Session["statusBox"] == null)
                statusBox.SelectedValue = "0";
            else
                statusBox.SelectedValue = Convert.ToString(Session["statusBox"]);

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;
            this.pageNum = 0;
            this.SearchOrder(3, 1);

           
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        Session["startDate"] = startDateField.Text;
        Session["endDate"] = endDateField.Text;
        Session["numberOfItemField"] = numberOfItemField.Text;

        if ((MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü"))
            kioskIds = "";
        else
        {
            kioskIds = MultipleSelection1.sValue;
            kioskIndex = MultipleSelection1.selectedIndex;
            this.MultipleSelection1.selectedIndex = kioskIndex;
        }

        Session["MultipleSelection1"] = kioskIds;
        Session["MultipleSelection1Index"] = kioskIndex;
        Session["instutionBox"] = instutionBox.Text;
        Session["statusBox"] = statusBox.Text;

        this.pageNum = 0;
        this.SearchOrder(3, 1);
    }

    private string ChangeMoneyFormat(string value)
    {
        value = value.Replace(',', '.');
        return String.Format(new System.Globalization.CultureInfo("tr-TR"), "{0:C}", Convert.ToDouble(value));
    }

    private void BindListTable(ParserListMutabakat items)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            TableRow[] Row = new TableRow[items.Mutabakats.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("Id", items.Mutabakats[i].id.ToString());

                TableCell indexCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell institutionCell = new TableCell();
                TableCell insertionDateCell = new TableCell();
                TableCell operatorNameCell = new TableCell();
                TableCell accountOwnerCell = new TableCell();
                TableCell accountDateCell = new TableCell();

                //TableCell mutabakatTypeCell = new TableCell();
                TableCell mutabakatDateCell = new TableCell();
                TableCell mutabakatUserCell = new TableCell();

                TableCell inputAmountCell = new TableCell();
                TableCell outputAmountCell = new TableCell();
                TableCell subtractionCell = new TableCell();
                TableCell statusCell = new TableCell();
                TableCell explanationCell = new TableCell();
                TableCell detailsCell = new TableCell();
                TableCell referenceNumberCell = new TableCell();

                TableCell fakeCell1 = new TableCell();
                TableCell fakeCell2 = new TableCell();
                TableCell fakeCell3 = new TableCell();
                TableCell fakeCell4 = new TableCell();
                TableCell fakeCell5 = new TableCell();
                TableCell fakeCell6 = new TableCell();

                fakeCell1.CssClass = "inputTitleCell4";
                fakeCell2.CssClass = "inputTitleCell4";
                fakeCell3.CssClass = "inputTitleCell4";
                fakeCell4.CssClass = "inputTitleCell4";
                fakeCell5.CssClass = "inputTitleCell4";
                fakeCell6.CssClass = "inputTitleCell4";


                fakeCell1.Visible = false;
                fakeCell2.Visible = false;
                fakeCell3.Visible = false;
                fakeCell4.Visible = false;
                fakeCell5.Visible = false;
                fakeCell6.Visible = false;

                indexCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                //mutabakatTypeCell.CssClass = "inputTitleCell4";
                mutabakatDateCell.CssClass = "inputTitleCell4";
                //mutabakatUSerCell.CssClass = "inputTitleCell4";
                accountOwnerCell.CssClass = "inputTitleCell4";
                inputAmountCell.CssClass = "inputTitleCell4";
                outputAmountCell.CssClass = "inputTitleCell4";
                subtractionCell.CssClass = "inputTitleCell4";
                detailsCell.CssClass = "inputTitleCell4";
                institutionCell.CssClass = "inputTitleCell4";
                insertionDateCell.CssClass = "inputTitleCell4";
                operatorNameCell.CssClass = "inputTitleCell4";
                accountOwnerCell.CssClass = "inputTitleCell4";
                statusCell.CssClass = "inputTitleCell4";
                explanationCell.CssClass = "inputTitleCell4";
                mutabakatDateCell.CssClass = "inputTitleCell4";
                mutabakatUserCell.CssClass = "inputTitleCell4";
                accountDateCell.CssClass = "inputTitleCell4";
                referenceNumberCell.CssClass = "inputTitleCell4";
                

                indexCell.Text = (startIndex + i).ToString();


                institutionCell.Text = items.Mutabakats[i].institution;
                insertionDateCell.Text = items.Mutabakats[i].insertionDate;
                operatorNameCell.Text = items.Mutabakats[i].operatorName;
                accountOwnerCell.Text = items.Mutabakats[i].accountOwner;
                referenceNumberCell.Text = items.Mutabakats[i].ReferenceNumber;

                string accountDate = "";
                if (items.Mutabakats[i].accountDate == "" || items.Mutabakats[i].accountDate.Contains("1900"))
                    accountDate = "Belirtilmedi";
                else
                    accountDate = items.Mutabakats[i].accountDate;

                accountDateCell.Text = accountDate;

                string mutabakatDate = "";
                if (items.Mutabakats[i].mutabakatDate.Contains("1900"))
                    mutabakatDate = "Belirtilmedi";
                else
                    mutabakatDate = items.Mutabakats[i].mutabakatDate;

                mutabakatDateCell.Text = mutabakatDate;
                mutabakatUserCell.Text = items.Mutabakats[i].mutabakatUser;


                inputAmountCell.Text = items.Mutabakats[i].inputAmount + " TL";
                outputAmountCell.Text = items.Mutabakats[i].outputAmount + " TL";
                
                statusCell.Text = items.Mutabakats[i].status;
                subtractionCell.Text = items.Mutabakats[i].amountDifference + " TL";

                if (this.LoginDatas.UserRoles[ownSubMenuIndex].CanDelete == "1")
                {
                    explanationCell.Text = items.Mutabakats[i].explanation;
                }
                else
                    explanationCell.Text = "***";

                string enteredMoney="";
                if (Convert.ToInt16(items.Mutabakats[i].StatusId) >1)
                {
                    enteredMoney = "<a href=# onClick=\"showEnterKioskEmptyCashCountButtonClicked(" + items.Mutabakats[i].id + ",'" + (items.Mutabakats[i].insertionDate).ToString() + "'," + (items.Mutabakats[i].emptyKioskId) + " );\" class=\"accountCodeButton\" Title=\"Giriş Detayını Göster\"></a>";
                }
                string mutabakat="";
                if (this.LoginDatas.UserRoles[ownSubMenuIndex].CanEdit == "1" && Convert.ToInt16(items.Mutabakats[i].StatusId) == 2)
                {
                    mutabakat = "<a href=# onClick=\"editEmptyKioskMutabakatClicked(" + items.Mutabakats[i].id + ",'" + (items.Mutabakats[i].insertionDate).ToString() + "'," + (items.Mutabakats[i].emptyKioskId) + " );\" class=\"approvedCodeButton\" Title=\"Onaylama Detayını Göster\"></a>";
                }

                string merkez="";
                string fark="";
                if (this.LoginDatas.UserRoles[ownSubMenuIndex].CanDelete == "1" && Convert.ToInt16(items.Mutabakats[i].StatusId) >= 2)
                {
                    merkez = "<a href=# onClick=\"showKioskEmptyCashCountClicked(" + items.Mutabakats[i].id + "," + items.Mutabakats[i].emptyKioskId + "," + 1 + ");\" class=\"detailCodeButton\" Title=\"Merkez Detayını Göster\"></a>";
                    fark = "<a href=# onClick=\"showKioskEmptyCashCountClicked(" + items.Mutabakats[i].id + "," + items.Mutabakats[i].emptyKioskId + "," + 0 + ");\" class=\"detailCodeButton\" Title=\"Fark Detayını Göster\"></a>";

                }

                if (this.LoginDatas.UserRoles[ownSubMenuIndex].CanAdd == "1" && Convert.ToInt16(items.Mutabakats[i].StatusId) < 3)
                {
                    kioskNameCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editEnterKioskEmptyCashCountButtonClicked(" + items.Mutabakats[i].id +
                           ",'" + (items.Mutabakats[i].insertionDate).ToString() + "'," + (items.Mutabakats[i].emptyKioskId) + " );\" class=\"anylink\">" + items.Mutabakats[i].kioskName.ToString() + "</a>";

                }
                else
                {
                    kioskNameCell.Text = items.Mutabakats[i].kioskName.ToString();
                }

                detailsCell.Text = enteredMoney + "" + mutabakat + "" + merkez + "" + fark;

                //}

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        kioskNameCell,
                        institutionCell,
                        insertionDateCell,
                        operatorNameCell,
                        accountOwnerCell,
                        accountDateCell,
                        mutabakatUserCell,
                        mutabakatDateCell,
                       
                        inputAmountCell,
                        outputAmountCell,
                        subtractionCell,
                        referenceNumberCell,
                        statusCell,
                        explanationCell,
                        detailsCell
					});

                if (i == Row.Length-1)              
                {
                    string totalmerkez = "";
                    string totalfark = "";

                    //tırnaklar
                    totalmerkez = "<a href=# onClick=\"showKioskTotalEmptyCashCountClicked(" + items.Mutabakats[i].id + "," + items.Mutabakats[i].emptyKioskId + "," + 1 + ",'" + Convert.ToDateTime(this.startDateField.Text) + "','" + Convert.ToDateTime(this.endDateField.Text) + "');\" class=\"detailCodeButton\" Title=\"Merkez Detayını Göster\"></a>";
                    totalfark = "<a href=# onClick=\"showKioskTotalEmptyCashCountClicked(" + items.Mutabakats[i].id + "," + items.Mutabakats[i].emptyKioskId + "," + 0 + ",'" + Convert.ToDateTime(this.startDateField.Text) + "','" + Convert.ToDateTime(this.endDateField.Text) + "');\" class=\"detailCodeButton\" Title=\"Fark Detayını Göster\"></a>";

                    Row[i].CssClass = "inputTitleCell99";
                    Row[i].Cells[8].Text = "TOPLAM";
                    Row[i].Cells[3].Text = "";
                    Row[i].Cells[4].Text = "";
                    Row[i].Cells[0].Text = "";
                    Row[i].Cells[5].Text = "";
                    Row[i].Cells[6].Text = "";
                    Row[i].Cells[7].Text = "";
                    Row[i].Cells[14].Text = totalmerkez + "" + totalfark;
                }
                else
                {
                    if (Convert.ToDecimal(items.Mutabakats[i].amountDifference) < 0)
                    {
                        Row[i].CssClass = "listRowAlternateRed";
                    }
                    else if (Convert.ToDecimal(items.Mutabakats[i].amountDifference) > 0)
                    {
                        Row[i].CssClass = "listRowAlternateYellow";
                    }
                    else
                    {
                        if (i % 2 == 0)
                            Row[i].CssClass = "listrow";
                        else
                            Row[i].CssClass = "listRowAlternate";
                    }
                }
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (items.recordCount < currentRecordEnd)
                currentRecordEnd = items.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + items.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(items.pageCount, this.pageNum, items.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);

            this.itemsTable.Visible = true;

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListMutabakatBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {


        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    public void Sort(Object sender, EventArgs e)
    {
       this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {
            case "KioskName":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //KioskName.Text = "<a>Kiosk Adı   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //KioskName.Text = "<a>Kiosk Adı     <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "Institution":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // MutabakatType.Text = "<a>Mutabakat Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // MutabakatType.Text = "<a>Mutabakat Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // MutabakatType.Text = "<a>Mutabakat Tipi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "InsertionDate":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  MutabakatDate.Text = "<a>Mutabakat Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // MutabakatDate.Text = "<a>Mutabakat Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // MutabakatDate.Text = "<a>Mutabakat Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "OperatorName":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Giriş Yapan Kullanıcı <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OutputAmount.Text = "<a>Giriş Yapan Kullanıcı   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Giriş Yapan Kullanıcı   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "AccountOwner":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  MutabakatUser.Text = "<a>Onaylayan Kullanıcı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  MutabakatUser.Text = "<a>Onaylayan Kullanıcı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // MutabakatUser.Text = "<a>Onaylayan Kullanıcı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "AccountDate":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //InputAmount.Text = "<a>Merkezdeki Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //InputAmount.Text = "<a>Merkezdeki Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // InputAmount.Text = "<a>Merkezdeki Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "MutabakatUser":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;



            case "MutabakatDate":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //OutputAmount.Text = "<a>Fark      <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  OutputAmount.Text = "<a>Fark      <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Fark      <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "InputAmount":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //OutputAmount.Text = "<a>Fark      <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  OutputAmount.Text = "<a>Fark      <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Fark      <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "OutputAmount":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //OutputAmount.Text = "<a>Fark      <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  OutputAmount.Text = "<a>Fark      <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Fark      <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "AmountDifference":
                if (orderSelectionColumn == 11)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //OutputAmount.Text = "<a>Fark      <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  OutputAmount.Text = "<a>Fark      <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Fark      <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 11;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Status":
                if (orderSelectionColumn == 12)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //OutputAmount.Text = "<a>Fark      <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  OutputAmount.Text = "<a>Fark      <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Fark      <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 12;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Explanation":
                if (orderSelectionColumn == 13)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //OutputAmount.Text = "<a>Fark      <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  OutputAmount.Text = "<a>Fark      <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Fark      <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 13;
                    orderSelectionDescAsc = 1;
                }
                break;

            default:

                orderSelectionColumn = 2;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            if ((MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü"))
                kioskIds = "";
            else
            {
                kioskIds = MultipleSelection1.sValue;
                kioskIndex = MultipleSelection1.selectedIndex;
                this.MultipleSelection1.selectedIndex = kioskIndex;
            }

            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListMutabakat items = callWebServ.CallListMutabakatService(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             Convert.ToInt32(ViewState["OrderDesc"]),
                                                                             kioskIds,
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             Convert.ToInt32(this.statusBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             1);
            if (items != null)
            {
                if (items.errorCode == 0)
                {
                    this.BindListTable(items);
                }
                else if (items.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + items.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListMutabakatSrch");
        }
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "";
            else
                kioskIds = MultipleSelection1.sValue;


            int recordCount = 0;
            int pageCount = 0;
            CallWebServices callWebServ = new CallWebServices();
            ParserListMutabakat lists = callWebServ.CallListMutabakatService(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             Convert.ToInt32(ViewState["OrderDesc"]),
                                                                             kioskIds,
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             Convert.ToInt32(this.statusBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             2);

            if (lists != null)
            {
                if (lists.errorCode == 0)
                {

                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    string[] headerNames = { "Kiosk No", "Mutabakat Tipi", "Bildirim Zamanı", "Giriş Yapan Kullanıcı", "Merkezdeki Para ", "Bildirilen Para",
                                               "Kurum Ad", "Bildirilen Para Son", "Bildirilen Para Son 2", "Bildirilen Para Son 3", "Bildirilen Para Son 4", 
                                               "Fark", "Bölge İsmi", "Onaylayan Kullanıcı ", "Bildiren Kullanıcı",
                                               "Kiosk Adı", "Uygulama Versiyonu","No", "Sıfırlama Zamanı" ,
                                               "Operatör", " Bildirim Zamanı", "Durum No ", "Durum", "Referans No" };

                    //exportExcell.ExportExcellByBlock(lists.Kiosks, "Kiosk Raporu", headerNames);
                    exportExcell.ExportExcellByBlock(lists.Mutabakats, "Mutabakat Listesi", headerNames);
                }
                else if (lists.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + lists.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelMutabakat");
        }
    }
}