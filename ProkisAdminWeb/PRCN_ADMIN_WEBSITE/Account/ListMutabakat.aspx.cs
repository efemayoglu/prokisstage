﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ListMutabakat : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 2;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/ListMutabakat.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            this.startDateField.Text = DateTime.Now.ToString("yyyy-MM-dd 00:00:00");
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd 00:00:00");

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;
            this.pageNum = 0;
            this.SearchOrder(2, 1);
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(2, 1);
    }

    private void BindListTable(ParserListMutabakatReport items)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            TableRow[] Row = new TableRow[items.Mutabakats.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("Id", items.Mutabakats[i].ResultCount.ToString());

                TableCell indexCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell insertionDateCell = new TableCell();
                TableCell updatedDateCell = new TableCell();
                TableCell lastCashOutDate = new TableCell();
                TableCell inputAmountCell = new TableCell();
                TableCell outputAmountCell = new TableCell();
                TableCell amountDifferenceCell = new TableCell();
                TableCell cashAmountCell = new TableCell();
                TableCell codeAmountCell = new TableCell();
                TableCell institutionAmountCell = new TableCell();
                TableCell usageAmountCell = new TableCell();
                TableCell calculatedChangeAmountCell = new TableCell();
                TableCell givenChangeAmountCell = new TableCell();
                TableCell activeCodeCell = new TableCell();
                TableCell usedCodeCell = new TableCell();
                TableCell kioskGeneratedActiveCodeCell = new TableCell();
                TableCell kioskGeneratedKioskUsedCodeCell = new TableCell();
                TableCell manuelGeneratedActiveCodeCell = new TableCell();
                TableCell manuelGeneratedUsedCodeCell = new TableCell();
                TableCell anotherKioskGeneratedUsedCodeCell = new TableCell();
                TableCell institutionCell = new TableCell();


                indexCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                insertionDateCell.CssClass = "inputTitleCell4";
                updatedDateCell.CssClass = "inputTitleCell4";
                lastCashOutDate.CssClass = "inputTitleCell4";
                inputAmountCell.CssClass = "inputTitleCell4";
                outputAmountCell.CssClass = "inputTitleCell4";
                amountDifferenceCell.CssClass = "inputTitleCell4";
                cashAmountCell.CssClass = "inputTitleCell4";
                codeAmountCell.CssClass = "inputTitleCell4";
                institutionAmountCell.CssClass = "inputTitleCell4";
                usageAmountCell.CssClass = "inputTitleCell4";
                calculatedChangeAmountCell.CssClass = "inputTitleCell4";
                givenChangeAmountCell.CssClass = "inputTitleCell4";
                activeCodeCell.CssClass = "inputTitleCell4";
                usedCodeCell.CssClass = "inputTitleCell4";
                kioskGeneratedActiveCodeCell.CssClass = "inputTitleCell4";
                kioskGeneratedKioskUsedCodeCell.CssClass = "inputTitleCell4";
                manuelGeneratedActiveCodeCell.CssClass = "inputTitleCell4";
                manuelGeneratedUsedCodeCell.CssClass = "inputTitleCell4";
                anotherKioskGeneratedUsedCodeCell.CssClass = "inputTitleCell4";
                institutionCell.CssClass = "inputTitleCell4";


                indexCell.Text = (startIndex + i).ToString();
                kioskNameCell.Text = items.Mutabakats[i].kioskName;
                insertionDateCell.Text = items.Mutabakats[i].insertionDate;
                updatedDateCell.Text = items.Mutabakats[i].UpdatedDate;
                lastCashOutDate.Text = items.Mutabakats[i].LastCashOutDate;
                inputAmountCell.Text = items.Mutabakats[i].inputAmount.ToString();
                outputAmountCell.Text = items.Mutabakats[i].outputAmount.ToString();
                amountDifferenceCell.Text = items.Mutabakats[i].amountDifference;
                cashAmountCell.Text = items.Mutabakats[i].CashAmount;
                codeAmountCell.Text = items.Mutabakats[i].CodeAmount;
                institutionAmountCell.Text = items.Mutabakats[i].institutionAmount;
                usageAmountCell.Text = items.Mutabakats[i].UsageAmount;
                calculatedChangeAmountCell.Text = items.Mutabakats[i].CalculatedChangeAmount;
                givenChangeAmountCell.Text = items.Mutabakats[i].GivenChangeAmount;
                activeCodeCell.Text = items.Mutabakats[i].activeCode;
                usedCodeCell.Text = items.Mutabakats[i].usedCode;
                kioskGeneratedActiveCodeCell.Text = items.Mutabakats[i].KioskGeneratedActiveCode;
                kioskGeneratedKioskUsedCodeCell.Text = items.Mutabakats[i].KioskGeneratedKioskUsedCode;
                manuelGeneratedActiveCodeCell.Text = items.Mutabakats[i].ManuelGeneratedActiveCode;
                manuelGeneratedUsedCodeCell.Text = items.Mutabakats[i].ManuelGeneratedUsedCode;
                anotherKioskGeneratedUsedCodeCell.Text = items.Mutabakats[i].AnotherKioskGeneratedUsedCode;
                institutionCell.Text = items.Mutabakats[i].Institution;


                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        kioskNameCell,
                        insertionDateCell,
                        updatedDateCell,
                        lastCashOutDate,
                        institutionCell,
                        inputAmountCell,
                        outputAmountCell,
                        amountDifferenceCell,
                        cashAmountCell , 
                        codeAmountCell ,
                        institutionAmountCell,
                        usageAmountCell,
                        calculatedChangeAmountCell,
                        givenChangeAmountCell,
                        activeCodeCell,
                        usedCodeCell,
                        kioskGeneratedActiveCodeCell,
                        kioskGeneratedKioskUsedCodeCell,
                        manuelGeneratedActiveCodeCell,
                        manuelGeneratedUsedCodeCell,
                        anotherKioskGeneratedUsedCodeCell
					});

                if (Convert.ToInt32(items.Mutabakats[i].ResultCount) - 1 == items.recordCount)
                {
                    Row[i].CssClass = "inputTitleCell99";
                    Row[i].Cells[2].Text = "";
                    Row[i].Cells[4].Text = "";
                    Row[i].Cells[5].Text = "TOPLAM";
                    Row[i].Cells[3].Text = "";
                    Row[i].Cells[0].Text = "";
                }
                else
                {
                    if (i % 2 == 0)
                        Row[i].CssClass = "listrow";
                    else
                        Row[i].CssClass = "listRowAlternate";
                }

            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (items.recordCount < currentRecordEnd)
                currentRecordEnd = items.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + items.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(items.pageCount, this.pageNum, items.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListMutabakatBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }



    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "Kiosk":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //MutabakatType.Text = "<a>Mutabakat Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //MutabakatType.Text = "<a>Mutabakat Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //MutabakatType.Text = "<a>Mutabakat Tipi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "InsertionDate":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //MutabakatDate.Text = "<a>Mutabakat Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // MutabakatDate.Text = "<a>Mutabakat Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //MutabakatDate.Text = "<a>Mutabakat Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "MutabakatDate":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // MutabakatUser.Text = "<a>Onaylayan Kullanıcı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //MutabakatUser.Text = "<a>Onaylayan Kullanıcı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //MutabakatUser.Text = "<a>Onaylayan Kullanıcı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "LastCashOutDate":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // InputAmount.Text = "<a>Merkezdeki Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // InputAmount.Text = "<a>Merkezdeki Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //InputAmount.Text = "<a>Merkezdeki Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Institution":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "InputAmount":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "OutputAmount":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;



            case "AmountDifference":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "CashAmount":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "CodeAmount":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "InstitutionAmount":
                if (orderSelectionColumn == 11)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 11;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "UsageAmount":
                if (orderSelectionColumn == 12)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 12;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "CalculatedChangeAmount":
                if (orderSelectionColumn == 13)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 13;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "GivenChangeAmount":
                if (orderSelectionColumn == 14)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 14;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "ActiveCode":
                if (orderSelectionColumn == 15)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 15;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "UsedCode":
                if (orderSelectionColumn == 16)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 16;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "KioskGeneratedActiveCode":
                if (orderSelectionColumn == 17)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 17;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "KioskGeneratedKioskUsedCode":
                if (orderSelectionColumn == 18)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 18;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "ManuelGeneratedActiveCode":
                if (orderSelectionColumn == 19)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 19;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "ManuelGeneratedUsedCode":
                if (orderSelectionColumn == 20)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 20;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "AnotherKioskGeneratedUsedCode":
                if (orderSelectionColumn == 21)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // OutputAmount.Text = "<a>Bildirilen Para    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 21;
                    orderSelectionDescAsc = 1;
                }
                break;

            default:

                orderSelectionColumn = 15;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }



    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListMutabakatReport items = callWebServ.CallListMutabakatServiceForReport(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             1);
            if (items != null)
            {
                if (items.errorCode == 0)
                {
                    this.BindListTable(items);
                }
                else if (items.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + items.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListMutabakatSrch");
        }
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListMutabakatReport lists = callWebServ.CallListMutabakatServiceForReport(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             Convert.ToInt32(ViewState["OrderDesc"]),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             2);

            if (lists != null)
            {
                if (lists.errorCode == 0)
                {

                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    string[] headerNames = { "Kiosk No", "Mutabakat Tipi", "Mutabakat Zamanı", "Onaylayan Kullanıcı", "Bildirilen Para", "Merkezdeki Para",
                                               "Ekleme Tarihi", "Nakit Girişi", "Kuruma Giden Tutar", "Aktif Olan Miktar", "Şehir İsmi", 
                                               "Bölge No", "Bölge İsmi", "Kiosk Tip No", "Bildirimi Yapan Kullanıcı ",
                                               "Kiosk Adı", "No","Son Çalışma Zamanı", "Kapasite" ,
                                               "Açıklama", "Para Kutu Tipi","Para Kutu Tipi No"};




                    exportExcell.ExportExcellByBlock(lists.Mutabakats, "Mutabakat Listesi", headerNames);
                }
                else if (lists.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + lists.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelMutabakat");
        }
    }
}