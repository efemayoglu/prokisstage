﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PRCNCORE.Parser.Other;

public partial class Account_ListCodeDetails : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Kiosk/ListKioskReports.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            this.startDateField.Text = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00";

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {
                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }

            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {

                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();

            }

            ListItem item1 = new ListItem();
            item1.Text = "Kurum Seçiniz";
            item1.Value = "0";
            this.instutionBox.Items.Add(item1);
            this.instutionBox.SelectedValue = "0";

            ParserListInstutionOperationName operationType = callWebServ.CallGetOperationTypeNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (operationType != null)
            {

                this.operationTypeBox.DataSource = operationType.ParserListInstutionOperationNames;
                this.operationTypeBox.DataBind();

            }

            ListItem item2 = new ListItem();
            item2.Text = "İşlem Tipi Seçiniz";
            item2.Value = "0";
            this.operationTypeBox.Items.Add(item2);
            this.operationTypeBox.SelectedValue = "0";

            //paymentBox
            ParserListPaymentType paymentType = callWebServ.CallGetPaymentTypeService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (paymentType != null)
            {

                this.paymentTypeBox.DataSource = paymentType.PaymentType;
                this.paymentTypeBox.DataBind();

            }

            ListItem item5 = new ListItem();
            item5.Text = "Ödeme Türünü Seçiniz";
            item5.Value = "0";
            this.paymentTypeBox.Items.Add(item5);
            this.paymentTypeBox.SelectedValue = "0";

            
            ParserListBankName banks = callWebServ.CallGetBankNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (banks != null)
            {

                this.bankTypeBox.DataSource = banks.BankNames;
                this.bankTypeBox.DataBind();

            }

            ListItem itembank = new ListItem();
            itembank.Text = "Banka Seçiniz";
            itembank.Value = "0";
            this.bankTypeBox.Items.Add(itembank);
            this.bankTypeBox.SelectedValue = "0";
            

            ViewState["OrderColumn"] = 5;
            ViewState["OrderDesc"] = 1;

            this.pageNum = 0;
            this.SearchOrder(5, 1);
        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }

    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(5, 1);

    }
    private string ChangeMoneyFormat(string value)
    {
        value = value.Replace(',', '.');
        return String.Format(new System.Globalization.CultureInfo("tr-TR"), "{0:C}", Convert.ToDouble(value));
    }

    private void BindListTable(ParserListCodeReport list, int recordcount, int pagecount)
    {

        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
             

            TableRow[] Row = new TableRow[list.KioskReports.Count];
            
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();

                TableCell indexCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell GeneratedTotalCell = new TableCell();
                TableCell GeneratedTodayUsableTodayCell = new TableCell();
                TableCell UsedTotalTodayCell = new TableCell();
                TableCell CreditCardCommisionAmountCell = new TableCell();
                TableCell MobilePaymentCommisinCell = new TableCell();
                TableCell ManuellyGeneratedCodeCell = new TableCell();
                TableCell RemainUsableCodeCell = new TableCell();
                TableCell MutabakatCell = new TableCell();
                TableCell CashAmountCell = new TableCell();
                TableCell CreditCardAmountCell = new TableCell();
                TableCell MobilPaymentCell = new TableCell();
                TableCell SuccessTxnCountCell = new TableCell();
                TableCell InstitutionAmountCell = new TableCell();
                TableCell ChangeAmountCell = new TableCell();
                TableCell UsageFeeCell = new TableCell();
                TableCell AskidaCodeCell = new TableCell();
                TableCell ErasedCodeCell = new TableCell();
                TableCell ReverseCountCell = new TableCell();
                TableCell ReverseTotalCell = new TableCell();
                TableCell NonReverseCountCell = new TableCell();
                TableCell NonReverseTotalCell = new TableCell();
                TableCell RecCountCell = new TableCell();
                TableCell RecTotalCell = new TableCell();

                TableCell fakeCell1 = new TableCell();
                TableCell fakeCell2 = new TableCell();
                TableCell fakeCell3 = new TableCell();
                TableCell fakeCell4 = new TableCell();
                TableCell fakeCell5 = new TableCell();
                TableCell fakeCell6 = new TableCell();
                TableCell fakeCell7 = new TableCell();
                TableCell fakeCell8 = new TableCell();
                TableCell fakeCell9 = new TableCell();
                TableCell fakeCell10 = new TableCell();
                TableCell fakeCell11 = new TableCell();
                TableCell fakeCell12 = new TableCell();

                fakeCell1.CssClass = "inputTitleCell4";
                fakeCell2.CssClass = "inputTitleCell4";
                fakeCell3.CssClass = "inputTitleCell4";
                fakeCell4.CssClass = "inputTitleCell4";
                fakeCell5.CssClass = "inputTitleCell4";
                fakeCell6.CssClass = "inputTitleCell4";
                fakeCell7.CssClass = "inputTitleCell4";
                fakeCell8.CssClass = "inputTitleCell4";
                fakeCell9.CssClass = "inputTitleCell4";
                fakeCell10.CssClass = "inputTitleCell4";
                fakeCell11.CssClass = "inputTitleCell4";
                fakeCell12.CssClass = "inputTitleCell4";

                fakeCell1.Visible = false;
                fakeCell2.Visible = false;
                fakeCell3.Visible = false;
                fakeCell4.Visible = false;
                fakeCell5.Visible = false;
                fakeCell6.Visible = false;
                fakeCell7.Visible = false;
                fakeCell8.Visible = false;
                fakeCell9.Visible = false;
                fakeCell10.Visible = false;
                fakeCell11.Visible = false;
                fakeCell12.Visible = false;

                indexCell.CssClass = "inputTitleCell4";
                //kioskIdCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                GeneratedTotalCell.CssClass = "inputTitleCell4";
                GeneratedTodayUsableTodayCell.CssClass = "inputTitleCell4";
                UsedTotalTodayCell.CssClass = "inputTitleCell4";
                CreditCardCommisionAmountCell.CssClass = "inputTitleCell4";
                MobilePaymentCommisinCell.CssClass = "inputTitleCell4";
                ManuellyGeneratedCodeCell.CssClass = "inputTitleCell4";
                RemainUsableCodeCell.CssClass = "inputTitleCell4";
                MutabakatCell.CssClass = "inputTitleCell4";
                CashAmountCell.CssClass = "inputTitleCell4";
                SuccessTxnCountCell.CssClass = "inputTitleCell4";
                InstitutionAmountCell.CssClass = "inputTitleCell4";
                ChangeAmountCell.CssClass = "inputTitleCell4";
                UsageFeeCell.CssClass = "inputTitleCell4";
                CreditCardAmountCell.CssClass = "inputTitleCell4";
                MobilPaymentCell.CssClass = "inputTitleCell4";
                AskidaCodeCell.CssClass = "inputTitleCell4";
                ErasedCodeCell.CssClass = "inputTitleCell4";

                ReverseCountCell.CssClass = "inputTitleCell4";
                ReverseTotalCell.CssClass = "inputTitleCell4";
                NonReverseCountCell.CssClass = "inputTitleCell4";
                NonReverseTotalCell.CssClass = "inputTitleCell4";
                RecCountCell.CssClass = "inputTitleCell4";
                RecTotalCell.CssClass = "inputTitleCell4";



                indexCell.Text = (startIndex + i).ToString();
                //kioskIdCell.Text = list.KioskReports[i].KioskId;
                kioskNameCell.Text = list.KioskReports[i].KioskName;
                GeneratedTotalCell.Text = ChangeMoneyFormat(list.KioskReports[i].GeneratedTotal);
                GeneratedTodayUsableTodayCell.Text = ChangeMoneyFormat(list.KioskReports[i].GeneratedTodayUsableToday);
                UsedTotalTodayCell.Text = ChangeMoneyFormat(list.KioskReports[i].UsedTotalToday);
                CreditCardCommisionAmountCell.Text = ChangeMoneyFormat(list.KioskReports[i].CreditCardCommisionAmount);
                CreditCardAmountCell.Text = ChangeMoneyFormat(list.KioskReports[i].CreditCardAmount);
                MobilePaymentCommisinCell.Text = ChangeMoneyFormat(list.KioskReports[i].MobilePaymentCommisionAmount);
                MobilPaymentCell.Text = ChangeMoneyFormat(list.KioskReports[i].MobilePaymentAmount);
                ManuellyGeneratedCodeCell.Text = list.KioskReports[i].ManeullyGeneratedCodeAmount;
                RemainUsableCodeCell.Text = ChangeMoneyFormat(list.KioskReports[i].RemainUsableCode);
                AskidaCodeCell.Text = list.KioskReports[i].AskidaCode;
                ErasedCodeCell.Text = list.KioskReports[i].ErasedCode;

                CashAmountCell.Text = ChangeMoneyFormat(list.KioskReports[i].CashAmount);
                SuccessTxnCountCell.Text = list.KioskReports[i].SuccessTxnCount;
                InstitutionAmountCell.Text = ChangeMoneyFormat(list.KioskReports[i].InstitutionAmount);
                UsageFeeCell.Text = ChangeMoneyFormat(list.KioskReports[i].UsageFee);
                ChangeAmountCell.Text = ChangeMoneyFormat(list.KioskReports[i].ParaUstu);

                ReverseCountCell.Text = list.KioskReports[i].ReverseRecCount;
                ReverseTotalCell.Text = ChangeMoneyFormat(list.KioskReports[i].ReverseRecTotal);
                NonReverseCountCell.Text = list.KioskReports[i].NonReverseRecCount;
                NonReverseTotalCell.Text = ChangeMoneyFormat(list.KioskReports[i].NonReverseRecTotal);
                RecCountCell.Text = list.KioskReports[i].RecCount;
                RecTotalCell.Text = ChangeMoneyFormat(list.KioskReports[i].RecTotal);

                double cashamount = Convert.ToDouble(list.KioskReports[i].CashAmount);
                double instamount = Convert.ToDouble(list.KioskReports[i].InstitutionAmount);
                double changeAmount = Convert.ToDouble(list.KioskReports[i].ParaUstu);
                double usageFee = Convert.ToDouble(list.KioskReports[i].UsageFee);
                double generatedtotal = Convert.ToDouble(list.KioskReports[i].GeneratedTotal);
                double usedtotal = Convert.ToDouble(list.KioskReports[i].UsedTotalToday);

                //MutabakatCell.Text = ChangeMoneyFormat((cashamount - (generatedtotal - usedtotal) - usageFee - instamount - changeAmount).ToString());
                MutabakatCell.Text = ChangeMoneyFormat(list.KioskReports[i].Mutabakat.ToString());
                

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        fakeCell1,
                        fakeCell2,
                        fakeCell3,
                        fakeCell4,
                        fakeCell5,
                        fakeCell6,
                        fakeCell7,
                        fakeCell8,
                        fakeCell9,
                        //fakeCell10,				
                        //kioskIdCell,
                        kioskNameCell,
                        CashAmountCell,
                        CreditCardAmountCell,                      
                        CreditCardCommisionAmountCell, 
                        MobilPaymentCell,                    
                        MobilePaymentCommisinCell, 
                        SuccessTxnCountCell,
                        InstitutionAmountCell,
                        ReverseCountCell,
                        ReverseTotalCell,
                        NonReverseCountCell,
                        NonReverseTotalCell,
                        RecCountCell,
                        RecTotalCell,
                        ManuellyGeneratedCodeCell, 
                        GeneratedTotalCell, //Günlük üretilen aski kod                       
                        UsedTotalTodayCell, //(Gün içinde) Toplam Kullanılan Aski Kod
                        GeneratedTodayUsableTodayCell, // GÜN İÇERİSİNDE ALINAN KULLANILMAYAN AKTİF ASKİ KOD
                        //RemainUsableCodeCell, //ÖNCEKİ GÜNLERDEN KALAN AKTİF ASKİ KOD
                        AskidaCodeCell,
                        ErasedCodeCell,
                        ChangeAmountCell,
                        UsageFeeCell,
                        MutabakatCell
                        
                        //transactionAmount,
                        //kioskEmptyAmountCell,
                        //institutionAmount,
                        //earnedMoneyCell 
                });

                if(Convert.ToInt32(list.KioskReports[i].ResultCount)==list.recordCount)
                {
                    Row[i].CssClass = "inputTitleCell99";
                    Row[i].Cells[10].Text="TOPLAM";
                }
                else
                {
                    if (i % 2 == 0)
                        Row[i].CssClass = "listrow";
                    else
                        Row[i].CssClass = "listRowAlternate";
                }
            }
            
            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (list.recordCount < currentRecordEnd)
                currentRecordEnd = list.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + list.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(pagecount, this.pageNum, recordcount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);

            this.itemsTable.Visible = true;

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskBDT");
        }
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);
            int recordCount = 0;
            int pageCount = 0;

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            CallWebServices callWebServ = new CallWebServices();
            ParserListCodeReport lists = callWebServ.CallListCodeDetails(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             kioskIds,
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             Convert.ToInt32(this.paymentTypeBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id, 2,0,
                                                                             Convert.ToInt32(this.bankTypeBox.SelectedValue));

            if (lists != null)
            {
                if (lists.errorCode == 0)
                {
                    for (int i = 0; i < lists.KioskReports.Count; i++)
                    {
                        lists.KioskReports[i].GeneratedTodayUsableToday = lists.KioskReports[i].GeneratedTodayUsableToday.Replace('.', ',');
                        lists.KioskReports[i].GeneratedTodayUsedToday = lists.KioskReports[i].GeneratedTodayUsedToday.Replace('.', ',');
                        //lists.KioskReports[i].KioskEmptyAmount = lists.KioskReports[i].TotalAmount.Replace('.', ',');
                        lists.KioskReports[i].GeneratedTotal = lists.KioskReports[i].GeneratedTotal.Replace('.', ',');
                        lists.KioskReports[i].GeneratedYesterdayUsedToday = lists.KioskReports[i].GeneratedYesterdayUsedToday.Replace('.', ',');
                        lists.KioskReports[i].GeneratedYesterdayUsedTodayCount = lists.KioskReports[i].GeneratedYesterdayUsedTodayCount.Replace('.', ',');
                        lists.KioskReports[i].RemainUsableCode = lists.KioskReports[i].RemainUsableCode.Replace('.', ',');
                        lists.KioskReports[i].UsedTotalToday = lists.KioskReports[i].UsedTotalToday.Replace('.', ',');
                        lists.KioskReports[i].UsedTotalToday = lists.KioskReports[i].UsageFee.Replace('.', ',');
                        lists.KioskReports[i].UsedTotalToday = lists.KioskReports[i].InstitutionAmount.Replace('.', ',');
                    }

                  


                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    //exportExcell.ExportExcell(lists.KioskReports, lists.recordCount, "Kiosk İşlemleri Listesi");

                    string[] headerNames = {  "Kiosk Adı", "Üretilen Kod", "Kullanılabilir Kod " ,"Başarılı Dolum Tutarı", "Üretilen Kod Tutarı","Üretilen Kod Sayısı",
                                                "Kullanılan Kod Sayısı",  "Toplanan Tutar", "No" ,"Nakit Para Girişi", "Başarılı İşlem Sayısı" ,"Başarılı Dolum Tutarı 2",
                                                "Toplam Kullanım Ücreti ", "Para Üstü", "Mutabakat" ,"Kredi Kartı","Mobil Ödeme", "KK Komisyon", "Silinen ", "Manüel Üretilen Kod", "Askıda Kalan Kod", "Silinen Kod"};

                     exportExcell.ExportExcellByBlock(lists.KioskReports, "Kiosk Kod Detayları Listesi", headerNames);
                }
                else if (lists.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + lists.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelAskiCode");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    protected void deleteButton_Click(object sender, EventArgs e)
    {
        this.SearchOrder(0, 1);
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "KioskName":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CodeGeneratedToday":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //CodeGeneratedToday.Text = "<a>İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //CodeGeneratedToday.Text = "<a>İşlem Sayısı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //CodeGeneratedToday.Text = "<a>İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CodeUsedToday":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //CodeUsedToday.Text = "<a>Üretilen Kod Tutarı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //CodeUsedToday.Text = "<a>Üretilen Kod Tutarı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //CodeUsedToday.Text = "<a>Üretilen Kod Tutarı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CodeUnusedToday":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //CodeUnusedToday.Text = "<a>Kullanılan Kod Tutarı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //CodeUnusedToday.Text = "<a>Kullanılan Kod Tutarı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //CodeUnusedToday.Text = "<a>Kullanılan Kod Tutarı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CodeUsedYesterday":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //CodeUsedYesterday.Text = "<a>Üretilen Kod Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //CodeUsedYesterday.Text = "<a>Üretilen Kod Sayısı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //CodeUsedYesterday.Text = "<a>Üretilen Kod Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CodeNumberUsedYesterday":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //CodeNumberUsedYesterday.Text = "<a>Kullanılan Kod Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //CodeNumberUsedYesterday.Text = "<a>Kullanılan Kod Sayısı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //CodeNumberUsedYesterday.Text = "<a>Kullanılan Kod Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CodeUnusedTotal":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //CodeUnusedTotal.Text = "<a>Toplanan Tutar    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //CodeUnusedTotal.Text = "<a>Toplanan Tutar    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //CodeUnusedTotal.Text = "<a>Toplanan Tutar    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;
            //case "KioskEmptyAmount":
            //    if (orderSelectionColumn == 9)
            //    {
            //        if (orderSelectionDescAsc == 0)
            //        {
            //            KioskEmptyAmount.Text = "<a>Kiosktan Alınan Tutar    <img src=../images/arrow_up.png border=0/></a>";
            //            orderSelectionDescAsc = 1;
            //        }
            //        else
            //        {
            //            KioskEmptyAmount.Text = "<a>Kiosktan Alınan Tutar    <img src=../images/arrow_down.png border=0/></a>";
            //            orderSelectionDescAsc = 0;
            //        }
            //    }
            //    else
            //    {
            //        KioskEmptyAmount.Text = "<a>Kiosktan Alınan Tutar    <img src=../images/arrow_up.png border=0/></a>";
            //        orderSelectionColumn = 9;
            //        orderSelectionDescAsc = 1;
            //    }
            //    break;
                /*
            case "CodeReGenerated":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        CodeReGenerated.Text = "<a>Kazanılan Tutar    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        CodeReGenerated.Text = "<a>Kazanılan Tutar    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    CodeReGenerated.Text = "<a>Kazanılan Tutar    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;
*/
            case "SuccessTxnCount":
                if (orderSelectionColumn == 12)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 12;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "AskidaCode":
                if (orderSelectionColumn == 13)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 13;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "ErasedCode":
                if (orderSelectionColumn == 14)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 14;
                    orderSelectionDescAsc = 1;
                }
                break;


            default:
                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            CallWebServices callWebServ = new CallWebServices();
            ParserListCodeReport kioskReport = callWebServ.CallListCodeDetails(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             kioskIds,
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             Convert.ToInt32(this.paymentTypeBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id, 
                                                                             1,
                                                                             Convert.ToInt32(this.operationTypeBox.SelectedValue),
                                                                             Convert.ToInt32(this.bankTypeBox.SelectedValue));
            if (kioskReport != null)
            {
                if (kioskReport.errorCode == 0)
                {
                    this.BindListTable(kioskReport, kioskReport.recordCount, kioskReport.pageCount);
                }
                else if (kioskReport.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + kioskReport.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "KioskReportSrch");
        }
    }

}