﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ListAccountDetails : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int accountId;
    private int instutionId;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/ListAccount.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }

        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        this.accountId = Convert.ToInt32(Request.QueryString["accountId"]);
        this.instutionId = Convert.ToInt32(Request.QueryString["instutionId"]);

        this.numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            this.BuildControls();
        }
    }

    private void BuildControls()
    {
        ListDetails();
    }

    public void ListDetails()
    {
        try
        {
            CallWebServices callWebService = new CallWebServices();
            ParserListAccountDetails listAccountDetails = callWebService.CallGetAccountDetailService(this.accountId,
                                                                                                     this.instutionId,
                                                                                                     this.numberOfItemsPerPage,
                                                                                                     this.pageNum, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (listAccountDetails != null)
            {
                if (listAccountDetails.errorCode == 0)
                {
                    this.BindListTable(listAccountDetails);
                }
                else if (listAccountDetails.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + listAccountDetails.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskMoneyDetailSrch");
        }
    }

    private void BindListTable(ParserListAccountDetails listAccountDetails)
    {
        try
        {
            TableRow[] Row = new TableRow[listAccountDetails.AccountDetail.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();

                TableCell indexCell = new TableCell();
                TableCell transactionIdCell = new TableCell();
                TableCell denizbankCell = new TableCell();
                TableCell instutionMoneyCell = new TableCell();
                TableCell merkezCell = new TableCell();
                TableCell moneyCodeCell = new TableCell();
                TableCell recievedAmountCell = new TableCell();
                TableCell operationTypeCell = new TableCell();
                TableCell transactionDateCell = new TableCell();
                TableCell instutionNameCell = new TableCell();
 
                instutionNameCell.CssClass = "inputTitleCell4";
                indexCell.CssClass = "inputTitleCell4";
                transactionIdCell.CssClass = "inputTitleCell4";
                denizbankCell.CssClass = "inputTitleCell4";
                instutionMoneyCell.CssClass = "inputTitleCell4";
                merkezCell.CssClass = "inputTitleCell4";
                moneyCodeCell.CssClass = "inputTitleCell4";
                recievedAmountCell.CssClass = "inputTitleCell4";
                operationTypeCell.CssClass = "inputTitleCell4";
                transactionDateCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                transactionIdCell.Text = listAccountDetails.AccountDetail[i].transactionId.ToString();
                denizbankCell.Text = listAccountDetails.AccountDetail[i].denizbank + " TL";
                instutionMoneyCell.Text = listAccountDetails.AccountDetail[i].instutionMoney + " TL";
                merkezCell.Text = listAccountDetails.AccountDetail[i].merkez + " TL";
                moneyCodeCell.Text = listAccountDetails.AccountDetail[i].moneyCode + " TL";
                recievedAmountCell.Text = listAccountDetails.AccountDetail[i].receivedAmount;
                operationTypeCell.Text = listAccountDetails.AccountDetail[i].operationTypeName;
                transactionDateCell.Text = listAccountDetails.AccountDetail[i].transactionDate;
                instutionNameCell.Text = listAccountDetails.AccountDetail[i].instutionName;

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        transactionIdCell,
                        instutionNameCell,
                        instutionMoneyCell,
                        denizbankCell,
                        merkezCell,
                        moneyCodeCell,
                        recievedAmountCell,
                        operationTypeCell,
                        transactionDateCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(listAccountDetails.pageCount, this.pageNum, listAccountDetails.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListAccountDetailBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.ListDetails();
    }
}