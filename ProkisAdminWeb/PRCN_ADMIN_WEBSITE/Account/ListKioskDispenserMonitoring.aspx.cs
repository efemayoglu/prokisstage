﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Account;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ListKioskDispenserMonitoring : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/ListKioskDispenserMonitoring.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }


       
        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.startDateField.Text = DateTime.Now.ToString("yyyy-MM-dd 00:00:00");
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd 00:00:00");

            CallWebServices callWebServ = new CallWebServices();
            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {

                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();

            }

            ListItem item1 = new ListItem();
            item1.Text = "Kurum Seçiniz";
            item1.Value = "0";
            this.instutionBox.Items.Add(item1);
            this.instutionBox.SelectedValue = "0";
  

            ViewState["OrderColumn"] = 4;
            ViewState["OrderDesc"] = 1;

            this.pageNum = 0;
            this.SearchOrder(3, 1);
        }


    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskDispenserMonitoring dispenserMonitoring = callWebServ.CallListKioskDispenserMonitoringService(this.searchTextField.Text,
                                                                                                           Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                                                           Convert.ToDateTime(this.startDateField.Text),
                                                                                                           Convert.ToDateTime(this.endDateField.Text),
                                                                                                           Convert.ToInt32(this.numberOfItemField.Text),
                                                                                                           this.pageNum,
                                                                                                           recordCount,
                                                                                                           pageCount,
                                                                                                           this.orderSelectionColumn,
                                                                                                           this.orderSelectionDescAsc,
                                                                                                           this.LoginDatas.AccessToken,
                                                                                                           this.LoginDatas.User.Id
                                                                                                           , 1);
            if (dispenserMonitoring != null)
            {
                if (dispenserMonitoring.errorCode == 0)
                {
                    this.BindListTable(dispenserMonitoring/*, dispenserMonitoring.recordCount, dispenserMonitoring.pageCount*/);
                }
                else if (dispenserMonitoring.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + dispenserMonitoring.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskDispenserMonitoringSrch");
        }
    }

    //BindListTable
    private void BindListTable(ParserListKioskDispenserMonitoring dispenserMonitoring)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            TableRow[] Row = new TableRow[dispenserMonitoring.KioskDispenserMonitoring.Count];

            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;

            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                //Row[i].Attributes.Add("commandId", dispenserMonitoring.KioskDispenserMonitoring[i].CommandId.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                
                TableCell createdUserCell = new TableCell();
                TableCell insertionDateCell = new TableCell();
                TableCell insertionAmountCell = new TableCell();
                TableCell kioskNameCell = new TableCell();

                TableCell value1Cell = new TableCell();
                TableCell amount1Cell = new TableCell();
                TableCell value2Cell = new TableCell();
                TableCell amount2Cell = new TableCell();
                TableCell value3Cell = new TableCell();
                TableCell amount3Cell = new TableCell();
                TableCell value4Cell = new TableCell();
                TableCell amount4Cell = new TableCell();
                TableCell value5Cell = new TableCell();
                TableCell amount5Cell = new TableCell();
                TableCell value6Cell = new TableCell();
                TableCell amount6Cell = new TableCell();
                TableCell value7Cell = new TableCell();
                TableCell amount7Cell = new TableCell();
                TableCell value8Cell = new TableCell();
                TableCell amount8Cell = new TableCell();
                //TableCell value9Cell = new TableCell();
                //TableCell amount9Cell = new TableCell();
                //TableCell value10Cell = new TableCell();
                //TableCell amount10Cell = new TableCell();
                //TableCell value11Cell = new TableCell();
                //TableCell amount11Cell = new TableCell();

                TableCell value1OldCell = new TableCell();
                TableCell amount1OldCell = new TableCell();
                TableCell value2OldCell = new TableCell();
                TableCell amount2OldCell = new TableCell();
                TableCell value3OldCell = new TableCell();
                TableCell amount3OldCell = new TableCell();
                TableCell value4OldCell = new TableCell();
                TableCell amount4OldCell = new TableCell();
                TableCell value5OldCell = new TableCell();
                TableCell amount5OldCell = new TableCell();
                TableCell value6OldCell = new TableCell();
                TableCell amount6OldCell = new TableCell();
                TableCell value7OldCell = new TableCell();
                TableCell amount7OldCell = new TableCell();
                TableCell value8OldCell = new TableCell();
                TableCell amount8OldCell = new TableCell();
                //TableCell value9OldCell = new TableCell();
                //TableCell amount9OldCell = new TableCell();
                //TableCell value10OldCell = new TableCell();
                //TableCell amount10OldCell = new TableCell();


                TableCell casette1RejectCell = new TableCell();
                TableCell casette1RejectOldCell = new TableCell();
                TableCell casette2RejectCell = new TableCell();
                TableCell casette2RejectOldCell = new TableCell();
                TableCell casette3RejectCell = new TableCell();
                TableCell casette3RejectOldCell = new TableCell();
                TableCell casette4RejectCell = new TableCell();
                TableCell casette4RejectOldCell = new TableCell();
             

                indexCell.CssClass = "inputTitleCell4";
                idCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                createdUserCell.CssClass = "inputTitleCell4";
                insertionDateCell.CssClass = "inputTitleCell4";
                insertionAmountCell.CssClass = "inputTitleCell4";

                value1Cell.CssClass = "inputTitleCell4";
                amount1Cell.CssClass = "inputTitleCell4";
                value2Cell.CssClass = "inputTitleCell4";
                amount2Cell.CssClass = "inputTitleCell4";
                value3Cell.CssClass = "inputTitleCell4";
                amount3Cell.CssClass = "inputTitleCell4";
                value4Cell.CssClass = "inputTitleCell4";
                amount4Cell.CssClass = "inputTitleCell4";
                value5Cell.CssClass = "inputTitleCell4";
                amount5Cell.CssClass = "inputTitleCell4";
                value6Cell.CssClass = "inputTitleCell4";
                amount6Cell.CssClass = "inputTitleCell4";
                value7Cell.CssClass = "inputTitleCell4";
                amount7Cell.CssClass = "inputTitleCell4";
                value8Cell.CssClass = "inputTitleCell4";
                amount8Cell.CssClass = "inputTitleCell4";
                //value9Cell.CssClass = "inputTitleCell4";
                //amount9Cell.CssClass = "inputTitleCell4";
                //value10Cell.CssClass = "inputTitleCell4";
                //amount10Cell.CssClass = "inputTitleCell4";
                //value11Cell.CssClass = "inputTitleCell4";
                //amount11Cell.CssClass = "inputTitleCell4";

                value1OldCell.CssClass = "inputTitleCell4";
                amount1OldCell.CssClass = "inputTitleCell4";
                value2OldCell.CssClass = "inputTitleCell4";
                amount2OldCell.CssClass = "inputTitleCell4";
                value3OldCell.CssClass = "inputTitleCell4";
                amount3OldCell.CssClass = "inputTitleCell4";
                value4OldCell.CssClass = "inputTitleCell4";
                amount4OldCell.CssClass = "inputTitleCell4";
                value5OldCell.CssClass = "inputTitleCell4";
                amount5OldCell.CssClass = "inputTitleCell4";
                value6OldCell.CssClass = "inputTitleCell4";
                amount6OldCell.CssClass = "inputTitleCell4";
                value7OldCell.CssClass = "inputTitleCell4";
                amount7OldCell.CssClass = "inputTitleCell4";
                value8OldCell.CssClass = "inputTitleCell4";
                amount8OldCell.CssClass = "inputTitleCell4";
                //value9OldCell.CssClass = "inputTitleCell4";
                //amount9OldCell.CssClass = "inputTitleCell4";
                //value10OldCell.CssClass = "inputTitleCell4";
                //amount10OldCell.CssClass = "inputTitleCell4";
                //value11OldCell.CssClass = "inputTitleCell4";
                //amount11OldCell.CssClass = "inputTitleCell4";

                casette1RejectCell.CssClass  = "inputTitleCell4";
                casette1RejectOldCell.CssClass = "inputTitleCell4";
                casette2RejectCell.CssClass = "inputTitleCell4";
                casette2RejectOldCell.CssClass = "inputTitleCell4";
                casette3RejectCell.CssClass = "inputTitleCell4";
                casette3RejectOldCell.CssClass = "inputTitleCell4";
                casette4RejectCell.CssClass = "inputTitleCell4";
                casette4RejectOldCell.CssClass = "inputTitleCell4";

                kioskNameCell.HorizontalAlign = HorizontalAlign.Left;
              
                indexCell.Text = (startIndex + i).ToString();
                idCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].Id.ToString();
                kioskNameCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].KioskName;
                createdUserCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].Createruser;
                insertionAmountCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].InsertionAmount;
                insertionDateCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].InsertionDate;

                
                amount1Cell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].firstDenomNumber;
                amount2Cell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].secondDenomNumber;
                amount3Cell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].thirdDenomNumber;
                amount4Cell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].fourthDenomNumber;
                amount5Cell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].fifthDenomNumber;
                amount6Cell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].sixthDenomNumber;
                amount7Cell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].seventhDenomNumber;
                amount8Cell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].eighthDenomNumber;
                //amount9Cell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].ninthDenomNumber;
                //amount10Cell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].tenthDenomNumber;
                //amount11Cell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].eleventhDenomNumber;

                amount1OldCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].firstDenomNumberOld;
                amount2OldCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].secondDenomNumberOld;
                amount3OldCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].thirdDenomNumberOld;
                amount4OldCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].fourthDenomNumberOld;
                amount5OldCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].fifthDenomNumberOld;
                amount6OldCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].sixthDenomNumberOld;
                amount7OldCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].seventhDenomNumberOld;
                amount8OldCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].eighthDenomNumberOld;
                //amount9OldCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].ninthDenomNumberOld;
                //amount10OldCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].tenthDenomNumberOld;
                //amount11OldCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].eleventhDenomNumberOld;

                casette1RejectCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].casette1Reject;
                casette1RejectOldCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].casette1RejectOld;
                casette2RejectCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].casette2Reject;
                casette2RejectOldCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].casette2RejectOld;
                casette3RejectCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].casette3Reject;
                casette3RejectOldCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].casette3RejectOld;
                casette4RejectCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].casette4Reject;
                casette4RejectOldCell.Text = dispenserMonitoring.KioskDispenserMonitoring[i].casette4RejectOld;

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
						 kioskNameCell,
                         createdUserCell,
                         insertionDateCell,
                         
                         amount1Cell,
                         amount1OldCell,
                         casette1RejectCell,
                         casette1RejectOldCell,
                         amount2Cell, 
                         amount2OldCell,
                         casette2RejectCell,
                         casette2RejectOldCell,
                         amount3Cell,
                         amount3OldCell,
                         casette3RejectCell,
                         casette3RejectOldCell,
                         amount4Cell,
                         amount4OldCell,
                         casette4RejectCell,
                         casette4RejectOldCell,
                         amount5Cell,
                         amount5OldCell,
                         amount6Cell,
                         amount6OldCell,
                         amount7Cell,
                         amount7OldCell,
                         amount8Cell,
                         amount8OldCell
                         //amount9Cell,
                         //amount9OldCell,
                         //amount10Cell,
                         //amount10OldCell,
                         //amount11Cell,
                         //amount11OldCell
                         
                         
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);

            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (dispenserMonitoring.recordCount < currentRecordEnd)
                currentRecordEnd = dispenserMonitoring.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + dispenserMonitoring.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(dispenserMonitoring.pageCount, this.pageNum, dispenserMonitoring.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);

            this.itemsTable.Visible = true;

            
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskCommandBDT");
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(3, 1);
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }



    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskDispenserMonitoring dispenserMonitoring = callWebServ.CallListKioskDispenserMonitoringService(this.searchTextField.Text,
                                                                                                           Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                                                           Convert.ToDateTime(this.startDateField.Text),
                                                                                                           Convert.ToDateTime(this.endDateField.Text),
                                                                                                           Convert.ToInt32(this.numberOfItemField.Text),
                                                                                                           this.pageNum,
                                                                                                           recordCount,
                                                                                                           pageCount,
                                                                                                           this.orderSelectionColumn,
                                                                                                           this.orderSelectionDescAsc,
                                                                                                           this.LoginDatas.AccessToken,
                                                                                                           this.LoginDatas.User.Id
                                                                                                           , 2);

            if (dispenserMonitoring != null)
            {
                if (dispenserMonitoring.errorCode == 0)
                {

                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    string[] headerNames = { "No", "Giriş Yapan Kullanıcı", "Giriş Yapılan Tarih", "#", "Kiosk Adı", "#**",
                                             "#1", "İkinci No", "#2", "Üçüncü No", "#3", "Dördüncü No", "#4", "Beşinci No"
                                           , "#5", "Altıncı No",  "#6", "Yedinci No",  "#7", "Sekizinci No", "#8", "Dokuzuncu No",
                                             "#9", "Onuncu No", "#10", "Onbirinci No", 
                                           "Kiosk Adı1", "Giriş Yapan Kullanıcı1", "#E_1", "#11", "#E_2", "#21",
                                             "#E_3", "İkinci1 No", "#E_4", "Üçü1ncü No", "#E_5", "Dördüncü N1o", "#E_6", "Beşinci No1"
                                           , "#E_7", "Al1tıncı No",  "#E_8", "Yed1inci No",  "#E_9", "Seki1zinci No", "#E_10", "Do1kuzuncu No",
                                             "#E_11","R_1","R_2","R_3","R_4","E_R_1","E_R_2","E_R_3","E_R_4" };


                    exportExcell.ExportExcellByBlock(dispenserMonitoring.KioskDispenserMonitoring, "Kiosk Para Üstü İzleme Raporu", headerNames);
                }
                else if (dispenserMonitoring.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + dispenserMonitoring.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelMutabakat");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;
        ClickableWebControl.ClickableTableHeaderCell Header = ((ClickableWebControl.ClickableTableHeaderCell)sender);
        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "KioskName":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Header.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";

                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  Header.Text = "<a>Kiosk Adı <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  Header.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CreatedUserId":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //InstitutionId.Text = "<a>Kurum Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //InstitutionId.Text = "<a>Kurum Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // InstitutionId.Text = "<a>Kurum Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "InsertionDate":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // InsertionDate.Text = "<a>İşlem Tarihi <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //InsertionDate.Text = "<a>İşlem Tarihi <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  InsertionDate.Text = "<a>İşlem Tarihi     <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "FirstDenomNumber":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // FirstDenomValue.Text = "<a>V1    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  FirstDenomValue.Text = "<a>V1    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //FirstDenomValue.Text = "<a>V1    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "FirstDenomNumberOld":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // FirstDenomNumber.Text = "<a>#1 <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //FirstDenomNumber.Text = "<a>#1   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // FirstDenomNumber.Text = "<a>#1    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "SecondDenomNumber":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SecondDenomValue.Text = "<a>V2    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // SecondDenomValue.Text = "<a>V2    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  SecondDenomValue.Text = "<a>V2    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "SecondDenomNumberOld":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SecondDenomNumber.Text = "<a>#2    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // SecondDenomNumber.Text = "<a>#2    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // SecondDenomNumber.Text = "<a>#2     <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "ThirdDenomNumber":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // ThirdDenomValue.Text = "<a>V3    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // ThirdDenomValue.Text = "<a>V3    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // ThirdDenomValue.Text = "<a>V3   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "ThirdDenomNumberOld":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // ThirdDenomNumber.Text = "<a>#3    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // ThirdDenomNumber.Text = "<a>#3    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // ThirdDenomNumber.Text = "<a>#3   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "FourthDenomNumber":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // FourthDenomValue.Text = "<a>V4    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //   FourthDenomValue.Text = "<a>V4    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //FourthDenomValue.Text = "<a>V4   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "FourthDenomNumberOld":
                if (orderSelectionColumn == 11)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // FourthDenomNumber.Text = "<a>#4   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // FourthDenomNumber.Text = "<a>#4    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // FourthDenomNumber.Text = "<a>#4   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 11;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "FifthDenomNumber":
                if (orderSelectionColumn == 12)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  FifthDenomValue.Text = "<a>V5   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // FifthDenomValue.Text = "<a>V5    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // FifthDenomValue.Text = "<a>V5   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 12;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "FifthDenomNumberOld":
                if (orderSelectionColumn == 13)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  FifthDenomNumber.Text = "<a>#5   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  FifthDenomNumber.Text = "<a>#5    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // FifthDenomNumber.Text = "<a>#5   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 13;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "SixthDenomNumber":
                if (orderSelectionColumn == 14)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SixthDenomValue.Text = "<a>V6   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  SixthDenomValue.Text = "<a>V6    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // SixthDenomValue.Text = "<a>V6   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 14;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "SixthDenomNumberOld":
                if (orderSelectionColumn == 15)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //SixthDenomNumber.Text = "#6<a>   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // SixthDenomNumber.Text = "#6<a>  <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //SixthDenomNumber.Text = "#6<a>   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 15;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "SeventhDenomNumber":
                if (orderSelectionColumn == 16)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SeventhDenomValue.Text = "<a>V7   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  SeventhDenomValue.Text = "<a>V7    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // SeventhDenomValue.Text = "<a>V7   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 16;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "SeventhDenomNumberOld":
                if (orderSelectionColumn == 17)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  SeventhDenomNumber.Text = "<a>#7   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //SeventhDenomNumber.Text = "<a>#7   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //   SeventhDenomNumber.Text = "<a>#7   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 17;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "EighthDenomNumber":
                if (orderSelectionColumn == 18)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  EighthDenomValue.Text = "<a>V7   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // EighthDenomValue.Text = "<a>V7    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // EighthDenomValue.Text = "<a>V7   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 18;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "EighthDenomNumberOld":
                if (orderSelectionColumn == 19)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // EighthDenomNumber.Text = "<a>#8   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // EighthDenomNumber.Text = "<a>#8   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //EighthDenomNumber.Text = "<a>#8   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 19;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Casette1Reject":
                if (orderSelectionColumn == 20)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //NinethDenomValue.Text = "<a>V9   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //NinethDenomValue.Text = "<a>V9    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //NinethDenomValue.Text = "<a>V9   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 20;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "Casette1RejectOld":
                if (orderSelectionColumn == 21)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //NinethDenomNumber.Text = "<a>#9   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //NinethDenomNumber.Text = "<a>#9   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // NinethDenomNumber.Text = "<a>#9   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 21;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Casette2Reject":
                if (orderSelectionColumn == 22)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // TenthDenomValue.Text = "<a>V10   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // TenthDenomValue.Text = "<a>V10    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TenthDenomValue.Text = "<a>V10   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 22;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "Casette2RejectOld":
                if (orderSelectionColumn == 23)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // TenthDenomNumber.Text = "<a>#10   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TenthDenomNumber.Text = "<a>#10   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TenthDenomNumber.Text = "<a>#10   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 23;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Casette3Reject":
                if (orderSelectionColumn == 24)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //EleventhDenomValue.Text = "<a>V11   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        ///EleventhDenomValue.Text = "<a>V11    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //EleventhDenomValue.Text = "<a>V11   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 24;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "Casette3RejectOld":
                if (orderSelectionColumn == 25)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //EleventhDenomNumber.Text = "<a>#11   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // EleventhDenomNumber.Text = "<a>#11   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    ///EleventhDenomNumber.Text = "<a>#11  <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 25;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Casette4Reject":
                if (orderSelectionColumn == 26)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //EleventhDenomNumber.Text = "<a>#11   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // EleventhDenomNumber.Text = "<a>#11   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    ///EleventhDenomNumber.Text = "<a>#11  <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 26;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Casette4RejectOld":
                if (orderSelectionColumn == 27)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //EleventhDenomNumber.Text = "<a>#11   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // EleventhDenomNumber.Text = "<a>#11   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    ///EleventhDenomNumber.Text = "<a>#11  <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 27;
                    orderSelectionDescAsc = 1;
                }
                break;
            /////////////////////////////////

            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

}