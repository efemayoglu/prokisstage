﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_DailyAskiCode : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int itemId;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/ListDailyAccountReportProkis.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.itemId = Convert.ToInt32(Request.QueryString["itemId"]);

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            //this.startDateField.Text = DateTime.Now.AddDays(-10).ToString("yyyy-MM-dd");
            //this.endDateField.Text = DateTime.Now.ToString("yyyy-MM-dd");

            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;
            this.pageNum = 0;
            this.SearchOrder(4, 1);
        }
    }
    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(4, 1);
    }

    private void BindListTable(ParserListMoneyCodes list)
    {
        try
        {
            TableRow[] Row = new TableRow[list.moneyCodes.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("codeId", list.moneyCodes[i].id.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell transactionIdCell = new TableCell();
                TableCell customerNameCell = new TableCell();
                TableCell aboneNoCell = new TableCell();
                TableCell statusCell = new TableCell();
                TableCell insertionDateCell = new TableCell();
                TableCell codeAmountCell = new TableCell();
                TableCell askiCodeCell = new TableCell();
                TableCell generationCaseCell = new TableCell();
                TableCell useDateCell = new TableCell();
                TableCell generatedUserCell = new TableCell();
                TableCell instutionNameCell = new TableCell();

                instutionNameCell.CssClass = "inputTitleCell4";
                indexCell.CssClass = "inputTitleCell4";
                idCell.CssClass = "inputTitleCell4";
                transactionIdCell.CssClass = "inputTitleCell4";
                customerNameCell.CssClass = "inputTitleCell4";
                aboneNoCell.CssClass = "inputTitleCell4";
                statusCell.CssClass = "inputTitleCell4";
                insertionDateCell.CssClass = "inputTitleCell4";
                codeAmountCell.CssClass = "inputTitleCell4";
                askiCodeCell.CssClass = "inputTitleCell4";
                generationCaseCell.CssClass = "inputTitleCell4";
                useDateCell.CssClass = "inputTitleCell4";
                generatedUserCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                idCell.Text = list.moneyCodes[i].id.ToString();
                transactionIdCell.Text = list.moneyCodes[i].transactionId.ToString();
                instutionNameCell.Text = list.moneyCodes[i].instutionName;
                customerNameCell.Text = list.moneyCodes[i].customerName;
                aboneNoCell.Text = list.moneyCodes[i].aboneNo;
                statusCell.Text = list.moneyCodes[i].statusName;
                insertionDateCell.Text = list.moneyCodes[i].insertionDate;
                codeAmountCell.Text = list.moneyCodes[i].codeAmount;
                askiCodeCell.Text = list.moneyCodes[i].codeNumber;
                generationCaseCell.Text = list.moneyCodes[i].codeGenerationCase;
                useDateCell.Text = list.moneyCodes[i].useDate;
                generatedUserCell.Text = list.moneyCodes[i].generatedUserName;
                idCell.Visible = false;


                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
						idCell,
						transactionIdCell,
                        instutionNameCell,
                        customerNameCell,
                        aboneNoCell,
                        statusCell,
                        insertionDateCell,
                        codeAmountCell,
                        askiCodeCell,
                        generationCaseCell,
                        useDateCell,
                        generatedUserCell,
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (list.recordCount < currentRecordEnd)
                currentRecordEnd = list.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + list.recordCount.ToString() + " kayıt bulundu.";
            }

            TableRow addNewRow = new TableRow();
            TableCell totalMoneyTextCell = new TableCell();
            TableCell totalMoneyCell = new TableCell();
            TableCell spaceCell = new TableCell();
            TableCell spaceCell1 = new TableCell();

            spaceCell.CssClass = "inputTitleCell99";
            spaceCell.ColumnSpan = (5);

            totalMoneyTextCell.CssClass = "inputTitleCell99";
            totalMoneyCell.CssClass = "inputTitleCell99";

            spaceCell1.CssClass = "inputTitleCell99";
            spaceCell1.ColumnSpan = (5);

            spaceCell.Text = " ";
            spaceCell1.Text = " ";

            totalMoneyTextCell.Text = "Toplam Miktar:";
            totalMoneyCell.Text = list.totalAmount + " TL ";

            addNewRow.Cells.Add(spaceCell);
            addNewRow.Cells.Add(totalMoneyTextCell);
            addNewRow.Cells.Add(totalMoneyCell);
            addNewRow.Cells.Add(spaceCell1);
            this.itemsTable.Rows.Add(addNewRow);

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(list.pageCount, this.pageNum, list.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {

            //ViewState["NumberOfItem"] = this.numberOfItemField.Text;
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListMoneyCodes transactions = callWebServ.CallListDailyAskiCodesService(Convert.ToInt64(Request.QueryString["itemId"]),
                                                                             Convert.ToInt32(Request.QueryString["isUsed"]),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id);
            if (transactions != null)
            {
                if (transactions.errorCode == 0)
                {
                    this.BindListTable(transactions);
                }
                else if (transactions.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + transactions.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionSrch");
        }
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    private void ExportToExcel()
    {
        try
        {

            int custId = 0;

            if (Convert.ToInt32(Request.QueryString["customerId"]) != 0)
            {
                custId = Convert.ToInt32(Request.QueryString["customerId"]);
            }

            CallWebServices callWebServ = new CallWebServices();
            ParserListMoneyCodes lists = callWebServ.CallListDailyAskiCodesForExcelService(Convert.ToInt64(Request.QueryString["itemId"]),
                                                                            Convert.ToInt32(Request.QueryString["isUsed"]),
                                                                            Convert.ToInt16(ViewState["OrderColumn"]),
                                                                            Convert.ToInt16(ViewState["OrderDesc"]),
                                                                            this.LoginDatas.AccessToken,
                                                                            this.LoginDatas.User.Id);

            if (lists != null)
            {
                if (lists.errorCode == 0)
                {

                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    exportExcell.ExportExcell(lists.moneyCodes, lists.recordCount, "Günlük Aski Kod Listesi");
                }
                else if (lists.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + lists.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelTransaction");
        }
    }
}