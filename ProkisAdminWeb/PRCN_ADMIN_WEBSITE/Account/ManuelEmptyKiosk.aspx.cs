﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ManuelEmptyKiosk : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/ManuelEmptyKiosk.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }

        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }
       
        if (!Page.IsPostBack)
        {

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {

                this.kioskBox.DataSource = kiosks.KioskNames;
                this.kioskBox.DataBind();

            }

            ListItem item = new ListItem();
            item.Text = "Kiosk Seçiniz";
            item.Value = "0";
            this.kioskBox.Items.Add(item);
            this.kioskBox.SelectedValue = "0";
            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;
        }
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        try
        {

                CallWebServices callWebServ = new CallWebServices();
                ParserOperation response = callWebServ.CallManuelEmptyKioskService(Convert.ToInt32(this.kioskBox.SelectedValue), this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                this.kioskBox.SelectedValue = "0";
                this.messageArea.InnerHtml = "";
                if (response != null)
                {
                    this.messageArea.InnerHtml = response.errorDescription;
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('" + response.errorDescription + "'); ", true);
                }
                else if (response.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('Sisteme Erişilemiyor !'); ", true);
                }

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ManuelKioskEmpty");
        }
    }
}
