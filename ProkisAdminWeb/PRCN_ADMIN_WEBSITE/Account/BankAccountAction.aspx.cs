﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Account;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_BankAccountAction : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/BankAccountAction.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);
        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {

            CallWebServices callWebServ = new CallWebServices();


            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.startDateField.Text = DateTime.Now.ToString("yyyy-MM-dd 00:00:00");
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd 00:00:00");

            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;
            this.pageNum = 0;
            this.SearchOrder(1, 1);

            //addnewRecord.Text = "<a href=\"javascript:void(0);\" onclick=\"editEnterKioskEmptyCashButtonClicked();\" class=\"anylink\"><img src=../images/add.png border=0 title=\"Add New...\" /></a>";


        }

    }



    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "InsertionDate":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "AskiBalance":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //AskiAccount.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "AskiFeeBalance":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //CustomerName.Text = "<a>Müşteri Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //CustomerName.Text = "<a>Müşteri Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //CustomerName.Text = "<a>Müşteri Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "BgazBalance":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "BgazFeeBalance":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //T//ransactionStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransactionStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "DesmerBalance":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Instution.Text = "<a>Kurum    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Instution.Text = "<a>Kurum    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Instution.Text = "<a>Kurum    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;


            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }


    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(1, 1);
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebService = new CallWebServices();
            ParserListBankAccountAction items = callWebService.CallBankAccountAction(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id, 1);

            if (items != null)
            {
                if (items.errorCode == 0)
                {
                    this.BindListTable(items);
                }
                else if (items.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + items.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskCashSrch");
        }
    }

    private void BindListTable(ParserListBankAccountAction items)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            TableRow[] Row = new TableRow[items.BankAccountActions.Count];

            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                //Row[i].Attributes.Add("Id", items.bankaccountaction[i].kioskId.ToString());

                TableCell insertionDateCell = new TableCell();
                TableCell askiBalanceCell = new TableCell();
                TableCell askiFeeBalanceCell = new TableCell();
                TableCell bgazBalanceCell = new TableCell();
                TableCell bgazFeeBalanceCell = new TableCell();
                TableCell desmerBalanceCell = new TableCell();
                TableCell indexCell = new TableCell();

                TableCell askiBalanceDifferenceCell = new TableCell();
                TableCell askiFeeBalanceDifferenceCell = new TableCell();
                TableCell bgazBalanceDifferenceCell = new TableCell();
                TableCell bgazFeeBalanceDifferenceCell = new TableCell();
                TableCell desmerBalanceDifferenceCell = new TableCell();

                insertionDateCell.CssClass = "inputTitleCell4";
                askiBalanceCell.CssClass = "inputTitleCell4";
                askiFeeBalanceCell.CssClass = "inputTitleCell4";
                bgazBalanceCell.CssClass = "inputTitleCell4";
                bgazFeeBalanceCell.CssClass = "inputTitleCell4";
                desmerBalanceCell.CssClass = "inputTitleCell4";
                indexCell.CssClass = "inputTitleCell4";

                askiBalanceDifferenceCell.CssClass = "inputTitleCell4";
                askiFeeBalanceDifferenceCell.CssClass = "inputTitleCell4";
                bgazBalanceDifferenceCell.CssClass = "inputTitleCell4";
                bgazFeeBalanceDifferenceCell.CssClass = "inputTitleCell4";
                desmerBalanceDifferenceCell.CssClass = "inputTitleCell4";


                insertionDateCell.Text = items.BankAccountActions[i].insertionDate;
                askiBalanceCell.Text = items.BankAccountActions[i].askiBalance.ToString();
                askiFeeBalanceCell.Text = items.BankAccountActions[i].askiFeeBalance.ToString();
                bgazBalanceCell.Text = items.BankAccountActions[i].bgazBalance.ToString();
                bgazFeeBalanceCell.Text = items.BankAccountActions[i].bgazFeeBalance.ToString();
                desmerBalanceCell.Text = items.BankAccountActions[i].desmerBalance.ToString();

                indexCell.Text = (startIndex + i).ToString();

                if (i > 0)
                {

                    askiBalanceDifferenceCell.Text = (Convert.ToDecimal(items.BankAccountActions[i - 1].askiBalance) - Convert.ToDecimal(items.BankAccountActions[i].askiBalance)).ToString();
                    askiFeeBalanceDifferenceCell.Text = (Convert.ToDecimal(items.BankAccountActions[i - 1].askiFeeBalance) - Convert.ToDecimal(items.BankAccountActions[i].askiFeeBalance)).ToString();
                    bgazBalanceDifferenceCell.Text = (Convert.ToDecimal(items.BankAccountActions[i - 1].bgazBalance) - Convert.ToDecimal(items.BankAccountActions[i].bgazBalance)).ToString();
                    bgazFeeBalanceDifferenceCell.Text = (Convert.ToDecimal(items.BankAccountActions[i - 1].bgazFeeBalance) - Convert.ToDecimal(items.BankAccountActions[i].bgazFeeBalance)).ToString();
                    desmerBalanceDifferenceCell.Text = (Convert.ToDecimal(items.BankAccountActions[i - 1].desmerBalance) - Convert.ToDecimal(items.BankAccountActions[i].desmerBalance)).ToString();


                }
                else
                {
                    askiBalanceDifferenceCell.Text = "0";
                    askiFeeBalanceDifferenceCell.Text = "0";
                    bgazBalanceDifferenceCell.Text = "0";
                    bgazFeeBalanceDifferenceCell.Text = "0";
                    desmerBalanceDifferenceCell.Text = "0";

                }


                //sumCell.Text = ((Convert.ToDouble(value1Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount1Cell.Text.Replace(',', '.'))) +
                //               (Convert.ToDouble(value2Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount2Cell.Text.Replace(',', '.'))) +
                //               (Convert.ToDouble(value3Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount3Cell.Text.Replace(',', '.'))) +
                //               (Convert.ToDouble(value4Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount4Cell.Text.Replace(',', '.'))) +
                //               (Convert.ToDouble(value5Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount5Cell.Text.Replace(',', '.'))) +
                //               (Convert.ToDouble(value6Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount6Cell.Text.Replace(',', '.'))) +
                //               (Convert.ToDouble(value7Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount7Cell.Text.Replace(',', '.'))) +
                //               (Convert.ToDouble(value8Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount8Cell.Text.Replace(',', '.'))) +
                //               (Convert.ToDouble(value9Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount9Cell.Text.Replace(',', '.'))) +
                //               (Convert.ToDouble(value10Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount10Cell.Text.Replace(',', '.'))) +
                //                (Convert.ToDouble(value11Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount11Cell.Text.Replace(',', '.')))
                //               ).ToString() + " ₺";


                Row[i].Cells.AddRange(new TableCell[]{
                    indexCell,
                    insertionDateCell,
                    askiBalanceCell ,
                    askiBalanceDifferenceCell,
                    askiFeeBalanceCell,
                    askiFeeBalanceDifferenceCell,
                    bgazBalanceCell,
                    bgazBalanceDifferenceCell,
                    bgazFeeBalanceCell,
                    bgazFeeBalanceDifferenceCell,
                    desmerBalanceCell,
                    desmerBalanceDifferenceCell
                });


                //if (i == Row.Length - 1)
                //{
                //    Row[i].CssClass = "inputTitleCell99";
                //    Row[i].Cells[1].Text = "TOPLAM";
                //    Row[i].Cells[0].Text = "";
                //}
                //else
                //{
                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
                //}
            }


            this.itemsTable.Rows.AddRange(Row);

            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (items.recordCount < currentRecordEnd)
                currentRecordEnd = items.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + items.recordCount.ToString() + " kayıt bulundu.";
            }

            //items.pageCount = 10;

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(items.pageCount, this.pageNum, items.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);

            this.itemsTable.Visible = true;


            // if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
            //  {
            //TableRow addNewRow = new TableRow();
            //TableCell addNewCell = new TableCell();
            //TableCell spaceCell = new TableCell();
            //spaceCell.CssClass = "inputTitleCell4";
            //spaceCell.ColumnSpan = (this.itemsTable.Rows[1].Cells.Count - 1);
            //addNewCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editButtonClicked('0');\" class=\"anylink\"><img src=../images/add.png border=0 title=\"Add New...\" /></a>";
            //addNewRow.Cells.Add(spaceCell);
            //addNewRow.Cells.Add(addNewCell);
            //this.itemsTable.Rows.Add(addNewRow);
            // }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListMutabakatBDT");
        }
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }


    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {

            int recordCount = 0;
            int pageCount = 0;



            CallWebServices callWebService = new CallWebServices();
            ParserListBankAccountAction items = callWebService.CallBankAccountAction(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             Convert.ToInt32(ViewState["OrderDesc"]),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id, 2);

            if (items != null)
            {
                if (items.errorCode == 0)
                {


                    ExportExcellDatas exportExcell = new ExportExcellDatas();

                    string[] headerNames = { "İşlem Zamanı ", "Aski Hesabı (359)", "Aski Hesap (360)", "Başkentgaz Hesap (369)", "Başkentgaz Hesap (368)",
                                             "Desmer Hesap (357)"};

                    exportExcell.ExportExcellByBlock(items.BankAccountActions, "Hesaplar", headerNames);
                    //exportExcell.ExportExcell(items.KioskReports, items.recordCount, "Kiosk İşlemleri Listesi");

                    //ExportExcellDatas exportExcell = new ExportExcellDatas();
                    //exportExcell.ExportExcell(items.ParserKioskCashAcceptor, items.recordCount, "Para Giriş Listesi");
                }
                else if (items.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + items.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelKioskCashAcceptor");
        }
    }
}