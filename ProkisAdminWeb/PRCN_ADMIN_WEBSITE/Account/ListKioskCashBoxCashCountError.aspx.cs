﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using PRCNCORE.Parser.Account;

public partial class Account_ListKioskCashBoxCashCountError : System.Web.UI.Page
{

    private ParserLogin LoginDatas = null;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 1;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;
    private int pageNum;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/ListKioskCashBoxCashCountError.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);
        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {

                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }


            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {

                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();

            }

            ListItem item1 = new ListItem();
            item1.Text = "Kurum Seçiniz";
            item1.Value = "0";
            this.instutionBox.Items.Add(item1);
            this.instutionBox.SelectedValue = "0";

             this.startDateField.Text = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00"; ;
             this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00";


            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;

            this.pageNum = 0;
            this.SearchOrder(3, 1);

        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }


    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);
            int recordCount = 0;
            int pageCount = 0;

            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;



            CallWebServices callWebService = new CallWebServices();

            ParserListKioskCashBoxCashCountError listCashCounts = callWebService.CallListKioskCashBoxCashCountErrorService(kioskIds,
                                                                                                           this.searchTextField.Text,
                                                                                                           Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                                                           Convert.ToDateTime(this.startDateField.Text),
                                                                                                           Convert.ToDateTime(this.endDateField.Text),
                                                                                                           Convert.ToInt32(this.numberOfItemField.Text),
                                                                                                           this.pageNum,
                                                                                                           recordCount,
                                                                                                           pageCount,
                                                                                                           this.orderSelectionColumn,
                                                                                                           this.orderSelectionDescAsc,
                                                                                                           this.LoginDatas.AccessToken,
                                                                                                           this.LoginDatas.User.Id
                                                                                                           , 1);

            if (listCashCounts != null)
            {
                if (listCashCounts.errorCode == 0)
                {
                    this.BindListTable(listCashCounts);
                }
                else if (listCashCounts.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + listCashCounts.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CreateKioskCommandSrch");
        }


    }

    private void BindListTable(ParserListKioskCashBoxCashCountError items)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            TableRow[] Row = new TableRow[items.ParserKioskCashBoxCashCountError.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();

                //Row[i].Attributes.Add("Id", items.ParserKioskCashDispenser[i].KioskId.ToString());

                TableCell indexCell = new TableCell();
                TableCell kioskIdCell = new TableCell();
                TableCell transactionIdCell = new TableCell();
                TableCell insertionDate = new TableCell();

                TableCell reject1Cell = new TableCell();
                TableCell reject2Cell = new TableCell();
                TableCell reject3Cell = new TableCell();
                TableCell reject4Cell = new TableCell();

                TableCell value1Cell = new TableCell();
                TableCell value2Cell = new TableCell();
                TableCell value3Cell = new TableCell();
                TableCell value4Cell = new TableCell();

                TableCell exit1Cell = new TableCell();
                TableCell exit2Cell = new TableCell();
                TableCell exit3Cell = new TableCell();
                TableCell exit4Cell = new TableCell();

                TableCell errorCell = new TableCell();
                TableCell errorStatusCell = new TableCell();
                TableCell exceptionCell = new TableCell();

                TableCell totalLogCell = new TableCell();
                TableCell totalDispensedCell = new TableCell();
                TableCell totalDifferenceCell = new TableCell();
                
                TableCell sumCell = new TableCell();

                indexCell.CssClass = "inputTitleCell4";
                kioskIdCell.CssClass = "inputTitleCell4";
                transactionIdCell.CssClass = "inputTitleCell4";
                insertionDate.CssClass = "inputTitleCell4";

                reject1Cell.CssClass = "inputTitleCell4";
                reject2Cell.CssClass = "inputTitleCell4";
                reject3Cell.CssClass = "inputTitleCell4";
                reject4Cell.CssClass = "inputTitleCell4";

                value1Cell.CssClass = "inputTitleCell4";
                value2Cell.CssClass = "inputTitleCell4";
                value3Cell.CssClass = "inputTitleCell4";
                value4Cell.CssClass = "inputTitleCell4";

                exit1Cell.CssClass = "inputTitleCell4";
                exit2Cell.CssClass = "inputTitleCell4";
                exit3Cell.CssClass = "inputTitleCell4";
                exit4Cell.CssClass = "inputTitleCell4";

                errorCell.CssClass = "inputTitleCell4";
                errorStatusCell.CssClass = "inputTitleCell4";
                exceptionCell.CssClass = "inputTitleCell4";

                totalLogCell.CssClass = "inputTitleCell4";
                totalDispensedCell.CssClass = "inputTitleCell4";
                totalDifferenceCell.CssClass = "inputTitleCell4";
             
                

                sumCell.CssClass = "inputTitleCell4";
                kioskIdCell.HorizontalAlign = HorizontalAlign.Left;

                indexCell.Text = (startIndex + i).ToString();


                kioskIdCell.Text = items.ParserKioskCashBoxCashCountError[i].kioskName;
                //kioskIdCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editKioskDispenserCashCountButtonClicked(" + items.ParserKioskCashDispenser[i].kioskID + "," + items.ParserKioskCashDispenser[i].Id + ");\" class=\"anylink\">" + items.ParserKioskCashDispenser[i].kioskId + "</a>";
                transactionIdCell.Text = items.ParserKioskCashBoxCashCountError[i].transactionId;
                insertionDate.Text = items.ParserKioskCashBoxCashCountError[i].insertionDate;

                reject1Cell.Text = items.ParserKioskCashBoxCashCountError[i].reject1;
                reject2Cell.Text = items.ParserKioskCashBoxCashCountError[i].reject2;
                reject3Cell.Text = items.ParserKioskCashBoxCashCountError[i].reject3;
                reject4Cell.Text = items.ParserKioskCashBoxCashCountError[i].reject4;

                value1Cell.Text = items.ParserKioskCashBoxCashCountError[i].value1;
                value2Cell.Text = items.ParserKioskCashBoxCashCountError[i].value2;
                value3Cell.Text = items.ParserKioskCashBoxCashCountError[i].value3;
                value4Cell.Text = items.ParserKioskCashBoxCashCountError[i].value4;

                exit1Cell.Text = items.ParserKioskCashBoxCashCountError[i].exit1;
                exit2Cell.Text = items.ParserKioskCashBoxCashCountError[i].exit2;
                exit3Cell.Text = items.ParserKioskCashBoxCashCountError[i].exit3;
                exit4Cell.Text = items.ParserKioskCashBoxCashCountError[i].exit4;

                errorCell.Text = items.ParserKioskCashBoxCashCountError[i].error;
                errorStatusCell.Text = items.ParserKioskCashBoxCashCountError[i].errorStatus;
                exceptionCell.Text = items.ParserKioskCashBoxCashCountError[i].exception;

                totalLogCell.Text = items.ParserKioskCashBoxCashCountError[i].totalLog;
                totalDispensedCell.Text = items.ParserKioskCashBoxCashCountError[i].totalDispensed;
                totalDifferenceCell.Text = items.ParserKioskCashBoxCashCountError[i].totalDifference;

                //value9Cell.Text = items.ParserKioskCashDispenser[i].ninthDenomValue;
                //amount9Cell.Text = items.ParserKioskCashDispenser[i].ninthDenomNumber;
                //value10Cell.Text = items.ParserKioskCashDispenser[i].tenthDenomValue;
                //amount10Cell.Text = items.ParserKioskCashDispenser[i].tenthDenomNumber;
                //value11Cell.Text = items.ParserKioskCashDispenser[i].eleventhDenomValue;
                //amount11Cell.Text = items.ParserKioskCashDispenser[i].eleventhDenomNumber;

                //sumCell.Text = ((Convert.ToDouble(value1Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount1Cell.Text.Replace(',', '.'))) +
                //             (Convert.ToDouble(value2Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount2Cell.Text.Replace(',', '.'))) +
                //             (Convert.ToDouble(value3Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount3Cell.Text.Replace(',', '.'))) +
                //             (Convert.ToDouble(value4Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount4Cell.Text.Replace(',', '.'))) +
                //             (Convert.ToDouble(value5Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount5Cell.Text.Replace(',', '.'))) +
                //             (Convert.ToDouble(value6Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount6Cell.Text.Replace(',', '.'))) +
                //             (Convert.ToDouble(value7Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount7Cell.Text.Replace(',', '.'))) +
                //             (Convert.ToDouble(value8Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount8Cell.Text.Replace(',', '.')))
                //    //(Convert.ToDouble(value9Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount9Cell.Text.Replace(',', '.'))) +
                //    //(Convert.ToDouble(value10Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount10Cell.Text.Replace(',', '.'))) +
                //    // (Convert.ToDouble(value11Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount11Cell.Text.Replace(',', '.')))
                //             ).ToString() + " ₺";
                //
                //
                //value1Cell.Text += " ₺";
                //value2Cell.Text += " ₺";
                //value3Cell.Text += " ₺";
                //value4Cell.Text += " ₺";
                //value5Cell.Text += " ₺";
                //value6Cell.Text += " ₺";
                //value7Cell.Text += " ₺";
                //value8Cell.Text += " ₺";
                ////value9Cell.Text += " ₺";
                ////value10Cell.Text += " ₺";
                ////value11Cell.Text += " ₺";

                Row[i].Cells.AddRange(new TableCell[]{

						   indexCell,
                           transactionIdCell,
						   kioskIdCell,
                           insertionDate,

                           value1Cell,
                           reject1Cell,
                           exit1Cell,

                           value2Cell,
                           reject2Cell,
                           exit2Cell,

                           value3Cell,
                           reject3Cell,
                           exit3Cell,

                           value4Cell,
                           reject4Cell,
                           exit4Cell,

                           errorCell,
                           errorStatusCell,
                           exceptionCell,

                           totalLogCell,
                           totalDispensedCell,
                           totalDifferenceCell
                       
                        
                     
                       // sumCell
					});

                if (i == Row.Length - 1)
                {
                    Row[i].CssClass = "inputTitleCell99";
                    Row[i].Cells[1].Text = "TOPLAM";
                    Row[i].Cells[0].Text = "";
                }
                else
                {
                    if (i % 2 == 0)
                        Row[i].CssClass = "listrow";
                    else
                        Row[i].CssClass = "listRowAlternate";
                }
            }

            this.itemsTable.Rows.AddRange(Row);

            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (items.recordCount < currentRecordEnd)
                currentRecordEnd = items.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + items.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(items.pageCount, this.pageNum, items.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListMutabakatBDT");
        }
    }



    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
      
        Session["numberOfItemField"] = numberOfItemField.Text;

        this.pageNum = 0;
        this.SearchOrder(3, 1);
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);
            int recordCount = 0;
            int pageCount = 0;

            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;



            CallWebServices callWebService = new CallWebServices();

            ParserListKioskCashBoxCashCountError listCashCounts = callWebService.CallListKioskCashBoxCashCountErrorService(kioskIds,
                                                                                                                      this.searchTextField.Text,
                                                                                                                      Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                                                                      Convert.ToDateTime(this.startDateField.Text),
                                                                                                                      Convert.ToDateTime(this.endDateField.Text),
                                                                                                                      Convert.ToInt32(this.numberOfItemField.Text),
                                                                                                                      this.pageNum,
                                                                                                                      recordCount,
                                                                                                                      pageCount,
                                                                                                                      this.orderSelectionColumn,
                                                                                                                      this.orderSelectionDescAsc,
                                                                                                                      this.LoginDatas.AccessToken,
                                                                                                                      this.LoginDatas.User.Id
                                                                                                                      , 2);

            if (listCashCounts != null)
            {
                if (listCashCounts.errorCode == 0)
                {

                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    string[] headerNames = { "İşlem Id", "Kiosk Adı", "İşlem Zamanı", "R1", "R2", "R3", "R4", 
                                               "V1", "V2", "V3", "V4", "E1", "E2", "E3", "E4",
                                               "Hata Tipi",  "Hata Durumu", "Exception No", 
                                               "Toplam Değer", "Verilen No", "Fark"};


                    exportExcell.ExportExcellByBlock(listCashCounts.ParserKioskCashBoxCashCountError, "Kiosk Para Üstü Hataları", headerNames);
                }
                else if (listCashCounts.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + listCashCounts.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelMutabakat");
        }
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        ClickableWebControl.ClickableTableHeaderCell Header = ((ClickableWebControl.ClickableTableHeaderCell)sender);

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "TransactionId":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Header.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";

                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  Header.Text = "<a>Kiosk Adı <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  Header.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "KioskName":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //InstitutionId.Text = "<a>Kurum Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //InstitutionId.Text = "<a>Kurum Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // InstitutionId.Text = "<a>Kurum Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "InsertionDate":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // InsertionDate.Text = "<a>İşlem Tarihi <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //InsertionDate.Text = "<a>İşlem Tarihi <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  InsertionDate.Text = "<a>İşlem Tarihi     <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Casette1Reject":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // FirstDenomValue.Text = "<a>V1    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  FirstDenomValue.Text = "<a>V1    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //FirstDenomValue.Text = "<a>V1    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Casette2Reject":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // FirstDenomNumber.Text = "<a>#1 <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //FirstDenomNumber.Text = "<a>#1   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // FirstDenomNumber.Text = "<a>#1    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Casette3Reject":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SecondDenomValue.Text = "<a>V2    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // SecondDenomValue.Text = "<a>V2    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  SecondDenomValue.Text = "<a>V2    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Casette4Reject":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SecondDenomNumber.Text = "<a>#2    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // SecondDenomNumber.Text = "<a>#2    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // SecondDenomNumber.Text = "<a>#2     <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Value1":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // ThirdDenomValue.Text = "<a>V3    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // ThirdDenomValue.Text = "<a>V3    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // ThirdDenomValue.Text = "<a>V3   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Value2":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // ThirdDenomNumber.Text = "<a>#3    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // ThirdDenomNumber.Text = "<a>#3    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // ThirdDenomNumber.Text = "<a>#3   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "Value3":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // FourthDenomValue.Text = "<a>V4    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //   FourthDenomValue.Text = "<a>V4    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //FourthDenomValue.Text = "<a>V4   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "Value4":
                if (orderSelectionColumn == 11)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // FourthDenomNumber.Text = "<a>#4   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // FourthDenomNumber.Text = "<a>#4    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // FourthDenomNumber.Text = "<a>#4   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 11;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "Exit1":
                if (orderSelectionColumn == 12)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  FifthDenomValue.Text = "<a>V5   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // FifthDenomValue.Text = "<a>V5    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // FifthDenomValue.Text = "<a>V5   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 12;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "Exit2":
                if (orderSelectionColumn == 13)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  FifthDenomNumber.Text = "<a>#5   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  FifthDenomNumber.Text = "<a>#5    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // FifthDenomNumber.Text = "<a>#5   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 13;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "Exit3":
                if (orderSelectionColumn == 14)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SixthDenomValue.Text = "<a>V6   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  SixthDenomValue.Text = "<a>V6    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // SixthDenomValue.Text = "<a>V6   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 14;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "Exit4":
                if (orderSelectionColumn == 15)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //SixthDenomNumber.Text = "#6<a>   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // SixthDenomNumber.Text = "#6<a>  <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //SixthDenomNumber.Text = "#6<a>   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 15;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Error":
                if (orderSelectionColumn == 16)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SeventhDenomValue.Text = "<a>V7   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  SeventhDenomValue.Text = "<a>V7    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // SeventhDenomValue.Text = "<a>V7   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 16;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "ErrorStatus":
                if (orderSelectionColumn == 17)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  SeventhDenomNumber.Text = "<a>#7   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //SeventhDenomNumber.Text = "<a>#7   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //   SeventhDenomNumber.Text = "<a>#7   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 17;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Exception":
                if (orderSelectionColumn == 18)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  EighthDenomValue.Text = "<a>V7   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // EighthDenomValue.Text = "<a>V7    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // EighthDenomValue.Text = "<a>V7   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 18;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "TotalLog":
                if (orderSelectionColumn == 19)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // EighthDenomNumber.Text = "<a>#8   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // EighthDenomNumber.Text = "<a>#8   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //EighthDenomNumber.Text = "<a>#8   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 19;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "TotalDispensed":
                if (orderSelectionColumn == 20)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //NinethDenomValue.Text = "<a>V9   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //NinethDenomValue.Text = "<a>V9    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //NinethDenomValue.Text = "<a>V9   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 20;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "TotalDifference":
                if (orderSelectionColumn == 21)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //NinethDenomNumber.Text = "<a>#9   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //NinethDenomNumber.Text = "<a>#9   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // NinethDenomNumber.Text = "<a>#9   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 21;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "TenthDenomValue":
                if (orderSelectionColumn == 22)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // TenthDenomValue.Text = "<a>V10   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // TenthDenomValue.Text = "<a>V10    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TenthDenomValue.Text = "<a>V10   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 22;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "TenthDenomNumber":
                if (orderSelectionColumn == 23)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // TenthDenomNumber.Text = "<a>#10   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TenthDenomNumber.Text = "<a>#10   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TenthDenomNumber.Text = "<a>#10   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 23;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "EleventhDenomValue":
                if (orderSelectionColumn == 24)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //EleventhDenomValue.Text = "<a>V11   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        ///EleventhDenomValue.Text = "<a>V11    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //EleventhDenomValue.Text = "<a>V11   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 24;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "EleventhDenomNumber":
                if (orderSelectionColumn == 25)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //EleventhDenomNumber.Text = "<a>#11   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // EleventhDenomNumber.Text = "<a>#11   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    ///EleventhDenomNumber.Text = "<a>#11  <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 25;
                    orderSelectionDescAsc = 1;
                }
                break;
            /////////////////////////////////

            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }


}