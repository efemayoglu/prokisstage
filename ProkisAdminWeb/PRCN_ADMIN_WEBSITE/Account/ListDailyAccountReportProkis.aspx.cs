﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ListDailyAccountReportProkis : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 1;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/ListDailyAccountReportProkis.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            this.startDateField.Text = DateTime.Now.AddDays(-10).ToString("yyyy-MM-dd");
            this.endDateField.Text = DateTime.Now.ToString("yyyy-MM-dd");

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;
            this.pageNum = 0;
            this.SearchOrder(2, 1);
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(1, 1);
    }

    private void BindListTable(ParserListDailyReportsProkis items)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);
            
            TableRow[] Row = new TableRow[items.DailyReports.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("Id", items.DailyReports[i].Id.ToString());

                TableCell indexCell = new TableCell();
                TableCell reportDateCell = new TableCell();
                TableCell allBalanceCell = new TableCell();
                TableCell activeBalanceCell = new TableCell();
                TableCell passiveBalanceCell = new TableCell();
                TableCell totalBalanceCell = new TableCell();
                TableCell detailsCell = new TableCell();

                TableCell pasifCodeBalanceCell = new TableCell();
                TableCell pasifCodeCountCell = new TableCell();
                TableCell usableCodeBeforeExpiringCell = new TableCell();

                TableCell fakeCell1 = new TableCell();
                TableCell fakeCell2 = new TableCell();
                TableCell fakeCell3 = new TableCell();
                TableCell fakeCell4 = new TableCell();
                TableCell fakeCell5 = new TableCell();
                TableCell fakeCell6 = new TableCell();

                fakeCell1.CssClass = "inputTitleCell4";
                fakeCell2.CssClass = "inputTitleCell4";
                fakeCell3.CssClass = "inputTitleCell4";
                fakeCell4.CssClass = "inputTitleCell4";
                fakeCell5.CssClass = "inputTitleCell4";
                fakeCell6.CssClass = "inputTitleCell4";

                fakeCell1.Visible = false;
                fakeCell2.Visible = false;
                fakeCell3.Visible = false;
                fakeCell4.Visible = false;
                fakeCell5.Visible = false;
                fakeCell6.Visible = false;

                indexCell.CssClass = "inputTitleCell4";
                reportDateCell.CssClass = "inputTitleCell4";
                allBalanceCell.CssClass = "inputTitleCell4";
                activeBalanceCell.CssClass = "inputTitleCell4";
                passiveBalanceCell.CssClass = "inputTitleCell4";
                totalBalanceCell.CssClass = "inputTitleCell4";
                detailsCell.CssClass = "inputTitleCell4";
                pasifCodeBalanceCell.CssClass = "inputTitleCell4";
                pasifCodeCountCell.CssClass = "inputTitleCell4";
                usableCodeBeforeExpiringCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                reportDateCell.Text = items.DailyReports[i].InsertionDate;
               // allBalanceCell.Text = items.DailyReports[i].TotalBalance + " TL";
                //activeBalanceCell.Text = items.DailyReports[i].ActiveAskiCode + " TL";
                //passiveBalanceCell.Text = items.DailyReports[i].PasifAskiCode + " TL";
               // detailsCell.Text = "<a href=# onClick=\"getDailyAccountActionsClicked(" + items.DailyReports[i].Id + ");\"><img src=../images/detail.png border=0 title=\"Haraketler\" /></a>";
                allBalanceCell.Text = "<a href=\"javascript:void(0);\" onclick=\"getDailyAccountActionsClicked(" + items.DailyReports[i].Id + ");\" class=\"anylink\">" + items.DailyReports[i].TotalBalance + " TL </a>";
                activeBalanceCell.Text = "<a href=\"javascript:void(0);\" onclick=\"getDailyGeneratedAskiCodesClicked(" + items.DailyReports[i].Id + ");\" class=\"anylink\">" + items.DailyReports[i].ActiveAskiCode + " TL </a>";
                passiveBalanceCell.Text = "<a href=\"javascript:void(0);\" onclick=\"getDailyUsedAskiCodesClicked(" + items.DailyReports[i].Id + ");\" class=\"anylink\">" + items.DailyReports[i].PasifAskiCode + " TL </a>";
                //usableBalanceCell.Text = (Convert.ToDouble(activeBalanceCell.Text) - Convert.ToDouble(passiveBalanceCell.Text)).ToString();
                totalBalanceCell.Text = String.Format("{0:0.00}", Convert.ToDouble(items.DailyReports[i].ActiveAskiCode) + Convert.ToDouble(items.DailyReports[i].PasifAskiCode))+" TL";
                pasifCodeBalanceCell.Text = items.DailyReports[i].PasifCodeBalance; //
                pasifCodeCountCell.Text = items.DailyReports[i].PasifCodeCount; //
                usableCodeBeforeExpiringCell.Text = items.DailyReports[i].UsableCodeBeforeExpiring;
                 

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        fakeCell1,
                        fakeCell2,
                        fakeCell3,
                        fakeCell4,
                        fakeCell5,
                        reportDateCell,
                        allBalanceCell,
                        activeBalanceCell,
                        passiveBalanceCell,
                        totalBalanceCell,
                        pasifCodeBalanceCell,
                        pasifCodeCountCell,
                        usableCodeBeforeExpiringCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (items.recordCount < currentRecordEnd)
                currentRecordEnd = items.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + items.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(items.pageCount, this.pageNum, items.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListDaiilyAccountReportBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "ReportDate":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // ReportDate.Text = "<a>Rapor Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //ReportDate.Text = "<a>Rapor Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                   // ReportDate.Text = "<a>Rapor Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "AllBalance":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // AllBalance.Text = "<a>Para Miktarı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //AllBalance.Text = "<a>Para Miktarı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //AllBalance.Text = "<a>Para Miktarı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "ActiveBalance":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // ActiveBalance.Text = "<a>Üretilen Para Kod Miktarı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //ActiveBalance.Text = "<a>Üretilen Para Kod Miktarı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //ActiveBalance.Text = "<a>Üretilen Para Kod Miktarı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "PasifBalance":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // PasifBalance.Text = "<a>Kullanılan Para Kod Miktarı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                       // PasifBalance.Text = "<a>Kullanılan Para Kod Miktarı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                   // PasifBalance.Text = "<a>Kullanılan Para Kod Miktarı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "TotalBalance":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // PasifBalance.Text = "<a>Kullanılan Para Kod Miktarı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // PasifBalance.Text = "<a>Kullanılan Para Kod Miktarı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // PasifBalance.Text = "<a>Kullanılan Para Kod Miktarı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;


            default:

                orderSelectionColumn = 5;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListDailyReportsProkis items = callWebServ.CallListDailyAccountReportProKisService(Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text).AddDays(1).AddSeconds(-1),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             1);
            if (items != null)
            {
                if (items.errorCode == 0)
                {
                    this.BindListTable(items);
                }
                else if (items.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + items.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListDailyAccountReportSrch");
        }
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        ExportToExcel();
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void ExportToExcel()
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListDailyReportsProkis lists = callWebServ.CallListDailyAccountReportProKisService(Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text).AddDays(1).AddSeconds(-1),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             Convert.ToInt32(ViewState["OrderDesc"]),
                                                                             this.LoginDatas.AccessToken, this.LoginDatas.User.Id,
                                                                             2);

            if (lists != null)
            {
                if (lists.errorCode == 0)
                {
                    ExportExcellDatas exportExcell = new ExportExcellDatas();

                    string[] headerNames = { "No", "Rapor Zaman", "Para Miktarı ", "Kullanılabilir Para Kod Miktar", "Toplam Para Kod Miktarı", "Pasif Kod Tutar", "Pasif Kod Sayısı", "Ex. Öncesi Aktif Kod" };

                    exportExcell.ExportExcellByBlock(lists.DailyReports,"Günlük Prokis Hesap Raporu", headerNames);
 
                }
                else if (lists.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + lists.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelDailyAccountReport");
        }
    }
}