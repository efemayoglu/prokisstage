﻿using MakeMutabakatParserNS;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Account;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_MakeMutabakatWithDate : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/MakeMutabakatWithDate.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        //this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        //numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {

            //this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.startDateField.Text = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {

                this.kioskBox.DataSource = kiosks.KioskNames;
                this.kioskBox.DataBind();

            }

            ListItem item = new ListItem();
            item.Text = "Kiosk Seçiniz";
            item.Value = "0";
            this.kioskBox.Items.Add(item);
            this.kioskBox.SelectedValue = "0";

            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesForMutabakat(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {

                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();

            }

            ListItem item1 = new ListItem();
            item1.Text = "Kurum Seçiniz";
            item1.Value = "0";
            this.instutionBox.Items.Add(item1);
            this.instutionBox.SelectedValue = "0";

            this.pageNum = 0;
            //this.SearchOrder();
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder();
    }

    private void BindListTable(MakeMutabakatParser datas)
    {
        try
        {
            TableRow[] Row;
            if (this.instutionBox.SelectedValue == "2")
            {
                Row = new TableRow[datas.KioskInfos.KioskInfo.Length];
            }
            else
                Row = new TableRow[1];


            int startIndex = 1;

            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                //Row[i].Attributes.Add("transactionId", datas.[i].TransactionId.ToString());

                TableCell indexCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell instutionNameCell = new TableCell();
                TableCell faturaCountCell = new TableCell();
                TableCell faturaAmountCell = new TableCell();
                TableCell kartDolumCountCell = new TableCell();
                TableCell kartDolumAmountCell = new TableCell();
                TableCell totalCountCell = new TableCell();
                TableCell totalAmountCell = new TableCell();
                TableCell totalCancelCell = new TableCell();
                TableCell totalCancelAmountCell = new TableCell();

                TableCell instutionCountCell = new TableCell();
                TableCell instutionAmountCell = new TableCell();
                TableCell differenceCell = new TableCell();
                TableCell detailsCell = new TableCell();
                TableCell kioskIdCell = new TableCell();
                TableCell txnDiffCell = new TableCell();
                TableCell amountDiffCell = new TableCell();

                indexCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                instutionNameCell.CssClass = "inputTitleCell4";
                faturaCountCell.CssClass = "inputTitleCell4";
                faturaAmountCell.CssClass = "inputTitleCell4";
                kartDolumCountCell.CssClass = "inputTitleCell4";
                kartDolumAmountCell.CssClass = "inputTitleCell4";
                totalCountCell.CssClass = "inputTitleCell4";
                totalAmountCell.CssClass = "inputTitleCell4";
                instutionCountCell.CssClass = "inputTitleCell4";
                instutionAmountCell.CssClass = "inputTitleCell4";
                differenceCell.CssClass = "inputTitleCell4";
                detailsCell.CssClass = "inputTitleCell4";
                totalCancelCell.CssClass = "inputTitleCell4";
                totalCancelAmountCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                kioskIdCell.CssClass = "inputTitleCell4";
                txnDiffCell.CssClass = "inputTitleCell4";
                amountDiffCell.CssClass = "inputTitleCell4";

                kioskIdCell.Visible = false;

                indexCell.Text = (startIndex + i).ToString();
                //kioskNameCell.Text = datas.DbDatas..Transactions[i].KioskName;
                //instutionNameCell.Text = transactions.Transactions[i].instutionName;
                faturaCountCell.Text = datas.DbDatas.FaturaCount.ToString();
                faturaAmountCell.Text = datas.DbDatas.FaturaAmount.ToString();
                kartDolumCountCell.Text = datas.DbDatas.KartDolumCount.ToString();
                kartDolumAmountCell.Text = datas.DbDatas.KartDolumAmount.ToString();
                totalCountCell.Text = datas.DbDatas.TotalDolumCount.ToString();
                totalAmountCell.Text = datas.DbDatas.TotalDolumAmount.ToString();

                kioskNameCell.Text = datas.KioskName;

                string kioskId = kioskBox.SelectedValue.ToString();
                string kioskUserName = "0";

                if (this.instutionBox.SelectedValue == "1")
                {
                    //instutionCountCell.Text = datas.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc.Length.ToString();
                    double totalNum = 0, tnxNum = 0;
                    for (int j = 0; j < datas.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc.Length; j++)
                    {
                        if (datas.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[j].ISLEM_KODU == 1)
                        {
                            totalNum += Convert.ToDouble(datas.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[j].SATISTUTARIYTL);
                            tnxNum++;
                        }
                    }
                    instutionAmountCell.Text = totalNum.ToString();
                    instutionCountCell.Text = tnxNum.ToString();
                }
                else if (this.instutionBox.SelectedValue == "2")
                {
                    kioskId = datas.KioskInfos.KioskInfo[i].KioskId;

                    kioskNameCell.Text = datas.KioskInfos.KioskInfo[i].KioskName;
                    faturaCountCell.Text = datas.KioskInfos.KioskInfo[i].FaturaSayısı;
                    faturaAmountCell.Text = datas.KioskInfos.KioskInfo[i].FaturaDolumTutar;
                    kartDolumCountCell.Text = datas.KioskInfos.KioskInfo[i].KartDolumSayısı;
                    kartDolumAmountCell.Text = datas.KioskInfos.KioskInfo[i].KartDolumTutar;
                    totalCountCell.Text = (Convert.ToDecimal(datas.KioskInfos.KioskInfo[i].FaturaSayısı.Replace(',', '.')) + Convert.ToDecimal(datas.KioskInfos.KioskInfo[i].KartDolumSayısı.Replace(',', '.'))).ToString();
                    totalAmountCell.Text = (Convert.ToDecimal(datas.KioskInfos.KioskInfo[i].FaturaDolumTutar.Replace(',', '.')) + Convert.ToDecimal(datas.KioskInfos.KioskInfo[i].KartDolumTutar.Replace(',', '.'))).ToString();

                    kioskUserName = datas.KioskInfos.KioskInfo[i].UserName;

                    double totalNum = 0, totalCancelNum = 0;
                    int totalIndex = 0, totalcancelIndex = 0;

                    /*
                    for (int j = 0; j < datas.Faturalar.Length; j++)
                    {
                        if (datas.Faturalar[j].IslemSekli.Contains("TAHSİLAT") && datas.Faturalar[j].IslemSekli.Contains(datas.KioskInfos.KioskInfo[i].UserName))
                        {
                            totalIndex++;
                            totalNum += datas.Faturalar[j].Tutar;
                        }
                        else if (!datas.Faturalar[j].IslemSekli.Contains("TAHSİLAT") && datas.Faturalar[j].IslemSekli.Contains(datas.KioskInfos.KioskInfo[i].UserName))
                        {
                            totalcancelIndex++;
                            totalCancelNum += datas.Faturalar[j].Tutar;
                        }
                    }*/
                    /*
                    for (int j = 0; j < datas.ET_DETAY.Length; j++)
                    {
                        if (datas.KioskInfos.KioskInfo[i].UserName == datas.ET_DETAY[j].VEZNE)
                        {
                            totalIndex++;
                            totalNum += Convert.ToDouble(datas.ET_DETAY[j].Tutar.Replace(',', '.'));
                        }
                    }
                     * */
                    for (int j = 0; j < datas.ET_VEZNE.Length; j++)
                    {
                        if (datas.KioskInfos.KioskInfo[i].UserName == datas.ET_VEZNE[j].VEZNE)
                        {
                            instutionAmountCell.Text = datas.ET_VEZNE[j].SATIS_TOP.Replace(',', '.');
                            totalCancelAmountCell.Text = datas.ET_VEZNE[j].IPTAL_SAY.Replace(',', '.');

                            instutionCountCell.Text = datas.ET_VEZNE[j].SATIS_SAY.Replace(',', '.');
                            totalCancelCell.Text = datas.ET_VEZNE[j].IPTAL_TOP.Replace(',', '.');
                        }
                    }
                    //instutionAmountCell.Text = totalNum.ToString().Replace(',', '.');
                    //totalCancelAmountCell.Text = totalCancelNum.ToString().Replace(',', '.');

                    //instutionCountCell.Text = totalIndex.ToString();
                    //totalCancelCell.Text = totalcancelIndex.ToString();

                    //kioskId = datas.KioskId;
                }
                else if (this.instutionBox.SelectedValue == "4" || this.instutionBox.SelectedValue == "5" || this.instutionBox.SelectedValue == "3")
                {

                    instutionAmountCell.Text = datas.paymentTotalAmount;
                    totalCancelAmountCell.Text = datas.paymentCancelAmount;

                    instutionCountCell.Text = datas.paymentTotalCount;
                    totalCancelCell.Text = datas.paymentCancelCount;
                }

                differenceCell.Width = Unit.Pixel(100);
                string us = "1";
                string same = "3";
                string other = "2";
                string multi = "4";

                differenceCell.Text = "<a href=# onClick=\"showMutabakatListWindow('" + startDateField.Text + "'," + kioskId + "," + instutionBox.SelectedValue.ToString() + "," + us + ",'" + kioskUserName + "');\"><img src=../images/detail.png border=0 title=\"BÖSde olup kurumda olmayanlar\" /></a>"
                    + "<a href=# onClick=\"showMutabakatListWindow('" + startDateField.Text + "'," + kioskId + "," + instutionBox.SelectedValue.ToString() + "," + other + ",'" + kioskUserName + "');\"><img src=../images/detail.png border=0 title=\"Kurumda olup BÖSde olmayanlar\" /></a>"
                   + "<a href=# onClick=\"showMutabakatListWindow('" + startDateField.Text + "'," + kioskId + "," + instutionBox.SelectedValue.ToString() + "," + same + ",'" + kioskUserName + "');\"><img src=../images/detail.png border=0 title=\"İki tarafta da olanlar\" /></a>"
                   + "<a href=# onClick=\"showMultiListWindow('" + startDateField.Text + "'," + kioskId + "," + instutionBox.SelectedValue.ToString() + "," + multi + ");\"><img src=../images/multi.png border=0 title=\"Birden fazla kayıt inceleme\" /></a>";

                detailsCell.Width = Unit.Pixel(65);
                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1")
                {
                    detailsCell.Text = "<a href=# onClick=\"useBOSValuesClicked(" + faturaAmountCell.Text + "," + faturaCountCell.Text + "," + kartDolumAmountCell.Text + "," + kartDolumCountCell.Text + "," + totalCountCell.Text + "," + totalAmountCell.Text + "," + instutionCountCell.Text + "," + instutionAmountCell.Text + ",'" + startDateField.Text + "'," + kioskId + "," + instutionBox.SelectedValue.ToString() + ");\"><img src=../images/generate_over.png border=0 title=\"BÖS Değerleri ile Yap\" /></a>"
                        + "<a href=# onClick=\"useBGazValuesClicked(" + faturaAmountCell.Text + "," + faturaCountCell.Text + "," + kartDolumAmountCell.Text + "," + kartDolumCountCell.Text + "," + totalCountCell.Text + "," + totalAmountCell.Text + "," + instutionCountCell.Text + "," + instutionAmountCell.Text + ",'" + startDateField.Text + "'," + kioskId + "," + instutionBox.SelectedValue.ToString() + "," + totalCancelAmountCell.Text + "," + totalCancelCell.Text + ");\"><img src=../images/approve.png border=0 title=\"BGaz Değerleri ile Yap\" /></a>";
                }
                else
                    detailsCell.Text = "";

                txnDiffCell.Text = (Convert.ToDecimal(totalCountCell.Text.Replace(',', '.')) - Convert.ToDecimal(instutionCountCell.Text.Replace(',', '.'))).ToString();
                decimal amountDif = Convert.ToDecimal(totalAmountCell.Text.Replace(',', '.'));
                decimal instAmountDif = Convert.ToDecimal(instutionAmountCell.Text.Replace(',', '.'));
                amountDiffCell.Text = (amountDif - instAmountDif).ToString();



                Row[i].Cells.AddRange(new TableCell[]{
                        indexCell,
                        kioskNameCell,
                        faturaCountCell,
                        faturaAmountCell,
                        kartDolumCountCell,
                        kartDolumAmountCell,
                        totalCountCell,
                        totalAmountCell,
                        instutionCountCell,
                        instutionAmountCell,
                        totalCancelCell,
                        totalCancelAmountCell,
                        txnDiffCell,
                        amountDiffCell,
                        differenceCell,
                        detailsCell
                    });

                if (instutionAmountCell.Text == "") instutionAmountCell.Text = "0";
                if (totalAmountCell.Text == "") totalAmountCell.Text = "0";

                if (Convert.ToDecimal(instutionAmountCell.Text.Replace(',', '.')) != Convert.ToDecimal(totalAmountCell.Text.Replace(',', '.')))
                {
                    Row[i].CssClass = "listRowAlternateRed";
                }
                else
                {
                    if (i % 2 == 0)
                        Row[i].CssClass = "listrow";
                    else
                        Row[i].CssClass = "listRowAlternate";
                }
            }

            this.itemsTable.Rows.AddRange(Row);
            //TableRow pagingRow = new TableRow();
            //TableCell pagingCell = new TableCell();

            //int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            //int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            //pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            //pagingCell.HorizontalAlign = HorizontalAlign.Right;
            //pagingCell.Text = WebUtilities.GetPagingText(transactions.pageCount, this.pageNum, transactions.recordCount);
            //pagingRow.Cells.Add(pagingCell);
            //this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionBDT");
        }
    }

    private void BindListTableMaski(MakeMutabakatMaskiParser datas)
    {
        try
        {
            TableRow[] Row = new TableRow[1];
            int startIndex = 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                //Row[i].Attributes.Add("transactionId", datas.[i].TransactionId.ToString());

                TableCell indexCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell instutionNameCell = new TableCell();
                TableCell faturaCountCell = new TableCell();
                TableCell faturaAmountCell = new TableCell();
                TableCell kartDolumCountCell = new TableCell();
                TableCell kartDolumAmountCell = new TableCell();
                TableCell totalCountCell = new TableCell();
                TableCell totalAmountCell = new TableCell();

                TableCell instutionCountCell = new TableCell();
                TableCell instutionAmountCell = new TableCell();
                TableCell differenceCell = new TableCell();
                TableCell detailsCell = new TableCell();

                indexCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                instutionNameCell.CssClass = "inputTitleCell4";
                faturaCountCell.CssClass = "inputTitleCell4";
                faturaAmountCell.CssClass = "inputTitleCell4";
                kartDolumCountCell.CssClass = "inputTitleCell4";
                kartDolumAmountCell.CssClass = "inputTitleCell4";
                totalCountCell.CssClass = "inputTitleCell4";
                totalAmountCell.CssClass = "inputTitleCell4";
                instutionCountCell.CssClass = "inputTitleCell4";
                instutionAmountCell.CssClass = "inputTitleCell4";
                differenceCell.CssClass = "inputTitleCell4";
                detailsCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                //kioskNameCell.Text = datas.DbDatas..Transactions[i].KioskName;
                //instutionNameCell.Text = transactions.Transactions[i].instutionName;
                faturaCountCell.Text = datas.DbDatas.FaturaCount.ToString();
                faturaAmountCell.Text = datas.DbDatas.FaturaAmount.ToString();
                kartDolumCountCell.Text = datas.DbDatas.KartDolumCount.ToString();
                kartDolumAmountCell.Text = datas.DbDatas.KartDolumAmount.ToString();
                totalCountCell.Text = datas.DbDatas.TotalDolumCount.ToString();
                totalAmountCell.Text = datas.DbDatas.TotalDolumAmount.ToString();
                instutionAmountCell.Text = datas.SoapEnvelope.SoapBody.BankaGenelMutabakatResponse.BankaGenelMutabakatResult.ToplamTahsilatTutari;
                instutionCountCell.Text = datas.SoapEnvelope.SoapBody.BankaGenelMutabakatResponse.BankaGenelMutabakatResult.ToplamTahsilatAdedi;
                differenceCell.Width = Unit.Pixel(100);
                string us = "1";
                string same = "3";
                string other = "2";

                differenceCell.Text = "<a href=# onClick=\"showMutabakatListWindow('" + startDateField.Text + "'," + kioskBox.SelectedValue.ToString() + "," + instutionBox.SelectedValue.ToString() + "," + us + ");\"><img src=../images/detail.png border=0 title=\"BÖSde olup kurumda olmayanlar\" /></a>"
                    + "<a href=# onClick=\"showMutabakatListWindow('" + startDateField.Text + "'," + kioskBox.SelectedValue.ToString() + "," + instutionBox.SelectedValue.ToString() + "," + other + ");\"><img src=../images/detail.png border=0 title=\"Kurumda olup BÖSde olmayanlar\" /></a>"
                   + "<a href=# onClick=\"showMutabakatListWindow('" + startDateField.Text + "'," + kioskBox.SelectedValue.ToString() + "," + instutionBox.SelectedValue.ToString() + "," + same + ");\"><img src=../images/detail.png border=0 title=\"İki tarafta da olanlar\" /></a>";

                detailsCell.Width = Unit.Pixel(65);
                detailsCell.Text = "<a href=# onClick=\"useBOSValuesClicked(" + faturaAmountCell.Text + "," + faturaCountCell.Text + "," + kartDolumAmountCell.Text + "," + kartDolumCountCell.Text + "," + totalCountCell.Text + "," + totalAmountCell.Text + "," + instutionCountCell.Text + "," + instutionAmountCell.Text + ",'" + startDateField.Text + "'," + kioskBox.SelectedValue.ToString() + "," + instutionBox.SelectedValue.ToString() + ");\"><img src=../images/generate_over.png border=0 title=\"BÖS Değerleri ile Yap\" /></a>"
                    + "<a href=# onClick=\"useBGazValuesClicked(" + faturaAmountCell.Text + "," + faturaCountCell.Text + "," + kartDolumAmountCell.Text + "," + kartDolumCountCell.Text + "," + totalCountCell.Text + "," + totalAmountCell.Text + "," + instutionCountCell.Text + "," + instutionAmountCell.Text + ",'" + startDateField.Text + "'," + kioskBox.SelectedValue.ToString() + "," + instutionBox.SelectedValue.ToString() + ");\"><img src=../images/approve.png border=0 title=\"BGaz Değerleri ile Yap\" /></a>";

                Row[i].Cells.AddRange(new TableCell[]{
                        indexCell,
                        faturaCountCell,
                        faturaAmountCell,
                        kartDolumCountCell,
                        kartDolumAmountCell,
                        totalCountCell,
                        totalAmountCell,
                        instutionCountCell,
                        instutionAmountCell,
                        differenceCell,
                        detailsCell
                    });

                if (instutionAmountCell != totalAmountCell)
                {
                    Row[i].CssClass = "listRowAlternateRed";
                }
                else
                {
                    if (i % 2 == 0)
                        Row[i].CssClass = "listrow";
                    else
                        Row[i].CssClass = "listRowAlternate";
                }
            }

            this.itemsTable.Rows.AddRange(Row);
            //TableRow pagingRow = new TableRow();
            //TableCell pagingCell = new TableCell();

            //int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            //int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            //pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            //pagingCell.HorizontalAlign = HorizontalAlign.Right;
            //pagingCell.Text = WebUtilities.GetPagingText(transactions.pageCount, this.pageNum, transactions.recordCount);
            //pagingRow.Cells.Add(pagingCell);
            //this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionBDT");
        }
    }
    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.SearchOrder();
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        ExportToExcel();
        this.SearchOrder();
    }

    private void ExportToExcel()
    {
        try
        {

            CallWebServices callWebServ = new CallWebServices();
            MakeMutabakatMaskiParser datas = callWebServ.CallGetMakeMutabakatMaskiValues(Convert.ToDateTime(this.startDateField.Text),
                                                                                Convert.ToInt32(this.kioskBox.SelectedValue),
                                                                                Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                                this.LoginDatas.AccessToken,
                                                                                this.LoginDatas.User.Id, 2);

            if (datas != null)
            {
                if (datas.ErrorCode == 0)
                {

                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    string[] headerNames = {  "Kiosk Adı","Kiosk Adı", "Adres","Kiosk Ip", "Kiosk Durum","6",
                                               "Eklenme Tarihi",  "Enlem", "Boylam" ,"Şehir No", "Şehir" ,"12", "Lokasyan Türü",
                                               "Kart Okuyucu Tipi", "Kurum","Kullanım Ücreti",   "Uygulama Versiyonu",  
                                               "Çalışma Durumu" ,"Kapasite Durumu","Açıklama","Kapasite Türü","22","23","24","Bağlantı Tipi","26","Kart Okuyucu Tip","Kiosk Tipi","29","Risk Sınıfı"
                                                 };

                    //exportExcell.ExportExcellByBlock(datas.DbDatas, "Günlük Mutabakat", headerNames);
                }
                else if (datas.ErrorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + datas.ErrorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelTransaction");
        }
    }



    private void SearchOrder()
    {
        try
        {
            CallWebServices callWebServ = new CallWebServices();

            if (Convert.ToInt32(this.instutionBox.SelectedValue) == 2)
            {
                if (Convert.ToInt32(this.kioskBox.SelectedValue) == 0)
                {
                    GetMutabakatValues(callWebServ, Convert.ToInt32(kioskBox.SelectedValue), this.kioskBox.SelectedItem.Text.Substring(0, 4));
                }
                else
                {
                    if (kioskBox.SelectedItem.Text.Substring(0, 1) == "4")
                    {
                        GetMutabakatValues(callWebServ, Convert.ToInt32(kioskBox.SelectedValue), this.kioskBox.SelectedItem.Text.Substring(0, 4));
                    }
                }
            }
            else
                GetMutabakatValues(callWebServ, Convert.ToInt32(this.kioskBox.SelectedValue), "");

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionSrch");
        }
    }


    protected void GetMutabakatValues(CallWebServices callWebServ, int kioskId, string kioskName)
    {

        try
        {
            MakeMutabakatParser datas = callWebServ.CallGetMakeMutabakatValues(Convert.ToDateTime(this.startDateField.Text),
                                                                                        kioskId,
                                                                                        Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                                        this.LoginDatas.AccessToken,
                                                                                        this.LoginDatas.User.Id, 1);

            if (datas != null)
            {
                if (datas.ErrorCode == 0)
                {
                    datas.KioskId = kioskId.ToString();
                    datas.KioskName = kioskName;

                    this.BindListTable(datas);
                }
                else if (datas.ErrorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + datas.ErrorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetMutabakatValues");
        }
    }
}