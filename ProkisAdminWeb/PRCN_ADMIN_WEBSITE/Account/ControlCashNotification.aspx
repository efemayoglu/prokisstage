﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webctrls/AdminSiteMPage.master" AutoEventWireup="true" CodeFile="ControlCashNotification.aspx.cs" Inherits="Account_ControlCashNotification" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<%@ Register Src="../MS_Control/MultipleSelection.ascx" TagName="MultipleSelection" TagPrefix="uc1" %>
<%@ Register Namespace="ClickableWebControl" TagPrefix="clkweb" %>

 
<asp:Content ID="pageContent" ContentPlaceHolderID="mainCPHolder" runat="server">

    <input type="hidden" id="pageNumRefField" runat="server" name="pageNumRefField" value="0" />
    <asp:Button ID="navigateButton" runat="server" OnClick="navigateButton_Click" CssClass="dummy" />

     <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { setInterval(function () { cache_clear() }, 120000); });
        function cache_clear() { window.location.reload(true); }
    </script>

    <script language="javascript" type="text/javascript">



        function editEnterKioskEmptyCashButtonClicked(controlCashId) {
            var retVal = showEnterKioskEmptyCash(controlCashId);
        }

        function strtDateChngd() {
            if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                document.getElementById('startDateField').value = document.getElementById('endDateField').value;
            }

        }

        function endDateChngd() {
            if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                document.getElementById('endDateField').value = document.getElementById('startDateField').value;
            }
        }

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        //function getDailyAccountActionsClicked(itemId) {
        //    var url = "../Account/KiosCashAcceptor.aspx?itemId=" + itemId;
        //    var retValue = openDialoagWindow(url, 1000, 600, "", "Kiosk Para Alımı");
        //}

        //function getDailyCustomerAccountsClicked(itemId) {
        //    var url = "../Account/KiosCashAcceptor.aspx?itemId=" + itemId + "&isActive=1";
        //    var retValue = openDialoagWindow(url, 700, 600, "", "Kiosk Para Alımı");
        //}

        function searchButtonClicked(sender) {
            document.getElementById('pageNumRefField').value = "0";
            return true;
        }

        function showTransactionDetailClicked(transactionID) {
            var retVal = showTransactionDetailWindow(transactionID);
        }

        function postForPaging(selectedPageNum) {
            document.getElementById('pageNumRefField').value = selectedPageNum;
            document.getElementById('navigateButton').click();
        }



        function validateNo(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

    </script>

    <script type="text/javascript" language="javascript" src="../js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery.tablednd_0_5.js"></script>

    <asp:HiddenField ID="idRefField" runat="server" Value="0" />
    <asp:HiddenField ID="oldOrderNumberField" runat="server" Value="0" />
    <asp:HiddenField ID="newOrderNumberField" runat="server" Value="0" />
    <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>

    <div align="center">
        <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp;</div>
        <asp:Panel ID="panel1" DefaultButton="searchButton" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto">
            <table border="0" cellpadding="0" cellspacing="0" id="Table2" runat="server" width="90%">
                <tr>
                    <td align="center" class="inputTitle" style="width: 80px">
                        <asp:Label ID="Label2" runat="server" Visible="true">Kurum:</asp:Label></td>
                    <td style="width: 9px; height: 30px;"></td>
                    <td style="width: 102px">
                        <asp:ListBox ID="instutionBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>
                    
                    <td style="width: 6px">&nbsp;</td>

                    <td align="left" class="inputTitle" style="width: 30px; text-align: left">
                        <asp:Label ID="mercLabel" runat="server" Visible="true">Kiosk:</asp:Label></td>
                    <td style="width: 9px; height: 30px;"></td>
                    <td style="width: 9px">
                        <uc1:MultipleSelection ID="MultipleSelection1" runat="server" CssClass="inputLine_100x20" />
                    </td>


                    <td style="width: 6px">&nbsp;</td>
                    <td align="center" class="inputTitle" style="width: 150px">
                        <asp:Label ID="startDateLabel" runat="server" Visible="true">Başlangıç Zamanı:</asp:Label>
                    </td>
                    <td style="width: 9px; height: 30px;"></td>
                    <td style="width: 161px">
                        <asp:TextBox ID="startDateField" runat="server" CssClass="inputLine_150x20"
                            Width="120px"></asp:TextBox>

                        <ajaxControlToolkit:CalendarExtender ID="startDateCalendar" runat="server" TargetControlID="startDateField"
                            Format="yyyy-MM-dd HH:mm:ss" DaysModeTitleFormat="yyyy-MM-dd HH:mm:ss" Enabled="True" OnClientDateSelectionChanged="strtDateChngd">
                        </ajaxControlToolkit:CalendarExtender>
                    </td>
                       <td style="width: 6px">&nbsp;</td>
                    <td align="center" class="inputTitle" style="width: 150px">
                        <asp:Label ID="endDateLabel" runat="server" Visible="true">Bitiş Zamanı:</asp:Label>
                    </td>
                    <td style="width: 9px; height: 30px;"></td>
                    <td style="width: 161px">
                        <asp:TextBox ID="endDateField" runat="server" CssClass="inputLine_150x20"
                            Width="120px"></asp:TextBox>

                        <ajaxControlToolkit:CalendarExtender ID="endDateCalendar" runat="server" TargetControlID="endDateField"
                            Format="yyyy-MM-dd HH:mm:ss" DaysModeTitleFormat="yyyy-MM-dd HH:mm:ss" Enabled="True" OnClientDateSelectionChanged="endDateChngd">
                        </ajaxControlToolkit:CalendarExtender>
                    </td>

                    <td style="width: 6px">&nbsp;</td>

                    <td align="left" class="inputTitle" style="width: 78px;">&nbsp;Kiosk Ara:
                    </td>
                    <td align="left" style="width: 157px">
                        <asp:TextBox ID="searchTextField" CssClass="inputLine_150x20" runat="server"></asp:TextBox>
                    </td>

                    <td style="width: 15px">&nbsp;</td>

                    <td align="left" class="inputTitle" style="width: 70px; text-align: left">
                         <asp:Label ID="Label3" runat="server" Visible="true">Durum:</asp:Label></td>
                  
                    <td style="width: 102px">
                        <asp:ListBox ID="isCardDeletedBox" CssClass="inputLine_150x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>

                    <td class="inputTitle" style="width: 120px; text-align: center"> 
                        <asp:Button ID="searchButton" runat="server" ClientIDMode="Static" CssClass="buttonCSSDesign" 
                            OnClick="searchButton_Click" Text="Ara" />

                    </td>
                   <%-- <td class="inputTitle" style="width: 500px;"></td>--%>

                    <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
            </table>
        </asp:Panel>
    </div>


    <div>&nbsp;</div>
    <div align="center">
        <asp:Panel ID="ListPanel" runat="server" CssClass="containerPanel_95Pxauto">
            <div align="left" class="windowTitle_container_autox30">
                Para Dolum Kontrolü<hr style="border-bottom: 1px solid #b2b2b4;" />
            </div>
            <table cellpadding="0" cellspacing="0" width="95%" id="upTable" runat="server">
                <tr style="height: 20px">

                    <td style="width: 22px; height: 20px;">
                        <asp:ImageButton ID="excellButton" runat="server" ClientIDMode="Static" OnClick="excelButton_Click" ImageUrl="~/images/excel.jpg" />
                    </td>
                    
                    <td style="width: 200px; height: 20px;">
                        <asp:Label ID="recordInfoLabel" runat="server" Style="width: 200px; height: 20px; text-align: left" CssClass="data"></asp:Label>
                    </td>
                    <td style="height: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td style="height: 20px; width: 130px; text-align: right">
                        <asp:Label ID="Label1" Text="Listelenecek Kayıt Sayısı :  " runat="server" Style="width: 200px; height: 20px; text-align: left" CssClass="data"></asp:Label>
                    </td>
                    <td align="right" style="height: 20px; width: 33px">
                        <asp:TextBox ID="numberOfItemField" CssClass="inputLine_30x20_s" MaxLength="3" runat="server" onkeypress='validateNo(event)'></asp:TextBox></td>

                </tr>
                <tr>
                    <td align="center" colspan="6">
                        <asp:UpdatePanel ID="ListUPanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" RenderMode="Block">
                            <ContentTemplate>
                                <asp:Table ID="itemsTable" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                    BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="100%">
                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="#"></asp:TableHeaderCell>

                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kiosk Adı    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="KioskId" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kurum Adı  <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="InstitutionId" />


                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#1 / 200 TL    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="FirstDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#2 / 100 TL   <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="SecondDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#3 / 50 TL    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="ThirdDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#4 / 20 TL <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="FourthDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#5 / 10 TL   <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="FifthDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#6 / 5 TL    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="SixthDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#7 / 1 TL   <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="SeventhDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#8 / 50 Kuruş   <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="EighthDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#9 / 25 Kuruş  <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="NinthDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#10 / 10 Kuruş   <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="TenthDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#11 / 5 Kuruş <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="EleventhDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Toplam <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Sum" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Oluşturan Kullanıcı <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="CreatorUser" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="İşlem Tarihi <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="InsertionDate" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Giriş Yapan Kullanıcı <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="EnteredUser" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Giriş Yapılma Tarihi <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="EnteredDate" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Durumu <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Status" />
                                    </asp:TableRow>
                                </asp:Table>

                                <tr style="height: 20px">

                                    <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

                                </tr>


                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="navigateButton" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <br />
                    </td>
                </tr>


            </table>



        </asp:Panel>
    </div>
    <br />
    <table width="95%">
        <tr align="right">
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="width: 80px">
                <asp:Label ID="Label5" runat="server" Text="powered by" CssClass="poweredbyTitle"></asp:Label>

            </td>
            <td style="width: 100px">
                <asp:LinkButton ID="LinkButton1" href="http://www.birlesikodeme.com/" runat="server" Text="BİRLEŞİK ÖDEME" CssClass="procenneTitle"></asp:LinkButton>

            </td>
        </tr>
    </table>
</asp:Content>
