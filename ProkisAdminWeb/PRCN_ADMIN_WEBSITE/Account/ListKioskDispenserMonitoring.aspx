﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webctrls/AdminSiteMPage.master" AutoEventWireup="true" CodeFile="ListKioskDispenserMonitoring.aspx.cs" Inherits="Account_ListKioskDispenserMonitoring" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<%@ Register Namespace="ClickableWebControl" TagPrefix="clkweb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="mainCPHolder" runat="Server">
    <input type="hidden" id="pageNumRefField" runat="server" name="pageNumRefField" value="0" />
    <asp:Button ID="navigateButton" runat="server" OnClick="navigateButton_Click" CssClass="dummy" />

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { setInterval(function () { cache_clear() }, 120000); });
        function cache_clear() { window.location.reload(true); }
    </script>
    <script language="javascript" type="text/javascript">

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function searchButtonClicked(sender) {
            document.getElementById('pageNumRefField').value = "0";
            return true;
        }

        function editButtonClicked(itemID) {
            var retVal = showCreateKioskCommandWindow(itemID);
        }

        function postForPaging(selectedPageNum) {
            document.getElementById('pageNumRefField').value = selectedPageNum;
            document.getElementById('navigateButton').click();
        }

        function strtDateChngd() {
            if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                document.getElementById('startDateField').value = document.getElementById('endDateField').value;
            }

        }

        function endDateChngd() {
            if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                document.getElementById('endDateField').value = document.getElementById('startDateField').value;
            }
        }

        function validateNo(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

    </script>
    <script type="text/javascript" language="javascript" src="../js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery.tablednd_0_5.js"></script>

    <asp:HiddenField ID="idRefField" runat="server" Value="0" />
    <asp:HiddenField ID="oldOrderNumberField" runat="server" Value="0" />
    <asp:HiddenField ID="newOrderNumberField" runat="server" Value="0" />
    <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>

    <div align="center">
        <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp;</div>
        <asp:Panel ID="panel1" DefaultButton="searchButton" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto">
            <table border="0" cellpadding="0" cellspacing="0" id="Table2" runat="server" width="90%">
                <tr>
                    <td align="left" class="inputTitle" style="width: 99px">
                        <asp:Label ID="startDateLabel" runat="server" Visible="true">Başlangıç Zamanı:</asp:Label>
                    </td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px">
                        <asp:TextBox ID="startDateField" runat="server" CssClass="inputLine_100x20"></asp:TextBox>
                        <ajaxControlToolkit:CalendarExtender ID="startDateCalendar" runat="server" TargetControlID="startDateField"
                            Format="yyyy-MM-dd HH:mm:ss" DaysModeTitleFormat="yyyy-MM-dd HH:mm:ss" Enabled="True" OnClientDateSelectionChanged="strtDateChngd">
                        </ajaxControlToolkit:CalendarExtender>
                    </td>
                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 70px">
                        <asp:Label ID="endDateLabel" runat="server" Visible="true">Bitiş Zamanı:</asp:Label>
                    </td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px">
                        <asp:TextBox ID="endDateField" runat="server" CssClass="inputLine_100x20"></asp:TextBox>
                        <ajaxControlToolkit:CalendarExtender ID="endDateCalendar" runat="server" TargetControlID="endDateField"
                            Format="yyyy-MM-dd HH:mm:ss" DaysModeTitleFormat="yyyy-MM-dd HH:mm:ss" Enabled="True" OnClientDateSelectionChanged="endDateChngd">
                        </ajaxControlToolkit:CalendarExtender>
                    </td>
                    <td style="width: 18px">&nbsp;&nbsp;</td>
                   
                    <td align="left" class="inputTitle" style="width: 50px; text-align: left">
                        <asp:Label ID="Label3" runat="server" Visible="true">Kurum:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px">
                        <asp:ListBox ID="instutionBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>
                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 80px;">&nbsp;Kiosk Ara:
                    </td>
                    <td style="width: 9px">&nbsp;</td>
                    <td align="left" style="width: 130px">
                        <asp:TextBox ID="searchTextField" CssClass="inputLine_100x20" runat="server"></asp:TextBox>
                    </td>
                   
                     
                    
                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    
                    <td style="width: 86px">
                        <asp:Button ID="searchButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                            OnClick="searchButton_Click" Text="Ara"></asp:Button>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>

    <div>&nbsp;</div>
    <div align="center">
        <asp:Panel ID="ListPanel" runat="server" CssClass="containerPanel_95Pxauto">
            <div align="left" class="windowTitle_container_autox30">
                Para Üstü Takibi<hr style="border-bottom: 1px solid #b2b2b4;" />
            </div>
            <table cellpadding="0" cellspacing="0" width="95%" id="upTable" runat="server">
                <tr style="height: 20px">
                    <td style="width: 22px;">
                        <asp:ImageButton ID="excellButton" runat="server" ClientIDMode="Static" OnClick="excelButton_Click" ImageUrl="~/images/excel.jpg" />
                    </td>
                    <td style="width: 8px;">&nbsp;</td>
                    <td style="width: 200px;">
                        <asp:Label ID="recordInfoLabel" runat="server" Style="width: 200px; height: 20px; text-align: left" CssClass="data"></asp:Label>
                    </td>
                    <td style="height: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td style="height: 20px; width: 130px; text-align: right">
                        <asp:Label ID="Label1" Text="Listelenecek Kayıt Sayısı :  " runat="server" Style="width: 200px; height: 20px; text-align: left" CssClass="data"></asp:Label>
                    </td>
                    <td align="right" style="height: 20px; width: 33px">
                        <asp:TextBox ID="numberOfItemField" CssClass="inputLine_30x20_s" MaxLength="3" runat="server" onkeypress='validateNo(event)'></asp:TextBox></td>


                </tr>
                <tr>
                    <td align="center" class="tableStyle1" colspan="6">
                        <asp:UpdatePanel ID="ListUPanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" RenderMode="Block">
                            <ContentTemplate>
                                <asp:Table ID="itemsTable" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                    BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="100%">
                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="#"></asp:TableHeaderCell>

                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kiosk Adı     <img src=../images/finger.png border=0 title=Sorting /></a>"            OnTableHeaderCellClicked="Sort" ID="KioskName" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Giriş Yapan Kullanıcı <img src=../images/finger.png border=0 title=Sorting /></a>"    OnTableHeaderCellClicked="Sort" ID="CreatedUserId" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Giriş Yapılan Tarih <img src=../images/finger.png border=0 title=Sorting /></a>"      OnTableHeaderCellClicked="Sort" ID="InsertionDate" />
                                       
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#1    <img src=../images/finger.png border=0 title=Sorting /></a>"                    OnTableHeaderCellClicked="Sort" ID="FirstDenomNumber" />  
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#E_1    <img src=../images/finger.png border=0 title=Sorting /></a>"                  OnTableHeaderCellClicked="Sort" ID="FirstDenomNumberOld" />  
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="R_1    <img src=../images/finger.png border=0 title=Sorting /></a>"                   OnTableHeaderCellClicked="Sort" ID="Casette1Reject" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="E_R_1    <img src=../images/finger.png border=0 title=Sorting /></a>"                 OnTableHeaderCellClicked="Sort" ID="Casette1RejectOld" />                         
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#2    <img src=../images/finger.png border=0 title=Sorting /></a>"                    OnTableHeaderCellClicked="Sort" ID="SecondDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#E_2    <img src=../images/finger.png border=0 title=Sorting /></a>"                  OnTableHeaderCellClicked="Sort" ID="SecondDenomNumberOld" /> 
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="R_2    <img src=../images/finger.png border=0 title=Sorting /></a>"                   OnTableHeaderCellClicked="Sort" ID="Casette2Reject" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="E_R_2    <img src=../images/finger.png border=0 title=Sorting /></a>"                 OnTableHeaderCellClicked="Sort" ID="Casette2RejectOld" />                                             
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#3     <img src=../images/finger.png border=0 title=Sorting /></a>"                   OnTableHeaderCellClicked="Sort" ID="ThirdDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#E_3     <img src=../images/finger.png border=0 title=Sorting /></a>"                 OnTableHeaderCellClicked="Sort" ID="ThirdDenomNumberOld" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="R_3    <img src=../images/finger.png border=0 title=Sorting /></a>"                   OnTableHeaderCellClicked="Sort" ID="Casette3Reject" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="E_R_3    <img src=../images/finger.png border=0 title=Sorting /></a>"                 OnTableHeaderCellClicked="Sort" ID="Casette3RejectOld" />  
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#4   <img src=../images/finger.png border=0 title=Sorting /></a>"                     OnTableHeaderCellClicked="Sort" ID="FourthDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#E_4     <img src=../images/finger.png border=0 title=Sorting /></a>"                 OnTableHeaderCellClicked="Sort" ID="FourthDenomNumberOld" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="R_4    <img src=../images/finger.png border=0 title=Sorting /></a>"                   OnTableHeaderCellClicked="Sort" ID="Casette4Reject" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="E_R_4    <img src=../images/finger.png border=0 title=Sorting /></a>"                 OnTableHeaderCellClicked="Sort" ID="Casette4RejectOld" />  
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#5    <img src=../images/finger.png border=0 title=Sorting /></a>"                    OnTableHeaderCellClicked="Sort" ID="FifthDenomNumber" /> 
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#E_5     <img src=../images/finger.png border=0 title=Sorting /></a>"                 OnTableHeaderCellClicked="Sort" ID="FifthDenomNumberOld" />                                      
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#6    <img src=../images/finger.png border=0 title=Sorting /></a>"                    OnTableHeaderCellClicked="Sort" ID="SixthDenomNumber" />                                        
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#E_6    <img src=../images/finger.png border=0 title=Sorting /></a>"                  OnTableHeaderCellClicked="Sort" ID="SixthDenomNumberOld" /> 
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#7    <img src=../images/finger.png border=0 title=Sorting /></a>"                    OnTableHeaderCellClicked="Sort" ID="SeventhDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#E_7    <img src=../images/finger.png border=0 title=Sorting /></a>"                  OnTableHeaderCellClicked="Sort" ID="SeventhDenomNumberOld" />                                          
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#8    <img src=../images/finger.png border=0 title=Sorting /></a>"                    OnTableHeaderCellClicked="Sort" ID="EighthDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#E_8    <img src=../images/finger.png border=0 title=Sorting /></a>"                  OnTableHeaderCellClicked="Sort" ID="EighthDenomNumberOld" />                                        
<%--                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#9   <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="NinethDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#E_9    <img src=../images/finger.png border=0 title=Sorting /></a>"  ID="NinethDenomNumberOld" /> 
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#10   <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="TenthDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#E_10    <img src=../images/finger.png border=0 title=Sorting /></a>"  ID="TenthDenomNumberOld" /> 
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#11 <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="EleventhDenomNumber" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="#E_11    <img src=../images/finger.png border=0 title=Sorting /></a>"  ID="EleventhDenomNumberOld" /> 
                                    --%></asp:TableRow>
                                </asp:Table>
                            </ContentTemplate>
                            <Triggers>

                                <asp:AsyncPostBackTrigger ControlID="navigateButton" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <br />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>

    <br />
    <table width="95%">
        <tr align="right">
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="width: 80px">
                <asp:Label ID="Label5" runat="server" Text="powered by" CssClass="poweredbyTitle"></asp:Label>

            </td>
            <td style="width: 100px">
                <asp:LinkButton ID="LinkButton1" href="http://www.birlesikodeme.com/" runat="server" Text="BİRLEŞİK ÖDEME" CssClass="procenneTitle"></asp:LinkButton>

            </td>
        </tr>
    </table>
</asp:Content>

