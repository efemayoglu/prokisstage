﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ListFullCashBox : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/ListFullCashBox.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.startDateField.Text = DateTime.Now.AddDays(-10).ToString("yyyy-MM-dd") + " 00:00:00";
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd 00:00:00");
 
            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {

                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }



            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {

                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();

            }

            ListItem item2 = new ListItem();
            item2.Text = "Kurum Seçiniz";
            item2.Value = "0";
            this.instutionBox.Items.Add(item2);
            this.instutionBox.SelectedValue = "0";


            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;
            this.pageNum = 0;
            this.SearchOrder(4, 1);
        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }
		
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "KioskName":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                       // KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                   // KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "ExpertUser":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // ExpertUser.Text = "<a>Yetkili Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                       // ExpertUser.Text = "<a>Yetkili Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //ExpertUser.Text = "<a>Yetkili Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "TotalAmout":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // TotalAmout.Text = "<a>Para Miktarı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                       // TotalAmout.Text = "<a>Para Miktarı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                   // TotalAmout.Text = "<a>Para Miktarı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;          
            case "UpdatedDate":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                      //  UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                  //  UpdatedDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;
            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            int recordCount = 0;
            int pageCount = 0;
            CallWebServices callWebService = new CallWebServices();
            ParserListFullCashBox listFullCashBox = callWebService.CallListFullCashBoxService(kioskIds,
                                                                                            Convert.ToDateTime(this.startDateField.Text),
                                                                                            Convert.ToDateTime(this.endDateField.Text),
                                                                                            Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                                            Convert.ToInt32(this.numberOfItemField.Text),
                                                                                            this.pageNum,
                                                                                            recordCount,
                                                                                            pageCount,
                                                                                            orderSelectionColumn,
                                                                                            descAsc, 
                                                                                            this.LoginDatas.AccessToken, 
                                                                                            this.LoginDatas.User.Id,1);
            if (listFullCashBox != null)
            {
                if (listFullCashBox.errorCode == 0)
                {
                    this.BindListTable(listFullCashBox);
                }
                else if (listFullCashBox.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + listFullCashBox.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListFullCashBox");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            CallWebServices callWebService = new CallWebServices();
            ParserListFullCashBox listFullCashBox = callWebService.CallListFullCashBoxService(kioskIds,
                                                                                            Convert.ToDateTime(this.startDateField.Text),
                                                                                            Convert.ToDateTime(this.endDateField.Text),
                                                                                            Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                                            Convert.ToInt32(this.numberOfItemField.Text),
                                                                                            this.pageNum,
                                                                                            recordCount,
                                                                                            pageCount,
                                                                                            orderSelectionColumn,
                                                                                            Convert.ToInt32(ViewState["OrderDesc"]),
                                                                                            this.LoginDatas.AccessToken,
                                                                                            this.LoginDatas.User.Id,
                                                                                            2
                                                                                            );


            if (listFullCashBox != null)
            {
                if (listFullCashBox.errorCode == 0)
                {
                    for (int i = 0; i < listFullCashBox.kioskFullCashBox.Count; i++)
                    {
                        listFullCashBox.kioskFullCashBox[i].amount = listFullCashBox.kioskFullCashBox[i].amount.Replace('.', ',');
                    }


                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    //string[] headerNames = { "Kiosk Adı", "Yetkili Adı ", "Para Miktarı", "İşlem Zamanı","sad","asd","asd","qweqw"};
                    string[] headerNames = { "No", "Kiosk No ", "Kiosk Adı", "Kiosk Ip", "Yetkili Adı", "İşlem Zamanı",
                                               "Para Miktarı"};

                    exportExcell.ExportExcellByBlock(listFullCashBox.kioskFullCashBox, "Kiosk Para Doldurma Listesi", headerNames);

                   // exportExcell.ExportExcell(listFullCashBox.kioskFullCashBox, listFullCashBox.recordCount, "Kiosk Para Doldurma Listesi");
                }
                else if (listFullCashBox.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + listFullCashBox.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelParaDoldurma");
        }
    }

    private void BindListTable(ParserListFullCashBox list)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            TableRow[] Row = new TableRow[list.kioskFullCashBox.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("cashFullId", list.kioskFullCashBox[i].id.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell experUserCell = new TableCell();
                TableCell totalAmounCell = new TableCell();
                TableCell updatedDateCell = new TableCell();
                TableCell detailsCell = new TableCell();

                TableCell fakeCell1 = new TableCell();
                TableCell fakeCell2 = new TableCell();
                TableCell fakeCell3 = new TableCell();
                TableCell fakeCell4 = new TableCell();

                fakeCell1.CssClass = "inputTitleCell4";
                fakeCell2.CssClass = "inputTitleCell4";
                fakeCell3.CssClass = "inputTitleCell4";
                fakeCell4.CssClass = "inputTitleCell4";

                fakeCell1.Visible = false;
                fakeCell2.Visible = false;
                fakeCell3.Visible = false;
                fakeCell4.Visible = false;
                idCell.Visible = false;

                indexCell.CssClass = "inputTitleCell4";
                idCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                experUserCell.CssClass = "inputTitleCell4";
                totalAmounCell.CssClass = "inputTitleCell4";
                updatedDateCell.CssClass = "inputTitleCell4";
                detailsCell.CssClass = "inputTitleCell4";
                indexCell.Text = (startIndex + i).ToString();
                //idCell.Text = listKioskMoney.kioskMoney[i].kioskId.ToString();
                kioskNameCell.Text = list.kioskFullCashBox[i].kioskName;
                experUserCell.Text = list.kioskFullCashBox[i].expertUserName;
                totalAmounCell.Text = list.kioskFullCashBox[i].amount + "  TL";
                updatedDateCell.Text = list.kioskFullCashBox[i].updatedDate;

                detailsCell.Width = Unit.Pixel(30);
                detailsCell.Text = "<a href=# onClick=\"showKioskFullCashBoxCountClicked(" + list.kioskFullCashBox[i].id + ");\" class=\"detailCodeButton\" Title=\"Para Adetleri Göster\"></a>";

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        idCell,
                        fakeCell1,
                        fakeCell2,
                        fakeCell3,
                        fakeCell4,
						kioskNameCell,
                        experUserCell,
                        totalAmounCell,
                        updatedDateCell,
                        detailsCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (list.recordCount < currentRecordEnd)
                currentRecordEnd = list.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + list.recordCount.ToString() + " kayıt bulundu.";
            }

            TableRow addNewRow = new TableRow();
            TableCell totalAmountCell = new TableCell();
            TableCell spaceCell = new TableCell();
            TableCell spaceCell1 = new TableCell();

            spaceCell.CssClass = "inputTitleCell99";
            spaceCell.ColumnSpan = (3);
            spaceCell1.CssClass = "inputTitleCell99";
            spaceCell1.ColumnSpan = (2);
            totalAmountCell.CssClass = "inputTitleCell99";

            spaceCell.Text = "Toplam";
            totalAmountCell.Text = list.totalAmount + " TL";
            spaceCell1.Text = "";

            addNewRow.Cells.Add(spaceCell);
            addNewRow.Cells.Add(totalAmountCell);
            addNewRow.Cells.Add(spaceCell1);
            this.itemsTable.Rows.Add(addNewRow);

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(list.pageCount, this.pageNum, list.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskCashEmptyBDT");
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(2, 1);
    }

}