﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Account;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ListKioskCashDispenser : System.Web.UI.Page
{

    private PRCNCORE.Parser.ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 2;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/ListKioskCashDispenser.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);
        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {

                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }

            this.startDateField.Text = DateTime.Now.AddDays(-2).ToString("yyyy-MM-dd") + " 00:00:00"; ;
            this.endDateField.Text = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd") + " 00:00:00";

            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {

                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();

            }

            ListItem item = new ListItem();
            item.Text = "Kurum Seçiniz";
            item.Value = "0";
            this.instutionBox.Items.Add(item);
            this.instutionBox.SelectedValue = "0";


            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;
            this.pageNum = 0;
            this.SearchOrder(2, 1);

        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }

    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(1, 1);
    }

    private void BindListTable(ParserListKioskCashDispenser items)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            TableRow[] Row = new TableRow[items.ParserKioskCashDispenser.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();

                //Row[i].Attributes.Add("Id", items.ParserKioskCashDispenser[i].KioskId.ToString());

                TableCell indexCell = new TableCell();
                TableCell kioskIdCell = new TableCell();
                TableCell institutionId = new TableCell();
                TableCell insertionDate = new TableCell();

                TableCell value1Cell = new TableCell();
                TableCell amount1Cell = new TableCell();
                TableCell value2Cell = new TableCell();
                TableCell amount2Cell = new TableCell();
                TableCell value3Cell = new TableCell();
                TableCell amount3Cell = new TableCell();
                TableCell value4Cell = new TableCell();
                TableCell amount4Cell = new TableCell();
                TableCell value5Cell = new TableCell();
                TableCell amount5Cell = new TableCell();
                TableCell value6Cell = new TableCell();
                TableCell amount6Cell = new TableCell();
                TableCell value7Cell = new TableCell();
                TableCell amount7Cell = new TableCell();
                TableCell value8Cell = new TableCell();
                TableCell amount8Cell = new TableCell();
                TableCell value9Cell = new TableCell();
                TableCell amount9Cell = new TableCell();
                TableCell value10Cell = new TableCell();
                TableCell amount10Cell = new TableCell();
                TableCell value11Cell = new TableCell();
                TableCell amount11Cell = new TableCell();

                TableCell sumCell = new TableCell();

                indexCell.CssClass = "inputTitleCell4";
                kioskIdCell.CssClass = "inputTitleCell4";
                institutionId.CssClass = "inputTitleCell4";
                insertionDate.CssClass = "inputTitleCell4";

                value1Cell.CssClass = "inputTitleCell4";
                amount1Cell.CssClass = "inputTitleCell4";
                value2Cell.CssClass = "inputTitleCell4";
                amount2Cell.CssClass = "inputTitleCell4";
                value3Cell.CssClass = "inputTitleCell4";
                amount3Cell.CssClass = "inputTitleCell4";
                value4Cell.CssClass = "inputTitleCell4";
                amount4Cell.CssClass = "inputTitleCell4";
                value5Cell.CssClass = "inputTitleCell4";
                amount5Cell.CssClass = "inputTitleCell4";
                value6Cell.CssClass = "inputTitleCell4";
                amount6Cell.CssClass = "inputTitleCell4";
                value7Cell.CssClass = "inputTitleCell4";
                amount7Cell.CssClass = "inputTitleCell4";
                value8Cell.CssClass = "inputTitleCell4";
                amount8Cell.CssClass = "inputTitleCell4";
                value9Cell.CssClass = "inputTitleCell4";
                amount9Cell.CssClass = "inputTitleCell4";
                value10Cell.CssClass = "inputTitleCell4";
                amount10Cell.CssClass = "inputTitleCell4";
                value11Cell.CssClass = "inputTitleCell4";
                amount11Cell.CssClass = "inputTitleCell4";

                sumCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();

                kioskIdCell.Text = items.ParserKioskCashDispenser[i].kioskId.ToString();
                institutionId.Text = items.ParserKioskCashDispenser[i].institutionId.ToString();
                insertionDate.Text = items.ParserKioskCashDispenser[i].insertionDate;

                value1Cell.Text = items.ParserKioskCashDispenser[i].firstDenomValue;
                amount1Cell.Text = items.ParserKioskCashDispenser[i].firstDenomNumber;
                value2Cell.Text = items.ParserKioskCashDispenser[i].secondDenomValue;
                amount2Cell.Text = items.ParserKioskCashDispenser[i].secondDenomNumber;
                value3Cell.Text = items.ParserKioskCashDispenser[i].thirdDenomValue;
                amount3Cell.Text = items.ParserKioskCashDispenser[i].thirdDenomNumber;
                value4Cell.Text = items.ParserKioskCashDispenser[i].fourthDenomValue;
                amount4Cell.Text = items.ParserKioskCashDispenser[i].fourthDenomNumber;
                value5Cell.Text = items.ParserKioskCashDispenser[i].fifthDenomValue;
                amount5Cell.Text = items.ParserKioskCashDispenser[i].fifthDenomNumber;
                value6Cell.Text = items.ParserKioskCashDispenser[i].sixthDenomValue;
                amount6Cell.Text = items.ParserKioskCashDispenser[i].sixthDenomNumber;
                value7Cell.Text = items.ParserKioskCashDispenser[i].seventhDenomValue;
                amount7Cell.Text = items.ParserKioskCashDispenser[i].seventhDenomNumber;
                value8Cell.Text = items.ParserKioskCashDispenser[i].eighthDenomValue;
                amount8Cell.Text = items.ParserKioskCashDispenser[i].eighthDenomNumber;
                value9Cell.Text = items.ParserKioskCashDispenser[i].ninthDenomValue;
                amount9Cell.Text = items.ParserKioskCashDispenser[i].ninthDenomNumber;
                value10Cell.Text = items.ParserKioskCashDispenser[i].tenthDenomValue;
                amount10Cell.Text = items.ParserKioskCashDispenser[i].tenthDenomNumber;
                value11Cell.Text = items.ParserKioskCashDispenser[i].eleventhDenomValue;
                amount11Cell.Text = items.ParserKioskCashDispenser[i].eleventhDenomNumber;

                sumCell.Text = ((Convert.ToDouble(value1Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount1Cell.Text.Replace(',', '.'))) +
                             (Convert.ToDouble(value2Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount2Cell.Text.Replace(',', '.'))) +
                             (Convert.ToDouble(value3Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount3Cell.Text.Replace(',', '.'))) +
                             (Convert.ToDouble(value4Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount4Cell.Text.Replace(',', '.'))) +
                             (Convert.ToDouble(value5Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount5Cell.Text.Replace(',', '.'))) +
                             (Convert.ToDouble(value6Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount6Cell.Text.Replace(',', '.'))) +
                             (Convert.ToDouble(value7Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount7Cell.Text.Replace(',', '.'))) +
                             (Convert.ToDouble(value8Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount8Cell.Text.Replace(',', '.'))) +
                             (Convert.ToDouble(value9Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount9Cell.Text.Replace(',', '.'))) +
                             (Convert.ToDouble(value10Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount10Cell.Text.Replace(',', '.'))) +
                             (Convert.ToDouble(value11Cell.Text.Replace(',', '.')) * Convert.ToDouble(amount11Cell.Text.Replace(',', '.')))
                             ).ToString() + " ₺";


                value1Cell.Text += " ₺";
                value2Cell.Text += " ₺";
                value3Cell.Text += " ₺";
                value4Cell.Text += " ₺";
                value5Cell.Text += " ₺";
                value6Cell.Text += " ₺";
                value7Cell.Text += " ₺";
                value8Cell.Text += " ₺";
                value9Cell.Text += " ₺";
                value10Cell.Text += " ₺";
                value11Cell.Text += " ₺";

                Row[i].Cells.AddRange(new TableCell[]{
						   indexCell,
						kioskIdCell,
                        institutionId,
                        insertionDate,
                        value1Cell,
                        amount1Cell,
                         value2Cell,
                        amount2Cell,
                        value3Cell,
                        amount3Cell,
                        value4Cell,
                        amount4Cell,
                        value5Cell,
                        amount5Cell,
                        value6Cell,
                        amount6Cell,
                        value7Cell,
                        amount7Cell,
                        value8Cell,
                        amount8Cell,
                        value9Cell,
                        amount9Cell,
                        value10Cell,
                        amount10Cell,
                        value11Cell,
                        amount11Cell,
                        sumCell
					});

                if (i == Row.Length - 1)
                {
                    Row[i].CssClass = "inputTitleCell99";
                    Row[i].Cells[1].Text = "TOPLAM";
                    Row[i].Cells[0].Text = "";
                }
                else
                {
                    if (i % 2 == 0)
                        Row[i].CssClass = "listrow";
                    else
                        Row[i].CssClass = "listRowAlternate";
                }
            }

            this.itemsTable.Rows.AddRange(Row);

            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (items.recordCount < currentRecordEnd)
                currentRecordEnd = items.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + items.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(items.pageCount, this.pageNum, items.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListMutabakatBDT");
        }
    }



    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;
        ClickableWebControl.ClickableTableHeaderCell Header = ((ClickableWebControl.ClickableTableHeaderCell)sender);
        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "KioskId":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Header.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";

                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  Header.Text = "<a>Kiosk Adı <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  Header.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "InstitutionId":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //InstitutionId.Text = "<a>Kurum Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //InstitutionId.Text = "<a>Kurum Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // InstitutionId.Text = "<a>Kurum Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "InsertionDate":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // InsertionDate.Text = "<a>İşlem Tarihi <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //InsertionDate.Text = "<a>İşlem Tarihi <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  InsertionDate.Text = "<a>İşlem Tarihi     <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "FirstDenomValue":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // FirstDenomValue.Text = "<a>V1    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  FirstDenomValue.Text = "<a>V1    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //FirstDenomValue.Text = "<a>V1    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "FirstDenomNumber":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // FirstDenomNumber.Text = "<a>#1 <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //FirstDenomNumber.Text = "<a>#1   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // FirstDenomNumber.Text = "<a>#1    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "SecondDenomValue":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SecondDenomValue.Text = "<a>V2    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // SecondDenomValue.Text = "<a>V2    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  SecondDenomValue.Text = "<a>V2    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "SecondDenomNumber":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SecondDenomNumber.Text = "<a>#2    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // SecondDenomNumber.Text = "<a>#2    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // SecondDenomNumber.Text = "<a>#2     <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "ThirdDenomValue":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // ThirdDenomValue.Text = "<a>V3    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // ThirdDenomValue.Text = "<a>V3    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // ThirdDenomValue.Text = "<a>V3   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "ThirdDenomNumber":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // ThirdDenomNumber.Text = "<a>#3    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // ThirdDenomNumber.Text = "<a>#3    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // ThirdDenomNumber.Text = "<a>#3   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "FourthDenomValue":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // FourthDenomValue.Text = "<a>V4    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //   FourthDenomValue.Text = "<a>V4    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //FourthDenomValue.Text = "<a>V4   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "FourthDenomNumber":
                if (orderSelectionColumn == 11)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // FourthDenomNumber.Text = "<a>#4   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // FourthDenomNumber.Text = "<a>#4    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // FourthDenomNumber.Text = "<a>#4   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 11;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "FifthDenomValue":
                if (orderSelectionColumn == 12)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  FifthDenomValue.Text = "<a>V5   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // FifthDenomValue.Text = "<a>V5    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // FifthDenomValue.Text = "<a>V5   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 12;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "FifthDenomNumber":
                if (orderSelectionColumn == 13)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  FifthDenomNumber.Text = "<a>#5   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  FifthDenomNumber.Text = "<a>#5    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // FifthDenomNumber.Text = "<a>#5   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 13;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "SixthDenomValue":
                if (orderSelectionColumn == 14)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SixthDenomValue.Text = "<a>V6   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  SixthDenomValue.Text = "<a>V6    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // SixthDenomValue.Text = "<a>V6   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 14;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "SixthDenomNumber":
                if (orderSelectionColumn == 15)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //SixthDenomNumber.Text = "#6<a>   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // SixthDenomNumber.Text = "#6<a>  <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //SixthDenomNumber.Text = "#6<a>   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 15;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "SeventhDenomValue":
                if (orderSelectionColumn == 16)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SeventhDenomValue.Text = "<a>V7   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  SeventhDenomValue.Text = "<a>V7    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // SeventhDenomValue.Text = "<a>V7   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 16;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "SeventhDenomNumber":
                if (orderSelectionColumn == 17)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  SeventhDenomNumber.Text = "<a>#7   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //SeventhDenomNumber.Text = "<a>#7   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //   SeventhDenomNumber.Text = "<a>#7   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 17;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "EighthDenomValue":
                if (orderSelectionColumn == 18)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  EighthDenomValue.Text = "<a>V7   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // EighthDenomValue.Text = "<a>V7    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // EighthDenomValue.Text = "<a>V7   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 18;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "EighthDenomNumber":
                if (orderSelectionColumn == 19)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // EighthDenomNumber.Text = "<a>#8   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // EighthDenomNumber.Text = "<a>#8   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //EighthDenomNumber.Text = "<a>#8   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 19;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "NinethDenomValue":
                if (orderSelectionColumn == 20)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //NinethDenomValue.Text = "<a>V9   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //NinethDenomValue.Text = "<a>V9    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //NinethDenomValue.Text = "<a>V9   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 20;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "NinethDenomNumber":
                if (orderSelectionColumn == 21)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //NinethDenomNumber.Text = "<a>#9   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //NinethDenomNumber.Text = "<a>#9   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // NinethDenomNumber.Text = "<a>#9   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 21;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "TenthDenomValue":
                if (orderSelectionColumn == 22)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // TenthDenomValue.Text = "<a>V10   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // TenthDenomValue.Text = "<a>V10    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TenthDenomValue.Text = "<a>V10   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 22;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "TenthDenomNumber":
                if (orderSelectionColumn == 23)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // TenthDenomNumber.Text = "<a>#10   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TenthDenomNumber.Text = "<a>#10   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TenthDenomNumber.Text = "<a>#10   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 23;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "EleventhDenomValue":
                if (orderSelectionColumn == 24)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //EleventhDenomValue.Text = "<a>V11   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //EleventhDenomValue.Text = "<a>V11    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //EleventhDenomValue.Text = "<a>V11   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 24;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "EleventhDenomNumber":
                if (orderSelectionColumn == 25)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //EleventhDenomNumber.Text = "<a>#11   <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // EleventhDenomNumber.Text = "<a>#11   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    ///EleventhDenomNumber.Text = "<a>#11  <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 25;
                    orderSelectionDescAsc = 1;
                }
                break;
            /////////////////////////////////

            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }



    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }


    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebService = new CallWebServices();
            ParserListKioskCashDispenser items = callWebService.CallListKioskCashDispenserService(
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text).AddDays(0).AddSeconds(-1),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             kioskIds,
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             1);

            if (items != null)
            {
                if (items.errorCode == 0)
                {
                    this.BindListTable(items);
                }
                else if (items.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + items.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskCashSrch");
        }
    }


    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {

            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebService = new CallWebServices();
            ParserListKioskCashDispenser lists = callWebService.CallListKioskCashDispenserService(
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text).AddDays(0).AddSeconds(-1),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             Convert.ToInt32(ViewState["OrderDesc"]),
                                                                             kioskIds,
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             2);

           
 
           
            if (lists != null)
            {
                if (lists.errorCode == 0)
                {

                    //for (int i = 0; i < items.ParserKioskCashDispenser.Count; i++)
                    //{
                    //    items.ParserKioskCashDispenser[i].GeneratedTodayUsableToday = items.ParserKioskCashDispenser[i].GeneratedTodayUsableToday.Replace('.', ',');
                    //    items.ParserKioskCashDispenser[i].GeneratedTodayUsedToday = items.ParserKioskCashDispenser[i].GeneratedTodayUsedToday.Replace('.', ',');
                    //    //lists.KioskReports[i].KioskEmptyAmount = lists.KioskReports[i].TotalAmount.Replace('.', ',');
                    //    items.ParserKioskCashDispenser[i].GeneratedTotal = items.ParserKioskCashDispenser[i].GeneratedTotal.Replace('.', ',');
                    //    items.ParserKioskCashDispenser[i].GeneratedYesterdayUsedToday = items.ParserKioskCashDispenser[i].GeneratedYesterdayUsedToday.Replace('.', ',');
                    //    items.ParserKioskCashDispenser[i].GeneratedYesterdayUsedTodayCount = items.ParserKioskCashDispenser[i].GeneratedYesterdayUsedTodayCount.Replace('.', ',');
                    //    items.ParserKioskCashDispenser[i].RemainUsableCode = items.ParserKioskCashDispenser[i].RemainUsableCode.Replace('.', ',');
                    //    items.ParserKioskCashDispenser[i].UsedTotalToday = items.ParserKioskCashDispenser[i].UsedTotalToday.Replace('.', ',');
                    //    items.ParserKioskCashDispenser[i].UsedTotalToday = items.ParserKioskCashDispenser[i].UsageFee.Replace('.', ',');
                    //    items.ParserKioskCashDispenser[i].UsedTotalToday = items.ParserKioskCashDispenser[i].InstitutionAmount.Replace('.', ',');

                    //}
                    ExportExcellDatas exportExcell = new ExportExcellDatas();

                    string[] headerNames = { "Kiosk Adı", "Kiosk No", "Kurum Adı", "Ekleme Tarihi", "İlk Değer", "İlk No",
                                             "İkinci Değer", "İkinci No", "Üçüncü Değer", "Üçüncü No", "Dördüncü Değer", "Dördüncü No", "Beşinci Değer", "Beşinci No"
                                           , "Altıncı Değer", "Altıncı No",  "Yedinci Değer", "Yedinci No",  "Sekizinci Değer", "Sekizinci No", "Dokuzuncu Değer", "Dokuzuncu No",
                                             "Onuncu Değer", "Onuncu No", "Onbirinci Değer", "Onbirinci No"};

                    exportExcell.ExportExcellByBlock(lists.ParserKioskCashDispenser,"Kiosk Dispenser (End of Day)", headerNames);
                }
                else if (lists.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + lists.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelKioskCashDispenser");
        }
    }
}