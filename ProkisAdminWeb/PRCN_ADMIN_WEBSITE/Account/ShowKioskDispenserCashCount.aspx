﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowKioskDispenserCashCount.aspx.cs" Inherits="Account_ShowKioskDispenserCashCount" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />


    <input type="hidden" id="hiddenId" runat="server" name="hiddenId" value="0" />
    <script language="javascript" type="text/javascript">

        //function callOwn() {
        //    alert("User has been saved successfully!");
        //    window.close();
        //    window.parent.location = "../Kiosk/ListKiosk.aspx";
        //}

        function checkForm() {
            var status = true;
            var messageText = "";


            if (document.getElementById('kioskBox').value == "0") {
                status = false;
                messageText = "Kiosk Seçiniz !";
            }
            else {
                messageText = "";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }


        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        //function validateIP(evt) {
        //    var theEvent = evt || window.event;
        //    var key = theEvent.keyCode || theEvent.which;
        //    key = String.fromCharCode(key);
        //    var regex = /[0-9]|\./;
        //    if (!regex.test(key)) {
        //        theEvent.returnValue = false;
        //        if (theEvent.preventDefault) theEvent.preventDefault();
        //    }
        //}

        //function validateNo(evt) {
        //    var theEvent = evt || window.event;
        //    var key = theEvent.keyCode || theEvent.which;
        //    key = String.fromCharCode(key);
        //    var regex = /[0-9]/;
        //    if (!regex.test(key)) {
        //        theEvent.returnValue = false;
        //        if (theEvent.preventDefault) theEvent.preventDefault();
        //    }
        //}

        //function ValidateIPaddress(inputText) {
        //    var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
        //    if (inputText.match(ipformat)) {
        //        return true;
        //    }
        //    else {
        //        return false;
        //    }
        //}

        //function validateDecimal(evt) {
        //    var theEvent = evt || window.event;
        //    var key = theEvent.keyCode || theEvent.which;
        //    key = String.fromCharCode(key);
        //    var regex = /[0-9]|\.|\,/;
        //    if (!regex.test(key)) {
        //        theEvent.returnValue = false;
        //        if (theEvent.preventDefault) theEvent.preventDefault();
        //    }
        //}

        //function ValidateDecimalNumber(inputText) {
        //    if (document.getElementById('amountTextBox').value == '')
        //        return true;
        //    var ipformat = /^[0-9]+((\.[0-9]{1,2})|(\,[0-9]{1,2}))?$/;
        //    if (inputText.match(ipformat)) {
        //        return true;
        //    }
        //    else {
        //        return false;
        //    }
        //}
    </script>

    <base target="_self" />
    <style type="text/css">
        .auto-style1 {
            height: 20px;
        }

        .auto-style14 {
            font-family: Tahoma;
            font-size: 12px;
            font-weight: bold;
            width: 190px;
            height: 20px;
            border-radius: 5px;
            border: 1px solid #2372BE;
            padding-bottom: 5px;
        }
    </style>

</head>


<body>

    <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>


        <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel7" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <div align="left" id="articleDetailsTab" class="windowTitle_container_autox30">
                    Kiosk Para Üstü Bildirim Kontrolleri<hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>
                <table border="0" cellpadding="2" cellspacing="0" id="Table1" runat="server">
                    <tr>
                        <td align="center" class="inputTitleCell2" height="25" colspan="2">Kiosk:
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" height="30">
                            <asp:Label ID="nameField" MaxLength="100" runat="server"></asp:Label>
                        </td>
                    </tr>


                    <tr>
                        <td align="center" class="inputTitleCell2" height="25" colspan="2">Toplam Tutar :
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" height="30">

                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="TotalAmountTb" runat="server" CssClass="inputLine_375x20" Style="text-align: center;" Enabled="false"></asp:TextBox>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="CashYpeTB1" EventName="TextChanged" />
                                    <%-- <asp:AsyncPostBackTrigger ControlID="CashYpeTB10" EventName="TextChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="CashYpeTB11" EventName="TextChanged" />--%>
                                    <asp:AsyncPostBackTrigger ControlID="CashYpeTB2" EventName="TextChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="CashYpeTB3" EventName="TextChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="CashYpeTB4" EventName="TextChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="CashYpeTB5" EventName="TextChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="CashYpeTB6" EventName="TextChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="CashYpeTB7" EventName="TextChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="CashYpeTB8" EventName="TextChanged" />
<%--                                    <asp:AsyncPostBackTrigger ControlID="CashYpeTB9" EventName="TextChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="CashYpeTB10" EventName="TextChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="CashYpeTB11" EventName="TextChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="CashYpeTB12" EventName="TextChanged" />--%>
                                    <%--<asp:AsyncPostBackTrigger ControlID="CashYpeTB9" EventName="TextChanged" />--%>
                                </Triggers>
                            </asp:UpdatePanel>

                        </td>
                    </tr>

                    <tr valign="middle">
                       <td align="left" class="staticTextLine_200x20">
                             <asp:Label ID="Label1" runat="server" ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="CashYpeTB1" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                        </td>

                    </tr>

                    <tr valign="middle">
                       <td align="left" class="staticTextLine_200x20">
                             <asp:Label ID="Label2" runat="server" ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="CashYpeTB2" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                     <td align="left" class="staticTextLine_200x20">
                             <asp:Label ID="Label3" runat="server" ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="CashYpeTB3" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                      <td align="left" class="staticTextLine_200x20">
                             <asp:Label ID="Label4" runat="server" ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="CashYpeTB4" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">
                             <asp:Label ID="Label5" runat="server" ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="CashYpeTB5" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                        </td>
                    </tr>
                    <tr valign="middle">
                       <td align="left" class="staticTextLine_200x20">
                             <asp:Label ID="Label6" runat="server" ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="CashYpeTB6" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                        </td>
                    </tr>
                    <tr valign="middle">
                       <td align="left" class="staticTextLine_200x20">
                             <asp:Label ID="Label7" runat="server" ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="CashYpeTB7" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">
                             <asp:Label ID="Label8" runat="server" ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="CashYpeTB8" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                        </td>
                    </tr>
                    <%--<tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">25 Krş:
                        </td>
                        <td>
                            <asp:TextBox ID="CashYpeTB9" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">10 Krş:
                        </td>
                        <td>
                            <asp:TextBox ID="CashYpeTB10" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">5 Krş:
                        </td>
                        <td>
                            <asp:TextBox ID="CashYpeTB11" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                        </td>
                    </tr>--%>

                    <tr style="margin:15px">
                        <td align="center" class="inputTitleCell2" height="20" colspan="2" >
                            Reject Bilgisi:
                        </td>
                    </tr>

                  
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Reject 1:
                        </td>
                        <td>
                            <asp:TextBox ID="CashYpeTB9" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Reject 2:
                        </td>
                        <td>
                            <asp:TextBox ID="CashYpeTB10" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Reject 3:
                        </td>
                        <td>
                            <asp:TextBox ID="CashYpeTB11" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Reject 4:
                        </td>
                        <td>
                            <asp:TextBox ID="CashYpeTB12" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                        </td>
                    </tr>


                </table>
                <br />
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Kaydet"></asp:Button>

                                <asp:Button ID="cancelButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClientClick="parent.hideModalPopup2_Exit();" Text="Vazgeç"></asp:Button>

                            </td>
                        </tr>
                    </table>
                </td>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
