﻿<%@ Page Language="C#" CodeFile="ListBankActions.aspx.cs" AutoEventWireup="true" Inherits="Account_ListBankActions" MasterPageFile="~/webctrls/AdminSiteMPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<%@ Register Src="../MS_Control/MultipleSelection.ascx" TagName="MultipleSelection" TagPrefix="uc1" %>
<%@ Register Namespace="ClickableWebControl" TagPrefix="clkweb" %>
<asp:Content ID="pageContent" ContentPlaceHolderID="mainCPHolder" runat="server">
    <input type="hidden" id="pageNumRefField" runat="server" name="pageNumRefField" value="0" />
    <asp:Button ID="navigateButton" runat="server" OnClick="navigateButton_Click" CssClass="dummy" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { setInterval(function () { cache_clear() }, 120000); });
        function cache_clear() { window.location.reload(true); }
    </script>
    <script language="javascript" type="text/javascript">

        function openNewTab(url) {
            var win = window.open("../Transaction/ListTransactionCondition.aspx?tranId=" + url, '_blank');
        }

        function openNewTabMoneyCode(url) {
            var win = window.open("../AskiCode/ListAskiCodes.aspx?tranId=" + url, '_blank');
        }

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function generateAskiCodeClicked(transactionId) {
            var url = "../AskiCode/GenerateAskiCode.aspx?itemID=" + transactionId;
            var retValue = openDialoagWindow(url, 350, 450, "", "Para Kod Üretme");
        }

        function searchButtonClicked(sender) {
            document.getElementById('pageNumRefField').value = "0";
            return true;
        }

        function showTransactionDetailClicked(transactionID) {
            var retVal = showTransactionDetailWindow(transactionID);
        }

        function editButtonClicked(itemID) {
            var retVal = showKioskWindow(itemID);
        }

        function postForPaging(selectedPageNum) {
            document.getElementById('pageNumRefField').value = selectedPageNum;
            document.getElementById('navigateButton').click();
        }

        function strtDateChngd() {
            if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                document.getElementById('startDateField').value = document.getElementById('endDateField').value;
            }

        }

        function endDateChngd() {
            if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                document.getElementById('endDateField').value = document.getElementById('startDateField').value;
            }
        }

        function validateNo(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
    <script type="text/javascript" language="javascript" src="../js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery.tablednd_0_5.js"></script>

    <asp:HiddenField ID="idRefField" runat="server" Value="0" />
    <asp:HiddenField ID="oldOrderNumberField" runat="server" Value="0" />
    <asp:HiddenField ID="newOrderNumberField" runat="server" Value="0" />
    <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>

    <div align="center">
        <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp;</div>
        <asp:Panel ID="panel1" DefaultButton="searchButton" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto">
            <table border="0" cellpadding="0" cellspacing="0" id="Table2" runat="server" width="100%">
                <tr>
                    <td style="width: 6px">&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 99px">
                        <asp:Label ID="startDateLabel" runat="server" Visible="true">Başlangıç Zamanı:</asp:Label>
                    </td>
                    <td style="width: 159px">
                        <asp:TextBox ID="startDateField" runat="server" CssClass="inputLine_150x20"
                            Width="130px"></asp:TextBox>

                        <ajaxControlToolkit:CalendarExtender ID="startDateCalendar" runat="server" TargetControlID="startDateField"
                            Format="yyyy-MM-dd HH:mm:ss" Enabled="True" DaysModeTitleFormat="yyyy-MM-dd HH:mm:ss" OnClientDateSelectionChanged="strtDateChngd">
                        </ajaxControlToolkit:CalendarExtender>
                    </td>

                    <td align="left" class="inputTitle" style="width: 70px">
                        <asp:Label ID="endDateLabel" runat="server" Visible="true">Bitiş Zamanı:</asp:Label>
                    </td>
                    <td style="width: 161px">
                        <asp:TextBox ID="endDateField" runat="server" CssClass="inputLine_150x20"
                            Width="130px"></asp:TextBox>

                        <ajaxControlToolkit:CalendarExtender ID="endDateCalendar" runat="server" TargetControlID="endDateField"
                            Format="yyyy-MM-dd HH:mm:ss" Enabled="True" DaysModeTitleFormat="yyyy-MM-dd HH:mm:ss" OnClientDateSelectionChanged="endDateChngd">
                        </ajaxControlToolkit:CalendarExtender>
                    </td>
                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 50px; text-align: left">
                        <asp:Label ID="Label2" runat="server" Visible="true">Kurum:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px">
                        <asp:ListBox ID="instutionBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>
                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 30px; text-align: left">
                        <asp:Label ID="mercLabel" runat="server" Visible="true">Kiosk:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px">
                        <uc1:MultipleSelection ID="MultipleSelection1" runat="server" CssClass="inputLine_100x20" />
                    </td>
                                        <td style="width: 6px">&nbsp;</td>
                    <td style="width: 86px">
                        <asp:Button ID="searchButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                            OnClick="searchButton_Click" Text="Ara"></asp:Button>
                    </td>
                    <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 6px">&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 99px; text-align: left">
                        <asp:Label ID="Label4" runat="server" Visible="true">İşlem Durumu:</asp:Label></td>
                    <td style="width: 159px">
                        <asp:ListBox ID="TransactionStatusBox" CssClass="inputLine_150x20" Width="130px" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>
                    <td align="left" class="inputTitle" style="width: 70px; text-align: left">
                        <asp:Label ID="Label3" runat="server" Visible="true">İşlem Tipi:</asp:Label></td>
                    <td style="width: 161px">
                        <asp:ListBox ID="operationTypeBox" CssClass="inputLine_150x20" Width="130px" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>

                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 50px; text-align: left">&nbsp;İşlem Ara:
                    </td>
                    <td style="width: 9px">&nbsp;</td>
                    <td align="left" style="width: 102px">
                        <asp:TextBox ID="searchTextField" CssClass="inputLine_100x20" runat="server"></asp:TextBox>
                    </td>


                </tr>
            </table>
        </asp:Panel>
    </div>
    <div>&nbsp;</div>
    <div align="center">
        <asp:Panel ID="ListPanel" runat="server" CssClass="containerPanel_95Pxauto">
            <div align="left" class="windowTitle_container_autox30">
                İşlemler<hr style="border-bottom: 1px solid #b2b2b4;" />
            </div>
            <asp:UpdatePanel ID="ListUPanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" RenderMode="Block">
                <ContentTemplate>
                    <table cellpadding="0" cellspacing="0" width="95%" id="upTable" runat="server">
                        <tr>
                            <td style="width: 22px; height: 20px;">
                                <asp:ImageButton ID="excellButton" runat="server" ClientIDMode="Static" OnClick="excelButton_Click" ImageUrl="~/images/excel.jpg" />
                            </td>
                            <td style="width: 8px; height: 20px;"></td>
                            <td style="width: 200px; height: 20px;">
                                <asp:Label ID="recordInfoLabel" runat="server" Style="width: 200px; height: 20px; text-align: left" CssClass="data"></asp:Label>
                            </td>
                            <td style="height: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td style="height: 20px; width: 130px; text-align: right">
                                <asp:Label ID="Label1" Text="Listelenecek Kayıt Sayısı :  " runat="server" Style="width: 200px; height: 20px; text-align: left" CssClass="data"></asp:Label>
                            </td>
                            <td align="right" style="height: 20px; width: 33px">
                                <asp:TextBox ID="numberOfItemField" CssClass="inputLine_30x20_s" MaxLength="3" runat="server" onkeypress='validateNo(event)'></asp:TextBox></td>

                        </tr>
                        <tr>
                            <td align="center" class="tableStyle1" colspan="6">


                                <asp:Table ID="itemsTable" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                    BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="100%">
                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="#"></asp:TableHeaderCell>
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCellLightBlue" Text="#K-Onay    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="InstitutionApproved" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCellLightBlue" Text="#B-Onay    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="BOSApproved" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="K-Onay Tutar    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="InstitutionApprovedAmount" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="B-Onay Tutar    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="BOSApprovedAmount" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCellLightBlue" Text="#K-Iptal    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="InstitutionCanceled" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCellLightBlue" Text="#B-Iptal    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="BOSCanceled" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="K-Iptal Tutar    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="InstitutionCanceledAmount" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="B-Iptal Tutar    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="BOSCanceledAmount" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCellLightBlue" Text="Hesap    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Account" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="işlem Zamanı    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="InsertionDate" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Manuel Mutabakat    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="ManuelUser" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Manuel İşlem Zamanı    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="ManuelApproveDate" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Açıklama    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Explanation" />

                                       <asp:TableHeaderCell CssClass="inputTitleCell4" HorizontalAlign="Left" Text=""></asp:TableHeaderCell>
                                    </asp:TableRow>
                                </asp:Table>


                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="tableStyle1" colspan="6">

                                <asp:GridView ID="GridView1" runat="server" CssClass="data" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                    BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" Width="100%"  ></asp:GridView>


                                <br />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="navigateButton" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </div>
    <br />
    <table width="95%">
        <tr align="right">
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="width: 80px">
                <asp:Label ID="Label5" runat="server" Text="powered by" CssClass="poweredbyTitle"></asp:Label>

            </td>
            <td style="width: 100px">
                <asp:LinkButton ID="LinkButton1" href="http://www.birlesikodeme.com/" runat="server" Text="BİRLEŞİK ÖDEME" CssClass="procenneTitle"></asp:LinkButton>

            </td>
        </tr>
    </table>
</asp:Content>
