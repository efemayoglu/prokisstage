﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_EmptyKioskMutabakat : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private int ownSubMenuIndex = -1;

    private ParserListKioskName kiosks;

    public int kioskId; 
    public int emptyKioskId;
    private ParserGetKiosk kiosk;
    public string insertDate;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/EmptyKioskMutabakat.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }

        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.insertDate = ((Request.QueryString["insertDate"])).ToString();
        this.kioskId = Convert.ToInt32(Request.QueryString["kioskID"]);
        this.emptyKioskId = Convert.ToInt32(Request.QueryString["emptyKioskId"]);


        if (this.kioskId == 0 && ViewState["kioskID"] != null)
        {
            this.kioskId = Convert.ToInt32(ViewState["kioskID"].ToString());

        }

        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }

    private void BindCtrls()
    {

        CallWebServices callWebServ = new CallWebServices();
        if (this.LoginDatas != null)
        {
            if (this.LoginDatas.User != null)
            {
                if (this.LoginDatas.User.Id != 0)
                {
                    ParserListEmptyKioskNames kiosks = callWebServ.CallGetKioskNameMutabakatService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

                    if (kiosks != null)
                    {
                        if (kiosks.errorCode == 0)
                        {
                            this.kiosk = callWebServ.CallGetKioskService(this.kioskId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                        }
                        else if (kiosks.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                        {
                            Session.Abandon();
                            Session.RemoveAll();
                            Response.Redirect("../root/Login.aspx", true);
                        }
                        else if (kiosks.errorCode == 2)
                            this.messageArea.InnerHtml = "Onaylamak için Kayıt bulunamadı !";
                        else
                            this.messageArea.InnerHtml = kiosks.errorDescription;
                    }
                    else
                        this.messageArea.InnerHtml = "Sisteme erişilemiyor !";
                }
            }
        }
 
        if (Request.QueryString["IsOK"] == "1")
        {
            this.messageArea.InnerHtml = "İşlem Başarılı";
        }
        this.SetControls();
    }

    private void SetControls()
    {
        try
        {

            txtkiosk.Text = kiosk.Kiosk.KioskName + ", Giriş Zamanı: " + insertDate;

            GetMutabakatCashCount();
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "EmptyKioskMutabakatSC");
        }
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        ApproveMutabakat();
    }

    private void ApproveMutabakat()
    {
        try
        {
            int updateStatus=0;

            CallWebServices callWebServ = new CallWebServices();
            ParserOperation response = callWebServ.CallApproveMutabakatService(emptyKioskId, this.LoginDatas.User.Id, this.MerkezTotalAmountTb.Text, this.ExpertTotalAmountTb.Text, this.LoginDatas.AccessToken, this.LoginDatas.User.Id, txtExplanation.Text);

            if (response != null)
            {
                this.messageArea.InnerHtml = response.errorDescription;
                scriptLiteral.Text = response.errorDescription;
                //ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('" + response.errorDescription + "'); ", true);
                //ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:callOwn(); ", true);
                //ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('" + response.errorDescription + "'); ", true);
                //Response.Redirect("../Account/EmptyKioskMutabakat.aspx");
            }
            else if (response.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
            {
                Session.Abandon();
                Session.RemoveAll();
                Response.Redirect("../root/Login.aspx", true);
            }
            else
            {
                updateStatus = -10;
                this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
                //ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('Sisteme Erişilemiyor !'); ", true);
            }

            string scriptText = "";

            switch (response.errorCode)
            {
                case (int)ManagementScreenErrorCodes.SystemError:
                    this.messageArea.InnerHtml = "System error has occurred. Try again.";
                    break;
                case (int)ManagementScreenErrorCodes.SQLServerError:
                    this.messageArea.InnerHtml = "System error has occurred. Try again. (SQL Server Error)";
                    break;
                case -2:
                    this.messageArea.InnerHtml = "A record already exists.";
                    break;
                case 0:
                    this.messageArea.InnerHtml = "İşlem Başarılı.";
                    scriptText = "parent.showMessageWindowApproved('İşlem Başarılı.');";
                    break;
                default:
                    this.messageArea.InnerHtml = "İşlem Başarısız.";
                    scriptText = "parent.showMessageWindowApproved('İşlem Başarısız.Hata Oluştu.');";

                    break;
            }
            if (scriptText != "")
            {
                ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);
            }

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ApproveMutabakat");
        }
    }

    protected void GetMutabakatCashCount()
    {
        calculatedDiff.ForeColor = System.Drawing.Color.Black;

        //int emptyKioskId = 0;
        CallWebServices callWebServ = new CallWebServices();
        if (this.kioskId != 0)
        {
            //emptyKioskId = kioskId;

            ParserListMutabakatCashCounts merkezCashCounts = callWebServ.CallGetMutabakatCashCountService(1, emptyKioskId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (merkezCashCounts != null)
            {
                if (merkezCashCounts.errorCode == 0)
                {
                    if (merkezCashCounts.cashCounts != null)
                    {
                        ParserListMutabakatCashCounts expertCashCounts = callWebServ.CallGetMutabakatCashCountService(2, emptyKioskId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                        if (expertCashCounts != null)
                        {
                            if (expertCashCounts.errorCode == 0)
                            {
                                if (expertCashCounts.cashCounts != null)
                                {
                                    BindMerkezTextBoxes(merkezCashCounts);
                                    BindExpertTextBoxes(expertCashCounts);
                                    decimal fark = Convert.ToDecimal(this.ExpertTotalAmountTb.Text) - Convert.ToDecimal(MerkezTotalAmountTb.Text);
                                    if (fark == 0)
                                    {
                                        calculatedDiff.ForeColor = System.Drawing.Color.Green;
                                        calculatedDiff.Text = fark.ToString() + " TL  Lütfen Onaylayınız.";

                                    }
                                    else
                                    {
                                        calculatedDiff.ForeColor = System.Drawing.Color.Red;
                                        calculatedDiff.Text = fark.ToString() + " TL  Lütfen Açıklama Giriniz.";
                                    }

                                    
                                }

                            }
                            else if (expertCashCounts.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                            {
                                Session.Abandon();
                                Session.RemoveAll();
                                Response.Redirect("../root/Login.aspx", true);
                            }
                            else
                                this.messageArea.InnerHtml = expertCashCounts.errorDescription;
                        }
                    }
                    else
                        this.messageArea.InnerHtml = "Sisteme erişilemiyor !";

                }
                else
                    this.messageArea.InnerHtml = merkezCashCounts.errorDescription;
            }
            else
                this.messageArea.InnerHtml = "Sisteme erişilemiyor !";
        }
    }

    private void BindMerkezTextBoxes(ParserListMutabakatCashCounts list)
    {
        decimal totalAmount = 0;

        for (int i = 0; i < list.cashCounts.Count; i++)
        {
            if (list.cashCounts[i].cashTypeId == 1)
            {
                this.MerkezTB1.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * 200;
            }
            else if (list.cashCounts[i].cashTypeId == 2)
            {
                this.MerkezTB2.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * 100;
            }
            else if (list.cashCounts[i].cashTypeId == 3)
            {
                this.MerkezTB3.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * 50;
            }
            else if (list.cashCounts[i].cashTypeId == 4)
            {
                this.MerkezTB4.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * 20;
            }
            else if (list.cashCounts[i].cashTypeId == 5)
            {
                this.MerkezTB5.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * 10;
            }
            else if (list.cashCounts[i].cashTypeId == 6)
            {
                this.MerkezTB6.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * 5;
            }
            else if (list.cashCounts[i].cashTypeId == 7)
            {
                this.MerkezTB7.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * 1;
            }
            else if (list.cashCounts[i].cashTypeId == 8)
            {
                this.MerkezTB8.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * (decimal)(0.5);
            }
            else if (list.cashCounts[i].cashTypeId == 9)
            {
                this.MerkezTB9.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * (decimal)(0.25);
            }
            else if (list.cashCounts[i].cashTypeId == 10)
            {
                this.MerkezTB10.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * (decimal)(0.1);
            }
            else if (list.cashCounts[i].cashTypeId == 11)
            {
                this.MerkezTB11.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * (decimal)(0.05);
            }
        }
        this.MerkezTotalAmountTb.Text = totalAmount.ToString();
    }

    private void BindExpertTextBoxes(ParserListMutabakatCashCounts list)
    {
        decimal totalAmount = 0;
        for (int i = 0; i < list.cashCounts.Count; i++)
        {
            if (list.cashCounts[i].cashTypeId == 1)
            {
                this.ExpertTB1.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * 200;
            }
            else if (list.cashCounts[i].cashTypeId == 2)
            {
                this.ExpertTB2.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * 100;
            }
            else if (list.cashCounts[i].cashTypeId == 3)
            {
                this.ExpertTB3.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * 50;
            }
            else if (list.cashCounts[i].cashTypeId == 4)
            {
                this.ExpertTB4.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * 20;
            }
            else if (list.cashCounts[i].cashTypeId == 5)
            {
                this.ExpertTB5.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * 10;
            }
            else if (list.cashCounts[i].cashTypeId == 6)
            {
                this.ExpertTB6.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * 5;
            }
            else if (list.cashCounts[i].cashTypeId == 7)
            {
                this.ExpertTB7.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * 1;
            }
            else if (list.cashCounts[i].cashTypeId == 8)
            {
                this.ExpertTB8.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * (decimal)(0.5);
            }
            else if (list.cashCounts[i].cashTypeId == 9)
            {
                this.ExpertTB9.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * (decimal)(0.25);
            }
            else if (list.cashCounts[i].cashTypeId == 10)
            {
                this.ExpertTB10.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * (decimal)(0.1);
            }
            else if (list.cashCounts[i].cashTypeId == 11)
            {
                this.ExpertTB11.Text = list.cashCounts[i].cashCount.ToString();
                totalAmount += list.cashCounts[i].cashCount * (decimal)(0.05);
            }
        }
        this.ExpertTotalAmountTb.Text = totalAmount.ToString();
    }
}
