﻿using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ShowKioskEmptyCashCountByCode : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private int ownSubMenuIndex = -1;
    // private ParserListKioskName kiosks;

    public int kioskId;
    public int emptyKioskId;    
    public string insertDate;
    private ParserGetKiosk kiosk;

    private ParserListEmptyKioskNames emptyKiosks;

    protected void Page_Load(object sender, EventArgs e)
    {
        
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/ListApprovedCollectedMoney.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }

        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }

        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1" || this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1")
            {

            }
            else
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.emptyKioskId = Convert.ToInt32(Request.QueryString["emptyKioskId"]);
        this.kioskId = Convert.ToInt32(Request.QueryString["kioskID"]);
        this.insertDate = ((Request.QueryString["insertDate"])).ToString();

        if (this.kioskId == 0 && ViewState["kioskID"] != null)
        {
            this.kioskId = Convert.ToInt32(ViewState["kioskID"].ToString());

        }

        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }

    private void BindCtrls()
    {
        CallWebServices callWebServ = new CallWebServices();
        if (this.kioskId != 0)
        {
            this.kiosk = callWebServ.CallGetKioskService(this.kioskId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        }

        this.SetControls();
    }

    private void SetControls()
    {
        if (kiosk.Kiosk == null)
        {
            txtkiosk.Text = "Kiosk Bulunamadı" + " , Giriş Zamanı: " + "Tarih kaydı yok ";
        }
        else
        txtkiosk.Text = kiosk.Kiosk.KioskName + " , Giriş Zamanı: " + insertDate;
       
        try
        {
            //ListItem item = new ListItem();
            //item.Text = "Kiosk Seçiniz";
            //item.Value = "0";
            //this.kioskBox.Items.Add(item);
            //this.kioskBox.SelectedValue = "0";

            this.TotalAmountTb.Text = "0";
            this.CashYpeTB1.Text = "0";
            this.CashYpeTB2.Text = "0";
            this.CashYpeTB3.Text = "0";
            this.CashYpeTB4.Text = "0";
            this.CashYpeTB5.Text = "0";
            this.CashYpeTB6.Text = "0";
            this.CashYpeTB7.Text = "0";
            this.CashYpeTB8.Text = "0";
            this.CashYpeTB9.Text = "0";
            this.CashYpeTB10.Text = "0";
            this.CashYpeTB11.Text = "0";


        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "EnterKioskEmptyCashCountSC");
        }
    }

    protected void CashYpeTB1_TextChanged(object sender, EventArgs e)
    {
        TextBox xsenderTB = (TextBox)sender;

        decimal output;
        bool result = Decimal.TryParse(xsenderTB.Text, out output);

        if (String.IsNullOrEmpty(xsenderTB.Text))
            xsenderTB.Text = "0";

        if (result)
        {
            double totalAmount = (Convert.ToInt32(this.CashYpeTB1.Text) * 200 +
                                  Convert.ToInt32(this.CashYpeTB2.Text) * 100 +
                                  Convert.ToInt32(this.CashYpeTB3.Text) * 50 +
                                  Convert.ToInt32(this.CashYpeTB4.Text) * 20 +
                                  Convert.ToInt32(this.CashYpeTB5.Text) * 10 +
                                  Convert.ToInt32(this.CashYpeTB6.Text) * 5 +
                                  Convert.ToInt32(this.CashYpeTB7.Text) * 1 +
                                  Convert.ToInt32(this.CashYpeTB8.Text) / 2.0 +
                                  Convert.ToInt32(this.CashYpeTB9.Text) / 4.0 +
                                  Convert.ToInt32(this.CashYpeTB10.Text) / 10.0 +
                                  Convert.ToInt32(this.CashYpeTB11.Text) / 20.0);

            this.TotalAmountTb.Text = totalAmount.ToString();
        }
        else
        {
            this.messageArea.InnerHtml = "Lütfen rakam giriniz. !";
        }
    }
    
    protected void saveButton_Click(object sender, EventArgs e)
    {
        SaveCashCount();
    }

    private void SaveCashCount()
    {
        try
        {

            CallWebServices callWebServ = new CallWebServices();

 

            JArray jsonArray = new JArray();

            JObject item1 = new JObject();
            item1.Add("KioskId", kioskId);
            item1.Add("ExpertUserId", this.LoginDatas.User.Id);
            item1.Add("CashTypeId", 1);
            item1.Add("Count", Convert.ToInt32(this.CashYpeTB1.Text));
            item1.Add("KioskEmptyId", Convert.ToInt32(emptyKioskId));
            jsonArray.Add(item1);

            JObject item2 = new JObject();
            item2.Add("KioskId", kioskId);
            item2.Add("ExpertUserId", this.LoginDatas.User.Id);
            item2.Add("CashTypeId", 2);
            item2.Add("Count", Convert.ToInt32(this.CashYpeTB2.Text));
            item2.Add("KioskEmptyId", Convert.ToInt32(emptyKioskId));
            jsonArray.Add(item2);

            JObject item3 = new JObject();
            item3.Add("KioskId", kioskId);
            item3.Add("ExpertUserId", this.LoginDatas.User.Id);
            item3.Add("CashTypeId", 3);
            item3.Add("Count", Convert.ToInt32(this.CashYpeTB3.Text));
            item3.Add("KioskEmptyId", Convert.ToInt32(emptyKioskId));
            jsonArray.Add(item3);

            JObject item4 = new JObject();
            item4.Add("KioskId", kioskId);
            item4.Add("ExpertUserId", this.LoginDatas.User.Id);
            item4.Add("CashTypeId", 4);
            item4.Add("Count", Convert.ToInt32(this.CashYpeTB4.Text));
            item4.Add("KioskEmptyId", Convert.ToInt32(emptyKioskId));
            jsonArray.Add(item4);

            JObject item5 = new JObject();
            item5.Add("KioskId", kioskId);
            item5.Add("ExpertUserId", this.LoginDatas.User.Id);
            item5.Add("CashTypeId", 5);
            item5.Add("Count", Convert.ToInt32(this.CashYpeTB5.Text));
            item5.Add("KioskEmptyId", Convert.ToInt32(emptyKioskId));
            jsonArray.Add(item5);

            JObject item6 = new JObject();
            item6.Add("KioskId", kioskId);
            item6.Add("ExpertUserId", this.LoginDatas.User.Id);
            item6.Add("CashTypeId", 6);
            item6.Add("Count", Convert.ToInt32(this.CashYpeTB6.Text));
            item6.Add("KioskEmptyId", Convert.ToInt32(emptyKioskId));
            jsonArray.Add(item6);

            JObject item7 = new JObject();
            item7.Add("KioskId", kioskId);
            item7.Add("ExpertUserId", this.LoginDatas.User.Id);
            item7.Add("CashTypeId", 7);
            item7.Add("Count", Convert.ToInt32(this.CashYpeTB7.Text));
            item7.Add("KioskEmptyId", Convert.ToInt32(emptyKioskId));
            jsonArray.Add(item7);

            JObject item8 = new JObject();
            item8.Add("KioskId", kioskId);
            item8.Add("ExpertUserId", this.LoginDatas.User.Id);
            item8.Add("CashTypeId", 8);
            item8.Add("Count", Convert.ToInt32(this.CashYpeTB8.Text));
            item8.Add("KioskEmptyId", Convert.ToInt32(emptyKioskId));
            jsonArray.Add(item8);

            JObject item9 = new JObject();
            item9.Add("KioskId", kioskId);
            item9.Add("ExpertUserId", this.LoginDatas.User.Id);
            item9.Add("CashTypeId", 9);
            item9.Add("Count", Convert.ToInt32(this.CashYpeTB9.Text));
            item9.Add("KioskEmptyId", Convert.ToInt32(emptyKioskId));
            jsonArray.Add(item9);

            JObject item10 = new JObject();
            item10.Add("KioskId", kioskId);
            item10.Add("ExpertUserId", this.LoginDatas.User.Id);
            item10.Add("CashTypeId", 10);
            item10.Add("Count", Convert.ToInt32(this.CashYpeTB10.Text));
            item10.Add("KioskEmptyId", Convert.ToInt32(emptyKioskId));
            jsonArray.Add(item10);

            JObject item11 = new JObject();
            item11.Add("KioskId", kioskId);
            item11.Add("ExpertUserId", this.LoginDatas.User.Id);
            item11.Add("CashTypeId", 11);
            item11.Add("Count", Convert.ToInt32(this.CashYpeTB11.Text));
            item11.Add("KioskEmptyId", Convert.ToInt32(emptyKioskId));
            jsonArray.Add(item11);

            int updateStatus = 0;

            if (kiosk == null)
            {
                ParserSaveKioskEmptyCashCount parserSaveKioskEmptyCashCount = callWebServ.CallSaveKioskEmptyCashCountService(jsonArray, kioskId, this.LoginDatas.User.Id, this.codeField.Text, emptyKioskId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);



                if (parserSaveKioskEmptyCashCount != null)
                {
                    if (parserSaveKioskEmptyCashCount.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                    {
                        Session.Abandon();
                        Session.RemoveAll();
                        Response.Redirect("../root/Login.aspx", true);
                    }
                    else
                    {
                        this.messageArea.InnerHtml = parserSaveKioskEmptyCashCount.errorDescription;
                        //ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('" + parserSaveKioskEmptyCashCount.errorDescription + "'); ", true);
                        
                        //ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:callOwn(); ", true);
                        
                        //if (parserSaveKioskEmptyCashCount.errorDescription.Contains("Başarılı")) BindCtrls();
                    }
                }
                else
                {
                    this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
                    //ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('Sisteme Erişilemiyor !'); ", true);
                }
            }
            else
            {
                updateStatus = -10;
            }

            string scriptText = "";

            switch (updateStatus)
            {
                case (int)ManagementScreenErrorCodes.SystemError:
                    this.messageArea.InnerHtml = "System error has occurred. Try again.";
                    break;
                case (int)ManagementScreenErrorCodes.SQLServerError:
                    this.messageArea.InnerHtml = "System error has occurred. Try again. (SQL Server Error)";
                    break;
                case -2:
                    this.messageArea.InnerHtml = "A record already exists.";
                    break;
                default:
                    this.messageArea.InnerHtml = "İşlem Başarılı.";
                    scriptText = "parent.showMessageWindowApproved('İşlem Başarılı.');";
                    //ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:callOwn(); ", true);
                    //ViewState["merchantID"] = this.merchantID;
                    
                    //scriptText = "parent.showMessageWindowApprovedCollectedMoney('İşlem Başarılı.');";
                    //scriptText = "parent.parent.hideModalPopup2_Exit();";
                    break;
            }
            if (scriptText != "")
            {
                ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);
            }


        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowUserRolesSv");
        }
    }

}