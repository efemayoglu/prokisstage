﻿using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Account;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ShowKioskEmptyCashCount : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private int ownSubMenuIndex = -1;
    private ParserListKioskName kiosks;
    public int controlCashId;

    private ParserGetControlCashNotification controlCashNotification;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/ControlCashNotification.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }

        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }

        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }


        this.controlCashId = Convert.ToInt32(Request.QueryString["controlCashId"]);


        if (this.controlCashId == 0 && ViewState["controlCashId"] != null)
        {
            this.controlCashId = Convert.ToInt32(ViewState["controlCashId"].ToString());
        }


        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }

    }

    private void BindCtrls()
    {

        CallWebServices callWebServ = new CallWebServices();
        if (this.LoginDatas != null)
        {
            if (this.LoginDatas.User != null)
            {
                if (this.LoginDatas.User.Id != 0)
                {
                    //this.kiosks = callWebServ.CallGetKioskNameService(this.LoginDatas.User.Id, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                    kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

                    if (this.kiosks != null)
                    {
                        if (this.kiosks.errorCode == 0)
                        {
                            this.kioskBox.DataSource = this.kiosks.KioskNames;
                            this.kioskBox.DataBind();
                        }
                        else if (kiosks.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                        {
                            Session.Abandon();
                            Session.RemoveAll();
                            Response.Redirect("../root/Login.aspx", true);
                        }
                        else if (kiosks.errorCode == 2)
                            this.messageArea.InnerHtml = "İlişkili Kiosk bulunamadı !";
                        else
                            this.messageArea.InnerHtml = kiosks.errorDescription;
                    }
                    else
                        this.messageArea.InnerHtml = "Sisteme erişilemiyor !";
                }
            }
        }

        if (this.controlCashId != 0)
        {
            this.controlCashNotification = callWebServ.CallGetControlCashNotificationService(this.controlCashId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        }

        this.SetControls();
    }

    private void SetControls()
    {
        try
        {
            if (this.controlCashNotification != null)
            {
                this.CashYpeTB1.Text = this.controlCashNotification.ControlCashNotifications.firstDenomNumber;
                this.CashYpeTB2.Text = this.controlCashNotification.ControlCashNotifications.secondDenomNumber;
                this.CashYpeTB3.Text = this.controlCashNotification.ControlCashNotifications.thirdDenomNumber;
                this.CashYpeTB4.Text = this.controlCashNotification.ControlCashNotifications.fourthDenomNumber;
                this.CashYpeTB5.Text = this.controlCashNotification.ControlCashNotifications.fifthDenomNumber;
                this.CashYpeTB6.Text = this.controlCashNotification.ControlCashNotifications.sixthDenomNumber;
                this.CashYpeTB7.Text = this.controlCashNotification.ControlCashNotifications.seventhDenomNumber;
                this.CashYpeTB8.Text = this.controlCashNotification.ControlCashNotifications.eighthDenomNumber;
                this.CashYpeTB9.Text = this.controlCashNotification.ControlCashNotifications.ninthDenomNumber;
                this.CashYpeTB10.Text = this.controlCashNotification.ControlCashNotifications.tenthDenomNumber;
                this.CashYpeTB11.Text = this.controlCashNotification.ControlCashNotifications.eleventhDenomNumber;


                CallWebServices callWebServ = new CallWebServices();
                ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                if (kiosks != null)
                {

                    this.kioskBox.DataSource = kiosks.KioskNames;
                    this.kioskBox.DataBind();
                }


                //ListItem item2 = new ListItem();
                //item2.Text = "Kiosk Seçiniz";
                //item2.Value = "0";
                //this.kioskBox.Items.Add(item2);
                this.kioskBox.SelectedValue = this.controlCashNotification.ControlCashNotifications.KioskId.ToString();
                kioskBox.Attributes.Add("disabled", "true");
            }
            else
            {
                ListItem item = new ListItem();
                item.Text = "Kiosk Seçiniz";
                item.Value = "0";
                this.kioskBox.Items.Add(item);
                this.kioskBox.SelectedValue = "0";

                this.TotalAmountTb.Text = "0";
                this.CashYpeTB1.Text = "0";
                this.CashYpeTB2.Text = "0";
                this.CashYpeTB3.Text = "0";
                this.CashYpeTB4.Text = "0";
                this.CashYpeTB5.Text = "0";
                this.CashYpeTB6.Text = "0";
                this.CashYpeTB7.Text = "0";
                this.CashYpeTB8.Text = "0";
                this.CashYpeTB9.Text = "0";
                this.CashYpeTB10.Text = "0";
                this.CashYpeTB11.Text = "0";
            }


        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "EnterKioskEmptyCashCountSC");
        }
    }

    protected void CashYpeTB1_TextChanged(object sender, EventArgs e)
    {
        TextBox xsenderTB = (TextBox)sender;

        decimal output;
        bool result = Decimal.TryParse(xsenderTB.Text, out output);

        if (String.IsNullOrEmpty(xsenderTB.Text))
            xsenderTB.Text = "0";

        if (result)
        {
            double totalAmount = (Convert.ToInt32(this.CashYpeTB1.Text) * 200 +
                                  Convert.ToInt32(this.CashYpeTB2.Text) * 100 +
                                  Convert.ToInt32(this.CashYpeTB3.Text) * 50 +
                                  Convert.ToInt32(this.CashYpeTB4.Text) * 20 +
                                  Convert.ToInt32(this.CashYpeTB5.Text) * 10 +
                                  Convert.ToInt32(this.CashYpeTB6.Text) * 5 +
                                  Convert.ToInt32(this.CashYpeTB7.Text) * 1 +
                                  Convert.ToInt32(this.CashYpeTB8.Text) / 2.0 +
                                  Convert.ToInt32(this.CashYpeTB9.Text) / 4.0 +
                                  Convert.ToInt32(this.CashYpeTB10.Text) / 10.0 +
                                  Convert.ToInt32(this.CashYpeTB11.Text) / 20.0);

            this.TotalAmountTb.Text = totalAmount.ToString();
        }
        else
        {
            this.messageArea.InnerHtml = "Lütfen rakam giriniz. !";
        }
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        SaveCashNotification();
    }

    private void SaveCashNotification()
    {
        try
        {

            CallWebServices callWebServ = new CallWebServices();

            //save 
            if (controlCashId == 0)
            {

            ParserOperation parserSaveControlCashNotification = callWebServ.CallSaveControlCashNotificationService(Convert.ToInt32(this.kioskBox.SelectedValue),
                                                                               this.LoginDatas.User.Id,
                                                                               this.LoginDatas.AccessToken,
                                                                               CashYpeTB1.Text,
                                                                               CashYpeTB2.Text,
                                                                               CashYpeTB3.Text,
                                                                               CashYpeTB4.Text,
                                                                               CashYpeTB5.Text,
                                                                               CashYpeTB6.Text,
                                                                               CashYpeTB7.Text,
                                                                               CashYpeTB8.Text,
                                                                               CashYpeTB9.Text,
                                                                               CashYpeTB10.Text,
                                                                               CashYpeTB11.Text,
                                                                               Convert.ToDecimal(this.TotalAmountTb.Text));


            if (parserSaveControlCashNotification != null)
            {
                string scriptText = "";

                //this.messageArea.InnerHtml = parserSaveControlCashNotification.errorDescription;

                scriptText = "parent.showMessageWindowCashCount('İşlem Başarılı.');";
                ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);

            }
            else if (parserSaveControlCashNotification.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
            {
                Session.Abandon();
                Session.RemoveAll();
                Response.Redirect("../root/Login.aspx", true);
            }
            else
            {
                this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('Sisteme Erişilemiyor !'); ", true);
            }

        }

        //update
        else
        {          
            ParserOperation parserSaveControlCashNotification = callWebServ.CallUpdateControlCashNotificationService(controlCashId,
                                                                               this.LoginDatas.User.Id,
                                                                               this.LoginDatas.AccessToken,
                                                                               CashYpeTB1.Text,
                                                                               CashYpeTB2.Text,
                                                                               CashYpeTB3.Text,
                                                                               CashYpeTB4.Text,
                                                                               CashYpeTB5.Text,
                                                                               CashYpeTB6.Text,
                                                                               CashYpeTB7.Text,
                                                                               CashYpeTB8.Text,
                                                                               CashYpeTB9.Text,
                                                                               CashYpeTB10.Text,
                                                                               CashYpeTB11.Text,
                                                                               Convert.ToDecimal(this.TotalAmountTb.Text));


            if (parserSaveControlCashNotification != null)
            {
                string scriptText = "";

                //this.messageArea.InnerHtml = parserSaveControlCashNotification.errorDescription;

                scriptText = "parent.showMessageWindowCashCount('İşlem Başarılı.');";
                ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);

            }
            else if (parserSaveControlCashNotification.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
            {
                Session.Abandon();
                Session.RemoveAll();
                Response.Redirect("../root/Login.aspx", true);
            }
            else
            {
                this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('Sisteme Erişilemiyor !'); ", true);
            }


        }

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "Settings");
        }
    }
}