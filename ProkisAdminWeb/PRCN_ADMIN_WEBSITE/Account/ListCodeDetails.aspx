﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListCodeDetails.aspx.cs" Inherits="Account_ListCodeDetails" MasterPageFile="~/webctrls/AdminSiteMPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<%@ Register Src="../MS_Control/MultipleSelection.ascx" TagName="MultipleSelection" TagPrefix="uc1" %>
<%@ Register Namespace="ClickableWebControl" TagPrefix="clkweb" %>

<asp:Content ID="pageContent" ContentPlaceHolderID="mainCPHolder" runat="server">
    <input type="hidden" id="pageNumRefField" runat="server" name="pageNumRefField" value="0" />
    <asp:Button ID="navigateButton" runat="server" OnClick="navigateButton_Click" CssClass="dummy" />
    <asp:Button ID="deleteButton" runat="server" CssClass="dummy" OnClick="deleteButton_Click" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { setInterval(function () { cache_clear() }, 120000); });
        function cache_clear() { window.location.reload(true); }
    </script>
    <script language="javascript" type="text/javascript">

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function searchButtonClicked(sender) {
            document.getElementById('pageNumRefField').value = "0";
            return true;
        }

        function postForPaging(selectedPageNum) {
            document.getElementById('pageNumRefField').value = selectedPageNum;
            document.getElementById('navigateButton').click();
        }

        function strtDateChngd() {
            if (document.getElementById('startDateField').value < "2015-11-04 00:00:00") {
                document.getElementById('startDateField').value = "2015-11-04 00:00:00"
            }
            else if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                document.getElementById('startDateField').value = document.getElementById('endDateField').value;
            }

        }

        function endDateChngd() {
            if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                document.getElementById('endDateField').value = document.getElementById('startDateField').value;
            }
        }

        function checkForm() {

            var status = true;

            var messageText = "";

            if (document.getElementById('instutionBox').value == "0") {
                status = false; //return false döner 
                messageText = "Kurum Seçiniz !";
            }

            else {
                messageText = "";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }

    </script>
    <script type="text/javascript" language="javascript" src="../js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery.tablednd_0_5.js"></script>
    <asp:HiddenField ID="idRefField" runat="server" Value="0" />
    <asp:HiddenField ID="oldOrderNumberField" runat="server" Value="0" />
    <asp:HiddenField ID="newOrderNumberField" runat="server" Value="0" />
    <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>

    <div align="center">
        <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp;</div>
        <asp:Panel ID="panel1" DefaultButton="searchButton" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto">
            <table border="0" cellpadding="0" cellspacing="0" id="Table2" runat="server" width="90%">
                <tr>
                    <td align="left" class="inputTitle" style="width: 99px">
                        <asp:Label ID="startDateLabel" runat="server" Visible="true">Başlangıç Zamanı:</asp:Label>
                    </td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 162px">
                        <asp:TextBox ID="startDateField" runat="server" CssClass="inputLine_100x20" Width="160"></asp:TextBox>
                        <ajaxControlToolkit:CalendarExtender ID="startDateCalendar" runat="server" TargetControlID="startDateField"
                            Format="yyyy-MM-dd HH:mm:ss" DaysModeTitleFormat="yyyy-MM-dd HH:mm:ss" Enabled="True" OnClientDateSelectionChanged="strtDateChngd">
                        </ajaxControlToolkit:CalendarExtender>
                    </td>
                    <td style="width: 18px">&nbsp;&nbsp;</td>

                    <td align="left" class="inputTitle" style="width: 79px; text-align: left">
                        <asp:Label ID="mercLabel" runat="server" Visible="true">Kiosk:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 200px">
                        <uc1:MultipleSelection ID="MultipleSelection1" runat="server" CssClass="inputLine_100x20" />
                    </td>
                    <td style="width: 18px">&nbsp;&nbsp;</td>

                    <td align="left" class="inputTitle" style="width: 50px; text-align: left">
                        <asp:Label ID="Label2" runat="server" Visible="true">Kurum:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px">
                        <asp:ListBox ID="instutionBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>
                  
                    <td style="width: 9px">&nbsp;</td>

                    <td align="left" class="inputTitle" style="width: 50px; text-align: left">
                        <asp:Label ID="Label7" runat="server" Visible="true">Ödeme Türü:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px">
                        <asp:ListBox ID="paymentTypeBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>


                    <td style="width: 86px">
                        <asp:Button ID="searchButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                            OnClick="searchButton_Click" Text="Ara" OnClientClick="return checkForm();"></asp:Button>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>


                <tr>
                    <td align="left" class="inputTitle" style="width: 10px">
                        <asp:Label ID="endDateLabel" runat="server">Bitiş Zamanı:</asp:Label>
                    </td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 162px">
                        <asp:TextBox ID="endDateField" runat="server" CssClass="inputLine_100x20" Width="160"></asp:TextBox>
                        <ajaxControlToolkit:CalendarExtender ID="endDateCalendar" runat="server" TargetControlID="endDateField"
                            Format="yyyy-MM-dd HH:mm:ss" DaysModeTitleFormat="yyyy-MM-dd HH:mm:ss" Enabled="True" OnClientDateSelectionChanged="endDateChngd">
                        </ajaxControlToolkit:CalendarExtender>
                    </td>
                    <td style="width: 18px">&nbsp;&nbsp;</td>

                      
                    <td align="left" class="inputTitle" style="width: 79px; text-align: left">&nbsp;Kod Ara:
                    </td>
                    <td style="width: 9px">&nbsp;</td>
                   <td style="width: 100px">
                        <asp:TextBox ID="searchTextField" CssClass="inputLine_100x20" runat="server"></asp:TextBox>
                    </td>
                        <td align="left" class="inputTitle" style="text-align: left">
                    </td>

                    <td align="left" class="inputTitle" style="width: 50px; text-align: left">
                    <asp:Label ID="Label3" runat="server" Visible="true">İşlem Tipi::</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px">
                        <asp:ListBox ID="operationTypeBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                       DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>
                    <td></td>
                    <td align="left" class="inputTitle" style="width: 50px; text-align: left">
                        <asp:Label ID="Label8" runat="server" Visible="true">Banka:</asp:Label>
                    </td>
                    <td></td>
                    <td>
                        <asp:ListBox ID="bankTypeBox" runat="server" CssClass="inputLine_100x20" DataTextField="Name" DataValueField="Id" Rows="1" SelectionMode="Single" Visible="true"></asp:ListBox>
                    </td>

   </tr>

            </table>
        </asp:Panel>
    </div>
    <div>&nbsp;</div>
    <div align="center">
        <asp:Panel ID="ListPanel" runat="server" CssClass="containerPanel_95Pxauto">
            <div align="left" class="windowTitle_container_autox30">
                Kod Detayları<hr style="border-bottom: 1px solid #b2b2b4;" />
            </div>
            <table cellpadding="0" cellspacing="0" width="95%" id="upTable" runat="server">
                <tr style="height: 20px">
                    <td style="width: 22px;">
                        <asp:ImageButton ID="excellButton" runat="server" ClientIDMode="Static" OnClick="excelButton_Click" ImageUrl="~/images/excel.jpg" />
                    </td>
                    <td style="width: 8px;">&nbsp;</td>
                    <td style="width: 200px;">
                        <asp:Label ID="recordInfoLabel" runat="server" Style="width: 200px; height: 20px; text-align: left" CssClass="data"></asp:Label>
                    </td>
                    <td style="height: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td style="height: 20px; width: 130px; text-align: right">
                        <asp:Label ID="Label1" Text="Listelenecek Kayıt Sayısı :  " runat="server" Style="width: 200px; height: 20px; text-align: left" CssClass="data"></asp:Label>
                    </td>
                    <td align="right" style="height: 20px; width: 33px">
                        <asp:TextBox ID="numberOfItemField" CssClass="inputLine_30x20_s" MaxLength="3" runat="server" onkeypress='validateNo(event)'></asp:TextBox></td>


                </tr>
                <tr>
                    <td align="center" class="tableStyle1" colspan="6">
                        <asp:UpdatePanel ID="ListUPanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" RenderMode="Block">
                            <ContentTemplate>
                                <asp:Table ID="itemsTable" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                    BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="95%">
                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="#"></asp:TableHeaderCell>
                                        <%--<clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kiosk No    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="KioskId" />--%>
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kiosk Adı    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="KioskName" Width="150px" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Nakit Para Girişi    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="CashAmount" Width="100px" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kredi Kartı    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="CreditCardAmount" Width="100px" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="KK Komisyon    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="CreditCardCommision" Width="100px" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Mobil Ödeme    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="MobilPayment" Width="100px" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="MÖ Komisyon    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="MobilePaymentCommision" Width="100px" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="İşlem Sayısı   <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="SuccessTxnCount" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Dolum Tutarı   <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="InstitutionAmount" Width="100px" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="İptal İşlem Say.   <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="InverseRecCount" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="İptal Tutar   <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="InverseRecAmount" Width="100px" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Satış İşlem Say.   <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="NonInverseRecCount" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Satış Tutar   <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="TotalRecAmount" Width="100px" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Toplam Tutar   <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="NonInverseRecAmount" Width="100px" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Toplam İşlem Say.   <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="TotalRecCount" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Manüel Üretilen Kod    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="ManuelGeneratedCode" Width="100px" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Üretilen Kod    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="CodeGeneratedToday" Width="100px" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kullanılan Kod    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="UsedTotalToday" Width="100px" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kullanılabilir Kod    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="CodeUnusedToday" Width="100px" />

                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Askıda Kalan Kod    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="AskidaCode" Width="100px" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Silinen Kod    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="ErasedCode" Width="100px" />

                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Para Üstü  <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="MoneyChange" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Toplam Kullanım Ücreti  <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="UsageFee" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Mutabakat    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Mutabakat" />
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" ID="DetailColumn" runat="server" Text=""></asp:TableHeaderCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="deleteButton" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="navigateButton" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <br />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <br />
    <table width="95%">
        <tr align="right">
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="width: 80px">
                <asp:Label ID="Label5" runat="server" Text="powered by" CssClass="poweredbyTitle"></asp:Label>

            </td>
            <td style="width: 100px">
                <asp:LinkButton ID="LinkButton1" href="http://www.birlesikodeme.com/" runat="server" Text="BİRLEŞİK ÖDEME" CssClass="procenneTitle"></asp:LinkButton>

            </td>
        </tr>
    </table>
</asp:Content>
