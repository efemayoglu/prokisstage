﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmptyKioskMutabakat.aspx.cs" Inherits="Account_EmptyKioskMutabakat" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />
    <input type="hidden" id="hiddenId" runat="server" name="hiddenId" value="0" />
    <script language="javascript" type="text/javascript">

        function callOwn() {
            //alert("İşlem Başarılı Şekilde Gerçekleştirildi.!");
            window.close();
            window.parent.location = "../Account/ListApprovedCollectedMoney.aspx";
        }

        function checkForm() {
            var status = true;
            var messageText = "";


            if (document.getElementById('kioskBox').value == "0") {
                status = false;
                messageText = "Kiosk Seçiniz !";
            }
            else {
                messageText = "";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }

        //function validateIP(evt) {
        //    var theEvent = evt || window.event;
        //    var key = theEvent.keyCode || theEvent.which;
        //    key = String.fromCharCode(key);
        //    var regex = /[0-9]|\./;
        //    if (!regex.test(key)) {
        //        theEvent.returnValue = false;
        //        if (theEvent.preventDefault) theEvent.preventDefault();
        //    }
        //}

        //function validateNo(evt) {
        //    var theEvent = evt || window.event;
        //    var key = theEvent.keyCode || theEvent.which;
        //    key = String.fromCharCode(key);
        //    var regex = /[0-9]/;
        //    if (!regex.test(key)) {
        //        theEvent.returnValue = false;
        //        if (theEvent.preventDefault) theEvent.preventDefault();
        //    }
        //}

        //function ValidateIPaddress(inputText) {
        //    var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
        //    if (inputText.match(ipformat)) {
        //        return true;
        //    }
        //    else {
        //        return false;
        //    }
        //}

        //function validateDecimal(evt) {
        //    var theEvent = evt || window.event;
        //    var key = theEvent.keyCode || theEvent.which;
        //    key = String.fromCharCode(key);
        //    var regex = /[0-9]|\.|\,/;
        //    if (!regex.test(key)) {
        //        theEvent.returnValue = false;
        //        if (theEvent.preventDefault) theEvent.preventDefault();
        //    }
        //}

        //function ValidateDecimalNumber(inputText) {
        //    if (document.getElementById('amountTextBox').value == '')
        //        return true;
        //    var ipformat = /^[0-9]+((\.[0-9]{1,2})|(\,[0-9]{1,2}))?$/;
        //    if (inputText.match(ipformat)) {
        //        return true;
        //    }
        //    else {
        //        return false;
        //    }
        //}
    </script>
    <base target="_self" />
    <style type="text/css">
        .auto-style14 {
            border: solid 1px #2372BE;
            font-family: Tahoma;
            font-size: 12px;
            font-weight: bold;
            width: 62px;
            height: 30px;
            border-radius: 5px;
            padding-bottom: 5px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>

        <div align="center">
            <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp;</div>
        </div>
        <div>&nbsp;</div>
        <div align="center">




            <asp:Panel ID="ListPanel" runat="server"  Height="680px" >
                <div align="left" class="windowTitle_container_autox30">
                    Kiosk Toplanan Para Mutabakat<hr style="border-bottom: 1px solid #b2b2b4;" />

                </div>

                <table>
                    <%--                <tr>
                    <td style="width: 230px"></td>
                    <td align="center" class="staticTextLine_30x25" style="width: 67px" >Kiosk:</td>
                    <td align="left" style="width: 58px">
                        <asp:ListBox ID="kioskBox" CssClass="inputLine_120x25" runat="server" SelectionMode="Single" AutoPostBack="True"
                            DataTextField="Name" DataValueField="Id" Rows="1" OnSelectedIndexChanged="kioskBox_SelectedIndexChanged"></asp:ListBox>
                    </td>
                    <td align="right" style="width: 170px"></td>
                </tr>--%>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width="500px" id="Table2" runat="server">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width="250px" id="Table1" runat="server">
                                            <tr>
                                                <td align="center" colspan="2" class="staticTextLine_200x20">Merkezde Hesaplanan Miktar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2">
                                                    <asp:Table ID="itemsTable" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                                        BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="100%">
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Para Türü" Width="60px"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Para Adedi"></asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="200 TL"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="MerkezTB1" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="100 TL"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="MerkezTB2" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="50 TL"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="MerkezTB3" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="20 TL"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="MerkezTB4" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="10 TL"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="MerkezTB5" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="5 TL"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="MerkezTB6" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="1 TL"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="MerkezTB7" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="50 Krş"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="MerkezTB8" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="25 Krş"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="MerkezTB9" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="10 Krş"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="MerkezTB10" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="5 Krş"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="MerkezTB11" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </td>
                                            </tr>

                                            <tr valign="middle">
                                                <td align="center" class="auto-style14">Toplam:</td>
                                                <td align="center">
                                                    <asp:TextBox ID="MerkezTotalAmountTb" runat="server" CssClass="inputLine_90x20" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width="250px" id="Table3" runat="server">
                                            <tr>
                                                <td align="center" colspan="2" class="staticTextLine_200x20">Kioskdan Alınan Miktar</td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2">
                                                    <asp:Table ID="Table4" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                                        BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="100%">
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Para Türü" Width="60px"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Para Adedi"></asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="200 TL"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="ExpertTB1" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="100 TL"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="ExpertTB2" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="50 TL"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="ExpertTB3" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="20 TL"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="ExpertTB4" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="10 TL"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="ExpertTB5" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="5 TL"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="ExpertTB6" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="1 TL"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="ExpertTB7" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="50 Krş"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="ExpertTB8" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="25 Krş"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="ExpertTB9" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="10 Krş"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="ExpertTB10" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                            <asp:TableHeaderCell CssClass="inputTitleCell8" Text="5 Krş"></asp:TableHeaderCell>
                                                            <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                                <asp:TextBox ID="ExpertTB11" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                            </asp:TableHeaderCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </td>
                                            </tr>

                                            <tr valign="middle">
                                                <td align="center" class="auto-style14">Toplam:
                                                </td>
                                                <td align="center">
                                                    <asp:TextBox ID="ExpertTotalAmountTb" runat="server" CssClass="inputLine_90x20" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr height="6">
                                    <td align="center" colspan="2"></td>
                                </tr>

                                <tr>
                                    <td align="center" class="inputTitleCell3" height="25" colspan="2">
                                        <asp:Literal ID="txtkiosk" runat="server"></asp:Literal>
                                    </td>
                                </tr>


                                <tr height="30">
                                    <td align="center" colspan="2" class="inputTitleCell123">
                                        <asp:Label ID="calculatedDiff" runat="server" Text="0 TL"></asp:Label>
                                    </td>
                                </tr>
                                <tr height="30">
                                    <td align="left" class="inputTitleCell2" style="width: 200px" colspan="2">Açıklama:</td>
                                </tr>
                                <tr height="55">
                                    <td align="center" class="inputTitleCell123" style="width: 410px; height: 20px;" colspan="2">
                                        <asp:TextBox ID="txtExplanation" runat="server" CssClass="inputLine_410x20" Height="45px" MaxLength="600" Width="500px" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                               

                            </table>
                        </td>
                    </tr>

                </table>
                <div align="center">
                    <table>
                        <tr>
                            <td align="center" style="padding-top: 3px; padding-bottom: 5px; width: 258px;">
                                <asp:Button ID="saveButton" CssClass="buttonCSSDesign" Style="height: 30px; width: 80px;" runat="server" ClientIDMode="Static"
                                    OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Onayla"></asp:Button>

                            </td>
                        </tr>
                       
                    </table>
                </div>
            </asp:Panel>


        </div>
    </form>
</body>
</html>
