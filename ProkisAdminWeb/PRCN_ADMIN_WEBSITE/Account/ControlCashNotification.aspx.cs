﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Account;
using PRCNCORE.Parser.Other;
using PRCNCORE.Utilities;
using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Account_ControlCashNotification : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;

    private int orderSelectionColumn = 2;
    private int orderSelectionDescAsc = 1;

    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/ControlCashNotification.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);
        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {


            CallWebServices callWebServ = new CallWebServices();


            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {

                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }

            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {
                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();
            }


            ListItem item = new ListItem();
            item.Text = "Kurum Seçiniz";
            item.Value = "0";
            this.instutionBox.Items.Add(item);
            this.instutionBox.SelectedValue = "0";

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.startDateField.Text = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00";

            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;
            this.pageNum = 0;
            this.SearchOrder(2, 1);

            ParserListIsCardDeleted isCardDeleted = callWebServ.CallGetIsCardDeletedService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (isCardDeleted != null)
            {

                this.isCardDeletedBox.DataSource = isCardDeleted.IsCardDeleted;
                this.isCardDeletedBox.DataBind();

            }

            ListItem item6 = new ListItem();
            item6.Text = "Durum Seçiniz";
            item6.Value = "0";
            this.isCardDeletedBox.Items.Add(item6);
            this.isCardDeletedBox.SelectedValue = "0";

            this.pageNum = 0;
            this.SearchOrder(16, 0);
        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(16, 0);
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskId = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskId = "0";
            else
                kioskId = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebService = new CallWebServices();
            ParserListKioskControlCashNotification items = callWebService.CallControlCashNotification(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text).AddDays(0).AddSeconds(-1),
                                                                             Convert.ToDateTime(this.endDateField.Text).AddDays(0).AddSeconds(-1),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             kioskId,
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id, 1,
                                                                             Convert.ToInt32(this.isCardDeletedBox.SelectedValue)
                                                                             ,0);

            if (items != null)
            {
                if (items.errorCode == 0 || items.errorCode== 2)
                {
                    this.BindListTable(items);
                }
                else if (items.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + items.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskCashSrch");
        }
    }


    private void BindListTable(ParserListKioskControlCashNotification items)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            if (items.ControlCashNotifications != null)
            {


                TableRow[] Row = new TableRow[items.ControlCashNotifications.Count];
                int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
                for (int i = 0; i < Row.Length; i++)
                {
                    Row[i] = new TableRow();
                    Row[i].Attributes.Add("Id", items.ControlCashNotifications[i].kioskId.ToString());

                    TableCell indexCell = new TableCell();
                    TableCell kioskIdCell = new TableCell();
                    TableCell institutionId = new TableCell();
                    TableCell insertionDate = new TableCell();

                    // TableCell value1Cell = new TableCell();
                    TableCell amount1Cell = new TableCell();
                    //TableCell value2Cell = new TableCell();
                    TableCell amount2Cell = new TableCell();
                    //TableCell value3Cell = new TableCell();
                    TableCell amount3Cell = new TableCell();
                    TableCell value4Cell = new TableCell();
                    TableCell amount4Cell = new TableCell();
                    //TableCell value5Cell = new TableCell();
                    TableCell amount5Cell = new TableCell();
                    //TableCell value6Cell = new TableCell();
                    TableCell amount6Cell = new TableCell();
                    //TableCell value7Cell = new TableCell();
                    TableCell amount7Cell = new TableCell();
                    //TableCell value8Cell = new TableCell();
                    TableCell amount8Cell = new TableCell();
                    //TableCell value9Cell = new TableCell();
                    TableCell amount9Cell = new TableCell();
                    //TableCell value10Cell = new TableCell();
                    TableCell amount10Cell = new TableCell();
                    //TableCell value11Cell = new TableCell();
                    TableCell amount11Cell = new TableCell();


                    TableCell totalAmountCell = new TableCell();

                    TableCell creatorUserCell = new TableCell();
                    TableCell enteredUserCell = new TableCell();
                    TableCell enteredDateCell = new TableCell();
                    TableCell statusCell = new TableCell();

                    indexCell.CssClass = "inputTitleCell4";
                    kioskIdCell.CssClass = "inputTitleCell4";
                    institutionId.CssClass = "inputTitleCell4";
                    insertionDate.CssClass = "inputTitleCell4";
                    //value1Cell.CssClass = "inputTitleCell4";
                    amount1Cell.CssClass = "inputTitleCell4";
                    //value2Cell.CssClass = "inputTitleCell4";
                    amount2Cell.CssClass = "inputTitleCell4";
                    //value3Cell.CssClass = "inputTitleCell4";
                    amount3Cell.CssClass = "inputTitleCell4";
                    //value4Cell.CssClass = "inputTitleCell4";
                    amount4Cell.CssClass = "inputTitleCell4";
                    //value5Cell.CssClass = "inputTitleCell4";
                    amount5Cell.CssClass = "inputTitleCell4";
                    //value6Cell.CssClass = "inputTitleCell4";
                    //amount6Cell.CssClass = "inputTitleCell4";
                    //value7Cell.CssClass = "inputTitleCell4";
                    amount7Cell.CssClass = "inputTitleCell4";
                    //value8Cell.CssClass = "inputTitleCell4";
                    amount8Cell.CssClass = "inputTitleCell4";
                    //value9Cell.CssClass = "inputTitleCell4";
                    amount9Cell.CssClass = "inputTitleCell4";
                    //value10Cell.CssClass = "inputTitleCell4";
                    amount10Cell.CssClass = "inputTitleCell4";
                    //value11Cell.CssClass = "inputTitleCell4";
                    amount11Cell.CssClass = "inputTitleCell4";
                    totalAmountCell.CssClass = "inputTitleCell4";

                    creatorUserCell.CssClass = "inputTitleCell4";
                    enteredUserCell.CssClass = "inputTitleCell4";
                    enteredDateCell.CssClass = "inputTitleCell4";
                    statusCell.CssClass = "inputTitleCell4";

                    indexCell.Text = (startIndex + i).ToString();

                    if (items.ControlCashNotifications[i].statusId == 1)
                    {
                        //kioskIdCell.Text = items.ControlCashNotifications[i].kioskId.ToString();
                        kioskIdCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editEnterKioskEmptyCashButtonClicked(" + items.ControlCashNotifications[i].controlCashId + ");\" class=\"anylink\">" + items.ControlCashNotifications[i].kioskId.ToString() + "</a>";
                    }

                    if (items.ControlCashNotifications[i].statusId == 2)
                    {
                        //kioskIdCell.Text = items.ControlCashNotifications[i].kioskId.ToString();
                        kioskIdCell.Text = items.ControlCashNotifications[i].kioskId.ToString();
                    }


                    institutionId.Text = items.ControlCashNotifications[i].institutionId.ToString();
                    insertionDate.Text = items.ControlCashNotifications[i].insertionDate;

                    amount1Cell.Text = items.ControlCashNotifications[i].firstDenomNumber;
                    amount2Cell.Text = items.ControlCashNotifications[i].secondDenomNumber;
                    amount3Cell.Text = items.ControlCashNotifications[i].thirdDenomNumber;
                    amount4Cell.Text = items.ControlCashNotifications[i].fourthDenomNumber;
                    amount5Cell.Text = items.ControlCashNotifications[i].fifthDenomNumber;
                    amount6Cell.Text = items.ControlCashNotifications[i].sixthDenomNumber;
                    amount7Cell.Text = items.ControlCashNotifications[i].seventhDenomNumber;
                    amount8Cell.Text = items.ControlCashNotifications[i].eighthDenomNumber;
                    amount9Cell.Text = items.ControlCashNotifications[i].ninthDenomNumber;
                    amount10Cell.Text = items.ControlCashNotifications[i].tenthDenomNumber;
                    amount11Cell.Text = items.ControlCashNotifications[i].eleventhDenomNumber;

                    totalAmountCell.Text = items.ControlCashNotifications[i].sum + " TL";





                    creatorUserCell.Text = items.ControlCashNotifications[i].createdrUser;
                    enteredUserCell.Text = items.ControlCashNotifications[i].userCashOut;
                    enteredDateCell.Text = items.ControlCashNotifications[i].cashOutDate;
                    statusCell.Text = items.ControlCashNotifications[i].status;


                    Row[i].Cells.AddRange(new TableCell[]{
                    indexCell,
                    kioskIdCell,
                    institutionId,
                   
                    //value1Cell,
                    amount1Cell,
                    //value2Cell,
                    amount2Cell,
                    //value3Cell,
                    amount3Cell,
                   // value4Cell,
                    amount4Cell,
                    //value5Cell,
                    amount5Cell,
                    //value6Cell,
                    amount6Cell,
                    //value7Cell,
                    amount7Cell,
                   // value8Cell,
                    amount8Cell,
                   // value9Cell,
                    amount9Cell,
                    //value10Cell,
                    amount10Cell,
                    //value11Cell,
                    amount11Cell,
                    
                    totalAmountCell,
                     creatorUserCell,
                     insertionDate,
                   
                    enteredUserCell,
                    enteredDateCell,
                    statusCell
                });


                    //if (i == Row.Length - 1)
                    //{
                    //    Row[i].CssClass = "inputTitleCell99";
                    //    Row[i].Cells[1].Text = "TOPLAM";
                    //    Row[i].Cells[0].Text = "";
                    //}
                    //else
                    //{
                    if (i % 2 == 0)
                        Row[i].CssClass = "listrow";
                    else
                        Row[i].CssClass = "listRowAlternate";
                    //}
                }


                this.itemsTable.Rows.AddRange(Row);

                TableRow pagingRow = new TableRow();
                TableCell pagingCell = new TableCell();

                int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
                int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

                if (items.recordCount < currentRecordEnd)
                    currentRecordEnd = items.recordCount;

                if (currentRecordEnd > 0)
                {
                    this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + items.recordCount.ToString() + " kayıt bulundu.";
                }

                //items.pageCount = 10;

                pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
                pagingCell.HorizontalAlign = HorizontalAlign.Right;

                pagingCell.Text = WebUtilities.GetPagingText(items.pageCount, this.pageNum, items.recordCount);
                pagingRow.Cells.Add(pagingCell);
                this.itemsTable.Rows.AddAt(0, pagingRow);

                this.itemsTable.Visible = true;


                //if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
                //{
                    TableRow addNewRow = new TableRow();

                    TableCell addNewCell = new TableCell();
                    
                    TableCell spaceCell = new TableCell();
                    spaceCell.CssClass = "inputTitleCell99";
                    spaceCell.ColumnSpan = (3);

                    TableCell spaceCell2 = new TableCell();
                    spaceCell2.CssClass = "inputTitleCell99";
                    spaceCell2.ColumnSpan = (5);

                    addNewCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editEnterKioskEmptyCashButtonClicked('0');\" class=\"anylink\"><img src=../images/add.png border=0 title=\"Add New...\" /></a>";
                    //addNewCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editCutOfMonitoringButtonClicked('0');\" class=\"anylink\"><img src=../images/add.png border=0 title=\"Add New...\" /></a>";


                    TableCell totalfirstDenomNumberCell     = new TableCell();
                    TableCell totalsecondDenomNumberCell = new TableCell();
                    TableCell totalthirdDenomNumberCell = new TableCell();
                    TableCell totalfourthDenomNumberCell = new TableCell();
                    TableCell totalfifthDenomNumberCell = new TableCell();

                    TableCell totalsixthDenomNumberCell = new TableCell();
                    TableCell totalseventhDenomNumberCell = new TableCell();
                    TableCell totaleighthDenomNumberCell = new TableCell();
                    TableCell totalninthDenomNumberCell = new TableCell();
                    TableCell totaltenthDenomNumberCell = new TableCell();
                    TableCell totaleleventhDenomNumberCell = new TableCell();
                    TableCell totalAmount2Cell = new TableCell();


                    totalfirstDenomNumberCell.CssClass = "inputTitleCell99";
                    totalsecondDenomNumberCell.CssClass = "inputTitleCell99";
                    totalthirdDenomNumberCell.CssClass = "inputTitleCell99";
                    //totalCreditCardCell.CssClass = "inputTitleCell99";
                    totalfourthDenomNumberCell.CssClass = "inputTitleCell99";
                    totalfifthDenomNumberCell.CssClass = "inputTitleCell99";
                    totalsixthDenomNumberCell.CssClass = "inputTitleCell99";
                    totalseventhDenomNumberCell.CssClass = "inputTitleCell99";
                    totaleighthDenomNumberCell.CssClass = "inputTitleCell99";
                    totalninthDenomNumberCell.CssClass = "inputTitleCell99";
                    totaltenthDenomNumberCell.CssClass = "inputTitleCell99";
                    totaleleventhDenomNumberCell.CssClass = "inputTitleCell99";
                    totalAmount2Cell.CssClass = "inputTitleCell99";

                    totalfirstDenomNumberCell.Text  = items.totalfirstDenomNumber;
                    totalsecondDenomNumberCell.Text = items.totalsecondDenomNumber;
                    totalthirdDenomNumberCell.Text = items.totalthirdDenomNumber;
                    totalfourthDenomNumberCell.Text = items.totalfourthDenomNumber;
                    totalfifthDenomNumberCell.Text = items.totalfifthDenomNumber;

                    totalsixthDenomNumberCell.Text = items.totalsixthDenomNumber;
                    totalseventhDenomNumberCell.Text = items.totalseventhDenomNumber;
                    totaleighthDenomNumberCell.Text = items.totaleighthDenomNumber;
                    totalninthDenomNumberCell.Text = items.totalninthDenomNumber;
                    totaltenthDenomNumberCell.Text = items.totaltenthDenomNumber;
                    totaleleventhDenomNumberCell.Text = items.totaleleventhDenomNumber;
                    totalAmount2Cell.Text = items.totalAmount;

                    addNewRow.Cells.Add(spaceCell);
                    addNewRow.Cells.Add(totalfirstDenomNumberCell);
                    addNewRow.Cells.Add(totalsecondDenomNumberCell);
                    addNewRow.Cells.Add(totalthirdDenomNumberCell);
                    addNewRow.Cells.Add(totalfourthDenomNumberCell);
                    addNewRow.Cells.Add(totalfifthDenomNumberCell);
                    addNewRow.Cells.Add(totalsixthDenomNumberCell);
                    addNewRow.Cells.Add(totalseventhDenomNumberCell);
                    addNewRow.Cells.Add(totaleighthDenomNumberCell);
                    addNewRow.Cells.Add(totalninthDenomNumberCell);
                    addNewRow.Cells.Add(totaltenthDenomNumberCell);
                    addNewRow.Cells.Add(totaleleventhDenomNumberCell);
                    addNewRow.Cells.Add(totalAmount2Cell);
                    addNewRow.Cells.Add(spaceCell2);

                  
                    //addNewRow.Cells.Add(addNewCell);
                    this.itemsTable.Rows.Add(addNewRow);

                    TableRow addNewRow2 = new TableRow();

                    TableCell spaceCell3 = new TableCell();
                 
                    spaceCell3.ColumnSpan = (19);


                    addNewRow2.Cells.Add(spaceCell3);
                    addNewRow2.Cells.Add(addNewCell);

                    this.itemsTable.Rows.Add(addNewRow2);
                //}
            
            
            
            
            }
            else
            {
                //if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
                //{
                    TableRow addNewRow = new TableRow();
                    TableCell addNewCell = new TableCell();
                    TableCell spaceCell = new TableCell();
                    spaceCell.CssClass = "inputTitleCell4";
                    spaceCell.ColumnSpan = 19;

                    addNewCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editEnterKioskEmptyCashButtonClicked('0');\" class=\"anylink\"><img src=../images/add.png border=0 title=\"Add New...\" /></a>";

                    addNewRow.Cells.Add(spaceCell);
                    addNewRow.Cells.Add(addNewCell);
                    this.itemsTable.Rows.Add(addNewRow);
                //}
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListMutabakatBDT");
        }
    }



    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskId = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskId = "0";
            else
                kioskId = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebService = new CallWebServices();
            ParserListKioskControlCashNotification items = callWebService.CallControlCashNotification(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text).AddDays(0).AddSeconds(-1),
                                                                             Convert.ToDateTime(this.endDateField.Text).AddDays(0).AddSeconds(-1),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             Convert.ToInt32(ViewState["OrderDesc"]),
                                                                             kioskId,
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             2,
                                                                             Convert.ToInt32(this.isCardDeletedBox.SelectedValue)
                                                                             ,0);

            if (items != null)
            {
                if (items.errorCode == 0)
                {

                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    string[] headerNames = { "Kiosk No", "Kurum Ad", "İşlem Tarihi", "#1 / 200 TL", "#2 / 100 TL", "#3 / 50 TL",
                                               "#4 / 20 TL", "#5 / 10 TL", "#6 / 5 TL", "#7 / 1 TL", "#8 / 50 Kuruş", 
                                               "#9 / 25 Kuruş", "#10 / 10 Kuruş", "#11 / 5 Kuruş", "Bildiren Kullanıcı", "Giriş Yapan Kullanıcı", "Giriş Yapılma Tarih", "Toplam", "Durum", "Durum No", "KioskId","StatusId"};

                    //exportExcell.ExportExcellByBlock(lists.Kiosks, "Kiosk Raporu", headerNames);
                    exportExcell.ExportExcellByBlock(items.ControlCashNotifications, "Control Cash Notification Listesi", headerNames);
                }
                else if (items.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + items.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelControlCashNotification");
        }
    }


    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "KioskId":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Name.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Name.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Name.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "InstitutionId":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "FirstDenomNumber":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Name.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Name.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Name.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "SecondDenomNumber":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "ThirdDenomNumber":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Ip.Text = "<a>Kiosk IP    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Ip.Text = "<a>Kiosk IP    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Ip.Text = "<a>Kiosk IP    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "FourthDenomNumber":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Status.Text = "<a>Kiosk Durum    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Status.Text = "<a>Kiosk Durum    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Status.Text = "<a>Kiosk Durum    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "FifthDenomNumber":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //InsertionDate.Text = "<a>Eklenme Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // InsertionDate.Text = "<a>Eklenme Tarihi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //InsertionDate.Text = "<a>Eklenme Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "SixthDenomNumber":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //City.Text = "<a>Şehir    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // City.Text = "<a>Şehir    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // City.Text = "<a>Şehir    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "SeventhDenomNumber":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Region.Text = "<a>Bölge    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Region.Text = "<a>Bölge    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Region.Text = "<a>Bölge    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "EighthDenomNumber":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Latitude.Text = "<a>Enlem    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Latitude.Text = "<a>Enlem    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Latitude.Text = "<a>Enlem    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "NinthDenomNumber":
                if (orderSelectionColumn == 11)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Longitude.Text = "<a>Boylam    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Longitude.Text = "<a>Boylam    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Longitude.Text = "<a>Boylam    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 11;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "TenthDenomNumber":
                if (orderSelectionColumn == 12)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Type.Text = "<a>Kiosk Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Type.Text = "<a>Kiosk Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Type.Text = "<a>Kiosk Tipi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 12;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "EleventhDenomNumber":
                if (orderSelectionColumn == 13)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // KioskFee.Text = "<a>Kullanım Ücreti    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //KioskFee.Text = "<a>Kullanım Ücreti    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // KioskFee.Text = "<a>Kullanım Ücreti    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 13;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Sum":
                if (orderSelectionColumn == 14)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Sum.Text = "<a>Versiyon    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Sum.Text = "<a>Versiyon    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Sum.Text = "<a>Versiyon    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 14;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CreatorUser":
                if (orderSelectionColumn == 15)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //CreatorUser.Text = "<a>Calışma Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // CreatorUser.Text = "<a>Calışma Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //reatorUser.Text = "<a>Calışma Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 15;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "InsertionDate":
                if (orderSelectionColumn == 16)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransactionDate.Text = "<a>Kapasite Durumu  <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransactionDate.Text = "<a>Kapasite Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionDate.Text = "<a>Kapasite Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 16;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "EnteredUser":
                if (orderSelectionColumn == 17)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Capacity.Text = "<a>Kapasite Durumu  <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 17;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "EnteredDate":
                if (orderSelectionColumn == 18)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Capacity.Text = "<a>Kapasite Durumu  <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 18;
                    orderSelectionDescAsc = 1;
                }
                break;

            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }
}