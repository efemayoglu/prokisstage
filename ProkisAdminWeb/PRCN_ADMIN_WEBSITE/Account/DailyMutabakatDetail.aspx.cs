﻿using MakeMutabakatParserNS;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Account_DailyMutabakatDetail : System.Web.UI.Page
{
    ParserLogin LoginDatas = null;
    int diffrences = 0;
    int kioskId = 0;
    int instutionId = 0;
    DateTime mutabakatDate = DateTime.Now;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/MakeMutabakatWithDate.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }


        this.diffrences = Convert.ToInt32(Request.QueryString["whichDifferences"]);
        this.kioskId = Convert.ToInt32(Request.QueryString["kioskId"]);
        this.instutionId = Convert.ToInt32(Request.QueryString["instutionId"]);
        this.mutabakatDate = Convert.ToDateTime(Request.QueryString["mutabakatDate"]);


        if (this.diffrences == 1)
            this.articleDetailsTab.InnerText = "BÖS'de Olup Kurumda Olmayan İşlemler";
        if (this.diffrences == 2)
            this.articleDetailsTab.InnerText = "Kurumda Olup BÖS'de Olmayan İşlemler";
        if (this.diffrences == 3)
            this.articleDetailsTab.InnerText = "Kurumda ve BÖS'de Ortak Olan İşlemler";
        if (this.diffrences == 4)
            this.articleDetailsTab.InnerText = "Kurumda Tekrar Eden Kayıtlar";

        if (!Page.IsPostBack)
        {
            this.BuildControls();
        }
    }

    private void BuildControls()
    {
        ListDiffrenceTransaction();
    }

    public void ListDiffrenceTransaction()
    {
        try
        {
            CallWebServices callWebServ = new CallWebServices();
            //if (this.instutionId != 3)
            //{
            ParserListMutabakatTransaction datas = callWebServ.CallGetMakeMutabakatListValues(this.mutabakatDate
                                                                                 , this.kioskId
                                                                                 , this.instutionId
                                                                                 , this.LoginDatas.AccessToken
                                                                                 , this.LoginDatas.User.Id);
            Utility.WriteLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), "srkn", true, false);
            if (datas != null)
            {
                if (datas.ErrorCode == 0)
                {
                    if (this.instutionId == 2 || this.instutionId == 3)
                    {
                        //Utility.WriteLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), " Biz value: " + datas.DbDatasList.Length.ToString() + " BG value: " + datas.ET_DETAY.Length.ToString(), true, false);
                        if (this.diffrences == 1)
                            BindListTable(FindOnlyUs(datas), 1);
                        else if (this.diffrences == 2)
                            BindListTable(FindOnlyTheirs(datas), 2);
                        else if (this.diffrences == 3)
                            BindListTable(FindSame(datas), 3);
                    }
                    else if (this.instutionId == 1)
                    {
                        Utility.WriteLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), " Biz value: " + datas.DbDatasList.Length.ToString() + " BG value: " + datas.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc.Length.ToString(), true, false);
                        if (this.diffrences == 1)
                            BindListTable(FindOnlyUsAski(datas), 1);
                        else if (this.diffrences == 2)
                            BindListTable(FindOnlyTheirsAski(datas), 2);
                        else if (this.diffrences == 3)
                            BindListTable(FindSameAski(datas), 3);
                        else if (this.diffrences == 4)
                            BindListTable(FindMultiAski(datas), 4);
                    }
                    else if (this.instutionId == 4 || this.instutionId == 5)
                    {
                        Utility.WriteLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), " Biz value: " + datas.DbDatasList.Length.ToString() + " BG value: " + datas.Response.Count(), true, false);
                        if (this.diffrences == 1)
                            BindListTable(FindOnlyUsAskiDB(datas), 1);
                        else if (this.diffrences == 2)
                            BindListTable(FindOnlyTheirsAskiDB(datas), 5);
                        else if (this.diffrences == 3)
                            BindListTable(FindSameAskiDB(datas), 3);
                        else if (this.diffrences == 4)
                            BindListTable(FindMultiAskiDB(datas), 4);
                    }
                }
                else if (datas.ErrorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + datas.ErrorDescription + "'); ", true);
                }
            }
            else
            {
                Utility.WriteLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), "datas null", true, false);
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
            /*
            }
            else
            {
                ParserListMutabakatMaskiTransaction datas = callWebServ.CallGetMakeMutabakatListMaskiValues(this.mutabakatDate
                                                                     , this.kioskId
                                                                     , this.instutionId
                                                                     , this.LoginDatas.AccessToken
                                                                     , this.LoginDatas.User.Id);

                Utility.WriteLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), "srkn", true, false);
                if (datas != null)
                {
                    if (datas.ErrorCode == 0)
                    {
                        Utility.WriteLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), " Biz value: " + datas.DbDatasList.Length.ToString(), true, false);
                        if (this.diffrences == 1)
                            BindListTable(FindOnlyUsMaski(datas),1);
                        else if (this.diffrences == 2)
                            BindListTable(FindOnlyTheirsMaski(datas),2);
                        else if (this.diffrences == 3)
                            BindListTable(FindSameMaski(datas),3);
                    }
                    else if (datas.ErrorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                    {
                        Session.Abandon();
                        Session.RemoveAll();
                        Response.Redirect("../root/Login.aspx", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + datas.ErrorDescription + "'); ", true);
                    }
                }
                else
                {
                    Utility.WriteLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), "datas null", true, false);
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
                }
            }
*/

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionDetailsSrch");
        }
    }

    public List<DbDatasList> FindOnlyTheirs(ParserListMutabakatTransaction transactions)
    {

        List<DbDatasList> newList = new List<DbDatasList>();

        int index = 0;
        for (int i = 0; i < transactions.Response.Count; i++)
        {
            index = 0;
            string customerNo = "";
            for (int j = 0; j < transactions.DbDatasList.Length; j++)
            {
                if (transactions.DbDatasList[j].faturaIds == transactions.Response[i].bankReferenceNumber)
                {
                    customerNo = transactions.DbDatasList[j].aboneNo.ToString();
                    index = 1;
                    break;
                }

                // Utility.WriteLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), " Biz value: " + transactions.Faturalar[i].AboneID.Trim() + "-" + transactions.DbDatasList[j].aboneNo.Trim() + "     " + transactions.Faturalar[i].Tutar+"-"+(Convert.ToDouble(transactions.DbDatasList[j].amount)), true, false);

            }
            if (index == 0)
            {

                DbDatasList newTxn = new DbDatasList();
                newTxn.aboneNo = customerNo;
                newTxn.amount = transactions.Response[i].institutionAmount.ToString();
                newTxn.faturaIds = transactions.Response[i].bankReferenceNumber.ToString();
                newTxn.insertionDate = transactions.Response[i].insProcessDate.ToString();
                newTxn.transactionId = 0;
                newList.Add(newTxn);
            }
        }
        return newList;
    }



    public List<DbDatasList> FindOnlyTheirsAski(ParserListMutabakatTransaction transactions)
    {
        //string data1 = "",data2="";
        List<DbDatasList> newList = new List<DbDatasList>();


        int index = 0;

        for (int i = 0; i < transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc.Length; i++)
        {
            index = 0;
            if (transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[i].ISLEM_KODU == 1)
            {
                for (int j = 0; j < transactions.DbDatasList.Length; j++)
                {
                    if (transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[i].ABONENO == transactions.DbDatasList[j].aboneNo
                        && transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[i].SYSTARIH.Substring(0, 19) == transactions.DbDatasList[j].insertionDate)
                    {
                        index = 1;
                        break;
                    }
                }
                if (index == 0)
                {
                    DbDatasList newTxn = new DbDatasList();
                    newTxn.aboneNo = transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[i].ABONENO.ToString();
                    newTxn.amount = transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[i].SATISTUTARIYTL;
                    newTxn.faturaIds = transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[i].SATISMIKTARI;
                    newTxn.insertionDate = transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[i].SYSTARIH;
                    newTxn.transactionId = 0;
                    newList.Add(newTxn);
                }
            }
        }
        return newList;
    }

    public List<DbDatasList> FindOnlyTheirsAskiDB(ParserListMutabakatTransaction transactions)
    {
        //string data1 = "",data2="";
        List<DbDatasList> newList = new List<DbDatasList>();


        int index = 0;

        for (int i = 0; i < transactions.Response.Count; i++)
        {
            index = 0;
            if (transactions.Response[i].status == "A")
            {
                for (int j = 0; j < transactions.DbDatasList.Length; j++)
                {
                    if (transactions.Response[i].bankReferenceNumber == transactions.DbDatasList[j].aboneNo)
                    {
                        index = 1;
                        break;
                    }
                }
                if (index == 0)
                {
                    DbDatasList newTxn = new DbDatasList();
                    newTxn.aboneNo = "0";
                    newTxn.amount = transactions.Response[i].institutionAmount.ToString();
                    newTxn.faturaIds = transactions.Response[i].bankReferenceNumber;
                    newTxn.insertionDate = transactions.Response[i].insProcessDate;
                    newTxn.transactionId = 0;
                    newList.Add(newTxn);
                }
            }
        }
        return newList;
    }

    public List<DbDatasList> FindOnlyTheirsMaski(ParserListMutabakatMaskiTransaction transactions)
    {
        List<DbDatasList> newList = new List<DbDatasList>();
        int index = 0;
        for (int i = 0; i < transactions.SoapEnvelope.SoapBody.BankaDetayMutabakatResponse.BankaDetayMutabakatResult.DetayMutabakatResultModel2.Length; i++)
        {
            index = 0;
            for (int j = 0; j < transactions.DbDatasList.Length; j++)
            {
                if (transactions.SoapEnvelope.SoapBody.BankaDetayMutabakatResponse.BankaDetayMutabakatResult.DetayMutabakatResultModel2[i].AboneNo == transactions.DbDatasList[j].aboneNo
                    && transactions.SoapEnvelope.SoapBody.BankaDetayMutabakatResponse.BankaDetayMutabakatResult.DetayMutabakatResultModel2[i].Yil == transactions.DbDatasList[j].insertionDate)
                {
                    index = 1;
                    break;
                }
            }
            if (index == 0)
            {
                DbDatasList newTxn = new DbDatasList();
                newTxn.aboneNo = transactions.SoapEnvelope.SoapBody.BankaDetayMutabakatResponse.BankaDetayMutabakatResult.DetayMutabakatResultModel2[i].AboneNo;
                newTxn.amount = transactions.SoapEnvelope.SoapBody.BankaDetayMutabakatResponse.BankaDetayMutabakatResult.DetayMutabakatResultModel2[i].Tutar;
                newTxn.faturaIds = transactions.SoapEnvelope.SoapBody.BankaDetayMutabakatResponse.BankaDetayMutabakatResult.DetayMutabakatResultModel2[i].MakbuzNo;
                newTxn.insertionDate = transactions.SoapEnvelope.SoapBody.BankaDetayMutabakatResponse.BankaDetayMutabakatResult.DetayMutabakatResultModel2[i].Yil;
                newTxn.transactionId = 0;
                newList.Add(newTxn);
            }
        }
        return newList;
    }

    public List<DbDatasList> FindSame(ParserListMutabakatTransaction transactions)
    {
        List<DbDatasList> newList = new List<DbDatasList>();

        for (int i = 0; i < transactions.Response.Count; i++)
        {
            for (int j = 0; j < transactions.DbDatasList.Length; j++)
            {
                if (transactions.Response[i].bankReferenceNumber == transactions.DbDatasList[j].faturaIds)
                {
                    DbDatasList newTxn = new DbDatasList();
                    newTxn.aboneNo = transactions.Response[i].customerNumber;
                    newTxn.amount = transactions.Response[i].institutionAmount.ToString();
                    newTxn.faturaIds = transactions.Response[i].bankReferenceNumber;
                    newTxn.insertionDate = transactions.Response[i].insProcessDate;
                    newTxn.transactionId = 0;
                    newList.Add(newTxn);
                    break;
                }
            }
        }
        return newList;
    }

    public List<DbDatasList> FindSameAski(ParserListMutabakatTransaction transactions)
    {
        List<DbDatasList> newList = new List<DbDatasList>();

        for (int i = 0; i < transactions.DbDatasList.Length; i++)
        {

            for (int j = 0; j < transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc.Length; j++)
            {
                if (transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[j].ISLEM_KODU == 1)
                {
                    if (transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[j].ABONENO == transactions.DbDatasList[i].aboneNo
                        && transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[j].SYSTARIH.Substring(0, 19) == transactions.DbDatasList[i].insertionDate)
                    {
                        DbDatasList newTxn = new DbDatasList();
                        newTxn.aboneNo = transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[j].ABONENO;
                        newTxn.amount = transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[j].SATISTUTARIYTL;
                        newTxn.faturaIds = transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[j].CIHAZNO;
                        newTxn.insertionDate = transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[j].SYSTARIH;
                        newTxn.transactionId = 0;
                        newList.Add(newTxn);
                        break;
                    }
                }
            }
        }
        return newList;
    }

    public List<DbDatasList> FindSameAskiDB(ParserListMutabakatTransaction transactions)
    {
        List<DbDatasList> newList = new List<DbDatasList>();

        for (int i = 0; i < transactions.DbDatasList.Length; i++)
        {

            for (int j = 0; j < transactions.Response.Count; j++)
            {
                if (transactions.Response[j].status == "A")
                {
                    if (transactions.Response[j].bankReferenceNumber == transactions.DbDatasList[i].aboneNo)
                    {
                        DbDatasList newTxn = new DbDatasList();
                        newTxn.aboneNo = transactions.Response[j].bankReferenceNumber;
                        newTxn.amount = transactions.Response[j].institutionAmount.ToString();
                        newTxn.faturaIds = transactions.Response[j].bankReferenceNumber;
                        newTxn.insertionDate = transactions.Response[j].insProcessDate;
                        newTxn.transactionId = 0;
                        newList.Add(newTxn);
                        break;
                    }
                }
            }
        }
        return newList;
    }

    public List<DbDatasList> FindSameMaski(ParserListMutabakatMaskiTransaction transactions)
    {
        List<DbDatasList> newList = new List<DbDatasList>();

        for (int i = 0; i < transactions.SoapEnvelope.SoapBody.BankaDetayMutabakatResponse.BankaDetayMutabakatResult.DetayMutabakatResultModel2.Length; i++)
        {
            for (int j = 0; j < transactions.DbDatasList.Length; j++)
            {
                if (transactions.SoapEnvelope.SoapBody.BankaDetayMutabakatResponse.BankaDetayMutabakatResult.DetayMutabakatResultModel2[i].AboneNo == transactions.DbDatasList[j].aboneNo
                    && transactions.SoapEnvelope.SoapBody.BankaDetayMutabakatResponse.BankaDetayMutabakatResult.DetayMutabakatResultModel2[i].Yil == transactions.DbDatasList[j].insertionDate)
                {
                    DbDatasList newTxn = new DbDatasList();
                    newTxn.aboneNo = transactions.SoapEnvelope.SoapBody.BankaDetayMutabakatResponse.BankaDetayMutabakatResult.DetayMutabakatResultModel2[i].AboneNo;
                    newTxn.amount = transactions.SoapEnvelope.SoapBody.BankaDetayMutabakatResponse.BankaDetayMutabakatResult.DetayMutabakatResultModel2[i].Tutar;
                    newTxn.faturaIds = transactions.SoapEnvelope.SoapBody.BankaDetayMutabakatResponse.BankaDetayMutabakatResult.DetayMutabakatResultModel2[i].MakbuzNo;
                    newTxn.insertionDate = transactions.SoapEnvelope.SoapBody.BankaDetayMutabakatResponse.BankaDetayMutabakatResult.DetayMutabakatResultModel2[i].Yil;
                    newTxn.transactionId = 0;
                    newList.Add(newTxn);
                    break;
                }
            }
        }
        return newList;
    }

    public List<DbDatasList> FindOnlyUs(ParserListMutabakatTransaction transactions)
    {
        /*
        List<DbDatasList> newList = new List<DbDatasList>();
        int index = 0;
        for (int j = 0; j < transactions.DbDatasList.Length; j++)
        {
            index = 0;
            for (int i = 0; i < transactions.Faturalar.Length; i++)
            {
                if (transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[i].ISLEM_KODU == 1)
                {
                    if ((transactions.Faturalar[i].AboneID == transactions.DbDatasList[j].aboneNo
                        && (transactions.Faturalar[i].Tutar.ToString() == transactions.DbDatasList[j].amount ||
                         transactions.Faturalar[i].Tutar.ToString() == transactions.DbDatasList[j].amount.Substring(0, transactions.DbDatasList[j].amount.Length - 1) ||
                          transactions.Faturalar[i].Tutar.ToString() == transactions.DbDatasList[j].amount.Substring(0, transactions.DbDatasList[j].amount.Length - 2) ||
                        transactions.Faturalar[i].Tutar.ToString() == transactions.DbDatasList[j].amount.Substring(0, transactions.DbDatasList[j].amount.Length - 3)))
                          || (transactions.Faturalar[i].AboneID == transactions.DbDatasList[j].aboneNo
                        && transactions.DbDatasList[j].faturaIds.Contains(transactions.Faturalar[i].FaturaID) == true))
                    {
                        index = 1;
                        break;
                    }
                }
            }
            if (index == 0)
            {
                newList.Add(transactions.DbDatasList[j]);
            }
        }*/
        List<DbDatasList> newList = new List<DbDatasList>();
        int index = 0;
        for (int j = 0; j < transactions.DbDatasList.Length; j++)
        {
            index = 0;
            for (int i = 0; i < transactions.Response.Count; i++)
            {
                if (transactions.Response[i].status == "1")
                {
                    if (transactions.Response[i].bankReferenceNumber == transactions.DbDatasList[j].faturaIds)
                    {
                        index = 1;
                        break;
                    }
                }
            }
            if (index == 0)
            {
                newList.Add(transactions.DbDatasList[j]);
            }
        }
        return newList;
    }
    // İki aynı kayıt var mı kontrol etmek için yapıldı.
    public List<DbDatasList> FindMultiAski(ParserListMutabakatTransaction transactions)
    {
        List<DbDatasList> newList = new List<DbDatasList>();
        int index = 0, mon = 0;
        for (int j = 0; j < transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc.Length; j++)
        {
            index = 0;
            for (int i = 0; i < transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc.Length; i++)
            {
                if (transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[i].ABONENO == transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[j].ABONENO
                    && transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[i].SYSTARIH.Substring(0, 19) == transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[j].SYSTARIH.Substring(0, 19))
                {
                    index++;
                }
            }
            if (index > 1)
            {
                DbDatasList newTxn = new DbDatasList();
                newTxn.aboneNo = transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[j].ABONENO;
                newTxn.amount = transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[j].SATISTUTARIYTL;
                newTxn.faturaIds = transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[j].CIHAZNO;
                newTxn.insertionDate = transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[j].SYSTARIH;
                newTxn.transactionId = 0;
                newList.Add(newTxn);
            }
        }
        return newList;
    }

    public List<DbDatasList> FindMultiAskiDB(ParserListMutabakatTransaction transactions)
    {
        List<DbDatasList> newList = new List<DbDatasList>();
        int index = 0, mon = 0;
        for (int j = 0; j < transactions.Response.Count; j++)
        {
            index = 0;
            for (int i = 0; i < transactions.Response.Count; i++)
            {
                if (transactions.Response[i].bankReferenceNumber == transactions.Response[j].bankReferenceNumber)
                {
                    index++;
                }
            }
            if (index > 1)
            {
                DbDatasList newTxn = new DbDatasList();
                newTxn.aboneNo = transactions.Response[j].bankReferenceNumber;
                newTxn.amount = transactions.Response[j].institutionAmount.ToString();
                newTxn.faturaIds = transactions.Response[j].status;
                newTxn.insertionDate = transactions.Response[j].insProcessDate;
                newTxn.transactionId = 0;
                newList.Add(newTxn);
            }
        }
        return newList;
    }

    public List<DbDatasList> FindOnlyUsAski(ParserListMutabakatTransaction transactions)
    {
        List<DbDatasList> newList = new List<DbDatasList>();
        int index = 0, mon = 0;
        for (int j = 0; j < transactions.DbDatasList.Length; j++)
        {
            index = 0;
            for (int i = 0; i < transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc.Length; i++)
            {
                if (transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[i].ABONENO == transactions.DbDatasList[j].aboneNo
                    && transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[i].SYSTARIH.Substring(0, 19) == transactions.DbDatasList[j].insertionDate
                    && transactions.SoapEnvelope.SoapBody.SatisListesiResponse.SatisListesiResult.DiffgrDiffgram.DocumentElement.Sonuc[i].ISLEM_KODU == 1)
                {
                    index = 1;
                    break;
                }
            }
            if (index == 0)
            {
                newList.Add(transactions.DbDatasList[j]);
            }
        }
        return newList;
    }

    public List<DbDatasList> FindOnlyUsAskiDB(ParserListMutabakatTransaction transactions)
    {
        List<DbDatasList> newList = new List<DbDatasList>();
        int index = 0, mon = 0;
        for (int j = 0; j < transactions.DbDatasList.Length; j++)
        {
            index = 0;
            for (int i = 0; i < transactions.Response.Count; i++)
            {
                if (transactions.DbDatasList[j].aboneNo == transactions.Response[i].bankReferenceNumber && transactions.Response[i].status == "A")
                {
                    index = 1;
                    break;
                }
            }
            if (index == 0)
            {
                newList.Add(transactions.DbDatasList[j]);
            }
        }
        return newList;
    }

    public List<DbDatasList> FindOnlyUsMaski(ParserListMutabakatMaskiTransaction transactions)
    {
        List<DbDatasList> newList = new List<DbDatasList>();
        int index = 0;
        for (int j = 0; j < transactions.DbDatasList.Length; j++)
        {
            index = 0;
            for (int i = 0; i < transactions.SoapEnvelope.SoapBody.BankaDetayMutabakatResponse.BankaDetayMutabakatResult.DetayMutabakatResultModel2.Length; i++)
            {
                if (transactions.SoapEnvelope.SoapBody.BankaDetayMutabakatResponse.BankaDetayMutabakatResult.DetayMutabakatResultModel2[i].AboneNo == transactions.DbDatasList[j].aboneNo
                    && transactions.SoapEnvelope.SoapBody.BankaDetayMutabakatResponse.BankaDetayMutabakatResult.DetayMutabakatResultModel2[i].Yil == transactions.DbDatasList[j].insertionDate)
                {
                    index = 1;
                    break;
                }
            }
            if (index == 0)
            {
                newList.Add(transactions.DbDatasList[j]);
            }
        }
        return newList;
    }
    private void Clicked(object sender, EventArgs e)
    {
        var btn = sender as LinkButton;
        btn.Text = "Clicked";

        /*
        string redirectURL = "../Transaction/ListTransactions.aspx";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "window.open('vehicle_trackView.aspx');", true);

        Response.Redirect("../Transaction/ListTransactions.aspx", true);
         * */
    }

    private void BindListTable(List<DbDatasList> transactions, int state)
    {
        try
        {
            TableRow[] Row = new TableRow[transactions.Count];

            for (int i = 0; i < Row.Length; i++)
            {

                Row[i] = new TableRow();
                Row[i].Attributes.Add("transactionId", transactions[i].transactionId.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell aboneNoCell = new TableCell();
                TableCell amountCell = new TableCell();
                TableCell transactionDateCell = new TableCell();
                TableCell faturaIdCell = new TableCell();



                indexCell.CssClass = "inputTitleCell4";
                idCell.CssClass = "inputTitleCell4";
                aboneNoCell.CssClass = "inputTitleCell4";

                transactionDateCell.CssClass = "inputTitleCell4";
                faturaIdCell.CssClass = "inputTitleCell4";


                indexCell.Text = (1 + i).ToString();


                if (state == 5)
                {
                    aboneNoCell.Text = transactions[i].aboneNo;
                    amountCell.CssClass = "inputTitleCell4";
                    idCell.Text = transactions[i].transactionId.ToString();
                    faturaIdCell.Text = transactions[i].faturaIds.ToString();
                }
                if (state == 2)
                {
                    LinkButton lbtn = new LinkButton();
                    lbtn.Text = transactions[i].aboneNo;
                    lbtn.OnClientClick = "document.forms[0].target = '_blank';";
                    lbtn.PostBackUrl = "../Transaction/ListTransactions.aspx?custId=" + transactions[i].aboneNo;
                    idCell.Text = transactions[i].transactionId.ToString();
                    faturaIdCell.Text = transactions[i].faturaIds.ToString();
                    aboneNoCell.Controls.Add(lbtn);

                }
                else if (state == 1)
                {
                    LinkButton lbtn = new LinkButton();
                    lbtn.Text = transactions[i].transactionId.ToString();
                    lbtn.OnClientClick = "document.forms[0].target = '_blank';";
                    lbtn.PostBackUrl = "../Transaction/ListTransactionCondition.aspx?tranId=" + transactions[i].transactionId;
                    aboneNoCell.Text = transactions[i].aboneNo;
                    idCell.Controls.Add(lbtn);
                }
                else
                {
                    aboneNoCell.Text = transactions[i].aboneNo;
                    amountCell.CssClass = "inputTitleCell4";
                    idCell.Text = transactions[i].transactionId.ToString();
                }
                amountCell.Text = transactions[i].amount;
                transactionDateCell.Text = transactions[i].insertionDate.ToString();
                amountCell.HorizontalAlign = HorizontalAlign.Left;


                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        idCell,
						aboneNoCell,
                        amountCell,
                        faturaIdCell,
                        transactionDateCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListMutabakatTransactionBDT");
        }

    }
}