﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DailyMutabakatDetailAnalysis.aspx.cs" Inherits="Account_DailyMutabakatDetailAnalysis" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../styles/style.css" />
    <link rel="stylesheet" type="text/css" href="../styles/tab.css" />

    <script type="text/javascript" language="javascript" src="../js/wdws.js"></script>
    <script language="javascript" type="text/javascript">

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function postForPaging(selectedPageNum) {
            document.getElementById('pageNumRefField').value = selectedPageNum;
            document.getElementById('navigateButton').click();
        }

        function showTransactionDetailClicked(transactionID) {
            var retVal = showTransactionDetailWindow(transactionID);
        }

        function openCity(evt, cityName, index ) {
            // Declare all variables
            var i, tabcontent, tablinks;

            // Get all elements with class="tabcontent" and hide them
            tabcontent = document.getElementsByClassName("tabcontent");

            if (cityName == "Basarısız")
                tabcontent["Basarılı"].style.display = "none";
            else if (cityName == "Basarılı")
                tabcontent["Basarısız"].style.display = "none";

            if (cityName == "BasarısızKurum")
                tabcontent["BasarılıKurum"].style.display = "none";
            else if (cityName == "BasarılıKurum")
                tabcontent["BasarısızKurum"].style.display = "none";
            /*
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            */

            // Get all elements with class="tablinks" and remove the class "active"
            tablinks = document.getElementsByClassName("tablinks");
            //for (i = 0; i < tablinks.length; i++) {
                if (index == 2 || index == 3)
                {
                    tablinks[2].className = tablinks[2].className.replace(" active", "");
                    tablinks[3].className = tablinks[3].className.replace(" active", "");
                }
                else if (index == 1 || index == 0) {
                    tablinks[0].className = tablinks[0].className.replace(" active", "");
                    tablinks[1].className = tablinks[1].className.replace(" active", "");
                }
                    
            //}

            // Show the current tab, and add an "active" class to the button that opened the tab
  
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

    </script>
    <base target="_self" />
</head>


<body>
<form id="form1" runat="server">

                <div style="width: 100%;overflow:auto;">
                    <div style="float:left; width: 50%;">
                        
                        <div class="tab">
                          <button type="button" class="tablinks" onclick="openCity(event, 'Basarılı',0)">Başarılı İşlemler</button>
                          <button type="button" class="tablinks" onclick="openCity(event, 'Basarısız',1)">Başarısız İşlemler</button>
                        </div>

                        <div id="Basarılı" class="tabcontent" style="height:435px;">

                                <div style="float:left; width:85%">
                                     <b>Birleşik Ödeme (Başarılı)</b>
                                </div>
                                 <div style="float:right; width:15%">
                                    <asp:Image ID="Image1" ImageUrl="~/images/if_tick-shield_27249.png" runat="server" />
                                    <asp:Label ID="Success1" runat="server" Text="0"></asp:Label>
                                    <asp:Image ID="Image2" ImageUrl="~/images/if_cross-shield_26051.png" runat="server" />
                                    <asp:Label ID="Fail1" runat="server" Text="0"></asp:Label>
                                 </div>
                           <p>
                               <asp:Table ID="BOSSuccess" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                        BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="100%">
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="#"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="İşlem No"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Abone No"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Tutar"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Fatura Id"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Işlem Durumu"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="İşlem Zamanı"></asp:TableHeaderCell>
                                        </asp:TableRow>
                                </asp:Table>
                           </p>
                        </div>

                        <div id="Basarısız" class="tabcontent" style="height:435px;">
                              <div style="float:left; width:85%">
                                     <b>Birleşik Ödeme (İptal)</b>
                                </div>
                                 <div style="float:right; width:15%">
                                    <asp:Image ID="Image3" ImageUrl="~/images/if_tick-shield_27249.png" runat="server" />
                                    <asp:Label ID="Success2" runat="server" Text="0"></asp:Label>
                                    <asp:Image ID="Image4" ImageUrl="~/images/if_cross-shield_26051.png" runat="server" />
                                    <asp:Label ID="Fail2" runat="server" Text="0"></asp:Label>
                                 </div>
                           <p>
                               <asp:Table ID="BOSCancel" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                        BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="100%">
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3vR">
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="#"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="İşlem No"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Abone No"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Tutar"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Fatura Id"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Işlem Durumu"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="İşlem Zamanı"></asp:TableHeaderCell>
                                        </asp:TableRow>
                                </asp:Table>
                           </p> 
                        </div>

                    </div>
                    <div style="float:right; width: 50%;">
                        
                        <div class="tab">
                          <button type="button" class="tablinks" onclick="openCity(event, 'BasarılıKurum',2)">Başarılı İşlemler</button>
                          <button type="button" class="tablinks" onclick="openCity(event, 'BasarısızKurum',3)">Başarısız İşlemler</button>
                        </div>

                        <div id="BasarılıKurum" class="tabcontent" style="height:435px;">
                             <div style="float:left; width:85%">
                                     <b>Kurum Başarılı</b>
                                </div>
                                 <div style="float:right; width:15%">
                                    <asp:Image ID="Image5" ImageUrl="~/images/if_tick-shield_27249.png" runat="server" />
                                    <asp:Label ID="Success3" runat="server" Text="0"></asp:Label>
                                    <asp:Image ID="Image6" ImageUrl="~/images/if_cross-shield_26051.png" runat="server" />
                                    <asp:Label ID="Fail3" runat="server" Text="0"></asp:Label>
                                 </div>
                           <p>
                               <asp:Table ID="InstitutionSuccess" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                        BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="100%">
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="#"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="İşlem No"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Abone No"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Tutar"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Fatura Id"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Işlem Durumu"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="İşlem Zamanı"></asp:TableHeaderCell>
                                        </asp:TableRow>
                                </asp:Table>
                           </p>
                        </div>

                        <div id="BasarısızKurum" class="tabcontent" style="height:435px;">
                             <div style="float:left; width:85%">
                                     <b>Kurum (İptal)</b>
                                </div>
                                 <div style="float:right; width:15%">
                                    <asp:Image ID="Image7" ImageUrl="~/images/if_tick-shield_27249.png" runat="server" />
                                    <asp:Label ID="Success4" runat="server" Text="0"></asp:Label>
                                    <asp:Image ID="Image8" ImageUrl="~/images/if_cross-shield_26051.png" runat="server" />
                                    <asp:Label ID="Fail4" runat="server" Text="0"></asp:Label>
                                 </div>
                           <p>
                               <asp:Table ID="InsttituionCancel" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                        BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="100%">
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3vR">
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="#"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="İşlem No"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Abone No"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Tutar"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Fatura Id"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Işlem Durumu"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="İşlem Zamanı"></asp:TableHeaderCell>
                                        </asp:TableRow>
                                </asp:Table>
                           </p> 
                        </div>

                    </div>
                </div>

</form> 
</body>
</html>

