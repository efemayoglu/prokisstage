﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class webctrls_DeleteMoneyCode : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    public string whichPage = " ";
    public int itemId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas == null)
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        detailTextLabel.InnerText = "Silme Nedeni Giriniz !";
        messageArea.InnerText = "Para Kod Silme İşleminiz Gerçekleştirilecektir !";
        itemId = Convert.ToInt32(Request.QueryString["itemId"].ToString());
        whichPage = "AskiCode";
    }

    protected void okButton_Click(object sender, EventArgs e)
    {
        DeleteOperitaion();
    }

    private void DeleteOperitaion()
    {
        try
        {
            ParserOperation response = null;
            CallWebServices callWebServ = new CallWebServices();

            response = callWebServ.CallMoneyCodeDeleteService(this.LoginDatas.User.Id, this.detailText.Text, this.vitelTicketNoBox.Text, this.itemId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (response != null)
            {
                if (response.errorCode == 0)
                    ScriptManager.RegisterClientScriptBlock(this.okButton, this.okButton.GetType(), "CloseScript", "parent.hideModalPopupLastandRefresh('" + whichPage + "');", true);
                else if (response.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    Response.Redirect("../webctrls/OperationInfo.aspx?message=" + response.errorDescription, true);
                }
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "DeleteMoneyCode");
        }
    }
}