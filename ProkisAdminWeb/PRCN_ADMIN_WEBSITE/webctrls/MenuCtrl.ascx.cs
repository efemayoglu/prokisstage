﻿using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class webctrls_MenuCtrl : System.Web.UI.UserControl
{
    private ParserLogin LoginDatas;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }

    private void BindCtrls()
    {
        this.BuildControls();
        this.SetControls();
    }

    private void SetControls()
    {

    }

    private void BuildControls()
    {
        this.PrepareMenuData();
    }

    private void PrepareMenuData()
    {
        XmlDocument xml = new XmlDocument();

        if (!HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains("login.aspx"))
        {
            XmlElement root = xml.CreateElement("menu");
            xml.AppendChild(root);

            int mainMenuId = -1;

            XmlElement parent;
            XmlElement subpa;
            XmlElement span;
            XmlElement child = null;

            for (int i = 0; i < LoginDatas.UserRoles.Count; i++)
            {
                XmlElement subli;
                XmlElement suba;

                if (LoginDatas.UserRoles[i].MenuId != mainMenuId)
                {
                    parent = xml.CreateElement("li");
                    parent.SetAttribute("class", "top");
                    parent.SetAttribute("id", LoginDatas.UserRoles[i].MenuName);
                    root.AppendChild(parent);
                    subpa = xml.CreateElement("a");
                    subpa.SetAttribute("href", "javascript:void(0);");
                    subpa.SetAttribute("class", "top_link");
                    parent.AppendChild(subpa);
                    span = xml.CreateElement("span");
                    span.SetAttribute("class", "down");
                    span.InnerText = LoginDatas.UserRoles[i].MenuName;
                    subpa.AppendChild(span);
                    child = xml.CreateElement("ul");
                    child.SetAttribute("class", "sub");
                    parent.AppendChild(child);
                    subli = xml.CreateElement("li");
                    child.AppendChild(subli);
                    suba = xml.CreateElement("a");
                    suba.SetAttribute("href", LoginDatas.UserRoles[i].SubMenuURL);
                    suba.InnerText = LoginDatas.UserRoles[i].SubMenuName;
                    subli.AppendChild(suba);

                    mainMenuId = LoginDatas.UserRoles[i].MenuId;
                }
                else
                {
                    subli = xml.CreateElement("li");
                    child.AppendChild(subli);
                    suba = xml.CreateElement("a");
                    suba.SetAttribute("href", LoginDatas.UserRoles[i].SubMenuURL);
                    suba.InnerText = LoginDatas.UserRoles[i].SubMenuName;
                    subli.AppendChild(suba);

                }
            }
            XmlNode rootNode = xml.SelectSingleNode("menu");
            this.menuDataLCtrl.Text = rootNode.InnerXml;
        }
    }
}