﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DeleteMoneyCode.aspx.cs" Inherits="webctrls_DeleteMoneyCode" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />

    <script language="javascript" type="text/javascript"></script>
    <base target="_self" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel7" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <br />
                <br />
                <div id="detailTextLabel" align="center" class="messageArea" runat="server">&nbsp;</div>
                <table>
                    <tr valign="middle">
                        <td align="center" class="auto-style14">
                            <asp:Label ID="Label1" runat="server">Vitel Ticket No</asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="vitelTicketNoBox" CssClass="inputLine_250x40" runat="server" MaxLength="600"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="detailText" CssClass="inputLine_250x40" runat="server" MaxLength="600"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="okButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                OnClick="okButton_Click" Text="Tamam"></asp:Button>
                            <asp:Button ID="cancelButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                OnClientClick="parent.hideModalPopup2();" Text="Vazgeç"></asp:Button>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
