﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class webctrls_ModalPopupCtrl : System.Web.UI.UserControl
{
    public string popupUrl;
    public int popupWidth;
    public int popupHeight;
    public bool popupScrollable;
    public string targetControlID;
    public string cancelControlID;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }


    private void BindCtrls()
    {
        this.BuildControls();
        this.SetControls();
    }

    private void SetControls()
    {
        //this.popupIFrame.Attributes["src"] = this.popupUrl;
        //this.popupIFrame.Attributes["width"] = this.popupWidth.ToString();
        //this.popupIFrame.Attributes["height"] = this.popupHeight.ToString();
        //this.popupIFrame.Attributes["scrolling"] = this.popupScrollable.ToString();
    }

    private void BuildControls()
    {

    }
}