﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class webctrls_ApproveMutabakat : System.Web.UI.Page
{
    private ParserLogin LoginDatas;

      public string faturaAmount  = " ";
      public string faturaCount  = " ";
      public string kartDolumAmount = " "; 
      public string kartDolumCount = " ";
      public string totalCount  = " ";
      public string totalAmount  = " ";
      public string instutionCount  = " ";
      public string instutionAmount  = " ";
      public string date = " ";
      public string kioskId = " ";
      public string instutionId = " ";
      public string whichSide = " ";
      public string cancelAmount = " ";
      public string cancelCount = " ";

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas == null)
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }

        messageArea.InnerText = "Mutabakat Gerçekleştirilecektir!";
        faturaAmount = Request.QueryString["faturaAmount"].ToString();
        faturaCount = Request.QueryString["faturaCount"].ToString();
        kartDolumAmount = Request.QueryString["kartDolumAmount"].ToString();
        kartDolumCount = Request.QueryString["kartDolumCount"].ToString();
        totalCount = Request.QueryString["totalCount"].ToString();
        totalAmount = Request.QueryString["totalAmount"].ToString();
        instutionCount = Request.QueryString["instutionCount"].ToString();
        instutionAmount = Request.QueryString["instutionAmount"].ToString();
        date = Request.QueryString["date"].ToString();
        kioskId = Request.QueryString["kioskId"].ToString();
        instutionId = Request.QueryString["instutionId"].ToString();
        whichSide = Request.QueryString["WhichSide"].ToString();
        cancelAmount = Request.QueryString["cancelAmount"].ToString();
        cancelCount = Request.QueryString["cancelCount"].ToString();
    }

    protected void okButton_Click(object sender, EventArgs e)
    {
        MakeMutabakat();
    }

    private void MakeMutabakat()
    {
        try
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("cancelCount:"+cancelCount+" canAmount:"+cancelAmount), "MakeMutabakat");

            ParserOperation response = null;
            CallWebServices callWebServ = new CallWebServices();
                    response = callWebServ.CallMakeMutabakat(this.faturaAmount
                                                            , this.faturaCount
                                                            , this.kartDolumAmount
                                                            , this.kartDolumCount
                                                            , this.totalCount
                                                            , this.totalAmount
                                                            , this.instutionCount
                                                            , this.instutionAmount
                                                            , this.date
                                                            , this.kioskId
                                                            , this.instutionId
                                                            , this.whichSide
                                                            , this.LoginDatas.AccessToken
                                                            , this.LoginDatas.User.Id
                                                            , this.cancelAmount
                                                            , this.cancelCount);

            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("resp:"+response.errorDescription), "MakeMutabakat");


            if (response != null)
            {
                if (response.errorCode == 0)
                    Response.Redirect("../webctrls/OperationInfo.aspx?message=" + response.errorDescription, true);
                    //ScriptManager.RegisterClientScriptBlock(this.okButton, this.okButton.GetType(), "CloseScript", "parent.hideModalPopup2();", true);
                else if (response.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    Response.Redirect("../webctrls/OperationInfo.aspx?message=" + response.errorDescription, true);
                }
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "MakeMutabakat");
        }
    }
}