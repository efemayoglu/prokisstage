﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
//using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class webctrls_ShowDeleteWindow : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    public string storedProcedure = " ";
    public string whichPage = " ";
    public int itemId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas == null)
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }

        messageArea.InnerText = "İşleminiz Gerçekleştirilecektir !";
        storedProcedure = Request.QueryString["storedProcedure"].ToString();
        itemId = Convert.ToInt32(Request.QueryString["itemId"].ToString());
        whichPage = Request.QueryString["whichPage"].ToString();
    }

    protected void okButton_Click(object sender, EventArgs e)
    {
        DeleteOperitaion();
    }

    private void DeleteOperitaion()
    {
        try
        {
            ParserOperation response = null;
            CallWebServices callWebServ = new CallWebServices();
            if (Request.QueryString["whichPage"].ToString() == "FraudAdmin")
            {
                if (Request.QueryString["type"].ToString() == "app")
                {
                    response = callWebServ.CallOperationInformService(this.LoginDatas.User.Id, this.storedProcedure, this.itemId, 1, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                }
                else if (Request.QueryString["type"].ToString() == "del")
                {
                    response = callWebServ.CallOperationInformService(this.LoginDatas.User.Id, this.storedProcedure, this.itemId, 0, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                }
                else
                {
                    response = callWebServ.CallDeleteOperationService(this.LoginDatas.User.Id, this.storedProcedure, this.itemId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                }
            }
            else
            {
                response = callWebServ.CallDeleteOperationService(this.LoginDatas.User.Id, this.storedProcedure, this.itemId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            }
            if (response != null)
            {
                if (response.errorCode == 0)
                    ScriptManager.RegisterClientScriptBlock(this.okButton, this.okButton.GetType(), "CloseScript", "parent.hideModalPopupLastandRefresh('" + whichPage + "');", true);
                else if (response.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    Response.Redirect("../webctrls/OperationInfo.aspx?message=" + response.errorDescription, true);
                }
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowDeleteWindowDO");
        }
    }
}