using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using PRCNCORE.Parser;

public partial class webctrls_AdminSiteMPage : System.Web.UI.MasterPage
{
    private ParserLogin LoginDatas = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }

    private void BindCtrls()
    {
        this.BuildControls();
        this.SetControls();
    }

    private void SetControls()
    {
        if (Request.Url.AbsolutePath.Contains("/Login.aspx"))
        {
            this.lbl_logo.Visible = false;
            this.menuCtrl.Visible = false;
        }

        if (this.LoginDatas != null)
        {
            this.SetUserRights();
        }

    }

    private void BuildControls()
    {
        this.SetUsernameLabel();
    }

    protected void logoutImage_Click(object sender, ImageClickEventArgs e)
    {
        this.LogoutImage();
    }

    private void LogoutImage()
    {
        Session.Abandon();
        FormsAuthentication.SignOut();
        Response.Redirect("~/Default.aspx", true);    
    }

    private void SetUsernameLabel()
    {
        if (this.LoginDatas != null)
        {
            if (this.LoginDatas.User != null)
            {
                this.lastUnSuccessLgn.Visible = true;
                this.usernameLabel.Text = this.LoginDatas.User.Name;
                this.lastUnsuccessLogin.Text = this.LoginDatas.User.LastWrongAccessTryDate.ToString("yyyy.MM.dd HH:mm");
            }
        }
    }

    public void SetTitleLabel(string pageTitle)
    {
        this.pageTitleLabel.Text = pageTitle;
    }

    private void SetUserRights()
    {
      
    }

    private MenuItem GetMenuItem(MenuItemCollection menuItems, string menuItemValue)
    {
        MenuItem selected = null;

        foreach (MenuItem aMenuItem in menuItems)
        {
            if (aMenuItem.Value == menuItemValue)
            {
                selected = aMenuItem;
                break;
            }
        }
        return selected;
    }
}
