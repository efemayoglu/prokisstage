﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class webctrls_MessageWindow : System.Web.UI.Page
{
    public string mainModalPopupId = "";
    
    protected void Page_Load(object sender, EventArgs e)
    { 
        messageArea.InnerText = Request.QueryString["messageText"].ToString();
        mainModalPopupId = Request.QueryString["mainModalPopupId"].ToString();
    }

   protected void okButton_Click(object sender, EventArgs e)
    {
        CloseMessageWindow();
    }

    private void CloseMessageWindow()
    {
        ScriptManager.RegisterClientScriptBlock(this.okButton, this.okButton.GetType(), "CloseScript", "parent.hideModalPopupLastandPrevious('" + mainModalPopupId + "');", true);
    }    
}
