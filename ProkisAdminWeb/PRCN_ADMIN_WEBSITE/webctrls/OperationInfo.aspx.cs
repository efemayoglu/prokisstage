﻿using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class webctrls_OperationInfo : System.Web.UI.Page
{
    string message = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        message = Request.QueryString["message"].ToString();
        messageArea.InnerHtml = message;
    }
    protected void okButton_Click(object sender, EventArgs e)
    {
        try
        {
            ScriptManager.RegisterClientScriptBlock(this.okButton, this.okButton.GetType(), "CloseScript", "parent.hideModalPopup2();", true);
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "OperationInfo");
        }
    }
}
