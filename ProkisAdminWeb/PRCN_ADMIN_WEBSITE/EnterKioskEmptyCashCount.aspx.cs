﻿using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_EnterKioskEmptyCashCount : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private int ownSubMenuIndex = -1;
    private ParserListEmptyKioskNames kiosks;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/EnterKioskEmptyCashCount.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }

    private void BindCtrls()
    {

        CallWebServices callWebServ = new CallWebServices();
        if (this.LoginDatas != null)
        {
            if (this.LoginDatas.User != null)
            {
                if (this.LoginDatas.User.Id != 0)
                {
                    this.kiosks = callWebServ.CallGetKioskNameService(this.LoginDatas.User.Id, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);


                    if (this.kiosks != null)
                    {
                        if (this.kiosks.errorCode == 0)
                        {
                            this.kioskBox.DataSource = this.kiosks.emptyKioskNames;
                            this.kioskBox.DataBind();
                        }
                        else if (kiosks.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                        {
                            Session.Abandon();
                            Session.RemoveAll();
                            Response.Redirect("../root/Login.aspx", true);
                        }
                        else if (kiosks.errorCode == 2)
                            this.messageArea.InnerHtml = "İlişkili Kiosk bulunamadı !";
                        else
                            this.messageArea.InnerHtml = kiosks.errorDescription;
                    }
                    else
                        this.messageArea.InnerHtml = "Sisteme erişilemiyor !";
                }
            }
        }

        this.SetControls();
    }

    private void SetControls()
    {
        try
        {
            ListItem item = new ListItem();
            item.Text = "Kiosk Seçiniz";
            item.Value = "0";
            this.kioskBox.Items.Add(item);
            this.kioskBox.SelectedValue = "0";

            this.TotalAmountTb.Text = "0";
            this.CashYpeTB1.Text = "0";
            this.CashYpeTB2.Text = "0";
            this.CashYpeTB3.Text = "0";
            this.CashYpeTB4.Text = "0";
            this.CashYpeTB5.Text = "0";
            this.CashYpeTB6.Text = "0";
            this.CashYpeTB7.Text = "0";
            this.CashYpeTB8.Text = "0";
            this.CashYpeTB9.Text = "0";
            this.CashYpeTB10.Text = "0";
            this.CashYpeTB11.Text = "0";

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "EnterKioskEmptyCashCountSC");
        }
    }

    protected void CashYpeTB1_TextChanged(object sender, EventArgs e)
    {
        TextBox xsenderTB = (TextBox)sender;

        if (String.IsNullOrEmpty(xsenderTB.Text))
            xsenderTB.Text = "0";


        double totalAmount = (Convert.ToInt32(this.CashYpeTB1.Text) * 200 +
                                    Convert.ToInt32(this.CashYpeTB2.Text) * 100 +
                                    Convert.ToInt32(this.CashYpeTB3.Text) * 50 +
                                    Convert.ToInt32(this.CashYpeTB4.Text) * 20 +
                                    Convert.ToInt32(this.CashYpeTB5.Text) * 10 +
                                    Convert.ToInt32(this.CashYpeTB6.Text) * 5 +
                                    Convert.ToInt32(this.CashYpeTB7.Text) * 1 +
                                    Convert.ToInt32(this.CashYpeTB8.Text) / 2.0 +
                                    Convert.ToInt32(this.CashYpeTB9.Text) / 4.0 +
                                    Convert.ToInt32(this.CashYpeTB10.Text) / 10.0 +
                                    Convert.ToInt32(this.CashYpeTB11.Text) / 20.0);
        this.TotalAmountTb.Text = totalAmount.ToString();
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        SaveCashCount();
    }

    private void SaveCashCount()
    {
        try
        {

            int KioskId = 0;
            CallWebServices callWebServ = new CallWebServices();

            this.kiosks = callWebServ.CallGetKioskNameService(this.LoginDatas.User.Id, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (this.kiosks != null)
            {
                if (this.kiosks.errorCode == 0)
                {
                    for (int i = 0; i < this.kiosks.emptyKioskNames.Count; i++)
                    {
                        if (this.kiosks.emptyKioskNames[i].id== Convert.ToInt32(this.kioskBox.SelectedValue))
                        {
                            KioskId = this.kiosks.emptyKioskNames[i].emptyKioskId;
                            break;
                        }
                    }
                }
                else if (this.kiosks.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
            }

            JArray jsonArray = new JArray();

            JObject item1 = new JObject();
            item1.Add("KioskId", KioskId);
            item1.Add("ExpertUserId", this.LoginDatas.User.Id);
            item1.Add("CashTypeId", 1);
            item1.Add("Count", Convert.ToInt32(this.CashYpeTB1.Text));
            item1.Add("KioskEmptyId", Convert.ToInt32(this.kioskBox.SelectedValue));
            jsonArray.Add(item1);

            JObject item2 = new JObject();
            item2.Add("KioskId", KioskId);
            item2.Add("ExpertUserId", this.LoginDatas.User.Id);
            item2.Add("CashTypeId", 2);
            item2.Add("Count", Convert.ToInt32(this.CashYpeTB2.Text));
            item2.Add("KioskEmptyId", Convert.ToInt32(this.kioskBox.SelectedValue));
            jsonArray.Add(item2);

            JObject item3 = new JObject();
            item3.Add("KioskId", KioskId);
            item3.Add("ExpertUserId", this.LoginDatas.User.Id);
            item3.Add("CashTypeId", 3);
            item3.Add("Count", Convert.ToInt32(this.CashYpeTB3.Text));
            item3.Add("KioskEmptyId", Convert.ToInt32(this.kioskBox.SelectedValue));
            jsonArray.Add(item3);

            JObject item4 = new JObject();
            item4.Add("KioskId", KioskId);
            item4.Add("ExpertUserId", this.LoginDatas.User.Id);
            item4.Add("CashTypeId", 4);
            item4.Add("Count", Convert.ToInt32(this.CashYpeTB4.Text));
            item4.Add("KioskEmptyId", Convert.ToInt32(this.kioskBox.SelectedValue));
            jsonArray.Add(item4);

            JObject item5 = new JObject();
            item5.Add("KioskId", KioskId);
            item5.Add("ExpertUserId", this.LoginDatas.User.Id);
            item5.Add("CashTypeId", 5);
            item5.Add("Count", Convert.ToInt32(this.CashYpeTB5.Text));
            item5.Add("KioskEmptyId", Convert.ToInt32(this.kioskBox.SelectedValue));
            jsonArray.Add(item5);

            JObject item6 = new JObject();
            item6.Add("KioskId", KioskId);
            item6.Add("ExpertUserId", this.LoginDatas.User.Id);
            item6.Add("CashTypeId", 6);
            item6.Add("Count", Convert.ToInt32(this.CashYpeTB6.Text));
            item6.Add("KioskEmptyId", Convert.ToInt32(this.kioskBox.SelectedValue));
            jsonArray.Add(item6);

            JObject item7 = new JObject();
            item7.Add("KioskId", KioskId);
            item7.Add("ExpertUserId", this.LoginDatas.User.Id);
            item7.Add("CashTypeId", 7);
            item7.Add("Count", Convert.ToInt32(this.CashYpeTB7.Text));
            item7.Add("KioskEmptyId", Convert.ToInt32(this.kioskBox.SelectedValue));
            jsonArray.Add(item7);

            JObject item8 = new JObject();
            item8.Add("KioskId", KioskId);
            item8.Add("ExpertUserId", this.LoginDatas.User.Id);
            item8.Add("CashTypeId", 8);
            item8.Add("Count", Convert.ToInt32(this.CashYpeTB8.Text));
            item8.Add("KioskEmptyId", Convert.ToInt32(this.kioskBox.SelectedValue));
            jsonArray.Add(item8);

            JObject item9 = new JObject();
            item9.Add("KioskId", KioskId);
            item9.Add("ExpertUserId", this.LoginDatas.User.Id);
            item9.Add("CashTypeId", 9);
            item9.Add("Count", Convert.ToInt32(this.CashYpeTB9.Text));
            item9.Add("KioskEmptyId", Convert.ToInt32(this.kioskBox.SelectedValue));
            jsonArray.Add(item9);

            JObject item10 = new JObject();
            item10.Add("KioskId", KioskId);
            item10.Add("ExpertUserId", this.LoginDatas.User.Id);
            item10.Add("CashTypeId", 10);
            item10.Add("Count", Convert.ToInt32(this.CashYpeTB10.Text));
            item10.Add("KioskEmptyId", Convert.ToInt32(this.kioskBox.SelectedValue));
            jsonArray.Add(item10);

            JObject item11 = new JObject();
            item11.Add("KioskId", KioskId);
            item11.Add("ExpertUserId", this.LoginDatas.User.Id);
            item11.Add("CashTypeId", 11);
            item11.Add("Count", Convert.ToInt32(this.CashYpeTB11.Text));
            item11.Add("KioskEmptyId", Convert.ToInt32(this.kioskBox.SelectedValue));
            jsonArray.Add(item11);

                                                                                                                                                                                                                                                                  
            ParserSaveKioskEmptyCashCount parserSaveKioskEmptyCashCount = callWebServ.CallSaveKioskEmptyCashCountService(jsonArray, KioskId, this.LoginDatas.User.Id, this.codeField.Text, Convert.ToInt32(this.kioskBox.SelectedValue), this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (parserSaveKioskEmptyCashCount != null)
            {
                if (parserSaveKioskEmptyCashCount.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    this.messageArea.InnerHtml = parserSaveKioskEmptyCashCount.errorDescription;
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('" + parserSaveKioskEmptyCashCount.errorDescription + "'); ", true);
                }
            }
            else
            {
                this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowUserRolesSv");
        }
    }
}