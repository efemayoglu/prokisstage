﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_ChangePassword : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../User/ChangePassword.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }
    }

    protected void saveButton_Click(object sender, System.EventArgs e)
    {
        try
        {
            if (Utility.CalculateSHA512Pin(this.oldPasswordField.Text) == this.LoginDatas.User.Password)
            {
                string oldPasswords = this.LoginDatas.User.LastPasswords;
                string newPassword = Utility.CalculateSHA512Pin(this.newPasswordField.Text);

                string newOldPasswords = "";

                if (oldPasswords.IndexOf(newPassword) == -1)
                {
                    if (oldPasswords.Length == 200)
                    {
                        newOldPasswords = oldPasswords.Substring(40, 160);
                        newOldPasswords += newPassword;
                    }
                    else
                    {
                        newOldPasswords = oldPasswords;
                        newOldPasswords += newPassword;
                    }

                    CallWebServices callWebServ = new CallWebServices();
                    ParserOperation response = callWebServ.CallChangeUserPasswordService(this.LoginDatas.User.Id, newPassword, newOldPasswords, this.LoginDatas.User.Id, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                  
                    if (response != null)
                    {
                        if (response.errorCode == 0)
                        {
                            this.messageArea.InnerHtml = "Şifre başarılı bir şekilde değiştirildi";

                            Session.Add("LoginData", LoginDatas);
                            Session.Remove("TempLoginData");

                            Response.Redirect("../root/Default.aspx");
                        }
                        else if (response.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                        {
                            Session.Abandon();
                            Session.RemoveAll();
                            Response.Redirect("../root/Login.aspx", true);
                        }
                        else
                        {

                            this.messageArea.InnerHtml = response.errorDescription;
                        }
                    }
                    else
                    {
                        this.messageArea.InnerHtml = "Sisteme erişilemiyor.";
                    }
                }
                else
                {
                    this.messageArea.InnerHtml = "Yeni Şifreniz son 5 şifrenizden farklı olmalıdır.";
                }
            }
            else
            {
                this.messageArea.InnerHtml = "Eski şifreniz hatalı !";
            }

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ChangePasswordSv");
        }

    }
}