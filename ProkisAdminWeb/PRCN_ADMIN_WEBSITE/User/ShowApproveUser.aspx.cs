﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
//using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_ShowApproveUser : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private int userId = 0;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../User/ApproveUser.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1" || this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
            {
            }
            else
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.userId = Convert.ToInt32(Request.QueryString["itemID"]);
        this.hiddenUserId.Value = Request.QueryString["itemID"];
        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }

    private void BindCtrls()
    {
        try
        {


            this.SetControls();
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowApproveUserBC");
        }
    }

    private void SetControls()
    {
        try
        {

            CallWebServices callWebServ = new CallWebServices();
            ParserGetUser user = callWebServ.CallGetUserService(this.userId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (user.User != null)
            {

                this.hiddenUserId.Value = user.User[0].Id.ToString();
                this.nameField.Text = user.User[0].Name;
                this.userNameField.Text = user.User[0].Username;
                this.mailField.Text = user.User[0].Email;
                this.cellPhoneField.Text = user.User[0].Cellphone;
                this.telephoneField.Text = user.User[0].Telephone;
                this.roleField.Text = user.User[0].RoleName;
                this.statusField.Text = user.User[0].StatusName;
            }
            else
            {

            }
            ParserGetUser newUser = callWebServ.CallGetApproveUserService(this.userId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (newUser.User != null)
            {

                this.hiddenUserId.Value = newUser.User[0].Id.ToString();
                this.newNameField.Text = newUser.User[0].Name;
                this.newUsernameField.Text = newUser.User[0].Username;
                this.newEmailField.Text = newUser.User[0].Email;
                this.newCellphoneField.Text = newUser.User[0].Cellphone;
                this.newPhoneField.Text = newUser.User[0].Telephone;
                this.newRoleField.Text = newUser.User[0].RoleName;
                this.newStatusField.Text = newUser.User[0].StatusName;
            }
            else
            {

            }

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowApproveUserSC");
        }
    }
}
