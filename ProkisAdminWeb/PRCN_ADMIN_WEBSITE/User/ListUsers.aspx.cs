﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_ListUsers : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 8;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
       this.LoginDatas = (ParserLogin)Session["LoginData"];

       if (this.LoginDatas != null)
       {
           for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
           {
               if (this.LoginDatas.UserRoles[i].SubMenuURL == "../User/ListUsers.aspx")
               {
                   this.ownSubMenuIndex = i;
                   break;
               }
           }
       }
       else
       {
           Session.Abandon();
           Response.Redirect("../root/Login.aspx", true);
       }
       if (this.ownSubMenuIndex == -1)
       {
           Response.Redirect("../Default.aspx", true);
       }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
        {
            if (this.LoginDatas.UserRoles[i].SubMenuURL == "../User/ListUsers.aspx")
            {
                this.ownSubMenuIndex = i;
                break;
            }
        }

        if (!Page.IsPostBack)
        {
            ViewState["OrderColumn"] = 8;
            ViewState["OrderDesc"] = 1;

            this.pageNum = 0;
            this.SearchOrder(8, 1);
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(8, 1);
    }

    public void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListUsers users = callWebServ.CallListUserService(this.searchTextField.Text,
                                                                    this.numberOfItemsPerPage,
                                                                    this.pageNum,
                                                                    recordCount,
                                                                    pageCount,
                                                                    orderSelectionColumn,
                                                                    descAsc, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (users != null)
            {
                if (users.errorCode == 0)
                {
                    this.BindListTable(users, users.recordCount, users.pageCount);
                }
                else if (users.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + users.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListUsersSrch");
        }
    }

    private void BindListTable(ParserListUsers users, int recordcount, int pagecount)
    {
        try
        {
            TableRow[] Row = new TableRow[users.Users.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("Id", users.Users[i].Id.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell nameCell = new TableCell();
                TableCell userNameCell = new TableCell();
                TableCell eMailCell = new TableCell();
                TableCell cellphoneCell = new TableCell();
                TableCell telephoneCell = new TableCell();
                TableCell userRoleCell = new TableCell();
                TableCell userStatusCell = new TableCell();
                TableCell creationDateCell = new TableCell();
                TableCell detailsCell = new TableCell();

                TableCell fakeCell1 = new TableCell();
                TableCell fakeCell2 = new TableCell();
                TableCell fakeCell3 = new TableCell();
                TableCell fakeCell4 = new TableCell();
                TableCell fakeCell5 = new TableCell();
                TableCell fakeCell6 = new TableCell();
                TableCell fakeCell7 = new TableCell();

                fakeCell1.CssClass = "inputTitleCell4";
                fakeCell2.CssClass = "inputTitleCell4";
                fakeCell3.CssClass = "inputTitleCell4";
                fakeCell4.CssClass = "inputTitleCell4";
                fakeCell5.CssClass = "inputTitleCell4";
                fakeCell6.CssClass = "inputTitleCell4";
                fakeCell7.CssClass = "inputTitleCell4";

                fakeCell1.Visible = false;
                fakeCell2.Visible = false;
                fakeCell3.Visible = false;
                fakeCell4.Visible = false;
                fakeCell5.Visible = false;
                fakeCell6.Visible = false;
                fakeCell7.Visible = false;

                indexCell.CssClass = "inputTitleCell4";
                idCell.CssClass = "inputTitleCell4";
                nameCell.CssClass = "inputTitleCell4";
                userNameCell.CssClass = "inputTitleCell4";
                eMailCell.CssClass = "inputTitleCell4";
                cellphoneCell.CssClass = "inputTitleCell4";
                telephoneCell.CssClass = "inputTitleCell4";
                userRoleCell.CssClass = "inputTitleCell4";
                userStatusCell.CssClass = "inputTitleCell4";
                creationDateCell.CssClass = "inputTitleCell4";
                detailsCell.CssClass = "inputTitleCell4";

                nameCell.Width = Unit.Pixel(150);
                nameCell.HorizontalAlign = HorizontalAlign.Center;
                nameCell.Style.Add("padding-left", "5px");

                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1")
                {
                    nameCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editButtonClicked(" + users.Users[i].Id + ");\" class=\"anylink\">" + users.Users[i].Name + "</a>";
                }
                else
                {
                    nameCell.Text = users.Users[i].Name;
                }

                userNameCell.Width = Unit.Pixel(100);
                userNameCell.HorizontalAlign = HorizontalAlign.Center;
                userNameCell.Style.Add("padding-left", "5px");
                userNameCell.Text = users.Users[i].Username;

                eMailCell.Width = Unit.Pixel(100);
                eMailCell.HorizontalAlign = HorizontalAlign.Center;
                eMailCell.Style.Add("padding-left", "5px");
                eMailCell.Text = users.Users[i].Email;


                cellphoneCell.Width = Unit.Pixel(100);
                cellphoneCell.HorizontalAlign = HorizontalAlign.Center;
                cellphoneCell.Style.Add("padding-left", "5px");
                cellphoneCell.Text = users.Users[i].Cellphone;


                telephoneCell.Width = Unit.Pixel(100);
                telephoneCell.HorizontalAlign = HorizontalAlign.Center;
                telephoneCell.Style.Add("padding-left", "5px");
                telephoneCell.Text = users.Users[i].Telephone;

                userRoleCell.Width = Unit.Pixel(100);
                userRoleCell.HorizontalAlign = HorizontalAlign.Center;
                userRoleCell.Style.Add("padding-left", "5px");
                userRoleCell.Text = users.Users[i].RoleName;

                userStatusCell.Width = Unit.Pixel(100);
                userStatusCell.HorizontalAlign = HorizontalAlign.Center;
                userStatusCell.Style.Add("padding-left", "5px");
                userStatusCell.Text = users.Users[i].StatusName.ToString();

                creationDateCell.Width = Unit.Pixel(100);
                creationDateCell.HorizontalAlign = HorizontalAlign.Center;
                creationDateCell.Style.Add("padding-left", "5px");
                creationDateCell.Text = users.Users[i].CreatedDate.ToString();

                indexCell.Width = Unit.Pixel(30);
                indexCell.Text = (startIndex + i).ToString();
                idCell.Visible = false;
                idCell.Text = users.Users[i].Id.ToString();

                detailsCell.Width = Unit.Pixel(75);

                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanDelete == "1")
                {
                    detailsCell.Text = "<a href=# onClick=\"changeUserPasswordClicked(" + users.Users[i].Id + ");\" class=\"changePasswordButton\" Title=\"Şifre Değiştir\"></a>" +
                         "<a href=# onClick=\"deleteButtonClicked('[pk].[delete_user]'," + users.Users[i].Id + ");\"><img src=../images/delete.png border=0 title=\"Delete\" /></a>";
                }
                else
                {
                    detailsCell.Text = " ";
                }

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
						idCell,
                        fakeCell1,
                        fakeCell2,
                        fakeCell3,
                        fakeCell4,
                        fakeCell5,
                        fakeCell6,
                        fakeCell7,
                        nameCell,
                        userNameCell,
                        eMailCell,
                        cellphoneCell,
                        telephoneCell,
                        userRoleCell,
                        userStatusCell,
                        creationDateCell,
                        detailsCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(pagecount, this.pageNum, recordcount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;

            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
            {
                TableRow addNewRow = new TableRow();
                TableCell addNewCell = new TableCell();
                TableCell spaceCell = new TableCell();
                spaceCell.CssClass = "inputTitleCell4";
                spaceCell.ColumnSpan = (this.itemsTable.Rows[1].Cells.Count - 1);
                addNewCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editButtonClicked('0');\" class=\"anylink\"><img src=../images/add.png border=0 title=\"Add New...\" /></a>";

                addNewRow.Cells.Add(spaceCell);
                addNewRow.Cells.Add(addNewCell);
                this.itemsTable.Rows.Add(addNewRow);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListUsersBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    protected void deleteButton_Click(object sender, EventArgs e)
    {

        this.SearchOrder(8, 1);
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "Name":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        Name.Text = "<a>Ad Soyad    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        Name.Text = "<a>Ad Soyad    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    Name.Text = "<a>Ad Soyad    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "UserName":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        UserName.Text = "<a>Kullanıcı Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        UserName.Text = "<a>Kullanıcı Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    UserName.Text = "<a>Kullanıcı Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "EMail":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        EMail.Text = "<a>E-mail    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        EMail.Text = "<a>E-mail    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    EMail.Text = "<a>E-mail    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Cellphone":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        Cellphone.Text = "<a>Cep Telefonu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        Cellphone.Text = "<a>Cep Telefonu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    Cellphone.Text = "<a>Cep Telefonu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Telephone":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        Telephone.Text = "<a>Telefonu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        Telephone.Text = "<a>Telefonu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    Telephone.Text = "<a>Telefonu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "UserRole":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        UserRole.Text = "<a>Rolü    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        UserRole.Text = "<a>Rolü    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    UserRole.Text = "<a>Rolü    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "UserStatus":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        UserStatus.Text = "<a>Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        UserStatus.Text = "<a>Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    UserStatus.Text = "<a>Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CreationDate":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        CreationDate.Text = "<a>Oluşturulma Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        CreationDate.Text = "<a>Oluşturulma Tarihi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    CreationDate.Text = "<a>Oluşturulma Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;
            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }


}