﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
//using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_ApproveUser : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
       this.LoginDatas = (ParserLogin)Session["LoginData"];

       if (this.LoginDatas != null)
       {
           for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
           {
               if (this.LoginDatas.UserRoles[i].SubMenuURL == "../User/ApproveUser.aspx")
               {
                   this.ownSubMenuIndex = i;
                   break;
               }
           }
       }
       else
       {
           Session.Abandon();
           Response.Redirect("../root/Login.aspx", true);
       }
       if (this.ownSubMenuIndex == -1)
       {
           Response.Redirect("../Default.aspx", true);
       }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            this.pageNum = 0;
            this.SearchOrder();
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder();
    }

    public void SearchOrder()
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListApproveUsers users = callWebServ.CallListApproveUserService(this.searchTextField.Text,
                                                                    this.numberOfItemsPerPage,
                                                                    this.pageNum,
                                                                    recordCount,
                                                                    pageCount, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (users != null)
            {
                if (users.errorCode == 0)
                {
                    this.BindListTable(users);
                }
                else if (users.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + users.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListUsersSrch");
        }
    }

    private void BindListTable(ParserListApproveUsers users)
    {
        try
        {
            TableRow[] Row = new TableRow[users.Users.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("Id", users.Users[i].Id.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell nameCell = new TableCell();
                TableCell userNameCell = new TableCell();
                TableCell eMailCell = new TableCell();
                TableCell cellphoneCell = new TableCell();
                TableCell telephoneCell = new TableCell();
                TableCell userRoleCell = new TableCell();
                TableCell userStatusCell = new TableCell();
                TableCell updatedDateCell = new TableCell();
                TableCell updatedUserCell = new TableCell();
                TableCell detailsCell = new TableCell();



                indexCell.CssClass = "inputTitleCell4";
                idCell.CssClass = "inputTitleCell4";
                nameCell.CssClass = "inputTitleCell4";
                userNameCell.CssClass = "inputTitleCell4";
                eMailCell.CssClass = "inputTitleCell4";
                cellphoneCell.CssClass = "inputTitleCell4";
                telephoneCell.CssClass = "inputTitleCell4";
                userRoleCell.CssClass = "inputTitleCell4";
                userStatusCell.CssClass = "inputTitleCell4";
                updatedDateCell.CssClass = "inputTitleCell4";
                updatedUserCell.CssClass = "inputTitleCell4";
                detailsCell.CssClass = "inputTitleCell4";

                nameCell.Width = Unit.Pixel(150);
                nameCell.HorizontalAlign = HorizontalAlign.Center;
                nameCell.Style.Add("padding-left", "5px");

                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1")
                {
                    nameCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editButtonClicked(" + users.Users[i].Id + ");\" class=\"anylink\">" + users.Users[i].Name + "</a>";
                }
                else
                {
                    nameCell.Text = users.Users[i].Name;
                }

                userNameCell.Text = users.Users[i].Username;
                eMailCell.Text = users.Users[i].Email;
                cellphoneCell.Text = users.Users[i].Cellphone;
                telephoneCell.Text = users.Users[i].Telephone;
                userRoleCell.Text = users.Users[i].RoleName;
                userStatusCell.Text = users.Users[i].StatusName;
                updatedDateCell.Text = users.Users[i].UpdatedDate;
                updatedUserCell.Text = users.Users[i].UpdatedUser;

                indexCell.Width = Unit.Pixel(30);
                indexCell.Text = (startIndex + i).ToString();
                idCell.Visible = false;
                idCell.Text = users.Users[i].Id.ToString();

                detailsCell.Width = Unit.Pixel(75);

                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanDelete == "1")
                {
                    detailsCell.Text = detailsCell.Text = "<a href=\"javascript: approveButtonClicked('[pk].[approve_user]'," + users.Users[i].Id + ");\" class=\"approveButton\" title=\"Onayla\"></a>"
                        + "<a href=# onClick=\"deleteButtonClicked('[pk].[delete_waiting_approve_user]'," + users.Users[i].Id + ");\"><img src=../images/delete.png border=0 title=\"Silme\" /></a>";
                }
                else
                {
                    detailsCell.Text = " ";
                }

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
						idCell,
                        nameCell,
                        userNameCell,
                        eMailCell,
                        cellphoneCell,
                        telephoneCell,
                        userRoleCell,
                        userStatusCell,
                        updatedDateCell,
                        updatedUserCell,
                        detailsCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(users.pageCount, this.pageNum, users.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
       
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListUsersBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.SearchOrder();
    }
}