﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
//using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_ShowUser : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private int userId = 0;
    private ParserGetUser user;
    private ParserListStatusName statusNames;
    private ParserListRoleName roleNames;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../User/ListUsers.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1" || this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd== "1")
            {
            }
            else
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.userId = Convert.ToInt32(Request.QueryString["itemID"]);
        this.hiddenUserId.Value = Request.QueryString["itemID"];
        this.passwordField.Enabled = false;
        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }

    private void BindCtrls()
    {
        try
        {
            CallWebServices callWebServ = new CallWebServices();

            this.user = callWebServ.CallGetUserService(this.userId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            this.roleNames = callWebServ.CallGetRoleNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            this.statusNames = callWebServ.CallGetStatusNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (statusNames != null)
            {
                this.userStatusBox.DataSource = this.statusNames.StatusNames;
                this.userStatusBox.DataBind();
            }
            if (roleNames != null)
            {
                this.userRoleBox.DataSource = this.roleNames.RoleNames;
                this.userRoleBox.DataBind();
            }
            this.SetControls();
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowUserBC");
        }
    }

    private void SetControls()
    {
        try
        {
            if (this.user.User != null)
            {
                //this.passwordField.Enabled = false;
                this.genPassButton.Enabled = false;
                this.genPassButton.Visible = false;
                this.hiddenUserId.Value = this.user.User[0].Id.ToString();
                this.nameField.Text = this.user.User[0].Name;
                this.userNameField.Text = this.user.User[0].Username;
                this.mailField.Text = this.user.User[0].Email;
                this.cellPhoneField.Text = this.user.User[0].Cellphone;
                this.telephoneField.Text = this.user.User[0].Telephone;
                this.userRoleBox.SelectedValue = this.user.User[0].RoleId.ToString();
                this.userRoleBox.DataBind();
                this.userStatusBox.SelectedValue = this.user.User[0].UserStatusId.ToString();
                this.userStatusBox.DataBind();
            }
            else
            {

            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowUserSC");
        }
    }

    protected void generatePassButton_Click(object sender, EventArgs e)
    {
        GeneratePass();
    }

    private void GeneratePass()
    {
        this.passwordField.Text = Utility.GeneratePassword(8, true);
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        SaveUser();
    }

    private void SaveUser()
    {
        try
        {
            CallWebServices callWebServ = new CallWebServices();

            ParserOperation operationResult = null;

            if (this.userId == 0)
            {
                operationResult = callWebServ.CallSaveUserService(this.nameField.Text, this.userNameField.Text, this.mailField.Text, this.cellPhoneField.Text, this.telephoneField.Text, Convert.ToInt32(this.userRoleBox.SelectedValue), Convert.ToInt32(this.userStatusBox.SelectedValue), Utility.CalculateSHA512Pin(this.passwordField.Text), this.LoginDatas.User.Id, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            }
            else
            {
                operationResult = callWebServ.CallUpdateUserService(this.userId, this.nameField.Text, this.userNameField.Text, this.mailField.Text, this.cellPhoneField.Text, this.telephoneField.Text, Convert.ToInt32(this.userRoleBox.SelectedValue), Convert.ToInt32(this.userStatusBox.SelectedValue), this.LoginDatas.User.Id, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            }

            if (operationResult != null)
            {
                if (operationResult.errorCode != 0)
                {
                    this.messageArea.InnerHtml = operationResult.errorDescription;
                }
                else
                {
                    this.messageArea.InnerHtml = operationResult.errorDescription;
                    ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", "parent.hideModalPopup2();", true);
                }
            }
            else
            {
                this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowUserSv");
        }
    }
}
