﻿
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_GenerateUserPassword : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;
    private int userId;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../User/ListUsers.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1" || this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanDelete == "1")
            {
            }
            else
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.userId = Convert.ToInt32(Request.QueryString["userId"]);

        //this.passwordField.Enabled = false;

        if (!this.IsPostBack)
        {
            //this.passwordField.Text = Utility.GeneratePassword(8, true);
        }

    }

    protected void saveButton_Click(object sender, System.EventArgs e)
    {
        this.saveButton.Enabled = false;
        string scriptText = "";
        try
        {
            if (this.userId == 0)
                this.userId = this.LoginDatas.User.Id;
            CallWebServices callWebServ = new CallWebServices();
            ParserOperation response = callWebServ.CallChangeUserPasswordAdminService(this.userId, this.LoginDatas.User.Id, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (response != null)
            {

                if (response.errorCode == 0)
                {
                    this.messageArea.InnerHtml = "Şifre başarılı bir şekilde değiştirildi";

                    scriptText = "parent.showMessageWindowUser('İşlem başarılı.');";
                    ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);
                }
                else if (response.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    this.saveButton.Enabled = true;
                    this.messageArea.InnerHtml = response.errorDescription;
                }
            }
            else
            {
                this.messageArea.InnerHtml = "Sisteme erişilemiyor.";
            }

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GenerateUserPasswordSv");
        }

    }

    //protected void generatePassButton_Click(object sender, EventArgs e)
    //{
    //    GeneratePass();
    //}

    //private void GeneratePass()
    //{
    //    this.passwordField.Text = Utility.GeneratePassword(8, true);
    //}
}