﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_ChangePasswordLogin : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    ///
    /// \brief Page_Load copies data to adminuser 
    /// \details Page_Load gets the session information
    /// \param[in] sender defaultParameter
    /// \param[in] e defaultParameter
    ///
    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["TempLoginData"];
    }

    /// \brief saveButton_Click is buttonEvent 
    /// \details saveButton_Click saves the new password
    /// \param[in] sender defaultParameter
    /// \param[in] e defaultParameter
    ///
    protected void saveButton_Click(object sender, System.EventArgs e)
    {
        try
        {
            if (Utility.CalculateSHA512Pin(this.oldPasswordField.Text) == this.LoginDatas.User.Password)
            {
                string oldPasswords = this.LoginDatas.User.LastPasswords;
                string newPassword = Utility.CalculateSHA512Pin(this.passwordField1.Text);

                string newOldPasswords = "";

                if (oldPasswords.IndexOf(newPassword) == -1)
                {
                    if (oldPasswords.Length == 640)
                    {
                        newOldPasswords = oldPasswords.Substring(128, 512);
                        newOldPasswords += newPassword;
                    }
                    else
                    {
                        newOldPasswords = oldPasswords;
                        newOldPasswords += newPassword;
                    }

                    CallWebServices callWebServ = new CallWebServices();
                    ParserOperation response = callWebServ.CallChangeUserPasswordService(this.LoginDatas.User.Id,
                        newPassword, newOldPasswords, this.LoginDatas.User.Id, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                    if (response != null)
                    {
                        if (response.errorCode == 0)
                        {
                            this.messageArea.InnerHtml = "Şifre başarılı bir şekilde değiştirildi";

                            Session.Add("LoginData", LoginDatas);
                            Session.Remove("TempLoginData");

                            Response.Redirect("../root/Default.aspx");
                        }
                        else if (response.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                        {
                            Session.Abandon();
                            Session.RemoveAll();
                            Response.Redirect("../root/Login.aspx", true);
                        }
                        else
                        {

                            this.messageArea.InnerHtml = response.errorDescription;
                        }
                    }
                    else
                    {
                        this.messageArea.InnerHtml = "Sisteme erişilemiyor.";
                    }
                }
                else
                {
                    this.messageArea.InnerHtml = "Yeni Şifreniz son 5 şifrenizden farklı olmalıdır.";
                }
            }
            else
            {
                this.messageArea.InnerHtml = "Eski şifreniz hatalı !";
            }

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ChangePasswordLogin");
        }
    }
}