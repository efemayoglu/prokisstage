﻿<%@ Page Language="C#" CodeFile="ChangePassword.aspx.cs" Inherits="User_ChangePassword" MasterPageFile="~/webctrls/AdminSiteMPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<%@ Register Namespace="ClickableWebControl" TagPrefix="clkweb" %>
<asp:Content ID="pageContent" ContentPlaceHolderID="mainCPHolder" runat="server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { setInterval(function () { cache_clear() }, 120000); });
        function cache_clear() { window.location.reload(true); }
    </script>
    <script language="javascript" type="text/javascript">

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        var anUpperCase = /[A-Z]/;
        var aLowerCase = /[a-z]/;
        var aNumber = /[0-9]/;
        var aSpecial = /[!|@|#|$|%|^|&|*|(|)|-|_]/;

        function checkPasswordComplexity(passValue) {
            if (passValue.length < 8 || passValue.search(anUpperCase) == -1 ||
                passValue.search(aLowerCase) == -1 || passValue.search(aNumber) == -1 || passValue.search(aSpecial) == -1) {
                resp = false;
            }
            else {
                resp = true;
            }
            return resp;
        }

        function checkForm() {
            var retVal = prepareFormForSubmitting();
            return retVal;
        }

        function prepareFormForSubmitting() {
            var status = true;
            var message = "";

            if (document.getElementById('oldPasswordField').value == "") {
                status = false;
                message = "Eski Şifrenizi giriniz.";
            }
            else if (document.getElementById('newPasswordField').value == "") {
                status = false;
                message = "Yeni Şifrenizi giriniz.";
            }
            else if (document.getElementById('reNewPasswordField').value == "") {
                status = false;
                message = "Yeni Şifrenizi tekrar giriniz.";
            }
            else if (document.getElementById('newPasswordField').value != document.getElementById('reNewPasswordField').value) {
                status = false;
                message = "Yeni Şifreler eşleşmiyor.";
            }
            else if (checkPasswordComplexity(document.getElementById('newPasswordField').value) == false) {
                status = false;
                message = "Şifre büyük harf, küçük harf, rakam ve özel karakter içermelidir.";
            }
            document.getElementById('messageArea').innerHTML = message;
            return status;
        }

    </script>
    <script type="text/javascript" language="javascript" src="../js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery.tablednd_0_5.js"></script>
    <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>
    <div>&nbsp;</div>
    <div align="center">
        <asp:Panel ID="panel1" DefaultButton="saveButton" runat="server" CssClass="containerPanel_95Pxauto">
            <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp</div>
            <div align="left" class="windowTitle_container_autox30">
                Şifre Değiştirme<hr style="border-bottom: 1px solid #b2b2b4;" />
            </div>
            <table cellspacing="0" cellpadding="0" border="0" id="upTable" runat="server" style="width: 90%">
                <tr>
                    <td align="center" valign="top">
                        <table cellpadding="2" cellspacing="2" style="width: 21%">
                            <tr>
                                <td align="left" class="inputTitle">Eski Şifre:
                                </td>
                                <td align="left" class="inputLine">
                                    <asp:TextBox ID="oldPasswordField" CssClass="inputLine_150x20" runat="server" TextMode="Password"
                                        ClientIDMode="Static"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="inputTitle">Yeni Şifre:
                                </td>
                                <td align="left" class="inputLine">
                                    <asp:TextBox ID="newPasswordField" CssClass="inputLine_150x20" runat="server" TextMode="Password"
                                        ClientIDMode="Static"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="inputTitle">Yeni Şifre (Tekrar):
                                </td>
                                <td align="left" class="inputLine">
                                    <asp:TextBox ID="reNewPasswordField" CssClass="inputLine_150x20" runat="server" TextMode="Password"
                                        ClientIDMode="Static"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table>
                            <tr>
                                <td>
                                    <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                        OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Kaydet"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <br />
    <table width="95%">
        <tr align="right">
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="width: 80px">
                <asp:Label ID="Label5" runat="server" Text="powered by" CssClass="poweredbyTitle"></asp:Label>

            </td>
            <td style="width: 100px">
                <asp:LinkButton ID="LinkButton1" href="http://www.birlesikodeme.com/" runat="server" Text="BİRLEŞİK ÖDEME" CssClass="procenneTitle"></asp:LinkButton>

            </td>
        </tr>
    </table>
</asp:Content>
