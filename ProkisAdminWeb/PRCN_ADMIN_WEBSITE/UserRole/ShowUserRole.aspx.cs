﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
//using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserRole_ShowUserRole : System.Web.UI.Page
{

    private ParserLogin LoginDatas = null;
    private ParserGetRole role;
    private int ownSubMenuIndex = -1;
    int roleId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../UserRole/ListUserRoles.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1" || this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
            {
            }
            else
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }

        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }

    private void BindCtrls()
    {
        this.SetControls();
    }

    private void SetControls()
    {
        try
        {
            /*
            roleId = Convert.ToInt32(Request.QueryString["itemID"]);
            if (roleId != 0)
            {
                CallWebServices callWebServ = new CallWebServices();
                this.role = callWebServ.CallGetRoleService(roleId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                if (this.role != null)
                {
                    this.nameField.Text = this.role.Role.Name;
                    this.descriptionField.Text = this.role.Role.Description;

                    for (int i = 0; i < this.role.RoleSubMenus.Count; i++)
                    {
                        ((CheckBox)userRolesTable.FindControl("view_" + this.role.RoleSubMenus[i].SubMenuId)).Checked = true;

                        if (this.role.RoleSubMenus[i].CanAdd == 1)
                        {
                            ((CheckBox)userRolesTable.FindControl("add_" + this.role.RoleSubMenus[i].SubMenuId)).Checked = true;
                        }
                        if (this.role.RoleSubMenus[i].CanDelete == 1)
                        {
                            ((CheckBox)userRolesTable.FindControl("delete_" + this.role.RoleSubMenus[i].SubMenuId)).Checked = true;
                        }
                        if (this.role.RoleSubMenus[i].CanEdit == 1)
                        {
                            ((CheckBox)userRolesTable.FindControl("edit_" + this.role.RoleSubMenus[i].SubMenuId)).Checked = true;
                        }
                    }
                }
            }
            */
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowUserRolesSC");
        }
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        SaveUserRole();
    }

    private void SaveUserRole()
    {
        try
        {
            int errorCode = 0;
            CallWebServices callWebServ = new CallWebServices();

            roleId = Convert.ToInt32(Request.QueryString["itemID"]);

            if (roleId == 0)
            {
                ParserSaveRole savedRole = callWebServ.CallSaveRoleService(this.nameField.Text, this.descriptionField.Text, this.LoginDatas.User.Id, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                errorCode = savedRole.errorCode;

                if (savedRole != null)
                {
                    JArray jsonArray = new JArray();

                    for (int i = 1; i < this.userRolesTable.Rows.Count; i++)
                    {
                        int view = 0;
                        int edit = 0;
                        int delete = 0;
                        int add = 0;
                        string idvalue = this.userRolesTable.Rows[i].ID.Substring(1, this.userRolesTable.Rows[i].ID.Length - 1);

                        if (((CheckBox)userRolesTable.FindControl("view_" + idvalue)).Checked == true)
                        {
                            view = 1;
                        }
                        if (((CheckBox)userRolesTable.FindControl("add_" + idvalue)).Checked == true)
                        {
                            add = 1;
                            view = 1;
                        }
                        if (((CheckBox)userRolesTable.FindControl("delete_" + idvalue)).Checked == true)
                        {
                            delete = 1;
                            view = 1;
                        }
                        if (((CheckBox)userRolesTable.FindControl("edit_" + idvalue)).Checked == true)
                        {
                            edit = 1;
                            view = 1;
                        }

                        if (view == 1)
                        {
                            JObject item = new JObject();
                            item.Add("roleId", savedRole.roleId);
                            item.Add("canEdit", edit);
                            item.Add("canAdd", add);
                            item.Add("canDelete", delete);
                            item.Add("subMenuId", Convert.ToInt32(idvalue));
                            jsonArray.Add(item);
                        }
                    }

                    ParserSaveRoleSubMenus parserSaveRoleSubMenus = callWebServ.CallSaveRoleSubMenusService(jsonArray, this.LoginDatas.User.Id, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                    errorCode = parserSaveRoleSubMenus.errorCode;
                }
            }
            else
            {
                ParserUpdateRole updatedRole = callWebServ.CallUpdateRoleService(roleId, this.nameField.Text, this.descriptionField.Text, this.LoginDatas.User.Id, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

                if (updatedRole.errorCode == 0)
                {
                    JArray jsonArray = new JArray();

                    for (int i = 1; i < this.userRolesTable.Rows.Count; i++)
                    {
                        int view = 0;
                        int edit = 0;
                        int delete = 0;
                        int add = 0;
                        string idvalue = this.userRolesTable.Rows[i].ID.Substring(1, this.userRolesTable.Rows[i].ID.Length - 1);

                        if (((CheckBox)userRolesTable.FindControl("view_" + idvalue)).Checked == true)
                        {
                            view = 1;
                        }
                        if (((CheckBox)userRolesTable.FindControl("add_" + idvalue)).Checked == true)
                        {
                            add = 1;
                            view = 1;
                        }
                        if (((CheckBox)userRolesTable.FindControl("delete_" + idvalue)).Checked == true)
                        {
                            delete = 1;
                            view = 1;
                        }
                        if (((CheckBox)userRolesTable.FindControl("edit_" + idvalue)).Checked == true)
                        {
                            edit = 1;
                            view = 1;
                        }

                        if (view == 1)
                        {
                            JObject item = new JObject();
                            item.Add("roleId", roleId);
                            item.Add("canEdit", edit);
                            item.Add("canAdd", add);
                            item.Add("canDelete", delete);
                            item.Add("subMenuId", Convert.ToInt32(idvalue));
                            jsonArray.Add(item);
                        }
                    }

                    ParserSaveRoleSubMenus parserSaveRoleSubMenus = callWebServ.CallSaveRoleSubMenusService(jsonArray, this.LoginDatas.User.Id, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                    errorCode = parserSaveRoleSubMenus.errorCode;

                }
                else if (updatedRole.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
            }


            string scriptText = "";

            switch (errorCode)
            {
                case (int)ManagementScreenErrorCodes.SystemError:
                    this.messageArea.InnerHtml = "Sistemde bir hata oluştu. Lütfen tekrar deneyin.";
                    break;
                case (int)ManagementScreenErrorCodes.SQLServerError:
                    this.messageArea.InnerHtml = "Sistemde bir hata oluştu. Lütfen tekrar deneyin. (SQL Server Error)";
                    break;
                case -2:
                    this.messageArea.InnerHtml = "Kayıt daha önceden oluşturulmuş.";
                    break;
                default:
                    this.messageArea.InnerHtml = "Rol verisi başarıyla kaydedilmiştir.";
                    scriptText = "parent.hideModalPopup2();";
                    break;
            }
            if (scriptText != "")
            {
                ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowUserRolesSv");
        }
    }

}