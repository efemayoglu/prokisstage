﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowApproveUserRole.aspx.cs" Inherits="UserRole_ShowApproveUserRole" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />

    <script language="javascript" type="text/javascript">

        function checkForm() {
            var status = true;
            var messageText = "";

            if (document.getElementById('nameField').value == "") {
                status = false;
                messageText = "Rol Adı giriniz.";
            }
            else if (document.getElementById('descriptionField').value == "") {
                status = false;
                messageText = "Rol Açıklaması giriniz.";
            }
            else {
                messageText = "";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }
    </script>
    <base target="_self" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel7" DefaultButton="cancelButton" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <div align="left" id="brandDetailsTab" class="windowTitle_container_autox30">
                    Rol Onaylama Detayı
                </div>
                <br />
                <div align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="userRoleListPanel" runat="server" CssClass="containerPanel_95Pxauto">
                                    <table border="0" cellpadding="2" cellspacing="0" id="Table3" runat="server">
                                        <tr valign="middle">
                                            <td align="left" class="staticTextLine_200x20">Eski Role Adı :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="oldnameField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr valign="middle">
                                            <td align="left" class="staticTextLine_200x20">Eski Rol Açıklama :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="olddescriptionField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <div align="left" class="windowTitle_container_autox30">
                                        Eski Menüler<hr style="border-bottom: 1px solid #b2b2b4;" />
                                    </div>
                                    <table cellpadding="0" cellspacing="0" width="95%" id="upTable" runat="server">
                                        <tr>
                                            <td align="center" class="tableStyle1">
                                                <asp:UpdatePanel ID="userRoleListUPanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" RenderMode="Block">
                                                    <ContentTemplate>
                                                        <asp:Table ID="userRolesTable" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                                            BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="95%">
                                                            <asp:TableRow CssClass="inputTitleCell3" Style="border-color: White; border-width: 1px; border-style: Double;">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Alt Menüler</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Görüntüle</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Güncelleme</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Ekleme</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Silme</asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m1">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kullanıcılar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_1" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_1" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_1" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_1" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m2">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Roller</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_2" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_2" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_2" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_2" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m3">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Tanımları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_3" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_3" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_3" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_3" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m4">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Şifre Değiştirme</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_4" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_4" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_4" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_4" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m5">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">İşlemler</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_5" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_5" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_5" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_5" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m6">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Müşteriler</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_6" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_6" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_6" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_6" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m7">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Komutları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_7" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_7" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_7" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_7" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m8">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Durumu</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_8" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_8" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_8" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_8" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m9">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Hesaplar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_9" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_9" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_9" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_9" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m10">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Aski Kodlar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_10" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_10" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_10" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_10" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m11">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Toplanan Para</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_11" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_11" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_11" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_11" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m12">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Toplanan Para Girişi</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_12" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_12" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_12" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_12" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m13">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Toplanan Para Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_13" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_13" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_13" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_13" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m14">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Mutabakat</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_14" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_14" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_14" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_14" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m15">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Raporları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_15" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_15" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_15" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_15" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m16">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Rol Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_16" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_16" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_16" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_16" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m17">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">E-Cüzdan Müşterileri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_17" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_17" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_17" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_17" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m18">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kullanıcı Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_18" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_18" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_18" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_18" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m19">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">E-Cüzdan İşlemleri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_19" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_19" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_19" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_19" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m20">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Hesap Hareketleri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_20" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_20" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_20" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_20" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m21">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Fraud İşlem Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_21" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_21" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_21" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_21" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m22">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Fraud İşlemler</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_22" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_22" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_22" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_22" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m23">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Müşteri Black List Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_23" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_23" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_23" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_23" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m24">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Black List</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_24" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_24" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_24" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_24" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m25">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Günlük Hesap Raporları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_25" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_25" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_25" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_25" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m26">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Günlük Prokis Raporları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_26" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_26" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_26" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_26" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m27">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Manuel Kiosk Boşaltma</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_27" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_27" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_27" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_27" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m28">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Para Doldurma</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_28" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_28" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_28" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_28" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m29">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Geriye Dönük Para Kod Sorgulama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_29" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_29" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_29" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_29" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m30">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Kurum Mutabakat</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_30" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_30" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_30" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_30" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m31">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Tarife İşlem Durumu</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_31" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_31" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_31" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_31" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m32">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Yerel Loglar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_32" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_32" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_32" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_32" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m33">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Bakımları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_33" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_33" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_33" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_33" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m34">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Cihaz Portları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_34" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_34" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_34" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_34" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m35">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Yeni Kiosk Raporları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_35" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_35" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_35" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_35" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m36">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Otomatik Mutabakat</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_36" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_36" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_36" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_36" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m37">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Çağrı ve Arıza Yönetimi</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_37" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_37" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_37" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_37" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m38">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kod Detayları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_38" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_38" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_38" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_38" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>


                                                            <asp:TableRow CssClass="listRowAlternate" ID="m39">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Aygıt Hata Logları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_39" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_39" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_39" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_39" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m40">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Para Girişleri (EOD)</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_40" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_40" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_40" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_40" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>


                                                            <asp:TableRow CssClass="listRowAlternate" ID="m41">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Dispenser (LCDM)</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_41" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_41" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_41" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_41" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m42">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Toplanan Para Onayı</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_42" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_42" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_42" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_42" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m43">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Ayarlar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_43" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_43" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_43" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_43" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m44">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Para Bildirim Kontrolü</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_44" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_44" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_44" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_44" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m45">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Banka Hesap İzlemi</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_45" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_45" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_45" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_45" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m46">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kesinti Takibi</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_46" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_46" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_46" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_46" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>


                                                            <asp:TableRow CssClass="listRowAlternate" ID="m47">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Takibi</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_47" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_47" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_47" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_47" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m48">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Şüpheli İşlemler</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_48" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_48" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_48" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_48" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>


                                                            <asp:TableRow CssClass="listRowAlternate" ID="m49">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Para Üstü Durumu</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_49" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_49" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_49" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_49" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m50">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Para Üstü Takibi</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_50" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_50" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_50" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_50" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m51">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kredi Kartı İşlemleri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_51" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_51" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_51" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_51" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m52">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kart Onarım İşlemleri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_52" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_52" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_52" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="delete_50" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_52" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m53">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">POS Son Günü Ekranı</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_53" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_53" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_53" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_53" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m54">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Mobil Ödeme İşlemleri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_54" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_54" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_54" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="delete_50" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_54" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m55">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kullanıcı Log Detayı</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_55" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_55" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_55" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="delete_50" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_55" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m56">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Parametreleri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_56" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_56" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_56" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="delete_50" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_56" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m57">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Dispenser LCDM Hopper</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_57" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_57" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_57" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="delete_50" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_57" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>


                                                            <asp:TableRow CssClass="listRowAlternate" ID="m58">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Para Üstü Hataları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_58" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_58" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_58" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="delete_50" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_58" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m59">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Referans Sistemi</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_59" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_59" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_59" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="delete_50" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_59" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m60">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Yerel Loglar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_60" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_60" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_60" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="delete_50" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_60" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="m61">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Yeni Kiosk Dispenser (LCDM)</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_61" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_61" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_61" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_61" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                        </asp:Table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:Panel ID="Panel1" runat="server" CssClass="containerPanel_95Pxauto">
                                    <table border="0" cellpadding="2" cellspacing="0" id="Table4" runat="server">
                                        <tr valign="middle">
                                            <td align="left" class="staticTextLine_200x20">Yeni Role Adı :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="newnameField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr valign="middle">
                                            <td align="left" class="staticTextLine_200x20">Yeni Rol Açıklama :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="newdescriptionField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <div align="left" class="windowTitle_container_autox30">
                                        Yeni Menüler<hr style="border-bottom: 1px solid #b2b2b4;" />
                                    </div>
                                    <table cellpadding="0" cellspacing="0" width="95%" id="Table1" runat="server">
                                        <tr>
                                            <td align="center" class="tableStyle1">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" RenderMode="Block">
                                                    <ContentTemplate>
                                                        <asp:Table ID="Table2" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                                            BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="95%">
                                                            <asp:TableRow CssClass="inputTitleCell3" Style="border-color: White; border-width: 1px; border-style: Double;">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Alt Menüler</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Görüntüle</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Güncelleme</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Ekleme</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Silme</asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om1">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kullanıcılar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_1" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_1" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_1" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_1" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om2">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Roller</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_2" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_2" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_2" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_2" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om3">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Tanımları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_3" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_3" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_3" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_3" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om4">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Şifre Değiştirme</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_4" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_4" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_4" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_4" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om5">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">İşlemler</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_5" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_5" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_5" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_5" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om6">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Müşteriler</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_6" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_6" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_6" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_6" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om7">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Komutları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_7" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_7" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_7" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_7" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om8">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Durumu</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_8" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_8" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_8" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_8" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om9">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Hesaplar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_9" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_9" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_9" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_9" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om10">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Aski Kodlar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_10" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_10" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_10" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_10" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om11">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Toplanan Para</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_11" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_11" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_11" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_11" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om12">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Toplanan Para Girişi</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_12" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_12" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_12" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_12" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om13">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Toplanan Para Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_13" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_13" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_13" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_13" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om14">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Mutabakat</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_14" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_14" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_14" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_14" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om15">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Raporları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_15" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_15" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_15" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_15" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om16">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Rol Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_16" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_16" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_16" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_16" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om17">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">E-Cüzdan Müşterileri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_17" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_17" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_17" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_17" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om18">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kullanıcı Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_18" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_18" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_18" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_18" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om19">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">E-Cüzdan İşlemleri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_19" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_19" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_19" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_19" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om20">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Hesap Hareketleri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_20" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_20" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_20" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_20" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om21">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Fraud İşlem Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_21" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_21" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_21" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_21" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om22">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Fraud Islemler</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_22" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_22" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_22" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_22" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om23">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Müşteri Black List Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_23" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_23" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_23" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_23" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om24">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Black List</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_24" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_24" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_24" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_24" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om25">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Günlük Hesap Raporları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_25" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_25" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_25" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_25" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om26">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Günlük Prokis Raporları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_26" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_26" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_26" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_26" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om27">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Manuel Kiosk Boşaltma</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_27" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_27" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_27" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_27" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om28">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Para Doldurma</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_28" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_28" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_28" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_28" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om29">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Geriye Dönük Para Kod Sorgulama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_29" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_29" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_29" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_29" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om30">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Kurum Mutabakat</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_30" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_30" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_30" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_30" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om31">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Tarife İşlem Durumu</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_31" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_31" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_31" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_31" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om32">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Yerel Loglar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_32" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_32" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_32" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_32" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om33">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Bakımları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_33" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_33" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_33" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_33" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om34">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Cihaz Portları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_34" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_34" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_34" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_34" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om35">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Yeni Kiosk Raporları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_35" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_35" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_35" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_35" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om36">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Otomatik Mutabakat</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_36" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_36" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_36" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_36" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om37">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Çağrı ve Arıza Yönetimi</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_37" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_37" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_37" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_37" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om38">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kod Detayları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_38" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_38" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_38" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_38" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om39">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Aygıt Hata Logları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_39" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_39" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_39" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_39" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om40">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Para Girişleri (EOD)</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_40" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_40" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_40" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_40" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om41">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Dispenser (LCDM)</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_41" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_41" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_41" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_41" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om42">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Toplanan Para Onayı</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_42" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_42" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_42" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_42" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om43">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Ayarlar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_43" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_43" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_43" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_43" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om44">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Para Bildirim Kontrolü</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_44" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_44" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_44" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_44" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>


                                                            <asp:TableRow CssClass="listRowAlternate" ID="om45">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Banka Hesap İzlemi</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_45" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_45" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_45" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_45" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om46">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kesinti Takibi</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_46" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_46" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_46" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_46" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om47">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Takibi</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_47" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_47" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_47" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_47" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om48">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Şüpheli İşlemler</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_48" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_48" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_48" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_48" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om49">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Para Üstü Durumu</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_49" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_49" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_49" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_49" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om50">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Para Üstü Takibi</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_50" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_50" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_50" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_50" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om51">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kredi Kartı İşlemleri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_51" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_51" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_51" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_51" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om52">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kart Onarım İşlemleri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_52" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_52" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_52" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_52" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om53">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">POS Son Günü Ekranı</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_53" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_53" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_53" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_53" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om54">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Mobil Ödeme İşlemleri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_54" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_54" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_54" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_54" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om55">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kullanıcı Log Detayı</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_55" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_55" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_55" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_55" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om56">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Parametreleri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_56" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_56" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_56" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_56" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om57">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Dispenser LCDM Hopper</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_57" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_57" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_57" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_57" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om58">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Para Üstü Hataları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_58" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_58" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_58" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_58" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>


                                                            <asp:TableRow CssClass="listRowAlternate" ID="om59">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Referans Sistemi</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_59" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_59" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_59" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_59" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                            <asp:TableRow CssClass="listRowAlternate" ID="om60">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Yerel Loglar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_60" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_60" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_60" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_60" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                              <asp:TableRow CssClass="listRowAlternate" ID="om61">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Yeni Kiosk Dispenser (LCDM)</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_61" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_61" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_61" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_61" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>

                                                        </asp:Table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>

                </div>
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="cancelButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClientClick="parent.hideModalPopup2();" Text="Kapat"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </td>
            </asp:Panel>
        </div>

    </form>
</body>
</html>
