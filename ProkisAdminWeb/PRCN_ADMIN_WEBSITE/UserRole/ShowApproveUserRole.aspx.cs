﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserRole_ShowApproveUserRole : System.Web.UI.Page
{

    private ParserLogin LoginDatas = null;
    private ParserApproveGetRole role;
    private int ownSubMenuIndex = -1;
    int roleId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../UserRole/ApproveUserRole.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1" || this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
            {
            }
            else
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }

        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }

    private void BindCtrls()
    {
        this.SetControls();
    }

    private void SetControls()
    {
        try
        {
            roleId = Convert.ToInt32(Request.QueryString["itemID"]);
            if (roleId != 0)
            {
                CallWebServices callWebServ = new CallWebServices();
                this.role = callWebServ.CallGetApproveRoleService(roleId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                if (this.role != null)
                {
                    this.oldnameField.Text = this.role.OldName;
                    this.olddescriptionField.Text = this.role.OldDescription;

                    for (int i = 0; i < this.role.OldRoleSubMenus.Count; i++)
                    {
                        ((CheckBox)userRolesTable.FindControl("view_" + this.role.OldRoleSubMenus[i].SubMenuId)).Checked = true;

                        if (this.role.OldRoleSubMenus[i].CanAdd == 1)
                        {
                            ((CheckBox)userRolesTable.FindControl("add_" + this.role.OldRoleSubMenus[i].SubMenuId)).Checked = true;
                        }
                        if (this.role.OldRoleSubMenus[i].CanDelete == 1)
                        {
                            ((CheckBox)userRolesTable.FindControl("delete_" + this.role.OldRoleSubMenus[i].SubMenuId)).Checked = true;
                        }
                        if (this.role.OldRoleSubMenus[i].CanEdit == 1)
                        {
                            ((CheckBox)userRolesTable.FindControl("edit_" + this.role.OldRoleSubMenus[i].SubMenuId)).Checked = true;
                        }
                    }

                    this.newnameField.Text = this.role.NewName;
                    this.newdescriptionField.Text = this.role.NewDescription;

                    for (int i = 0; i < this.role.NewRoleSubMenus.Count; i++)
                    {
                        ((CheckBox)userRolesTable.FindControl("oview_" + this.role.NewRoleSubMenus[i].SubMenuId)).Checked = true;

                        if (this.role.NewRoleSubMenus[i].CanAdd == 1)
                        {
                            ((CheckBox)userRolesTable.FindControl("oadd_" + this.role.NewRoleSubMenus[i].SubMenuId)).Checked = true;
                        }
                        if (this.role.NewRoleSubMenus[i].CanDelete == 1)
                        {
                            ((CheckBox)userRolesTable.FindControl("odelete_" + this.role.NewRoleSubMenus[i].SubMenuId)).Checked = true;
                        }
                        if (this.role.NewRoleSubMenus[i].CanEdit == 1)
                        {
                            ((CheckBox)userRolesTable.FindControl("oedit_" + this.role.NewRoleSubMenus[i].SubMenuId)).Checked = true;
                        }
                    }
                }
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowApproveUserRolesSC");
        }
    }
}