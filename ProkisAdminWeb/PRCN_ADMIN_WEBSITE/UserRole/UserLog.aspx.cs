﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Kiosk;
using PRCNCORE.Parser.User;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class UserRole_UserLog : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../UserRole/UserLog.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);
        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
             
            CallWebServices callWebServ = new CallWebServices();


            this.startDateField.Text = DateTime.Now.ToString("yyyy-MM-dd 00:00:00");
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd 00:00:00");


            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;

            this.pageNum = 0;
            this.SearchOrder(3, 1);
        }

    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
         this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
         this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
         this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
         this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
         this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
         ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
         this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }


    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);
            int recordCount = 0;
            int pageCount = 0;

            string kioskIds = "";





            CallWebServices callWebService = new CallWebServices();

            ParserListUserLogDetails logs = callWebService.CallListUserLogDetailService(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             2);

            if (logs != null)
            {
                if (logs.errorCode == 0)
                {

                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    string[] headerNames = { "Kullanıcı Adı", "İşlem Tarihi", "İşlem", "Sayfa", "Durum",
                                             "Açıklama"};


                    exportExcell.ExportExcellByBlock(logs.UserLog, "Kullanıcı Logları Detayı", headerNames);
                }
                else if (logs.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + logs.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelMutabakat");
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
         this.pageNum = 0;
         this.SearchOrder(3, 1);
    }


    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);
            int recordCount = 0;
            int pageCount = 0;

            string kioskIds = "";




            CallWebServices callWebServ = new CallWebServices();
            ParserListUserLogDetails logs = callWebServ.CallListUserLogDetailService(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id, 
                                                                             1);


            if (logs != null)
            {
                if (logs.errorCode == 0)
                {
                   this.BindListTable(logs);
                }
                else if (logs.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + logs.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionSrch");
        }
    }

    private void BindListTable(ParserListUserLogDetails items)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            TableRow[] Row = new TableRow[items.UserLog.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();

                //Row[i].Attributes.Add("Id", items.ParserKioskCashDispenser[i].KioskId.ToString());

                TableCell indexCell = new TableCell();
                TableCell userNameCell = new TableCell();

                TableCell transactionDateCell = new TableCell();
                TableCell transactionCell = new TableCell();
                TableCell pageCell = new TableCell();
                TableCell statusCell = new TableCell();
                TableCell explanationCell = new TableCell();
                TableCell sumCell = new TableCell();
                TableCell newValueCell = new TableCell();
                TableCell oldValueCell = new TableCell();

                indexCell.CssClass = "inputTitleCell4";
                userNameCell.CssClass = "inputTitleCell4";

                transactionDateCell.CssClass = "inputTitleCell4";
                transactionCell.CssClass = "inputTitleCell4";
                pageCell.CssClass = "inputTitleCell4";
                statusCell.CssClass = "inputTitleCell4";
                explanationCell.CssClass = "inputTitleCell4";
                sumCell.CssClass = "inputTitleCell4";
                newValueCell.CssClass  = "inputTitleCell4";
                oldValueCell.CssClass = "inputTitleCell4";
               

                indexCell.Text = (startIndex + i).ToString();


                //kioskIdCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editKioskDispenserCashCountButtonClicked(" + items.ParserKioskCashDispenser[i].kioskID + ");\" class=\"anylink\">" + items.ParserKioskCashDispenser[i].kioskId + "</a>";
                //kioskIdCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editKioskDispenserCashCountButtonClicked(" + items.ParserKioskCashDispenser[i].kioskID + "," + items.ParserKioskCashDispenser[i].Id + ");\" class=\"anylink\">" + items.ParserKioskCashDispenser[i].kioskId + "</a>";


                userNameCell.Text = items.UserLog[i].userName.ToString();
                transactionDateCell.Text = items.UserLog[i].transactionDate;
                transactionCell.Text = items.UserLog[i].transaction;
                pageCell.Text = items.UserLog[i].pages;
                statusCell.Text = items.UserLog[i].status;
                explanationCell.Text = items.UserLog[i].explanation;
                newValueCell.Text = items.UserLog[i].newValue;
                oldValueCell.Text = items.UserLog[i].oldValue;

          

                Row[i].Cells.AddRange(new TableCell[]{
                        indexCell,
						userNameCell,
                        transactionDateCell,
                        transactionCell,
                        pageCell,
                        newValueCell,
                        oldValueCell,
                        statusCell,
                        explanationCell,
                        sumCell
					});

               
                    if (i % 2 == 0)
                        Row[i].CssClass = "listrow";
                    else
                        Row[i].CssClass = "listRowAlternate";
                
            }

            this.itemsTable.Rows.AddRange(Row);

            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (items.recordCount < currentRecordEnd)
                currentRecordEnd = items.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + items.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(items.pageCount, this.pageNum, items.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListMutabakatBDT");
        }
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "UserName":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "TransactionDate":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Transaction":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Pages":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // Instution.Text = "<a>Kurum    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  Instution.Text = "<a>Kurum    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Instution.Text = "<a>Kurum    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Status":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // TotalAmount.Text = "<a>T. Tutar    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // TotalAmount.Text = "<a>T. Tutar    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TotalAmount.Text = "<a>T. Tutar    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Explanation":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // CashAmount.Text = "<a>Nakit    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  CashAmount.Text = "<a>Nakit    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  CashAmount.Text = "<a>Nakit    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;
           


            default:

                orderSelectionColumn = 3;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

}