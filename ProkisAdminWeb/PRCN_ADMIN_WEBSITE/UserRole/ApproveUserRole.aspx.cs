﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
//using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserRole_ApproveUserRole : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;
    private int ownSubMenuIndex = -1;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 8;
    private int orderSelectionDescAsc = 1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../UserRole/ApproveUserRole.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            ViewState["OrderColumn"] = 8;
            ViewState["OrderDesc"] = 1;

            this.pageNum = 0;
            this.SearchOrder(8, 1);
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(3, 1);
    }

    public void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListApproveRoles roles = callWebServ.CallListApproveRoleService(this.searchTextField.Text,
                                                                    this.numberOfItemsPerPage,
                                                                    this.pageNum,
                                                                    recordCount,
                                                                    pageCount, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (roles != null)
            {
                if (roles.errorCode == 0)
                {
                    this.BindListTable(roles);
                }
                else if (roles.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + roles.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListUserRolesSrch");
        }
    }

    private void BindListTable(ParserListApproveRoles roles)
    {
        try
        {
            TableRow[] Row = new TableRow[roles.Roles.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("Id", roles.Roles[i].Id.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell nameCell = new TableCell();
                TableCell descriptionCell = new TableCell();
                TableCell updatedDateCell = new TableCell();
                TableCell updatedUserCell = new TableCell();
                TableCell oldnameCell = new TableCell();
                TableCell olddescriptionCell = new TableCell();
                TableCell oldupdatedDateCell = new TableCell();
                TableCell oldupdatedUserCell = new TableCell();
                TableCell detailsCell = new TableCell();

                indexCell.CssClass = "inputTitleCell4";
                idCell.CssClass = "inputTitleCell4";
                nameCell.CssClass = "inputTitleCell4";
                descriptionCell.CssClass = "inputTitleCell4";
                updatedDateCell.CssClass = "inputTitleCell4";
                updatedUserCell.CssClass = "inputTitleCell4";
                oldnameCell.CssClass = "inputTitleCell4";
                olddescriptionCell.CssClass = "inputTitleCell4";
                oldupdatedDateCell.CssClass = "inputTitleCell4";
                oldupdatedUserCell.CssClass = "inputTitleCell4";
                detailsCell.CssClass = "inputTitleCell4";

                nameCell.Width = Unit.Pixel(100);
                nameCell.HorizontalAlign = HorizontalAlign.Center;
                nameCell.Style.Add("padding-left", "5px");

                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1")
                {
                    nameCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editButtonClicked(" + roles.Roles[i].Id + ");\" class=\"anylink\">" + roles.Roles[i].Name + "</a>";
                }
                else
                {
                    nameCell.Text = roles.Roles[i].Name;
                }


                descriptionCell.Text = roles.Roles[i].Description;
                updatedDateCell.Text = roles.Roles[i].UpdatedDate;
                updatedUserCell.Text = roles.Roles[i].UpdatedUser;

                oldnameCell.Text = roles.Roles[i].OldName;
                olddescriptionCell.Text = roles.Roles[i].OldDescription;
                oldupdatedDateCell.Text = roles.Roles[i].OldUpdatedDate;
                oldupdatedUserCell.Text = roles.Roles[i].OldUpdatedUser;


                indexCell.Width = Unit.Pixel(30);
                indexCell.Text = (startIndex + i).ToString();
                idCell.Visible = false;
                idCell.Text = roles.Roles[i].Id.ToString();

                detailsCell.Width = Unit.Pixel(70);

                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1")
                {
                    detailsCell.Text = "<a href=\"javascript: approveButtonClicked('[pk].[approve_user_role]'," + roles.Roles[i].Id + ");\" class=\"approveButton\" title=\"Onayla\"></a>"
                    + "<a href=# onClick=\"deleteButtonClicked('[pk].[delete_waiting_approve_role]'," + roles.Roles[i].Id + ");\"><img src=../images/delete.png border=0 title=\"Silme\" /></a>";
                }
                else
                {
                    detailsCell.Text = " ";
                }

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
						idCell,
                        nameCell,
                        descriptionCell,
                        updatedDateCell,
                        updatedUserCell,
                        detailsCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(roles.pageCount, this.pageNum, roles.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListUserRolesBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    protected void deleteButton_Click(object sender, EventArgs e)
    {
        this.SearchOrder(3, 1);
    }



    //public void Sort(Object sender, EventArgs e)
    //{
    //    this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
    //    this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
    //    this.pageNum = 0;

    //    switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
    //    {

    //        case "Name":
    //            if (orderSelectionColumn == 1)
    //            {
    //                if (orderSelectionDescAsc == 0)
    //                {
    //                    Name.Text = "<a>Rol Adı    <img src=../images/arrow_up.png border=0/></a>";
    //                    orderSelectionDescAsc = 1;
    //                }
    //                else
    //                {
    //                    Name.Text = "<a>Rol Adı    <img src=../images/arrow_down.png border=0/></a>";
    //                    orderSelectionDescAsc = 0;
    //                }
    //            }
    //            else
    //            {
    //                Name.Text = "<a>Rol Adı    <img src=../images/arrow_up.png border=0/></a>";
    //                orderSelectionColumn = 1;
    //                orderSelectionDescAsc = 1;
    //            }
    //            break;
    //        case "Description":
    //            if (orderSelectionColumn == 2)
    //            {
    //                if (orderSelectionDescAsc == 0)
    //                {
    //                    Description.Text = "<a>Rol Açıklama    <img src=../images/arrow_up.png border=0/></a>";
    //                    orderSelectionDescAsc = 1;
    //                }
    //                else
    //                {
    //                    Description.Text = "<a>Rol Açıklama    <img src=../images/arrow_down.png border=0/></a>";
    //                    orderSelectionDescAsc = 0;
    //                }
    //            }
    //            else
    //            {
    //                Description.Text = "<a>Rol Açıklama    <img src=../images/arrow_up.png border=0/></a>";
    //                orderSelectionColumn = 2;
    //                orderSelectionDescAsc = 1;
    //            }
    //            break;
    //        case "CreationDate":
    //            if (orderSelectionColumn == 3)
    //            {
    //                if (orderSelectionDescAsc == 0)
    //                {
    //                    CreationDate.Text = "<a>Oluşturulma Tarihi    <img src=../images/arrow_up.png border=0/></a>";
    //                    orderSelectionDescAsc = 1;
    //                }
    //                else
    //                {
    //                    CreationDate.Text = "<a>Oluşturulma Tarihi    <img src=../images/arrow_down.png border=0/></a>";
    //                    orderSelectionDescAsc = 0;
    //                }
    //            }
    //            else
    //            {
    //                CreationDate.Text = "<a>Oluşturulma Tarihi    <img src=../images/arrow_up.png border=0/></a>";
    //                orderSelectionColumn = 3;
    //                orderSelectionDescAsc = 1;
    //            }
    //            break;
    //        default:

    //            orderSelectionColumn = 1;
    //            orderSelectionDescAsc = 1;

    //            break;
    //    }

    //    ViewState["OrderColumn"] = this.orderSelectionColumn;
    //    ViewState["OrderDesc"] = this.orderSelectionDescAsc;

    //    SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    //}
}