﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AskiCode_SendMoneyCode : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private int moneyCodeId = 0;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];
        this.moneyCodeId = Convert.ToInt32(Request.QueryString["itemID"]);
        this.hiddenUserId.Value = Request.QueryString["itemID"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../AskiCode/ListAskiCodes.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit != "1" && this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd != "1")
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }

        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }

    private void BindCtrls()
    {
        try
        {
            CallWebServices callWebServ = new CallWebServices();
            ParserPhoneNumber phoneNumberServ = callWebServ.CallPhoneNumbers(2,this.moneyCodeId.ToString(), this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            cellphoneBox.ForeColor = System.Drawing.Color.Gray;
            if (phoneNumberServ.phoneNumber != "0" && phoneNumberServ.phoneNumber != "")
            {
                if (phoneNumberServ.phoneNumber.ToString().Length > 2)
                {
                    phoneNumberServ.phoneNumber = phoneNumberServ.phoneNumber.ToString().Substring(2, phoneNumberServ.phoneNumber.Length - 2);
                }
                cellphoneBox.Text = phoneNumberServ.phoneNumber;
                cellphoneBox.ForeColor = System.Drawing.Color.Red;
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SendSMSCode");
        }
    }

    protected void sendButton_Click(object sender, EventArgs e)
    {
        SendAskiCode();
    }
    private string ClearPhoneNumberText(string phoneNumber)
    {
        phoneNumber=phoneNumber.Replace(")", "");
        phoneNumber=phoneNumber.Replace("(", "");
        phoneNumber = phoneNumber.Replace("-", "");
        phoneNumber = phoneNumber.Replace(" ", "");
        return phoneNumber;
    }
    private void SendAskiCode()
    {
        try
        {
            string scriptText = "";

            ParserSaveMoneyCode operationResult = null;

            if (this.moneyCodeId != 0)
            {
                CallWebServices callWebServ = new CallWebServices();
                string phoneNumber = ClearPhoneNumberText(this.cellphoneBox.Text);
                operationResult = callWebServ.CallSendMoneyCodeService(0, this.moneyCodeId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id, phoneNumber);

                scriptText = "parent.showMessageWindowSendMoneyCode('İşlem Başarılı.');";
            }
            else
            {
                this.messageArea.InnerHtml = "İlgili işlem alınamadı !";
            }

            if (operationResult != null)
            {
                if (operationResult.errorCode != 0)
                {
                    this.messageArea.InnerHtml = operationResult.errorDescription;
                }
                else if (operationResult.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    this.messageArea.InnerHtml = operationResult.errorDescription;
                    this.sendButton.Enabled = false;
                    this.sendButton.Attributes.Add("disabled", "disabled");
                }
            }
            else
            {
                this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
            }


            if (scriptText != "")
            {
                ScriptManager.RegisterClientScriptBlock(this.sendButton, this.sendButton.GetType(), "CloseScript", scriptText, true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowUserSv");
        }
    }
}