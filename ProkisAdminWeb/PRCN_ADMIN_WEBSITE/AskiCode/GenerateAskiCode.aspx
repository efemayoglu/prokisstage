﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GenerateAskiCode.aspx.cs" Inherits="AskiCode_GenerateAskiCode" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />
    <input type="hidden" id="hiddenUserId" runat="server" name="hiddenUserId" value="0" />
    <script language="javascript" type="text/javascript">


        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function checkForm() {
            var status = true;
            var messageText = "";

            if (document.getElementById('amountTextBox').value == '') {
                status = false;
                messageText = "Tutar giriniz !";
            }

            else if (ValidateDecimalNumber(document.getElementById('amountTextBox').value) == false) {
                status = false;
                messageText = "Tutar formatı hatalı !";
            }

            else {

                messageText = " ";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }
        function validateDecimal(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]|\.|\,/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function validateNo(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function ValidateDecimalNumber(inputText) {
            if (document.getElementById('amountTextBox').value == '')
                return true;
            var ipformat = /^[0-9]+((\.[0-9]{1,2})|(\,[0-9]{1,2}))?$/;
            if (inputText.match(ipformat)) {
                return true;
            }
            else {
                return false;
            }
        }

    </script>
    <base target="_self" />
    <style type="text/css">
        .auto-style14 {
            font-family: Century Gothic;
            font-size: 14px;
            color: #8A8B8C;
            text-decoration: none;
            font-weight: bold;
            padding-bottom: 4px;
            width: 184px;
        }

        .auto-style15 {
            height: 17px;
            width: 184px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel7" DefaultButton="saveButton" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <div align="left" id="brandDetailsTab" class="windowTitle_container_autox30">
                    Para Kod Üretme<hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>
                <table border="0" cellpadding="2" cellspacing="0" id="Table1" runat="server">
                    <tr valign="middle">
                        <td align="center" class="auto-style14">
                            <asp:Label runat="server">Para Kod Tutarını Giriniz</asp:Label></td>
                    </tr>
                    <tr valign="middle">
                        <td align="center" class="auto-style14">
                            <asp:TextBox ID="amountTextBox" CssClass="inputLine_100x20" runat="server" onkeypress='validateDecimal(event)'></asp:TextBox></td>
                    </tr>
                    <tr valign="middle">
                        <td align="center" class="auto-style14">
                            <asp:Label ID="Label1" runat="server">Müşteri Telefon Numarası</asp:Label></td>
                    </tr>
                     <tr valign="middle">
                         <td align="center" class="auto-style14">
                            <asp:TextBox ID="cellphoneBox" CssClass="inputLine_100x20" runat="server" onkeypress='validateNo(event)' MaxLength="10"></asp:TextBox>
                            <ajaxControlToolkit:MaskedEditExtender ID="cellphoneBox_MaskedEditExtender" runat="server" BehaviorID="cellphoneBox_MaskedEditExtender" Century="2000" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" TargetControlID="cellphoneBox" Mask="(999) 999-9999" MaskType="Number">
                            </ajaxControlToolkit:MaskedEditExtender>
                         </td>
                     </tr>

                    <tr valign="middle">
                        <td align="center" class="auto-style14">
                            <asp:Label ID="Label2" runat="server">Üretilen Para Kod</asp:Label></td>
                    </tr>
                    <tr valign="middle">
                        <td align="center" class="auto-style14">
                            <asp:TextBox ID="askiCodeBox" CssClass="inputLine_100x20" runat="server" Enabled="false" ></asp:TextBox></td>
                    </tr>
                </table>
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Üret" Width="70px"></asp:Button>
                            </td>
                            <td>
                                <asp:Button ID="cancelButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClientClick="parent.hideModalPopup2();" Text="Vazgeç"></asp:Button>
                            </td>
                        <tr style="height:10px"><td></td></tr>
                    </table>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
