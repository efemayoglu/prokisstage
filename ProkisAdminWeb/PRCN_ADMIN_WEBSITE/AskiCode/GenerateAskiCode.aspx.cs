﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AskiCode_GenerateAskiCode : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private int transactionId = 0;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];
        this.transactionId = Convert.ToInt32(Request.QueryString["itemID"]);
        this.hiddenUserId.Value = Request.QueryString["itemID"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Transaction/ListTransactions.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit != "1")
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }

        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }
    private string ClearPhoneNumberText(string phoneNumber)
    {
        phoneNumber = phoneNumber.Replace(")", "");
        phoneNumber = phoneNumber.Replace("(", "");
        phoneNumber = phoneNumber.Replace("-", "");
        phoneNumber = phoneNumber.Replace(" ", "");
        return phoneNumber;
    }
    private void BindCtrls()
    {
        try
        {
            CallWebServices callWebServ = new CallWebServices();
            ParserPhoneNumber phoneNumberServ = callWebServ.CallPhoneNumbers(1,transactionId.ToString(),this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            cellphoneBox.ForeColor = Color.Gray;
            if (phoneNumberServ.phoneNumber != "0" && phoneNumberServ.phoneNumber !="")
            {  
                if(phoneNumberServ.phoneNumber.ToString().Length>2)
                {
                    phoneNumberServ.phoneNumber = phoneNumberServ.phoneNumber.ToString().Substring(2, phoneNumberServ.phoneNumber.Length - 2);
                }
                cellphoneBox.Text = phoneNumberServ.phoneNumber;
                cellphoneBox.ForeColor = Color.Red;
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowUserBC");
        }
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        SaveAskiCode();
    }

    private void SaveAskiCode()
    {
        try
        {


            ParserSaveMoneyCode operationResult = null;

            if (this.transactionId != 0)
            {
                string phoneNumber = ClearPhoneNumberText(this.cellphoneBox.Text);
                CallWebServices callWebServ = new CallWebServices();
                operationResult = callWebServ.CallSaveMoneyCodeService(this.transactionId
                    , this.LoginDatas.User.Id
                    , this.amountTextBox.Text
                    , this.LoginDatas.AccessToken
                    , this.LoginDatas.User.Id
                    , phoneNumber);
            }
            else
            {
                this.messageArea.InnerHtml = "İlgili işlem alınamadı !";
            }

            if (operationResult != null)
            {
                if (operationResult.errorCode != 0)
                {
                    this.messageArea.InnerHtml = operationResult.errorDescription;
                }
                else if (operationResult.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    this.messageArea.InnerHtml = operationResult.errorDescription;
                    this.askiCodeBox.Text = operationResult.moneyCode.Substring(0, 3) + " *** " + operationResult.moneyCode.Substring(8, 3);
                    this.saveButton.Attributes.Add("disabled", "disabled");
                }
            }
            else
            {
                this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowUserSv");
        }
    }
}