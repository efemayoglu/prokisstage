﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.MoneyCode;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AskiCode_SMSLogDetails : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private Int64 moneyCodeId = 0;
    private int ownSubMenuIndex = -1;
   
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 1;
    private int orderSelectionDescAsc = 1;
    

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];
        this.moneyCodeId = Convert.ToInt64(Request.QueryString["itemID"]);
        //this.hiddenUserId.Value = Request.QueryString["itemID"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../AskiCode/ListAskiCodes.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit != "1" && this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd != "1")
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }

        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }

    private void BindCtrls()
    {
        try
        {
            CallWebServices callWebServ = new CallWebServices();
            ParserListMoneyCodeSMSNumber listMoneyCodeSMSNumber = callWebServ.CallGetSMSLogDetailsService(moneyCodeId
                                                                            , this.LoginDatas.AccessToken
                                                                            , this.LoginDatas.User.Id);

            if (listMoneyCodeSMSNumber != null)
            {
                if (listMoneyCodeSMSNumber.errorCode == 0)
                {
                    this.BindListTable(listMoneyCodeSMSNumber);
                }
                else if (listMoneyCodeSMSNumber.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + listMoneyCodeSMSNumber.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
             
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SMSLogDetails");
        }
    }


    private void BindListTable(ParserListMoneyCodeSMSNumber list)
    {
        try
        {
                       
            TableRow[] Row = new TableRow[list.listMoneyCodeSMSNumber.Count];
            
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();

                TableCell indexCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell cashTypeCell = new TableCell();
                TableCell cashCountCell = new TableCell();

                TableCell sumCell = new TableCell();

                indexCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                cashTypeCell.CssClass = "inputTitleCell4";
                cashCountCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                kioskNameCell.Text = list.listMoneyCodeSMSNumber[i].OperationId;
                cashTypeCell.Text = list.listMoneyCodeSMSNumber[i].UserName;
                cashCountCell.Text = list.listMoneyCodeSMSNumber[i].InsertionDate;




                //string[] words = cashTypeCell.Text.Split(' ');

                //// O satıra ait toplam (sumCell)
                //if (cashTypeCell.Text.Contains("Krs"))
                //{
                //    sumCell.Text = (((Convert.ToDouble(words[0])) / 100 * (list.cashCount[i].cashCount))).ToString();

                //    sumCell.Text = sumCell.Text + " TL";

                //}
                //else
                //{

                //    sumCell.Text = ((Convert.ToDouble(words[0]) * (list.cashCount[i].cashCount))).ToString() + " TL";
                //}

                //// Genel Toplam (sumAll)
                //if (cashTypeCell.Text.Contains("Krs"))
                //{
                //    sumAll = ((Convert.ToDouble(words[0]) / 100 * (list.cashCount[i].cashCount))) + sumAll;
                //}
                //else
                //{
                //    sumAll = ((Convert.ToDouble(words[0]) * (list.cashCount[i].cashCount))) + sumAll;
                //}


                //countAll = (list.cashCount[i].cashCount) + countAll;

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
						kioskNameCell,
                        cashTypeCell,
                        cashCountCell,
                        sumCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }


            this.itemsTable.Rows.AddRange(Row);

            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            //pagingCell.Text = WebUtilities.GetPagingText(list.pageCount, this.pageNum, list.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);



            //this.itemsTable.Rows.AddRange(Row);

            /* TableRow addNewRow = new TableRow();
             TableCell totalMoneyTextCell = new TableCell();
             TableCell totalMoneyCell = new TableCell();
             TableCell spaceCell = new TableCell();
             TableCell countCell = new TableCell();

             spaceCell.CssClass = "inputTitleCell3";
             spaceCell.ColumnSpan = (3);

             totalMoneyTextCell.CssClass = "inputTitleCell3";
             totalMoneyCell.CssClass = "inputTitleCell3";

             countCell.CssClass = "inputTitleCell3";
             countCell.Text = countAll.ToString() + "  Adet";

             spaceCell.Text = " ";


             spaceCell.Text = "Toplam Miktar:";
             totalMoneyCell.Text = sumAll + " TL ";

             addNewRow.Cells.Add(spaceCell);
             addNewRow.Cells.Add(countCell);
             //addNewRow.Cells.Add(totalMoneyTextCell);
             addNewRow.Cells.Add(totalMoneyCell);
            

             this.itemsTable.Rows.Add(addNewRow);
             */


            //////////////////////////////////////
            ////////// ParaÜstü Reject 
            ////////sumAll = 0;
            ////////countAll = 0;

            ////////TableRow[] RowReject = new TableRow[4];
            ////////int startIndex2 = (this.pageNum * this.numberOfItemsPerPage) + 1;
            ////////for (int i = 0; i < RowReject.Length; i++)
            ////////{
            ////////    RowReject[i] = new TableRow();

            ////////    TableCell indexCell = new TableCell();
            ////////    TableCell kioskNameCell = new TableCell();
            ////////    TableCell cashTypeCell = new TableCell();
            ////////    TableCell cashCountCell = new TableCell();

            ////////    TableCell sumCell = new TableCell();

            ////////    indexCell.CssClass = "inputTitleCell4";
            ////////    kioskNameCell.CssClass = "inputTitleCell4";
            ////////    cashTypeCell.CssClass = "inputTitleCell4";
            ////////    cashCountCell.CssClass = "inputTitleCell4";

            ////////    indexCell.Text = (startIndex + i).ToString();
            ////////    kioskNameCell.Text = list.cashCount[i + 8].KioskName;
            ////////    cashTypeCell.Text = list.cashCount[i + 8].cashTypeName;
            ////////    cashCountCell.Text = list.cashCount[i + 8].cashCount.ToString() + "  Adet";




            ////////    string[] words = cashTypeCell.Text.Split(' ');

            ////////    // O satıra ait toplam (sumCell)
            ////////    if (cashTypeCell.Text.Contains("Krs"))
            ////////    {
            ////////        sumCell.Text = (((Convert.ToDouble(words[0])) / 100 * (list.cashCount[i + 8].cashCount)) / 100).ToString();

            ////////        sumCell.Text = sumCell.Text + " TL";

            ////////    }
            ////////    else
            ////////    {

            ////////        sumCell.Text = ((Convert.ToDouble(words[0]) * (list.cashCount[i + 8].cashCount)) / 100).ToString() + " TL";
            ////////    }

            ////////    // Genel Toplam (sumAll)
            ////////    if (cashTypeCell.Text.Contains("Krs"))
            ////////    {
            ////////        sumAll = ((Convert.ToDouble(words[0]) / 100 * (list.cashCount[i + 8].cashCount)) / 100) + sumAll;
            ////////    }
            ////////    else
            ////////    {
            ////////        sumAll = ((Convert.ToDouble(words[0]) * (list.cashCount[i + 8].cashCount)) / 100) + sumAll;
            ////////    }


            ////////    countAll = (list.cashCount[i + 8].cashCount) + countAll;

            ////////    RowReject[i].Cells.AddRange(new TableCell[]{
            ////////            indexCell,
            ////////            kioskNameCell,
            ////////            cashTypeCell,
            ////////            cashCountCell,
            ////////            sumCell
            ////////        });

            ////////    if (i % 2 == 0)
            ////////        RowReject[i].CssClass = "listrow";
            ////////    else
            ////////        RowReject[i].CssClass = "listRowAlternate";
            ////////}
            ////////this.itemsTable2.Rows.AddRange(RowReject);


            ////////TableRow addNewRow2 = new TableRow();
            ////////TableCell totalMoneyTextCell2 = new TableCell();
            ////////TableCell totalMoneyCell2 = new TableCell();
            ////////TableCell spaceCell2 = new TableCell();
            ////////TableCell countCell2 = new TableCell();

            ////////spaceCell2.CssClass = "inputTitleCell3";
            ////////spaceCell2.ColumnSpan = (3);

            ////////totalMoneyTextCell2.CssClass = "inputTitleCell3";
            ////////totalMoneyCell2.CssClass = "inputTitleCell3";

            ////////countCell2.CssClass = "inputTitleCell3";
            ////////countCell2.Text = countAll.ToString() + "  Adet";

            ////////spaceCell2.Text = " ";


            ////////spaceCell2.Text = "Toplam Miktar:";
            ////////totalMoneyCell2.Text = sumAll + " TL ";

            ////////addNewRow2.Cells.Add(spaceCell2);
            ////////addNewRow2.Cells.Add(countCell2);
            //////////addNewRow.Cells.Add(totalMoneyTextCell);
            ////////addNewRow2.Cells.Add(totalMoneyCell2);

            ////////this.itemsTable2.Rows.Add(addNewRow2);
 
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskSMSLogDetails");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        //this.List(4, 1);
    }
}