﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AskiCode_ListAskiCodes_SweepBack : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../AskiCode/ListAskiCodesSweepBack.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd")+" 00:00:00";

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            CallWebServices callWebServ = new CallWebServices();
            ParserListItems status = callWebServ.CallGetMoneyCodeStatusNames(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (status != null)
            {
                if (status.errorCode == 0)
                {
                    int cnt = 0;
                    foreach (var list in status.items)
                    {
                        cnt++;
                        ListItem itemCombo = new ListItem();
                        itemCombo.Value = Convert.ToString(list.id);
                        itemCombo.Text = list.name;
                        this.codeStatusBox.Items.Add(itemCombo);
                        if (cnt > 1) break;
                    }
                    //this.codeStatusBox.DataSource = status.items;
                    //this.codeStatusBox.DataBind();
                }
                else if (status.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
            }

            ListItem item = new ListItem();
            item.Text = "Durum Seçiniz";
            item.Value = "0";
            this.codeStatusBox.Items.Add(item);
            this.codeStatusBox.SelectedValue = "0";
            this.generatedTypeBox.SelectedValue = "0";

            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {

                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();

            }

            ListItem item1 = new ListItem();
            item1.Text = "Kurum Seçiniz";
            item1.Value = "0";
            this.instutionBox.Items.Add(item1);
            this.instutionBox.SelectedValue = "0";


            ViewState["OrderColumn"] = 5;
            ViewState["OrderDesc"] = 1;

            this.pageNum = 0;
            this.SearchOrder(5, 1);
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(5, 1);
    }

    private void BindListTable(ParserListMoneyCodes list)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);


            TableRow[] Row = new TableRow[list.moneyCodes.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("codeId", list.moneyCodes[i].id.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell transactionIdCell = new TableCell();
                TableCell customerNameCell = new TableCell();
                TableCell aboneNoCell = new TableCell();
                TableCell statusCell = new TableCell();
                TableCell insertionDateCell = new TableCell();
                TableCell codeAmountCell = new TableCell();
                TableCell askiCodeCell = new TableCell();
                TableCell generationCaseCell = new TableCell();
                TableCell useDateCell = new TableCell();
                TableCell generatedUserCell = new TableCell();
                TableCell detailsCell = new TableCell();
                TableCell instutionNameCell = new TableCell();
                TableCell fakeCell00 = new TableCell();
                fakeCell00.CssClass = "inputTitleCell4";
                fakeCell00.Visible = false;
                instutionNameCell.CssClass = "inputTitleCell4";

                TableCell fakeCell1 = new TableCell();
                TableCell fakeCell2 = new TableCell();
                TableCell fakeCell3 = new TableCell();
                TableCell fakeCell4 = new TableCell();
                TableCell fakeCell5 = new TableCell();
                TableCell fakeCell6 = new TableCell();
                TableCell fakeCell7 = new TableCell();
                TableCell fakeCell8 = new TableCell();
                TableCell fakeCell9 = new TableCell();
                TableCell fakeCell10 = new TableCell();
                TableCell fakeCell11 = new TableCell();

                fakeCell1.CssClass = "inputTitleCell4";
                fakeCell2.CssClass = "inputTitleCell4";
                fakeCell3.CssClass = "inputTitleCell4";
                fakeCell4.CssClass = "inputTitleCell4";
                fakeCell5.CssClass = "inputTitleCell4";
                fakeCell6.CssClass = "inputTitleCell4";
                fakeCell7.CssClass = "inputTitleCell4";
                fakeCell8.CssClass = "inputTitleCell4";
                fakeCell9.CssClass = "inputTitleCell4";
                fakeCell10.CssClass = "inputTitleCell4";
                fakeCell11.CssClass = "inputTitleCell4";

                fakeCell1.Visible = false;
                fakeCell2.Visible = false;
                fakeCell3.Visible = false;
                fakeCell4.Visible = false;
                fakeCell5.Visible = false;
                fakeCell6.Visible = false;
                fakeCell7.Visible = false;
                fakeCell8.Visible = false;
                fakeCell9.Visible = false;
                fakeCell10.Visible = false;
                fakeCell11.Visible = false;

                indexCell.CssClass = "inputTitleCell4";
                idCell.CssClass = "inputTitleCell4";
                transactionIdCell.CssClass = "inputTitleCell4";
                customerNameCell.CssClass = "inputTitleCell4";
                aboneNoCell.CssClass = "inputTitleCell4";
                statusCell.CssClass = "inputTitleCell4";
                insertionDateCell.CssClass = "inputTitleCell4";
                codeAmountCell.CssClass = "inputTitleCell4";
                askiCodeCell.CssClass = "inputTitleCell4";
                generationCaseCell.CssClass = "inputTitleCell4";
                useDateCell.CssClass = "inputTitleCell4";
                generatedUserCell.CssClass = "inputTitleCell4";
                detailsCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                idCell.Text = list.moneyCodes[i].id.ToString();
                transactionIdCell.Text = "<a href=\"javascript:void(0);\" onclick=\"showTransactionDetailClicked(" + list.moneyCodes[i].transactionId + ");\" class=\"anylink\">" + list.moneyCodes[i].transactionId.ToString() + "</a>";
                //transactionIdCell.Text = list.askiCodes[i].transactionId.ToString();
                customerNameCell.Text = list.moneyCodes[i].customerName;
                aboneNoCell.Text = list.moneyCodes[i].aboneNo;
                statusCell.Text = list.moneyCodes[i].statusName;
                insertionDateCell.Text = list.moneyCodes[i].insertionDate;
                codeAmountCell.Text = list.moneyCodes[i].codeAmount;
                askiCodeCell.Text = list.moneyCodes[i].codeNumber;
                generationCaseCell.Text = list.moneyCodes[i].codeGenerationCase;
                useDateCell.Text = list.moneyCodes[i].useDate;
                generatedUserCell.Text = list.moneyCodes[i].generatedUserName;
                instutionNameCell.Text = list.moneyCodes[i].instutionName;
                detailsCell.Width = Unit.Pixel(50);
                idCell.Visible = false;

                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1" && this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanDelete == "1")
                {
                    if (list.moneyCodes[i].statusId == 4)
                    {
                        detailsCell.Text = "<a href=\"javascript: approveButtonClicked('[pk].[approve_money_code]'," + list.moneyCodes[i].id + ");\" class=\"approveButton\" title=\"Onayla\"></a>"
                            + "<a href=# onClick=\"deleteButtonClicked('[pk].[delete_aski_code]'," + list.moneyCodes[i].id + ");\"><img src=../images/delete.png border=0 title=\"Silme\" /></a>";
                    }
                    else
                    {
                        detailsCell.Text = "<a href=# onClick=\"deleteButtonClicked('[pk].[delete_aski_code]'," + list.moneyCodes[i].id + ");\"><img src=../images/delete.png border=0 title=\"Silme\" /></a>";
                    }
                }
                else if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "0" && this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanDelete == "1")
                {
                    detailsCell.Text = "<a href=# onClick=\"deleteButtonClicked('[pk].[delete_aski_code]'," + list.moneyCodes[i].id + ");\"><img src=../images/delete.png border=0 title=\"Silme\" /></a>";
                }
                else if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1" && this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanDelete == "0")
                {
                    if (list.moneyCodes[i].statusId == 4)
                    {
                        detailsCell.Text = "<a href=\"javascript: approveButtonClicked('[pk].[approve_money_code]'," + list.moneyCodes[i].id + ");\" class=\"approveButton\" title=\"Onayla\"></a>";
                    }
                    else
                    {
                        detailsCell.Text = " ";
                    }
                }

                else
                {
                    DetailColumn.Visible = false;
                    detailsCell.Text = " ";
                }

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
						idCell,
                        fakeCell1,
                        fakeCell2,
                        fakeCell3,
                        fakeCell4,
                        fakeCell5,
                        fakeCell6,
                        fakeCell7,
                        fakeCell8,
                        fakeCell9,
                        fakeCell10,
                        fakeCell00,
						transactionIdCell,
                        instutionNameCell,
                        customerNameCell,
                        aboneNoCell,
                        statusCell,
                        insertionDateCell,
                        codeAmountCell,
                        askiCodeCell,
                        generationCaseCell,
                        useDateCell,
                        generatedUserCell,
                        detailsCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (list.recordCount < currentRecordEnd)
                currentRecordEnd = list.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + list.recordCount.ToString() + " kayıt bulundu.";
            }

            TableRow addNewRow = new TableRow();
            TableCell totalMoneyTextCell = new TableCell();
            TableCell totalMoneyCell = new TableCell();
            TableCell spaceCell = new TableCell();
            TableCell spaceCell1 = new TableCell();

            spaceCell.CssClass = "inputTitleCell99";
            spaceCell.ColumnSpan = (6);

            totalMoneyTextCell.CssClass = "inputTitleCell99";
            totalMoneyCell.CssClass = "inputTitleCell99";

            spaceCell1.CssClass = "inputTitleCell99";
            spaceCell1.ColumnSpan = (5);

            spaceCell.Text = " ";
            spaceCell1.Text = " ";

            totalMoneyTextCell.Text = "Toplam Miktar:";
            totalMoneyCell.Text = list.totalAmount + " TL ";

            addNewRow.Cells.Add(spaceCell);
            addNewRow.Cells.Add(totalMoneyTextCell);
            addNewRow.Cells.Add(totalMoneyCell);
            addNewRow.Cells.Add(spaceCell1);
            this.itemsTable.Rows.Add(addNewRow);

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(list.pageCount, this.pageNum, list.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskBDT");
        }
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {

            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListMoneyCodes items = callWebServ.CallListMoneyCodeServiceSweepBack(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             Convert.ToInt16(ViewState["OrderDesc"]),
                                                                             Convert.ToInt32(this.codeStatusBox.SelectedValue),
                                                                             Convert.ToInt32(this.generatedTypeBox.SelectedValue)
                                                                             , this.LoginDatas.AccessToken, this.LoginDatas.User.Id
                                                                             , Convert.ToInt32(this.instutionBox.SelectedValue)
                                                                             ,2);

            if (items != null)
            {
                if (items.errorCode == 0)
                {

                    //for (int i = 0; i < items.moneyCodes.Count; i++)
                    //{
                    //    items.moneyCodes[i].statusName = items.moneyCodes[i].statusName.Replace('.', ',');
                    //    items.moneyCodes[i].generatedKioskName = items.moneyCodes[i].generatedKioskName.Replace('.', ',');
                    //    items.moneyCodes[i].usedKioskName = items.moneyCodes[i].usedKioskName.Replace('.', ',');
                    //    items.moneyCodes[i].insertionDate = items.moneyCodes[i].insertionDate.Replace('.', ',');
                    //    items.moneyCodes[i].customerName = items.moneyCodes[i].customerName.Replace('.', ',');
                    //    items.moneyCodes[i].aboneNo = items.moneyCodes[i].aboneNo.Replace('.', ',');
                    //    items.moneyCodes[i].codeAmount = items.moneyCodes[i].codeAmount.Replace('.', ',');
                    //    items.moneyCodes[i].codeNumber = items.moneyCodes[i].codeNumber.Replace('.', ',');
                    //    items.moneyCodes[i].codeGenerationCase = items.moneyCodes[i].codeGenerationCase.Replace('.', ',');
                    //    items.moneyCodes[i].useDate = items.moneyCodes[i].useDate.Replace('.', ',');
                    //    items.moneyCodes[i].generatedUserName = items.moneyCodes[i].generatedUserName.Replace('.', ',');
                    //    items.moneyCodes[i].instutionName = items.moneyCodes[i].instutionName.Replace('.', ',');
                    //    items.moneyCodes[i].deletingReason = items.moneyCodes[i].deletingReason.Replace('.', ',');
                    //    items.moneyCodes[i].sendingSMS = items.moneyCodes[i].sendingSMS.Replace('.', ',');
                    //    items.moneyCodes[i].approvalSMS = items.moneyCodes[i].approvalSMS.Replace('.', ',');
                    //    items.moneyCodes[i].codeDeleteUser = items.moneyCodes[i].codeDeleteUser.Replace('.', ',');

                    //}
                    ExportExcellDatas exportExcell = new ExportExcellDatas();

                    string[] headerNames = { "No", "Durum No", "Durum İsmi", "Üretilen Kiosk ", "Kullanılan Kiosk", "Oluşturulma Tarihi",
                                             "Müşteri Adı", "Abone No", "Kart No", "İşlem No", "Kod Miktarı", "Kod No", 
                                             "Kod Üretilme Adımı", "Kullanılma Tarih", "Üreten Kullanıcı No",
                                             "Üreten Kullanıcı İsmi", "Kurum Adı", "Silinme Nedeni", "Gönderilen SMS",
                                             "Onay SMS", "Kodu Silen Kullanıcı" , "Kurum", "Unknown","Onaylayn Kullanıcı","Onay Tarihi"};

                    exportExcell.ExportExcellByBlock(items.moneyCodes, "Kiosk Geçmişe Dönük Para Kod Raporu", headerNames);
                    //exportExcell.ExportExcell(items.KioskReports, items.recordCount, "Kiosk İşlemleri Listesi");
                }
                else if (items.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + items.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelAskiCode");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    protected void deleteButton_Click(object sender, EventArgs e)
    {
        this.SearchOrder(0, 1);
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "TransactionId":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        TransactionId.Text = "<a>İşlem No    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        TransactionId.Text = "<a>İşlem No    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    TransactionId.Text = "<a>İşlem No    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CustomerName":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        CustomerName.Text = "<a>Müşteri Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        CustomerName.Text = "<a>Müşteri Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    CustomerName.Text = "<a>Müşteri Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "AboneNo":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        AboneNo.Text = "<a>Abone No    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        AboneNo.Text = "<a>Abone No    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    AboneNo.Text = "<a>Abone No    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Status":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        Status.Text = "<a>Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        Status.Text = "<a>Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    Status.Text = "<a>Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "GeneretionDate":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        GeneretionDate.Text = "<a>Oluşturulma Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        GeneretionDate.Text = "<a>Oluşturulma Tarihi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    GeneretionDate.Text = "<a>Oluşturulma Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CodeAmount":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        CodeAmount.Text = "<a>Tutarı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        CodeAmount.Text = "<a>Tutarı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    CodeAmount.Text = "<a>Tutarı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "AskiCode":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        AskiCode.Text = "<a>Kod    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        AskiCode.Text = "<a>Kod    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    AskiCode.Text = "<a>Kod    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "GenerationCase":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        GenerationCase.Text = "<a>Kod Üretilme Adımı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        GenerationCase.Text = "<a>Kod Üretilme Adımı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    GenerationCase.Text = "<a>Kod Üretilme Adımı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "UsedDate":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        UsedDate.Text = "<a>Kullanılma Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        UsedDate.Text = "<a>Kullanılma Tarihi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    UsedDate.Text = "<a>Kullanılma Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "GeneratedUser":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        GeneratedUser.Text = "<a>Üreten Kullanıcı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        GeneratedUser.Text = "<a>Üreten Kullanıcı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    GeneratedUser.Text = "<a>Üreten Kullanıcı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Instution":
                if (orderSelectionColumn == 11)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        Instution.Text = "<a>Kurum    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        Instution.Text = "<a>Kurum    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    Instution.Text = "<a>Kurum    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 11;
                    orderSelectionDescAsc = 1;
                }
                break;
            default:
                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {

 


            string dateEnd = endDateField.Text;

            DateTime date1End = new DateTime(2016, 1, 1, 0, 0, 0);
            DateTime date2End = new DateTime(2016, 1, 2, 0, 0, 0);

            if (Convert.ToDateTime(dateEnd) >= date1End && Convert.ToDateTime(dateEnd) < date2End)
            {
                endDateField.Text = "2016-01-01 00:00:00";
            }



            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListMoneyCodes askiCodes = callWebServ.CallListMoneyCodeServiceSweepBack(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             Convert.ToInt32(this.codeStatusBox.SelectedValue),
                                                                             Convert.ToInt32(this.generatedTypeBox.SelectedValue)
                                                                             , this.LoginDatas.AccessToken, this.LoginDatas.User.Id
                                                                             , Convert.ToInt32(this.instutionBox.SelectedValue),1);
            if (askiCodes != null)
            {
                if (askiCodes.errorCode == 0)
                {
                    this.BindListTable(askiCodes);
                }
                else if (askiCodes.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + askiCodes.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CreateKioskCommandSrch");
        }
    }

}