var chars = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'ı', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');

function openNewWindow(url, windowName, parametersString) {
    window.open(url, windowName, parametersString);
}

function queryString(value) {
    //    if (value != null) {
    //        var regEx = new RegExp("(\\?|&)(" + value + "=)(.*?)(&|$|#)", "i");
    //        var exec = regEx.exec(URL);
    //        var result = RegExp.$3;
    //    } else {
    //    var regEx = new RegExp("(\\?)(.*?)($)", "i");
    //    var exec = regEx.exec(URL);
    //    var result = RegExp.$2;
    //    }

    //    return (result)
}

function isNumeric(text) {
    if (isNaN(text))
        return false;
    else
        return true;
}

function checkIsNumeric(whichTextField) {
    if (!isNumeric(whichTextField.value.substring(whichTextField.value.length - 1))) {
        whichTextField.value = whichTextField.value.substring(0, whichTextField.value.length - 1);
    }

    if (whichTextField.value == "") {
        whichTextField.value = 0;
    }
}

function addItems(sourceBox, destBox) {
    var sourceOptions = sourceBox.options;
    for (var i = 0; i < sourceOptions.length; i++) {
        if (sourceOptions[i].selected) {
            var newOption = document.createElement("OPTION");
            newOption.text = sourceOptions[i].text;
            newOption.value = sourceOptions[i].value;
            if (!isContained(destBox, newOption.value))
                destBox.options.add(newOption);
        }
    }
}

function removeItems(box) {
    for (var i = 0; i < box.options.length; i++) {
        if (box.options[i].selected)
            box.options.remove(i);
    }
}

function isContained(box, itemValue) {
    var status = false;
    for (var i = 0; i < box.options.length; i++) {
        if (box.options[i].value == itemValue) {
            status = true;
            break;
        }
    }
    return status;
}

function dateAdd(p_Interval, p_Number, p_Date) {
    p_Number = new Number(p_Number);
    var dt = new Date(p_Date);
    switch (p_Interval.toLowerCase()) {
        case "yyyy":
            {// year
                dt.setFullYear(dt.getFullYear() + p_Number);
                break;
            }
        case "q":
            {		// quarter
                dt.setMonth(dt.getMonth() + (p_Number * 3));
                break;
            }
        case "m":
            {		// month
                dt.setMonth(dt.getMonth() + p_Number);
                break;
            }
        case "y": 	// day of year
        case "d": 	// day
        case "w":
            {		// weekday
                dt.setDate(dt.getDate() + p_Number);
                break;
            }
        case "ww":
            {	// week of year
                dt.setDate(dt.getDate() + (p_Number * 7));
                break;
            }
        case "h":
            {		// hour
                dt.setHours(dt.getHours() + p_Number);
                break;
            }
        case "n":
            {		// minute
                dt.setMinutes(dt.getMinutes() + p_Number);
                break;
            }
        case "s":
            {		// second
                dt.setSeconds(dt.getSeconds() + p_Number);
                break;
            }
        case "ms":
            {		// second
                dt.setMilliseconds(dt.getMilliseconds() + p_Number);
                break;
            }
        default:
            {
                return "invalid interval: '" + p_Interval + "'";
            }
    }
    return dt;
}

function getBrowserMajorVersion() {
    //4.0 (compatible; MSIE 7.0; Windows NT 5.2; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)
    var majorVersion;
    var startIndex = navigator.appVersion.indexOf("MSIE") + 5;
    var endIndex = navigator.appVersion.indexOf(";", startIndex);
    majorVersion = parseInt(navigator.appVersion.substring(startIndex, endIndex));
    return majorVersion;
}

function resizeArray(toBeResized) {
    var resized = new Array();

    for (var i = 0; i < toBeResized.length; i++) {
        if (toBeResized[i] != null) {
            resized[resized.length] = toBeResized[i];
        }
    }

    return resized;
}

function validateEmailAddress(emailAddress) {
    var reg = new RegExp("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*", "gi");
    var stat = reg.test(emailAddress);
    return stat;
}


function setSelectedItem(listBox, selectedValue) {
    for (var i = 0; i < listBox.length; i++) {
        if (listBox[i].value == selectedValue) {
            listBox[i].selected = true;
            break;
        }
    }
}

function generateRandomNumber(num1, num2) {
    //"Number 2 should be greater than Number 1"
    num1 = parseInt(num1);
    num2 = parseInt(num2);

    var generator = Math.random() * (num2 - num1);
    generator = Math.round(num1 + generator);

    return generator;
}


function getQuerystringValue(paramName) {
    paramName = paramName.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + paramName + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
}

