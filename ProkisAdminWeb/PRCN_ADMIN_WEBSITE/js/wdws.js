﻿
var windowRetVal = false;

function setDialogReturnValue(retVal) {
    windowRetVal = retVal;
}

function openDialoagWindow(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopup(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowUser(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupUser(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowBL(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupBL(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowApproveUser(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupApproveUser(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowAskiCode(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupAskiCode(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowBL(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupBL(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowApproveRole(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupApproveRole(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowRole(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupRole(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowApproveRole(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupApproveRole(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowApproveUser(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupApproveUser(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowKioskReferenceSystem(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupKioskReferenceSystem(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowKioskLocalLog(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupKioskLocalLog(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowAAA(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupAAA(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowAdminAAA(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupAdminAAA(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowAdminBLC(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupAdminBLC(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowBLC(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupBLC(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowAdminATBL(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupAdminAATBL(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowAdminBLC(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupAdminBLC(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowBLC(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupBLC(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}


function openDialoagWindowATBL(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupATBL(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}
function openDialoagWindowKiosk(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupKiosk(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowControlCashNotification(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupControlCashNotification(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}


function openDialoagWindowCutOfMonitoring(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupCutOfMonitoring(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowEnterKioskEmptyCashCount(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupEnterKioskEmptyCashCount(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowKioskManuelEmpty(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupKioskManuelEmpty(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowKioskDevice(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupKioskDevice(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowCreateKioskCommand(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupCreateKioskCommand(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowKioskCommand(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupKioskCommand(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindow2(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopup2(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindow3(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopup3(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, popupID) {

    var modalPopupWindow = document.createElement("div");
    var modalPopupTitleArea = document.createElement("div");
    var modalPopupTitle = document.createElement("div");
    var modalPopupCloseButtonArea = document.createElement("div");
    var modalPopupIFrame = document.createElement("iframe");

    modalPopupWindow.id = popupID;
    modalPopupWindow.appendChild(modalPopupTitleArea);
    modalPopupWindow.appendChild(modalPopupIFrame);
    modalPopupWindow.setAttribute("align", "center");
    modalPopupWindow.setAttribute("class", "modalPopupInternal");
    modalPopupWindow.style.width = frameWidth + "px";
    modalPopupWindow.style.height = (frameHeight + 29) + "px";

    modalPopupTitleArea.appendChild(modalPopupTitle);
    modalPopupTitleArea.appendChild(modalPopupCloseButtonArea);
    modalPopupTitleArea.setAttribute("align", "right");
    modalPopupTitleArea.setAttribute("class", "modalPopupTitleArea");
    modalPopupTitleArea.id = popupID + "_tA";


    modalPopupCloseButtonArea.innerHTML = "<a href=\"javascript:void(0);\" title=\"Close\" onclick=\"hideModalPopup(\'" + popupID + "\');\">" +
            "<img src=\"../images/logout_icon.png\" alt=\"Close\" border=\"0\" /></a>";
    modalPopupCloseButtonArea.setAttribute("class", "modalPopupCloseButtonArea");

    modalPopupTitle.innerHTML = title;
    modalPopupTitle.setAttribute("class", "modalPopupTitle");

    modalPopupIFrame.src = popupUrl;
    modalPopupIFrame.width = frameWidth;
    modalPopupIFrame.height = frameHeight;
    modalPopupIFrame.scrolling = popupScrollable;
    modalPopupIFrame.setAttribute("frameborder", "0");
    modalPopupIFrame.setAttribute("allowtransparency", "true");

    positionModalPopup(modalPopupWindow);

    return modalPopupWindow;
}

function createModalPopup6_(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, popupID) {

    var modalPopupWindow = document.createElement("div");
    var modalPopupTitleArea = document.createElement("div");
    var modalPopupTitle = document.createElement("div");
    var modalPopupCloseButtonArea = document.createElement("div");
    var modalPopupIFrame = document.createElement("iframe");

    modalPopupWindow.id = popupID;
    modalPopupWindow.appendChild(modalPopupTitleArea);
    modalPopupWindow.appendChild(modalPopupIFrame);
    modalPopupWindow.setAttribute("align", "center");
    modalPopupWindow.setAttribute("class", "modalPopupInternal");
    modalPopupWindow.style.width = frameWidth + "px";
    modalPopupWindow.style.height = (frameHeight + 29) + "px";

    modalPopupTitleArea.appendChild(modalPopupTitle);
    modalPopupTitleArea.appendChild(modalPopupCloseButtonArea);
    modalPopupTitleArea.setAttribute("align", "right");
    modalPopupTitleArea.setAttribute("class", "modalPopupTitleArea");
    modalPopupTitleArea.id = popupID + "_tA";

    modalPopupTitle.innerHTML = title;
    modalPopupTitle.setAttribute("class", "modalPopupTitle");

    if (popupID == 'modalPopup_Merchant' || popupID == 'modalPopup_HSM' || popupID == 'modalPopup_User') {
    }
    else {
        modalPopupCloseButtonArea.innerHTML = "<a href=\"javascript:void(0);\" title=\"Close\" onclick=\"hideModalPopup(\'" + popupID + "\');\">" +
            "<img src=\"../images/logout_icon.png\" alt=\"Close\" border=\"0\" /></a>";
        modalPopupCloseButtonArea.setAttribute("class", "modalPopupCloseButtonArea");
    }

    modalPopupIFrame.src = popupUrl;
    modalPopupIFrame.width = frameWidth;
    modalPopupIFrame.height = frameHeight;
    modalPopupIFrame.scrolling = popupScrollable;
    modalPopupIFrame.setAttribute("frameborder", "0");
    modalPopupIFrame.setAttribute("allowtransparency", "true");

    positionModalPopup(modalPopupWindow);

    return modalPopupWindow;
}

function createModalPopupCardTxn(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, popupID) {

    var modalPopupWindow = document.createElement("div");
    var modalPopupTitleArea = document.createElement("div");
    var modalPopupTitle = document.createElement("div");
    var modalPopupCloseButtonArea = document.createElement("div");
    var modalPopupIFrame = document.createElement("iframe");

    modalPopupWindow.id = popupID;
    modalPopupWindow.appendChild(modalPopupTitleArea);
    modalPopupWindow.appendChild(modalPopupIFrame);
    modalPopupWindow.setAttribute("align", "center");
    modalPopupWindow.setAttribute("class", "modalPopupInternal");
    modalPopupWindow.style.width = frameWidth + "px";
    modalPopupWindow.style.height = (frameHeight + 29) + "px";

    modalPopupTitleArea.appendChild(modalPopupTitle);
    modalPopupTitleArea.appendChild(modalPopupCloseButtonArea);
    modalPopupTitleArea.setAttribute("align", "right");
    modalPopupTitleArea.setAttribute("class", "modalPopupTitleArea");
    modalPopupTitleArea.id = popupID + "_tA";

    modalPopupTitle.innerHTML = title;
    modalPopupTitle.setAttribute("class", "modalPopupTitle");

    modalPopupCloseButtonArea.innerHTML = "<a href=\"javascript:void(0);\" title=\"Close\" onclick=\"hideModalPopup(\'" + popupID + "\');\">" +
        "<img src=\"../images/logout_icon.png\" alt=\"Close\" border=\"0\" /></a>";
    modalPopupCloseButtonArea.setAttribute("class", "modalPopupCloseButtonArea");

    modalPopupIFrame.src = popupUrl;
    modalPopupIFrame.width = frameWidth;
    modalPopupIFrame.height = frameHeight;
    modalPopupIFrame.scrolling = popupScrollable;
    modalPopupIFrame.setAttribute("frameborder", "0");
    modalPopupIFrame.setAttribute("allowtransparency", "true");

    positionModalPopupCardTxn(modalPopupWindow);

    return modalPopupWindow;
}

function createModalPopup2(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, popupID) {

    var modalPopupWindow = document.createElement("div");
    var modalPopupTitleArea = document.createElement("div");
    var modalPopupTitle = document.createElement("div");
    var modalPopupCloseButtonArea = document.createElement("div");
    var modalPopupIFrame = document.createElement("iframe");

    modalPopupWindow.id = popupID;
    modalPopupWindow.appendChild(modalPopupTitleArea);
    modalPopupWindow.appendChild(modalPopupIFrame);
    modalPopupWindow.setAttribute("align", "center");
    modalPopupWindow.setAttribute("class", "modalPopupInternal");
    modalPopupWindow.style.width = frameWidth + "px";
    modalPopupWindow.style.height = (frameHeight + 29) + "px";

    modalPopupTitleArea.appendChild(modalPopupTitle);
    modalPopupTitleArea.appendChild(modalPopupCloseButtonArea);
    modalPopupTitleArea.setAttribute("align", "right");
    modalPopupTitleArea.setAttribute("class", "modalPopupTitleArea");
    modalPopupTitleArea.id = popupID + "_tA";

    modalPopupTitle.innerHTML = title;
    modalPopupTitle.setAttribute("class", "modalPopupTitle");

    var lastModalPopup = getLastModalPopup();

    modalPopupIFrame.src = popupUrl;
    modalPopupIFrame.width = frameWidth;
    modalPopupIFrame.height = frameHeight;
    modalPopupIFrame.scrolling = popupScrollable;
    modalPopupIFrame.setAttribute("frameborder", "0");
    modalPopupIFrame.setAttribute("allowtransparency", "true");

    positionModalPopup2(modalPopupWindow);

    return modalPopupWindow;
}

function createModalPopup5_2(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, popupID) {

    var modalPopupWindow = document.createElement("div");
    var modalPopupTitleArea = document.createElement("div");
    var modalPopupTitle = document.createElement("div");
    var modalPopupCloseButtonArea = document.createElement("div");
    var modalPopupIFrame = document.createElement("iframe");

    modalPopupWindow.id = popupID;
    modalPopupWindow.appendChild(modalPopupTitleArea);
    modalPopupWindow.appendChild(modalPopupIFrame);
    modalPopupWindow.setAttribute("align", "center");
    modalPopupWindow.setAttribute("class", "modalPopupInternal");
    modalPopupWindow.style.width = frameWidth + "px";
    modalPopupWindow.style.height = (frameHeight + 29) + "px";

    modalPopupTitleArea.appendChild(modalPopupTitle);
    modalPopupTitleArea.appendChild(modalPopupCloseButtonArea);
    modalPopupTitleArea.setAttribute("align", "right");
    modalPopupTitleArea.setAttribute("class", "modalPopupTitleArea");
    modalPopupTitleArea.id = popupID + "_tA";

    modalPopupTitle.innerHTML = title;
    modalPopupTitle.setAttribute("class", "modalPopupTitle");

    var lastModalPopup = getLastModalPopup();

    if (popupID == 'modalPopup_Merchant' || popupID == 'modalPopup_HSM' || popupID == 'modalPopup_User' || lastModalPopup.id == 'modalPopup_Merchant' || lastModalPopup.id == 'modalPopup_HSM' || lastModalPopup.id == 'modalPopup_User') {
    }
    else {
        modalPopupCloseButtonArea.innerHTML = "<a href=\"javascript:void(0);\" title=\"Close\" onclick=\"hideModalPopup(\'" + popupID + "\');\">" +
            "<img src=\"../images/logout_icon.png\" alt=\"Close\" border=\"0\" /></a>";
        modalPopupCloseButtonArea.setAttribute("class", "modalPopupCloseButtonArea");
    }

    modalPopupIFrame.src = popupUrl;
    modalPopupIFrame.width = frameWidth;
    modalPopupIFrame.height = frameHeight;
    modalPopupIFrame.scrolling = popupScrollable;
    modalPopupIFrame.setAttribute("frameborder", "0");
    modalPopupIFrame.setAttribute("allowtransparency", "true");

    positionModalPopup2(modalPopupWindow);

    return modalPopupWindow;
}

function createModalPopup3(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, popupID) {

    var modalPopupWindow = document.createElement("div");
    var modalPopupTitleArea = document.createElement("div");
    var modalPopupTitle = document.createElement("div");
    var modalPopupCloseButtonArea = document.createElement("div");
    var modalPopupIFrame = document.createElement("iframe");

    modalPopupWindow.id = popupID;
    modalPopupWindow.appendChild(modalPopupTitleArea);
    modalPopupWindow.appendChild(modalPopupIFrame);
    modalPopupWindow.setAttribute("align", "center");
    modalPopupWindow.setAttribute("class", "modalPopupInternal");
    modalPopupWindow.style.width = frameWidth + "px";
    modalPopupWindow.style.height = (frameHeight + 29) + "px";

    modalPopupTitleArea.appendChild(modalPopupTitle);
    modalPopupTitleArea.appendChild(modalPopupCloseButtonArea);
    modalPopupTitleArea.setAttribute("align", "right");
    modalPopupTitleArea.setAttribute("class", "modalPopupTitleArea");
    modalPopupTitleArea.id = popupID + "_tA";

    modalPopupTitle.innerHTML = title;
    modalPopupTitle.setAttribute("class", "modalPopupTitle");

    var lastModalPopup = getLastModalPopup();

    modalPopupIFrame.src = popupUrl;
    modalPopupIFrame.width = frameWidth;
    modalPopupIFrame.height = frameHeight;
    modalPopupIFrame.scrolling = popupScrollable;
    modalPopupIFrame.setAttribute("frameborder", "0");
    modalPopupIFrame.setAttribute("allowtransparency", "true");

    positionModalPopup3(modalPopupWindow);

    return modalPopupWindow;
}

function createModalPopup4_3(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, popupID) {

    var modalPopupWindow = document.createElement("div");
    var modalPopupTitleArea = document.createElement("div");
    var modalPopupTitle = document.createElement("div");
    var modalPopupCloseButtonArea = document.createElement("div");
    var modalPopupIFrame = document.createElement("iframe");

    modalPopupWindow.id = popupID;
    modalPopupWindow.appendChild(modalPopupTitleArea);
    modalPopupWindow.appendChild(modalPopupIFrame);
    modalPopupWindow.setAttribute("align", "center");
    modalPopupWindow.setAttribute("class", "modalPopupInternal");
    modalPopupWindow.style.width = frameWidth + "px";
    modalPopupWindow.style.height = (frameHeight + 29) + "px";

    modalPopupTitleArea.appendChild(modalPopupTitle);
    modalPopupTitleArea.appendChild(modalPopupCloseButtonArea);
    modalPopupTitleArea.setAttribute("align", "right");
    modalPopupTitleArea.setAttribute("class", "modalPopupTitleArea");
    modalPopupTitleArea.id = popupID + "_tA";

    modalPopupTitle.innerHTML = title;
    modalPopupTitle.setAttribute("class", "modalPopupTitle");

    var lastModalPopup = getLastModalPopup();

    if (popupID == 'modalPopup_Merchant' || popupID == 'modalPopup_HSM' || popupID == 'modalPopup_User' || lastModalPopup.id == 'modalPopup_Merchant' || lastModalPopup.id == 'modalPopup_HSM' || lastModalPopup.id == 'modalPopup_User') {
    }
    else {
        modalPopupCloseButtonArea.innerHTML = "<a href=\"javascript:void(0);\" title=\"Close\" onclick=\"hideModalPopup(\'" + popupID + "\');\">" +
            "<img src=\"../images/logout_icon.png\" alt=\"Close\" border=\"0\" /></a>";
        modalPopupCloseButtonArea.setAttribute("class", "modalPopupCloseButtonArea");
    }

    modalPopupIFrame.src = popupUrl;
    modalPopupIFrame.width = frameWidth;
    modalPopupIFrame.height = frameHeight;
    modalPopupIFrame.scrolling = popupScrollable;
    modalPopupIFrame.setAttribute("frameborder", "0");
    modalPopupIFrame.setAttribute("allowtransparency", "true");

    positionModalPopup3(modalPopupWindow);

    return modalPopupWindow;
}

function positionModalPopup(modalPopup) {
    var previousModalPopup = getLastModalPopup();
    var lefPos = "";
    var topPos = "";

    modalPopup.style.position = "absolute";
    if (previousModalPopup == null) {
        leftPos = ((window.screen.availWidth - modalPopup.style.width.replace("px", "")) / 2) + 60;
        topPos = ((window.screen.availHeight - modalPopup.style.height.replace("px", "")) / 2) - 50;
        leftPos = parseInt(leftPos);
        topPos = parseInt(topPos);
    }
    else {
        leftPos = previousModalPopup.style.left.replace("px", "");
        topPos = previousModalPopup.style.top.replace("px", "");
        leftPos = parseInt(leftPos) + 30;
        topPos = parseInt(topPos) + 30;
    }
    modalPopup.style.top = topPos + "px";
    modalPopup.style.left = leftPos + "px";
}

function positionModalPopup2(modalPopup) {
    var previousModalPopup = getLastModalPopup();
    var lefPos = "";
    var topPos = "";

    modalPopup.style.position = "absolute";
    if (previousModalPopup == null) {
        leftPos = ((window.screen.availWidth - modalPopup.style.width.replace("px", "")) / 2);
        topPos = ((window.screen.availHeight - modalPopup.style.height.replace("px", "")) / 2);
        leftPos = parseInt(leftPos);
        topPos = parseInt(topPos);
    }
    else {
        leftPos = previousModalPopup.style.left.replace("px", "");
        topPos = previousModalPopup.style.top.replace("px", "");
        leftPos = parseInt(leftPos) + 40;
        topPos = parseInt(topPos) + 120;
    }
    modalPopup.style.top = topPos + "px";
    modalPopup.style.left = leftPos + "px";
}

function positionModalPopup3(modalPopup) {
    var previousModalPopup = getLastModalPopup();
    var lefPos = "";
    var topPos = "";

    modalPopup.style.position = "absolute";
    if (previousModalPopup == null) {
        leftPos = ((window.screen.availWidth - modalPopup.style.width.replace("px", "")) / 2);
        topPos = ((window.screen.availHeight - modalPopup.style.height.replace("px", "")) / 2);
        leftPos = parseInt(leftPos);
        topPos = parseInt(topPos);
    }
    else {
        leftPos = previousModalPopup.style.left.replace("px", "");
        topPos = previousModalPopup.style.top.replace("px", "");
        leftPos = parseInt(leftPos) + 190;
        topPos = parseInt(topPos) + 120;
    }
    modalPopup.style.top = topPos + "px";
    modalPopup.style.left = leftPos + "px";
}

var modalPopupZIndex = 9000;

function showModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_" + generateRandomNumber(10001, 20001);
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupKiosk(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_Kiosk";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupControlCashNotification(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_ControlCashNotification";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}


function showModalPopupCutOfMonitoring(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_CutOfMonitoring";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupEnterKioskEmptyCashCount(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_EnterKioskEmptyCashCount";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}



function showModalPopupKioskManuelEmpty(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_KioskManuelEmpty";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupKioskDevice(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_KioskDevice";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupCreateKioskCommand(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_CreateKioskCommand";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}
function showModalPopupKioskCommand(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_KioskCommand";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupUser(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_User";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupBL(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_BL";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupApproveUser(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_ApproveUser";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}


function showModalPopupKioskReferenceSystem(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_KioskReferenceSystem";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

//showModalPopupKioskLocalLog
function showModalPopupKioskLocalLog(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_KioskLocalLog";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupAAA(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_Fraud";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupAdminAAA(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_FraudAdmin";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupAdminBLC(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_BLCAdmin";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupBLC(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_BLC";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupAdminAATBL(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_FraudAdmin";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupATBL(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_Fraud";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupAskiCode(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_AskiCode";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupBL(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_BL";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupApproveRole(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_ApproveRole";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupApproveRole(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_ApproveUser";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupRole(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_Role";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupApproveRole(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_ApproveRole";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopup2(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_" + generateRandomNumber(10001, 20001);
    var modalPopupWindow = createModalPopup2(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 600;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopup3(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_" + generateRandomNumber(10001, 20001);
    var modalPopupWindow = createModalPopup3(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 600;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function focusModalPopup(modalPopupID) {
    var modalPopupContainer = document.getElementById("modalPopupArea");
    var modalPopupCount = modalPopupContainer.childNodes.length;

    for (var i = (modalPopupCount - 1) ; i >= 0; i--) {
        try {
            if (modalPopupContainer.childNodes[i].tagName.toLowerCase() == "div" &&
                    modalPopupContainer.childNodes[i].id.indexOf("modalPopup_") == 0) {
                document.getElementById(modalPopupContainer.childNodes[i].id + "_tA").className = "modalPopupTitleArea_Inactive";
                modalPopupContainer.childNodes[i].style.borderColor = "#999999";
            }
        }
        catch (exception) {
        }
    }

    var lastModalPopup = getLastModalPopup();

    if (lastModalPopup != null) {
        document.getElementById(lastModalPopup.id + "_tA").className = "modalPopupTitleArea";
        lastModalPopup.style.borderColor = "#8A8B8C";
    }
}

function hideModalPopup(id) {
    var modalPopupContainer = document.getElementById("modalPopupArea");
    var modalPopupCount = modalPopupContainer.childNodes.length;

    for (var i = 0; i < modalPopupCount; i++) {
        try {
            if (modalPopupContainer.childNodes[i].id == id) {
                var modalPopup = modalPopupContainer.childNodes[i];
                modalPopupContainer.removeChild(modalPopup);
                modalPopup.innerHTML = "";
                break;
            }
        }
        catch (exception) {
        }
    }

    var lastModalPopup = getLastModalPopup();

    if (lastModalPopup == null) {
        modalPopupContainer.style.visibility = "hidden";
        modalPopupContainer.style.display = "none";
    }
    else {
        focusModalPopup(lastModalPopup.id);
    }

    if (id == 'modalPopup_User')
        window.parent.location = "../User/ListUsers.aspx";
    else if (id == 'modalPopup_Kiosk')
        window.parent.location = "../Kiosk/ListKiosk.aspx";
    else if (id == 'modalPopup_Role')
        window.parent.location = "../UserRole/ListUserRoles.aspx";
    else if (id == 'modalPopup_KioskCommand')
        window.parent.location = "../Kiosk/ListKioskCommand.aspx";
    else if (id == 'modalPopup_ApproveRole')
        window.parent.location = "../UserRole/ApproveUserRole.aspx";
    else if (id == 'modalPopup_ApproveUser')
        window.parent.location = "../User/ApproveUser.aspx";
    else if (id == 'modalPopup_KioskDevice')
        window.parent.location = "../DevicePort/ListKioskDevicePort.aspx";
    else if (id == 'modalPopup_KioskManuelEmpty')
        window.parent.location = "../Account/ManuelEmptyKiosk.aspx";
    else if (id == 'modalPopup_ControlCashNotification')
        window.parent.location = "../Account/ControlCashNotification.aspx";
    else if (id == 'modalPopup_Approved')
        window.parent.location = "../Account/ListApprovedCollectedMoney.aspx";

}

function hideModalPopup_Exit(id) {
    var modalPopupContainer = document.getElementById("modalPopupArea");
    var modalPopupCount = modalPopupContainer.childNodes.length;

    for (var i = 0; i < modalPopupCount; i++) {
        try {
            if (modalPopupContainer.childNodes[i].id == id) {
                var modalPopup = modalPopupContainer.childNodes[i];
                modalPopupContainer.removeChild(modalPopup);
                modalPopup.innerHTML = "";
                break;
            }
        }
        catch (exception) {
        }
    }

    var lastModalPopup = getLastModalPopup();

    if (lastModalPopup == null) {
        modalPopupContainer.style.visibility = "hidden";
        modalPopupContainer.style.display = "none";
    }
    else {
        focusModalPopup(lastModalPopup.id);
    }

    //if (id == 'modalPopup_User')
    //    window.parent.location = "../User/ListUsers.aspx";
    //else if (id == 'modalPopup_Kiosk')
    //    window.parent.location = "../Kiosk/ListKiosk.aspx";
    //else if (id == 'modalPopup_Role')
    //    window.parent.location = "../UserRole/ListUserRoles.aspx";
    //else if (id == 'modalPopup_KioskCommand')
    //    window.parent.location = "../Kiosk/ListKioskCommand.aspx";
    //else if (id == 'modalPopup_ApproveRole')
    //    window.parent.location = "../UserRole/ApproveUserRole.aspx";
    //else if (id == 'modalPopup_ApproveUser')
    //    window.parent.location = "../User/ApproveUser.aspx";
    //else if (id == 'modalPopup_KioskDevice')
    //    window.parent.location = "../DevicePort/ListKioskDevicePort.aspx";
    //else if (id == 'modalPopup_KioskManuelEmpty')
    //    window.parent.location = "../Account/ManuelEmptyKiosk.aspx";
    //else if (id == 'modalPopup_ControlCashNotification')
    //    window.parent.location = "../Account/ControlCashNotification.aspx";

}

function getLastModalPopup() {
    var modalPopupContainer = document.getElementById("modalPopupArea");
    var modalPopupCount = modalPopupContainer.childNodes.length;
    var lastModalPopup = null;

    for (var i = (modalPopupCount - 1) ; i >= 0; i--) {
        try {
            if (modalPopupContainer.childNodes[i].tagName.toLowerCase() == "div" &&
                    modalPopupContainer.childNodes[i].id.indexOf("modalPopup_") == 0) {
                lastModalPopup = modalPopupContainer.childNodes[i];
                break;
            }
        }
        catch (exception) {
        }
    }
    return lastModalPopup;
}

function getPreviousModalPopup(modalPopupID) {
    var modalPopupContainer = document.getElementById("modalPopupArea");
    var modalPopupCount = modalPopupContainer.childNodes.length;
    var previousModalPopup = null;

    for (var i = (modalPopupCount - 1) ; i >= 0; i--) {
        try {
            if (modalPopupContainer.childNodes[i].tagName.toLowerCase() == "div" &&
                    modalPopupContainer.childNodes[i].id.indexOf("modalPopup_") == 0 &&
                    modalPopupContainer.childNodes[i] != modalPopupID) {
                previousModalPopup = modalPopupContainer.childNodes[i];
                break;
            }
        }
        catch (exception) {
        }
    }
    return previousModalPopup;
}

function hideModalPopup2() {
    var lastModalPopupID = "";

    var lastModalPopup = getLastModalPopup();

    if (lastModalPopup != null)
        hideModalPopup(lastModalPopup.id);
}


function hideModalPopup2_Exit() {
    var lastModalPopupID = "";

    var lastModalPopup = getLastModalPopup();

    if (lastModalPopup != null)
        hideModalPopup_Exit(lastModalPopup.id);
}

function hideModalPopupLastandPrevious(id) {
    var lastModalPopupID = "";
    hideModalPopup(id);

    var lastModalPopup = getLastModalPopup();

    if (lastModalPopup != null)
        hideModalPopup(lastModalPopup.id);

    if (id == 'modalPopup_User')
        window.parent.location = "../User/ListUsers.aspx";
    else if (id == 'modalPopup_Role')
        window.parent.location = "../UserRole/ListUserRoles.aspx";
    else if (id == 'modalPopup_Kiosk')
        window.parent.location = "../Kiosk/ListKiosk.aspx";
    else if (id == 'modalPopup_KioskCommand')
        window.parent.location = "../Kiosk/ListKioskCommand.aspx";
    else if (id == 'modalPopup_AskiCode')
        window.parent.location = "../AskiCode/ListAskiCodes.aspx";
    else if (id == 'modalPopup_ApproveRole')
        window.parent.location = "../UserRole/ApproveUserRole.aspx";
    else if (id == 'modalPopup_ApproveUser')
        window.parent.location = "../User/ApproveUser.aspx";
    else if (id == 'modalPopup_KioskDevice')
        window.parent.location = "../DevicePort/ListKioskDevicePort.aspx";
    else if (id == 'modalPopup_KioskManuelEmpty')
        window.parent.location = "../Account/ManuelEmptyKiosk.aspx";
    else if (id == 'modalPopup_ApprovedCollectedMoney')
        hideModalPopup_Exit(lastModalPopup.id);
    else if (id == 'modalPopup_CashCount')
        window.parent.location = "../Account/ControlCashNotification.aspx";
    else if (id == 'modalPopup_CutOffMonitoring')
        window.parent.location = "../Settings/CutOfMonitoring.aspx";
    else if (id == 'modalPopup_KioskErrorLogs')
        window.parent.location = "../Kiosk/ListKioskErrorLogs.aspx";
    else if (id == 'modalPopup_ListKioskDispenserCashCount')
        window.parent.location = "../Account/ListKioskDispenserCashCount.aspx";
    else if (id == 'modalPopup_ListRepairCard')
        window.parent.location = "../Transaction/ListTransactions.aspx";
    else if (id == 'modalPopup_RepairCard')
        window.parent.location = "../Transaction/ListRepairCard.aspx";
    else if (id == 'modalPopup_ListSetting')
        window.parent.location = "../Settings/ListSetting.aspx";
    else if (id == 'modalPopup_ListAskiCodes')
        window.parent.location = "../AskiCode/ListAskiCodes.aspx"; 
    else if (id == 'modalPopup_ListKioskReferenceSystem')
        window.parent.location = "../Kiosk/ListKioskReferenceSystem.aspx";
    else if (id == 'modalPopup_ListKioskLocalLog')
        window.parent.location = "../Transaction/ListKioskLocalLog.aspx";
}

function hideModalPopupLastandRefresh(id) {
    if (id == 'User') {
        hideModalPopup('modalPopup_User');
        window.parent.location = "../User/ListUsers.aspx";
    }
    else if (id == 'Role') {
        hideModalPopup('modalPopup_Role');
        window.parent.location = "../UserRole/ListUserRoles.aspx";
    }
    else if (id == 'Kiosk') {
        hideModalPopup('modalPopup_Kiosk');
        window.parent.location = "../Kiosk/ListKiosk.aspx";
    }
    else if (id == 'KioskDevice') {
        hideModalPopup('modalPopup_KioskDevice');
        window.parent.location = "../DevicePort/ListKioskDevicePort.aspx";
    }
    else if (id == 'KioskCommand') {
        hideModalPopup('modalPopup_KioskCommand');
        window.parent.location = "../Kiosk/ListKioskCommand.aspx";
    }
    else if (id == 'KioskManuelEmpty') {
        hideModalPopup('modalPopup_KioskManuelEmpty');
        window.parent.location = "../Account/ManuelEmptyKiosk.aspx";
    }
    else if (id == 'AskiCode') {
        hideModalPopup('modalPopup_AskiCode');
        window.parent.location = "../AskiCode/ListAskiCodes.aspx";
    }
    else if (id == 'ApproveRole') {
        hideModalPopup('modalPopup_ApproveRole');
        window.parent.location = "../UserRole/ApproveUserRole.aspx";
    }
    else if (id == 'ApproveUser') {
        hideModalPopup('modalPopup_ApproveUser');
        window.parent.location = "../User/ApproveUser.aspx";
    }
    else if (id == 'Fraud') {
        hideModalPopup('modalPopup_Fraud');
        window.parent.location = "../Transaction/ApproveAccountAction.aspx";
    }
    else if (id == 'FraudAdmin') {
        hideModalPopup('modalPopup_FraudAdmin');
        window.parent.location = "../Transaction/AdminApproveAccountAction.aspx";
    }
    else if (id == 'BLC') {
        hideModalPopup('modalPopup_BLC');
        window.parent.location = "../Customer/ListWebCustomer.aspx";
    }
    else if (id == 'BLCAdmin') {
        hideModalPopup('modalPopup_BLCAdmin');
        window.parent.location = "../Customer/ApproveBlackListWebCustomer.aspx";
    }
    else if (id == 'BlackList') {
        hideModalPopup('modalPopup_BL');
        window.parent.location = "../Customer/ListBlackList.aspx";
    }
    else if (id == 'RepairCard') {
        hideModalPopup('modalPopup_BL');
        window.parent.location = "../Transaction/ListRepairCard.aspx";
    }
    else if (id == 'KioskReferenceSystem') {
        hideModalPopup('modalPopup_KioskReferenceSystem');
        window.parent.location = "../Kiosk/ListKioskReferenceSystem.aspx";
    }
    else if (id == 'KioskLocalLog') {
        hideModalPopup('modalPopup_KioskLocalLog');
        window.parent.location = "../Transaction/ListKioskLocalLog.aspx";
    }
    }

function getOpenerModalPopup() {
    var modalPopupContainer = parent.document.getElementById("modalPopupArea");
    var modalPopupCount = modalPopupContainer.childNodes.length;
    var openerModalPopup = null;
    var count = 0;

    for (var i = (modalPopupCount - 1) ; i >= 0; i--) {
        try {
            if (modalPopupContainer.childNodes[i].tagName.toLowerCase() == "div" &&
                    modalPopupContainer.childNodes[i].id.indexOf("modalPopup_") == 0) {
                count++;
                if (count == 2) {
                    openerModalPopup = modalPopupContainer.childNodes[i];
                    break;
                }
            }
        }
        catch (exception) {
        }
    }
    return openerModalPopup;
}

function getFrameInModalPopup(modalPopup) {
    var childNodes = modalPopup.childNodes.length;
    var iframe = null;

    for (var i = 0; i < childNodes; i++) {
        if (modalPopup.childNodes[i].tagName.toLowerCase() == "iframe") {
            iframe = modalPopup.childNodes[i];
            break;
        }
    }

    return iframe;
}

function runDialogCloseScript(userMessage, retVal) {

    retval = (retVal == null ? false : retVal);

    var openerModalPopup = getOpenerModalPopup();
    var iframe = null;

    if (openerModalPopup != null) {
        iframe = getFrameInModalPopup(openerModalPopup);
    }

    parent.setDialogReturnValue(retVal);

    if (retVal) {
        if (iframe != null) {
            iframe.contentWindow.refresh();
        }
        else {
            parent.refresh();
        }
    }

    parent.alert(userMessage);
    parent.hideModalPopup2();
}

function showUserWindow(itemID) {
    var url = "../User/ShowUser.aspx?itemID=" + itemID;
    var retVal = openDialoagWindowUser(url, 510, 360, window, "Detaylar");
    return retVal;
}

function showBLWindow() {
    var url = "../Customer/ShowBlackList.aspx";
    var retVal = openDialoagWindowBL(url, 400, 270, window, "Black List Kişi Ekleme");
    return retVal;
}

function showApproveUserWindow(itemID) {
    var url = "../User/ShowApproveUser.aspx?itemID=" + itemID;
    var retVal = openDialoagWindowApproveUser(url, 800, 420, window, "Detaylar");
    return retVal;
}

function showRoleWindow(itemID) {
    var url = "../UserRole/ShowUserRole.aspx?itemID=" + itemID;
    var retVal = openDialoagWindowRole(url, 510, 600, window, "Detaylar");
    return retVal;
}

function showApproveRoleWindow(itemID) {
    var url = "../UserRole/ShowApproveUserRole.aspx?itemID=" + itemID;
    var retVal = openDialoagWindowApproveRole(url, 900, 600, window, "Detaylar");
    return retVal;
}

function showTransactionDetailWindow(transactionID) {
    var url = "../Transaction/ListTransactionDetails.aspx?transactionId=" + transactionID;
    var retVal = openDialoagWindow(url, 1200, 600, window, "İşlem Detayı");
    return retVal;
}



function showChangeCompleteStatusDetailWindow(transactionID) {
    var url = "../Transaction/ChangeCompleteStatus.aspx?itemID=" + transactionID;
    var retVal = openDialoagWindow(url, 400, 450, window, "İşlem Durumu Detayı");
    return retVal;
}

function cancelApprovedTxnMet(transactionID) {
    var url = "../Transaction/CancelApprovedTxn.aspx?itemID=" + transactionID;
    var retVal = openDialoagWindow(url, 400, 450, window, "İşlem İptal Ekranı");
    return retVal;
}
function showConditionDetailWindow(kioskID) {
    var url = "../Kiosk/ListKioskConditionDetails.aspx?kioskId=" + kioskID;
    var retVal = openDialoagWindow(url, 800, 350, window, "Para Akışı");
    return retVal;
}

function showCashCountWindow(kioskID) {
    var url = "../Kiosk/ListKioskCashCount.aspx?kioskId=" + kioskID;
    var retVal = openDialoagWindow(url, 400, 300, window, "Para Adetleri");
    return retVal;
}

function showCashBoxCashCountWindow(kioskID) {
    var url = "../Account/ListFullCashBoxCashCount.aspx?kioskId=" + kioskID;
    var retVal = openDialoagWindow(url, 400, 600, window, "Para Üstü Adetleri");
    return retVal;
}

function showCashBoxCashCountWindow(kioskID) {
    var url = "../Account/ListFullCashBoxCashCount.aspx?kioskId=" + kioskID;
    var retVal = openDialoagWindow(url, 400, 600, window, "Para Üstü Adetleri");
    return retVal;
}

function showKioskLocalLogDetailsWindow(transactionID) {
    var url = "../Transaction/ListKioskLocalLogDetails.aspx?transactionID=" + transactionID;
    var retVal = openDialoagWindow(url, 1200, 600, window, "Kiosk Yerel Log Detayı");
    return retVal;
}

function showSMSLogDetailsWindow(moneyCodeId) {
    
    var url = "../AskiCode/SMSLogDetails.aspx?itemID=" + moneyCodeId;
    var retVal = openDialoagWindow(url, 400, 600, window, "SMS Log Detayı");
    return retVal;
}

function showCashBoxRejectCountWindow(kioskID) {
    var url = "../Account/ListCashBoxRejectCount.aspx?kioskId=" + kioskID;
    var retVal = openDialoagWindow(url, 400, 300, window, "Para Üstü Reject Adetleri");
    return retVal;
}



function showKioskEmptyCashCountWindow(kioskId, emptyKioskId, opType) {
    var url = "../Kiosk/ListKioskCashCount.aspx?operationId=" + emptyKioskId + "&operationType=" + opType + "&kioskId=" + kioskId;
    if (opType == 0) {
        var retVal = openDialoagWindow(url, 400, 500, window, "Fark Para Adetleri");
    } else if (opType == 1) {
        var retVal = openDialoagWindow(url, 400, 500, window, "Merkez Para Adetleri");
    }
    
    return retVal;
}


function showKioskTotalEmptyCashCountWindow(kioskId, emptyKioskId, opType, startDate, endDate) {
    var url = "../Kiosk/ListKioskTotalCashCount.aspx?operationId=" + emptyKioskId + "&operationType=" + opType + "&kioskId=" + kioskId + "&startDate=" + startDate + "&endDate=" + endDate;
    if (opType == 0) {
        var retVal = openDialoagWindow(url, 400, 500, window, "Fark Para Adetleri");
    } else if (opType == 1) {
        var retVal = openDialoagWindow(url, 400, 500, window, "Merkez Para Adetleri");
    }

    return retVal;
}


function showEnterKioskEmptyCashCountButtonWindow(kioskID, insertDate, emptyKioskId) {
    var url = "../Account/ListEnterKioskCashCount.aspx?kioskID=" + kioskID + "&insertDate=" + insertDate + "&emptyKioskId=" + emptyKioskId ;
    var retVal = openDialoagWindow(url, 400, 500, window, "Girilen Para Adetleri Detay");
    return retVal;
}

function showKioskFullCashBoxCountWindow(operationId) {
    var url = "../Account/ListFullCashBoxCashCount.aspx?operationId=" + operationId;
    var retVal = openDialoagWindow(url, 400, 300, window, "Doldurulan Para Adetleri");
    return retVal;
}

function showAccountDetailWindow(accountId, instutionId) {
    var url = "../Account/ListAccountDetails.aspx?accountId=" + accountId + "&instutionId=" + instutionId;
    var retVal = openDialoagWindow(url, 800, 450, window, "Hesap Detay");
    return retVal;
}

function showKioskWindow(itemID) {
    var url = "../Kiosk/ShowKiosk.aspx?kioskID=" + itemID;
    var retVal = openDialoagWindowKiosk(url, 550, 580, window, "Kiosk Detay Ekranı");
    return retVal;
}

function showEnterKioskEmptyCash(itemID) {
    var url = "../Account/ShowKioskEmptyCashCount.aspx?controlCashId=" + itemID;
    var retVal = openDialoagWindowControlCashNotification(url, 550, 600, window, "Para Giriş Ekranı");
    return retVal;
}

function showKioskReferenceSystem(itemID) {
    var url = "../Kiosk/ShowKioskReferenceSystem.aspx?referenceId=" + itemID;
    var retVal = openDialoagWindow(url, 400, 500, window, "Referans Sistemi Giriş Ekranı");
    return retVal;
}

function showKioskLocalLog() {
    var url = "../Transaction/ShowKioskLocalLog.aspx";
    var retVal = openDialoagWindow(url, 400, 400, window, "Kiosk Yerel Log Giriş Ekranı");
    return retVal;
}



function showKioskDispenserCashCount(itemID, Id) {
    var url = "../Account/ShowKioskDispenserCashCount.aspx?kioskID=" + itemID;
    var retVal = openDialoagWindow(url, 550, 610, window, "Para Üstü Giriş Ekranı");
    return retVal;
}

function showCutOfMonitoringWindow(cutID) {
    var url = "../Settings/ShowCutOfMonitoring.aspx?cutID=" + cutID;
    var retVal = openDialoagWindowCutOfMonitoring(url, 550, 400, window, "Kiosk Kesinti Ekranı");
    return retVal;
}

function showCommisionSetting(itemID) {
    var url = "../Settings/ShowCommisionSetting.aspx?instutionId=" + itemID;
    var retVal = openDialoagWindow(url, 500, 300, window, "Kurum Komisyon Detay Ekranı");
    return retVal;
}



function showRepairedCardWindow(itemId) {
    var url = "../Transaction/ShowRepairedCard.aspx?itemId=" + itemId;
    var retValue = openDialoagWindow(url, 350, 450, "", "Kart Onarım Düzenleme");
    return retVal;
}

function showEnterKioskEmptyCashCount(kioskId, insertDate, emptyKioskId) {
    var url = "../Account/ShowKioskEmptyCashCountByCode.aspx?kioskID=" + kioskId + "&insertDate=" + insertDate + "&emptyKioskId=" + emptyKioskId;
    var retVal = openDialoagWindow(url, 600, 600, window, "Para Bildirim Ekranı");
    return retVal;
}

function showEnterKioskEmptyDispenserCashCount(kioskId, amount, value) {
    var url = "../Kiosk/ShowKioskEmptyDispenserCashCountByCode.aspx?kioskID=" + kioskId + "&amount=" + amount + "&value=" + value;
    var retVal = openDialoagWindow(url, 600, 600, window, "Para Üstü Ekranı");
    return retVal;
}


function showEmptyKioskMutabakatClicked(kioskId, insertDate, emptyKioskId) {
    var url = "../Account/EmptyKioskMutabakat.aspx?kioskID=" + kioskId + "&insertDate=" + insertDate + "&emptyKioskId=" + emptyKioskId;
    var retVal = openDialoagWindow(url, 600, 730, window, "Kiosk Toplanan Para Mutabakat");
    return retVal;
}


function showKioskDeviceWindow(itemID, kioskName, deviceName, oldPortNumber) {
    var url = "../DevicePort/ShowKioskDevicePort.aspx?itemID=" + itemID + "&kioskName=" + kioskName + "&deviceName=" + deviceName + "&oldPortNumber=" + oldPortNumber;
    var retVal = openDialoagWindowKioskDevice(url, 510, 310, window, "Port Ayarı");
    return retVal;
}

function showCreateKioskCommandWindow(itemID) {
    var url = "../Kiosk/CreateKioskCommand.aspx?itemID=" + itemID;
    var retVal = openDialoagWindowCreateKioskCommand(url, 510, 510, window, "Komut Oluşturma");
    return retVal;
}

function showDeleteWindow(sqlQuery) {
    var url = "../webctrls/ShowDeleteWindow.aspx?sqlQuery=" + sqlQuery;
    var retVal = openDialoagWindow(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showDeleteWindowUser(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=User";
    var retVal = openDialoagWindowUser(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showDeleteWindowApproveUser(storedProcedure, itemId) {
    alert(12);  
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=User";
    var retVal = openDialoagWindowApproveUser(url, 400, 90, window, "Silme");
    return retVal;
}

function showApproveWindowApproveUser(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=User";
    var retVal = openDialoagWindowApproveUser(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showDeleteWindowKioskReferenceSystem(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=KioskReferenceSystem";
    var retVal = openDialoagWindowKioskReferenceSystem(url, 300, 90, window, "Referans Kaydı Silme");
    return retVal;
}

function showDeleteWindowKioskLocalLog(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=KioskLocalLog";
    var retVal = openDialoagWindowKioskLocalLog(url, 300, 90, window, "Kiosk Yerel Log Silme");
    return retVal;
}



function showApproveWindowAAA(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=Fraud";
    var retVal = openDialoagWindowAAA(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showDeleteWindowAAA(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=Fraud";
    var retVal = openDialoagWindowAAA(url, 400, 90, window, "Silme");
    return retVal;
}

function showApproveWindowAdminAAA(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=FraudAdmin&type=app";
    var retVal = openDialoagWindowAdminAAA(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showApproveWindowAdminDAA(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=FraudAdmin&type=del";
    var retVal = openDialoagWindowAdminAAA(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showDeleteWindowAdminAAA(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=FraudAdmin&type=de";
    var retVal = openDialoagWindowAdminAAA(url, 400, 90, window, "Silme");
    return retVal;
}

function showDeleteWindowAdminBLC(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=BLCAdmin";
    var retVal = openDialoagWindowAdminBLC(url, 400, 90, window, "Silme");
    return retVal;
}

function showDeleteWindowBLC(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=BLC";
    var retVal = openDialoagWindowBLC(url, 400, 90, window, "Silme");
    return retVal;
}

function showWindowAdminATBL(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=FraudAdmin&type=de";
    var retVal = openDialoagWindowAdminATBL(url, 400, 90, window, "Black List Ekleme");
    return retVal;
}

function showWindowAdminBLC(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=BLCAdmin";
    var retVal = openDialoagWindowAdminBLC(url, 400, 90, window, "Black List Ekleme");
    return retVal;
}

function showWindowBLC(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=BLC";
    var retVal = openDialoagWindowBLC(url, 400, 90, window, "Black List Ekleme");
    return retVal;
}

function showWindowATBL(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=Fraud";
    var retVal = openDialoagWindowATBL(url, 400, 90, window, "Black List Ekleme");
    return retVal;
}

function showApproveWindowAskiCode(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=AskiCode";
    var retVal = openDialoagWindowAskiCode(url, 400, 80, window, "Onaylama");
    return retVal;
}

function showDeleteWindowAskiCode(storedProcedure, itemId) {
    var url = "../webctrls/DeleteMoneyCode.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId;
    var retVal = openDialoagWindowAskiCode(url, 500, 200, window, "Onaylama");
    return retVal;
}

function showDeleteWindowBL(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=BlackList";
    var retVal = openDialoagWindowBL(url, 350, 100, window, "Silme");
    return retVal;
}

function showApproveWindowBL(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=BlackList";
    var retVal = openDialoagWindowBL(url, 350, 100, window, "Onaylama");
    return retVal;
}

function showDeleteWindowApproveRole(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=ApproveRole";
    var retVal = openDialoagWindowApproveRole(url, 400, 80, window, "Silme");
    return retVal;
}

function showApproveWindowApproveRole(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=ApproveRole";
    var retVal = openDialoagWindowApproveRole(url, 400, 80, window, "Onaylama");
    return retVal;
}
function showDeleteWindowRole(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=Role";
    var retVal = openDialoagWindowRole(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showDeleteWindowKiosk(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=Kiosk";
    var retVal = openDialoagWindowKiosk(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showDeleteWindowKioskDevice(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=KioskDevice";
    var retVal = openDialoagWindowKioskDevice(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showDeleteWindowKioskCommand(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=KioskCommand";
    var retVal = openDialoagWindowKioskCommand(url, 400, 90, window, "Onaylama");
    return retVal;
}


function showDeleteWindowRepairCard(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=RepairCard";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showMessageWindow(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText;
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}

function showAlertWindow(messageText) {
    var url = "../webctrls/AlertWindow.aspx?messageText=" + messageText;
    var retVal = openDialoagWindow(url, 400, 90, window, "Uyarı");
    return retVal;
}

function showAlertWindowManuel(messageText) {
    var url = "../webctrls/AlertWindow.aspx?messageText=" + messageText + "&whichPage=ManuelKioskEmpty" ;
    var retVal = openDialoagWindowKioskManuelEmpty(url, 400, 90, window, "Uyarı");
    return retVal;
}

function showAlertWindow2(messageText) {
    var url = "../webctrls/AlertWindow.aspx?messageText=" + messageText;
    var retVal = openDialoagWindow3(url, 500, 100, window, "Uyarı");
    return retVal;
}

function showMessageWindowUser(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_User";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}

function showMessageWindowRole(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_Role";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}

function showMessageWindowKiosk(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_Kiosk";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}

function showMessageWindowApproved(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_Approved";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Giriş Durumu");
    return retVal;
}


function showMessageWindowCashCount(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_CashCount";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}

function showMessageWindowCutOffMonitoring(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_CutOffMonitoring";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Kiosk Kesinti Mesaj Detayı");
    return retVal;
}

function showMessageWindowKioskErrorLogs(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_KioskErrorLogs";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Kiosk Aygıt Hataları Mesaj Detayı");
    return retVal;
}

function showMessageWindowRepairedCard(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_RepairCard";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Kiosk Kart Onarım Düzenleme Mesaj Detayı");
    return retVal;
}
 


function showMessageWindowKioskDevice(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_KioskDevice";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}

function showMessageWindowKioskCommand(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_KioskCommand";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}

function showMessageWindowDefault(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_Default";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}

function showMessageWindowApprovedCollectedMoney(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_ListApprovedCollectedMoney";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}

function showMessageWindowKioskDispenserCashCount(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_ListKioskDispenserCashCount";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}

//showMessageWindowCardRepair hideModalPopupLastandPrevious modalPopup_ListRepairCard
function showMessageWindowCardRepair(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_ListRepairCard";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}

//showMessageWindowCutOffMonitoring
function showMessageWindowCommisionSetting(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_ListSetting";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}

function showMessageWindowSendMoneyCode(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_ListAskiCodes";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}

function showMessageWindowKioskReferenceSystem(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_ListKioskReferenceSystem";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Kiosk Referans Sistemi Mesaj Detayı");
    return retVal;
}

function showMessageWindowKioskLocalLog(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_ListKioskLocalLog";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Kiosk Yerel Log Giriş Ekran Detayı");
    return retVal;
}