﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Other;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Transaction_ListFailedTransactions : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;

    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Transaction/ListFailedTransactions.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.startDateField.Text = DateTime.Now.AddDays(-0).ToString("yyyy-MM-dd") + " 00:00:00";
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00";

            this.SearchOrder(4, 1);
        }
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            //ViewState["NumberOfItem"] = this.numberOfItemField.Text;
            int recordCount = 0;
            int pageCount = 0;
            int custId = 0;

            if (Convert.ToInt32(Request.QueryString["customerId"]) != 0)
            {
                custId = Convert.ToInt32(Request.QueryString["customerId"]);
            }

            CallWebServices callWebServ = new CallWebServices();
            ParserListFailedTransactions failedtransactions = callWebServ.CallListFailedTransactionService(this.searchTextField.Text.Trim(),
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             custId,
                                                                             kioskIds,
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             Convert.ToInt32(this.operationTypeBox.SelectedValue),
                                                                             this.TransactionStatusBox.SelectedValue,
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             1);


            if (failedtransactions != null)
            {
                if (failedtransactions.errorCode == 0)
                {
                    this.BindListTable(failedtransactions);
                }
                else if (failedtransactions.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + failedtransactions.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListFailedTransactionSrch");
        }
    }

    private void BindListTable(ParserListFailedTransactions failedtransactions)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);


            TableRow[] Row = new TableRow[failedtransactions.Transactions.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("transactionId", failedtransactions.Transactions[i].TransactionId.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell customerNameCell = new TableCell();
                TableCell aboneNoCell = new TableCell();
                TableCell transactionStatusCell = new TableCell();
                TableCell transactionDateCell = new TableCell();
                TableCell detailsCell = new TableCell();

                TableCell operationTypeCell = new TableCell();
                TableCell instutionNameCell = new TableCell();
                TableCell fakeCell00 = new TableCell();
                TableCell fakeCell01 = new TableCell();
                fakeCell00.CssClass = "inputTitleCell4";
                fakeCell00.Visible = false;
                instutionNameCell.CssClass = "inputTitleCell4";

                fakeCell01.CssClass = "inputTitleCell4";
                fakeCell01.Visible = false;
                operationTypeCell.CssClass = "inputTitleCell4";

                TableCell fakeCell1 = new TableCell();
                TableCell fakeCell2 = new TableCell();
                TableCell fakeCell3 = new TableCell();
                TableCell fakeCell4 = new TableCell();
                TableCell fakeCell5 = new TableCell();
                TableCell fakeCell6 = new TableCell();
                TableCell fakeCell7 = new TableCell();

                fakeCell1.CssClass = "inputTitleCell4";
                fakeCell2.CssClass = "inputTitleCell4";
                fakeCell3.CssClass = "inputTitleCell4";
                fakeCell4.CssClass = "inputTitleCell4";
                fakeCell5.CssClass = "inputTitleCell4";
                fakeCell6.CssClass = "inputTitleCell4";
                fakeCell7.CssClass = "inputTitleCell4";

                fakeCell1.Visible = false;
                fakeCell2.Visible = false;
                fakeCell3.Visible = false;
                fakeCell4.Visible = false;
                fakeCell5.Visible = false;
                fakeCell6.Visible = false;
                fakeCell7.Visible = false;

                indexCell.CssClass = "inputTitleCell4";
                idCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                customerNameCell.CssClass = "inputTitleCell4";
                aboneNoCell.CssClass = "inputTitleCell4";
                transactionDateCell.CssClass = "inputTitleCell4";
                transactionStatusCell.CssClass = "inputTitleCell4";
                detailsCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                idCell.Text = "<a href=\"javascript:void(0);\" onclick=\"showTransactionDetailClicked(" + failedtransactions.Transactions[i].TransactionId + ");\" class=\"anylink\">" + transactions.Transactions[i].TransactionId.ToString() + "</a>";
                kioskNameCell.Text = failedtransactions.Transactions[i].KioskName;
                customerNameCell.Text = failedtransactions.Transactions[i].CustomerName;
                aboneNoCell.Text = failedtransactions.Transactions[i].AboneNo;
                transactionDateCell.Text = failedtransactions.Transactions[i].TransactionDate.ToString();
                transactionStatusCell.Text = failedtransactions.Transactions[i].ProcessStatus;
                instutionNameCell.Text = failedtransactions.Transactions[i].InstutionName;
                operationTypeCell.Text = failedtransactions.Transactions[i].OperationTypeName;

                 


                 

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        fakeCell1,
                        fakeCell2,
                        fakeCell3,
                        fakeCell4,
                        fakeCell5,
                        fakeCell6,
                        fakeCell7,
                        fakeCell01,
                        idCell,
						kioskNameCell,
                        customerNameCell,
                        aboneNoCell,
                        transactionDateCell,
                        transactionStatusCell,
                        operationTypeCell,
                        instutionNameCell,
                         
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (failedtransactions.recordCount < currentRecordEnd)
                currentRecordEnd = failedtransactions.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + transactions.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(failedtransactions.pageCount, this.pageNum, failedtransactions.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionBDT");
        }
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "TransactionId":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "KioskName":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CustomerName":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        CustomerName.Text = "<a>Müşteri Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        CustomerName.Text = "<a>Müşteri Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    CustomerName.Text = "<a>Müşteri Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CardReaderType":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "AboneNo":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        TransactionStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        TransactionStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    TransactionStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "TransactionDate":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        Instution.Text = "<a>Kurum    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        Instution.Text = "<a>Kurum    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    Instution.Text = "<a>Kurum    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "TransactionStatus":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        AboneNo.Text = "<a>Abone No    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        AboneNo.Text = "<a>Abone No    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    AboneNo.Text = "<a>Abone No    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "OperationType":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    OperationType.Text = "<a>İşlem Tipi   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Instution":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    OperationType.Text = "<a>İşlem Tipi   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "LogTime":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    OperationType.Text = "<a>İşlem Tipi   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Cellphone":
                if (orderSelectionColumn == 11)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    OperationType.Text = "<a>İşlem Tipi   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 11;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Status":
                if (orderSelectionColumn == 12)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    OperationType.Text = "<a>İşlem Tipi   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 12;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "CallDate":
                if (orderSelectionColumn == 13)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    OperationType.Text = "<a>İşlem Tipi   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 13;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "CallCount":
                if (orderSelectionColumn == 14)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    OperationType.Text = "<a>İşlem Tipi   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 14;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Explanation":
                if (orderSelectionColumn == 14)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    OperationType.Text = "<a>İşlem Tipi   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 14;
                    orderSelectionDescAsc = 1;
                }
                break;
            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(4, 1);
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        //this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        //this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        //ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        //this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }
}