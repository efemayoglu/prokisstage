﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Account;
using PRCNCORE.Parser.Kiosk;
using PRCNCORE.Parser.Other;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class Transaction_ListKioskLocalLog : System.Web.UI.Page
{

    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;

    private int orderSelectionColumn = 2;
    private int orderSelectionDescAsc = 1;

    private int ownSubMenuIndex = -1;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Transaction/ListKioskLocalLog.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);
        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {

            CallWebServices callWebServ = new CallWebServices();


            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {

                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }


            ParserListIsCardDeleted isCardDeleted = callWebServ.CallGetIsCardDeletedService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (isCardDeleted != null)
            {

                this.logStatusBox.DataSource = isCardDeleted.IsCardDeleted;
                this.logStatusBox.DataBind();

            }

            ListItem item6 = new ListItem();
            item6.Text = " YerelLog Durumu Seçiniz";
            item6.Value = "0";
            this.logStatusBox.Items.Add(item6);
            this.logStatusBox.SelectedValue = "0";

            //ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            //if (instutions != null)
            //{
            //    this.instutionBox.DataSource = instutions.InstutionNames;
            //    this.instutionBox.DataBind();
            //}


            //ListItem item = new ListItem();
            //item.Text = "Kurum Seçiniz";
            //item.Value = "0";
            //this.instutionBox.Items.Add(item);
            //this.instutionBox.SelectedValue = "0";

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.startDateField.Text = DateTime.Now.AddDays(-3).ToString("yyyy-MM-dd") + " 00:00:00";
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00";

            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;
            this.pageNum = 0;
            this.SearchOrder(4, 1);


        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }

    }


    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(4, 1);
    }


    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskId = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskId = "0";
            else
                kioskId = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebService = new CallWebServices();
            ParserListKioskLocalLog items = callWebService.CallListKioskLocalLog(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text).AddDays(0).AddSeconds(-1),
                                                                             Convert.ToDateTime(this.endDateField.Text).AddDays(0).AddSeconds(-1),
                                                                             Convert.ToInt32(logStatusBox.SelectedValue),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             kioskId,
                //  Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             1);

            if (items != null)
            {
                if (items.errorCode == 0 || items.errorCode == 2)
                {
                    this.BindListTable(items);
                }
                else if (items.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + items.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskLocalLog");
        }
    }

    private void BindListTable(ParserListKioskLocalLog items)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            if (items.KioskLocalLog != null)
            {


                TableRow[] Row = new TableRow[items.KioskLocalLog.Count];
                int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
                for (int i = 0; i < Row.Length; i++)
                {
                    Row[i] = new TableRow();
                    // Row[i].Attributes.Add("Id", items.kioskReferenceSystem[i].kioskId.ToString());

                    TableCell indexCell = new TableCell();
                    TableCell transactionIdCell = new TableCell();
                    TableCell kioskIdCell = new TableCell();
                    
                    TableCell insertionDate = new TableCell();
                    TableCell startDateCell = new TableCell();
                    TableCell endDateCell = new TableCell();
                    TableCell userNameCell = new TableCell();
                    TableCell statusCell = new TableCell();
                    TableCell logStatusCell = new TableCell();
                    
                    TableCell detailsCell = new TableCell();

                    indexCell.CssClass = "inputTitleCell4";
                    kioskIdCell.CssClass = "inputTitleCell4";
                    transactionIdCell.CssClass = "inputTitleCell4";
                    insertionDate.CssClass = "inputTitleCell4";
                    startDateCell.CssClass = "inputTitleCell4";
                    
                    endDateCell.CssClass = "inputTitleCell4";
                    userNameCell.CssClass = "inputTitleCell4";
                    statusCell.CssClass = "inputTitleCell4";
                    logStatusCell.CssClass = "inputTitleCell4";
                    detailsCell.CssClass = "inputTitleCell4";

                    indexCell.Text = (startIndex + i).ToString();

                    //if (items.kioskReferenceSystem[i].statusId == 1)
                    //{
                    //    //kioskIdCell.Text = items.ControlCashNotifications[i].kioskId.ToString();
                    //    kioskIdCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editKioskReferenceSystemButtonClicked(" + items.ControlCashNotifications[i].controlCashId + ");\" class=\"anylink\">" + items.ControlCashNotifications[i].kioskId.ToString() + "</a>";
                    //}

                    //if (items.kioskReferenceSystem[i].statusId == 2)
                    //{
                    //    //kioskIdCell.Text = items.ControlCashNotifications[i].kioskId.ToString();
                    //    kioskIdCell.Text = items.ControlCashNotifications[i].kioskId.ToString();
                    //}


                    kioskIdCell.Text = items.KioskLocalLog[i].KioskName;
                    transactionIdCell.Text = "<a href=\"javascript:void(0);\" onclick=\"showTransactionDetailClicked(" + items.KioskLocalLog[i].TransactionId + ");\" class=\"anylink\">" + items.KioskLocalLog[i].TransactionId.ToString() + "</a>";
                     
                    insertionDate.Text = items.KioskLocalLog[i].InsertionDate;
                    startDateCell.Text = items.KioskLocalLog[i].Name;
                    endDateCell.Text = items.KioskLocalLog[i].CustomerNo;
                    userNameCell.Text = items.KioskLocalLog[i].UserName;
                    statusCell.Text = items.KioskLocalLog[i].Status;
                    logStatusCell.Text = items.KioskLocalLog[i].LogStatusText;
                    statusCell.Text = items.KioskLocalLog[i].Status;


                    detailsCell.Width = Unit.Pixel(85);

                    //if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanDelete == "1")
                    //{
                    //    detailsCell.Text = "<a href=# onClick=\"deleteButtonClicked('[pk].[delete_kiosk_reference_system]'," + items.kioskReferenceSystem[i].Id + " );\"><img src=../images/delete.png border=0 title=\"Delete\" /></a>";
                    //    // "<a href=# onClick=\"deleteButtonClicked('[pk].[delete_user]'," + users.Users[i].Id + ");\"><img src=../images/delete.png border=0 title=\"Delete\" /></a>";
                    //}
                    //else
                    //{
                    //    detailsCell.Text = " ";
                    //}


                    if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1" && items.KioskLocalLog[i].LogStatus == "2")
                    {
                        detailsCell.Text = "<a href=# onClick=\"showKioskLocalLogDetailsClicked(" + items.KioskLocalLog[i].Id + ");\" class=\"detailCodeButton\" Title=\"Para Girişlerini Göster\"></a>";
                       
                    }

                    if (items.KioskLocalLog[i].LogStatus == "3")
                    {
                        //detailsCell.Text = "<a href=# onClick=\"showKioskLocalLogDetailsClicked(" + items.KioskLocalLog[i].Id + ");\" class=\"notFoundButton\" Title=\"Kayıt Bulunamadı.\"></a>";
                        detailsCell.Text = "<a href=# class=\"generateCancelButton\" Title=\"Kayıt Bulunamadı\"></a>";
                    }

                    if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanDelete == "1")
                    {
                        detailsCell.Text = detailsCell.Text + "<a href=\"javascript: deleteButtonClicked('[pk].[delete_kiosk_local_log]'," + items.KioskLocalLog[i].Id + ");\" class=\"deleteButton\" title=\"Kiosk Yerel Log Sil\"></a>";
                    }


                    /*  detailsCell.Text = "<a href=# onClick=\"cancelApprovedTxn(" + transactions.TransactionConditions[i].TransactionId + ");\" class=\"generateCancelButton\" Title=\"İşlemi İptal Et\"></a>"+
                        "<a href=# onClick=\"showCompleteStatusDetailWindow(" + transactions.TransactionConditions[i].TransactionId + ");\" class=\"generateChangeCompleteStatus\" Title=\"İşlem Durumu\"></a>";*/


                    Row[i].Cells.AddRange(new TableCell[]{
                    indexCell,
                    transactionIdCell,
                    kioskIdCell,
                    insertionDate,
                    startDateCell,
                    endDateCell, 
                    userNameCell,
                    statusCell,
                    logStatusCell,
                    statusCell,
                    detailsCell
                });


                    //if (i == Row.Length - 1)
                    //{
                    //    Row[i].CssClass = "inputTitleCell99";
                    //    Row[i].Cells[1].Text = "TOPLAM";
                    //    Row[i].Cells[0].Text = "";
                    //}
                    //else
                    //{
                    if (i % 2 == 0)
                        Row[i].CssClass = "listrow";
                    else
                        Row[i].CssClass = "listRowAlternate";
                    //}
                }


                this.itemsTable.Rows.AddRange(Row);

                TableRow pagingRow = new TableRow();
                TableCell pagingCell = new TableCell();

                int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
                int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

                if (items.recordCount < currentRecordEnd)
                    currentRecordEnd = items.recordCount;

                if (currentRecordEnd > 0)
                {
                    this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + items.recordCount.ToString() + " kayıt bulundu.";
                }

                //items.pageCount = 10;

                pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
                pagingCell.HorizontalAlign = HorizontalAlign.Right;

                pagingCell.Text = WebUtilities.GetPagingText(items.pageCount, this.pageNum, items.recordCount);
                pagingRow.Cells.Add(pagingCell);
                this.itemsTable.Rows.AddAt(0, pagingRow);

                this.itemsTable.Visible = true;


                //if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
                //{
                TableRow addNewRow = new TableRow();

                TableCell addNewCell = new TableCell();

                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
                {
                    addNewCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editKioskLocalLogButtonClicked();\" class=\"anylink\"><img src=../images/add.png border=0 title=\"Add New...\" /></a>";
                    //addNewCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editCutOfMonitoringButtonClicked('0');\" class=\"anylink\"><img src=../images/add.png border=0 title=\"Add New...\" /></a>";
                }

                //else
                //{
                //    addNewCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editKioskReferenceSystemButtonClicked('0');\" class=\"anylink\"><img src=../images/add.png border=0 title=\"Add New...\" /></a>";
                //    //kioskIdCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editKioskDispenserCashCountButtonClicked(" + items.ParserKioskCashDispenser[i].kioskID + "," + items.ParserKioskCashDispenser[i].Id + ");\" class=\"anylink\">" + items.ParserKioskCashDispenser[i].kioskId + "</a>";

                //}

                //addNewRow.Cells.Add(addNewCell);
                this.itemsTable.Rows.Add(addNewRow);

                TableRow addNewRow2 = new TableRow();

                TableCell spaceCell3 = new TableCell();

                spaceCell3.ColumnSpan = (9);


                addNewRow2.Cells.Add(spaceCell3);
                addNewRow2.Cells.Add(addNewCell);

                this.itemsTable.Rows.Add(addNewRow2);
                //}




            }
            else
            {
                //if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
                //{
                TableRow addNewRow = new TableRow();
                TableCell addNewCell = new TableCell();
                TableCell spaceCell = new TableCell();
                spaceCell.CssClass = "inputTitleCell4";
                spaceCell.ColumnSpan = 9;

                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
                {
                    addNewCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editKioskLocalLogButtonClicked('0');\" class=\"anylink\"><img src=../images/add.png border=0 title=\"Add New...\" /></a>";
                }

                addNewRow.Cells.Add(spaceCell);
                addNewRow.Cells.Add(addNewCell);
                this.itemsTable.Rows.Add(addNewRow);
                //}
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListMutabakatBDT");
        }
    }


    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {

            string kioskId = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskId = "0";
            else
                kioskId = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            int recordCount = 0;
            int pageCount = 0;

 
            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskLocalLog items = callWebServ.CallListKioskLocalLog(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text).AddDays(0).AddSeconds(-1),
                                                                             Convert.ToDateTime(this.endDateField.Text).AddDays(0).AddSeconds(-1),
                                                                              Convert.ToInt32(logStatusBox.SelectedValue),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             kioskId,
                //  Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             2);

            if (items != null)
            {
                if (items.errorCode == 0)
                {
                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    //exportExcell.ExportExcellByBlock(lists.Transactions,"İşlemler", null);

                    string[] headerNames = {  "İşlem No", "Kiosk Adı", "Empty", "Başlangıç Tarih","Bitiş Tarihi", "Giriş Zamanı","Kullanıcı Adı",
                                               "Log Durumu",  "Durum"};

                    exportExcell.ExportExcellByBlock(items.KioskLocalLog, "Kiosk Yerel Log Raporu", headerNames);
                }
                else if (items.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + items.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelTransaction");
        }
    }



    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        ClickableWebControl.ClickableTableHeaderCell Header = ((ClickableWebControl.ClickableTableHeaderCell)sender);

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "TransactionId":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Header.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";

                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  Header.Text = "<a>Kiosk Adı <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  Header.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "KioskName":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //InstitutionId.Text = "<a>Kurum Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //InstitutionId.Text = "<a>Kurum Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // InstitutionId.Text = "<a>Kurum Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "InsertionDate":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // InsertionDate.Text = "<a>İşlem Tarihi <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //InsertionDate.Text = "<a>İşlem Tarihi <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  InsertionDate.Text = "<a>İşlem Tarihi     <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "StartDate":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // FirstDenomValue.Text = "<a>V1    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  FirstDenomValue.Text = "<a>V1    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //FirstDenomValue.Text = "<a>V1    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "EndDate":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // FirstDenomNumber.Text = "<a>#1 <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //FirstDenomNumber.Text = "<a>#1   <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // FirstDenomNumber.Text = "<a>#1    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "UserName":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SecondDenomValue.Text = "<a>V2    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // SecondDenomValue.Text = "<a>V2    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  SecondDenomValue.Text = "<a>V2    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "LogStatus":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SecondDenomNumber.Text = "<a>#2    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // SecondDenomNumber.Text = "<a>#2    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // SecondDenomNumber.Text = "<a>#2     <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "Status":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SecondDenomNumber.Text = "<a>#2    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // SecondDenomNumber.Text = "<a>#2    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // SecondDenomNumber.Text = "<a>#2     <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;

            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

}