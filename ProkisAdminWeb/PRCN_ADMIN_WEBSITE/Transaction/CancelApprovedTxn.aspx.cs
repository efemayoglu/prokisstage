﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Transaction_CancelApprovedTxn : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private int transactionId = 0;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];
        this.transactionId = Convert.ToInt32(Request.QueryString["itemID"]);
        this.hiddenUserId.Value = Request.QueryString["itemID"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Transaction/ListTransactionCondition.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit != "1")
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }

        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }

    private void BindCtrls()
    {
        try
        {
            CallWebServices callWebServ = new CallWebServices();
            CancelTransactionResult getSaleParams = callWebServ.CancelTransaction(transactionId.ToString(), this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            
            if (getSaleParams!=null && getSaleParams.CancelTransaction.Count>0)
            {  
                billDateTextBox.Text = getSaleParams.CancelTransaction[0].MakbuzTarihi.ToString();
                billNoTextBox.Text=getSaleParams.CancelTransaction[0].MakbuzNo.ToString();
                CustomerNoTextBox.Text = getSaleParams.CancelTransaction[0].AboneNo.ToString();
            }
             
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowUserBC");
        }
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        CancelTransaction();
    }

    private void CancelTransaction()
    {
     
       try
       {
           
           ParserCancelTnx operationResult = null;

           if (this.transactionId != 0)
           {
               //string phoneNumber = ClearPhoneNumberText(this.cellphoneBox.Text);
               CallWebServices callWebServ = new CallWebServices();
               operationResult = callWebServ.CallCancelTransactionService(this.transactionId
                   , this.LoginDatas.User.Id
                   , this.LoginDatas.AccessToken
                   , this.LoginDatas.User.Id
                   , CustomerNoTextBox.Text
                   , billDateTextBox.Text
                   , billNoTextBox.Text);

               string serviceResult="";

               if (operationResult != null)
               {
                   if (operationResult.errorCode != 0)
                   {
                       serviceResult = "Hata Kodu:" + operationResult.errorCode;
                   }
                   else 
                   {
                       if (operationResult.cancelResult != null)
                       {
                           serviceResult = operationResult.cancelResult.ToString();
                           if (serviceResult == "1")
                           {
                               this.messageArea.InnerHtml = "İşlem Başarılı.";
                           }
                           else
                           {
                               this.messageArea.InnerHtml = "Merkezde kayıt bulunamadı.";
                           }
                       }
                       else
                           serviceResult = "Hata:null";
                   }

                   ParserCancelTnxLog logResult = callWebServ.UpdateTransactionCancel(this.transactionId
                          , this.LoginDatas.User.Id
                          , this.LoginDatas.AccessToken
                          , this.LoginDatas.User.Id
                          , CustomerNoTextBox.Text
                          , billDateTextBox.Text
                          , billNoTextBox.Text
                          , serviceResult);
               }
               else
               {
                   this.messageArea.InnerHtml = "Servisten cevap alınamadı.";
               }               
           }
           else
           {
               this.messageArea.InnerHtml = "İlgili işlem alınamadı !";
           }

           if (operationResult != null)
           {
               
               if (operationResult.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
               {
                   Session.Abandon();
                   Session.RemoveAll();
                   Response.Redirect("../root/Login.aspx", true);
               }
               else
               {
                   this.saveButton.Attributes.Add("disabled", "disabled");
               }
           }
       }
       catch (Exception exp)
       {
           Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowUserSv");
       }
          
    }
}