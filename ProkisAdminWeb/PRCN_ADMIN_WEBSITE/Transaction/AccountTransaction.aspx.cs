﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Transaction_AccountTransaction : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Transaction/AccountTransaction.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.startDateField.Text = DateTime.Now.AddDays(-10).ToString("yyyy-MM-dd HH:mm:ss");
            this.endDateField.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;
            this.pageNum = 0;
            this.SearchOrder(4, 1);
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(4, 1);
    }

    private void BindListTable(ParserListAccountTransactions transactions)
    {
        try
        {
            TableRow[] Row = new TableRow[transactions.Transactions.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("id", transactions.Transactions[i].Id.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell transactionDateCell = new TableCell();
                TableCell descriptionCell = new TableCell();
                TableCell transactionTypeCell = new TableCell();
                TableCell amountCell = new TableCell();
                TableCell senderNoCell = new TableCell();
                TableCell senderBalanceCell = new TableCell();
                TableCell recieverNoCell = new TableCell();
                TableCell recieverBalanceCell = new TableCell();
                TableCell transactionPriceCell = new TableCell();
                TableCell transactionPlatformCell = new TableCell();

                TableCell fakeCell1 = new TableCell();
                TableCell fakeCell2 = new TableCell();
                TableCell fakeCell3 = new TableCell();
                TableCell fakeCell4 = new TableCell();
                TableCell fakeCell5 = new TableCell();
                TableCell fakeCell6 = new TableCell();
                TableCell fakeCell7 = new TableCell();
                TableCell fakeCell8 = new TableCell();
                TableCell fakeCell9 = new TableCell();
                TableCell fakeCell10 = new TableCell();
                TableCell fakeCell11 = new TableCell();

                fakeCell1.CssClass = "inputTitleCell4";
                fakeCell2.CssClass = "inputTitleCell4";
                fakeCell3.CssClass = "inputTitleCell4";
                fakeCell4.CssClass = "inputTitleCell4";
                fakeCell5.CssClass = "inputTitleCell4";
                fakeCell6.CssClass = "inputTitleCell4";
                fakeCell7.CssClass = "inputTitleCell4";
                fakeCell8.CssClass = "inputTitleCell4";
                fakeCell9.CssClass = "inputTitleCell4";
                fakeCell10.CssClass = "inputTitleCell4";
                fakeCell11.CssClass = "inputTitleCell4";

                fakeCell1.Visible = false;
                fakeCell2.Visible = false;
                fakeCell3.Visible = false;
                fakeCell4.Visible = false;
                fakeCell5.Visible = false;
                fakeCell6.Visible = false;
                fakeCell7.Visible = false;
                fakeCell8.Visible = false;
                fakeCell9.Visible = false;
                fakeCell10.Visible = false;
                fakeCell11.Visible = false;

                indexCell.CssClass = "inputTitleCell4";
                idCell.CssClass = "inputTitleCell4";
                transactionDateCell.CssClass = "inputTitleCell4";
                descriptionCell.CssClass = "inputTitleCell4";
                transactionTypeCell.CssClass = "inputTitleCell4";
                amountCell.CssClass = "inputTitleCell4";
                senderNoCell.CssClass = "inputTitleCell4";
                senderBalanceCell.CssClass = "inputTitleCell4";
                recieverNoCell.CssClass = "inputTitleCell4";
                recieverBalanceCell.CssClass = "inputTitleCell4";
                transactionPriceCell.CssClass = "inputTitleCell4";
                transactionPlatformCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();

                idCell.Text =  transactions.Transactions[i].Id.ToString();
                transactionDateCell.Text =  transactions.Transactions[i].TransactionDate;
                descriptionCell.Text =  transactions.Transactions[i].Description;
                transactionTypeCell.Text =  transactions.Transactions[i].TransactionType;
                amountCell.Text =  transactions.Transactions[i].Amount;
                senderNoCell.Text =  transactions.Transactions[i].SenderNo;
                senderBalanceCell.Text =  transactions.Transactions[i].SenderBalance;
                recieverNoCell.Text =  transactions.Transactions[i].RecieverNo;
                recieverBalanceCell.Text =  transactions.Transactions[i].RecieverBalance;
                transactionPriceCell.Text =  transactions.Transactions[i].TransactionPrice;
                if (transactions.Transactions[i].TransactionPlatform == "1")
                {
                    transactionPlatformCell.Text = "Kiosk";
                }
                else
                    transactionPlatformCell.Text = "Web";

                //transactionPlatformCell.Text = transactions.Transactions[i].TransactionPlatform;

                idCell.Visible = false;

                Row[i].Cells.AddRange(new TableCell[]{
						                             indexCell,
                                                     fakeCell1,
                                                     fakeCell2,
                                                     fakeCell3,
                                                     fakeCell4,
                                                     fakeCell5,
                                                     idCell,
                                                     indexCell,
                                                     transactionDateCell,
                                                     descriptionCell,
                                                     transactionTypeCell,
                                                     amountCell,
                                                     senderNoCell,
                                                     senderBalanceCell,
                                                     recieverNoCell,
                                                     //recieverBalanceCell,
                                                     transactionPriceCell,
                                                     transactionPlatformCell
					                                });

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (transactions.recordCount < currentRecordEnd)
                currentRecordEnd = transactions.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + transactions.recordCount.ToString() + " kayıt bulundu.";
            }


            TableRow addNewRow = new TableRow();
            TableCell totalEarnedMoneyTextCell = new TableCell();
            TableCell totalEarnedMoneyCell = new TableCell();
            TableCell totalTransferMoneyTextCell = new TableCell();
            TableCell totalTransferMoneyCell = new TableCell();
            TableCell spaceCell = new TableCell();
            TableCell spaceCell1 = new TableCell();
            TableCell spaceCell2 = new TableCell();

            spaceCell.CssClass = "inputTitleCell99";
            spaceCell.ColumnSpan = (3);
            totalEarnedMoneyTextCell.CssClass = "inputTitleCell99";
            totalEarnedMoneyCell.CssClass = "inputTitleCell99";
            totalTransferMoneyTextCell.CssClass = "inputTitleCell99";
            totalTransferMoneyCell.CssClass = "inputTitleCell99";

            spaceCell1.CssClass = "inputTitleCell99";
            spaceCell1.ColumnSpan = (2);

            spaceCell2.CssClass = "inputTitleCell99";
            spaceCell2.ColumnSpan = (1);

            spaceCell.Text = " ";
            spaceCell1.Text = " ";
            spaceCell2.Text = " ";

            totalTransferMoneyTextCell.Text = "Toplam Transfer:";
            totalTransferMoneyCell.Text = transactions.totalTransferAmount;
            totalEarnedMoneyTextCell.Text = "Toplam Kazanç:";
            totalEarnedMoneyCell.Text = transactions.totalEarnedAmount;

            addNewRow.Cells.Add(spaceCell);
            addNewRow.Cells.Add(totalTransferMoneyTextCell);
            addNewRow.Cells.Add(totalTransferMoneyCell);
            addNewRow.Cells.Add(spaceCell1);
            addNewRow.Cells.Add(totalEarnedMoneyTextCell);
            addNewRow.Cells.Add(totalEarnedMoneyCell);
            addNewRow.Cells.Add(spaceCell2);
            this.itemsTable.Rows.Add(addNewRow);


            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(transactions.pageCount, this.pageNum, transactions.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "TransactionDate":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Description":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        Description.Text = "<a>Açıklama    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        Description.Text = "<a>Açıklama    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    Description.Text = "<a>Açıklama    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "TransactionType":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        TransactionType.Text = "<a>İşlem Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        TransactionType.Text = "<a>İşlem Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    TransactionType.Text = "<a>İşlem Tipi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Amount":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        Amount.Text = "<a>Miktar    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        Amount.Text = "<a>Miktar    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    Amount.Text = "<a>Miktar    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "SenderNo":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        SenderNo.Text = "<a>G. Müşteri No    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        SenderNo.Text = "<a>G. Müşteri No    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    SenderNo.Text = "<a>G. Müşteri No    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "SenderBalance":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        SenderBalance.Text = "<a>G. Bakiye    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        SenderBalance.Text = "<a>G. Bakiye    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    SenderBalance.Text = "<a>G. Bakiye    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "RecieverNo":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        RecieverNo.Text = "<a>A. Müşteri No    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        RecieverNo.Text = "<a>A. Müşteri No    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    RecieverNo.Text = "<a>A. Müşteri No    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;
            //case "RecieverBalance":
            //    if (orderSelectionColumn == 8)
            //    {
            //        if (orderSelectionDescAsc == 0)
            //        {
            //            RecieverBalance.Text = "<a>A. Bakiye    <img src=../images/arrow_up.png border=0/></a>";
            //            orderSelectionDescAsc = 1;
            //        }
            //        else
            //        {
            //            RecieverBalance.Text = "<a>A. Bakiye    <img src=../images/arrow_down.png border=0/></a>";
            //            orderSelectionDescAsc = 0;
            //        }
            //    }
            //    else
            //    {
            //        RecieverBalance.Text = "<a>A. Bakiye    <img src=../images/arrow_up.png border=0/></a>";
            //        orderSelectionColumn = 8;
            //        orderSelectionDescAsc = 1;
            //    }
            //    break;

            case "TransactionPrice":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        TransactionPrice.Text = "<a>İşlem Ücreti    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        TransactionPrice.Text = "<a>İşlem Ücreti    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    TransactionPrice.Text = "<a>İşlem Ücreti    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "TransactionPlatform":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        TransactionPlatform.Text = "<a>İşlem Yeri    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        TransactionPlatform.Text = "<a>İşlem Yeri    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    TransactionPlatform.Text = "<a>İşlem Yeri    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;
            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {

            //ViewState["NumberOfItem"] = this.numberOfItemField.Text;
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListAccountTransactions transactions = callWebServ.CallListAccountTransactionService(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             this.kioskBox.SelectedValue,
                                                                             0, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (transactions != null)
            {
                if (transactions.errorCode == 0)
                {
                    this.BindListTable(transactions);
                }
                else if (transactions.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + transactions.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionSrch");
        }
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    private void ExportToExcel()
    {
        try
        {

            int custId = 0;

            if (Convert.ToInt32(Request.QueryString["customerId"]) != 0)
            {
                custId = Convert.ToInt32(Request.QueryString["customerId"]);
            }

            CallWebServices callWebServ = new CallWebServices();
            ParserListAccountTransactions lists = callWebServ.CallListAccountTransactionForExcelService(this.searchTextField.Text,
                                                                            Convert.ToDateTime(this.startDateField.Text),
                                                                            Convert.ToDateTime(this.endDateField.Text),
                                                                            Convert.ToInt16(ViewState["OrderColumn"]),
                                                                            Convert.ToInt16(ViewState["OrderDesc"]),
                                                                            this.kioskBox.SelectedValue,
                                                                            0, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (lists != null)
            {
                if (lists.errorCode == 0)
                {

                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    exportExcell.ExportExcell(lists.Transactions, lists.recordCount, "İşlemler Listesi");
                }
                else if (lists.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + lists.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelTransaction");
        }
    }
}