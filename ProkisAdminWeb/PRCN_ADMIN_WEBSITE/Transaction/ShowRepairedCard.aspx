﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowRepairedCard.aspx.cs" Inherits="Transaction_ShowRepairedCard" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />
    <input type="hidden" id="hiddenUserId" runat="server" name="hiddenUserId" value="0" />
</head>

<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <div align="center">

           <asp:Panel ID="panel7" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <div align="left" id="articleDetailsTab" class="windowTitle_container_autox30">
                    Kiosk Kart Onarım İşlemi<hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>
                <table border="0" cellpadding="2" cellspacing="0" id="Table1" runat="server">

                     <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20_2">İşlem No:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="transactionIdField" MaxLength="100" CssClass="inputTitleCell9" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>

                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20_2">Kiosk Adı:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="kioskNameField" MaxLength="100" CssClass="inputTitleCell9" runat="server" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>

                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20_2">Müşteri Adı:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="customerNameField" runat="server" CssClass="inputTitleCell9" onkeypress='validateIP(event)' Enabled="false"></asp:TextBox>
                        </td>

                    </tr>

                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20_2">İşlem Zamanı:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="transactionDateField" runat="server" CssClass="inputTitleCell9" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" class="staticTextLine_200x20_2">Ana Kredi:
                        </td>
                        <td align="left" class="auto-style1">
                             <asp:TextBox TextMode="Number"   ID="anaKredi" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9"  min="0" max="1000000" step="1" value="0"/>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" class="staticTextLine_200x20_2">Yedek Kredi:
                        </td>
                        <td align="left" class="auto-style1">
                             <asp:TextBox TextMode="Number"   ID="yedekKredi" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9"  min="0" max="1000000" step="1" value="0"/>
                        </td>
                    </tr>

 
                </table>
                <br />
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Kaydet"></asp:Button>

                                <asp:Button ID="cancelButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClientClick="parent.hideModalPopup2();" Text="Vazgeç"></asp:Button>

                            </td>
                        </tr>
                    </table>
                </td>
            </asp:Panel>
        </div>


    </form>
</body>

</html>
