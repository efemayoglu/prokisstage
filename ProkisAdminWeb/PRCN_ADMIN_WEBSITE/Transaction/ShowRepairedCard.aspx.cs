﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Other;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Transaction_ShowRepairedCard : System.Web.UI.Page
{

    private ParserLogin LoginDatas;
    private int repairId = 0;
    private int ownSubMenuIndex = -1;

    ParserGetRepairCard repairCardDetails;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];
        this.repairId = Convert.ToInt32(Request.QueryString["itemId"]);
        //this.hiddenUserId.Value = Request.QueryString["itemID"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Transaction/ListRepairCard.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd != "1")
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }

        if (!Page.IsPostBack)
        {
            this.BuildControls();
        }

    }

    private void BuildControls()
    {
        ListRepairedCardDetails();
    }

    public void ListRepairedCardDetails()
    {
        try
        {
            CallWebServices callWebServ = new CallWebServices();
            repairCardDetails = callWebServ.CallGetRepairedCardDetailService(this.repairId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            this.SetControls();
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskCardFixedSrch");
        }
    }


    private void SetControls()
    {
            this.transactionIdField.Text = this.repairCardDetails.RepairCards.TransactionId.ToString();
            this.kioskNameField.Text = this.repairCardDetails.RepairCards.KioskName;
            this.customerNameField.Text = this.repairCardDetails.RepairCards.CustomerName;
            this.transactionDateField.Text = this.repairCardDetails.RepairCards.TransactionDate;

            this.anaKredi.Text = this.repairCardDetails.RepairCards.AnaKredi.ToString();
            this.yedekKredi.Text = this.repairCardDetails.RepairCards.YedekKredi;

 
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        UpdateRepairedCardDetails();
    }

    private void UpdateRepairedCardDetails()
    {

        try
        {

            CallWebServices callWebServ = new CallWebServices();

            ParserOperation parserUpdateRepairedCard = callWebServ.CallUpdateRepairedCardDetailsService(repairId,
                                                                                                      anaKredi.Text,
                                                                                                      yedekKredi.Text,
                                                                                                      this.LoginDatas.User.Id,
                                                                                                      this.LoginDatas.AccessToken);


            if (parserUpdateRepairedCard != null)
            {
                string scriptText = "";

                //this.messageArea.InnerHtml = parserSaveControlCashNotification.errorDescription;

                scriptText = "parent.showMessageWindowRepairedCard('İşlem Başarılı.');";
                ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);

            }
            else if (parserUpdateRepairedCard.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
            {
                Session.Abandon();
                Session.RemoveAll();
                Response.Redirect("../root/Login.aspx", true);
            }
            else
            {
                this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('Sisteme Erişilemiyor !'); ", true);
            }

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "Transaction");
        }
    }
    

}