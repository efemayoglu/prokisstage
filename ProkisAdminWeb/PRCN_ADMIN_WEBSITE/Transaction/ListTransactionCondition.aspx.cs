﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Other;

public partial class Transaction_ListTransactionCondition : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 3;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Transaction/ListTransactionCondition.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.startDateField.Text = DateTime.Now.AddDays(0).ToString("yyyy-MM-dd 00:00:00");
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd 00:00:00");

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {

                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }


            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {

                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();

            }

            ListItem item1 = new ListItem();
            item1.Text = "Kurum Seçiniz";
            item1.Value = "0";
            this.instutionBox.Items.Add(item1);
            this.instutionBox.SelectedValue = "0";

            ParserListInstutionOperationName operationType = callWebServ.CallGetOperationTypeNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (operationType != null)
            {

                this.operationTypeBox.DataSource = operationType.ParserListInstutionOperationNames;
                this.operationTypeBox.DataBind();

            }

            ListItem item2 = new ListItem();
            item2.Text = "İşlem Tipi Seçiniz";
            item2.Value = "0";
            this.operationTypeBox.Items.Add(item2);
            this.operationTypeBox.SelectedValue = "0";

            ParserListCardTypeName cardType = callWebServ.CallGetCardTypeNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (cardType != null)
            {

                this.cardTypeBox.DataSource = cardType.ParserListCardTypeNames;
                this.cardTypeBox.DataBind();

            }

            ListItem item3 = new ListItem();
            item3.Text = "Kart Tipi Seçiniz";
            item3.Value = "0";
            this.cardTypeBox.Items.Add(item3);
            this.cardTypeBox.SelectedValue = "0";


            ParserListTransactionConditionName transactionConditionName = callWebServ.CallGetTransactionConditionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (transactionConditionName != null)
            {

                this.transactionStatusBox.DataSource = transactionConditionName.ParserListTransactionConditionNames;
                this.transactionStatusBox.DataBind();

            }

            ListItem item4 = new ListItem();
            item4.Text = "İşlem Durumu Seçiniz";
            item4.Value = "0";
            this.transactionStatusBox.Items.Add(item4);
            this.transactionStatusBox.SelectedValue = "0";


            //paymentBox
            ParserListPaymentType paymentType = callWebServ.CallGetPaymentTypeService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (paymentType != null)
            {

                this.paymentTypeBox.DataSource = paymentType.PaymentType;
                this.paymentTypeBox.DataBind();

            }

            ListItem item5 = new ListItem();
            item5.Text = "Ödeme Türünü Seçiniz";
            item5.Value = "0";
            this.paymentTypeBox.Items.Add(item5);
            this.paymentTypeBox.SelectedValue = "0";



            ViewState["OrderColumn"] = 3;
            ViewState["OrderDesc"] = 1;
            this.pageNum = 0;

            string customerId = Request.QueryString["tranId"];
            if (customerId != null)
            {
                this.startDateField.Text = DateTime.Now.AddDays(-2).ToString("yyyy-MM-dd 00:00:00");
                searchTextField.Text = customerId;
            }

            this.SearchOrder(3, 1);
        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(3, 1);
    }

    private void BindListTable(ParserListTransactionCondition transactions)
    {
        try
        {
            TableRow[] Row = new TableRow[transactions.TransactionConditions.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("transactionId", transactions.TransactionConditions[i].TransactionId.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell transactionIdCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell customerNameCell = new TableCell();
                TableCell transactionDateCell = new TableCell();
                TableCell instutionNameCell = new TableCell();
                TableCell totalAmountCell = new TableCell();
                TableCell cashAmountCell = new TableCell();
                TableCell codeAmountCell = new TableCell();
                TableCell creditCardCell = new TableCell();
                TableCell instutionAmountCell = new TableCell();
                TableCell commissionCell = new TableCell();
                TableCell calculatedChangeCell = new TableCell();
                TableCell givenChangeCell = new TableCell();
                TableCell usageFeeCell = new TableCell();
                TableCell paidFaturaCell = new TableCell();
                TableCell transationStatusCell = new TableCell();
                TableCell detailsCell = new TableCell();
                TableCell payTypeNameCell = new TableCell();
                TableCell reverseRecCell = new TableCell();
                

                indexCell.CssClass = "inputTitleCell4";
                transactionIdCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                transactionDateCell.CssClass = "inputTitleCell4";
                instutionNameCell.CssClass = "inputTitleCell4";
                totalAmountCell.CssClass = "inputTitleCell4";
                cashAmountCell.CssClass = "inputTitleCell4";
                codeAmountCell.CssClass = "inputTitleCell4";
                creditCardCell.CssClass = "inputTitleCell4";
                instutionAmountCell.CssClass = "inputTitleCell4";
                commissionCell.CssClass = "inputTitleCell4";
                calculatedChangeCell.CssClass = "inputTitleCell4";
                givenChangeCell.CssClass = "inputTitleCell4";
                usageFeeCell.CssClass = "inputTitleCell4";
                paidFaturaCell.CssClass = "inputTitleCell4";
                transationStatusCell.CssClass = "inputTitleCell4";
                detailsCell.CssClass = "inputTitleCell4";
                customerNameCell.CssClass = "inputTitleCell4";
                payTypeNameCell.CssClass = "inputTitleCell4";
                reverseRecCell.CssClass = "inputTitleCell4";

                TableCell fakeCell1 = new TableCell();
                TableCell fakeCell2 = new TableCell();
                TableCell fakeCell3 = new TableCell();
                TableCell fakeCell4 = new TableCell();
                TableCell fakeCell5 = new TableCell();
                TableCell fakeCell6 = new TableCell();
                TableCell fakeCell7 = new TableCell();
                TableCell fakeCell8 = new TableCell();
                TableCell fakeCell9 = new TableCell();
                TableCell fakeCell10 = new TableCell();
                TableCell fakeCell11 = new TableCell();
                TableCell fakeCell12 = new TableCell();
                TableCell fakeCell13 = new TableCell();
                TableCell fakeCell14 = new TableCell();

                fakeCell1.CssClass = "inputTitleCell4";
                fakeCell2.CssClass = "inputTitleCell4";
                fakeCell3.CssClass = "inputTitleCell4";
                fakeCell4.CssClass = "inputTitleCell4";
                fakeCell5.CssClass = "inputTitleCell4";
                fakeCell6.CssClass = "inputTitleCell4";
                fakeCell7.CssClass = "inputTitleCell4";
                fakeCell8.CssClass = "inputTitleCell4";
                fakeCell9.CssClass = "inputTitleCell4";
                fakeCell10.CssClass = "inputTitleCell4";
                fakeCell11.CssClass = "inputTitleCell4";
                fakeCell12.CssClass = "inputTitleCell4";
                fakeCell13.CssClass = "inputTitleCell4";
                fakeCell14.CssClass = "inputTitleCell4";

                fakeCell1.Visible = false;
                fakeCell2.Visible = false;
                fakeCell3.Visible = false;
                fakeCell4.Visible = false;
                fakeCell5.Visible = false;
                fakeCell6.Visible = false;
                fakeCell7.Visible = false;
                fakeCell8.Visible = false;
                fakeCell9.Visible = false;
                fakeCell10.Visible = false;
                fakeCell11.Visible = false;
                fakeCell12.Visible = false;
                fakeCell13.Visible = false;
                fakeCell14.Visible = false;

                indexCell.Text = (startIndex + i).ToString();
                transactionIdCell.Text = transactionIdCell.Text = "<a href=\"javascript:void(0);\" onclick=\"showTransactionDetailClicked(" + transactions.TransactionConditions[i].TransactionId + ");\" class=\"anylink\">" + transactions.TransactionConditions[i].TransactionId.ToString() + "</a>";
                //transactions.TransactionConditions[i].TransactionId;

                kioskNameCell.Text = transactions.TransactionConditions[i].KioskName;
                transactionDateCell.Text = transactions.TransactionConditions[i].TransactionDate;
                instutionNameCell.Text = transactions.TransactionConditions[i].InstutionName;
                totalAmountCell.Text = transactions.TransactionConditions[i].TotalAmount;
                cashAmountCell.Text = transactions.TransactionConditions[i].CashAmount;
                codeAmountCell.Text = transactions.TransactionConditions[i].CodeAmount;
                creditCardCell.Text = transactions.TransactionConditions[i].CreditCardAmount;
                //creditCardCell.Text = "Kredi Kartı Kolonu";
                instutionAmountCell.Text = transactions.TransactionConditions[i].InstutionAmount;
                //commissionCell.Text = "Komisyon Kolonu";
                commissionCell.Text = transactions.TransactionConditions[i].CommisionAmount;
                calculatedChangeCell.Text = transactions.TransactionConditions[i].CalculatedChange;
                givenChangeCell.Text = transactions.TransactionConditions[i].GivenChange;
                usageFeeCell.Text = transactions.TransactionConditions[i].UsageFee;
                paidFaturaCell.Text = transactions.TransactionConditions[i].PaidFaturaCount;
                transationStatusCell.Text = transactions.TransactionConditions[i].TransationStatus;
                customerNameCell.Text = transactions.TransactionConditions[i].CustomerName;
                payTypeNameCell.Text = transactions.TransactionConditions[i].PayTypeName;
                reverseRecCell.Text = transactions.TransactionConditions[i].ReverseRecord;

                detailsCell.Width = Unit.Pixel(60);

                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1")
                {
                    detailsCell.Text = "<a href=# onClick=\"cancelApprovedTxn(" + transactions.TransactionConditions[i].TransactionId + ");\" class=\"generateCancelButton\" Title=\"İşlemi İptal Et\"></a>"+
                        "<a href=# onClick=\"showCompleteStatusDetailWindow(" + transactions.TransactionConditions[i].TransactionId + ");\" class=\"generateChangeCompleteStatus\" Title=\"İşlem Durumu\"></a>";
                }
                else
                {
                    detailsCell.Text = " ";
                }

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        fakeCell1,
                        fakeCell2,
                        fakeCell3,
                        fakeCell4,
                        fakeCell5,
                        fakeCell6,
                        fakeCell7,
                        fakeCell8,
                        fakeCell9,
                        fakeCell10,
                        fakeCell11,
                        fakeCell12,
                        fakeCell13,
                        fakeCell14,
						transactionIdCell,
                        kioskNameCell,
                        customerNameCell,
                        transactionDateCell,
                        instutionNameCell,
                        payTypeNameCell,
                        totalAmountCell, 
                        cashAmountCell,
                        codeAmountCell,
                        creditCardCell,
                        instutionAmountCell,
                        commissionCell,
                        calculatedChangeCell,
                        givenChangeCell,
                        usageFeeCell,
                        paidFaturaCell,
                        transationStatusCell,
                        reverseRecCell,
				        detailsCell
                        
                });

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (transactions.recordCount < currentRecordEnd)
                currentRecordEnd = transactions.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + transactions.recordCount.ToString() + " kayıt bulundu.";
            }


            TableRow addNewRow = new TableRow();
            TableCell totalTotalAmountCell = new TableCell();
            TableCell totalCashAmountCell = new TableCell();
            TableCell totalCodeAmountCell = new TableCell();
            TableCell totalCreditCardCell = new TableCell();
            TableCell totalInsttutionAmountCell = new TableCell();
            //TableCell totalCommissionCell = new TableCell();
            TableCell totalKioskUsageFeeCell = new TableCell();
            TableCell totalCalculatedChangeAmountCell = new TableCell();
            TableCell totalGivenChangeAmountCell = new TableCell();
            TableCell totalPaidFaturaCountCell = new TableCell();
            TableCell totalCreditCardAmountCell = new TableCell();
            TableCell totalCommisionAmountCell = new TableCell();
            TableCell totalCell = new TableCell();
            TableCell spaceCell = new TableCell();
            TableCell spaceCell1 = new TableCell();
            TableCell spaceCell2 = new TableCell();
            TableCell spaceCell3 = new TableCell();

            totalTotalAmountCell.CssClass = "inputTitleCell99";
            totalCashAmountCell.CssClass = "inputTitleCell99";
            totalCodeAmountCell.CssClass = "inputTitleCell99";
            //totalCreditCardCell.CssClass = "inputTitleCell99";
            totalInsttutionAmountCell.CssClass = "inputTitleCell99";
            totalCommisionAmountCell.CssClass = "inputTitleCell99";
            totalKioskUsageFeeCell.CssClass = "inputTitleCell99";
            totalCalculatedChangeAmountCell.CssClass = "inputTitleCell99";
            totalGivenChangeAmountCell.CssClass = "inputTitleCell99";
            totalPaidFaturaCountCell.CssClass = "inputTitleCell99";
            totalCell.CssClass = "inputTitleCell99";
            totalCreditCardAmountCell.CssClass = "inputTitleCell99";
            totalCommisionAmountCell.CssClass = "inputTitleCell99";


            spaceCell.CssClass = "inputTitleCell99";
            spaceCell.ColumnSpan = (6);

            spaceCell1.CssClass = "inputTitleCell99";
            spaceCell2.CssClass = "inputTitleCell99";
            spaceCell3.CssClass = "inputTitleCell99";
            spaceCell1.ColumnSpan = (1);

            spaceCell.Text = " ";
            spaceCell1.Text = " ";
            totalCell.Text = "Toplam : ";
            spaceCell2.Text = "";
            totalTotalAmountCell.Text = transactions.totalTotalAmount;
            totalCashAmountCell.Text = transactions.totalCashAmount;
            totalCodeAmountCell.Text = transactions.totalCodeAmount;
            totalCreditCardCell.Text = "";
            totalInsttutionAmountCell.Text = transactions.totalInsttutionAmount;
            //totalCommissionCell.Text = "";
            totalKioskUsageFeeCell.Text = transactions.totalKioskUsageFee;
            totalCalculatedChangeAmountCell.Text = transactions.totalCalculatedChangeAmount;
            totalGivenChangeAmountCell.Text = transactions.totalGivenChangeAmount;
            totalPaidFaturaCountCell.Text = transactions.totalPaidFaturaCount;
            totalCreditCardAmountCell.Text = transactions.totalCreditCardAmount;
            totalCommisionAmountCell.Text = transactions.totalCommisionAmount;

            addNewRow.Cells.Add(spaceCell);
            addNewRow.Cells.Add(totalCell);
            addNewRow.Cells.Add(totalTotalAmountCell);
            addNewRow.Cells.Add(totalCashAmountCell);
            addNewRow.Cells.Add(totalCodeAmountCell);
            //addNewRow.Cells.Add(totalCreditCardCell);
            addNewRow.Cells.Add(totalCreditCardAmountCell);
            addNewRow.Cells.Add(totalInsttutionAmountCell);
            addNewRow.Cells.Add(totalCommisionAmountCell);
            addNewRow.Cells.Add(totalCalculatedChangeAmountCell);
            addNewRow.Cells.Add(totalGivenChangeAmountCell);
            addNewRow.Cells.Add(totalKioskUsageFeeCell);
            addNewRow.Cells.Add(totalPaidFaturaCountCell);
            addNewRow.Cells.Add(spaceCell1);
            addNewRow.Cells.Add(spaceCell2);
            addNewRow.Cells.Add(spaceCell3);
            this.itemsTable.Rows.Add(addNewRow);


            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(transactions.pageCount, this.pageNum, transactions.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "TransactionId":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "KioskName":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "TransactionDate":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Instution":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // Instution.Text = "<a>Kurum    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  Instution.Text = "<a>Kurum    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Instution.Text = "<a>Kurum    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "TotalAmount":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // TotalAmount.Text = "<a>T. Tutar    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // TotalAmount.Text = "<a>T. Tutar    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TotalAmount.Text = "<a>T. Tutar    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CashAmount":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // CashAmount.Text = "<a>Nakit    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  CashAmount.Text = "<a>Nakit    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  CashAmount.Text = "<a>Nakit    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CodeAmount":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // CodeAmount.Text = "<a>Kod    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  CodeAmount.Text = "<a>Kod    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // CodeAmount.Text = "<a>Kod    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "InstutionAmount":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // InstutionAmount.Text = "<a>Y. Tutar    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // InstutionAmount.Text = "<a>Y. Tutar    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //InstutionAmount.Text = "<a>Y. Tutar    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CalculatedChange":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //CalculatedChange.Text = "<a>H. Paraüstü    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //CalculatedChange.Text = "<a>H. Paraüstü    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //CalculatedChange.Text = "<a>H. Paraüstü    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "GivenChange":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //GivenChange.Text = "<a>V. Paraüstü    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // GivenChange.Text = "<a>V. Paraüstü    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // GivenChange.Text = "<a>V. Paraüstü    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "UsageFee":
                if (orderSelectionColumn == 11)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // UsageFee.Text = "<a>K. Ücreti    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //UsageFee.Text = "<a>K. Ücreti    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //UsageFee.Text = "<a>K. Ücreti    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 11;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "PaidFatura":
                if (orderSelectionColumn == 12)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //PaidFatura.Text = "<a>Ö. Fat. Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // PaidFatura.Text = "<a>Ö. Fat. Sayısı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //PaidFatura.Text = "<a>Ö. Fat. Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 12;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "TransationStatus":
                if (orderSelectionColumn == 13)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransationStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransationStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransationStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 13;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "CreditCard":
                if (orderSelectionColumn == 14)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransationStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransationStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransationStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 14;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Commission":
                if (orderSelectionColumn == 15)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransationStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransationStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransationStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 15;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "CustomerName":
                if (orderSelectionColumn == 16)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransationStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransationStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransationStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 16;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "PayTypeName":
                if (orderSelectionColumn == 17)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransationStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransationStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransationStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 17;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "LastStatus":
                if (orderSelectionColumn == 18)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransationStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransationStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransationStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 18;
                    orderSelectionDescAsc = 1;
                }
                break;

            default:

                orderSelectionColumn = 3;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {

            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            //ViewState["NumberOfItem"] = this.numberOfItemField.Text;
            int recordCount = 0;
            int pageCount = 0;
            int custId = 0;

            if (Convert.ToInt32(Request.QueryString["customerId"]) != 0)
            {
                custId = Convert.ToInt32(Request.QueryString["customerId"]);
            }

            CallWebServices callWebServ = new CallWebServices();
            ParserListTransactionCondition transactions = callWebServ.CallListTransactionConditionService(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             Convert.ToInt32(this.operationTypeBox.SelectedValue),
                                                                             Convert.ToInt32(this.cardTypeBox.SelectedValue),
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             kioskIds,
                                                                             Convert.ToInt32(this.transactionStatusBox.SelectedValue),
                                                                             Convert.ToInt32(this.paymentTypeBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             1);


            if (transactions != null)
            {
                if (transactions.errorCode == 0)
                {
                    this.BindListTable(transactions);
                }
                else if (transactions.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + transactions.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionSrch");
        }
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            int recordCount = 0;
            int pageCount = 0;
            int custId = 0;

            if (Convert.ToInt32(Request.QueryString["customerId"]) != 0)
            {
                custId = Convert.ToInt32(Request.QueryString["customerId"]);
            }

            CallWebServices callWebServ = new CallWebServices();
            ParserListTransactionCondition lists = callWebServ.CallListTransactionConditionService(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             Convert.ToInt32(this.operationTypeBox.SelectedValue),
                                                                             Convert.ToInt32(this.cardTypeBox.SelectedValue),
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             kioskIds,
                                                                             Convert.ToInt32(this.transactionStatusBox.SelectedValue),
                                                                             Convert.ToInt32(this.paymentTypeBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             2);
            if (lists != null)
            {
                if (lists.errorCode == 0)
                {
                    for (int i = 0; i < lists.TransactionConditions.Count; i++)
                    {
                        lists.TransactionConditions[i].CalculatedChange = lists.TransactionConditions[i].CalculatedChange.Replace('.', ',');
                        lists.TransactionConditions[i].CashAmount = lists.TransactionConditions[i].CashAmount.Replace('.', ',');
                        lists.TransactionConditions[i].CodeAmount = lists.TransactionConditions[i].CodeAmount.Replace('.', ',');
                        lists.TransactionConditions[i].GivenChange = lists.TransactionConditions[i].GivenChange.Replace('.', ',');
                        lists.TransactionConditions[i].InstutionAmount = lists.TransactionConditions[i].InstutionAmount.Replace('.', ',');
                        lists.TransactionConditions[i].TotalAmount = lists.TransactionConditions[i].TotalAmount.Replace('.', ',');
                        lists.TransactionConditions[i].UsageFee = lists.TransactionConditions[i].UsageFee.Replace('.', ',');
                    }
      

                    //ExportExcellDatas exportExcell = new ExportExcellDatas();
                   // exportExcell.ExportExcell(lists.TransactionConditions, lists.recordCount, "Tarife İşlem Raporu");
                    //exportExcell.ExportExcellByBlock(lists.TransactionConditions, "Tarife İşlem Raporu",null);

                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    string[] headerNames = {  "Kiosk No","İşlem No", "Kiosk Adı","İşlem Zamanı", "Kurum","T.Tutar",
                                               "Nakit",  "Kod", "Kredi Kartı" ,"Y.Tutar" ,"Komisyon", "H. Paraüstü ", "V. Paraüstü",
                                               "K. Ücreti", "Ö. Fat. Sayısı","İşlem Durumu","Müşteri Adı","Ödeme Türü"};

                    exportExcell.ExportExcellByBlock(lists.TransactionConditions, "Kiosk İşlem Durumu", headerNames);

                }
                else if (lists.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + lists.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelTransaction");
        }
    }
}