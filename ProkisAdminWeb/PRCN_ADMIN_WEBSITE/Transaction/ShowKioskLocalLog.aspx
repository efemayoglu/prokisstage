﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowKioskLocalLog.aspx.cs" Inherits="Transaction_ShowKioskLocalLog" %>


<%@ Register Src="../MS_Control/MultipleSelection.ascx" TagName="MultipleSelection" TagPrefix="uc1" %>
<%@ Register Assembly="CheckBoxListExCtrl" Namespace="CheckBoxListExCtrl" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />
    <input type="hidden" id="hiddenId" runat="server" name="hiddenId" value="0" />
</head>

    
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { setInterval(function () { cache_clear() }, 120000); });
        function cache_clear() { window.location.reload(true); }
    </script>

       <script language="javascript" type="text/javascript">



           //function editKioskReferenceSystemButtonClicked(referenceId) {
           //    var retVal = showKioskReferenceSystem(referenceId);
           //}

           function editKioskLocalLogButtonClicked() {
               var retVal = showKioskLocalLog();
           }

           //
           //function deleteButtonClicked(storedProcedure, itemId) {
           //
           //    var retVal = showDeleteWindowKioskReferenceSystem(storedProcedure, itemId);
           //}

           function strtDateChngd() {
               if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                   document.getElementById('startDateField').value = document.getElementById('endDateField').value;
               }

           }

           function endDateChngd() {
               if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                   document.getElementById('endDateField').value = document.getElementById('startDateField').value;
               }
           }

           function showAlert(message) {
               var retVal = showAlertWindow(message);
           }

           //function getDailyAccountActionsClicked(itemId) {
           //    var url = "../Account/KiosCashAcceptor.aspx?itemId=" + itemId;
           //    var retValue = openDialoagWindow(url, 1000, 600, "", "Kiosk Para Alımı");
           //}

           //function getDailyCustomerAccountsClicked(itemId) {
           //    var url = "../Account/KiosCashAcceptor.aspx?itemId=" + itemId + "&isActive=1";
           //    var retValue = openDialoagWindow(url, 700, 600, "", "Kiosk Para Alımı");
           //}

           function searchButtonClicked(sender) {
               document.getElementById('pageNumRefField').value = "0";
               return true;
           }

           function showTransactionDetailClicked(transactionID) {
               var retVal = showTransactionDetailWindow(transactionID);
           }

           function postForPaging(selectedPageNum) {
               document.getElementById('pageNumRefField').value = selectedPageNum;
               document.getElementById('navigateButton').click();
           }


           function validateNo(evt) {
               var theEvent = evt || window.event;
               var key = theEvent.keyCode || theEvent.which;
               key = String.fromCharCode(key);
               var regex = /[0-9]/;
               if (!regex.test(key)) {
                   theEvent.returnValue = false;
                   if (theEvent.preventDefault) theEvent.preventDefault();
               }
           }

    </script>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
 
<body>
    <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>


        <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel7" runat="server" BackColor="White" Width="70%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <div align="center" id="articleDetailsTab" class="windowTitle_container_autox30">
                    Kiosk Yerel Log
                    <hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>

                <table border="0" cellpadding="2" cellspacing="0" id="Table1" runat="server">

                    <tr>
                        <td align="center" class="inputTitleCell2" height="25" colspan="2">İşlem No:
                        </td>
                    </tr>

                    <tr>
                        <td align="center" colspan="2" height="30">
                            <asp:TextBox ID="transactionId" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_text_align_center" text-align="center"/>
                        </td>
                    </tr>



                </table>


                




                <br />

                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Kaydet"></asp:Button>

                                <asp:Button ID="cancelButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClientClick="parent.hideModalPopup2_Exit();" Text="Vazgeç"></asp:Button>

                            </td>
                        </tr>
                    </table>
                </td>
            </asp:Panel>
        </div>
    </form>

</body>
</html>
