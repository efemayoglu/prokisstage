﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Transaction_ListTransactionDetails : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;
    private int ownSubMenuIndex = -1;
    private int transactionId;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Transaction/ListTransactions.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.transactionId = Convert.ToInt32(Request.QueryString["transactionId"]);

        if (!Page.IsPostBack)
        {
            this.BuildControls();
        }
    }

    private void BuildControls()
    {
        ListTransactionDetails();
    }

    public void ListTransactionDetails()
    {
        try
        {
            CallWebServices callWebServ = new CallWebServices();
            ParserListTransactionDetails transactionDetails = callWebServ.CallListTransactionDetailService(this.transactionId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (transactionDetails != null)
            {
                if (transactionDetails.errorCode == 0)
                {
                    this.BindListTable(transactionDetails);
                }
                else if (transactionDetails.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + transactionDetails.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionDetailsSrch");
        }
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    private void ExportToExcel()
    {
        try
        {

            int custId = 0;

            if (Convert.ToInt32(Request.QueryString["customerId"]) != 0)
            {
                custId = Convert.ToInt32(Request.QueryString["customerId"]);
            }

            CallWebServices callWebServ = new CallWebServices();
            ParserListTransactionDetails lists = callWebServ.CallListTransactionDetailService(this.transactionId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (lists != null)
            {
                if (lists.errorCode == 0)
                {

                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    //for (int i = 0; i < lists.TransactionDetails.Count; i++)
                    //{
                    //    lists.TransactionDetails[i].
                    //}
                    exportExcell.ExportExcell(lists.TransactionDetails, 0, "İşlem Detay Listesi");
                }
                else if (lists.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + lists.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelTransactionDetail");
        }
    }

    private void BindListTable(ParserListTransactionDetails transactions)
    {
        try
        {
            TableRow[] Row = new TableRow[transactions.TransactionDetails.Count];

            if(transactions.TransactionDetails.Count>0)
            {
                lblIslemId.Text = transactions.TransactionDetails[0].TransactionId.ToString();
                lblKioskName.Text = transactions.TransactionDetails[0].kioskName;
            }

            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("transactionId", transactions.TransactionDetails[i].TransactionId.ToString());

                TableCell indexCell = new TableCell();
                //TableCell idCell = new TableCell();
                TableCell processStatusCell = new TableCell();
                TableCell processDescriptionCell = new TableCell();
                TableCell transactionDateCell = new TableCell();
                //TableCell kioskNameCell = new TableCell();

                //kioskNameCell.Width = 150;

                indexCell.CssClass = "inputTitleCell4";
                //idCell.CssClass = "inputTitleCell4";
                processStatusCell.CssClass = "inputTitleCell4";
                processDescriptionCell.CssClass = "inputTitleCell4";
                transactionDateCell.CssClass = "inputTitleCell4";
                //kioskNameCell.CssClass = "inputTitleCell4";

                indexCell.Text = (1 + i).ToString();
                //idCell.Text = transactions.TransactionDetails[i].TransactionId.ToString();
                processStatusCell.Text = transactions.TransactionDetails[i].ProcessStatus;

                string processDesc=transactions.TransactionDetails[i].ProcessDescription;
                string partial = "";

                if (processDesc.Contains("Tarife:"))
                {
                    processDesc = processDesc.Substring(0, processDesc.IndexOf("Tarife:"))+" Tarife Gizlendi.";
                }
                else if (processDesc.Contains("\"tarife\":") && processDesc.Contains("\"satisMiktari\":"))
                {
                    partial = processDesc.Substring(processDesc.IndexOf("\"tarife\":"), processDesc.IndexOf("\"satisMiktari\":") - processDesc.IndexOf("\"tarife\":"));
                    processDesc = processDesc.Replace(partial, " Tarife Gizlendi. ");
                }
                
                processDescriptionCell.Text = processDesc;

                transactionDateCell.Text = transactions.TransactionDetails[i].TransactionDate.ToString();
                //kioskNameCell.Text = transactions.TransactionDetails[i].kioskName;
                processDescriptionCell.HorizontalAlign = HorizontalAlign.Left;

                //processDescriptionCell.Width = Unit.Pixel(40);
                //idCell.Width = Unit.Pixel(80);
                //processStatusCell.Width = Unit.Pixel(80);
                //transactionDateCell.Width = Unit.Pixel(100);

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        //idCell,
                        //kioskNameCell,
						processStatusCell,
                        processDescriptionCell,
                        transactionDateCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionDetailsBDT");
        }

    }
}