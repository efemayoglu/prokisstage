﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Other;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Transaction_ListMobilePaymentTransaction : System.Web.UI.Page
{

    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Transaction/ListMobilePaymentTransaction.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.startDateField.Text = DateTime.Now.AddDays(-0).ToString("yyyy-MM-dd") + " 00:00:00";
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00";

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {

                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }

            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {

                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();

            }

            ListItem item1 = new ListItem();
            item1.Text = "Kurum Seçiniz";
            item1.Value = "0";
            this.instutionBox.Items.Add(item1);
            this.instutionBox.SelectedValue = "0";


            ParserListMuhasebeStatus muhasebe = callWebServ.CallGetParserListMuhasebeStatusService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (muhasebe != null)
            {

                this.muhasebeBox.DataSource = muhasebe.MuhasebeStatus;
                this.muhasebeBox.DataBind();

            }

            ListItem item2 = new ListItem();
            item2.Text = "Muhasebe Durumunu Seçiniz";
            item2.Value = "0";
            this.muhasebeBox.Items.Add(item2);
            this.muhasebeBox.SelectedValue = "0";


            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;

            this.SearchOrder(4, 1);
        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);


            int recordCount = 0;
            int pageCount = 0;


            CallWebServices callWebServ = new CallWebServices();
            ParserListMobilePaymentTransactions transactions = callWebServ.CallListMobilePaymentTransactionService(this.searchTextField.Text.Trim(),
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             kioskIds,
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                              Convert.ToInt32(this.muhasebeBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             1);


            if (transactions != null)
            {
                if (transactions.errorCode == 0)
                {
                    this.BindListTable(transactions);
                }
                else if (transactions.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + transactions.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListCreditCardTransactionSrch");
        }
    }

    private void BindListTable(ParserListMobilePaymentTransactions transactions)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);


            TableRow[] Row = new TableRow[transactions.Transactions.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                //Row[i].Attributes.Add("transactionId", transactions.Transactions[i].TransactionId.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell customerNameCell = new TableCell();
                //TableCell aboneNoCell = new TableCell();
                TableCell transactionDateCell = new TableCell();
                TableCell mobileNoCell = new TableCell();
                TableCell statusCell = new TableCell();
               
                TableCell instutionNameCell = new TableCell();
                TableCell transactionAmountCell = new TableCell();
                TableCell muhasebeStatusCell = new TableCell();

                indexCell.CssClass = "inputTitleCell4";
                idCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                customerNameCell.CssClass = "inputTitleCell4";
                //aboneNoCell.CssClass = "inputTitleCell4";
                transactionDateCell.CssClass = "inputTitleCell4";
                mobileNoCell.CssClass = "inputTitleCell4";
                statusCell.CssClass = "inputTitleCell4";
                instutionNameCell.CssClass = "inputTitleCell4";
                muhasebeStatusCell.CssClass = "inputTitleCell4";

             
                transactionAmountCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                idCell.Text = "<a href=\"javascript:void(0);\" onclick=\"showTransactionDetailClicked(" + transactions.Transactions[i].TransactionId + ");\" class=\"anylink\">" + transactions.Transactions[i].TransactionId.ToString() + "</a>";
                kioskNameCell.Text = transactions.Transactions[i].KioskName;
                customerNameCell.Text = transactions.Transactions[i].CustomerName;
                //aboneNoCell.Text = transactions.Transactions[i].AboneNo;
                mobileNoCell.Text = transactions.Transactions[i].MobileNo;
                statusCell.Text = transactions.Transactions[i].Status;
                transactionDateCell.Text = transactions.Transactions[i].TransactionDate.ToString();
                
                instutionNameCell.Text = transactions.Transactions[i].InstutionName;
                transactionAmountCell.Text = transactions.Transactions[i].TransactionAmount;
                muhasebeStatusCell.Text = transactions.Transactions[i].MuhasebeStatus;

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        idCell,
						kioskNameCell,
                        customerNameCell,
                        //aboneNoCell,
                        transactionDateCell,
                        transactionAmountCell,
                        mobileNoCell,
                        statusCell,
                        muhasebeStatusCell,
                        instutionNameCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (transactions.recordCount < currentRecordEnd)
                currentRecordEnd = transactions.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + transactions.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(transactions.pageCount, this.pageNum, transactions.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);



            TableRow addNewRow = new TableRow();
            TableCell spaceCell = new TableCell();
            TableCell totalCell = new TableCell();
            TableCell transactionTotalAmountCell = new TableCell();
            TableCell space2Cell = new TableCell();

            spaceCell.CssClass = "inputTitleCell99";
            totalCell.CssClass = "inputTitleCell99";
            transactionTotalAmountCell.CssClass = "inputTitleCell99";
            space2Cell.CssClass = "inputTitleCell99";

            spaceCell.ColumnSpan = (4);
            space2Cell.ColumnSpan = (4);

            spaceCell.Text = " ";
            totalCell.Text = "Toplam : ";
            transactionTotalAmountCell.Text = transactions.TotalTransactionAmount;

            addNewRow.Cells.Add(spaceCell);
            addNewRow.Cells.Add(totalCell);
            addNewRow.Cells.Add(transactionTotalAmountCell);
            addNewRow.Cells.Add(space2Cell);

            this.itemsTable.Rows.Add(addNewRow);


            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListCreditCardTransactionBDT");
        }
    }


    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(4, 1);
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }


    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "TransactionId":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "KioskName":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CustomerName":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // CustomerName.Text = "<a>Müşteri Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // CustomerName.Text = "<a>Müşteri Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // CustomerName.Text = "<a>Müşteri Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "TransactionDate":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "AboneNo":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransactionStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransactionStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Instution":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // Instution.Text = "<a>Kurum    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Instution.Text = "<a>Kurum    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Instution.Text = "<a>Kurum    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "MobileNo":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //AboneNo.Text = "<a>Abone No    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //AboneNo.Text = "<a>Abone No    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //AboneNo.Text = "<a>Abone No    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Status":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //OperationType.Text = "<a>İşlem Tipi   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "RRN":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //OperationType.Text = "<a>İşlem Tipi   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "TransactionAmount":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //OperationType.Text = "<a>İşlem Tipi   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "MuhasebeStatus":
                if (orderSelectionColumn == 11)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //OperationType.Text = "<a>İşlem Tipi   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 11;
                    orderSelectionDescAsc = 1;
                }
                break;

            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }


    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {

            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);


            int recordCount = 0;
            int pageCount = 0;


            CallWebServices callWebServ = new CallWebServices();
            ParserListMobilePaymentTransactions transactions = callWebServ.CallListMobilePaymentTransactionService(this.searchTextField.Text.Trim(),
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             kioskIds,
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             Convert.ToInt32(this.muhasebeBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             2);



            if (transactions != null)
            {
                if (transactions.errorCode == 0)
                {
                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    //exportExcell.ExportExcellByBlock(lists.Transactions,"İşlemler", null);

                    string[] headerNames = {  "İşlem No","Kiosk Adı", "Kiosk No", "Müşteri Adı","Abone No",
                                               "İşlem Tarihi", "Kredi Kart No",  "Durum",  "Kurum" , "İşlem Tutarı","Muhasebe Durumu" };

                    exportExcell.ExportExcellByBlock(transactions.Transactions, "Kiosk Kredi Kartı İşlem Raporu", headerNames);
                }
                else if (transactions.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + transactions.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelCreditCardTransaction");
        }
    }

}