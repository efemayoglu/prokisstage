﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Transaction_ShowKioskCardFixed : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private int transactionId = 0;
    private int ownSubMenuIndex = -1;

    ParserListTransactionDetails transactionDetails;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];
        this.transactionId = Convert.ToInt32(Request.QueryString["transactionId"]);
        //this.hiddenUserId.Value = Request.QueryString["itemID"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Transaction/ListTransactions.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd != "1")
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }

        if (!Page.IsPostBack)
        {
            this.BuildControls();
        }

    }

    private void BuildControls()
    {
        ListTransactionDetails();
    }

    public void ListTransactionDetails()
    {
        try
        {
            CallWebServices callWebServ = new CallWebServices();
            transactionDetails = callWebServ.CallListTransactionDetailService(this.transactionId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (transactionDetails != null)
            {
                if (transactionDetails.errorCode == 0)
                {
                    this.SetControls();
                }
                else if (transactionDetails.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + transactionDetails.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskCardFixedSrch");
        }
    }


    private void SetControls()
    {
        try
        {
            this.kioskNameField.Text = this.transactionDetails.TransactionDetails[0].kioskName;
            this.transactionIdField.Text = this.transactionDetails.TransactionDetails[0].TransactionId.ToString();
            this.transactionDateField.Text = this.transactionDetails.TransactionDetails[0].TransactionDate;


        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "EnterListKioskDispenserCashCountSC");
        }
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        int updateStatus = 0;
       


        CallWebServices callWebServ = new CallWebServices();
        ParserOperation parserSaveCardRepair = callWebServ.CallSaveCardRepairService(this.transactionId, this.anaKredi.Text, this.yedekKredi.Text, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        
        //messageArea.InnerText = "İşlem Başarılı..";
        //scriptText = "parent.showMessageWindowKiosk('İşlem Başarılı.');";

        //switch (updateStatus)
        //{
        //    case (int)ManagementScreenErrorCodes.SystemError:
        //        this.messageArea.InnerHtml = "System error has occurred. Try again.";
        //        break;
        //    case (int)ManagementScreenErrorCodes.SQLServerError:
        //        this.messageArea.InnerHtml = "System error has occurred. Try again. (SQL Server Error)";
        //        break;
        //    case -2:
        //        this.messageArea.InnerHtml = "A record already exists.";
        //        break;
        //    default:
        //        this.messageArea.InnerHtml = "İşlem Başarılı.";
        //        //ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:callOwn(); ", true);
        //        //ViewState["merchantID"] = this.merchantID;
        //        scriptText = "parent.showMessageWindowKiosk('İşlem Başarılı.');";
        //        break;
        //}

        if (parserSaveCardRepair != null)
        {
            string scriptText = "";

            //this.messageArea.InnerHtml = parserSaveControlCashNotification.errorDescription;

            scriptText = "parent.showMessageWindowCardRepair('İşlem Başarılı.');";
            ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);

        }
        else if (parserSaveCardRepair.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
        {
            Session.Abandon();
            Session.RemoveAll();
            Response.Redirect("../root/Login.aspx", true);
        }
        else
        {
            this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
            ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('Sisteme Erişilemiyor !'); ", true);
        }
    }




}