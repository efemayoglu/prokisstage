﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListTransactionDetails.aspx.cs" Inherits="Transaction_ListTransactionDetails" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />
    <script type="text/javascript" language="javascript" src="../js/wdws.js"></script>
    <script language="javascript" type="text/javascript">

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

    </script>
    <base target="_self" />
    <style type="text/css">
        .auto-style14 {
            font-weight: bold;
            font-size: 11px;
            font-family: Century Gothic;
            color: #8A8B8C;
            border: 0px outset white;
            width: 99px;
            height: 20px;
        }
        .auto-style15 {
            height: 20px;
        }
    </style>
    </head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <asp:HiddenField ID="transactionIDRefField" runat="server" Value="0" />
        <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel7" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <div align="left" id="articleDetailsTab" class="windowTitle_container_autox30">
                    İşlem Detayları
                    <hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>
                <table cellpadding="0" cellspacing="0" width="95%" id="upTable" runat="server">
                    <tr style="height: 20px">
                        <td style="width: 99px;" align="left" class="inputTitle">
                            <asp:Label ID="startDateLabel" runat="server" Visible="true">İşlem ID:</asp:Label>
                        </td>
                        <td class="inputTitle" align="left" style="width: 200px">
                            <asp:Label ID="lblIslemId" runat="server" Visible="true">İşlem ID:</asp:Label>
                        </td>
                        <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="excellButton0" runat="server" ClientIDMode="Static" ImageUrl="~/images/excel.jpg" OnClick="excelButton_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="auto-style14">
                            <asp:Label ID="Label1" runat="server" Visible="true">Kiosk İsmi:</asp:Label>
                        </td>
                        <td class="auto-style14" align="left">
                            <asp:Label ID="lblKioskName" runat="server" Visible="true">Kiosk İsmi:</asp:Label>
                        </td>
                        <td class="auto-style15"></td>
                    </tr>
                    <tr>
                        <td align="center" class="tableStyle1" colspan="3">
                            <asp:UpdatePanel ID="galleryListUPanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" RenderMode="Block">
                                <ContentTemplate>
                                    <asp:Table ID="itemsTable" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                        BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="100%">
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="#"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Operasyon Adı"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Operasyon Sonuç"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Operasyon Zamanı"></asp:TableHeaderCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </ContentTemplate>
                                <Triggers>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
