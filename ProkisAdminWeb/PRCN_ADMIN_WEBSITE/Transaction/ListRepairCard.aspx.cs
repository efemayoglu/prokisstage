﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Other;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Transaction_ListRepairCard : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Transaction/ListRepairCard.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {


            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.startDateField.Text = DateTime.Now.AddDays(-0).ToString("yyyy-MM-dd") + " 00:00:00";
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00";
 
            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {

                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }


            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {

                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();

            }

            ListItem item1 = new ListItem();
            item1.Text = "Kurum Seçiniz";
            item1.Value = "0";
            this.instutionBox.Items.Add(item1);
            this.instutionBox.SelectedValue = "0";


            //paymentBox
            ParserListCardRepairType cardRepairStatus = callWebServ.CallGetCardRepairStatusService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (cardRepairStatus != null)
            {

                this.cardRepairStatusBox.DataSource = cardRepairStatus.CardRepairStatus;
                this.cardRepairStatusBox.DataBind();

            }

            ListItem item5 = new ListItem();
            item5.Text = "Onarım Durumunu Seçiniz";
            item5.Value = "0";
            this.cardRepairStatusBox.Items.Add(item5);
            this.cardRepairStatusBox.SelectedValue = "0";


            ParserListIsCardDeleted isCardDeleted = callWebServ.CallGetIsCardDeletedService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (isCardDeleted != null)
            {

                this.isCardDeletedBox.DataSource = isCardDeleted.IsCardDeleted;
                this.isCardDeletedBox.DataBind();

            }

            ListItem item6 = new ListItem();
            item6.Text = "Kart Silme Seçiniz";
            item6.Value = "0";
            this.isCardDeletedBox.Items.Add(item6);
            this.isCardDeletedBox.SelectedValue = "0";


            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;
            this.pageNum = 0;

            string aboneId = Request.QueryString["custId"];
            

            this.SearchOrder(6,1);
        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }
 
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";
            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);
             

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            //ViewState["NumberOfItem"] = this.numberOfItemField.Text;
            int recordCount = 0;
            int pageCount = 0;
            int custId = 0;

            if (Convert.ToInt32(Request.QueryString["customerId"]) != 0)
            {
                custId = Convert.ToInt32(Request.QueryString["customerId"]);
            }

            CallWebServices callWebServ = new CallWebServices();
            ParserListRepairCard repairs = callWebServ.CallListRepairCardService(this.searchTextField.Text.Trim(),
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             kioskIds,
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             Convert.ToInt32(this.cardRepairStatusBox.SelectedValue),
                                                                              Convert.ToInt32(this.isCardDeletedBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             1);


            if (repairs != null)
            {
                if (repairs.errorCode == 0)
                {
                    this.BindListTable(repairs);
                }
                else if (repairs.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + repairs.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionSrch");
        }
    }

    private void BindListTable(ParserListRepairCard transactions)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);


            TableRow[] Row = new TableRow[transactions.RepairCards.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {

                //if (transactions.RepairCards[i].IsDeleted == "Aktif")
                //{ 
               
                Row[i] = new TableRow();
                Row[i].Attributes.Add("transactionId", transactions.RepairCards[i].TransactionId.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell transactionIdCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell customerNameCell = new TableCell();
                TableCell anaKrediCell = new TableCell();
                TableCell yedekKrediCell = new TableCell();
                TableCell transactionDateCell = new TableCell();
                TableCell insertionDateCell = new TableCell();
                TableCell userNameCell = new TableCell();
                TableCell statusCell = new TableCell();
                TableCell isDeletedCell = new TableCell();
                TableCell repairDateCell = new TableCell();
                TableCell repairedTransactionIdCell = new TableCell();

                TableCell detailsCell = new TableCell();

                transactionIdCell.CssClass = "inputTitleCell4";
                customerNameCell.CssClass = "inputTitleCell4";
                anaKrediCell.CssClass = "inputTitleCell4";
                yedekKrediCell.CssClass = "inputTitleCell4";
                transactionDateCell.CssClass = "inputTitleCell4";
                insertionDateCell.CssClass = "inputTitleCell4";
                userNameCell.CssClass = "inputTitleCell4";
                statusCell.CssClass = "inputTitleCell4";
                isDeletedCell.CssClass = "inputTitleCell4";
                repairDateCell.CssClass = "inputTitleCell4";
                repairedTransactionIdCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                detailsCell.CssClass = "inputTitleCell4";


                indexCell.Text = (startIndex + i).ToString();
                //idCell.Text = "<a href=\"javascript:void(0);\" onclick=\"showTransactionDetailClicked(" + transactions.RepairCards[i]. + ");\" class=\"anylink\">" + transactions.Transactions[i].TransactionId.ToString() + "</a>";
                //transactionIdCell.Text = transactions.RepairCards[i].TransactionId.ToString();
                transactionIdCell.Text = "<a href=\"javascript:void(0);\" onclick=\"showTransactionDetailClicked(" + transactions.RepairCards[i].TransactionId + ");\" class=\"anylink\">" + transactions.RepairCards[i].TransactionId.ToString() + "</a>";
                
                customerNameCell.Text = transactions.RepairCards[i].CustomerName;
                anaKrediCell.Text = transactions.RepairCards[i].AnaKredi;
                yedekKrediCell.Text = transactions.RepairCards[i].YedekKredi;
                transactionDateCell.Text = transactions.RepairCards[i].TransactionDate.ToString();
                insertionDateCell.Text = transactions.RepairCards[i].InsertionDate;
                userNameCell.Text = transactions.RepairCards[i].UserName;
                statusCell.Text = transactions.RepairCards[i].Status;
                isDeletedCell.Text = transactions.RepairCards[i].IsDeleted;
                repairDateCell.Text = transactions.RepairCards[i].RepairDate;
                //repairedTransactionIdCell.Text = transactions.RepairCards[i].RepairedTransactionId;
                kioskNameCell.Text = transactions.RepairCards[i].KioskName;
                repairedTransactionIdCell.Text = "<a href=\"javascript:void(0);\" onclick=\"showTransactionDetailClicked(" + transactions.RepairCards[i].RepairedTransactionId + ");\" class=\"anylink\">" + transactions.RepairCards[i].RepairedTransactionId + "</a>";

                detailsCell.Width = Unit.Pixel(85);

                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1" && transactions.RepairCards[i].Status == "Onarim Bekleniyor")
                {

                    //detailsCell.Text = "<a href=# onClick=\"deleteButtonClicked('[pk].[delete_card_repair]'," + transactions.RepairCards[i].RepairedTransactionId + ");\"><img src=../images/delete.png border=0 title=\"Delete\" /></a>";
                    detailsCell.Text = "<a href=# onClick=\"repairCardClicked(" + transactions.RepairCards[i].RepairedId + ");\" class=\"generateCardFixRedirectButton\" Title=\"Kart Onarım Düzenleme\"></a>";

                }


                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanDelete == "1")
                {

                    //detailsCell.Text = "<a href=# onClick=\"deleteButtonClicked('[pk].[delete_card_repair]'," + transactions.RepairCards[i].RepairedTransactionId + ");\"><img src=../images/delete.png border=0 title=\"Delete\" /></a>";
                    detailsCell.Text = detailsCell.Text + "     " +  "<a href=\"javascript: deleteButtonClicked('[pk].[delete_card_repair]'," + transactions.RepairCards[i].RepairedId + ");\" class=\"deleteButton\" title=\"Kart Onarım Sil\"></a>";
                }
 


               

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        transactionIdCell,
                        kioskNameCell,
                        customerNameCell,
                        transactionDateCell,
                        anaKrediCell,
                        yedekKrediCell,
                        insertionDateCell,
                        userNameCell,
                        statusCell,
                        isDeletedCell,
                        repairedTransactionIdCell,
                        repairDateCell,
                        detailsCell,
                        
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";

                //}

            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (transactions.recordCount < currentRecordEnd)
                currentRecordEnd = transactions.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + transactions.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(transactions.pageCount, this.pageNum, transactions.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionBDT");
        }
    }


    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(6,1);
    }

    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {

            
            string kioskIds = "";
            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;
          

            int recordCount = 0;
            int pageCount = 0;
            int custId = 0;

            if (Convert.ToInt32(Request.QueryString["customerId"]) != 0)
            {
                custId = Convert.ToInt32(Request.QueryString["customerId"]);
            }

            CallWebServices callWebServ = new CallWebServices();
            ParserListRepairCard repairs = callWebServ.CallListRepairCardService(this.searchTextField.Text.Trim(),
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             //custId,
                                                                             kioskIds,
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             //Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                            //Convert.ToInt32(this.isCardDeletedBox.SelectedValue),
                                                                             //this.TransactionStatusBox.SelectedValue,
                                                                             Convert.ToInt32(this.cardRepairStatusBox.SelectedValue),
                                                                             Convert.ToInt32(this.isCardDeletedBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             2);

            if (repairs != null)
            {
                if (repairs.errorCode == 0)
                {
                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    //exportExcell.ExportExcellByBlock(lists.Transactions,"İşlemler", null);

                    string[] headerNames = {  "İşlem No","Müşteri Adı", "Ana Kredi","Yedek Kredi", "İşlem Zamanı","Giriş Zamanı",
                                               "Kullanıcı Adı",  "Durum", "Kart Silme Durumu" ,"Onarım Zamanı" , "Onarım No" , "Kiosk No", "Kiosk Adı"};

                    exportExcell.ExportExcellByBlock(repairs.RepairCards, "Kiosk Kart Onarım Raporu", headerNames);
                }
                else if (repairs.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + repairs.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelTransaction");
        }
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "TransactionId":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "CustomerName":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "TransactionDate":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "AnaKredi":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "YedekKredi":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "InsertionDate":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "UserName":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Status":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "IsDeleted":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "CardRepairId":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "CardRepairDate":
                if (orderSelectionColumn == 11)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 11;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "KioskName":
                if (orderSelectionColumn == 12)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 12;
                    orderSelectionDescAsc = 1;
                }
                break;



            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }


}