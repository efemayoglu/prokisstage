﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Other;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Transaction_ListTransactions : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Transaction/ListTransactions.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.startDateField.Text = DateTime.Now.AddDays(-0).ToString("yyyy-MM-dd") + " 00:00:00";
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00";

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {

                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }

            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {

                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();

            }

            ParserListTxnStatusName transactionStatus = callWebServ.CallGetTxnStatusNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (transactionStatus != null)
            {

                this.TransactionStatusBox.DataSource = transactionStatus.TxnStatusNames;
                this.TransactionStatusBox.DataBind();

            }

            ListItem item1 = new ListItem();
            item1.Text = "Kurum Seçiniz";
            item1.Value = "0";
            this.instutionBox.Items.Add(item1);
            this.instutionBox.SelectedValue = "0";

            ParserListInstutionOperationName operationType = callWebServ.CallGetOperationTypeNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (operationType != null)
            {

                this.operationTypeBox.DataSource = operationType.ParserListInstutionOperationNames;
                this.operationTypeBox.DataBind();

            }

            ListItem item2 = new ListItem();
            item2.Text = "İşlem Tipi Seçiniz";
            item2.Value = "0";
            this.operationTypeBox.Items.Add(item2);
            this.operationTypeBox.SelectedValue = "0";

            ListItem item3 = new ListItem();
            item3.Text = "İşlem Durumu Seçiniz";
            item3.Value = "0";
            this.TransactionStatusBox.Items.Add(item3);
            this.TransactionStatusBox.SelectedValue = "0";

            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;
            this.pageNum = 0;

            string aboneId = Request.QueryString["custId"];
            if (aboneId != null)
            {
                this.startDateField.Text = DateTime.Now.AddDays(-2).ToString("yyyy-MM-dd") + " 00:00:00";
                searchTextField.Text = aboneId;
            }

           
                this.startDateField.Text = DateTime.Now.ToString("yyyy-MM-dd 00:00:00");
                this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd 00:00:00");
         

            this.SearchOrder(4, 1);
        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        //Session["startDate"] = startDateField.Text;
        //Session["endDate"] = endDateField.Text;

        this.pageNum = 0;
        this.SearchOrder(4, 1);
    }

    private void BindListTable(ParserListTransactions transactions)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);


            TableRow[] Row = new TableRow[transactions.Transactions.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("transactionId", transactions.Transactions[i].TransactionId.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell customerNameCell = new TableCell();
                TableCell aboneNoCell = new TableCell();
                TableCell transactionStatusCell = new TableCell();
                TableCell transactionDateCell = new TableCell();
                TableCell detailsCell = new TableCell();

                TableCell operationTypeCell = new TableCell();
                TableCell instutionNameCell = new TableCell();
                TableCell fakeCell00 = new TableCell();
                TableCell fakeCell01 = new TableCell();
                fakeCell00.CssClass = "inputTitleCell4";
                fakeCell00.Visible = false;
                instutionNameCell.CssClass = "inputTitleCell4";

                fakeCell01.CssClass = "inputTitleCell4";
                fakeCell01.Visible = false;
                operationTypeCell.CssClass = "inputTitleCell4";

                TableCell fakeCell1 = new TableCell();
                TableCell fakeCell2 = new TableCell();
                TableCell fakeCell3 = new TableCell();
                TableCell fakeCell4 = new TableCell();
                TableCell fakeCell5 = new TableCell();
                TableCell fakeCell6 = new TableCell();
                TableCell fakeCell7 = new TableCell();

                fakeCell1.CssClass = "inputTitleCell4";
                fakeCell2.CssClass = "inputTitleCell4";
                fakeCell3.CssClass = "inputTitleCell4";
                fakeCell4.CssClass = "inputTitleCell4";
                fakeCell5.CssClass = "inputTitleCell4";
                fakeCell6.CssClass = "inputTitleCell4";
                fakeCell7.CssClass = "inputTitleCell4";

                fakeCell1.Visible = false;
                fakeCell2.Visible = false;
                fakeCell3.Visible = false;
                fakeCell4.Visible = false;
                fakeCell5.Visible = false;
                fakeCell6.Visible = false;
                fakeCell7.Visible = false;

                indexCell.CssClass = "inputTitleCell4";
                idCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                customerNameCell.CssClass = "inputTitleCell4";
                aboneNoCell.CssClass = "inputTitleCell4";
                transactionDateCell.CssClass = "inputTitleCell4";
                transactionStatusCell.CssClass = "inputTitleCell4";
                detailsCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                idCell.Text = "<a href=\"javascript:void(0);\" onclick=\"showTransactionDetailClicked(" + transactions.Transactions[i].TransactionId + ");\" class=\"anylink\">" + transactions.Transactions[i].TransactionId.ToString() + "</a>";
                kioskNameCell.Text = transactions.Transactions[i].KioskName;
                customerNameCell.Text = transactions.Transactions[i].CustomerName;
                aboneNoCell.Text = transactions.Transactions[i].AboneNo;
                transactionDateCell.Text = transactions.Transactions[i].TransactionDate.ToString();
                transactionStatusCell.Text = transactions.Transactions[i].ProcessStatus;
                instutionNameCell.Text = transactions.Transactions[i].InstutionName;
                operationTypeCell.Text = transactions.Transactions[i].OperationTypeName;

                detailsCell.Width = Unit.Pixel(80);

                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
                {
                    detailsCell.Text = "<a href=# onClick=\"repairCardClicked(" + transactions.Transactions[i].TransactionId + ");\" class=\"generateCardFixRedirectButton\" Title=\"Kart Onarım\"></a>";
                }

                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1")
                {
                    if (String.IsNullOrEmpty(transactions.Transactions[i].CardId))
                    {
                        //detailsCell.Text = "<a href=# onClick=\"repairCardClicked(" + transactions.Transactions[i].TransactionId + ");\" class=\"generateCardFixRedirectButton\" Title=\"Kart Onarım\"></a>";
                    }
                    else
                    {
                        if (transactions.Transactions[i].ProcessStatus.Contains("Para") || transactions.Transactions[i].ProcessStatus.Contains("Yükleme")
                            || transactions.Transactions[i].ProcessStatus.Contains("Kod") || transactions.Transactions[i].ProcessStatus.Contains("Ödemeyi"))
                        {
                            detailsCell.Text = detailsCell.Text + "<a href=# onClick=\"openNewTab(" + transactions.Transactions[i].TransactionId + ");\" class=\"generateCondRedirectButton\" Title=\"Kurum İşlemleri\"></a>"
                                + "<a href=# onClick=\"generateAskiCodeClicked(" + transactions.Transactions[i].TransactionId + ");\" class=\"generateCodeButton\" Title=\"Para Kod Üret\"></a>"
                                + "<a href=# onClick=\"openNewTabMoneyCode('" + transactions.Transactions[i].AboneNo.ToString().Trim() + "');\" class=\"generateMoneyCodeRedirectButton\" Title=\"Para Kod Kontrolü\"></a>";
                       
                        }
                        //else
                           // detailsCell.Text = "<a href=# onClick=\"generateAskiCodeClicked(" + transactions.Transactions[i].TransactionId + ");\" class=\"generateCodeButton\" Title=\"Para Kod Üret\"></a>";
                       
                    }
                }
                else
                {
                    if (transactions.Transactions[i].ProcessStatus.Contains("Para") || transactions.Transactions[i].ProcessStatus.Contains("Yükleme") || transactions.Transactions[i].ProcessStatus.Contains("Ödemeyi"))
                        detailsCell.Text = detailsCell.Text + "<a href=# onClick=\"openNewTab(" + transactions.Transactions[i].TransactionId + ");\" class=\"generateCondRedirectButton\" Title=\"Kurum İşlemleri\"></a>"
                            + "<a href=# onClick=\"openNewTabMoneyCode(" + transactions.Transactions[i].AboneNo + ");\" class=\"generateMoneyCodeRedirectButton\" Title=\"Para Kod Kontrolü\"></a>";
                }





                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        fakeCell1,
                        fakeCell2,
                        fakeCell3,
                        fakeCell4,
                        fakeCell5,
                        fakeCell6,
                        fakeCell7,
                        fakeCell01,
                        idCell,
						kioskNameCell,
                        customerNameCell,
                        aboneNoCell,
                        transactionDateCell,
                        transactionStatusCell,
                        operationTypeCell,
                        instutionNameCell,
                        detailsCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (transactions.recordCount < currentRecordEnd)
                currentRecordEnd = transactions.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + transactions.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(transactions.pageCount, this.pageNum, transactions.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;

 
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "TransactionId":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    TransactionId.Text = "<a>İşlem Id    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "KioskName":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CustomerName":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        CustomerName.Text = "<a>Müşteri Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        CustomerName.Text = "<a>Müşteri Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    CustomerName.Text = "<a>Müşteri Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "TransactionDate":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    TransactionDate.Text = "<a>İşlem Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "TransactionStatus":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        TransactionStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        TransactionStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    TransactionStatus.Text = "<a>İşlem Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Instution":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        Instution.Text = "<a>Kurum    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        Instution.Text = "<a>Kurum    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    Instution.Text = "<a>Kurum    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "AboneNo":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        AboneNo.Text = "<a>Abone No    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        AboneNo.Text = "<a>Abone No    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    AboneNo.Text = "<a>Abone No    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "OperationType":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        OperationType.Text = "<a>İşlem Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    OperationType.Text = "<a>İşlem Tipi   <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;
            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            //ViewState["NumberOfItem"] = this.numberOfItemField.Text;
            int recordCount = 0;
            int pageCount = 0;
            int custId = 0;

            if (Convert.ToInt32(Request.QueryString["customerId"]) != 0)
            {
                custId = Convert.ToInt32(Request.QueryString["customerId"]);
            }

             string date = startDateField.Text;
             
             DateTime date1 = new DateTime(2016, 1, 1, 0, 0, 0);
             DateTime date2 = new DateTime(2016, 1, 2, 0, 0, 0);
             
             if (Convert.ToDateTime(date) >= date1 && Convert.ToDateTime(date) < date2)
             {
                 startDateField.Text = "2016-01-01 00:00:00";
             }


             string dateEnd = endDateField.Text;

             DateTime date1End = new DateTime(2016, 1, 1, 0, 0, 0);
             DateTime date2End = new DateTime(2016, 1, 2, 0, 0, 0);

             if (Convert.ToDateTime(dateEnd) >= date1End && Convert.ToDateTime(dateEnd) < date2End)
             {
                 endDateField.Text = "2016-01-01 00:00:00";
             }

            CallWebServices callWebServ = new CallWebServices();
            //callWebServ.CallListTransactionDetailsService(11823994, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            ParserListTransactions transactions = callWebServ.CallListTransactionService(this.searchTextField.Text.Trim(),
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             custId,
                                                                             kioskIds,
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             Convert.ToInt32(this.operationTypeBox.SelectedValue),
                                                                             this.TransactionStatusBox.SelectedValue,
                                                                             this.LoginDatas.AccessToken, 
                                                                             this.LoginDatas.User.Id,
                                                                             1);
            

            if (transactions != null)
            {
                if (transactions.errorCode == 0)
                {
                    this.BindListTable(transactions);
                }
                else if (transactions.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + transactions.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionSrch");
        }
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {

            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            int recordCount = 0;
            int pageCount = 0;
            int custId = 0;

            if (Convert.ToInt32(Request.QueryString["customerId"]) != 0)
            {
                custId = Convert.ToInt32(Request.QueryString["customerId"]);
            }

            CallWebServices callWebServ = new CallWebServices();
            ParserListTransactions lists = callWebServ.CallListTransactionService(this.searchTextField.Text.Trim(),
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             Convert.ToInt32(ViewState["OrderDesc"]),
                                                                             custId,
                                                                             kioskIds,
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             Convert.ToInt32(this.operationTypeBox.SelectedValue),
                                                                             this.TransactionStatusBox.SelectedValue,
                                                                             this.LoginDatas.AccessToken, 
                                                                             this.LoginDatas.User.Id,
                                                                            2);

            if (lists != null)
            {
                if (lists.errorCode == 0)
                {
                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    //exportExcell.ExportExcellByBlock(lists.Transactions,"İşlemler", null);

                    string[] headerNames = {  "İşlem No","Kiosk Adı", "Kiosk No","Kart No", "Abone","Abone No",
                                               "İşlem Tarihi",  "İşlem Durumu", "Kurum" ,"İşlem Tipi" };

                    exportExcell.ExportExcellByBlock(lists.Transactions, "Kiosk İşlem Raporu", headerNames);
                }
                else if (lists.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + lists.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelTransaction");
        }
    }
}