﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Other;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class Transaction_ShowKioskLocalLog : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex;
    private ParserListKioskName kiosks;

    protected void Page_Load(object sender, EventArgs e)
    {

        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Transaction/ListKioskLocalLog.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        //this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));


        if (!Page.IsPostBack)
        {
            //this.numberOfItemField.Text = numberOfItemsPerPage.ToString();
            CallWebServices callWebServ = new CallWebServices();


            ViewState["OrderColumn"] = 4;
            ViewState["OrderDesc"] = 1;



            // instutionBox.Attributes.Add("disabled", "true");
            // kioskType.Attributes.Add("disabled", "true");


            this.pageNum = 0;
            //this.SearchOrder(4, 1);
        }
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        try
        {

            CallWebServices callWebServ = new CallWebServices();

            ParserOperation parserSaveKioskReferenceSystem = callWebServ.CallSaveKioskLocalLog(Convert.ToInt32(transactionId.Text),
                                                                                              this.LoginDatas.AccessToken,
                                                                                              this.LoginDatas.User.Id);



              if (parserSaveKioskReferenceSystem != null)
              {

                  string scriptText = "";

                  //this.messageArea.InnerHtml = parserSaveControlCashNotification.errorDescription;

                  scriptText = "parent.showMessageWindowKioskLocalLog('İşlem Başarılı.');";
                  ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);

              }
              else if (parserSaveKioskReferenceSystem.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
              {
                  Session.Abandon();
                  Session.RemoveAll();
                  Response.Redirect("../root/Login.aspx", true);
              }
              else
              {
                  this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
                  ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('Sisteme Erişilemiyor !'); ", true);
              }
             
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveKioskLocalLog");
        }

    }
}