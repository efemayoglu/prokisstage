﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Transaction_ListKioskLocalLogDetails : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int localLogID;
    private int operationId;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 1;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];


        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Transaction/ListKioskLocalLog.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }

        //this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        this.localLogID = Convert.ToInt32(Request.QueryString["transactionID"]);
  
        this.numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            ViewState["OrderColumn"] = 1;
            ViewState["OrderDesc"] = 1;

            this.SearchOrder();
        }
    }


    private void SearchOrder()
    {
        try
        {
           
            CallWebServices callWebService = new CallWebServices();
            ParserListKioskLocalLogDetail listKioskMoney = callWebService.CallListKioskLocalLogDetailService(localLogID, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (listKioskMoney != null)
            {
                if (listKioskMoney.errorCode == 0)
                {
                    this.BindListTable(listKioskMoney);
                }
                else if (listKioskMoney.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + listKioskMoney.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskConditionSrch");

        }

    }

    private void BindListTable(ParserListKioskLocalLogDetail list)
    {
        try
        {
            double sumAll = 0;
            double countAll = 0;

            // ParaÜstü 
            TableRow[] Row = new TableRow[list.ParserListKioskLocalLogDetailList.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();

                TableCell indexCell = new TableCell();
                TableCell insertionDateCell = new TableCell();
                TableCell recordInsertionDateCell = new TableCell();
                TableCell detailCell = new TableCell();
                TableCell totalAmountCell = new TableCell();

                TableCell sumCell = new TableCell();

                indexCell.CssClass = "inputTitleCell4";
                insertionDateCell.CssClass = "inputTitleCell4";
                recordInsertionDateCell.CssClass = "inputTitleCell4";
                detailCell.CssClass = "inputTitleCell4";
                totalAmountCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                insertionDateCell.Text = list.ParserListKioskLocalLogDetailList[i].InsertionDate;
                recordInsertionDateCell.Text = list.ParserListKioskLocalLogDetailList[i].RecordInsertionDate;
                detailCell.Text = list.ParserListKioskLocalLogDetailList[i].Detail.ToString();
                totalAmountCell.Text = list.ParserListKioskLocalLogDetailList[i].TotalAmount.ToString() + "  TL";


                Row[i].Cells.AddRange(new TableCell[]{
						insertionDateCell,
						recordInsertionDateCell,
                        detailCell,
                        totalAmountCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }


            this.itemsTable.Rows.AddRange(Row);


            /*
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(list.pageCount, this.pageNum, list.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);



            //this.itemsTable.Rows.AddRange(Row);

            TableRow addNewRow = new TableRow();
            TableCell totalMoneyTextCell = new TableCell();
            TableCell totalMoneyCell = new TableCell();
            TableCell spaceCell = new TableCell();
            TableCell countCell = new TableCell();

            spaceCell.CssClass = "inputTitleCell3";
            spaceCell.ColumnSpan = (3);

            totalMoneyTextCell.CssClass = "inputTitleCell3";
            totalMoneyCell.CssClass = "inputTitleCell3";

            countCell.CssClass = "inputTitleCell3";
            countCell.Text = countAll.ToString() + "  Adet";

            spaceCell.Text = " ";


            spaceCell.Text = "Toplam Miktar:";
            totalMoneyCell.Text = sumAll + " TL ";

            addNewRow.Cells.Add(spaceCell);
            addNewRow.Cells.Add(countCell);
            //addNewRow.Cells.Add(totalMoneyTextCell);
            addNewRow.Cells.Add(totalMoneyCell);

            this.itemsTable.Rows.Add(addNewRow);



            //////////////////////////////
            // ParaÜstü Reject 
            sumAll = 0;
            countAll = 0;

            TableRow[] RowReject = new TableRow[4];
            int startIndex2 = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < RowReject.Length; i++)
            {
                RowReject[i] = new TableRow();

                TableCell indexCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell cashTypeCell = new TableCell();
                TableCell cashCountCell = new TableCell();

                TableCell sumCell = new TableCell();

                indexCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                cashTypeCell.CssClass = "inputTitleCell4";
                cashCountCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                kioskNameCell.Text = list.cashCount[i + 8].KioskName;
                cashTypeCell.Text = list.cashCount[i + 8].cashTypeName;
                cashCountCell.Text = list.cashCount[i + 8].cashCount.ToString() + "  Adet";




                string[] words = cashTypeCell.Text.Split(' ');

                // O satıra ait toplam (sumCell)
                if (cashTypeCell.Text.Contains("Krs"))
                {
                    sumCell.Text = (((Convert.ToDouble(words[0])) / 100 * (list.cashCount[i + 8].cashCount)) / 100).ToString();

                    sumCell.Text = sumCell.Text + " TL";

                }
                else
                {

                    sumCell.Text = ((Convert.ToDouble(words[0]) * (list.cashCount[i + 8].cashCount)) / 100).ToString() + " TL";
                }

                // Genel Toplam (sumAll)
                if (cashTypeCell.Text.Contains("Krs"))
                {
                    sumAll = ((Convert.ToDouble(words[0]) / 100 * (list.cashCount[i + 8].cashCount)) / 100) + sumAll;
                }
                else
                {
                    sumAll = ((Convert.ToDouble(words[0]) * (list.cashCount[i + 8].cashCount)) / 100) + sumAll;
                }


                countAll = (list.cashCount[i + 8].cashCount) + countAll;

                RowReject[i].Cells.AddRange(new TableCell[]{
						indexCell,
						kioskNameCell,
                        cashTypeCell,
                        cashCountCell,
                        sumCell
					});

                if (i % 2 == 0)
                    RowReject[i].CssClass = "listrow";
                else
                    RowReject[i].CssClass = "listRowAlternate";
            }
            this.itemsTable2.Rows.AddRange(RowReject);


            TableRow addNewRow2 = new TableRow();
            TableCell totalMoneyTextCell2 = new TableCell();
            TableCell totalMoneyCell2 = new TableCell();
            TableCell spaceCell2 = new TableCell();
            TableCell countCell2 = new TableCell();

            spaceCell2.CssClass = "inputTitleCell3";
            spaceCell2.ColumnSpan = (3);

            totalMoneyTextCell2.CssClass = "inputTitleCell3";
            totalMoneyCell2.CssClass = "inputTitleCell3";

            countCell2.CssClass = "inputTitleCell3";
            countCell2.Text = countAll.ToString() + "  Adet";

            spaceCell2.Text = " ";


            spaceCell2.Text = "Toplam Miktar:";
            totalMoneyCell2.Text = sumAll + " TL ";

            addNewRow2.Cells.Add(spaceCell2);
            addNewRow2.Cells.Add(countCell2);
            //addNewRow.Cells.Add(totalMoneyTextCell);
            addNewRow2.Cells.Add(totalMoneyCell2);

            this.itemsTable2.Rows.Add(addNewRow2);


            this.itemsTable.Visible = true;
             * 
             * */
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskMoneyDetailBDT");
        }
    }

}