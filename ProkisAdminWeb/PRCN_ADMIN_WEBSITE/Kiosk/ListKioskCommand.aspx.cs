﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Kiosk_ListKioskCommand : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Kiosk/ListKioskCommand.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.startDateField.Text = DateTime.Now.ToString("yyyy-MM-dd")+" 00:00:00";
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00";

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {

                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }


            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {
                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();
            }


            ListItem item = new ListItem();
            item.Text = "Kurum Seçiniz";
            item.Value = "0";
            this.instutionBox.Items.Add(item);
            this.instutionBox.SelectedValue = "0";



            ViewState["OrderColumn"] = 3;
            ViewState["OrderDesc"] = 1;

            this.pageNum = 0;
            this.SearchOrder(3, 1);
        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(3, 1);
    }

    private void BindAddButton()
    {
        if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
        {
            TableRow addNewRow = new TableRow();
            TableCell addNewCell = new TableCell();
            TableCell spaceCell = new TableCell();
            spaceCell.CssClass = "inputTitleCell4";
            spaceCell.ColumnSpan = (11);
            addNewCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editButtonClicked('0');\" class=\"anylink\"><img src=../images/add.png border=0 title=\"Komut Ekle\" /></a>";
            addNewRow.Cells.Add(spaceCell);
            addNewRow.Cells.Add(addNewCell);
            this.itemsTable.Rows.Add(addNewRow);
        }
    }

    private void BindListTable(ParserListKioskCommands commands)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            TableRow[] Row = new TableRow[commands.KioskCommands.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("commandId", commands.KioskCommands[i].CommandId.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell createdUserCell = new TableCell();
                TableCell createdDateCell = new TableCell();
                TableCell commandTypeCell = new TableCell();
                TableCell commandStringCell = new TableCell();
                TableCell executedDateCell = new TableCell();
                TableCell statusCell = new TableCell();
                TableCell detailsCell = new TableCell();
                TableCell commandReason = new TableCell();
                TableCell explanation = new TableCell();
                TableCell institutionCell = new TableCell();
                

                TableCell fakeCell1 = new TableCell();
                TableCell fakeCell2 = new TableCell();
                TableCell fakeCell3 = new TableCell();
                TableCell fakeCell4 = new TableCell();
                TableCell fakeCell5 = new TableCell();
                TableCell fakeCell6 = new TableCell();
                TableCell fakeCell7 = new TableCell();

                fakeCell1.CssClass = "inputTitleCell4";
                fakeCell2.CssClass = "inputTitleCell4";
                fakeCell3.CssClass = "inputTitleCell4";
                fakeCell4.CssClass = "inputTitleCell4";
                fakeCell5.CssClass = "inputTitleCell4";
                fakeCell6.CssClass = "inputTitleCell4";
                fakeCell7.CssClass = "inputTitleCell4";

                fakeCell1.Visible = false;
                fakeCell2.Visible = false;
                fakeCell3.Visible = false;
                fakeCell4.Visible = false;
                fakeCell5.Visible = false;
                fakeCell6.Visible = false;
                fakeCell7.Visible = false;

                indexCell.CssClass  = "inputTitleCell4";
                idCell.CssClass     = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                createdUserCell.CssClass = "inputTitleCell4";
                createdDateCell.CssClass = "inputTitleCell4";
                commandTypeCell.CssClass = "inputTitleCell4";
                commandStringCell.CssClass = "inputTitleCell4";
                executedDateCell.CssClass = "inputTitleCell4";
                statusCell.CssClass = "inputTitleCell4";
                detailsCell.CssClass = "inputTitleCell4";
                commandReason.CssClass = "inputTitleCell4";
                explanation.CssClass = "inputTitleCell4";
                institutionCell.CssClass = "inputTitleCell4";

                detailsCell.Width = Unit.Pixel(75);
                kioskNameCell.HorizontalAlign = HorizontalAlign.Left;
                kioskNameCell.Style.Add("padding-left", "5px");
                idCell.Visible = false;
                indexCell.Text = (startIndex + i).ToString();
                idCell.Text = commands.KioskCommands[i].CommandId.ToString();

                //kioskNameCell.Text = commands.KioskCommands[i].KioskName;
                kioskNameCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editButtonClicked(" + commands.KioskCommands[i].CommandId + ");\" class=\"anylink\">" + commands.KioskCommands[i].KioskName + "</a>";
                
                createdUserCell.Text = commands.KioskCommands[i].CreatedUserName;
                createdDateCell.Text = commands.KioskCommands[i].CreatedDate.ToString();
                commandTypeCell.Text = commands.KioskCommands[i].CommandType;
                commandStringCell.Text = commands.KioskCommands[i].CommandString;
                executedDateCell.Text = commands.KioskCommands[i].ExecutedDate.ToString();
                statusCell.Text = commands.KioskCommands[i].Status;
                institutionCell.Text = commands.KioskCommands[i].Institution;

                commandReason.Text = commands.KioskCommands[i].commandReason;
                explanation.Text = commands.KioskCommands[i].explanation;


                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanDelete == "1")
                {
                    if (commands.KioskCommands[i].Status == "Komut Yürütülmeyi Bekliyor")
                    {
                        detailsCell.Text = "<a href=\"javascript: deleteButtonClicked('[pk].[delete_kiosk_command]'," + commands.KioskCommands[i].CommandId + ");\" class=\"deleteButton\" title=\"Komut Sil\"></a>";
                    }
                    else
                    {
                        detailsCell.Text = " ";
                    }
                }
                else
                {
                    detailsCell.Text = " ";
                }
                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
						idCell,
                        fakeCell1,
                        fakeCell2,
                        fakeCell3,
                        fakeCell4,
                        fakeCell5,
                        fakeCell6,
                        fakeCell7,
						 kioskNameCell,
                         createdUserCell,
                         createdDateCell,
                         institutionCell,
                         commandTypeCell,
                         commandStringCell,
                         executedDateCell,
                         statusCell,
                         commandReason,
                         explanation,
                         detailsCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);

            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (commands.recordCount < currentRecordEnd)
                currentRecordEnd = commands.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + commands.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(commands.pageCount, this.pageNum, commands.recordCount);
            pagingRow.Cells.Add(pagingCell);

            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;

            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
            {
                TableRow addNewRow = new TableRow();
                TableCell addNewCell = new TableCell();
                TableCell spaceCell = new TableCell();
                spaceCell.CssClass = "inputTitleCell4";
                spaceCell.ColumnSpan = (this.itemsTable.Rows[1].Cells.Count - 1);
                addNewCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editButtonClicked('0');\" class=\"anylink\"><img src=../images/add.png border=0 title=\"Add New...\" /></a>";
                addNewRow.Cells.Add(spaceCell);
                addNewRow.Cells.Add(addNewCell);
                this.itemsTable.Rows.Add(addNewRow);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskCommandBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }



    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;


            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);
            int recordCount = 0;
            int pageCount = 0;
 
            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskCommands commands = callWebServ.CallListKioskCommandService(this.searchTextField.Text,
                                                                                       Convert.ToDateTime(this.startDateField.Text),
                                                                                       Convert.ToDateTime(this.endDateField.Text),
                                                                                       kioskIds,
                                                                                       Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                                       Convert.ToInt32(this.numberOfItemField.Text),
                                                                                       this.pageNum,
                                                                                       recordCount,
                                                                                       pageCount,
                                                                                       orderSelectionColumn,
                                                                                       Convert.ToInt32(ViewState["OrderDesc"]),
                                                                                       this.LoginDatas.AccessToken,
                                                                                       this.LoginDatas.User.Id,
                                                                                       2);

            if (commands != null)
            {
                if (commands.errorCode == 0)
                {

                    ExportExcellDatas exportExcell = new ExportExcellDatas();

                    string[] headerNames = { "Komut No", "Kiosk Adı", "Oluşturan Kullanıcı", "Oluşturulma Zamanı", "Komut Tipi", "Komut",
                                               "Yürütülme Zamanı", "Durumu", "Komut Oluşturma Nedeni", "Açıklama", "CommandReasonId","CommandTypeId","KioskId","Kurum","Null"};

                    exportExcell.ExportExcellByBlock(commands.KioskCommands, "Kiosk Komut Raporu", headerNames);
                }
                else if (commands.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + commands.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelMutabakat");
        }
    }

    protected void deleteButton_Click(object sender, EventArgs e)
    {
        //MerchantOpr merchantOpr = new MerchantOpr();

        //merchantOpr.DeleteMerchant(Convert.ToInt32(this.kioskIDRefField.Value));
        //merchantOpr.Dispose();

        this.SearchOrder(0, 1);
    }




    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "Name":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // Name.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Name.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Name.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CreatedUser":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // CreatedUser.Text = "<a>Oluşturan Kullanıcı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                       // CreatedUser.Text = "<a>Oluşturan Kullanıcı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                   // CreatedUser.Text = "<a>Oluşturan Kullanıcı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CreatedDate":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // CreatedDate.Text = "<a>Oluşturulma Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                       // CreatedDate.Text = "<a>Oluşturulma Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //CreatedDate.Text = "<a>Oluşturulma Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CommandType":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // CommandType.Text = "<a>Komut Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                       // CommandType.Text = "<a>Komut Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //CommandType.Text = "<a>Komut Tipi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CommandString":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //CommandString.Text = "<a>Komut    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //CommandString.Text = "<a>Komut    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //CommandString.Text = "<a>Komut    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "ExecutedDate":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //ExecutedDate.Text = "<a>Yürütülme Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                       // ExecutedDate.Text = "<a>Yürütülme Zamanı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //ExecutedDate.Text = "<a>Yürütülme Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Status":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // Status.Text = "<a>Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                       // Status.Text = "<a>Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Status.Text = "<a>Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "CommandReason":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // Status.Text = "<a>Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Status.Text = "<a>Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Status.Text = "<a>Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Explanation":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // Status.Text = "<a>Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Status.Text = "<a>Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Status.Text = "<a>Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Institution":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // Status.Text = "<a>Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Status.Text = "<a>Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Status.Text = "<a>Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;


            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;


            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskCommands commands = callWebServ.CallListKioskCommandService(this.searchTextField.Text,
                                                                                       Convert.ToDateTime(this.startDateField.Text),
                                                                                       Convert.ToDateTime(this.endDateField.Text),
                                                                                       kioskIds,
                                                                                       Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                                       Convert.ToInt32(this.numberOfItemField.Text),
                                                                                       this.pageNum,
                                                                                       recordCount,
                                                                                       pageCount,
                                                                                       orderSelectionColumn,
                                                                                       descAsc, 
                                                                                       this.LoginDatas.AccessToken, 
                                                                                       this.LoginDatas.User.Id,
                                                                                       1);
            if (commands != null)
            {
                if (commands.errorCode == 0)
                {
                    this.BindListTable(commands);
                }
                else if (commands.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else if (commands.errorCode == 2)
                {
                    this.BindAddButton();
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + commands.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskCommandSrch");
        }
    }
}