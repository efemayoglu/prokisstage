﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webctrls/AdminSiteMPage.master" AutoEventWireup="true" CodeFile="ListKioskNew.aspx.cs" Inherits="Kiosk_ListKioskNew" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<%@ Register Namespace="ClickableWebControl" TagPrefix="clkweb" %>
<%@ Register Src="../MS_Control/MultipleSelection.ascx" TagName="MultipleSelection" TagPrefix="uc1" %>
 

<asp:Content ID="pageContent" ContentPlaceHolderID="mainCPHolder" runat="server">
    <input type="hidden" id="pageNumRefField" runat="server" name="pageNumRefField" value="0" />
    <asp:Button ID="navigateButton" runat="server" OnClick="navigateButton_Click" CssClass="dummy" />
    <asp:Button ID="deleteButton" runat="server" CssClass="dummy"
        OnClick="deleteButton_Click" />

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { setInterval(function () { cache_clear() }, 120000); });
        function cache_clear() { window.location.reload(true); }
    </script>
    <script language="javascript" type="text/javascript">

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function deleteButtonClicked(storedProcedure, itemId) {
            var retVal = showDeleteWindowKiosk(storedProcedure, itemId);
        }

        function searchButtonClicked(sender) {
            document.getElementById('pageNumRefField').value = "0";
            return true;
        }

        function editButtonClicked(itemID) {
            var retVal = showKioskWindow(itemID);
        }

        function postForPaging(selectedPageNum) {
            document.getElementById('pageNumRefField').value = selectedPageNum;
            document.getElementById('navigateButton').click();
        }

        function validateNo(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }



    </script>
    <script type="text/javascript" language="javascript" src="../js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery.tablednd_0_5.js"></script>

    <asp:HiddenField ID="idRefField" runat="server" Value="0" />
    <asp:HiddenField ID="oldOrderNumberField" runat="server" Value="0" />
    <asp:HiddenField ID="newOrderNumberField" runat="server" Value="0" />
    <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>

    <div align="center">
        <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp;</div>
        <asp:Panel ID="panel1" DefaultButton="searchButton" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto">
            <table border="0" cellpadding="0" cellspacing="0" id="Table2" runat="server" width="90%">
                <tr style="padding-bottom: 12px; padding-top: 12px;">


                    <td align="left" class="inputTitle" style="width: 177px; text-align: left; height: 30px;">
                        <asp:Label ID="Label2" runat="server" Visible="true">Kiosk Tipi:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 140px">
                        <asp:ListBox ID="kioskTypeBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>
 

                    <td style="width: 6px">&nbsp;&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 177px; text-align: left; height: 30px;">
                        <asp:Label ID="Label3" runat="server" Visible="true">Kurum:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px; height: 30px;">
                        <asp:ListBox ID="instutionBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>


                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 79px; text-align: left">
                        <asp:Label ID="mercLabel" runat="server" Visible="true">Kiosk:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 200px">
                        <uc1:MultipleSelection ID="MultipleSelection1" runat="server" CssClass="inputLine_100x20" /></td>
                  


                    <td style="width: 6px">&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 177px; text-align: left; height: 30px;">
                        <asp:Label ID="Label4" runat="server" Visible="true">Lokasyon Türü:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px; height: 30px;">
                        <asp:ListBox ID="regionBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>



                    <td style="width: 6px">&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 177px; text-align: left; height: 30px;">
                        <asp:Label ID="Label10" runat="server" Visible="true">Risk Sınıfı:</asp:Label></td>
                                        <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px; height: 30px;">
                        <asp:ListBox ID="riskClassBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>



                     <td style="width: 6px">&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 78px;">&nbsp;Kiosk Ara:
                    </td>
                    <td align="left" style="width: 140px">
                        <asp:TextBox ID="searchTextField" CssClass="inputLine_150x20_2" runat="server"></asp:TextBox>
                    </td>


 
                </tr>

                <tr>


                    <td align="left" class="inputTitle" style="width: 177px; text-align: left; height: 30px;">
                        <asp:Label ID="Label6" runat="server" Visible="true">Bağlantı Tipi:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 140px">
                        <asp:ListBox ID="connectionBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>
 

                    <td style="width: 6px">&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 177px; text-align: left; height: 30px;">
                        <asp:Label ID="Label7" runat="server" Visible="true">Kart Okuyucu Tipi:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px; height: 30px;">
                        <asp:ListBox ID="cardReaderBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>

                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 79px; text-align: left">
                        <asp:Label ID="Label8" runat="server" Visible="true">Kiosk Durum:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                     <td style="width: 200px">
                        <asp:ListBox ID="statusBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>

                    <td style="width: 6px">&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 177px; text-align: left; height: 30px;">
                        <asp:Label ID="Label9" runat="server" Visible="true">Şehir:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px; height: 30px;">
                        <asp:ListBox ID="cityBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>

                    <td style="width: 6px">&nbsp;</td>
                    <td style="width: 86px">
                        <asp:Button ID="searchButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                            OnClick="searchButton_Click" Text="Ara"></asp:Button>
                    </td>


                </tr>
            </table>
        </asp:Panel>
    </div>
    <div>&nbsp;</div>
    <div align="center">
        <asp:Panel ID="ListPanel" runat="server" CssClass="containerPanel_95Pxauto">
            <div align="left" class="windowTitle_container_autox30">
                Kiosklar<hr style="border-bottom: 1px solid #b2b2b4;" />
            </div>
            <table cellpadding="0" cellspacing="0" width="95%" id="upTable" runat="server">
                <tr style="height: 20px">
                    <td style="width: 22px;">
                        <asp:ImageButton ID="excellButton" runat="server" ClientIDMode="Static" OnClick="excelButton_Click" ImageUrl="~/images/excel.jpg" />
                    </td>
                    <td style="width: 8px;">&nbsp;</td>
                    <td style="width: 200px;">
                        <asp:Label ID="recordInfoLabel" runat="server" Style="width: 200px; height: 20px; text-align: left" CssClass="data"></asp:Label>
                    </td>
                    <td style="height: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td style="height: 20px; width: 130px; text-align: right">
                        <asp:Label ID="Label1" Text="Listelenecek Kayıt Sayısı :  " runat="server" Style="width: 200px; height: 20px; text-align: left" CssClass="data"></asp:Label>
                    </td>
                    <td align="right" style="height: 20px; width: 33px">
                        <asp:TextBox ID="numberOfItemField" CssClass="inputLine_30x20_s" MaxLength="3" runat="server" onkeypress='validateNo(event)'></asp:TextBox></td>


                </tr>
                <tr>
                    <td align="center" class="tableStyle1" colspan="6">
                        <asp:UpdatePanel ID="ListUPanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" RenderMode="Block">
                            <ContentTemplate>
                                <asp:Table ID="itemsTable" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                    BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="100%">
                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="#"></asp:TableHeaderCell>
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kiosk Adı    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Name" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Başarılı İşlem    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="AccomplishedTransactions" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Çalışma Durumu    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="LastAppAliveTime" />
                                        <%--<clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Açıklama    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Explanation" />--%>
                                        <%--<clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kiosk Adres    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Address" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Şehir   <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="City" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Lokasyan Türü     <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Region" />--%>
                                        <%--<clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Lokasyan Türü    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="LocationType" />--%>
                                        <%--<clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Enlem    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Latitude" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Boylam    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Longitude" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kiosk IP    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Ip" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kiosk Durum    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Status" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kiosk Tipi    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Type" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kurum <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Instution" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Bağlantı Tipi    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="ConnectionType" />--%>
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kart Okuyucu Tipi    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="CardReaderType" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Risk Sınıfı    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="RiskClass" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kullanım Ücreti    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="KioskFee" />
                                        <%--<clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Eklenme Tarihi    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="InsertionDate" />--%>


                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Çalışma Periyodu  <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="AppAliveDurationTime" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Makbuz Alarm Durumu  <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="MakbuzStatus" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="JCM Dolum Durumu  <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="JCMStatus" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kredi Kartı  <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="CreditCard" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Mobil Ödeme  <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="MobilePayment" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kapasite Durumu  <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Capacity" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Versiyon    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="AppVersion" />
                                    </asp:TableRow>
                                </asp:Table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="deleteButton" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="navigateButton" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <br />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <br />
    <table width="95%">
        <tr align="right">
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="width: 80px">
                <asp:Label ID="Label5" runat="server" Text="powered by" CssClass="poweredbyTitle"></asp:Label>

            </td>
            <td style="width: 100px">
                <asp:LinkButton ID="LinkButton1" href="http://www.birlesikodeme.com/" runat="server" Text="BİRLEŞİK ÖDEME" CssClass="procenneTitle"></asp:LinkButton>

            </td>
        </tr>
    </table>
</asp:Content>

