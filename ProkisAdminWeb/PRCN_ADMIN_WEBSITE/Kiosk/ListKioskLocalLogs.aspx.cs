﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Kiosk;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Kiosk_ListKioskLocalLogs : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int ownSubMenuIndex = -1;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Kiosk/ListKioskLocalLogs.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.startDateField.Text = DateTime.Now.AddDays(-0).ToString("yyyy-MM-dd") + " 00:00:00";
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00";

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {

                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }

            ParserListProcessType processType = callWebServ.CallGetProcessTypeFromLogs(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (processType != null)
            {

                this.ProcessType.DataSource = processType.parserProcessType;
                this.ProcessType.DataBind();

            }
            ListItem item2 = new ListItem();
            item2.Text = "İşlem Tipi Seçiniz";
            item2.Value = "0";
            this.ProcessType.Items.Add(item2);
            this.ProcessType.SelectedValue = "0";

            this.pageNum = 0;
            ViewState["OrderColumn"] = this.orderSelectionColumn;
            ViewState["OrderDesc"] = this.orderSelectionDescAsc;
            SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
           
        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "KioskName":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                       // KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                   // KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "LogTime":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // ExpertUser.Text = "<a>Yetkili Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //ExpertUser.Text = "<a>Yetkili Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //ExpertUser.Text = "<a>Yetkili Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "LogDetail":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TotalAmout.Text = "<a>Para Miktarı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TotalAmout.Text = "<a>Para Miktarı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TotalAmout.Text = "<a>Para Miktarı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
           
            
            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;
        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;
        ExportToExcel(this.orderSelectionColumn, this.orderSelectionDescAsc);
       
        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            //ViewState["NumberOfItem"] = this.numberOfItemField.Text;
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListLocalLogs logs = callWebServ.CallListLocalLogsService(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc, 
                                                                             kioskIds,
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             Convert.ToInt32(this.ProcessType.SelectedValue),
                                                                             2);

            if (logs != null)
            {
                if (logs.errorCode == 0)
                {

                    ExportExcellDatas exportExcell = new ExportExcellDatas();

                    string[] headerNames = { "Kiosk Adı	", "Log Zamanı", "Log Detay" };
                    exportExcell.ExportExcellByBlock(logs.LocalLogs, "Kiosk Yerel Log Raporu", headerNames);
                }
                else if (logs.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + logs.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelMutabakat");
        }
    }

    private void BindListTable(ParserListLocalLogs localLogs)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            TableRow[] Row = new TableRow[localLogs.LocalLogs.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("LocalLogId", localLogs.LocalLogs[i].logId);

                TableCell indexCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell logDateCell = new TableCell();
                TableCell logDetailCell = new TableCell();
                indexCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                logDateCell.CssClass = "inputTitleCell4";
                logDetailCell.CssClass = "inputTitleCell4";
                logDetailCell.HorizontalAlign = HorizontalAlign.Left;
                logDetailCell.Width = Unit.Pixel(500);

                indexCell.Text = (startIndex + i).ToString();        
                kioskNameCell.Text = localLogs.LocalLogs[i].kioskName;
                logDateCell.Text = localLogs.LocalLogs[i].logDate;
                if(localLogs.LocalLogs[i].logDetail.Contains("EncryptedData"))
                {
                    string text=localLogs.LocalLogs[i].logDetail;
                    logDetailCell.Text = text.Substring(1, text.Length - text.IndexOf("EncryptedData"));
                }
                else
                logDetailCell.Text = localLogs.LocalLogs[i].logDetail;
       

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
						kioskNameCell,
                        logDateCell,
                        logDetailCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (localLogs.recordCount < currentRecordEnd)
                currentRecordEnd = localLogs.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + localLogs.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(localLogs.pageCount, this.pageNum, localLogs.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;
        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            //ViewState["NumberOfItem"] = this.numberOfItemField.Text;
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListLocalLogs logs = callWebServ.CallListLocalLogsService(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc, 
                                                                             kioskIds,
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             Convert.ToInt32(this.ProcessType.SelectedValue),
                                                                             1);


            if (logs != null)
            {
                if (logs.errorCode == 0)
                {
                    this.BindListTable(logs);
                }
                else if (logs.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + logs.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionSrch");
        }
    }
}