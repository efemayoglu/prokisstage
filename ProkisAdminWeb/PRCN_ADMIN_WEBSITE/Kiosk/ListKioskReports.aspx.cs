﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Kiosk;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Globalization;
using System.Web.UI.WebControls;

public partial class Kiosk_ListKioskReports : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Kiosk/ListKioskReports.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            this.startDateField.Text = DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00";
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00";

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {
                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }

            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {

                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();

            }

            ListItem item1 = new ListItem();
            item1.Text = "Kurum Seçiniz";
            item1.Value = "0";
            this.instutionBox.Items.Add(item1);
            this.instutionBox.SelectedValue = "0";

            ViewState["OrderColumn"] = 5;
            ViewState["OrderDesc"] = 1;

            this.pageNum = 0;
            this.SearchOrder(5, 1);
        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }

    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(5, 1);
    }
    private string ChangeMoneyFormat(string value)
    {
        value = value.Replace(',', '.');
        return String.Format(new System.Globalization.CultureInfo("tr-TR"), "{0:C}", Convert.ToDouble(value));
    }

    private void BindListTable(ParserListKioskReport list)
    {

        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            Double transactionTotal = 0, transactionTotalSub = 0;

            TableRow[] Row = new TableRow[list.KioskReports.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("kioskId", list.KioskReports[i].KioskId);

                TableCell indexCell = new TableCell();
                TableCell kioskIdCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell txnCountCell = new TableCell();
                TableCell generatedAskiAmountCell = new TableCell();
                TableCell usedAskiAmountCell = new TableCell();
                TableCell generatedAskiCountCell = new TableCell();
                TableCell usedAskiCountCell = new TableCell();
                TableCell receivedAmountCell = new TableCell();
                TableCell creditCardAmountCell = new TableCell();
                TableCell commisionAmountCell = new TableCell();

                TableCell givenMoneyCell = new TableCell();
                TableCell transactionAmount = new TableCell();
                //TableCell kioskEmptyAmountCell = new TableCell();
                TableCell earnedMoneyCell = new TableCell();
                TableCell successTxnCountCell = new TableCell();
                TableCell institutionAmount = new TableCell();

                TableCell fakeCell1 = new TableCell();
                TableCell fakeCell2 = new TableCell();
                TableCell fakeCell3 = new TableCell();
                TableCell fakeCell4 = new TableCell();
                TableCell fakeCell5 = new TableCell();
                TableCell fakeCell6 = new TableCell();
                TableCell fakeCell7 = new TableCell();
                TableCell fakeCell8 = new TableCell();
                TableCell fakeCell9 = new TableCell();
                TableCell fakeCell10 = new TableCell();
                TableCell fakeCell11 = new TableCell();
                TableCell fakeCell12 = new TableCell();

                fakeCell1.CssClass = "inputTitleCell4";
                fakeCell2.CssClass = "inputTitleCell4";
                fakeCell3.CssClass = "inputTitleCell4";
                fakeCell4.CssClass = "inputTitleCell4";
                fakeCell5.CssClass = "inputTitleCell4";
                fakeCell6.CssClass = "inputTitleCell4";
                fakeCell7.CssClass = "inputTitleCell4";
                fakeCell8.CssClass = "inputTitleCell4";
                fakeCell9.CssClass = "inputTitleCell4";
                fakeCell10.CssClass = "inputTitleCell4";
                fakeCell11.CssClass = "inputTitleCell4";
                fakeCell12.CssClass = "inputTitleCell4";

                fakeCell1.Visible = false;
                fakeCell2.Visible = false;
                fakeCell3.Visible = false;
                fakeCell4.Visible = false;
                fakeCell5.Visible = false;
                fakeCell6.Visible = false;
                fakeCell7.Visible = false;
                fakeCell8.Visible = false;
                fakeCell9.Visible = false;
                fakeCell10.Visible = false;
                fakeCell11.Visible = false;
                fakeCell12.Visible = false;

                indexCell.CssClass = "inputTitleCell4";
                kioskIdCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                txnCountCell.CssClass = "inputTitleCell4";
                generatedAskiAmountCell.CssClass = "inputTitleCell4";
                usedAskiAmountCell.CssClass = "inputTitleCell4";
                generatedAskiCountCell.CssClass = "inputTitleCell4";
                usedAskiCountCell.CssClass = "inputTitleCell4";
                receivedAmountCell.CssClass = "inputTitleCell4";
                givenMoneyCell.CssClass = "inputTitleCell4";
                transactionAmount.CssClass = "inputTitleCell4";
                //kioskEmptyAmountCell.CssClass = "inputTitleCell4";
                earnedMoneyCell.CssClass = "inputTitleCell4";
                successTxnCountCell.CssClass = "inputTitleCell4";
                institutionAmount.CssClass = "inputTitleCell4";
                creditCardAmountCell.CssClass = "inputTitleCell4";
                commisionAmountCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                kioskIdCell.Text = list.KioskReports[i].KioskId;
                kioskNameCell.Text = list.KioskReports[i].KioskName;
                txnCountCell.Text = list.KioskReports[i].TxnCount;
                successTxnCountCell.Text = list.KioskReports[i].SuccessTxnCount;
                generatedAskiAmountCell.Text = ChangeMoneyFormat(list.KioskReports[i].GeneratedAskiAmount);
                usedAskiAmountCell.Text = ChangeMoneyFormat(list.KioskReports[i].UsedAskiAmount);
                generatedAskiCountCell.Text = list.KioskReports[i].GeneratedAskiCount;
                usedAskiCountCell.Text = list.KioskReports[i].UsedAskiCount;
                receivedAmountCell.Text = ChangeMoneyFormat(list.KioskReports[i].ReceivedAmount);
                institutionAmount.Text = ChangeMoneyFormat(list.KioskReports[i].InstutionAmount);
                creditCardAmountCell.Text = list.KioskReports[i].CreditCardAmount;
                commisionAmountCell.Text = list.KioskReports[i].CommisionAmount;

                if (String.IsNullOrEmpty(list.KioskReports[i].ParaUstu))
                {
                    list.KioskReports[i].ParaUstu = "0";
                }

                givenMoneyCell.Text = ChangeMoneyFormat(list.KioskReports[i].ParaUstu);

                transactionTotalSub = Convert.ToDouble(list.KioskReports[i].ReceivedAmount.Replace(',', '.')) - Convert.ToDouble(list.KioskReports[i].ParaUstu.Replace(',', '.'));

                transactionAmount.Text = ChangeMoneyFormat(list.KioskReports[i].TotalAmount);
                //kioskEmptyAmountCell.Text =ChangeMoneyFormat(list.KioskReports[i].KioskEmptyAmount);
                earnedMoneyCell.Text = ChangeMoneyFormat(list.KioskReports[i].EarnedMoney);

                //transactionTotal = transactionTotal + Convert.ToDouble(list.KioskReports[i].TotalAmount.Replace(',', '.'));


                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        fakeCell1,
                        fakeCell2,
                        fakeCell3,
                        fakeCell4,
                        fakeCell5,
                        fakeCell6,
                        fakeCell7,
                        fakeCell8,
                        fakeCell9,
                        //fakeCell10,				
                        //kioskIdCell,
                        kioskNameCell,
                        txnCountCell,
                        successTxnCountCell,
                        generatedAskiAmountCell,
                        usedAskiAmountCell,
                        generatedAskiCountCell,
                        usedAskiCountCell,
                        receivedAmountCell,
                        creditCardAmountCell,
                        commisionAmountCell,
                        givenMoneyCell,
                        transactionAmount,
                        //kioskEmptyAmountCell,
                        institutionAmount,
                        earnedMoneyCell });

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (list.recordCount < currentRecordEnd)
                currentRecordEnd = list.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + list.recordCount.ToString() + " kayıt bulundu.";
            }

            TableRow addNewRow = new TableRow();
            TableCell totalTxnCountCell = new TableCell();
            TableCell totalSuccessTxnCountCell = new TableCell();
            TableCell totalGeneratedAskiAmountCell = new TableCell();
            TableCell totalUsedAskiAmountCell = new TableCell();
            TableCell totalGeneratedAskiCountCell = new TableCell();
            TableCell totalUsedAskiCountCell = new TableCell();
            TableCell totalReceivedAmountCell = new TableCell();
            TableCell totalGivenAmountCell = new TableCell();
            TableCell totalTransationCell = new TableCell();
            //TableCell totalKioskEmptyAmountCell = new TableCell();
            TableCell totalEarnedMoneyCell = new TableCell();
            TableCell totalInstitutionCell = new TableCell();
            TableCell spaceCell = new TableCell();
            TableCell totalCreditCardAmountCell = new TableCell();
            TableCell totalCommisionAmountCell = new TableCell();

            spaceCell.CssClass = "inputTitleCell99";
            spaceCell.ColumnSpan = (2);
            totalTxnCountCell.CssClass = "inputTitleCell99";
            totalSuccessTxnCountCell.CssClass = "inputTitleCell99";
            totalGeneratedAskiAmountCell.CssClass = "inputTitleCell99";
            totalUsedAskiAmountCell.CssClass = "inputTitleCell99";
            totalGeneratedAskiCountCell.CssClass = "inputTitleCell99";
            totalUsedAskiCountCell.CssClass = "inputTitleCell99";
            totalReceivedAmountCell.CssClass = "inputTitleCell99";
            totalGivenAmountCell.CssClass = "inputTitleCell99";
            totalTransationCell.CssClass = "inputTitleCell99";
            //totalKioskEmptyAmountCell.CssClass = "inputTitleCell99";
            totalEarnedMoneyCell.CssClass = "inputTitleCell99";
            totalInstitutionCell.CssClass = "inputTitleCell99";
            totalCreditCardAmountCell.CssClass = "inputTitleCell99";
            totalCommisionAmountCell.CssClass = "inputTitleCell99";

            spaceCell.Text = "Toplam";
            totalTxnCountCell.Text = list.TotalTxnCount;
            totalSuccessTxnCountCell.Text = list.TotalSuccesTxnCount;
            totalGeneratedAskiAmountCell.Text = ChangeMoneyFormat(list.TotalGeneratedAskiAmount);
            totalUsedAskiAmountCell.Text = ChangeMoneyFormat(list.TotalUsedAskiAmount);
            totalGeneratedAskiCountCell.Text = list.TotalGeneratedAskiCount;
            totalUsedAskiCountCell.Text = list.TotalUsedAskiCount;
            totalReceivedAmountCell.Text = ChangeMoneyFormat(list.TotalReceivedAmount);
            totalGivenAmountCell.Text = ChangeMoneyFormat(list.TotalParaUstu);
            totalTransationCell.Text = ChangeMoneyFormat(list.TotalTotalAmount.ToString());
            //totalKioskEmptyAmountCell.Text = ChangeMoneyFormat(list.TotalKioskEmptyAmount);
            totalEarnedMoneyCell.Text = ChangeMoneyFormat(list.TotalEarnedAmount);
            totalInstitutionCell.Text = ChangeMoneyFormat(list.TotalInstitutionAmount);
            totalCreditCardAmountCell.Text = list.TotalCreditCardAmount;
            totalCommisionAmountCell.Text = list.TotalCommisionAmount;

            addNewRow.Cells.Add(spaceCell);
            addNewRow.Cells.Add(totalTxnCountCell);
            addNewRow.Cells.Add(totalSuccessTxnCountCell);
            addNewRow.Cells.Add(totalGeneratedAskiAmountCell);
            addNewRow.Cells.Add(totalUsedAskiAmountCell);
            addNewRow.Cells.Add(totalGeneratedAskiCountCell);
            addNewRow.Cells.Add(totalUsedAskiCountCell);
            addNewRow.Cells.Add(totalReceivedAmountCell);
            addNewRow.Cells.Add(totalCreditCardAmountCell);
            addNewRow.Cells.Add(totalCommisionAmountCell);
            addNewRow.Cells.Add(totalGivenAmountCell);
            addNewRow.Cells.Add(totalTransationCell);
            //addNewRow.Cells.Add(totalKioskEmptyAmountCell);
            addNewRow.Cells.Add(totalInstitutionCell);
            addNewRow.Cells.Add(totalEarnedMoneyCell);
            this.itemsTable.Rows.Add(addNewRow);


            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(list.pageCount, this.pageNum, list.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskBDT");
        }
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }


    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskReport lists = callWebServ.CallListKioskReportsService(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             Convert.ToInt32(ViewState["OrderDesc"]),
                                                                             kioskIds,
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             2);

            if (lists != null)
            {
                if (lists.errorCode == 0)
                {
                    for (int i = 0; i < lists.KioskReports.Count; i++)
                    {
                        lists.KioskReports[i].EarnedMoney = lists.KioskReports[i].EarnedMoney.Replace('.', ',');
                        lists.KioskReports[i].GeneratedAskiAmount = lists.KioskReports[i].GeneratedAskiAmount.Replace('.', ',');
                        //lists.KioskReports[i].KioskEmptyAmount = lists.KioskReports[i].TotalAmount.Replace('.', ',');
                        lists.KioskReports[i].ParaUstu = lists.KioskReports[i].ParaUstu.Replace('.', ',');
                        lists.KioskReports[i].ReceivedAmount = lists.KioskReports[i].ReceivedAmount.Replace('.', ',');
                        lists.KioskReports[i].UsedAskiAmount = lists.KioskReports[i].UsedAskiAmount.Replace('.', ',');
                    }

                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    string[] headerNames = { "No", "Kiosk Adı", "İşlem", "Üretilen Kod Tutar", "Kullanılan Kod Tutarı", "Üretilen Kod",
                                               "Kullanılan Kod", "Toplanan Tutar", "Para Üstü", "Kazanılan Tutar", "KioskEmptyAmount", 
                                               "İşlem Tutarı", "B. İşlem", "Kurum Tutarı", "Kredi Kartı Tutarı", "Komisyon Tutarı"};
                    exportExcell.ExportExcellByBlock(lists.KioskReports, "Kiosk Rapor Listesi", headerNames);

                }
                else if (lists.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + lists.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelAskiCode");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    protected void deleteButton_Click(object sender, EventArgs e)
    {
        this.SearchOrder(0, 1);
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            //case "KioskId":
            //    if (orderSelectionColumn == 1)
            //    {
            //        if (orderSelectionDescAsc == 0)
            //        {
            //            KioskId.Text = "<a>Kiosk No    <img src=../images/arrow_up.png border=0/></a>";
            //            orderSelectionDescAsc = 1;
            //        }
            //        else
            //        {
            //            KioskId.Text = "<a>Kiosk No    <img src=../images/arrow_down.png border=0/></a>";
            //            orderSelectionDescAsc = 0;
            //        }
            //    }
            //    else
            //    {
            //        KioskId.Text = "<a>Kiosk No    <img src=../images/arrow_up.png border=0/></a>";
            //        orderSelectionColumn = 1;
            //        orderSelectionDescAsc = 1;
            //    }
            //    break;
            case "KioskName":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                   // KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "TxnCount":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // TxnCount.Text = "<a>İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                       // TxnCount.Text = "<a>İşlem Sayısı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TxnCount.Text = "<a>İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "GeneratedAskiAmount":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // GeneratedAskiAmount.Text = "<a>Üretilen Kod Tutarı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                       // GeneratedAskiAmount.Text = "<a>Üretilen Kod Tutarı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //GeneratedAskiAmount.Text = "<a>Üretilen Kod Tutarı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "UsedAskiAmount":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // UsedAskiAmount.Text = "<a>Kullanılan Kod Tutarı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                       // UsedAskiAmount.Text = "<a>Kullanılan Kod Tutarı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //UsedAskiAmount.Text = "<a>Kullanılan Kod Tutarı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "GeneratedAskiCount":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //GeneratedAskiCount.Text = "<a>Üretilen Kod Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                       // GeneratedAskiCount.Text = "<a>Üretilen Kod Sayısı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                   // GeneratedAskiCount.Text = "<a>Üretilen Kod Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "UsedAskiCount":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // UsedAskiCount.Text = "<a>Kullanılan Kod Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                       // UsedAskiCount.Text = "<a>Kullanılan Kod Sayısı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                   // UsedAskiCount.Text = "<a>Kullanılan Kod Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "ReceivedAmount":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // ReceivedAmount.Text = "<a>Toplanan Tutar    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                       // ReceivedAmount.Text = "<a>Toplanan Tutar    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                  //  ReceivedAmount.Text = "<a>Toplanan Tutar    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "KioskGivenMoney":
                if (orderSelectionColumn == 13)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // KioskEmptyAmount.Text = "<a>Kiosktan Alınan Tutar    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                      //  KioskEmptyAmount.Text = "<a>Kiosktan Alınan Tutar    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                   // KioskEmptyAmount.Text = "<a>Kiosktan Alınan Tutar    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 13;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "EarnedMoney":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //EarnedMoney.Text = "<a>Kazanılan Tutar    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                       // EarnedMoney.Text = "<a>Kazanılan Tutar    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //EarnedMoney.Text = "<a>Kazanılan Tutar    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "SuccessTxnCount":
                if (orderSelectionColumn == 12)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 12;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "TransactionAmount":
                if (orderSelectionColumn == 14)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 14;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "InstitutionAmount":
                if (orderSelectionColumn == 15)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 15;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "CreditCardAmount":
                if (orderSelectionColumn == 16)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 16;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "CommisionAmount":
                if (orderSelectionColumn == 17)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //SuccessTxnCount.Text = "<a>B. İşlem Sayısı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 17;
                    orderSelectionDescAsc = 1;
                }
                break;

            default:
                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskReport kioskReport = callWebServ.CallListKioskReportsService(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             orderSelectionColumn,
                                                                             descAsc,
                                                                             kioskIds,
                                                                             Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             1);
            if (kioskReport != null)
            {
                if (kioskReport.errorCode == 0)
                {
                    this.BindListTable(kioskReport);
                }
                else if (kioskReport.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + kioskReport.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "KioskReportSrch");
        }
    }

}