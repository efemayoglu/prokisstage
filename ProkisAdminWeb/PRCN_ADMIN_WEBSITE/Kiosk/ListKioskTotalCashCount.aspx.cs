﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Kiosk;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Kiosk_ListKioskTotalCashCount : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int kioskId, operationType, emptyKioskId;
    private int operationId;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 1;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;
    private string startDate;
    private string endDate;

    public string insertDate;
    private ParserGetKiosk kiosk;


    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];


        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Kiosk/ListKioskCashCount.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        //if (this.ownSubMenuIndex == -1)
        //{
        //    Response.Redirect("../Default.aspx", true);
        //}

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        this.kioskId = Convert.ToInt32(Request.QueryString["kioskId"]);
        this.operationType = Convert.ToInt32(Request.QueryString["operationType"]);
        this.operationId = Convert.ToInt32(Request.QueryString["operationId"]); //startDate, endDate

        this.startDate = (Request.QueryString["startDate"]).ToString();
        this.endDate = (Request.QueryString["endDate"]).ToString();

        this.numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            ViewState["OrderColumn"] = 1;
            ViewState["OrderDesc"] = 1;

            this.BuildControls();
        }
    }

    private void BuildControls()
    {
        CallWebServices callWebServ = new CallWebServices();
        if (this.kioskId != 0)
        {
            this.kiosk = callWebServ.CallGetKioskService(this.kioskId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        }

        List(1, 1);
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.List(4, 1);
    }

    public void List(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int itemId = 0;

            int whichOperation = 0;

            //Merkezi para
            if (operationType == 1)
            {
                //Doldurulan paraların bulunması
                if (this.kioskId != 0)
                {
                    //ListKioskConditions.aspx
                    if (this.kioskId == this.operationId)
                    {
                        whichOperation = 1;
                        itemId = this.kioskId;
                    }
                    //ListApprovedCollectedMoney.aspx
                    else
                    {
                        whichOperation = 2;
                        itemId = this.operationId;
                    }
                }
                else
                {
                    //ListKioskCashEmpty .aspx (sıfırlama)
                    if (this.kioskId == 0)
                    {
                        whichOperation = 2;
                        itemId = this.operationId;
                    }
                }

            }
            else if (operationType == 0)
            {
                //Fark detayının bulunması,
                whichOperation = 3;
                itemId = this.operationId;
            }
            else if (operationType == 3)
            {
                //Toplam Sayılar,
                whichOperation = 4;
                itemId = this.operationId;
            }
            else if (operationType == 4)
            {
                //Toplam Fark,
                whichOperation = 5;
                itemId = this.operationId;
            }


            if (whichOperation != 0)
            {
                CallWebServices callWebService = new CallWebServices();
                ParserListTotalCashCounts listTotalCashCounts = callWebService.CallGetTotalCashCountService(
                                                                                             Convert.ToDateTime(this.startDate),
                                                                                             Convert.ToDateTime(this.endDate),
                                                                                             whichOperation,
                                                                                             //itemId,
                                                                                             this.numberOfItemsPerPage,
                                                                                             this.pageNum,
                                                                                             this.orderSelectionColumn,
                                                                                             this.orderSelectionDescAsc, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                if (listTotalCashCounts != null)
                {
                    if (listTotalCashCounts.errorCode == 0)
                    {
                        this.BindListTable(listTotalCashCounts);
                    }
                    else if (listTotalCashCounts.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                    {
                        Session.Abandon();
                        Session.RemoveAll();
                        Response.Redirect("../root/Login.aspx", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + listTotalCashCounts.errorDescription + "'); ", true);
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Bir Sorun Oluştu !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskMoneyDetailSrch");
        }
    }

    private void BindListTable(ParserListTotalCashCounts list)
    {
        try
        {
            double sumAll = 0;
            double countAll = 0;

            TableRow[] Row = new TableRow[list.cashCount.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                //Row[i].Attributes.Add("transactionId", listKioskMoneyDetail.kioskMoneyDetail[i].TransactionId.ToString());

                TableCell indexCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                //TableCell cashTypeCell = new TableCell();
                TableCell cashCountCell = new TableCell();

                TableCell cashTypeNameCell = new TableCell();
                TableCell sumCell = new TableCell();


               

                indexCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                //cashTypeCell.CssClass = "inputTitleCell4";
                cashCountCell.CssClass = "inputTitleCell4";

                cashTypeNameCell.CssClass = "inputTitleCell4";
                sumCell.CssClass = "inputTitleCell4";
 

                indexCell.Text = (startIndex + i).ToString();
                // kioskNameCell.Text = list.cashCount[i].KioskName;
                cashTypeNameCell.Text = list.cashCount[i].cashTypeName;
                cashCountCell.Text = list.cashCount[i].cashCount.ToString() + "  Adet";


                string[] words = cashTypeNameCell.Text.Split(' ');

                if (cashTypeNameCell.Text.Contains("Krs"))
                {
                    sumCell.Text = ((Convert.ToDouble(words[0])) / 100 * (list.cashCount[i].cashCount)).ToString();

                    sumCell.Text = sumCell.Text + " TL";

                }
                else
                    sumCell.Text = (Convert.ToDouble(words[0]) * (list.cashCount[i].cashCount)).ToString() + " TL";


                if (cashTypeNameCell.Text.Contains("Krs"))
                {
                    sumAll = (Convert.ToDouble(words[0]) / 100 * (list.cashCount[i].cashCount)) + sumAll;
                }
                else
                {
                    sumAll = (Convert.ToDouble(words[0]) * (list.cashCount[i].cashCount)) + sumAll;
                }


                countAll = (list.cashCount[i].cashCount) + countAll;

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
						cashTypeNameCell,
                        cashCountCell,
                        sumCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);

            TableRow addNewRow = new TableRow();
            TableCell totalMoneyTextCell = new TableCell();
            TableCell totalMoneyCell = new TableCell();
            TableCell spaceCell = new TableCell();
            TableCell countCell = new TableCell();

            spaceCell.CssClass = "inputTitleCell3";
            spaceCell.ColumnSpan = (2);

            totalMoneyTextCell.CssClass = "inputTitleCell3";
            totalMoneyCell.CssClass = "inputTitleCell3";

            countCell.CssClass = "inputTitleCell3";
            countCell.Text = countAll.ToString() + "  Adet";

            spaceCell.Text = " ";


            spaceCell.Text = "Toplam Miktar:";
            totalMoneyCell.Text = sumAll + " TL ";

            addNewRow.Cells.Add(spaceCell);
            addNewRow.Cells.Add(countCell);
            //addNewRow.Cells.Add(totalMoneyTextCell);
            addNewRow.Cells.Add(totalMoneyCell);

            this.itemsTable.Rows.Add(addNewRow);


            if (operationType == 1)
            {
                lblKioskName.Text = "Kiosk Para Merkez Adetleri / " + kiosk.Kiosk.KioskName;

            }
            else if (operationType == 0)
            {
                lblKioskName.Text = "Kiosk Para Fark Adetleri / " + kiosk.Kiosk.KioskName;
            }

            //TableRow pagingRow = new TableRow();
            //TableCell pagingCell = new TableCell();

            //pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            // pagingCell.HorizontalAlign = HorizontalAlign.Right;
            // pagingCell.Text = WebUtilities.GetPagingText(list.pageCount, this.pageNum, list.recordCount);
            // pagingRow.Cells.Add(pagingCell);
            // this.itemsTable.Rows.AddAt(0, pagingRow);
            // this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskMoneyDetailBDT");
        }
    }
}