﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Other;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Kiosk_ListKioskNew : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Kiosk/ListNewKiosk.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            ViewState["OrderColumn"] = 4;
            ViewState["OrderDesc"] = 1;

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskTypeName kioskTypes = callWebServ.CallGetKisokTypeNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kioskTypes != null)
            {

                this.kioskTypeBox.DataSource = kioskTypes.KioskTypeNames;
                this.kioskTypeBox.DataBind();

            }

            ListItem item1 = new ListItem();
            item1.Text = "Kiosk Tipi Seçiniz";
            item1.Value = "0";
            this.kioskTypeBox.Items.Add(item1);
            this.kioskTypeBox.SelectedValue = "0";


            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {

                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();

            }

            ListItem item2 = new ListItem();
            item2.Text = "Kurum Seçiniz";
            item2.Value = "0";
            this.instutionBox.Items.Add(item2);
            this.instutionBox.SelectedValue = "0";



            ParserListRegion regions = callWebServ.CallGetRegionsService(6, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (regions != null)
            {
                this.regionBox.DataSource = regions.Regions;
                this.regionBox.DataBind();
            }

            ListItem item3 = new ListItem();
            item3.Text = "Lokasyon Türü Seçiniz";
            item3.Value = "0";
            this.regionBox.Items.Add(item3);
            this.regionBox.SelectedValue = "0";


            ParserListConnectionType connectionType = callWebServ.CallGetConnectionTypeService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (connectionType != null)
            {
                this.connectionBox.DataSource = connectionType.ConnectionType;
                this.connectionBox.DataBind();
            }

            ListItem item4 = new ListItem();
            item4.Text = "Bağlantı Tipi Seçiniz";
            item4.Value = "0";
            this.connectionBox.Items.Add(item4);
            this.connectionBox.SelectedValue = "0";


            ParserListCardReaderType cardReaderType = callWebServ.CallGetCardReaderTypeService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (cardReaderType != null)
            {
                this.cardReaderBox.DataSource = cardReaderType.CardReaderType;
                this.cardReaderBox.DataBind();
            }

            ListItem item5 = new ListItem();
            item5.Text = "Kart Tipi Seçiniz";
            item5.Value = "0";
            this.cardReaderBox.Items.Add(item5);
            this.cardReaderBox.SelectedValue = "0";


            ParserListStatusName statusNames = callWebServ.CallGetKioskStatusNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (statusNames != null)
            {
                this.statusBox.DataSource = statusNames.StatusNames;
                this.statusBox.DataBind();
            }

            ListItem item6 = new ListItem();
            item6.Text = "Kiosk Durum Seçiniz";
            item6.Value = "0";
            this.statusBox.Items.Add(item6);
            this.statusBox.SelectedValue = "0";

            ParserListCity cities = callWebServ.CallGetCitiesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (cities != null)
            {
                this.cityBox.DataSource = cities.Cities;
                this.cityBox.DataBind();
            }

            ListItem item7 = new ListItem();
            item7.Text = "Şehir Seçiniz";
            item7.Value = "0";
            this.cityBox.Items.Add(item7);
            this.cityBox.SelectedValue = "0";


            ParserListRiskClass riskClass = callWebServ.CallGetRiskClassService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (riskClass != null)
            {
                this.riskClassBox.DataSource = riskClass.RiskClass;
                this.riskClassBox.DataBind();
            }


            ListItem item8 = new ListItem();
            item8.Text = "Risk Sınıfı Seçiniz";
            item8.Value = "0";
            this.riskClassBox.Items.Add(item8);
            this.riskClassBox.SelectedValue = "0";


            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {
                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }


            // instutionBox.Attributes.Add("disabled", "true");
            // kioskType.Attributes.Add("disabled", "true");


            this.pageNum = 0;
            this.SearchOrder(4, 1);
        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(4, 1);
    }


    private void BindListTable(ParserListKiosks kiosks, int recordcount, int pagecount)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            TableRow[] Row = new TableRow[kiosks.Kiosks.Count];

            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;

            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("kioskId", kiosks.Kiosks[i].KioskId.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell nameCell = new TableCell();
                TableCell adressCell = new TableCell();
                TableCell ipCell = new TableCell();
                TableCell statusCell = new TableCell();
                TableCell insertionDateCell = new TableCell();
                TableCell latitudeCell = new TableCell();
                TableCell longitudeCell = new TableCell();
                TableCell cityCell = new TableCell();
                TableCell regionCell = new TableCell();
                //TableCell locationCell = new TableCell();
                TableCell kioskTypeNameCell = new TableCell();
                TableCell institutionCell = new TableCell();
                TableCell connectionTypeCell = new TableCell();
                TableCell cardReaderTypeCell = new TableCell();
                TableCell usageFeeCell = new TableCell();
                TableCell appVersionCell = new TableCell();
                TableCell lastAppAliveTimeCell = new TableCell();
                TableCell detailsCell = new TableCell();
                TableCell capacityCell = new TableCell();
                TableCell explanationCell = new TableCell();
                TableCell accomplishedTransactionsCell = new TableCell();
                TableCell riskClassCell = new TableCell();
                TableCell appAliveDurationTimeCell = new TableCell();
                TableCell makbuzStatusCell = new TableCell();
                TableCell jcmStatusCell = new TableCell();
                TableCell creditcardCell = new TableCell();
                TableCell mobilePaymentCell = new TableCell();

                TableCell fakeCell1 = new TableCell();
                TableCell fakeCell2 = new TableCell();
                TableCell fakeCell3 = new TableCell();
                TableCell fakeCell4 = new TableCell();
                TableCell fakeCell5 = new TableCell();
                TableCell fakeCell6 = new TableCell();
                TableCell fakeCell7 = new TableCell();
                TableCell fakeCell8 = new TableCell();
                TableCell fakeCell9 = new TableCell();
                TableCell fakeCell10 = new TableCell();
                TableCell fakeCell11 = new TableCell();
                TableCell fakeCell12 = new TableCell();
                TableCell fakeCell13 = new TableCell();

                fakeCell1.CssClass = "inputTitleCell4";
                fakeCell2.CssClass = "inputTitleCell4";
                fakeCell3.CssClass = "inputTitleCell4";
                fakeCell4.CssClass = "inputTitleCell4";
                fakeCell5.CssClass = "inputTitleCell4";
                fakeCell6.CssClass = "inputTitleCell4";
                fakeCell7.CssClass = "inputTitleCell4";
                fakeCell8.CssClass = "inputTitleCell4";
                fakeCell9.CssClass = "inputTitleCell4";
                fakeCell10.CssClass = "inputTitleCell4";
                fakeCell11.CssClass = "inputTitleCell4";
                fakeCell12.CssClass = "inputTitleCell4";
                fakeCell13.CssClass = "inputTitleCell4";

                fakeCell1.Visible = false;
                fakeCell2.Visible = false;
                fakeCell3.Visible = false;
                fakeCell4.Visible = false;
                fakeCell5.Visible = false;
                fakeCell6.Visible = false;
                fakeCell7.Visible = false;
                fakeCell8.Visible = false;
                fakeCell9.Visible = false;
                fakeCell10.Visible = false;
                fakeCell11.Visible = false;
                fakeCell12.Visible = false;
                fakeCell13.Visible = false;

                indexCell.CssClass = "inputTitleCell4";
                idCell.CssClass = "inputTitleCell4";
                nameCell.CssClass = "inputTitleCell4";
                adressCell.CssClass = "inputTitleCell4";
                ipCell.CssClass = "inputTitleCell4";
                statusCell.CssClass = "inputTitleCell4";
                insertionDateCell.CssClass = "inputTitleCell4";
                latitudeCell.CssClass = "inputTitleCell4";
                longitudeCell.CssClass = "inputTitleCell4";
                cityCell.CssClass = "inputTitleCell4";
                regionCell.CssClass = "inputTitleCell4";
                //locationCell.CssClass = "inputTitleCell4";
                kioskTypeNameCell.CssClass = "inputTitleCell4";
                connectionTypeCell.CssClass = "inputTitleCell4";
                cardReaderTypeCell.CssClass = "inputTitleCell4";
                usageFeeCell.CssClass = "inputTitleCell4";
                appVersionCell.CssClass = "inputTitleCell4";
                lastAppAliveTimeCell.CssClass = "inputTitleCell4";
                detailsCell.CssClass = "inputTitleCell4";
                capacityCell.CssClass = "inputTitleCell4";
                explanationCell.CssClass = "inputTitleCell4";
                accomplishedTransactionsCell.CssClass = "inputTitleCell4";
                detailsCell.Width = Unit.Pixel(75);
                nameCell.HorizontalAlign = HorizontalAlign.Left;
                nameCell.Style.Add("padding-left", "5px");
                idCell.Visible = false;
                institutionCell.CssClass = "inputTitleCell4";
                riskClassCell.CssClass = "inputTitleCell4";
                appAliveDurationTimeCell.CssClass = "inputTitleCell4";
                makbuzStatusCell.CssClass = "inputTitleCell4";
                jcmStatusCell.CssClass = "inputTitleCell4";
                creditcardCell.CssClass = "inputTitleCell4";
                mobilePaymentCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();






                nameCell.Text = kiosks.Kiosks[i].KioskName.ToString();
                latitudeCell.Text = kiosks.Kiosks[i].Latitude;
                longitudeCell.Text = kiosks.Kiosks[i].Longitude;
                cityCell.Text = kiosks.Kiosks[i].CityName;
                regionCell.Text = kiosks.Kiosks[i].RegionName;
              
                kioskTypeNameCell.Text = kiosks.Kiosks[i].KioskTypeName;
                connectionTypeCell.Text = kiosks.Kiosks[i].ConnectionType;
                cardReaderTypeCell.Text = kiosks.Kiosks[i].CardReaderType;
                idCell.Text = kiosks.Kiosks[i].KioskId.ToString();
                adressCell.Text = kiosks.Kiosks[i].KioskAddress.ToString();
                ipCell.Text = kiosks.Kiosks[i].KioskIp.ToString();
                statusCell.Text = kiosks.Kiosks[i].KioskStatusName.ToString();
                insertionDateCell.Text = kiosks.Kiosks[i].KioskInsertionDate.ToString();
                usageFeeCell.Text = kiosks.Kiosks[i].UsageFee.ToString();
                appVersionCell.Text = kiosks.Kiosks[i].appVersion;
                lastAppAliveTimeCell.Text = kiosks.Kiosks[i].lastAppAliveTime.ToString();
                capacityCell.Text = kiosks.Kiosks[i].CashBoxAcceptorTypeName.ToString() + "/" + kiosks.Kiosks[i].Capacity.ToString();
                explanationCell.Text = kiosks.Kiosks[i].Explanation;
                accomplishedTransactionsCell.Text = kiosks.Kiosks[i].AccomplishedTransactions;
                institutionCell.Text = kiosks.Kiosks[i].institutionName;
               
                riskClassCell.Text = kiosks.Kiosks[i].riskClass;
                appAliveDurationTimeCell.Text = kiosks.Kiosks[i].AppAliveDurationTime;
                makbuzStatusCell.Text = kiosks.Kiosks[i].MakbuzStatus;
                jcmStatusCell.Text = kiosks.Kiosks[i].JCMStatus;
                creditcardCell.Text = kiosks.Kiosks[i].IsCreditCard;
                mobilePaymentCell.Text = kiosks.Kiosks[i].IsMobilePayment;

                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanDelete == "1")
                {
                    detailsCell.Text = "<a href=\"javascript: deleteButtonClicked('[pk].[delete_kiosk]'," + kiosks.Kiosks[i].KioskId + ");\" class=\"deleteButton\" title=\"Kiosk Sil\"></a>";
                }
                else
                {
                    detailsCell.Text = " ";
                }

                System.TimeSpan zaman;
                if (!string.IsNullOrEmpty(kiosks.Kiosks[i].lastAppAliveTime))
                {
                    zaman = kiosks.serverTime.Subtract(Convert.ToDateTime(kiosks.Kiosks[i].lastAppAliveTime));
                    int toplamdakika = Convert.ToInt32(zaman.TotalMinutes);
                    if (toplamdakika > Convert.ToInt32(Utility.GetConfigValue("AliveTimeOutMinute")))
                    {
                        lastAppAliveTimeCell.CssClass = "inputTitleCell4Red";
                    }
                    else
                    {
                        lastAppAliveTimeCell.CssClass = "inputTitleCell4Green";
                    }
                }
                else
                {
                    lastAppAliveTimeCell.CssClass = "inputTitleCellLightBlue";

                }

                if (!string.IsNullOrEmpty(kiosks.Kiosks[i].AccomplishedTransactions))
                {
                    zaman = kiosks.serverTime.Subtract(Convert.ToDateTime(kiosks.Kiosks[i].AccomplishedTransactions));
                    int toplamdakika = Convert.ToInt32(zaman.TotalMinutes);
                    if (toplamdakika > 60 && toplamdakika <= 120)
                        accomplishedTransactionsCell.CssClass = "inputTitleCell4Yellow";
                    else if (toplamdakika > 120 && toplamdakika <= 180)
                        accomplishedTransactionsCell.CssClass = "inputTitleCell4Orange";
                    else if (toplamdakika > 180)
                        accomplishedTransactionsCell.CssClass = "inputTitleCell4Red";
                }

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
						idCell,
                        fakeCell1,
                        fakeCell2,
                        fakeCell3,
                        fakeCell4,
                        fakeCell5,
                        fakeCell6,
                        fakeCell7,
                        fakeCell8,
                        fakeCell9,
                        fakeCell10,
                        fakeCell11,
                        fakeCell12,
                        fakeCell13,
						nameCell,
                        accomplishedTransactionsCell,
                        lastAppAliveTimeCell,
                        //explanationCell,
                        //adressCell,
                        //cityCell,
                        //regionCell,
                        ////locationCell,
                        //latitudeCell,
                        //longitudeCell,
                        //ipCell,
                        //statusCell,
                        //kioskTypeNameCell,
                        //institutionCell,
                        //connectionTypeCell,
                        cardReaderTypeCell,
                        riskClassCell,
                        usageFeeCell,
                       // insertionDateCell,
                        
                       
                        appAliveDurationTimeCell,
                        makbuzStatusCell,
                        jcmStatusCell,
                        creditcardCell,
                        mobilePaymentCell,
                         capacityCell,
                        appVersionCell,                                     
					});


                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (kiosks.recordCount < currentRecordEnd)
                currentRecordEnd = kiosks.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + kiosks.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(pagecount, this.pageNum, recordcount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;

            //if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
            //{
            //    TableRow addNewRow = new TableRow();
            //    TableCell addNewCell = new TableCell();
            //    TableCell spaceCell = new TableCell();
            //    spaceCell.CssClass = "inputTitleCell4";
            //    spaceCell.ColumnSpan = (this.itemsTable.Rows[1].Cells.Count - 1);
            //    addNewCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editButtonClicked('0');\" class=\"anylink\"><img src=../images/add.png border=0 title=\"Add New...\" /></a>";
            //    addNewRow.Cells.Add(spaceCell);
            //    addNewRow.Cells.Add(addNewCell);
            //    this.itemsTable.Rows.Add(addNewRow);
            //}
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);
            int recordCount = 0;
            int pageCount = 0;

            string kioskIds = "";
            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;


            CallWebServices callWebServ = new CallWebServices();
            ParserListKiosks lists = callWebServ.CallListKioskService(this.searchTextField.Text,
                                                                       Convert.ToInt32(this.kioskTypeBox.SelectedValue),
                                                                       Convert.ToInt32(this.numberOfItemField.Text),
                                                                       this.pageNum,
                                                                       recordCount,
                                                                       pageCount,
                                                                       orderSelectionColumn,
                                                                       Convert.ToInt32(ViewState["OrderDesc"]),
                                                                       kioskIds,
                                                                       Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                       Convert.ToInt32(this.connectionBox.SelectedValue),
                                                                       Convert.ToInt32(this.cardReaderBox.SelectedValue),
                                                                       Convert.ToInt32(this.statusBox.SelectedValue),
                                                                       Convert.ToInt32(this.regionBox.SelectedValue),
                                                                       Convert.ToInt32(this.cityBox.SelectedValue),
                                                                       Convert.ToInt32(this.riskClassBox.SelectedValue),
                                                                       this.LoginDatas.AccessToken,
                                                                       this.LoginDatas.User.Id,
                                                                       2);

            if (lists != null)
            {
                if (lists.errorCode == 0)
                {

                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    string[] headerNames = {  "Kiosk No","Kiosk Adı", "Adres","Kiosk Ip", "Kiosk Durum", 
                                               "Durum",  "Eklenme Tarihi", "Enlem" ,"Boylam", "CityId" ,"Şehir", "RegionId",
                                               "Lokasyan Türü", "KioskTypeId","KioskTypeName",   "Kullanım Ücreti",  
                                               "Versiyon" ,"Çalışma Durumu","Kapasite Durumu","Açıklama","Capacity","CashBoxAcceptorTypeName","Başarılı İşlem","Unknown","Bağlantı Tipi",
                                               "Kart Okuyucu Tip","Kiosk Tipi","Kurum","Risk Sınıfı2", "Risk Sınıfı","Kredi Kartı"
                                               ,"Mobil Ödeme","Çalışma Periyodu","Makbuz Durum","JCM Durum"};

                    exportExcell.ExportExcellByBlock(lists.Kiosks, "Kiosk Raporu", headerNames);
                }
                else if (lists.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + lists.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelMutabakat");
        }
    }

    protected void deleteButton_Click(object sender, EventArgs e)
    {

        this.SearchOrder(0, 1);
    }

    //OnTableHeaderCellClicked
    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "Name":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Name.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Name.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Name.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Address":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Ip":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Ip.Text = "<a>Kiosk IP    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Ip.Text = "<a>Kiosk IP    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Ip.Text = "<a>Kiosk IP    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Status":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // Status.Text = "<a>Kiosk Durum    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Status.Text = "<a>Kiosk Durum    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Status.Text = "<a>Kiosk Durum    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "InsertionDate":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // InsertionDate.Text = "<a>Eklenme Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  InsertionDate.Text = "<a>Eklenme Tarihi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //InsertionDate.Text = "<a>Eklenme Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "City":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //City.Text = "<a>Şehir    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // City.Text = "<a>Şehir    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // City.Text = "<a>Şehir    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Region":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // Region.Text = "<a>Bölge    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Region.Text = "<a>Bölge    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Region.Text = "<a>Bölge    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Latitude":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // Latitude.Text = "<a>Enlem    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Latitude.Text = "<a>Enlem    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Latitude.Text = "<a>Enlem    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Longitude":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // Longitude.Text = "<a>Boylam    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Longitude.Text = "<a>Boylam    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Longitude.Text = "<a>Boylam    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Type":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Type.Text = "<a>Kiosk Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Type.Text = "<a>Kiosk Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Type.Text = "<a>Kiosk Tipi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "KioskFee":
                if (orderSelectionColumn == 11)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // KioskFee.Text = "<a>Kullanım Ücreti    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //KioskFee.Text = "<a>Kullanım Ücreti    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //KioskFee.Text = "<a>Kullanım Ücreti    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 11;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "AppVersion":
                if (orderSelectionColumn == 12)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //AppVersion.Text = "<a>Versiyon    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // AppVersion.Text = "<a>Versiyon    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // AppVersion.Text = "<a>Versiyon    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 12;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "LastAppAliveTime":
                if (orderSelectionColumn == 13)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //LastAppAliveTime.Text = "<a>Calışma Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //LastAppAliveTime.Text = "<a>Calışma Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //LastAppAliveTime.Text = "<a>Calışma Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 13;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Capacity":
                if (orderSelectionColumn == 14)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Capacity.Text = "<a>Kapasite Durumu  <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 14;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Explanation":
                if (orderSelectionColumn == 15)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Capacity.Text = "<a>Kapasite Durumu  <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 15;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "AccomplishedTransactions":
                if (orderSelectionColumn == 16)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Capacity.Text = "<a>Kapasite Durumu  <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 16;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "ConnectionType":
                if (orderSelectionColumn == 17)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Capacity.Text = "<a>Kapasite Durumu  <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 17;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "CardReaderType":
                if (orderSelectionColumn == 18)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Capacity.Text = "<a>Kapasite Durumu  <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 18;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Instution":
                if (orderSelectionColumn == 19)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Capacity.Text = "<a>Kapasite Durumu  <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 19;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "RiskClass":
                if (orderSelectionColumn == 20)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Capacity.Text = "<a>Kapasite Durumu  <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 20;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "AppAliveDurationTime":
                if (orderSelectionColumn == 21)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Capacity.Text = "<a>Kapasite Durumu  <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 21;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "MakbuzStatus":
                if (orderSelectionColumn == 22)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Capacity.Text = "<a>Kapasite Durumu  <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 22;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "JCMStatus":
                if (orderSelectionColumn == 23)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Capacity.Text = "<a>Kapasite Durumu  <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 23;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "CreditCard":
                if (orderSelectionColumn == 24)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Capacity.Text = "<a>Kapasite Durumu  <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 24;
                    orderSelectionDescAsc = 1;
                }
                break;


            case "MobilePayment":
                if (orderSelectionColumn == 25)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Capacity.Text = "<a>Kapasite Durumu  <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 25;
                    orderSelectionDescAsc = 1;
                }
                break;


            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            string kioskIds = "";
            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;


            CallWebServices callWebServ = new CallWebServices();
            ParserListKiosks kiosks = callWebServ.CallListKioskService(this.searchTextField.Text,
                                                                       Convert.ToInt32(this.kioskTypeBox.SelectedValue),
                                                                       Convert.ToInt32(this.numberOfItemField.Text),
                                                                       this.pageNum,
                                                                       recordCount,
                                                                       pageCount,
                                                                       orderSelectionColumn,
                                                                       descAsc,
                                                                       kioskIds,
                                                                       Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                       Convert.ToInt32(this.connectionBox.SelectedValue),
                                                                       Convert.ToInt32(this.cardReaderBox.SelectedValue),
                                                                       Convert.ToInt32(this.statusBox.SelectedValue),
                                                                       Convert.ToInt32(this.regionBox.SelectedValue),
                                                                       Convert.ToInt32(this.cityBox.SelectedValue),
                                                                       Convert.ToInt32(this.riskClassBox.SelectedValue),
                                                                       this.LoginDatas.AccessToken,
                                                                       this.LoginDatas.User.Id,
                                                                       1);
            if (kiosks != null)
            {
                if (kiosks.errorCode == 0)
                {
                    this.BindListTable(kiosks, kiosks.recordCount, kiosks.pageCount);
                }
                else if (kiosks.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + kiosks.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CreateKioskCommandSrch");
        }
    }
}