﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowKioskReferenceSystem.aspx.cs" Inherits="Kiosk_ShowKioskReferenceSystem" %>

<%@ Register Src="../MS_Control/MultipleSelection.ascx" TagName="MultipleSelection" TagPrefix="uc1" %>
<%@ Register Assembly="CheckBoxListExCtrl" Namespace="CheckBoxListExCtrl" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />
    <input type="hidden" id="hiddenId" runat="server" name="hiddenId" value="0" />


      <script language="javascript" type="text/javascript">

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function searchButtonClicked(sender) {
            document.getElementById('pageNumRefField').value = "0";
            return true;
        }

        function postForPaging(selectedPageNum) {
            document.getElementById('pageNumRefField').value = selectedPageNum;
            document.getElementById('navigateButton').click();
        }

        function strtDateChngd() {
            if (document.getElementById('startDateField').value < "2015-11-04 00:00:00") {
                document.getElementById('startDateField').value = "2015-11-04 00:00:00"
            }
            else if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                document.getElementById('startDateField').value = document.getElementById('endDateField').value;
            }

        }

        function endDateChngd() {
            if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                document.getElementById('endDateField').value = document.getElementById('startDateField').value;
            }
        }


        function checkForm() {

            var status = true;

            var messageText = "";

            if (document.getElementById('MultipleSelection1').id == "0") {
                status = false; //return false döner 
                messageText = "Kiosk Seçiniz !";
            }

            else {
                messageText = "";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }

    </script>


</head>

<body>

    <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>


        <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel7" runat="server" BackColor="White" Width="50%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <div align="center" id="articleDetailsTab" class="windowTitle_container_autox30">
                    Kiosk Referans Sistemi<hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>

                <table border="0" cellpadding="2" cellspacing="0" id="Table1" runat="server">

                    <tr>
                        <td align="center" class="inputTitleCell2" height="25" colspan="2">Referans No:
                        </td>
                    </tr>

                    <tr>
                        <td align="center" colspan="2" height="30">
                            <asp:TextBox TextMode="Number" ID="referenceNo" runat="server" BorderStyle="Ridge" CssClass="inputTitleCell9_v4" min="1" max="10000" step="1" value="0" text-align="center"/>
                        </td>
                    </tr>


                    <tr>
                        <td align="center" class="inputTitleCell2" height="25" colspan="2">Kiosk:
                        </td>
                    </tr>
                    <%-- <tr>
                        <td align="center" colspan="2" height="30">
                            <asp:ListBox ID="kioskBox" runat="server" AutoPostBack="True" CssClass="inputLine_375x20" DataTextField="Name" DataValueField="Id" Rows="1" SelectionMode="Single"></asp:ListBox>
                        </td>
                    </tr>--%>




                    <tr>
                        <td align="center" colspan="2" height="30">

                            <uc1:MultipleSelection ID="MultipleSelection1" runat="server" CssClass="inputLine_100x20" />

                            <%--
                            <cc2:HoverMenuExtender ID="HoverMenuExtender1"
                                runat="server"
                                TargetControlID="MultiSelectDDL"
                                PopupControlID="PanelPopUp"
                                PopupPosition="bottom"
                                OffsetX="6"
                                PopDelay="25" HoverCssClass="popupHover">
                            </cc2:HoverMenuExtender>




                            <asp:DropDownList ID="MultiSelectDDL" CssClass="ddlMenu regularText" runat="server">
                                <asp:ListItem Value="all">Select</asp:ListItem>
                            </asp:DropDownList> 




                          <asp:Panel ID="PanelPopUp" CssClass="popupMenu" runat="server" Style="height: 300px;">
                                <cc1:CheckBoxListExCtrl ID="CheckBoxListExCtrl1" CssClass="regularText" runat="server">
                                    <asp:ListItem Value="d1">Dummy 1</asp:ListItem>
                                    <asp:ListItem Value="d2">Dummy 2</asp:ListItem>
                                    <asp:ListItem Value="d3">Dummy 3</asp:ListItem>
                                    <asp:ListItem Value="d4">Dummy 4</asp:ListItem>
                                    <asp:ListItem Value="d5">Dummy 5</asp:ListItem>
                                    <asp:ListItem Value="d6">Dummy 6</asp:ListItem>
                                    <asp:ListItem Value="d7">Dummy 7</asp:ListItem>
                                    <asp:ListItem Value="d8">Dummy 8</asp:ListItem>
                                </cc1:CheckBoxListExCtrl>
                            </asp:Panel>
                            <asp:HiddenField ID="hf_checkBoxValue" runat="server" />
                            <asp:HiddenField ID="hf_checkBoxText" runat="server" />
                            <asp:HiddenField ID="hf_checkBoxSelIndex" runat="server" />--%>

                        </td>
                    </tr>


                     <tr>
                        <td align="center" class="inputTitleCell2" height="25" colspan="2">Açıklama:    
                        </td>
                    </tr>

                      <tr>
                       <td>
                            <asp:TextBox TextMode="MultiLine" Style="resize: none; text-align: left;" Height="85" ID="explanationField" CssClass="inputLine_90x20_2" runat="server"></asp:TextBox>
                        </td>
                    </tr>

                 

                </table>


                <%-- <table border="0" cellpadding="2" cellspacing="0" id="Table1" runat="server">
                    <tr>
                        <td align="center" class="inputTitleCell2" height="25" colspan="2">Kiosk:
                        </td>
                    </tr>

                  <%--  <tr>
                        <td align="center" colspan="2" height="30">

                            <cc2:HoverMenuExtender ID="HoverMenuExtender1"
                                runat="server"
                                TargetControlID="MultiSelectDDL"
                                PopupControlID="PanelPopUp"
                                PopupPosition="bottom"
                                OffsetX="6"
                                PopDelay="25" HoverCssClass="popupHover">
                            </cc2:HoverMenuExtender>


                            <asp:DropDownList ID="MultiSelectDDL" CssClass="ddlMenu regularText" runat="server">
                                <asp:ListItem Value="all">Select</asp:ListItem>
                            </asp:DropDownList>


                            <asp:Panel ID="PanelPopUp" CssClass="popupMenu" runat="server" Style="height: 300px;">
                                <cc1:CheckBoxListExCtrl ID="CheckBoxListExCtrl1" CssClass="regularText" runat="server">
                                    <asp:ListItem Value="d1">Dummy 1</asp:ListItem>
                                    <asp:ListItem Value="d2">Dummy 2</asp:ListItem>
                                    <asp:ListItem Value="d3">Dummy 3</asp:ListItem>
                                    <asp:ListItem Value="d4">Dummy 4</asp:ListItem>
                                    <asp:ListItem Value="d5">Dummy 5</asp:ListItem>
                                    <asp:ListItem Value="d6">Dummy 6</asp:ListItem>
                                    <asp:ListItem Value="d7">Dummy 7</asp:ListItem>
                                    <asp:ListItem Value="d8">Dummy 8</asp:ListItem>
                                </cc1:CheckBoxListExCtrl>
                            </asp:Panel>
                            <asp:HiddenField ID="hf_checkBoxValue" runat="server" />
                            <asp:HiddenField ID="hf_checkBoxText" runat="server" />
                            <asp:HiddenField ID="hf_checkBoxSelIndex" runat="server" />

                        </td>
                    </tr>--%>




                <br />

                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Kaydet"></asp:Button>

                                <asp:Button ID="cancelButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClientClick="parent.hideModalPopup2_Exit();" Text="Vazgeç"></asp:Button>

                            </td>
                        </tr>
                    </table>
                </td>
            </asp:Panel>
        </div>
    </form>

</body>
</html>
