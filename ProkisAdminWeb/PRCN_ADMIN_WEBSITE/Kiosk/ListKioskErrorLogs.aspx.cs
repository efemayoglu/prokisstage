﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Kiosk;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Kiosk_ListKioskErrorLogs : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Kiosk/ListKioskErrorLogs.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

             
            this.startDateField.Text = DateTime.Now.ToString("yyyy-MM-dd 00:00:00");
            this.endDateField.Text = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd 00:00:00");
 

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {

                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }

            ParserListProcessType processType = callWebServ.CallGetProcessTypeFromLogs(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (processType != null)
            {

                this.ProcessType.DataSource = processType.parserProcessType;
                this.ProcessType.DataBind();

            }
            ListItem item2 = new ListItem();
            item2.Text = "İşlem Tipi Seçiniz";
            item2.Value = "0";
            this.ProcessType.Items.Add(item2);
            this.ProcessType.SelectedValue = "0";

            this.pageNum = 0;
            ViewState["OrderColumn"] = this.orderSelectionColumn;
            ViewState["OrderDesc"] = this.orderSelectionDescAsc;
            SearchOrder(3, 1);
        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {

        //Session["startDate"] = startDateField.Text;
        //Session["endDate"] = endDateField.Text;
        Session["numberOfItemField"] = numberOfItemField.Text;


        this.pageNum = 0;
        //ViewState["OrderColumn"] = this.orderSelectionColumn;
        //ViewState["OrderDesc"] = this.orderSelectionDescAsc;
        //SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
        SearchOrder(3, 1);
    }


    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "KioskName":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "LogTime":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //  Address.Text = "<a>Kiosk Adres    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "TransactionTime":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  Ip.Text = "<a>Kiosk IP    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Ip.Text = "<a>Kiosk IP    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Ip.Text = "<a>Kiosk IP    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "DeviceType":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // Status.Text = "<a>Kiosk Durum    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Status.Text = "<a>Kiosk Durum    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // Status.Text = "<a>Kiosk Durum    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "ErrorType":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // InsertionDate.Text = "<a>Eklenme Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //InsertionDate.Text = "<a>Eklenme Tarihi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //InsertionDate.Text = "<a>Eklenme Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Notification":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // City.Text = "<a>Şehir    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //City.Text = "<a>Şehir    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //City.Text = "<a>Şehir    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "NotificationTime":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // Region.Text = "<a>Bölge    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Region.Text = "<a>Bölge    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Region.Text = "<a>Bölge    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "NotificationType":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // Latitude.Text = "<a>Enlem    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Latitude.Text = "<a>Enlem    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Latitude.Text = "<a>Enlem    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "StateIntervention":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // Longitude.Text = "<a>Boylam    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //   Longitude.Text = "<a>Boylam    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Longitude.Text = "<a>Boylam    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "InterventionTime":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  Type.Text = "<a>Kiosk Tipi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Type.Text = "<a>Kiosk Tipi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Type.Text = "<a>Kiosk Tipi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Technician":
                if (orderSelectionColumn == 11)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // KioskFee.Text = "<a>Kullanım Ücreti    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // KioskFee.Text = "<a>Kullanım Ücreti    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //KioskFee.Text = "<a>Kullanım Ücreti    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 11;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Approval":
                if (orderSelectionColumn == 12)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // AppVersion.Text = "<a>Versiyon    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //AppVersion.Text = "<a>Versiyon    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //AppVersion.Text = "<a>Versiyon    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 12;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "ApprovalTime":
                if (orderSelectionColumn == 13)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //LastAppAliveTime.Text = "<a>Calışma Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // LastAppAliveTime.Text = "<a>Calışma Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // LastAppAliveTime.Text = "<a>Calışma Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 13;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "Explanation":
                if (orderSelectionColumn == 14)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  Capacity.Text = "<a>Kapasite Durumu  <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 14;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "WorkTime":
                if (orderSelectionColumn == 15)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //  Capacity.Text = "<a>Kapasite Durumu  <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Capacity.Text = "<a>Kapasite Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 15;
                    orderSelectionDescAsc = 1;
                }
                break;

            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

      
        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    private void BindListTable(ParserListErrorLogs localLogs)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            TableRow[] Row = new TableRow[localLogs.ErrorLogs.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("LocalLogId", localLogs.ErrorLogs[i].Id);

                TableCell indexCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell logDateCell = new TableCell();
                TableCell processDateCell = new TableCell();
                TableCell deviceTypeCell = new TableCell();
                TableCell errorTypeCell = new TableCell();
                TableCell stateTypeCell = new TableCell();
                TableCell repairDateCell = new TableCell();
                TableCell technicianCell = new TableCell();
                TableCell explanationCell = new TableCell();
                TableCell isNotifiedCell = new TableCell();
                TableCell notificationDateCell = new TableCell();
                TableCell notificationTypeCell = new TableCell();
                TableCell approvedUser = new TableCell();
                TableCell approvedDate = new TableCell();
                TableCell calculatedTime = new TableCell();
                TableCell process = new TableCell();

                indexCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                logDateCell.CssClass = "inputTitleCell4";
                processDateCell.CssClass = "inputTitleCell4";
                deviceTypeCell.CssClass = "inputTitleCell4";
                errorTypeCell.CssClass = "inputTitleCell4";
                stateTypeCell.CssClass = "inputTitleCell4";
                repairDateCell.CssClass = "inputTitleCell4";
                technicianCell.CssClass = "inputTitleCell4";
                explanationCell.CssClass = "inputTitleCell4";
                isNotifiedCell.CssClass = "inputTitleCell4";
                notificationDateCell.CssClass = "inputTitleCell4";
                notificationTypeCell.CssClass = "inputTitleCell4";
                approvedUser.CssClass = "inputTitleCell4";
                approvedDate.CssClass = "inputTitleCell4";
                calculatedTime.CssClass = "inputTitleCell4";
                process.CssClass = "inputTitleCell4";

                kioskNameCell.HorizontalAlign = HorizontalAlign.Left;
                kioskNameCell.Width = Unit.Pixel(200);
                logDateCell.Width = Unit.Pixel(150);
                processDateCell.Width = Unit.Pixel(150);

                indexCell.Text = (startIndex + i).ToString();
                //kioskNameCell.Text = localLogs.ErrorLogs[i].KioskId;
                //kioskNameCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editCutOfMonitoringButtonClicked(" + items.Cuts[i].cutId + ");\" class=\"anylink\">" + items.Cuts[i].kioskName + "</a>";

                if (localLogs.ErrorLogs[i].State == "1")
                {
                    kioskNameCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editKioskErrorLogsButtonClicked(" + localLogs.ErrorLogs[i].Id + ");\" class=\"anylink\">" + localLogs.ErrorLogs[i].KioskId + "</a>";
                }
                else
                {
                    kioskNameCell.Text = localLogs.ErrorLogs[i].KioskId ;
                }

                logDateCell.Text = localLogs.ErrorLogs[i].InsertionDate;
                processDateCell.Text = localLogs.ErrorLogs[i].ProcessDate;
                deviceTypeCell.Text = localLogs.ErrorLogs[i].DeviceType;
                errorTypeCell.Text = localLogs.ErrorLogs[i].ErrorType;
                stateTypeCell.Text = localLogs.ErrorLogs[i].State;
                //repairDateCell.Text = localLogs.ErrorLogs[i].RepairTime;
                repairDateCell.Text = "-";
                technicianCell.Text = localLogs.ErrorLogs[i].RepairOperatorName;
                explanationCell.Text = localLogs.ErrorLogs[i].Explanation;
                isNotifiedCell.Text = localLogs.ErrorLogs[i].IsNotified;
                notificationDateCell.Text = localLogs.ErrorLogs[i].NotificationDate;
                notificationTypeCell.Text = localLogs.ErrorLogs[i].NotificationType;
                approvedUser.Text = localLogs.ErrorLogs[i].ApprovedUser;
                approvedDate.Text = localLogs.ErrorLogs[i].ApprovedDate;
                calculatedTime.Text = localLogs.ErrorLogs[i].CalculatedTime;

                if (localLogs.ErrorLogs[i].State == "0")
                {
                    stateTypeCell.Text = "Müdahale edilmedi";
                }
                else
                {
                    stateTypeCell.Text = "Müdahale edildi";
                }

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
						kioskNameCell,
                        logDateCell,
                        processDateCell,
                        deviceTypeCell,
                        errorTypeCell,
                        isNotifiedCell,
                        notificationDateCell,
                        notificationTypeCell,
                        stateTypeCell,
                        repairDateCell,
                        technicianCell,
                        approvedUser,
                        approvedDate,
                        explanationCell,
                        calculatedTime,
                        process
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (localLogs.recordCount < currentRecordEnd)
                currentRecordEnd = localLogs.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + localLogs.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(localLogs.pageCount, this.pageNum, localLogs.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }



    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            //ViewState["NumberOfItem"] = this.numberOfItemField.Text;
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListErrorLogs logs = callWebServ.CallListErrorLogsService(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             kioskIds,
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             Convert.ToInt32(this.ProcessType.SelectedValue),
                                                                              orderSelectionColumn,
                                                                       Convert.ToInt32(ViewState["OrderDesc"]),
                                                                             2);



            if (logs != null)
            {
                if (logs.errorCode == 0)
                {

                    ExportExcellDatas exportExcell = new ExportExcellDatas();

                    string[] headerNames = { "Kiosk No", "Log Zamanı ", "İşlem Zamanı ", "Cihaz Tipi ", "Hata Tipi", "Hata Tipi No",
                                               "Müdahale Durumu ", "Müdahale Zamanı", "Teknisyen 1 ", "Teknisyen 2", "Açıklama", 
                                               "Kiosk Adı", "Bildirim", "Bildirim Tarihi ", "Bildirim Tipi ",
                                               "Onaylayan", "Onaylama Zamanı ","Son Çalışma Zamanı"};

                    exportExcell.ExportExcellByBlock(logs.ErrorLogs, "Kiosk Aygıt Hataları", headerNames);
                }
                else if (logs.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + logs.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelMutabakat");
        }
    }
    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            //ViewState["NumberOfItem"] = this.numberOfItemField.Text;
            int recordCount = 0;
            int pageCount = 0;

            

            CallWebServices callWebServ = new CallWebServices();
            ParserListErrorLogs logs = callWebServ.CallListErrorLogsService(this.searchTextField.Text,
                                                                             Convert.ToDateTime(this.startDateField.Text),
                                                                             Convert.ToDateTime(this.endDateField.Text),
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             kioskIds,
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id,
                                                                             Convert.ToInt32(this.ProcessType.SelectedValue),
                                                                             orderSelectionColumn,
                                                                             descAsc, 
                                                                             1);


            if (logs != null)
            {
                if (logs.errorCode == 0)
                {
                    this.BindListTable(logs);
                }
                else if (logs.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + logs.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionSrch");
        }
    }
}