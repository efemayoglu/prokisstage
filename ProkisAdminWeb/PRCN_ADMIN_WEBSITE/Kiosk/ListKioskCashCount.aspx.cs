﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Kiosk_ListKioskCashCount : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int kioskId, operationType, emptyKioskId;
    private int operationId;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 1;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;


    public string insertDate;
    private ParserGetKiosk kiosk;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];


        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Kiosk/ListKioskCashCount.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        //if (this.ownSubMenuIndex == -1)
        //{
        //    Response.Redirect("../Default.aspx", true);
        //}

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        this.kioskId = Convert.ToInt32(Request.QueryString["kioskId"]);
        this.operationType = Convert.ToInt32(Request.QueryString["operationType"]);
        this.operationId = Convert.ToInt32(Request.QueryString["operationId"]);

        this.numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            ViewState["OrderColumn"] = 1;
            ViewState["OrderDesc"] = 1;

            this.BuildControls();
        }
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    private void ExportToExcel()
    {
        try
        {
            int itemId = 0;

            int whichOperation = 0;

            if (operationType == 1)
            {
                //Doldurulan paraların bulunması
                if (this.operationId == 0)
                {
                    if (this.kioskId != 0)
                    {
                        whichOperation = 1;
                        itemId = this.kioskId;
                    }
                }
                else
                {
                    if (this.kioskId == 0)
                    {
                        whichOperation = 1;
                        itemId = this.operationId;
                    }
                }
            }
            else if (operationType == 0)
            {
                //Fark detayının bulunması,


            }

            if (whichOperation != 0)
            {
                CallWebServices callWebService = new CallWebServices();
                ParserListCashCounts lists = callWebService.CallGetCashCountForExcelService(whichOperation,
                                                                                             itemId,
                                                                                             this.orderSelectionColumn,
                                                                                             this.orderSelectionDescAsc, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

                if (lists != null)
                {
                    if (lists.errorCode == 0)
                    {

                        ExportExcellDatas exportExcell = new ExportExcellDatas();
                        exportExcell.ExportExcell(lists.cashCount, lists.recordCount, "Kiosk Anlık Para Adetleri Listesi");
                    }
                    else if (lists.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                    {
                        Session.Abandon();
                        Session.RemoveAll();
                        Response.Redirect("../root/Login.aspx", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + lists.errorDescription + "'); ", true);
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Bir Sorun Oluştu !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelListKioskCashCount");
        }
    }

    private void BuildControls()
    {
        CallWebServices callWebServ = new CallWebServices();
        if (this.kioskId != 0)
        {
            this.kiosk = callWebServ.CallGetKioskService(this.kioskId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        }

        List(1, 1);
    }

    public void List(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int itemId = 0;

            int whichOperation = 0;

            //Merkezi para
            if (operationType == 1)
            {
                //Doldurulan paraların bulunması
                if (this.kioskId != 0)
                {
                    //ListKioskConditions.aspx
                    if (this.kioskId == this.operationId)
                    {
                        whichOperation = 1;
                        itemId = this.kioskId;
                    }
                    //ListApprovedCollectedMoney.aspx
                    else
                    {
                        whichOperation = 2;
                        itemId = this.operationId;
                    }
                }
                else
                {
                    //ListKioskCashEmpty .aspx (sıfırlama)
                    if (this.kioskId == 0)
                    {
                        whichOperation = 2;
                        itemId = this.operationId;
                    }
                }

            }
            else if (operationType == 0)
            {
                //Fark detayının bulunması,
                whichOperation = 3;
                itemId = this.operationId;
            }
            else if (operationType == 3)
            {
                //Toplam Sayılar,
                whichOperation = 4;
                itemId = this.operationId;
            }
            else if (operationType == 4)
            {
                //Toplam Fark,
                whichOperation = 5;
                itemId = this.operationId;
            }


            if (whichOperation != 0)
            {
                CallWebServices callWebService = new CallWebServices();
                ParserListCashCounts listCashCounts = callWebService.CallGetCashCountService(whichOperation,
                                                                                             itemId,
                                                                                             this.numberOfItemsPerPage,
                                                                                             this.pageNum,
                                                                                             this.orderSelectionColumn,
                                                                                             this.orderSelectionDescAsc, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                if (listCashCounts != null)
                {
                    if (listCashCounts.errorCode == 0)
                    {
                        this.BindListTable(listCashCounts);
                    }
                    else if (listCashCounts.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                    {
                        Session.Abandon();
                        Session.RemoveAll();
                        Response.Redirect("../root/Login.aspx", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + listCashCounts.errorDescription + "'); ", true);
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Bir Sorun Oluştu !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskMoneyDetailSrch");
        }
    }

    private void BindListTable(ParserListCashCounts list)
    {
        try
        {
            double sumAll = 0;
            double countAll = 0;

            TableRow[] Row = new TableRow[list.cashCount.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                //Row[i].Attributes.Add("transactionId", listKioskMoneyDetail.kioskMoneyDetail[i].TransactionId.ToString());

                TableCell indexCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                //TableCell cashTypeCell = new TableCell();
                TableCell cashCountCell = new TableCell();

                TableCell cashTypeNameCell = new TableCell();
                TableCell sumCell = new TableCell();


                TableCell fakeCell1 = new TableCell();
                TableCell fakeCell2 = new TableCell();

                indexCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                //cashTypeCell.CssClass = "inputTitleCell4";
                cashCountCell.CssClass = "inputTitleCell4";

                cashTypeNameCell.CssClass = "inputTitleCell4";
                sumCell.CssClass = "inputTitleCell4";

                fakeCell1.CssClass = "inputTitleCell4";
                fakeCell2.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                // kioskNameCell.Text = list.cashCount[i].KioskName;
                cashTypeNameCell.Text = list.cashCount[i].cashTypeName;
                cashCountCell.Text = list.cashCount[i].cashCount.ToString() + "  Adet";


                string[] words = cashTypeNameCell.Text.Split(' ');

                // O satıra ait toplam (sumCell)
                if (cashTypeNameCell.Text.Contains("Krs"))
                {
                    sumCell.Text = ((Convert.ToDouble(words[0])) / 100 * (list.cashCount[i].cashCount)).ToString();
 
                    sumCell.Text = sumCell.Text + " TL";
                    
                }
                else 
                { 

                    sumCell.Text = (Convert.ToDouble(words[0]) * (list.cashCount[i].cashCount)).ToString() + " TL";
                }

                // Genel Toplam (sumAll)
                if (cashTypeNameCell.Text.Contains("Krs"))
                {
                    sumAll = (Convert.ToDouble(words[0]) / 100 * (list.cashCount[i].cashCount)) + sumAll;
                }
                else
                {
                    sumAll = (Convert.ToDouble(words[0]) * (list.cashCount[i].cashCount)) + sumAll;
                }

                
                countAll = (list.cashCount[i].cashCount) + countAll;

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
						cashTypeNameCell,
                        cashCountCell,
                        sumCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);

            TableRow addNewRow = new TableRow();
            TableCell totalMoneyTextCell = new TableCell();
            TableCell totalMoneyCell = new TableCell();
            TableCell spaceCell = new TableCell();
            TableCell countCell = new TableCell();

            spaceCell.CssClass = "inputTitleCell3";
            spaceCell.ColumnSpan = (2);

            totalMoneyTextCell.CssClass = "inputTitleCell3";
            totalMoneyCell.CssClass = "inputTitleCell3";

            countCell.CssClass = "inputTitleCell3";
            countCell.Text = countAll.ToString() + "  Adet";

            spaceCell.Text = " ";


            spaceCell.Text = "Toplam Miktar:";
            totalMoneyCell.Text = sumAll + " TL ";

            addNewRow.Cells.Add(spaceCell);
            addNewRow.Cells.Add(countCell);
            //addNewRow.Cells.Add(totalMoneyTextCell);
            addNewRow.Cells.Add(totalMoneyCell);

            this.itemsTable.Rows.Add(addNewRow);


            if (operationType == 1)
            {
                lblKioskName.Text = "Kiosk Para Merkez Adetleri / " + kiosk.Kiosk.KioskName;

            }
            else if (operationType == 0)
            {
                lblKioskName.Text = "Kiosk Para Fark Adetleri / " + kiosk.Kiosk.KioskName;
            }

            //TableRow pagingRow = new TableRow();
            //TableCell pagingCell = new TableCell();

            //pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            // pagingCell.HorizontalAlign = HorizontalAlign.Right;
            // pagingCell.Text = WebUtilities.GetPagingText(list.pageCount, this.pageNum, list.recordCount);
            // pagingRow.Cells.Add(pagingCell);
            // this.itemsTable.Rows.AddAt(0, pagingRow);
            // this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskMoneyDetailBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.List(4, 1);
    }

    //public void Sort(Object sender, EventArgs e)
    //{
    //    this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
    //    this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
    //    this.pageNum = 0;

    //    switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
    //    {

    //        case "CashTye":
    //            if (orderSelectionColumn == 1)
    //            {
    //                if (orderSelectionDescAsc == 0)
    //                {
    //                    CashTye.Text = "<a>Para Türü    <img src=../images/arrow_up.png border=0/></a>";
    //                    orderSelectionDescAsc = 1;
    //                }
    //                else
    //                {
    //                    CashTye.Text = "<a>Para Türü    <img src=../images/arrow_down.png border=0/></a>";
    //                    orderSelectionDescAsc = 0;
    //                }
    //            }
    //            else
    //            {
    //                CashTye.Text = "<a>Para Türü    <img src=../images/arrow_up.png border=0/></a>";
    //                orderSelectionColumn = 1;
    //                orderSelectionDescAsc = 1;
    //            }
    //            break;
    //        case "CashCount":
    //            if (orderSelectionColumn == 2)
    //            {
    //                if (orderSelectionDescAsc == 0)
    //                {
    //                    CashCount.Text = "<a>Para Adedi    <img src=../images/arrow_up.png border=0/></a>";
    //                    orderSelectionDescAsc = 1;
    //                }
    //                else
    //                {
    //                    CashCount.Text = "<a>Para Adedi    <img src=../images/arrow_down.png border=0/></a>";
    //                    orderSelectionDescAsc = 0;
    //                }
    //            }
    //            else
    //            {
    //                CashCount.Text = "<a>Para Adedi    <img src=../images/arrow_up.png border=0/></a>";
    //                orderSelectionColumn = 2;
    //                orderSelectionDescAsc = 1;
    //            }
    //            break;
    //        default:

    //            orderSelectionColumn = 1;
    //            orderSelectionDescAsc = 1;

    //            break;
    //    }

    //    ViewState["OrderColumn"] = this.orderSelectionColumn;
    //    ViewState["OrderDesc"] = this.orderSelectionDescAsc;

    //    List(this.orderSelectionColumn, this.orderSelectionDescAsc);
    //}
}