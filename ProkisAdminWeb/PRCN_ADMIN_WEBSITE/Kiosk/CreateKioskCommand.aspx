﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CreateKioskCommand.aspx.cs" Inherits="Kiosk_CreateKioskCommand" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="../styles/style.css" />
    <title></title>
    <style type="text/css">
        .auto-style14 {
            font-family: Tahoma;
            font-size: 12px;
            font-weight: bold;
            width: 272px;
            height: 20px;
            border-radius: 5px;
            border: 1px solid #2372BE;
            padding-bottom: 5px;
        }
    </style>

        <script language="javascript" type="text/javascript">

        function editEnterKioskEmptyCashButtonClicked() {
            var retVal = showEnterKioskEmptyCash();
        }

        function strtDateChngd() {
            if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                document.getElementById('startDateField').value = document.getElementById('endDateField').value;
            }

            document.getElementById('navigateButton').click();

        }

        function endDateChngd() {
            if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                document.getElementById('endDateField').value = document.getElementById('startDateField').value;
            }

            //document.getElementById('instutionBox').value == "0";
            //document.getElementById('kioskBox').value == "0";

            document.getElementById('navigateButton').click();
        }

        function insertDateChngd() {
            if (document.getElementById('insertDateField').value < document.getElementById('startDateField').value) {
                document.getElementById('insertDateField').value = document.getElementById('startDateField').value;
            }

            document.getElementById('navigateButton').click();
        }


        function checkForm() {
            var status = true;
            var messageText = "";

            if (document.getElementById('instutionBox').value == "0" && document.getElementById('kioskBox').value == "0") {
                status = false;
                messageText = "Kurum  veya Kiosk Seçiniz !";
            }
            
            else {
                messageText = "";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }

         
        function checkKioskBox() {
          
           
            if (document.getElementById('kioskBox').value == 0) {

                document.getElementById('instutionBox').disabled = false;
            }
            else
            {
                document.getElementById('instutionBox').disabled = true;
            }
                
           // document.getElementById('instutionBox').value = "0";
            
          
        }

        
        function checkInstutionBox() {
           

            if (document.getElementById('instutionBox').value == 0) {

                document.getElementById('kioskBox').disabled = false;
            }
            else {
                document.getElementById('kioskBox').disabled = true;
            }

          
        }

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        //function getDailyAccountActionsClicked(itemId) {
        //    var url = "../Account/KiosCashAcceptor.aspx?itemId=" + itemId;
        //    var retValue = openDialoagWindow(url, 1000, 600, "", "Kiosk Para Alımı");
        //}

        //function getDailyCustomerAccountsClicked(itemId) {
        //    var url = "../Account/KiosCashAcceptor.aspx?itemId=" + itemId + "&isActive=1";
        //    var retValue = openDialoagWindow(url, 700, 600, "", "Kiosk Para Alımı");
        //}

        function searchButtonClicked(sender) {
            document.getElementById('pageNumRefField').value = "0";
            return true;
        }

        function showTransactionDetailClicked(transactionID) {
            var retVal = showTransactionDetailWindow(transactionID);
        }

        function postForPaging(selectedPageNum) {
            document.getElementById('pageNumRefField').value = selectedPageNum;
            document.getElementById('navigateButton').click();
        }



        function validateNo(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server" onsubmit="return prepareFormForSubmitting();">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <div align="center">
            <div>&nbsp;</div>
            <asp:Panel ID="panel1" DefaultButton="saveButton" runat="server" CssClass="containerPanel_95Pxauto">
                <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp</div>
                <div align="left" class="windowTitle_container_autox30">
                    Kiosk Komut Oluşturma<hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>
                <table cellspacing="0" cellpadding="0" border="0" id="upTable" runat="server" style="width: 90%">
                    <tr>
                        <td align="center" valign="top">
                            <table cellpadding="2" cellspacing="2">

                                <tr>
                                    <td align="left" class="auto-style14">Kurum :                                             
                                    </td>
                                    <td>
                                        <asp:ListBox ID="instutionBox" CssClass="inputLine_90x20" runat="server" SelectionMode="Single"
                                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true" onchange="checkInstutionBox();"></asp:ListBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td align="left" class="auto-style14">Kiosk Seçiniz:
                                    </td>
                                    <td align="left" class="auto-style2">
                                        <asp:ListBox ID="kioskBox" CssClass="inputLine_90x20" runat="server" SelectionMode="Single" AutoPostBack="True"
                                            DataTextField="Name" DataValueField="Id" Rows="1" onchange="checkKioskBox();"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="auto-style14">Komut Tipi Seçiniz:
                                    </td>
                                    <td align="left" class="auto-style2">
                                        <asp:ListBox ID="commandTypeBox" CssClass="inputLine_90x20" runat="server" SelectionMode="Single" AutoPostBack="True"
                                            DataTextField="Name" DataValueField="Id" Rows="1"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="auto-style14">Komut Oluşturma Nedeni:</td>
                                    <td align="left" class="auto-style2">
                                        <asp:ListBox ID="CommandReasonBox" runat="server" AutoPostBack="True" CssClass="inputLine_90x20" DataTextField="Name" DataValueField="Id" Rows="1" SelectionMode="Single"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="auto-style14">Açıklama:</td>
                                    <td align="left" class="auto-style2">
                                        <asp:TextBox ID="ExplanationTextBox" runat="server" Height="30px" TextMode="MultiLine" Width="217px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="auto-style14">Komutu Yazınız:
                                    </td>
                                    <td align="left" class="auto-style2">
                                        <asp:TextBox runat="server" ID="CommandTextBox" Height="71px" TextMode="MultiLine" Width="217px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                            OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Kaydet"></asp:Button>

                                    </td>
                                    <td>
                                        <asp:Button ID="cancelButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                            OnClientClick="parent.hideModalPopup2();" Text="Vazgeç"></asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
