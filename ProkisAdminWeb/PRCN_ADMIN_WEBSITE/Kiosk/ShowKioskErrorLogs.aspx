﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowKioskErrorLogs.aspx.cs" Inherits="Kiosk_ShowKioskErrorLogs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />

    <script language="javascript" type="text/javascript">



        function checkForm() {
            var status = true;
            var messageText = "";

            if (document.getElementById('instutionBox').value == "0" && document.getElementById('kioskBox').value == "0") {
                status = false;
                messageText = "Kurum  veya Kiosk Seçiniz !";
            }

            else {
                messageText = "";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }




    </script>
</head>

<body>

    <form id="form1" runat="server">

        <asp:Button ID="navigateButton" runat="server" OnClick="navigateButton_Click" CssClass="dummy" />
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>

        <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel7" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <div align="left" id="articleDetailsTab" class="windowTitle_container_autox30">
                    Kiosk Aygıt Hataları Detay<hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>
                <table border="0" cellpadding="2" cellspacing="0" id="Table1" runat="server">


                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Kiosk:
                        </td>

                        <td>
                            <asp:TextBox ID="kioskTextField" runat="server" CssClass="inputLine_90x20" AutoPostBack="true" OnTextChanged="navigateButton_Click" Enabled="false"></asp:TextBox>
                        </td>

                    </tr>

                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Log Zamanı :
                        </td>

                        <td>
                            <asp:TextBox ID="logDateField" runat="server" CssClass="inputLine_90x20" AutoPostBack="true" OnTextChanged="navigateButton_Click" Enabled="false"></asp:TextBox>

                        </td>

                    </tr>



                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Cihaz Tipi :
                        </td>

                        <td>
                            <asp:TextBox ID="deviceTypeField" runat="server" CssClass="inputLine_90x20" AutoPostBack="true" OnTextChanged="navigateButton_Click" Enabled="false"></asp:TextBox>

                        </td>

                    </tr>

                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Hata Tipi :
                        </td>

                        <td>
                            <asp:TextBox ID="errorTypeField" runat="server" CssClass="inputLine_90x20" AutoPostBack="true" OnTextChanged="navigateButton_Click" Enabled="false"></asp:TextBox>

                        </td>

                    </tr>

                   

                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Açıklama Giriniz:                                             
                        </td>
                        <td>
                            <asp:TextBox TextMode="MultiLine" Style="resize: none; text-align: left;" Height="85" ID="explanationField" CssClass="inputLine_90x20" runat="server"></asp:TextBox>
                        </td>
                    </tr>



                </table>
                <br />
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Kaydet"></asp:Button>

                                <asp:Button ID="cancelButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClientClick="parent.hideModalPopup2_Exit();" Text="Vazgeç"></asp:Button>

                            </td>
                        </tr>
                    </table>
                </td>
            </asp:Panel>
        </div>
    </form>
</body>

</html>
