﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Kiosk;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Kiosk_CreateKioskCommand : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private int ownSubMenuIndex = -1;

    public int commandId;
    private ParserGetKioskCommand commandKiosk;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Kiosk/ListKioskCommand.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd != "1")
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }


        this.commandId = Convert.ToInt32(Request.QueryString["itemID"]);


        if (this.commandId == 0 && ViewState["itemID"] != null)
        {
            this.commandId = Convert.ToInt32(ViewState["itemID"].ToString());
        }



        if (!this.IsPostBack)
        {
            this.BeforeBindCtrls();
        }
    }

    private void BeforeBindCtrls()
    {
        CallWebServices callWebServ = new CallWebServices();
        if (this.commandId != 0)
        {
            this.commandKiosk = callWebServ.CallGetKioskCommandService(this.commandId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        }

        this.BindCtrls();
    }


    private void BindCtrls()
    {
        try
        {
            if (this.commandKiosk != null)
            {
                CallWebServices callWebServ = new CallWebServices();
                ParserListKioskName kioskNames = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                ParserListCommandTypeName commandTypeNames = callWebServ.CallCommandTypeNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                ParserListCommandReasonName commandReasonNames = callWebServ.CallCommandReasonNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

                if (kioskNames != null)
                {
                    this.kioskBox.DataSource = kioskNames.KioskNames;
                    this.kioskBox.DataBind();
                }
                if (commandTypeNames != null)
                {
                    this.commandTypeBox.DataSource = commandTypeNames.CommandTypeNames;
                    this.commandTypeBox.DataBind();
                }
                if (commandReasonNames != null)
                {
                    this.CommandReasonBox.DataSource = commandReasonNames.CommandReasonNames;
                    this.CommandReasonBox.DataBind();
                }
               
                if (instutions != null)
                {
                    this.instutionBox.DataSource = instutions.InstutionNames;
                    this.instutionBox.DataBind();
                }

                ListItem item = new ListItem();
                item.Text = "Kurum Seçiniz";
                item.Value = "0";
                this.instutionBox.Items.Add(item);
                this.instutionBox.SelectedValue = "0";

                ListItem item2 = new ListItem();
                item2.Text = "Kiosk Seçiniz";
                item2.Value = "0";
                this.kioskBox.Items.Add(item2);
                this.kioskBox.SelectedValue = "0";

                this.kioskBox.SelectedValue = this.commandKiosk.KioskCommand.KioskId.ToString();
                kioskBox.Attributes.Add("disabled", "true");

                //this.instutionBox.SelectedValue = this.commandKiosk.KioskCommand.InstitutionId.ToString();
                instutionBox.Attributes.Add("disabled", "true");

                this.commandTypeBox.SelectedValue = this.commandKiosk.KioskCommand.CommandTypeId.ToString();
                //commandTypeBox.Attributes.Add("disabled", "true");

                this.CommandReasonBox.SelectedValue = this.commandKiosk.KioskCommand.commandReasonId.ToString();
                //CommandReasonBox.Attributes.Add("disabled", "true");

                this.ExplanationTextBox.Text = this.commandKiosk.KioskCommand.explanation.ToString();

                this.CommandTextBox.Text = this.commandKiosk.KioskCommand.CommandString.ToString();
            }


            else
            {
                CallWebServices callWebServ = new CallWebServices();
                ParserListKioskName kioskNames = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                ParserListCommandTypeName commandTypeNames = callWebServ.CallCommandTypeNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                ParserListCommandReasonName commandReasonNames = callWebServ.CallCommandReasonNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

                if (kioskNames != null)
                {
                    this.kioskBox.DataSource = kioskNames.KioskNames;
                    this.kioskBox.DataBind();
                }
                if (commandTypeNames != null)
                {
                    this.commandTypeBox.DataSource = commandTypeNames.CommandTypeNames;
                    this.commandTypeBox.DataBind();
                }
                if (commandReasonNames != null)
                {
                    this.CommandReasonBox.DataSource = commandReasonNames.CommandReasonNames;
                    this.CommandReasonBox.DataBind();
                }

                if (instutions != null)
                {
                    this.instutionBox.DataSource = instutions.InstutionNames;
                    this.instutionBox.DataBind();
                }

                ListItem item = new ListItem();
                item.Text = "Tüm Kiosklar";
                item.Value = "0";
                this.kioskBox.Items.Add(item);
                this.kioskBox.SelectedValue = "0";

                ListItem item2 = new ListItem();
                item2.Text = "Kurum Seçiniz";
                item2.Value = "0";
                this.instutionBox.Items.Add(item2);
                this.instutionBox.SelectedValue = "0";
            }


        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CreateKioskCommandBndCntrl");
        }
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        SaveKioskCommand();
    }

    private void SaveKioskCommand()
    {
        if (this.commandId == 0)
        {
            if (this.instutionBox.SelectedValue == "")
            {
                this.instutionBox.SelectedValue = "0";
            }

            if (this.kioskBox.SelectedValue == "")
            {
                this.kioskBox.SelectedValue = "0";
            }


            string expText = ExplanationTextBox.Text.Trim();
            if (expText == null || expText == "") expText = "Açıklama Girilmedi.";

            CallWebServices callWebServ = new CallWebServices();
            ParserSaveKioskCommand parserSaveKioskCommand = callWebServ.CallSaveKioskCommandService(Convert.ToInt32(this.kioskBox.SelectedValue), Convert.ToInt32(this.instutionBox.SelectedValue), this.LoginDatas.User.Id, Convert.ToInt32(this.commandTypeBox.SelectedValue), this.CommandTextBox.Text, this.LoginDatas.AccessToken, this.LoginDatas.User.Id, Convert.ToInt32(this.CommandReasonBox.SelectedValue), expText);

            string scriptText = "parent.showMessageWindowKioskCommand('" + parserSaveKioskCommand.errorDescription + "');";

            ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);
        }
        

        //update
        else
        {
            if (this.instutionBox.SelectedValue == "")
            {
                this.instutionBox.SelectedValue = "0";
            }

            if (this.kioskBox.SelectedValue == "")
            {
                this.kioskBox.SelectedValue = "0";
            }


            string expText = ExplanationTextBox.Text.Trim();
            if (expText == null || expText == "") expText = "Açıklama Girilmedi.";

            CallWebServices callWebServ = new CallWebServices();
            ParserOperation parserUpdateKioskCommand = callWebServ.CallUpdateKioskCommandService(this.commandId,  this.LoginDatas.User.Id, Convert.ToInt32(this.commandTypeBox.SelectedValue), this.CommandTextBox.Text, this.LoginDatas.AccessToken, this.LoginDatas.User.Id, Convert.ToInt32(this.CommandReasonBox.SelectedValue), expText);

            string scriptText = "parent.showMessageWindowKioskCommand('" + parserUpdateKioskCommand.errorDescription + "');";

            ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);
        

        }


    }


}
