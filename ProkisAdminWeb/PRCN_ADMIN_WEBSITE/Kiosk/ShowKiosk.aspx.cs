﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Other;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Kiosk_ShowKiosk : System.Web.UI.Page
{
    public int kioskId;
    private ParserLogin LoginDatas;
    public string merchantName = "";
    private ParserGetKiosk kiosk;
    private ParserListStatusName statusNames;
    private ParserListCity cities;
    private ParserListRegion regions;
    private ParserListCapacity capacities;
    private ParserListKioskTypeName kioskTypes;
    private int ownSubMenuIndex = -1;

    private ParserListConnectionType connectionType;
    private ParserListCardReaderType cardReaderType;
    private ParserListInstutionName institutions;
    private ParserListRiskClass riskClass;  

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Kiosk/ListKiosk.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1" || this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1")
            {
              
            }
            else
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.kioskId = Convert.ToInt32(Request.QueryString["kioskID"]);


        if (this.kioskId == 0 && ViewState["kioskID"] != null)
        {
            this.kioskId = Convert.ToInt32(ViewState["kioskID"].ToString());

        }    

        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }

        
    }

    private void BindCtrls()
    {

        CallWebServices callWebServ = new CallWebServices();
        if (this.kioskId != 0)
        {
            this.kiosk = callWebServ.CallGetKioskService(this.kioskId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        }

        this.statusNames = callWebServ.CallGetKioskStatusNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

        if (statusNames != null)
        {
            this.statusBox.DataSource = this.statusNames.StatusNames;
            this.statusBox.DataBind();
        }

     


        this.cities = callWebServ.CallGetCitiesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        if (cities != null)
        {
            this.cityBox.DataSource = this.cities.Cities;
            this.cityBox.DataBind();
        }

        this.kioskTypes = callWebServ.CallGetKisokTypeNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        if (kioskTypes != null)
        {
            this.kioskType.DataSource = this.kioskTypes.KioskTypeNames;
            this.kioskType.DataBind();
        }

        this.capacities = callWebServ.CallGetCapacitiesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        if (capacities != null)
        {
            this.capacityBox.DataSource = this.capacities.Capacities;
            this.capacityBox.DataBind();
        }


        this.connectionType = callWebServ.CallGetConnectionTypeService( this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        if (connectionType != null)
        {
            this.connectionBox.DataSource = this.connectionType.ConnectionType;
            this.connectionBox.DataBind();
        }

        this.cardReaderType = callWebServ.CallGetCardReaderTypeService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        if (cardReaderType != null)
        {
            this.cardReaderBox.DataSource = this.cardReaderType.CardReaderType;
            this.cardReaderBox.DataBind();
        }

        this.institutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        if (institutions != null)
        {
            this.instutionBox.DataSource = this.institutions.InstutionNames;
            this.instutionBox.DataBind();
        }


        this.riskClass = callWebServ.CallGetRiskClassService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        if (riskClass != null)
        {
            this.riskClassBox.DataSource = this.riskClass.RiskClass;
            this.riskClassBox.DataBind();
        }

         
        ParserListRegion regions = callWebServ.CallGetRegionsService(6, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

        if (regions != null)
        {
            this.regionBox.DataSource = regions.Regions;
            this.regionBox.DataBind();
        }


        if (kioskId == 0)
        {
            usageFee.Enabled = true;
            kioskType.Enabled = true;
            instutionBox.Attributes.Remove("disabled");
            kioskType.Attributes.Remove("disabled");
        }
        else
        {
            usageFee.Enabled = false;
            kioskType.Enabled = false;
            instutionBox.Attributes.Add("disabled", "true");
            kioskType.Attributes.Add("disabled", "true");
        }

        this.SetControls();
    }

    private void SetControls()
    {
        try
        {
            if (this.kiosk != null)
            {
               

             
                this.nameField.Text = this.kiosk.Kiosk.KioskName;
                this.ipField.Text = this.kiosk.Kiosk.KioskIp;
                this.addressField.Text = this.kiosk.Kiosk.KioskAddress;
                this.cityBox.SelectedValue = this.kiosk.Kiosk.CityId.ToString();
                this.regionBox.SelectedValue = this.kiosk.Kiosk.RegionId.ToString();
                this.connectionBox.SelectedValue = this.kiosk.Kiosk.ConnectionTypeId.ToString();
                this.cardReaderBox.SelectedValue = this.kiosk.Kiosk.CardReaderTypeId.ToString();
                this.kioskType.SelectedValue = this.kiosk.Kiosk.KioskTypeId.ToString();
                this.statusBox.SelectedValue = this.kiosk.Kiosk.KioskStatusId.ToString();
                this.capacityBox.SelectedValue = this.kiosk.Kiosk.CashBoxAcceptorTypeId;
                
                this.insertionDateField.Text = this.kiosk.Kiosk.KioskInsertionDate;
                this.latitudeField.Text = this.kiosk.Kiosk.Latitude;
                this.longitudeField.Text = this.kiosk.Kiosk.Longitude;
                this.usageFee.Text = this.kiosk.Kiosk.UsageFee.ToString();
                this.txtExplanation.Text = this.kiosk.Kiosk.Explanation;
                
                this.instutionBox.SelectedValue = this.kiosk.Kiosk.institutionId.ToString();
                instutionBox.Enabled = false;
                //instutionBox. = "None";

                if (this.kiosk.Kiosk.IsCreditCard == "1")
                {
                    this.creditChechBox.Checked = true;
                }
                else
                {
                    this.creditChechBox.Checked = false;
                }

                if (this.kiosk.Kiosk.IsMobilePayment == "1")
                {
                    this.mobileChechBox.Checked = true;
                }
                else
                {
                    this.mobileChechBox.Checked = false;
                }



            }
            else
            {
                ListItem itemCity = new ListItem();
                itemCity.Text = "Şehir Seçiniz";
                itemCity.Value = "0";
                this.cityBox.Items.Add(itemCity);
                this.cityBox.SelectedValue = "0";

                ListItem itemRegion = new ListItem();
                itemRegion.Text = "Bölge Seçiniz";
                itemRegion.Value = "0";
                this.regionBox.Items.Add(itemRegion);
                this.regionBox.SelectedValue = "0";

                ListItem itemType= new ListItem();
                itemType.Text = "Kiosk Tipi Seçiniz";
                itemType.Value = "0";
                this.kioskType.Items.Add(itemType);
                this.kioskType.SelectedValue = "0";


                ListItem itemConnection = new ListItem();
                itemConnection.Text = "Bağlantı Seçiniz";
                itemConnection.Value = "0";
                this.connectionBox.Items.Add(itemConnection);
                this.connectionBox.SelectedValue = "0";

                ListItem itemCardReader = new ListItem();
                itemCardReader.Text = "Kart Okuyucu Tipi Seçiniz";
                itemCardReader.Value = "0";
                this.cardReaderBox.Items.Add(itemCardReader);
                this.cardReaderBox.SelectedValue = "0";

                ListItem itemInstitution = new ListItem();
                itemInstitution.Text = "Kurum Giriniz";
                itemInstitution.Value = "0";
                this.instutionBox.Items.Add(itemInstitution);
                this.instutionBox.SelectedValue = "0";


                //ListItem itemStatus = new ListItem();
                //itemStatus.Text = "Durum Seçiniz";
                //itemStatus.Value = "0";
                //this.statusBox.Items.Add(itemStatus);
                //this.statusBox.SelectedValue = "0";


                ListItem itemStatus = new ListItem();
                itemStatus.Text = "Durum Seçiniz";
                itemStatus.Value = "0";
                this.statusBox.Items.Add(itemStatus);
                this.statusBox.SelectedValue = "0";


                ListItem itemRisk = new ListItem();
                itemRisk.Text = "Risk Sınıfı Seçiniz";
                itemRisk.Value = "0";
                this.riskClassBox.Items.Add(itemRisk);
                this.riskClassBox.SelectedValue = "0";

                ListItem itemCapacity = new ListItem();
                itemCapacity.Text = "Para Toplama Haznesini Seçiniz";
                itemCapacity.Value = "0";
                this.capacityBox.Items.Add(itemCapacity);
                this.capacityBox.SelectedValue = "0";



                insertionDateField.Text = DateTime.Now.ToString();


                //ListItem itemConnectionType = new ListItem();
                //itemConnectionType.Text = "Kiosk Bağlantı Tipi Seçiniz";
                //itemConnectionType.Value = "0";
                //this.connectionBox.Items.Add(itemConnectionType);
                //this.connectionBox.SelectedValue = "0";

                //ListItem itemCardReaderType = new ListItem();
                //itemCardReaderType.Text = "Kiosk Kart Okuyucu Tipi Seçiniz";
                //itemCardReaderType.Value = "0";
                //this.cardReaderBox.Items.Add(itemCardReaderType);
                //this.cardReaderBox.SelectedValue = "0";
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowKioskSC");
        }
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        SaveMerchant();
    }

    //private void SaveMerchant()
    //{
    //    try
    //    {
    //        int updateStatus = 0;
    //        CallWebServices callWebServ = new CallWebServices();

    //        if (this.kioskId == 0)
    //        {

    //            if (kiosk == null)
    //            {
    //                insertionDateField.Text = DateTime.Now.ToString();

    //                callWebServ.CallSaveKioskService(this.nameField.Text, this.addressField.Text, this.ipField.Text, Convert.ToInt32(this.statusBox.SelectedValue), this.LoginDatas.User.Id, this.latitudeField.Text, this.longitudeField.Text, Convert.ToInt32(this.regionBox.SelectedValue), this.usageFee.Text, Convert.ToInt32(this.kioskType.SelectedValue), this.LoginDatas.AccessToken, this.LoginDatas.User.Id, Convert.ToInt32(this.cityBox.SelectedValue), this.insertionDateField.Text, Convert.ToInt32(this.capacityBox.SelectedValue), this.txtExplanation.Text, Convert.ToInt32(this.connectionBox.SelectedValue), Convert.ToInt32(this.cardReaderBox.SelectedValue), Convert.ToInt32(this.instutionBox.SelectedValue), Convert.ToInt32(this.riskClassBox.SelectedValue));
    //            }
    //            else
    //            {
    //                updateStatus = -10;
    //            }
    //        }
    //        else
    //        {
    //            callWebServ.CallUpdateKioskService(this.kioskId, this.nameField.Text, this.addressField.Text, this.ipField.Text, Convert.ToInt32(this.statusBox.SelectedValue), this.LoginDatas.User.Id, this.latitudeField.Text, this.longitudeField.Text, Convert.ToInt32(this.regionBox.SelectedValue), this.usageFee.Text, Convert.ToInt32(this.kioskType.SelectedValue), this.LoginDatas.AccessToken, this.LoginDatas.User.Id, Convert.ToInt32(this.cityBox.SelectedValue), this.insertionDateField.Text, Convert.ToInt32(this.capacityBox.SelectedValue), this.txtExplanation.Text, Convert.ToInt32(this.connectionBox.SelectedValue), Convert.ToInt32(this.cardReaderBox.SelectedValue), Convert.ToInt32(this.instutionBox.SelectedValue), Convert.ToInt32(this.riskClassBox.SelectedValue));
    //        }

    //        string scriptText = "";

    //        switch (updateStatus)
    //        {
    //            case (int)ManagementScreenErrorCodes.SystemError:
    //                this.messageArea.InnerHtml = "System error has occurred. Try again.";
    //                break;
    //            case (int)ManagementScreenErrorCodes.SQLServerError:
    //                this.messageArea.InnerHtml = "System error has occurred. Try again. (SQL Server Error)";
    //                break;
    //            case -2:
    //                this.messageArea.InnerHtml = "A record already exists.";
    //                break;
    //            default:
    //                this.messageArea.InnerHtml = "İşlem Başarılı.";
    //                //ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:callOwn(); ", true);
    //                //ViewState["merchantID"] = this.merchantID;
    //                scriptText = "parent.showMessageWindowKiosk('İşlem Başarılı.');";
    //                break;
    //        }
    //        if (scriptText != "")
    //        {
    //            ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);
    //        }
    //    }
    //    catch (Exception exp)
    //    {
    //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowKioskSv");
    //    }
    //}


    private void SaveMerchant()
    {
        try
        {

            int updateStatus = 0;
            CallWebServices callWebServ = new CallWebServices();
            string scriptText = "";

            if (this.kioskId == 0)
            {
                messageArea.InnerText = "";


                if (kiosk == null)
                {
                    if (cityBox.SelectedValue == "0")
                    {
                        messageArea.InnerText = "Şehir Seçiniz.";
                    }

                    else if (regionBox.SelectedValue == "0")
                    {
                        messageArea.InnerText = "Lokasyon Türü Seçiniz.";
                    }

                    else if (regionBox.SelectedValue == "0")
                    {
                        messageArea.InnerText = "Lokasyon Türü Seçiniz.";
                    }
                    else if (statusBox.SelectedValue == "0")
                    {
                        messageArea.InnerText = "Durum Seçiniz.";
                    }
                    else if (kioskType.SelectedValue == "0")
                    {
                        messageArea.InnerText = "Kiosk Tipi Seçiniz.";
                    }
                    else if (instutionBox.SelectedValue == "0")
                    {
                        messageArea.InnerText = "Kurum Seçiniz.";
                    }
                    else if (connectionBox.SelectedValue == "0")
                    {
                        messageArea.InnerText = "Bağlantı Türü Seçiniz.";
                    }
                    else if (cardReaderBox.SelectedValue == "0")
                    {
                        messageArea.InnerText = "Kart Tipi Seçiniz.";
                    }
                    else if (riskClassBox.SelectedValue == "0")
                    {
                        messageArea.InnerText = "Risk Sınıfını Seçiniz.";
                    }
                    else if (capacityBox.SelectedValue == "0")
                    {
                        messageArea.InnerText = "Kapasite Türünü Seçiniz.";
                    }
                    else if (string.IsNullOrEmpty(usageFee.Text))
                    {
                        messageArea.InnerText = "Kullanım Ücretini Giriniz.";
                    }

                     int isCredit = 0;
                     if (creditChechBox.Checked)
                     {
                         isCredit=1;
                     }


                      int isMobile = 0;
                      if (mobileChechBox.Checked)
                      {
                          isMobile = 1;
                      }



                    //eğer kişi bütün alanları girmişse save eder
                    else
                    {
                        insertionDateField.Text = DateTime.Now.ToString();

                        callWebServ.CallSaveKioskService(this.nameField.Text, this.addressField.Text, this.ipField.Text, Convert.ToInt32(this.statusBox.SelectedValue), this.LoginDatas.User.Id, this.latitudeField.Text, this.longitudeField.Text, Convert.ToInt32(this.regionBox.SelectedValue), this.usageFee.Text, Convert.ToInt32(this.kioskType.SelectedValue), this.LoginDatas.AccessToken, this.LoginDatas.User.Id, Convert.ToInt32(this.cityBox.SelectedValue), this.insertionDateField.Text, Convert.ToInt32(this.capacityBox.SelectedValue), this.txtExplanation.Text, Convert.ToInt32(this.connectionBox.SelectedValue), Convert.ToInt32(this.cardReaderBox.SelectedValue), Convert.ToInt32(this.instutionBox.SelectedValue), Convert.ToInt32(this.riskClassBox.SelectedValue), isCredit, isMobile);
                        //messageArea.InnerText = "İşlem Başarılı..";
                        //scriptText = "parent.showMessageWindowKiosk('İşlem Başarılı.');";

                        switch (updateStatus)
                        {
                            case (int)ManagementScreenErrorCodes.SystemError:
                                this.messageArea.InnerHtml = "System error has occurred. Try again.";
                                break;
                            case (int)ManagementScreenErrorCodes.SQLServerError:
                                this.messageArea.InnerHtml = "System error has occurred. Try again. (SQL Server Error)";
                                break;
                            case -2:
                                this.messageArea.InnerHtml = "A record already exists.";
                                break;
                            default:
                                this.messageArea.InnerHtml = "İşlem Başarılı.";
                                //ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:callOwn(); ", true);
                                //ViewState["merchantID"] = this.merchantID;
                                scriptText = "parent.showMessageWindowKiosk('İşlem Başarılı.');";
                                break;
                        }
                    }
                }
                else
                {
                    updateStatus = -10;
                }
            }
            else
            {
                int isCredit = 0;
                if (creditChechBox.Checked)
                {
                    isCredit = 1;
                }


                int isMobile = 0;
                if (mobileChechBox.Checked)
                {
                    isMobile = 1;
                }


                callWebServ.CallUpdateKioskService(this.kioskId, this.nameField.Text, this.addressField.Text, this.ipField.Text, Convert.ToInt32(this.statusBox.SelectedValue), this.LoginDatas.User.Id, this.latitudeField.Text, this.longitudeField.Text, Convert.ToInt32(this.regionBox.SelectedValue), this.usageFee.Text, Convert.ToInt32(this.kioskType.SelectedValue), this.LoginDatas.AccessToken, this.LoginDatas.User.Id, Convert.ToInt32(this.cityBox.SelectedValue), this.insertionDateField.Text, Convert.ToInt32(this.capacityBox.SelectedValue), this.txtExplanation.Text, Convert.ToInt32(this.connectionBox.SelectedValue), Convert.ToInt32(this.cardReaderBox.SelectedValue), Convert.ToInt32(this.instutionBox.SelectedValue), Convert.ToInt32(this.riskClassBox.SelectedValue), isCredit,isMobile);

                switch (updateStatus)
                {
                    case (int)ManagementScreenErrorCodes.SystemError:
                        this.messageArea.InnerHtml = "System error has occurred. Try again.";
                        break;
                    case (int)ManagementScreenErrorCodes.SQLServerError:
                        this.messageArea.InnerHtml = "System error has occurred. Try again. (SQL Server Error)";
                        break;
                    case -2:
                        this.messageArea.InnerHtml = "A record already exists.";
                        break;
                    default:
                        this.messageArea.InnerHtml = "İşlem Başarılı.";
                        //ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:callOwn(); ", true);
                        //ViewState["merchantID"] = this.merchantID;
                        scriptText = "parent.showMessageWindowKiosk('İşlem Başarılı.');";
                        break;
                }

            }

            //string scriptText = "";

            //switch (updateStatus)
            //{
            //    case (int)ManagementScreenErrorCodes.SystemError:
            //        this.messageArea.InnerHtml = "System error has occurred. Try again.";
            //        break;
            //    case (int)ManagementScreenErrorCodes.SQLServerError:
            //        this.messageArea.InnerHtml = "System error has occurred. Try again. (SQL Server Error)";
            //        break;
            //    case -2:
            //        this.messageArea.InnerHtml = "A record already exists.";
            //        break;
            //    default:
            //        this.messageArea.InnerHtml = "İşlem Başarılı.";
            //        //ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:callOwn(); ", true);
            //        //ViewState["merchantID"] = this.merchantID;
            //        scriptText = "parent.showMessageWindowKiosk('İşlem Başarılı.');";
            //        break;
            //}


            if (scriptText != "")
            {
                ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowKioskSv");
        }
    }


    /*
    protected void cityBox_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cityBox.SelectedValue == "0")
        {
            regionBox.Items.Clear();

            ListItem itemRegion = new ListItem();
            itemRegion.Text = "Bölge Seçiniz";
            itemRegion.Value = "0";
            this.regionBox.Items.Add(itemRegion);
            this.regionBox.SelectedValue = "0";
        }
        else
        {
            regionBox.Items.Clear();

            CallWebServices callWebServ = new CallWebServices();
            this.regions = callWebServ.CallGetRegionsService(Convert.ToInt32(cityBox.SelectedValue), this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (regions != null)
            {
                this.regionBox.DataSource = this.regions.Regions;
                this.regionBox.DataBind();

                ListItem itemRegion = new ListItem();
                itemRegion.Text = "Bölge Seçiniz";
                itemRegion.Value = "0";
                this.regionBox.Items.Add(itemRegion);
                this.regionBox.SelectedValue = "0";
            }
        }
    }
     
    protected void cancelButton_Click(object sender, EventArgs e)
    {

    }
    */
}