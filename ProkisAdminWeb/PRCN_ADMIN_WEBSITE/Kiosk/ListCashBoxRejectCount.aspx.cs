﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Kiosk_ListCashBoxRejectCount : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int kioskId;
    private int operationId;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 1;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    //private ParserGetCashBoxRejectCounts cashBoxReject;
    

    protected void Page_Load(object sender, EventArgs e)
    {

        this.LoginDatas = (ParserLogin)Session["LoginData"];


        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/ListFullCashBox.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        this.kioskId = Convert.ToInt32(Request.QueryString["kioskId"]);

        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }

    //protected void excelButton_Click(object sender, EventArgs e)
    //{
    //    //ExportToExcel();
    //}


    private void BindCtrls()
    {

        CallWebServices callWebServ = new CallWebServices();


        if (this.kioskId != 0)
        {
            //this.cashBoxReject = callWebServ.CallGetCashBoxRejectService(this.kioskId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        }

        //this.SetControls();
    }

}