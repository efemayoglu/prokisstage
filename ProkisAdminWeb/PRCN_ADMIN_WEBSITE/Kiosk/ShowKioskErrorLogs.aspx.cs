﻿using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Account;
using PRCNCORE.Parser.Kiosk;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Kiosk_ShowKioskErrorLogs : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private int ownSubMenuIndex = -1;
    
    public int errorId;
    private ParserGetKioskErrorLogs errorKiosk;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Kiosk/ListKioskErrorLogs.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }

        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }

        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.errorId = Convert.ToInt32(Request.QueryString["errorId"]);

        if (this.errorId == 0 && ViewState["errorId"] != null)
        {
            this.errorId = Convert.ToInt32(ViewState["errorId"].ToString());
        }


        if (!Page.IsPostBack)
        {
            this.BindCtrls();
        }

    }

    private void BindCtrls()
    {
        CallWebServices callWebServ = new CallWebServices();
        if (this.errorId != 0)
        {
            this.errorKiosk = callWebServ.CallGetKioskErrorLogsService(this.errorId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        }

        this.SetControls();
    }


    private void SetControls()
    {
        this.kioskTextField.Text = this.errorKiosk.Errors.KioskId;
        this.logDateField.Text = this.errorKiosk.Errors.NotificationDate;
        this.deviceTypeField.Text = this.errorKiosk.Errors.DeviceType;
        this.errorTypeField.Text = this.errorKiosk.Errors.ErrorType;
        this.explanationField.Text = this.errorKiosk.Errors.Explanation;
    }


    protected void saveButton_Click(object sender, EventArgs e)
    {
        SaveKioskErrorLogs();
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    { 
    
    }

    private void SaveKioskErrorLogs()
    {

        try
        {
            
             CallWebServices callWebServ = new CallWebServices();

             ParserOperation parserSaveCutOfMonitoring = callWebServ.CallUpdateKioskErrorLogsService(errorId,
                                                                                                       explanationField.Text,
                                                                                                       this.LoginDatas.User.Id,
                                                                                                       this.LoginDatas.AccessToken);


                if (parserSaveCutOfMonitoring != null)
                {
                    string scriptText = "";

                    //this.messageArea.InnerHtml = parserSaveControlCashNotification.errorDescription;

                    scriptText = "parent.showMessageWindowKioskErrorLogs('İşlem Başarılı.');";
                    ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);

                }
                else if (parserSaveCutOfMonitoring.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "showAlert('Sisteme Erişilemiyor !'); ", true);
                }
          
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "Kiosk");
        }
    }
}