﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Kiosk_ListKioskConditionDetails : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int kioskId;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Kiosk/ListKioskConditions.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        this.kioskId = Convert.ToInt32(Request.QueryString["kioskId"]);

        this.numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            this.BuildControls();
        }
    }

    private void BuildControls()
    {
        ListDetails(4, 1);
    }

    public void ListDetails(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebService = new CallWebServices();
            ParserListKioskConditionDetail listKioskMoneyDetail = callWebService.CallListKioskConditionDetailService(this.kioskId,
                                                                                                                     this.numberOfItemsPerPage,
                                                                                                                     this.pageNum,
                                                                                                                     recordCount,
                                                                                                                     pageCount, 
                                                                                                                     this.LoginDatas.AccessToken, 
                                                                                                                     this.LoginDatas.User.Id);
            if (listKioskMoneyDetail != null)
            {
                if (listKioskMoneyDetail.errorCode == 0)
                {
                    this.BindListTable(listKioskMoneyDetail);
                }
                else if (listKioskMoneyDetail.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + listKioskMoneyDetail.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskMoneyDetailSrch");
        }
    }

    private void BindListTable(ParserListKioskConditionDetail listKioskMoneyDetail)
    {
        try
        {
            TableRow[] Row = new TableRow[listKioskMoneyDetail.kioskConditionDetail.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                //Row[i].Attributes.Add("transactionId", listKioskMoneyDetail.kioskMoneyDetail[i].TransactionId.ToString());

                TableCell indexCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell amountCell = new TableCell();
                TableCell transactionIdCell = new TableCell();
                TableCell transactionDateCell = new TableCell();
                TableCell cashCountCell = new TableCell();
                TableCell coinCountCell = new TableCell();
                TableCell ribbonLenCell = new TableCell();
                TableCell receivedAmountCell = new TableCell();
                TableCell cashTypeCell = new TableCell();
                TableCell aboneNoCell = new TableCell();

                indexCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                amountCell.CssClass = "inputTitleCell4";
                transactionIdCell.CssClass = "inputTitleCell4";
                transactionDateCell.CssClass = "inputTitleCell4";
                cashCountCell.CssClass = "inputTitleCell4";
                coinCountCell.CssClass = "inputTitleCell4";
                ribbonLenCell.CssClass = "inputTitleCell4";
                receivedAmountCell.CssClass = "inputTitleCell4";
                cashTypeCell.CssClass = "inputTitleCell4";
                aboneNoCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                kioskNameCell.Text = listKioskMoneyDetail.kioskConditionDetail[i].kioskName;
                amountCell.Text = listKioskMoneyDetail.kioskConditionDetail[i].amount + "  TL"; ;
                transactionIdCell.Text = listKioskMoneyDetail.kioskConditionDetail[i].transactionId.ToString();
                transactionDateCell.Text = listKioskMoneyDetail.kioskConditionDetail[i].updatedDate.ToString();
                cashCountCell.Text = listKioskMoneyDetail.kioskConditionDetail[i].cashCount.ToString() + "  Adet";
                coinCountCell.Text = listKioskMoneyDetail.kioskConditionDetail[i].coinCount.ToString() + "  Adet";
                int billCount = listKioskMoneyDetail.kioskConditionDetail[i].ribbonRate / 17;
                ribbonLenCell.Text = billCount.ToString() + "  Adet";
                receivedAmountCell.Text = listKioskMoneyDetail.kioskConditionDetail[i].receivedAmount;
                cashTypeCell.Text = listKioskMoneyDetail.kioskConditionDetail[i].cashTypeName;
                aboneNoCell.Text = listKioskMoneyDetail.kioskConditionDetail[i].aboneNo;




                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
						kioskNameCell,
                        amountCell,
                        cashCountCell,
                        coinCountCell,
                        ribbonLenCell,
                        receivedAmountCell,
                        cashTypeCell,
                        transactionIdCell,
                        aboneNoCell,
                        transactionDateCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (listKioskMoneyDetail.recordCount < currentRecordEnd)
                currentRecordEnd = listKioskMoneyDetail.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + listKioskMoneyDetail.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(listKioskMoneyDetail.pageCount, this.pageNum, listKioskMoneyDetail.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskMoneyDetailBDT");
        }
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    private void ExportToExcel()
    {
        try
        {
            CallWebServices callWebService = new CallWebServices();
            ParserListKioskConditionDetail lists = callWebService.CallListKioskConditionDetailForExcelService(this.kioskId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (lists != null)
            {
                if (lists.errorCode == 0)
                {

                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    exportExcell.ExportExcell(lists.kioskConditionDetail, lists.recordCount, "Kiosk Para Akış Listesi");
                }
                else if (lists.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + lists.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelMutabakat");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.ListDetails(4, 1);
    }
}