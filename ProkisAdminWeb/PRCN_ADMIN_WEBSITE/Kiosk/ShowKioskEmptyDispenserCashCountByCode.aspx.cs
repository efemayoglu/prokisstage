﻿using System;
using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Kiosk_ShowKioskEmptyDispenserCashCountByCode : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private int ownSubMenuIndex = -1;
    // private ParserListKioskName kiosks;

    public int kioskId;
    
    private ParserGetKiosk kiosk;

    private ParserListEmptyKioskNames emptyKiosks;
    double totalValue;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Account/ListApprovedCollectedMoney.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }

        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }

        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1" || this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1")
            {

            }
            else
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }


        //this.emptyKioskId = Convert.ToInt32(Request.QueryString["emptyKioskId"]);
        this.kioskId = Convert.ToInt32(Request.QueryString["kioskID"]);
        //this.insertDate = ((Request.QueryString["insertDate"])).ToString();

        if (this.kioskId == 0 && ViewState["kioskID"] != null)
        {
            this.kioskId = Convert.ToInt32(ViewState["kioskID"].ToString());

        }

        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }


    private void BindCtrls()
    {
        CallWebServices callWebServ = new CallWebServices();
        if (this.kioskId != 0)
        {
            this.kiosk = callWebServ.CallGetKioskService(this.kioskId, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
        }

        this.SetControls();
    }



    private void SetControls()
    {
        if (kiosk.Kiosk == null)
        {
            txtkiosk.Text = "Kiosk Bulunamadı"; // +" , Giriş Zamanı: " + "Tarih kaydı yok ";
        }
        else
            txtkiosk.Text = kiosk.Kiosk.KioskName; //+ " , Giriş Zamanı: " + insertDate;

        try
        {
            //ListItem item = new ListItem();
            //item.Text = "Kiosk Seçiniz";
            //item.Value = "0";
            //this.kioskBox.Items.Add(item);
            //this.kioskBox.SelectedValue = "0";

            this.TotalAmountTb.Text = "0";
            this.CashYpeTB1.Text = "0";
            this.CashYpeTB2.Text = "0";
            this.CashYpeTB3.Text = "0";
            this.CashYpeTB4.Text = "0";
            this.CashYpeTB5.Text = "0";
            this.CashYpeTB6.Text = "0";
            this.CashYpeTB7.Text = "0";
            this.CashYpeTB8.Text = "0";
            this.CashYpeTB9.Text = "0";
            this.CashYpeTB10.Text = "0";
            this.CashYpeTB11.Text = "0";


        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "EnterKioskEmptyDispenserCashCountSC");
        }
    }

    protected void CashYpeTB1_TextChanged(object sender, EventArgs e)
    {
        TextBox xsenderTB = (TextBox)sender;

        decimal output;
        bool result = Decimal.TryParse(xsenderTB.Text, out output);

        if (String.IsNullOrEmpty(xsenderTB.Text))
            xsenderTB.Text = "0";

        if (result)
        {
            double totalAmount = (Convert.ToInt32(this.CashYpeTB1.Text) * 200 +
                                  Convert.ToInt32(this.CashYpeTB2.Text) * 100 +
                                  Convert.ToInt32(this.CashYpeTB3.Text) * 50 +
                                  Convert.ToInt32(this.CashYpeTB4.Text) * 20 +
                                  Convert.ToInt32(this.CashYpeTB5.Text) * 10 +
                                  Convert.ToInt32(this.CashYpeTB6.Text) * 5 +
                                  Convert.ToInt32(this.CashYpeTB7.Text) * 1 +
                                  Convert.ToInt32(this.CashYpeTB8.Text) / 2.0 +
                                  Convert.ToInt32(this.CashYpeTB9.Text) / 4.0 +
                                  Convert.ToInt32(this.CashYpeTB10.Text) / 10.0 +
                                  Convert.ToInt32(this.CashYpeTB11.Text) / 20.0);

            totalValue = (Convert.ToInt32(this.CashYpeTB1.Text)   +
                                  Convert.ToInt32(this.CashYpeTB2.Text)  +
                                  Convert.ToInt32(this.CashYpeTB3.Text)  +
                                  Convert.ToInt32(this.CashYpeTB4.Text)  +
                                  Convert.ToInt32(this.CashYpeTB5.Text)  +
                                  Convert.ToInt32(this.CashYpeTB6.Text)  +
                                  Convert.ToInt32(this.CashYpeTB7.Text)  +
                                  Convert.ToInt32(this.CashYpeTB8.Text)  +
                                  Convert.ToInt32(this.CashYpeTB9.Text)  +
                                  Convert.ToInt32(this.CashYpeTB10.Text) +
                                  Convert.ToInt32(this.CashYpeTB11.Text));

            this.TotalAmountTb.Text = totalAmount.ToString();
        }
        else
        {
            this.messageArea.InnerHtml = "Lütfen rakam giriniz. !";
        }
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        SaveDispenserCashCount();
    }

    private void SaveDispenserCashCount()
    {
        
        
        CallWebServices callWebServ = new CallWebServices();

        ParserOperation parserUpdateDispenserCashCount = callWebServ.CallUpdateDispenserCashCountService(this.kiosk.Kiosk.KioskId.ToString(),
                                                                                               this.TotalAmountTb.Text,
                                                                                               totalValue.ToString(),
                                                                                               this.LoginDatas.User.Id,
                                                                                               this.LoginDatas.AccessToken);
    
    }
}