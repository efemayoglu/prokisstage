﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Kiosk_ListKioskConditions : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Kiosk/ListKioskConditions.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {
                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }

            //ParserListKioskTypeName kioskTypes = callWebServ.CallGetKisokTypeNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            //if (kioskTypes != null)
            //{
            //
            //    this.kioskTypeBox.DataSource = kioskTypes.KioskTypeNames;
            //    this.kioskTypeBox.DataBind();
            //
            //}
            //
            //ListItem item1 = new ListItem();
            //item1.Text = "Kiosk Tipi Seçiniz";
            //item1.Value = "0";
            //this.kioskTypeBox.Items.Add(item1);
            //this.kioskTypeBox.SelectedValue = "0";


            ParserListInstutionName instutions = callWebServ.CallGetInstutionNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {

                this.instutionBox.DataSource = instutions.InstutionNames;
                this.instutionBox.DataBind();

            }

            ListItem item1 = new ListItem();
            item1.Text = "Kurum Seçiniz";
            item1.Value = "0";
            this.instutionBox.Items.Add(item1);
            this.instutionBox.SelectedValue = "0";



            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;
            this.pageNum = 0;
            this.SearchOrder(2, 1);
        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(2, 1);
    }

    private void BindListTable(ParserListKioskCondition listKioskMoney)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            double totalAmount = 0;
            int totalCoinCount =0;
            int totalCashCount = 0;

            TableRow[] Row = new TableRow[listKioskMoney.kioskCondition.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("kioskId", listKioskMoney.kioskCondition[i].kioskId.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell kioskAddresCell = new TableCell();
                TableCell totalAmounCell = new TableCell();
                TableCell cashCountCell = new TableCell();
                TableCell coinCountCell = new TableCell();
                TableCell ribbonCell = new TableCell();
                //TableCell updatedDateCell = new TableCell();
                TableCell detailsCell = new TableCell();
                TableCell capacityCell = new TableCell();
                TableCell explanationCell = new TableCell();
                TableCell dispenserCountCell = new TableCell();
                TableCell dispenserAmountCell = new TableCell();


                TableCell fakeCell1 = new TableCell();
                TableCell fakeCell2 = new TableCell();
                TableCell fakeCell3 = new TableCell();
                TableCell fakeCell4 = new TableCell();
                TableCell fakeCell5 = new TableCell();
                TableCell fakeCell6 = new TableCell();
                TableCell fakeCell7 = new TableCell();

                fakeCell1.CssClass = "inputTitleCell4";
                fakeCell2.CssClass = "inputTitleCell4";
                fakeCell3.CssClass = "inputTitleCell4";
                fakeCell4.CssClass = "inputTitleCell4";
                fakeCell5.CssClass = "inputTitleCell4";
                fakeCell6.CssClass = "inputTitleCell4";
                fakeCell7.CssClass = "inputTitleCell4";

                fakeCell1.Visible = false;
                fakeCell2.Visible = false;
                fakeCell3.Visible = false;
                fakeCell4.Visible = false;
                fakeCell5.Visible = false;
                fakeCell6.Visible = false;
                //fakeCell7.Visible = false;
                idCell.Visible = false;

                indexCell.CssClass = "inputTitleCell4";
                idCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                kioskAddresCell.CssClass = "inputTitleCell4v1";
                totalAmounCell.CssClass = "inputTitleCell4";
                cashCountCell.CssClass = "inputTitleCell4";
                coinCountCell.CssClass = "inputTitleCell4";
                ribbonCell.CssClass = "inputTitleCell4";
                //updatedDateCell.CssClass = "inputTitleCell4";
                detailsCell.CssClass = "inputTitleCell4";
                capacityCell.CssClass = "inputTitleCell4";
                explanationCell.CssClass = "inputTitleCell4v1";
                dispenserCountCell.CssClass = "inputTitleCell4";
                dispenserAmountCell.CssClass = "inputTitleCell4";


                indexCell.Text = (startIndex + i).ToString();
                //idCell.Text = listKioskMoney.kioskMoney[i].kioskId.ToString();
                
                //kioskNameCell.Text = "<a href=\"javascript:void(0);\" onclick=\"showConditionDetailClicked(" + listKioskMoney.kioskCondition[i].kioskId + ");\" class=\"anylink\">" + listKioskMoney.kioskCondition[i].kioskName.ToString() + "</a>";
                kioskNameCell.Text = listKioskMoney.kioskCondition[i].kioskName.ToString();
                
                kioskAddresCell.Text = listKioskMoney.kioskCondition[i].kioskAddress;
                totalAmount = totalAmount + Convert.ToDouble(listKioskMoney.kioskCondition[i].amount);
                totalCashCount = totalCashCount + listKioskMoney.kioskCondition[i].cashCount;
                totalCoinCount = totalCoinCount + listKioskMoney.kioskCondition[i].coinCount;
                totalAmounCell.Text = listKioskMoney.kioskCondition[i].amount + "  TL";
                cashCountCell.Text = listKioskMoney.kioskCondition[i].cashCount.ToString() + "  Adet";
                coinCountCell.Text = listKioskMoney.kioskCondition[i].coinCount.ToString() + "  Adet";
                int billCount = listKioskMoney.kioskCondition[i].ribbonRate;
                ribbonCell.Text = billCount.ToString() + "  Adet";
                //updatedDateCell.Text = listKioskMoney.kioskCondition[i].updatedDate;
                capacityCell.Text = listKioskMoney.kioskCondition[i].cashBoxAcceptorTypeNamedataRow + "/" + listKioskMoney.kioskCondition[i].capacity;
                explanationCell.Text = listKioskMoney.kioskCondition[i].explanation;

                /*dispenserCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editEnterKioskEmptyDispenserCashCountButtonClicked(" + listKioskMoney.kioskCondition[i].kioskId +
                               ",'" + (listKioskMoney.kioskCondition[i].amount).ToString() + "'," + (listKioskMoney.kioskCondition[i].amount) + " );\" class=\"anylink\">" + listKioskMoney.kioskCondition[i].amount + "</a>";*/
                dispenserAmountCell.Text = listKioskMoney.kioskCondition[i].dispenserAmount + " TL"; ;
                dispenserCountCell.Text = listKioskMoney.kioskCondition[i].dispenserCount + " Adet"; ;


                kioskNameCell.HorizontalAlign = HorizontalAlign.Left;
                detailsCell.Width = Unit.Pixel(60);
                detailsCell.Text = "<a href=# onClick=\"showKioskEmptyCashCountClicked(" + listKioskMoney.kioskCondition[i].kioskId + "," + 1 + ");\" class=\"detailCodeButton\" Title=\"Para Adetleri Göster\"></a>"
                    + "<a href=# onClick=\"showCashBoxCashCountClicked(" + listKioskMoney.kioskCondition[i].kioskId + ");\" class=\"detailCodeButton\" Title=\"Para Üstü Para Adetleri Göster\"></a>";

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        idCell,
                        fakeCell1,
                        fakeCell2,
                        fakeCell3,
                        fakeCell4,
                        fakeCell5,
                        fakeCell6,
                        
						kioskNameCell,
                        capacityCell,
                        explanationCell,
                        kioskAddresCell,
                       
                       
                        totalAmounCell,
                       
                        cashCountCell,
                        coinCountCell,
                        ribbonCell,
                        //updatedDateCell,
                        dispenserAmountCell,
                        dispenserCountCell,
                        detailsCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (listKioskMoney.recordCount < currentRecordEnd)
                currentRecordEnd = listKioskMoney.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + listKioskMoney.recordCount.ToString() + " kayıt bulundu.";
            }

            TableRow addNewRow = new TableRow();
            TableCell totalAmountCell = new TableCell();
            TableCell totalCoinCountCell = new TableCell();
            TableCell totalCashCountCell = new TableCell();
            TableCell totalDispenserAmountCell=new TableCell();
            TableCell totalDispenserCountCell=new TableCell();
            TableCell spaceCell = new TableCell();
            TableCell spaceCell1 = new TableCell();
            TableCell spaceCell2 = new TableCell();
            TableCell spaceCell3 = new TableCell();

            spaceCell.CssClass = "inputTitleCell99";
            spaceCell.ColumnSpan = (1);
            spaceCell1.CssClass = "inputTitleCell99";
            spaceCell1.ColumnSpan = (1);
            spaceCell2.CssClass = "inputTitleCell99";
            spaceCell2.ColumnSpan = (3);
            totalAmountCell.CssClass = "inputTitleCell99";
            totalCoinCountCell.CssClass = "inputTitleCell99";
            totalCashCountCell.CssClass = "inputTitleCell99";
            spaceCell3.CssClass = "inputTitleCell99";
            spaceCell3.ColumnSpan = (4);

            spaceCell.Text = "Toplam";
            totalAmountCell.Text = listKioskMoney.TotalAmount + " TL";
            totalCoinCountCell.Text = listKioskMoney.TotalCoinCount + " Adet";
            totalCashCountCell.Text = listKioskMoney.TotalCashCount + " Adet";
            //totalDispenserAmountCell.Text = listKioskMoney.TotalDispenserAmount;

            spaceCell1.Text = "";

            addNewRow.Cells.Add(spaceCell1);
            addNewRow.Cells.Add(spaceCell);
            addNewRow.Cells.Add(spaceCell2); 
           

            addNewRow.Cells.Add(totalAmountCell);
            addNewRow.Cells.Add(totalCashCountCell);
            addNewRow.Cells.Add(totalCoinCountCell);
            addNewRow.Cells.Add(spaceCell3);

            this.itemsTable.Rows.Add(addNewRow);

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(listKioskMoney.pageCount, this.pageNum, listKioskMoney.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskCobditionBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "KioskName":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "KioskAddress":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //KioskAddress.Text = "<a>Kiosk Adresi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //KioskAddress.Text = "<a>Kiosk Adresi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //KioskAddress.Text = "<a>Kiosk Adresi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "TotalAmout":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TotalAmout.Text = "<a>Para Miktarı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TotalAmout.Text = "<a>Para Miktarı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TotalAmout.Text = "<a>Para Miktarı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CashCount":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //CashCount.Text = "<a>Kağıt Para Adedi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                      //  CashCount.Text = "<a>Kağıt Para Adedi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //CashCount.Text = "<a>Kağıt Para Adedi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CoinCount":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //CoinCount.Text = "<a>Bozuk Para Adedi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //CoinCount.Text = "<a>Bozuk Para Adedi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                   // CoinCount.Text = "<a>Bozuk Para Adedi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Ribbon":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                       // Ribbon.Text = "<a>Kalan Ribon    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                       // Ribbon.Text = "<a>Kalan Ribon    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Ribbon.Text = "<a>Kalan Ribon    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Capacity":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        Capacity.Text = "<a>Kapasite    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        Capacity.Text = "<a>Kapasite  <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    Capacity.Text = "<a>Kapasite  <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Explanation":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Explanation.Text = "<a>Açıklama    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Explanation.Text = "<a>Açıklama  <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Explanation.Text = "<a>Güncelleme Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "DispenserAmount":
                if (orderSelectionColumn == 10)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Explanation.Text = "<a>Açıklama    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Explanation.Text = "<a>Açıklama  <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Explanation.Text = "<a>Güncelleme Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 10;
                    orderSelectionDescAsc = 1;
                }
                break;

            case "DispenserCount":
                if (orderSelectionColumn == 11)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Explanation.Text = "<a>Açıklama    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Explanation.Text = "<a>Açıklama  <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Explanation.Text = "<a>Güncelleme Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 11;
                    orderSelectionDescAsc = 1;
                }
                break;

            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }


    private void ExportToExcel(int orderSelectionColumn, int descAsc)			
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            int recordCount = 0;
            int pageCount = 0;
            CallWebServices callWebService = new CallWebServices();
            ParserListKioskCondition lists = callWebService.CallListKioskConditionService(kioskIds,
                                                                                           this.searchTextField.Text,
                                                                                           Convert.ToInt32(this.numberOfItemField.Text),
                                                                                           this.pageNum,
                                                                                           recordCount,
                                                                                           pageCount,
                                                                                           orderSelectionColumn,
                                                                                           Convert.ToInt32(ViewState["OrderDesc"]),
                                                                                           this.LoginDatas.AccessToken,
                                                                                           this.LoginDatas.User.Id,
                                                                                           Convert.ToInt32(this.instutionBox.SelectedValue),
                                                                                           2);

            if (lists != null)
            {
                if (lists.errorCode == 0)
                {

                    //for (int i = 0; i < lists.kioskCondition.Count; i++)
                    //{
                    //    lists.kioskCondition[i].amount = Convert.ToDecimal(lists.kioskCondition[i].amount.ToString().Replace('.', ','));
                    //}

                    ExportExcellDatas exportExcell = new ExportExcellDatas();

                    string[] headerNames = { "No", "Kiosk No", "Kiosk Adı", "Adres", "Kağıt Para Adedi", "Bozuk Para Adedi",
                                             "Kalan Ribon","Para Miktarı", "Kapasite","Kapasite Durumu", "Açıklama", "Para Üstü Miktarı", "Para Üstü Miktarı Adedi"};

                    exportExcell.ExportExcellByBlock(lists.kioskCondition, "Kiosk Durum Raporu", headerNames);
                }
                else if (lists.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + lists.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelAskiCode");
        }
    }

    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            int recordCount = 0;
            int pageCount = 0;
            CallWebServices callWebService = new CallWebServices();
            ParserListKioskCondition listKioskMoney = callWebService.CallListKioskConditionService(kioskIds,
                                                                                           this.searchTextField.Text,
                                                                                           Convert.ToInt32(this.numberOfItemField.Text),
                                                                                           this.pageNum,
                                                                                           recordCount,
                                                                                           pageCount,
                                                                                           orderSelectionColumn,
                                                                                           descAsc, 
                                                                                           this.LoginDatas.AccessToken, 
                                                                                           this.LoginDatas.User.Id,
                                                                                           Convert.ToInt32(this.instutionBox.SelectedValue)
                                                                                           ,1);
            if (listKioskMoney != null)
            {
                if (listKioskMoney.errorCode == 0)
                {
                    this.BindListTable(listKioskMoney);
                }
                else if (listKioskMoney.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + listKioskMoney.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskConditionSrch");

        }
    }

}