﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Kiosk;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Kiosk_ListKioskMonitoring : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 4;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Kiosk/ListKioskMonitoring.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {
            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {
                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);

            }

            ViewState["OrderColumn"] = 2;
            ViewState["OrderDesc"] = 1;
            this.pageNum = 0;
            this.SearchOrder(1, 1);
        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }

    }


    private void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebService = new CallWebServices();
            ParserListKioskMonitoring listKioskMonitoring = callWebService.CallListKioskMonitoringService(kioskIds,
                                                                                           this.searchTextField.Text,
                                                                                           Convert.ToInt32(this.numberOfItemField.Text),
                                                                                           this.pageNum,
                                                                                           recordCount,
                                                                                           pageCount,
                                                                                           orderSelectionColumn,
                                                                                           descAsc,
                                                                                           this.LoginDatas.AccessToken,
                                                                                           this.LoginDatas.User.Id,
                                                                                           1);
            if (listKioskMonitoring != null)
            {
                if (listKioskMonitoring.errorCode == 0)
                {
                    this.BindListTable(listKioskMonitoring);
                }
                else if (listKioskMonitoring.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + listKioskMonitoring.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskMonitoringSrch");

        }
    }



    private void BindListTable(ParserListKioskMonitoring listKioskMonitoring)
    {
        try
        {
            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

           

            TableRow[] Row = new TableRow[listKioskMonitoring.kioskMonitoring.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                //Row[i].Attributes.Add("kioskId", listKioskMonitoring.kioskMonitoring[i].kioskId.ToString());

                TableCell indexCell = new TableCell();
                 TableCell kioskNameCell = new TableCell();
 
                TableCell lastTransactionTimeCell = new TableCell();
                TableCell onlineStatusCell = new TableCell();
                TableCell transactionCountCell = new TableCell();
                TableCell totalTransactionAmoutCell = new TableCell();
                 
                TableCell adressCell = new TableCell();
                TableCell latitudeCell = new TableCell();
                TableCell longitudeCell = new TableCell();


                TableCell fakeCell1 = new TableCell();
                TableCell fakeCell2 = new TableCell();
                TableCell fakeCell3 = new TableCell();
                TableCell fakeCell4 = new TableCell();
                TableCell fakeCell5 = new TableCell();
                TableCell fakeCell6 = new TableCell();
                TableCell fakeCell7 = new TableCell();

             

                indexCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4v1";
                lastTransactionTimeCell.CssClass = "inputTitleCell4";
                onlineStatusCell.CssClass = "inputTitleCell4";
                transactionCountCell.CssClass = "inputTitleCell4";
                totalTransactionAmoutCell.CssClass = "inputTitleCell4";
                adressCell.CssClass = "inputTitleCell4v1";
                latitudeCell.CssClass = "inputTitleCell4"; 
                longitudeCell.CssClass = "inputTitleCell4";
              


                indexCell.Text = (startIndex + i).ToString();
                kioskNameCell.Text = listKioskMonitoring.kioskMonitoring[i].kioskName; 
                lastTransactionTimeCell.Text = listKioskMonitoring.kioskMonitoring[i].lastSuccessTime; 
                onlineStatusCell.Text = listKioskMonitoring.kioskMonitoring[i].lastAppAliveTime; 
                transactionCountCell.Text = listKioskMonitoring.kioskMonitoring[i].successTxnCount; 
                totalTransactionAmoutCell.Text = listKioskMonitoring.kioskMonitoring[i].successTxnAmount;             
                adressCell.Text = listKioskMonitoring.kioskMonitoring[i].address;
                latitudeCell.Text = listKioskMonitoring.kioskMonitoring[i].latitude;
                longitudeCell.Text = listKioskMonitoring.kioskMonitoring[i].longitude;

 
                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        kioskNameCell,
                        lastTransactionTimeCell,
                        onlineStatusCell,
                        transactionCountCell,                        
						totalTransactionAmoutCell,
                        adressCell,
                        latitudeCell,
                        longitudeCell,
  
					});

                if (i == Row.Length - 1)
                {
                    Row[i].CssClass = "inputTitleCell99";
                    Row[i].Cells[3].Text = "TOPLAM";
                    Row[i].Cells[0].Text = "";
                }
                else
                {
                    if (i % 2 == 0)
                        Row[i].CssClass = "listrow";
                    else
                        Row[i].CssClass = "listRowAlternate";
                }
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (listKioskMonitoring.recordCount < currentRecordEnd)
                currentRecordEnd = listKioskMonitoring.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + listKioskMonitoring.recordCount.ToString() + " kayıt bulundu.";
            }


            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(listKioskMonitoring.pageCount, this.pageNum, listKioskMonitoring.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListKioskCobditionBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(1, 1);
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        ExportToExcel(orderSelectionColumn, orderSelectionDescAsc);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }



    private void ExportToExcel(int orderSelectionColumn, int descAsc)
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            int recordCount = 0;
            int pageCount = 0;
            CallWebServices callWebService = new CallWebServices();
            ParserListKioskMonitoring listKioskMonitoring = callWebService.CallListKioskMonitoringService(kioskIds,
                                                                                         this.searchTextField.Text,
                                                                                         Convert.ToInt32(this.numberOfItemField.Text),
                                                                                         this.pageNum,
                                                                                         recordCount,
                                                                                         pageCount,
                                                                                         orderSelectionColumn,
                                                                                         Convert.ToInt32(ViewState["OrderDesc"]),
                                                                                         this.LoginDatas.AccessToken,
                                                                                         this.LoginDatas.User.Id,
                                                                                         2);

            if (listKioskMonitoring != null)
            {
                if (listKioskMonitoring.errorCode == 0)
                {

                    //for (int i = 0; i < lists.kioskCondition.Count; i++)
                    //{
                    //    lists.kioskCondition[i].amount = Convert.ToDecimal(lists.kioskCondition[i].amount.ToString().Replace('.', ','));
                    //}

                    ExportExcellDatas exportExcell = new ExportExcellDatas();

                    string[] headerNames = {  "Kiosk No", "Son İşlem Zamanı", "Online (Çalışma Durumu)", "İşlem Sayısı", "Toplam İşlem Tutarı ", "Adres",
                                             "Enlem","Boylam","No"};

                    exportExcell.ExportExcellByBlock(listKioskMonitoring.kioskMonitoring, "Kiosk İzleme Raporu", headerNames);
                }
                else if (listKioskMonitoring.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + listKioskMonitoring.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelAskiCode");
        }
    }


    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "KioskName":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //KioskName.Text = "<a>Kiosk Adı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "LastTransactionTime":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //KioskAddress.Text = "<a>Kiosk Adresi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //KioskAddress.Text = "<a>Kiosk Adresi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //KioskAddress.Text = "<a>Kiosk Adresi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "OnlineStatus":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //TotalAmout.Text = "<a>Para Miktarı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //TotalAmout.Text = "<a>Para Miktarı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //TotalAmout.Text = "<a>Para Miktarı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "TransactionCount":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //CashCount.Text = "<a>Kağıt Para Adedi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //  CashCount.Text = "<a>Kağıt Para Adedi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //CashCount.Text = "<a>Kağıt Para Adedi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "TotalTransactionAmout":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //CoinCount.Text = "<a>Bozuk Para Adedi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //CoinCount.Text = "<a>Bozuk Para Adedi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    // CoinCount.Text = "<a>Bozuk Para Adedi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Adress":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        // Ribbon.Text = "<a>Kalan Ribon    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        // Ribbon.Text = "<a>Kalan Ribon    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Ribbon.Text = "<a>Kalan Ribon    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Latitude":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Capacity.Text = "<a>Kapasite    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Capacity.Text = "<a>Kapasite  <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Capacity.Text = "<a>Kapasite  <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Longitude":
                if (orderSelectionColumn == 8)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Explanation.Text = "<a>Açıklama    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Explanation.Text = "<a>Açıklama  <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Explanation.Text = "<a>Güncelleme Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 8;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "TotalAmount":
                if (orderSelectionColumn == 9)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        //Explanation.Text = "<a>Açıklama    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        //Explanation.Text = "<a>Açıklama  <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    //Explanation.Text = "<a>Güncelleme Zamanı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 9;
                    orderSelectionDescAsc = 1;
                }
                break;
            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }
}