using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;

public class Global : System.Web.HttpApplication
{
    /// <summary>
    /// Required designer variable.
    /// </summary>

    public Global()
    {
        InitializeComponent();
    }

    protected void Application_Start(Object sender, EventArgs e)
    {

    }

    protected void Session_Start(Object sender, EventArgs e)
    {

    }

    protected void Application_BeginRequest(Object sender, EventArgs e)
    {

    }

    protected void Application_EndRequest(Object sender, EventArgs e)
    {

    }

    protected void Application_AuthenticateRequest(Object sender, EventArgs e)
    {

    }

    protected void Application_Error(Object sender, EventArgs e)
    {

    }

    protected void Session_End(Object sender, EventArgs e)
    {

    }

    protected void Application_End(Object sender, EventArgs e)
    {

    }

    #region Web Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.PreSendRequestHeaders += new System.EventHandler(this.Global_PreSendRequestHeaders);

    }
    #endregion

    private void Global_PreSendRequestHeaders(object sender, System.EventArgs e)
    {
        this.SetCachePolicy();
    }

    private void SetCachePolicy()
    {
        HttpCachePolicy cachePolicy = Response.Cache;
        cachePolicy.SetExpires(new DateTime(1950, 1, 1));
        cachePolicy.SetNoStore();
        cachePolicy.SetCacheability(HttpCacheability.NoCache);
    }
}