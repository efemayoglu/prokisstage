﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for NumericUpDown
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class NumericUpDown1 : System.Web.Services.WebService {

    [WebMethod]
    public int Up(int current, string tag)
    {
        if (current <= 536870912)
        {
            return current * 2;
        }
        else
        {
            return current;
        }
    }
    [WebMethod]
    public int Down(int current, string tag)
    {
        if (current >= 2)
        {
            return (int)(current / 2);
        }
        else
        {
            return current;
        };
    }
    
}
