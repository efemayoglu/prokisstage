﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Summary description for ClickableTableHeaderCell
/// </summary>
namespace ClickableWebControl
{
    /// <summary>
    /// Summary description for ClickableTableHeaderCell
    /// </summary>
    public class ClickableTableHeaderCell : TableHeaderCell, IPostBackEventHandler
    {
        public ClickableTableHeaderCell()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        protected override void Render(HtmlTextWriter writer)
        {
            this.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this, "", true));
            base.Render(writer);
        }
        /// <summary>
        /// Event raised when a text box click event takes place
        /// </summary>
        public event EventHandler TableHeaderCellClicked;
        /// <summary>
        /// Handler for the text box event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TableHeaderCell_Click(object sender, EventArgs e)
        {
            if (TableHeaderCellClicked != null)
            {
                TableHeaderCellClicked(sender, e);
            }
        }
        #region IPostBackEventHandler Members

        void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
        {
            if (TableHeaderCellClicked != null)
            {
                TableHeaderCellClicked(this, new EventArgs());
            }
        }
        #endregion
    }
}