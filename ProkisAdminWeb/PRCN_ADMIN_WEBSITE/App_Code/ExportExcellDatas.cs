﻿using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
//using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Excel = Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;
using ExcelLibrary;
using PRCNCORE.Parser;
using System.Windows.Forms;
using System.Threading;
using ClosedXML;
using ClosedXML.Excel;

/// <summary>
/// Summary description for ExportExcellDatas
/// </summary>
public class ExportExcellDatas
{
    public ExportExcellDatas()
    {
    }

    public void ExportExcell(object list, int recordCount, string fileName)
    {
        try
        {
            System.IO.TextWriter textWriter = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlTextWriter = new System.Web.UI.HtmlTextWriter(textWriter);

            System.Web.UI.WebControls.DataGrid dataGrid = new System.Web.UI.WebControls.DataGrid();
            dataGrid.DataSource = list;
            dataGrid.DataBind();
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1254");
            HttpContext.Current.Response.Charset = "windows-1254"; //ISO-8859-13 ISO-8859-9  windows-1254
            HttpContext.Current.Response.Buffer = true;
            //this.EnableViewState = false;
            HttpContext.Current.Response.ContentType = "application/vnd.xls";
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xls");
            string header = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n<title></title>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1254\" />\n<style>\n</style>\n</head>\n<body>\n";
            string footer = "\n</body>\n</html>";

            dataGrid.RenderControl(htmlTextWriter);

            string data = header + textWriter + footer;



            HttpContext.Current.Response.Write(data);

            HttpContext.Current.Response.Flush();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            HttpContext.Current.Response.End();
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExportExcell");
        }

    }


    public static DataSet ToDataSet<T>(IList<T> list)
    {
        Type elementType = typeof(T);

        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("6"), "Excel");

        DataSet ds = new DataSet();

        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("7"), "Excel");

        DataTable t = new DataTable();
        ds.Tables.Add(t);

        //add a column to table for each public property on T
        foreach (var propInfo in elementType.GetProperties())
        {
            Type ColType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;

            t.Columns.Add(propInfo.Name, ColType);
        }

        //go through each property on T and add each value to the table
        foreach (T item in list)
        {
            DataRow row = t.NewRow();

            foreach (var propInfo in elementType.GetProperties())
            {
                row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
            }

            t.Rows.Add(row);
        }

        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("8"), "Excel");

        return ds;
    }

    public void FileExport(object objTable)
    {
        try
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Excel Files | *.xls";
            saveFileDialog.DefaultExt = "xls";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string path = saveFileDialog.FileName;
                ExcelLibrary.DataSetHelper.CreateWorkbook(path, (DataSet)objTable);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("12" + ex.ToString()), "Excel");
        }

    }

    public void ExportExcellByBlock<T>(IList<T> data, string filename, string[] headerNames)
    {
        DataSet dataSet = ToDataSet(data);

        if (headerNames != null)
        {
            for (int i = 0; i < dataSet.Tables[0].Columns.Count; i++)
            {
                if (i <= headerNames.Length)
                {
                    dataSet.Tables[0].Columns[i].ColumnName = headerNames[i];
                }

            }

        }
        dataSet.Tables[0].AcceptChanges();

        // dataSet.Tables[0].Rows[0][0] = "Deneme";

        using (XLWorkbook wb = new XLWorkbook())
        {
            wb.Worksheets.Add(dataSet);

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xlsx");
            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                wb.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }
        }

        //string path = "C:";
        //ExcelLibrary.DataSetHelper.CreateWorkbook(path, dataSet);

        /*
        ParameterizedThreadStart threadFileExport = FileExport;
        Thread threadExport = new Thread(threadFileExport)
        {
            ApartmentState = ApartmentState.STA,
            IsBackground = true,
            Name = "threadExport",
            Priority = ThreadPriority.AboveNormal
        };
        threadExport.Start(dataSet);
        */
    }


    private Excel.Worksheet FindSheet(Excel.Workbook workbook, string sheet_name)
    {
        foreach (Excel.Worksheet sheet in workbook.Sheets)
        {
            if (sheet.Name == sheet_name) return sheet;
        }
        return null;

    }


    public void ExportExcellByBlock<T>(IList<T> data)
    {
        DataSet dataSet = ToDataSet(data);

        ParameterizedThreadStart threadFileExport = FileExport;
        Thread threadExport = new Thread(threadFileExport)
        {
            ApartmentState = ApartmentState.STA,
            IsBackground = true,
            Name = "threadExport",
            Priority = ThreadPriority.AboveNormal
        };
        threadExport.Start(dataSet);

    }
}

