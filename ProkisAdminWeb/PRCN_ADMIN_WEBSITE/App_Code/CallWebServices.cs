﻿using MakeMutabakatParserNS;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Account;
using PRCNCORE.Parser.Kiosk;
using PRCNCORE.Parser.MoneyCode;
using PRCNCORE.Parser.Other;
using PRCNCORE.Parser.User;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web;


/// <summary>
/// Summary description for CallWebServices
/// </summary>
public class CallWebServices
{
    public CallWebServices()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    public ParserListMutabakatTransaction CallGetMakeMutabakatListValues(DateTime mutabakatDate
        , int kioskId
        , Int32 instutionId
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListMutabakatTransaction parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");

            data.Add("mutabakatDate", mutabakatDate);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            if (instutionId == 1)
            {
                data.Add("functionCode", (int) FC.GET_ASKI_MUTABAKAT_DATA_LIST);
            }
            else if (instutionId == 2)
            {
                data.Add("functionCode", (int) FC.GET_BASKENT_MUTABAKAT_DATA_LIST);
            }
            else if (instutionId == 3)
            {
                data.Add("functionCode", (int) FC.GET_MASKI_MUTABAKAT_DATA_LIST);
            }
            else if (instutionId == 4)
            {
                data.Add("functionCode", (int) FC.GET_ASKI_DB_MUTABAKAT_DATA_LIST);
            }
            else if (instutionId == 5)
            {
                data.Add("functionCode", (int) FC.GET_BASKENT_DB_MUTABAKAT_DATA_LIST);
            }

            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            Utility.WriteLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), "Response01:" + responseText, true,
                false);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                string modifiedResponse =
                    responseText.Replace("\\\"", "\"").Replace("\"[{", "[{").Replace("}]\"", "}]");
                //string modifiedResponse = "{\"Response\":[{\"bankReferenceNumber\":\"Smith\",\"insProcessDate\":\"Doe\",\"institutionAmount\":49.10},{\"bankReferenceNumber\":\"Smith\",\"insProcessDate\":\"Doe\",\"institutionAmount\":49.10}]}";
                //.Replace("\"[","[").Replace("\"]","]").Replace("\r","").Replace("\n","").Replace(" ","");
                parser = JsonConvert.DeserializeObject<ParserListMutabakatTransaction>(modifiedResponse);
                Utility.WriteLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), modifiedResponse, true, false);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_BASKENT_MUTABAKAT_DATA_LIST).ToString());
        }

        return parser;
    }

    public ParserGetKioskReferenceNumber CallGetKioskReferenceNumberService(string accessToken
        , Int64 adminUserId)
    {
        ParserGetKioskReferenceNumber parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_REFERENCE_NUMBER);

            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserGetKioskReferenceNumber>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_REFERENCE_NUMBER).ToString());
        }

        return parser;
    }

    public ParserListMutabakatMaskiTransaction CallGetMakeMutabakatListMaskiValues(DateTime mutabakatDate
        , int kioskId
        , Int32 instutionId
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListMutabakatMaskiTransaction parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");

            data.Add("mutabakatDate", mutabakatDate);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("functionCode", (int) FC.GET_MASKI_MUTABAKAT_DATA_LIST);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListMutabakatMaskiTransaction>(responseText);
                Utility.WriteLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), responseText, true, false);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_BASKENT_MUTABAKAT_DATA_LIST).ToString());
        }

        return parser;
    }

    public ParserListTransactionCondition CallListTransactionConditionService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , int paymentTypeId
        , int cardTypeId
        , int instutionId
        , string kioskId
        , int completeStatusId
        , int paymentType
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListTransactionCondition parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_TRANSACTION_CONDITION);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("paymentTypeId", paymentTypeId);
            data.Add("cardTypeId", cardTypeId);
            data.Add("completeStatusId", completeStatusId);
            data.Add("paymentType", paymentType);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListTransactionCondition>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_TRANSACTION_CONDITION).ToString());
        }

        return parser;
    }


    //public ParserListTransactionCondition CallListTransactionConditionForExcelService(string searchText
    //                                                               , DateTime startDate
    //                                                               , DateTime endDate
    //                                                               , int orderSelectionColumn
    //                                                               , int descAsc
    //                                                               , int paymentTypeId
    //                                                               , int cardTypeId
    //                                                               , int instutionId
    //                                                               , string kioskId
    //                                                               , int completeStatusId
    //                                                               , string accessToken
    //                                                               , Int64 adminUserId)
    //{

    //    ParserListTransactionCondition parser = null;
    //    try
    //    {
    //        HttpStatusCode statusCode = HttpStatusCode.Accepted;

    //        JObject data = new JObject();
    //        data.Add("apiKey", "TEST");
    //        data.Add("apiPass", "ABCD1234");
    //        data.Add("functionCode", (int)FC.EXCEL_LIST_TRANSACTION_CONDITION);
    //        data.Add("searchText", searchText);
    //        data.Add("startDate", startDate);
    //        data.Add("endDate", endDate);
    //        data.Add("orderSelectionColumn", orderSelectionColumn);
    //        data.Add("descAsc", descAsc);
    //        data.Add("kioskId", kioskId);
    //        data.Add("instutionId", instutionId);
    //        data.Add("paymentTypeId", paymentTypeId);
    //        data.Add("cardTypeId", cardTypeId);
    //        data.Add("completeStatusId", completeStatusId);
    //        data.Add("adminUserId", adminUserId);

    //        string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

    //        string responseText = Utility.ExecuteHttpRequest
    //            (URL,
    //            "POST",
    //            System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
    //            ref statusCode);

    //        if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
    //        {

    //            parser = JsonConvert.DeserializeObject<ParserListTransactionCondition>(responseText);
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex, ((int)FC.EXCEL_LIST_TRANSACTION_CONDITION).ToString());
    //    }

    //    return parser;
    //}

    public ParserListKioskReport CallListKioskReportService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , int instutionId
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListKioskReport parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSK_REPORT);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskReport>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_REPORT).ToString());
        }

        return parser;
    }

    public MakeMutabakatMaskiParser CallGetMakeMutabakatMaskiValues(DateTime mutabakatDate
        , int kioskId
        , Int32 instutionId
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        MakeMutabakatMaskiParser parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");

            data.Add("mutabakatDate", mutabakatDate);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("functionCode", (int) FC.GET_MASKI_MUTABAKAT_DATA);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<MakeMutabakatMaskiParser>(responseText);
                Utility.WriteLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), responseText, true, false);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_MASKI_MUTABAKAT_DATA).ToString());
        }

        return parser;
    }

    //public ParserListMoneyCodes CallListMoneyCodeSweepBackForExcelService(string searchText
    //                                                      , DateTime startDate
    //                                                      , DateTime endDate
    //                                                      , int numberOfItemsPerPage
    //                                                      , int currentPageNum
    //                                                      , int recordCount
    //                                                      , int pageCount
    //                                                      , int orderSelectionColumn
    //                                                      , int descAsc
    //                                                      , int codeStatus
    //                                                      , int generatedType, string accessToken
    //                                                      , Int64 adminUserId
    //                                                      , int instutionId)
    //{

    //    ParserListMoneyCodes parser = null;
    //    try
    //    {
    //        HttpStatusCode statusCode = HttpStatusCode.Accepted;

    //        JObject data = new JObject();
    //        data.Add("apiKey", "TEST");
    //        data.Add("apiPass", "ABCD1234");
    //        data.Add("functionCode", (int)FC.EXCEL_LIST_MONEY_CODE_SWEEP_BACK);
    //        data.Add("searchText", searchText);
    //        data.Add("startDate", startDate);
    //        data.Add("endDate", endDate);
    //        data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
    //        data.Add("currentPageNum", currentPageNum);
    //        data.Add("recordCount", recordCount);
    //        data.Add("pageCount", pageCount);
    //        data.Add("orderSelectionColumn", orderSelectionColumn);
    //        data.Add("descAsc", descAsc);
    //        data.Add("codeStatus", codeStatus);
    //        data.Add("instutionId", instutionId);
    //        data.Add("generatedType", generatedType);
    //        data.Add("adminUserId", adminUserId);

    //        string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

    //        string responseText = Utility.ExecuteHttpRequest
    //            (URL,
    //            "POST",
    //            System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
    //            ref statusCode);

    //        if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
    //        {

    //            parser = JsonConvert.DeserializeObject<ParserListMoneyCodes>(responseText);
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex, ((int)FC.EXCEL_LIST_MONEY_CODE).ToString());
    //    }

    //    return parser;
    //}

    public MakeMutabakatParser CallGetMakeMutabakatValues(DateTime mutabakatDate
        , int kioskId
        , Int32 instutionId
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        MakeMutabakatParser parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");

            data.Add("mutabakatDate", mutabakatDate);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            if (instutionId == 1)
                data.Add("functionCode", (int) FC.GET_ASKI_MUTABAKAT_DATA);
            else if (instutionId == 2)
                data.Add("functionCode", (int) FC.GET_BASKENT_MUTABAKAT_DATA);
            else if (instutionId == 3)
                data.Add("functionCode", (int) FC.GET_MASKI_MUTABAKAT_DATA);
            else if (instutionId == 4)
                data.Add("functionCode", (int) FC.GET_ASKI_DB_MUTABAKAT_DATA);
            else if (instutionId == 5)
                data.Add("functionCode", (int) FC.GET_BASKENT_DB_MUTABAKAT_DATA);

            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            Utility.WriteLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), "Response:" + responseText, true,
                false);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<MakeMutabakatParser>(responseText);
                Utility.WriteLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), responseText, true, false);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_BASKENT_MUTABAKAT_DATA).ToString());
        }

        return parser;
    }

    public ParserListKioskReport CallListKioskReportForExcelService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , int instutionId
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListKioskReport parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_KIOSK_REPORT);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskReport>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_KIOSK_REPORT).ToString());
        }

        return parser;
    }

    public ParserListKioskReport CallListKioskReportForExcelServiceNew(string searchText
        , DateTime startDate
        , DateTime endDate
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , int instutionId
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListKioskReport parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_KIOSK_REPORT_NEW);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskReport>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_KIOSK_REPORT_NEW).ToString());
        }

        return parser;
    }


    public ParserListCodeReport CallListCodeReportForExcelService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , int instutionId
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListCodeReport parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_CODE_REPORT);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCodeReport>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_KIOSK_REPORT_NEW).ToString());
        }

        return parser;
    }


    public ParserListCity CallGetCitiesService(string accessToken, Int64 adminUserId)
    {
        ParserListCity parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_CITIES);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCity>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_CITIES).ToString());
        }

        return parser;
    }


    public ParserListCapacity CallGetCapacitiesService(string accessToken, Int64 adminUserId)
    {
        ParserListCapacity parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_CAPACITIES);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCapacity>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_CAPACITIES).ToString());
        }

        return parser;
    }


    public ParserListRegion CallGetRegionsService(int cityId, string accessToken, Int64 adminUserId)
    {
        ParserListRegion parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_REGIONS);
            data.Add("cityId", cityId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListRegion>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_REGIONS).ToString());
        }

        return parser;
    }


    public ParserListConnectionType CallGetConnectionTypeService(string accessToken, Int64 adminUserId)
    {
        ParserListConnectionType parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_KIOSK_CONNECTION_TYPE);
            //data.Add("connectionTypeId", connectionTypeId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListConnectionType>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_KIOSK_CONNECTION_TYPE).ToString());
        }

        return parser;
    }


    public ParserListCardReaderType CallGetCardReaderTypeService(string accessToken, Int64 adminUserId)
    {
        ParserListCardReaderType parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_KIOSK_CARD_TYPE);
            //data.Add("cityId", cityId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCardReaderType>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_KIOSK_CARD_TYPE).ToString());
        }

        return parser;
    }

    public ParserListMutabakat CallListMutabakatService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , int instutionId
        , int status
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListMutabakat parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_MUTABAKAT);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("status", status);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListMutabakat>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_MUTABAKAT).ToString());
        }

        return parser;
    }

    public ParserListMutabakatReport CallListMutabakatServiceForReport(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListMutabakatReport parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_MUTABAKAT_FOR_REPORT);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListMutabakatReport>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_MUTABAKAT).ToString());
        }

        return parser;
    }

    public ParserListDailyReports CallListDailyAccountReportService(DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListDailyReports parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_DAILY_ACCOUNT_REPORTS);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListDailyReports>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_DAILY_ACCOUNT_REPORTS).ToString());
        }

        return parser;
    }

    public ParserListDailyReportsProkis CallListDailyAccountReportProKisService(DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListDailyReportsProkis parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_DAILY_ACCOUNT_REPORTS_PROKIS);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListDailyReportsProkis>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_DAILY_ACCOUNT_REPORTS_PROKIS).ToString());
        }

        return parser;
    }


    public ParserListKioskCashAcceptor CallListKioskCashAcceptor(
        DateTime transactionDateField
        , DateTime enteredDateField
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , int instutionId
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListKioskCashAcceptor parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSKS_CASH_ACCEPTOR);
            data.Add("startDate", transactionDateField);
            data.Add("endDate", enteredDateField);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskCashAcceptor>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSKS_CASH_ACCEPTOR).ToString());
        }

        return parser;
    }

    public ParserListKioskCashDispenser CallListKioskCashDispenserService(
        DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , int instutionId
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListKioskCashDispenser parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSKS_CASH_DISPENSER);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskCashDispenser>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSKS_CASH_DISPENSER).ToString());
        }

        return parser;
    }


    public ParserListKioskCashDispenser CallListKioskCashDispenserNewService(
        DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , int instutionId
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListKioskCashDispenser parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSKS_CASH_DISPENSER_NEW);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskCashDispenser>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSKS_CASH_DISPENSER).ToString());
        }

        return parser;
    }


    public ParserListKioskCashDispenser CallListKioskCashDispenserLCDMHopperService(
        DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , int instutionId
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListKioskCashDispenser parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSKS_CASH_DISPENSER_LCDM_HOPPER);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskCashDispenser>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSKS_CASH_DISPENSER).ToString());
        }

        return parser;
    }

    public ParserListKioskControlCashNotification CallControlCashNotification(string searchText,
        DateTime startDateField
        , DateTime endDateField
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , int instutionId
        , string accessToken
        , Int64 adminUserId
        , int orderType
        , int statusId
        , int kioskfullfillstatus)
    {
        ParserListKioskControlCashNotification parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.CONTROL_CASH_NOTIFICATION);
            data.Add("searchText", searchText);
            data.Add("startDate", startDateField);
            data.Add("endDate", endDateField);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);
            data.Add("StatusId", statusId);
            data.Add("kioskfullfillstatus", kioskfullfillstatus);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskControlCashNotification>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.CONTROL_CASH_NOTIFICATION).ToString());
        }

        return parser;
    }


    public ParserListKioskReferenceSystem CallListKioskReferenceSystem(string searchText,
        DateTime startDateField
        , DateTime endDateField
        , int referenceStatus
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , int instutionId
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListKioskReferenceSystem parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSK_REFERENCE_SYSTEM);
            data.Add("searchText", searchText);
            data.Add("startDate", startDateField);
            data.Add("endDate", endDateField);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);
            data.Add("referenceStatus", referenceStatus);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskReferenceSystem>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_REFERENCE_SYSTEM).ToString());
        }

        return parser;
    }

    //CallSaveKioskReferenceSystem
    public ParserOperation CallSaveKioskReferenceSystem(string kioskId,
        string referenceNo,
        string explanation,
        string accessToken,
        Int64 adminUserId)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();

            data.Add("functionCode", (int) FC.SAVE_KIOSK_REFERENCE_SYSTEM);
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("kioskId", kioskId);
            data.Add("referenceNo", referenceNo);
            data.Add("explanation", explanation);
            data.Add("adminUserId", adminUserId);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.SAVE_KIOSK_REFERENCE_SYSTEM).ToString());
        }

        return parser;
    }


    //CallSaveKioskReferenceSystem
    public ParserOperation CallSaveKioskLocalLog(int transactionId, string accessToken, Int64 adminUserId)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();

            data.Add("functionCode", (int) FC.SAVE_KIOSK_LOCAL_LOG);
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("transactionId", transactionId);
            data.Add("adminUserId", adminUserId);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.SAVE_KIOSK_LOCAL_LOG).ToString());
        }

        return parser;
    }


    //CallGetControlCashNotificationService
    public ParserGetControlCashNotification CallGetControlCashNotificationService(int controlCashId, string accessToken
        , Int64 adminUserId)
    {
        ParserGetControlCashNotification parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_CONTROL_CASH_NOTIFICATION);
            data.Add("controlCashId", controlCashId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserGetControlCashNotification>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_CUT_KIOSK).ToString());
        }

        return parser;
    }


    public ParserListKioskCutOfMonitoring CutOffMonitoring(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , int instutionId
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListKioskCutOfMonitoring parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.CUT_OFF_MONITORING);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskCutOfMonitoring>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.CUT_OFF_MONITORING).ToString());
        }

        return parser;
    }

    public ParserListBankAccountAction CallBankAccountAction(string searchText
        , DateTime startDateField
        , DateTime endDateField
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListBankAccountAction parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.BANK_ACCOUNT_ACTION);

            data.Add("searchText", searchText);
            data.Add("startDate", startDateField);
            data.Add("endDate", endDateField);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("orderType", orderType);
            data.Add("adminUserId", adminUserId);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListBankAccountAction>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.BANK_ACCOUNT_ACTION).ToString());
        }

        return parser;
    }


    public ParserListEnterKioskEmptyCashCount CallEnterKioskEmptyCashCount(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , string enteredUser
        , string operatorUser
        , string status
        //, int instutionId
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListEnterKioskEmptyCashCount parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.ENTER_KIOSK_EMPTY_CASH_COUNT);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("enteredUser", enteredUser);
            data.Add("operatorUser", operatorUser);
            data.Add("status", status);
            //data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);
            //data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListEnterKioskEmptyCashCount>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSKS_CASH_ACCEPTOR).ToString());
        }

        return parser;
    }


    public ParserListMutabakat CallListMutabakatToExcelService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListMutabakat parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_MUTABAKAT);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListMutabakat>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_MUTABAKAT).ToString());
        }

        return parser;
    }

    public ParserListDailyReports CallListDailyAccountReportToExcelService(DateTime startDate
        , DateTime endDate
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListDailyReports parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_DAILY_ACCOUNT_REPORTS);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListDailyReports>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_DAILY_ACCOUNT_REPORTS).ToString());
        }

        return parser;
    }

    //public ParserListDailyReports CallListDailyAccountReportProkisToExcelService(DateTime startDate
    //                                                       , DateTime endDate
    //                                                       , int orderSelectionColumn
    //                                                       , int descAsc
    //                                                       , string accessToken
    //                                                       , Int64 adminUserId)
    //{

    //    ParserListDailyReports parser = null;
    //    try
    //    {
    //        HttpStatusCode statusCode = HttpStatusCode.Accepted;

    //        JObject data = new JObject();
    //        data.Add("apiKey", "TEST");
    //        data.Add("apiPass", "ABCD1234");
    //        data.Add("functionCode", (int)FC.EXCEL_LIST_DAILY_ACCOUNT_REPORTS_PROKIS);
    //        data.Add("startDate", startDate);
    //        data.Add("endDate", endDate);
    //        data.Add("orderSelectionColumn", orderSelectionColumn);
    //        data.Add("descAsc", descAsc);
    //        data.Add("adminUserId", adminUserId);

    //        string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

    //        string responseText = Utility.ExecuteHttpRequest
    //            (URL,
    //            "POST",
    //            System.Text.Encoding.UTF8.GetBytes(data.ToString()),
    //            accessToken,
    //            ref statusCode);

    //        if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
    //        {

    //            parser = JsonConvert.DeserializeObject<ParserListDailyReports>(responseText);
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex, ((int)FC.EXCEL_LIST_DAILY_ACCOUNT_REPORTS_PROKIS).ToString());
    //    }

    //    return parser;
    //}

    public ParserOperation CallApproveMutabakatService(int kioskEmptyId
        , int userId
        , string inputAmount
        , string outputAmount
        , string accessToken
        , Int64 adminUserId
        , string explanation)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.SAVE_EMPTY_KIOSK_MUTABAKAT);
            data.Add("userId", userId);
            data.Add("kioskEmptyId", kioskEmptyId);
            data.Add("inputAmount", inputAmount);
            data.Add("outputAmount", outputAmount);
            data.Add("adminUserId", adminUserId);
            data.Add("explanation", explanation);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.SAVE_EMPTY_KIOSK_MUTABAKAT).ToString());
        }

        return parser;
    }

    public ParserListMutabakatCashCounts CallGetMutabakatCashCountService(int whichSide
        , int kioskEmptyId
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListMutabakatCashCounts parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_MUTABAKAT_CASH_COUNT);
            data.Add("whichSide", whichSide);
            data.Add("kioskEmptyId", kioskEmptyId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListMutabakatCashCounts>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_MUTABAKAT_CASH_COUNT).ToString());
        }

        return parser;
    }

    public ParserListEmptyKioskNames CallGetKioskNameMutabakatService(string accessToken, Int64 adminUserId)
    {
        ParserListEmptyKioskNames parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_EMPTY_KIOSK_NAME_FOR_MUTABAKAT);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListEmptyKioskNames>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_EMPTY_KIOSK_NAME_FOR_MUTABAKAT).ToString());
        }

        return parser;
    }

    public ParserListEmptyKioskNames CallGetKioskNameService(int expertUserId, string accessToken, Int64 adminUserId)
    {
        ParserListEmptyKioskNames parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_EMPTY_KIOSK_NAME);
            data.Add("expertUserId", expertUserId);
            data.Add("adminUserId", adminUserId);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListEmptyKioskNames>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_EMPTY_KIOSK_NAME).ToString());
        }

        return parser;
    }

    public ParserSaveKioskEmptyCashCount CallSaveKioskEmptyCashCountService(JArray jsonArray
        , int kioskId
        , int expertUserId
        , string code
        , int emptyKioskId
        , string accessToken
        , Int64 adminUserId)
    {
        ParserSaveKioskEmptyCashCount parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.SAVE_KIOSK_EMPTY_CASH_COUNT);
            data.Add("kioskId", kioskId);
            data.Add("expertUserId", expertUserId);
            data.Add("code", code);
            data.Add("emptyKioskId", emptyKioskId);
            data.Add("cashCounts", jsonArray);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserSaveKioskEmptyCashCount>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.SAVE_KIOSK_EMPTY_CASH_COUNT).ToString());
        }

        return parser;
    }


    public ParserOperation CallSaveControlCashNotificationService(int kioskId
        , int adminUserId
        , string accessToken
        , string firstDenomNumber
        , string secondDenomNumber
        , string thirdDenomNumber
        , string fourthDenomNumber
        , string fifthDenomNumber
        , string sixthDenomNumber
        , string seventhDenomNumber
        , string eighthDenomNumber
        , string ninthDenomNumber
        , string tenthDenomNumber
        , string eleventhDenomNumber
        , decimal totalAmount
    )
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.SAVE_CONTROL_CASH_NOTIFICATION_SERVICE);
            data.Add("adminUserId", adminUserId);
            data.Add("kioskId", kioskId);
            data.Add("firstDenomNumber", firstDenomNumber);
            data.Add("secondDenomNumber", secondDenomNumber);
            data.Add("thirdDenomNumber", thirdDenomNumber);
            data.Add("fourthDenomNumber", fourthDenomNumber);
            data.Add("fifthDenomNumber", fifthDenomNumber);
            data.Add("sixthDenomNumber", sixthDenomNumber);
            data.Add("seventhDenomNumber", seventhDenomNumber);
            data.Add("eighthDenomNumber", eighthDenomNumber);
            data.Add("ninthDenomNumber", ninthDenomNumber);
            data.Add("tenthDenomNumber", tenthDenomNumber);
            data.Add("eleventhDenomNumber", eleventhDenomNumber);
            data.Add("totalAmount", totalAmount);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.SAVE_KIOSK_EMPTY_CASH_COUNT).ToString());
        }

        return parser;
    }


    //CallUpdateControlCashNotificationService
    public ParserOperation CallUpdateControlCashNotificationService(int controlCashId
        , int adminUserId
        , string accessToken
        , string firstDenomNumber
        , string secondDenomNumber
        , string thirdDenomNumber
        , string fourthDenomNumber
        , string fifthDenomNumber
        , string sixthDenomNumber
        , string seventhDenomNumber
        , string eighthDenomNumber
        , string ninthDenomNumber
        , string tenthDenomNumber
        , string eleventhDenomNumber
        , decimal totalAmount)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.UPDATE_CONTROL_CASH_NOTIFICATION_SERVICE);
            data.Add("adminUserId", adminUserId);
            data.Add("controlCashId", controlCashId);
            data.Add("firstDenomNumber", firstDenomNumber);
            data.Add("secondDenomNumber", secondDenomNumber);
            data.Add("thirdDenomNumber", thirdDenomNumber);
            data.Add("fourthDenomNumber", fourthDenomNumber);
            data.Add("fifthDenomNumber", fifthDenomNumber);
            data.Add("sixthDenomNumber", sixthDenomNumber);
            data.Add("seventhDenomNumber", seventhDenomNumber);
            data.Add("eighthDenomNumber", eighthDenomNumber);
            data.Add("ninthDenomNumber", ninthDenomNumber);
            data.Add("tenthDenomNumber", tenthDenomNumber);
            data.Add("eleventhDenomNumber", eleventhDenomNumber);
            data.Add("totalAmount", totalAmount);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.UPDATE_CONTROL_CASH_NOTIFICATION_SERVICE).ToString());
        }

        return parser;
    }


    //CallUpdateDispenserCashCountService
    public ParserOperation CallUpdateDispenserCashCountService(int kioskId
        , int adminUserId
        , string accessToken
        , string firstDenomNumber
        , string secondDenomNumber
        , string thirdDenomNumber
        , string fourthDenomNumber
        , string fifthDenomNumber
        , string sixthDenomNumber
        , string seventhDenomNumber
        , string eighthDenomNumber
        , string ninthDenomNumber
        , string tenthDenomNumber
        , string eleventhDenomNumber
        , string twelfthDenomNumber
        , int cassette1Status
        , int cassette2Status
        , int cassette3Status
        , int cassette4Status
        , int hopper1Status
        , int hopper2Status
        , int hopper3Status
        , int hopper4Status
        , decimal totalAmount
    )
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.UPDATE_DISPENSER_CASH_COUNT_SERVICE);
            data.Add("adminUserId", adminUserId);
            data.Add("kioskId", kioskId);
            data.Add("firstDenomNumber", firstDenomNumber);
            data.Add("secondDenomNumber", secondDenomNumber);
            data.Add("thirdDenomNumber", thirdDenomNumber);
            data.Add("fourthDenomNumber", fourthDenomNumber);
            data.Add("fifthDenomNumber", fifthDenomNumber);
            data.Add("sixthDenomNumber", sixthDenomNumber);
            data.Add("seventhDenomNumber", seventhDenomNumber);
            data.Add("eighthDenomNumber", eighthDenomNumber);
            data.Add("ninthDenomNumber", ninthDenomNumber);
            data.Add("tenthDenomNumber", tenthDenomNumber);
            data.Add("eleventhDenomNumber", eleventhDenomNumber);
            data.Add("twelfthDenomNumber", twelfthDenomNumber);
            data.Add("cassette1Status", cassette1Status);
            data.Add("cassette2Status", cassette2Status);
            data.Add("cassette3Status", cassette3Status);
            data.Add("cassette4Status", cassette4Status);
            data.Add("hopper1Status", hopper1Status);
            data.Add("hopper2Status", hopper2Status);
            data.Add("hopper3Status", hopper3Status);
            data.Add("hopper4Status", hopper4Status);
            data.Add("totalAmount", totalAmount);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.UPDATE_DISPENSER_CASH_COUNT_SERVICE).ToString());
        }

        return parser;
    }

    //ParserListKioskDispenserMonitoring CallListKioskDispenserMonitoringService
    //CallUpdateDispenserCashCountService
    public ParserListKioskDispenserMonitoring CallListKioskDispenserMonitoringService(string searchText
        , int instutionId
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListKioskDispenserMonitoring parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSK_DISPENSER_MONITORING_SERVICE);
            data.Add("adminUserId", adminUserId);


            data.Add("searchText", searchText);
            data.Add("instutionId", instutionId);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);

            data.Add("orderType", orderType);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskDispenserMonitoring>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_DISPENSER_MONITORING_SERVICE).ToString());
        }

        return parser;
    }

    public string CallListKioskDispenserErrors(string searchText
        , int instutionId
        , int errorId
        , int kioskId
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListKioskDispenserMonitoring parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSKS_DISPENSER_ERRORS);
            data.Add("adminUserId", adminUserId);
            data.Add("searchText", searchText);
            data.Add("instutionId", instutionId);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("errorId", errorId);
            data.Add("kioskId", kioskId);
            data.Add("orderType", orderType);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskDispenserMonitoring>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_DISPENSER_MONITORING_SERVICE).ToString());
        }

        return "";
    }

    public string CallListCustomerContacts(string searchText, DateTime startDate, DateTime endDate
        , int instutionId
        , int transactionId
        , int kioskId
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListKioskDispenserMonitoring parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.NOKTALAR);
            data.Add("adminUserId", adminUserId);
            data.Add("ilKod", "06");
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("instutionId", instutionId);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("transactionId", transactionId);
            data.Add("orderType", orderType);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Desmer.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskDispenserMonitoring>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_DISPENSER_MONITORING_SERVICE).ToString());
        }

        return "";
    }

    public ParserOperation CallSaveCutOffMonitoringService(DateTime startDate
        , DateTime endDate
        , string cutDurationText
        , string kioskId
        , int instutionId
        , string cutReasonText
        , DateTime insertDate
        , Int64 adminUserId
        , string accessToken)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.SAVE_CUT_CASH_OFF_MONITORING);
            data.Add("adminUserId", adminUserId);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("cutDurationText", cutDurationText);
            data.Add("instutionId", instutionId);
            data.Add("cutReasonText", cutReasonText);
            data.Add("kioskId", kioskId);

            data.Add("insertDate", insertDate);

            data.Add("accessToken", accessToken);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.SAVE_CUT_CASH_OFF_MONITORING).ToString());
        }

        return parser;
    }


    public ParserOperation CallUpdateCutOffMonitoringService(int cutId,
        DateTime startDate
        , DateTime endDate
        , string cutDurationText
        , string kioskId
        , int instutionId
        , string cutReasonText
        , DateTime insertDate
        , Int64 adminUserId
        , string accessToken)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.UPDATE_CUT_CASH_OFF_MONITORING);
            data.Add("adminUserId", adminUserId);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("cutDurationText", cutDurationText);
            data.Add("instutionId", instutionId);
            data.Add("cutReasonText", cutReasonText);
            data.Add("kioskId", kioskId);
            data.Add("cutId", cutId);

            data.Add("insertDate", insertDate);

            data.Add("accessToken", accessToken);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.UPDATE_CUT_CASH_OFF_MONITORING).ToString());
        }

        return parser;
    }

    public ParserOperation CallUpdateKioskErrorLogsService(int errorId
        , string explanation
        , Int64 adminUserId
        , string accessToken)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.UPDATE_ERROR_LOGS);
            data.Add("adminUserId", adminUserId);
            data.Add("explanation", explanation);
            data.Add("errorId", errorId);

            data.Add("accessToken", accessToken);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.UPDATE_ERROR_LOGS).ToString());
        }

        return parser;
    }


    public ParserListSetting CallGetSettingService(string billFieldNotificationLimit,
        int isNotificationLimitReceiptActive,
        string cashBoxNotificationLimit,
        int isNotificationCashBoxLimitActive,
        int isYellowAlertActive,
        int isRedAlertActive,
        string moneyCodeGenerationLimit,
        string moneyCodeGenerationCountLimit,
        string oneyCodeGenerationCountLimitPeriod,
        string bGazUpperLoadingLimit,
        int adminUserId,
        string accessToken)
    {
        ParserListSetting parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("adminUserId", adminUserId);
            data.Add("functionCode", (int) FC.GET_KIOSK_SETTINGS);
            data.Add("billFieldNotificationLimit", billFieldNotificationLimit);
            data.Add("isNotificationLimitReceiptActive", isNotificationLimitReceiptActive);
            data.Add("cashBoxNotificationLimit", cashBoxNotificationLimit);
            data.Add("isNotificationCashBoxLimitActive", isNotificationCashBoxLimitActive);
            data.Add("isYellowAlertActive", isYellowAlertActive);
            data.Add("isRedAlertActive", isRedAlertActive);
            data.Add("moneyCodeGenerationLimit", moneyCodeGenerationLimit);
            data.Add("moneyCodeGenerationCountLimit", moneyCodeGenerationCountLimit);
            data.Add("oneyCodeGenerationCountLimitPeriod", oneyCodeGenerationCountLimitPeriod);
            data.Add("bGazUpperLoadingLimit", bGazUpperLoadingLimit);

            //data.Add("approvedCode", approvedCode);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListSetting>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_KIOSK_SETTINGS).ToString());
        }

        return parser;
    }


    //CallGetCommisionSettingService
    public ParserGetCommisionSetting CallCommisionSettingService(int adminUserId,
        string accessToken)
    {
        ParserGetCommisionSetting parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("adminUserId", adminUserId);
            data.Add("functionCode", (int) FC.KIOSK_COMMISION_SETTINGS);


            //data.Add("approvedCode", approvedCode);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserGetCommisionSetting>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_KIOSK_COMMISION_SETTINGS).ToString());
        }

        return parser;
    }


    public GetKioskCommisionSetting CallGetCommisionSettingService(int instutionId, string accessToken
        , Int64 adminUserId)
    {
        GetKioskCommisionSetting parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_KIOSK_COMMISION_SETTINGS);
            data.Add("cutId", instutionId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<GetKioskCommisionSetting>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.KIOSK_COMMISION_SETTINGS).ToString());
        }

        return parser;
    }

    //CallUpdateCommisionSettingService
    public ParserOperation CallUpdateCommisionSettingService(int instutionId
        , string creditText
        , string mobileText
        , string cashText
        , string yellowText
        , string redText
        , Int64 adminUserId
        , string accessToken)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.UPDATE_KIOSK_COMMISION_SETTINGS);
            data.Add("adminUserId", adminUserId);
            data.Add("instutionId", instutionId);
            data.Add("creditCommision", creditText);
            data.Add("mobileCommision", mobileText);
            data.Add("cashCommision", cashText);
            data.Add("yellowAlarm", yellowText);
            data.Add("redAlarm", redText);
            data.Add("accessToken", accessToken);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.UPDATE_KIOSK_COMMISION_SETTINGS).ToString());
        }

        return parser;
    }


    public ParserOperation CallSaveSettingService(string billFieldNotificationLimit,
        int isNotificationLimitReceiptActive,
        string cashBoxNotificationLimit,
        int isNotificationCashBoxLimitActive,
        int isYellowAlertActive,
        int isRedAlertActive,
        string moneyCodeGenerationLimit,
        string moneyCodeGenerationCountLimit,
        string oneyCodeGenerationCountLimitPeriod,
        string bGazUpperLoadingLimit,
        string accessToken,
        Int64 adminUserId)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();

            data.Add("functionCode", (int) FC.SAVE_KIOSK_SETTINGS);
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("billFieldNotificationLimit", billFieldNotificationLimit);
            data.Add("isNotificationLimitReceiptActive", isNotificationLimitReceiptActive);
            data.Add("cashBoxNotificationLimit", cashBoxNotificationLimit);
            data.Add("isNotificationCashBoxLimitActive", isNotificationCashBoxLimitActive);
            data.Add("isYellowAlertActive", isYellowAlertActive);
            data.Add("isRedAlertActive", isRedAlertActive);
            data.Add("moneyCodeGenerationLimit", moneyCodeGenerationLimit);
            data.Add("moneyCodeGenerationCountLimit", moneyCodeGenerationCountLimit);
            data.Add("oneyCodeGenerationCountLimitPeriod", oneyCodeGenerationCountLimitPeriod);
            data.Add("bGazUpperLoadingLimit", bGazUpperLoadingLimit);
            data.Add("adminUserId", adminUserId);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.SAVE_KIOSK_SETTINGS).ToString());
        }

        return parser;
    }


    public ParserOperation CallSaveCommisionSettingsService(string creditAski,
        string mobileAski,
        string cashAski,
        string yellowAski,
        string redAski,
        string creditBaskent,
        string mobileBaskent,
        string cashBaskent,
        string yellowBaskent,
        string redBaskent,
        string creditAksaray,
        string mobileAksaray,
        string cashAksaray,
        string yellowAksaray,
        string redAksaray,
        string accessToken,
        Int64 adminUserId)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();

            data.Add("functionCode", (int) FC.SAVE_COMMISION_SETTINGS);
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("creditAski", creditAski);
            data.Add("mobileAski", mobileAski);
            data.Add("cashAski", cashAski);
            data.Add("yellowAski", yellowAski);
            data.Add("redAski", redAski);
            data.Add("creditBaskent", creditBaskent);
            data.Add("mobileBaskent", mobileBaskent);
            data.Add("cashBaskent", cashBaskent);
            data.Add("yellowBaskent", yellowBaskent);
            data.Add("redBaskent", redBaskent);
            data.Add("creditAksaray", creditAksaray);
            data.Add("mobileAksaray", mobileAksaray);
            data.Add("cashAksaray", cashAksaray);
            data.Add("yellowAksaray", yellowAksaray);
            data.Add("redAksaray", redAksaray);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.SAVE_COMMISION_SETTINGS).ToString());
        }

        return parser;
    }


    public ParserSaveKiosk CallSaveKioskEmptyCashCountService(int kioskId
        , int adminUserId
        , string accessToken
        , string firstDenomNumber
        , string secondDenomNumber
        , string thirdDenomNumber
        , string fourthDenomNumber
        , string fifthDenomNumber
        , string sixthDenomNumber
        , string seventhDenomNumber
        , string eighthDenomNumber
        , string ninthDenomNumber
        , string tenthDenomNumber
        , string eleventhDenomNumber
        , decimal totalAmount
        //, string approvedCode     
    )
    {
        ParserSaveKiosk parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.SAVE_KIOSK_EMPTY_CASH_COUNT);
            data.Add("adminUserId", adminUserId);
            data.Add("kioskId", kioskId);
            data.Add("firstDenomNumber", firstDenomNumber);
            data.Add("secondDenomNumber", secondDenomNumber);
            data.Add("thirdDenomNumber", thirdDenomNumber);
            data.Add("fourthDenomNumber", fourthDenomNumber);
            data.Add("fifthDenomNumber", fifthDenomNumber);
            data.Add("sixthDenomNumber", sixthDenomNumber);
            data.Add("seventhDenomNumber", seventhDenomNumber);
            data.Add("eighthDenomNumber", eighthDenomNumber);
            data.Add("ninthDenomNumber", ninthDenomNumber);
            data.Add("tenthDenomNumber", tenthDenomNumber);
            data.Add("eleventhDenomNumber", eleventhDenomNumber);
            data.Add("totalAmount", totalAmount);
            //data.Add("approvedCode", approvedCode);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserSaveKiosk>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.SAVE_KIOSK_EMPTY_CASH_COUNT).ToString());
        }

        return parser;
    }


    public ParserListCashCounts CallGetCashCountService(int operatinType,
        int itemId,
        int numberOfItemsPerPage,
        int currentPageNum,
        int orderSelectionColumn,
        int descAsc,
        string accessToken,
        Int64 adminUserId)
    {
        ParserListCashCounts parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_KIOSK_CASH_COUNT);
            data.Add("itemId", itemId);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("operatinType", operatinType);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCashCounts>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_KIOSK_CASH_COUNT).ToString());
        }

        return parser;
    }

    public ParserListTotalCashCounts CallGetTotalCashCountService(DateTime startDate,
        DateTime endDate,
        int operatinType,
        //int itemId,
        int numberOfItemsPerPage,
        int currentPageNum,
        int orderSelectionColumn,
        int descAsc,
        string accessToken,
        Int64 adminUserId)
    {
        ParserListTotalCashCounts parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_KIOSK_TOTAL_CASH_COUNT);
            //data.Add("itemId", itemId);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("operatinType", operatinType);
            data.Add("adminUserId", adminUserId);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListTotalCashCounts>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_KIOSK_TOTAL_CASH_COUNT).ToString());
        }

        return parser;
    }

    public ParserListCashBoxCounts CallGetFullCashBoxCashCountService(int operatinType,
        int itemId,
        int numberOfItemsPerPage,
        int currentPageNum,
        int orderSelectionColumn,
        int descAsc,
        string accessToken,
        Int64 adminUserId)
    {
        ParserListCashBoxCounts parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_KIOSK_CASHBOX_COUNT);
            data.Add("itemId", itemId);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("operatinType", operatinType);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCashBoxCounts>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_KIOSK_CASHBOX_COUNT).ToString());
        }

        return parser;
    }

    public ParserListCashCounts CallGetCashCountForExcelService(int operatinType,
        int itemId,
        int orderSelectionColumn,
        int descAsc,
        string accessToken,
        Int64 adminUserId)
    {
        ParserListCashCounts parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_GET_KIOSK_CASH_COUNT);
            data.Add("itemId", itemId);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("operatinType", operatinType);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCashCounts>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_GET_KIOSK_CASH_COUNT).ToString());
        }

        return parser;
    }

    public ParserListCashBoxCounts CallGetFullCashBoxCountForExcelService(int operatinType,
        int itemId,
        int orderSelectionColumn,
        int descAsc,
        string accessToken,
        Int64 adminUserId)
    {
        ParserListCashBoxCounts parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_GET_KIOSK_CASHBOX_COUNT);
            data.Add("itemId", itemId);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("operatinType", operatinType);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCashBoxCounts>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_GET_KIOSK_CASHBOX_COUNT).ToString());
        }

        return parser;
    }


    //CallListKioskDispenserCashCountService
    public ParserListKioskDispenserCashCount CallListKioskDispenserCashCountService(string kioskId
        , string searchText
        , int instutionId
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListKioskDispenserCashCount parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSK_DISPENSER_CASH_COUNT);


            data.Add("kioskId", kioskId);
            data.Add("searchText", searchText);
            data.Add("instutionId", instutionId);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);

            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskDispenserCashCount>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_DISPENSER_CASH_COUNT).ToString());
        }

        return parser;
    }

    //CallGetDispenserCashCountService
    public ParserGetKioskCashDispenser CallGetDispenserCashCountService(int kioskId, string accessToken
        , Int64 adminUserId)
    {
        ParserGetKioskCashDispenser parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_KIOSK_DISPENSER_CASH_COUNT);
            data.Add("kioskId", kioskId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserGetKioskCashDispenser>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_KIOSK_DISPENSER_CASH_COUNT).ToString());
        }

        return parser;
    }


    //CallListKioskDispenserCashCountService
    public ParserListKioskCashBoxCashCountError CallListKioskCashBoxCashCountErrorService(string kioskId
        , string searchText
        , int instutionId
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListKioskCashBoxCashCountError parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSK_CASHBOX_CASH_COUNT_ERROR);


            data.Add("kioskId", kioskId);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("instutionId", instutionId);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);

            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskCashBoxCashCountError>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_DISPENSER_CASH_COUNT).ToString());
        }

        return parser;
    }

    /*
    public ParserGetCashBoxRejectCounts CallGetCashBoxRejectService(int itemId,
                                                    string accessToken,
                                                    Int64 adminUserId)
    {
        ParserGetCashBoxRejectCounts parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int)FC.GET_KIOSK_CASHBOX_REJECT_COUNT);
            data.Add("itemId", itemId);
            
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
                (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {

                parser = JsonConvert.DeserializeObject<ParserGetCashBoxRejectCounts>(responseText);
            }

        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex, ((int)FC.GET_KIOSK_CASHBOX_COUNT).ToString());
        }

        return parser;
    }
*/


    public ParserCancelTnx CallCancelTransactionService(int transactionId
        , int userId
        , string accessToken
        , Int64 adminUserId
        , string customerNo
        , string billDate
        , string billNumber)
    {
        ParserCancelTnx parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.CALL_CANCEL_TXN_SERVICE);
            data.Add("transactionId", transactionId);
            data.Add("userId", userId);
            data.Add("adminUserId", adminUserId);
            data.Add("customerNo", customerNo);
            data.Add("billDate", billDate);
            data.Add("billNumber", billNumber);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserCancelTnx>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GENERATE_MONEY_CODE).ToString());
        }

        return parser;
    }

    public ParserCancelTnx CallChangeStatusService(int transactionId
        , int userId
        , string accessToken
        , Int64 adminUserId
        , string explanation
        , string newStatus)
    {
        ParserCancelTnx parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.CALL_STATUS_TXN_SERVICE);
            data.Add("transactionId", transactionId);
            data.Add("userId", userId);
            data.Add("adminUserId", adminUserId);
            data.Add("explanation", explanation);
            data.Add("newStatus", newStatus);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserCancelTnx>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GENERATE_MONEY_CODE).ToString());
        }

        return parser;
    }

    public ParserCancelTnxLog UpdateTransactionCancel(int transactionId
        , int userId
        , string accessToken
        , Int64 adminUserId
        , string customerNo
        , string billDate
        , string billNumber
        , string serviceResult)
    {
        ParserCancelTnxLog parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.CALL_UPDATE_CANCEL_TXN_LOG);
            data.Add("transactionId", transactionId);
            data.Add("userId", userId);
            data.Add("adminUserId", adminUserId);
            data.Add("customerNo", customerNo);
            data.Add("billDate", billDate);
            data.Add("billNumber", billNumber);
            data.Add("serviceResult", serviceResult);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserCancelTnxLog>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GENERATE_MONEY_CODE).ToString());
        }

        return parser;
    }

    public ParserSaveMoneyCode CallSaveMoneyCodeService(int transactionId
        , int userId
        , string codeAmount
        , string accessToken
        , Int64 adminUserId
        , string cellPhone)
    {
        ParserSaveMoneyCode parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GENERATE_MONEY_CODE);
            data.Add("transactionId", transactionId);
            data.Add("userId", userId);
            data.Add("adminUserId", adminUserId);
            data.Add("codeAmount", codeAmount);
            data.Add("cellPhone", cellPhone);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserSaveMoneyCode>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GENERATE_MONEY_CODE).ToString());
        }

        return parser;
    }

    public ParserSaveMoneyCode CallSendMoneyCodeService(int transactionId
        , int moneyCodeId
        , string accessToken
        , Int64 adminUserId
        , string cellPhone)
    {
        ParserSaveMoneyCode parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.SEND_MONEY_CODE);
            data.Add("transactionId", transactionId);
            data.Add("moneyCodeId", moneyCodeId);
            data.Add("adminUserId", adminUserId);
            data.Add("cellPhone", cellPhone);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserSaveMoneyCode>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.SEND_MONEY_CODE).ToString());
        }

        return parser;
    }


    public ParserListMoneyCodeSMSNumber CallGetSMSLogDetailsService(Int64 moneyCodeId, string accessToken
        , Int64 adminUserId)
    {
        ParserListMoneyCodeSMSNumber parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_MONEY_CODE_SMS_NUMBER);

            data.Add("moneyCodeId", moneyCodeId);
            data.Add("accessToken", accessToken);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListMoneyCodeSMSNumber>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_MONEY_CODE_SMS_NUMBER).ToString());
        }

        return parser;
    }


    public ParserOperation CallGenetareCustomerPinService(int customerId, int userId, string accessToken
        , Int64 adminUserId)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GENERATE_WEB_CUSTOMER_PIN);
            data.Add("customerId", customerId);
            data.Add("userId", userId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Customer.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GENERATE_WEB_CUSTOMER_PIN).ToString());
        }

        return parser;
    }

    public ParserGetSecretKey CallGetCustomerKeyService(int customerId, int userId, string accessToken
        , Int64 adminUserId)
    {
        ParserGetSecretKey parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_WEB_CUSTOMER_KEY);
            data.Add("customerId", customerId);
            data.Add("userId", userId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Customer.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserGetSecretKey>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_WEB_CUSTOMER_KEY).ToString());
        }

        return parser;
    }

    public ParserChangeCustomerInfo CallGetChangeCustomerInfoService(int customerId, int userId, string accessToken
        , Int64 adminUserId)
    {
        ParserChangeCustomerInfo parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_WEB_CUSTOMER_INFO);
            data.Add("customerId", customerId);
            data.Add("userId", userId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Customer.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserChangeCustomerInfo>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_WEB_CUSTOMER_INFO).ToString());
        }

        return parser;
    }


    public ParserOperation CallUpdateCustomerInfoService(int customerId, int userId, string email, string cellphone,
        string accessToken
        , Int64 adminUserId)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.UPDATE_WEB_CUSTOMER_INFO);
            data.Add("customerId", customerId);
            data.Add("userId", userId);
            data.Add("email", email);
            data.Add("cellphone", cellphone);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Customer.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.UPDATE_WEB_CUSTOMER_INFO).ToString());
        }

        return parser;
    }

    public ParserListItems CallGetMoneyCodeStatusNames(string accessToken
        , Int64 adminUserId)
    {
        ParserListItems parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_MONEY_CODE_STATUS);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListItems>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_MONEY_CODE_STATUS).ToString());
        }

        return parser;
    }

    public ParserListMoneyCodes CallListMoneyCodeService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , int codeStatus
        , int generatedType, string accessToken
        , Int64 adminUserId
        , int instutionId
        , string generatedKioskIds
        , string usedKioskIds
        , int paymentType
        , int orderType)
    {
        ParserListMoneyCodes parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_MONEY_CODE);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("codeStatus", codeStatus);
            data.Add("instutionId", instutionId);
            data.Add("generatedType", generatedType);
            data.Add("adminUserId", adminUserId);
            data.Add("generatedKioskIds", generatedKioskIds);
            data.Add("usedKioskIds", usedKioskIds);
            data.Add("paymentType", paymentType);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListMoneyCodes>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_MONEY_CODE).ToString());
        }

        return parser;
    }

    public ParserListMoneyCodes CallListMoneyCodeServiceSweepBack(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , int codeStatus
        , int generatedType, string accessToken
        , Int64 adminUserId
        , int instutionId
        , int orderType)
    {
        ParserListMoneyCodes parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_MONEY_CODE_SWEEP_BACK);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("codeStatus", codeStatus);
            data.Add("instutionId", instutionId);
            data.Add("generatedType", generatedType);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListMoneyCodes>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_MONEY_CODE_SWEEP_BACK).ToString());
        }

        return parser;
    }

    public ParserListMoneyCodes CallListMoneyCodeForExcelService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , int codeStatus
        , int generatedType, string accessToken
        , Int64 adminUserId
        , int instutionId
        , string generatedKioskIds
        , string usedKioskIds)
    {
        ParserListMoneyCodes parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_MONEY_CODE);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("codeStatus", codeStatus);
            data.Add("instutionId", instutionId);
            data.Add("generatedType", generatedType);
            data.Add("adminUserId", adminUserId);
            data.Add("generatedKioskIds", generatedKioskIds);
            data.Add("usedKioskIds", usedKioskIds);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListMoneyCodes>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_MONEY_CODE).ToString());
        }

        return parser;
    }

    public ParserListAccountDetails CallGetAccountDetailService(int accountId
        , int instutionId
        , int numberOfItemsPerPage
        , int currentPageNum
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListAccountDetails parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_ACCOUNT_DETAIL);
            data.Add("accountId", accountId);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("adminUserId", adminUserId);
            data.Add("instutionId", instutionId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListAccountDetails>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_ACCOUNT_DETAIL).ToString());
        }

        return parser;
    }

    public ParserListAccounts CallListAccountService(int numberOfItemsPerPage,
        int currentPageNum,
        int recordCount,
        int pageCount,
        int orderSelectionColumn,
        int descAsc,
        int instutionId,
        string accessToken,
        Int64 adminUserId,
        int orderType)
    {
        ParserListAccounts parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_ACCOUNT);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListAccounts>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_ACCOUNT).ToString());
        }

        return parser;
    }

    public ParserListAccounts CallListAccountForExcelService(int instutionId,
        string accessToken,
        Int64 adminUserId)
    {
        ParserListAccounts parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_ACCOUNT);
            data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListAccounts>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_ACCOUNT).ToString());
        }

        return parser;
    }

    public ParserListTransactions CallListTransactionDetailsService(long transactionId, string accessToken,
        int adminUserId)
    {
        ParserListTransactions parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_TRANSACTION_DETAILS);
            data.Add("transactionId", transactionId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListTransactions>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_TRANSACTION).ToString());
        }

        return parser;
    }

    public ParserListTransactions CallListTransactionService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , int customerId
        , string kioskId
        , int instutionId
        , int operationTypeId
        , string txnStatusId
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListTransactions parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_TRANSACTION);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("operationTypeId", operationTypeId);
            data.Add("txnStatusId", txnStatusId);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            if (customerId != 0)
            {
                data.Add("customerId", customerId);
            }

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListTransactions>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_TRANSACTION).ToString());
        }

        return parser;
    }


    public ParserListKioskLocalLog CallListKioskLocalLog(string searchText,
        DateTime startDateField
        , DateTime endDateField
        , int logStatus
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        //, int instutionId
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListKioskLocalLog parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSK_LOCAL_LOG);
            data.Add("searchText", searchText);
            data.Add("startDate", startDateField);
            data.Add("endDate", endDateField);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            // data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);
            data.Add("logStatus", logStatus);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskLocalLog>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_LOCAL_LOG).ToString());
        }

        return parser;
    }


    //  *?* LIST_CREDIT_CARD_TRANSACTION
    public ParserListCreditCardTransactions CallListCreditCardTransactionService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , int instutionId
        , int muhasebeStatusId
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListCreditCardTransactions parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_CREDIT_CARD_TRANSACTION);

            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("muhasebeStatusId", muhasebeStatusId);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCreditCardTransactions>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_CREDIT_CARD_TRANSACTION).ToString());
        }

        return parser;
    }


    //  *?* LIST_CREDIT_CARD_TRANSACTION
    public ParserListMobilePaymentTransactions CallListMobilePaymentTransactionService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , int instutionId
        , int muhasebeStatusId
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListMobilePaymentTransactions parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_MOBILE_PAYMENT_TRANSACTION);

            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);
            data.Add("muhasebeStatusId", muhasebeStatusId);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListMobilePaymentTransactions>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_MOBILE_PAYMENT_TRANSACTION).ToString());
        }

        return parser;
    }

    public ParserListRepairCard CallListRepairCardService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , int instutionId
        , int cardRepairStatus
        , int isCardDeleted
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListRepairCard parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_CARD_REPAIR);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);
            data.Add("instutionId", instutionId);
            data.Add("cardRepairStatus", cardRepairStatus);
            data.Add("isCardDeleted", isCardDeleted);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListRepairCard>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_CARD_REPAIR).ToString());
        }

        return parser;
    }


    public ParserListFailedTransactions CallListFailedTransactionService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , int customerId
        , string kioskId
        , int instutionId
        , int operationTypeId
        , string txnStatusId
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListFailedTransactions parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_FAILED_TRANSACTION);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("operationTypeId", operationTypeId);
            data.Add("txnStatusId", txnStatusId);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            if (customerId != 0)
            {
                data.Add("customerId", customerId);
            }

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListFailedTransactions>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_FAILED_TRANSACTION).ToString());
        }

        return parser;
    }


    public ParserListAutoMutabakat CallListAutoMutabakatService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , int userId
        //, int state
        , int instutionId
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListAutoMutabakat parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_AUTO_MUTABAKAT_TRANSACTION);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("instutionId", instutionId);
            data.Add("accessToken", accessToken);
            data.Add("userId", userId);
            //data.Add("state", state);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListAutoMutabakat>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_TRANSACTION).ToString());
        }

        return parser;
    }


    //public ParserListKioskCashAcceptor CallListKioskCashAcceptorForExcelService(
    //                                                DateTime startDate
    //                                               , DateTime endDate
    //                                               , int numberOfItemsPerPage
    //                                               , int currentPageNum
    //                                               , int recordCount
    //                                               , int pageCount
    //                                               , int orderSelectionColumn
    //                                               , int descAsc
    //                                               , string kioskIds
    //                                               , int instutionId
    //                                               , string accessToken
    //                                               , Int64 adminUserId, int orderType)
    //{

    //    ParserListKioskCashAcceptor parser = null;
    //    try
    //    {
    //        HttpStatusCode statusCode = HttpStatusCode.Accepted;

    //        JObject data = new JObject();
    //        data.Add("apiKey", "TEST");
    //        data.Add("apiPass", "ABCD1234");
    //        data.Add("functionCode", (int)FC.EXCEL_LIST_KIOSKS_CASH_ACCEPTOR); 
    //        data.Add("startDate", startDate);
    //        data.Add("endDate", endDate);
    //        data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
    //        data.Add("currentPageNum", currentPageNum);
    //        data.Add("recordCount", recordCount);
    //        data.Add("pageCount", pageCount);
    //        data.Add("orderSelectionColumn", orderSelectionColumn);
    //        data.Add("descAsc", descAsc);
    //        data.Add("instutionId", instutionId);
    //        data.Add("accessToken", accessToken);
    //        data.Add("kioskIds", kioskIds);
    //        data.Add("adminUserId", adminUserId);
    //        data.Add("orderType", orderType);


    //        string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

    //        string responseText = Utility.ExecuteHttpRequest
    //            (URL,
    //            "POST",
    //            System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
    //            ref statusCode);

    //        if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
    //        {

    //            parser = JsonConvert.DeserializeObject<ParserListKioskCashAcceptor>(responseText);
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex, ((int)FC.EXCEL_LIST_KIOSKS_CASH_ACCEPTOR).ToString());
    //    }

    //    return parser;
    //}


    //public ParserListKioskCashDispenser CallListKioskCashDispenserForExcelService(
    //                                                DateTime startDate
    //                                               , DateTime endDate
    //                                               , int numberOfItemsPerPage
    //                                               , int currentPageNum
    //                                               , int recordCount
    //                                               , int pageCount
    //                                               , int orderSelectionColumn
    //                                               , int descAsc
    //                                               , int kioskIds
    //                                               , int instutionId
    //                                               , string accessToken
    //                                               , Int64 adminUserId)
    //{

    //    ParserListKioskCashDispenser parser = null;
    //    try
    //    {
    //        HttpStatusCode statusCode = HttpStatusCode.Accepted;

    //        JObject data = new JObject();
    //        data.Add("apiKey", "TEST");
    //        data.Add("apiPass", "ABCD1234");
    //        data.Add("functionCode", (int)FC.EXCEL_LIST_KIOSKS_CASH_DISPENSER);
    //        data.Add("startDate", startDate);
    //        data.Add("endDate", endDate);
    //        data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
    //        data.Add("currentPageNum", currentPageNum);
    //        data.Add("recordCount", recordCount);
    //        data.Add("pageCount", pageCount);
    //        data.Add("orderSelectionColumn", orderSelectionColumn);
    //        data.Add("descAsc", descAsc);
    //        data.Add("instutionId", instutionId);
    //        data.Add("accessToken", accessToken);
    //        data.Add("kioskIds", kioskIds);
    //        data.Add("adminUserId", adminUserId);

    //        string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

    //        string responseText = Utility.ExecuteHttpRequest
    //            (URL,
    //            "POST",
    //            System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
    //            ref statusCode);

    //        if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
    //        {

    //            parser = JsonConvert.DeserializeObject<ParserListKioskCashDispenser>(responseText);
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex, ((int)FC.EXCEL_LIST_KIOSKS_CASH_DISPENSER).ToString());
    //    }

    //    return parser;
    //}


    public ParserListDevices CallListDeviceService(string searchText
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , string kioskId
        , int deviceId
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListDevices parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_DEVICE);
            data.Add("searchText", searchText);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("kioskId", kioskId);
            data.Add("custemerId", deviceId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListDevices>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_DEVICE).ToString());
        }

        return parser;
    }


    public ParserListLocalLogs CallListLocalLogsService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , string accessToken
        , Int64 adminUserId
        , int processType
        , int orderType)
    {
        ParserListLocalLogs parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_LOCAL_LOGS);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("kioskId", kioskId);
            data.Add("adminUserId", adminUserId);
            data.Add("processType", processType);
            data.Add("orderType", orderType);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListLocalLogs>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_LOCAL_LOGS).ToString());
        }

        return parser;
    }


    public ParserListErrorLogs CallListErrorLogsService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , string kioskId
        , string accessToken
        , Int64 adminUserId
        , int processType
        , int orderSelectionColumn
        , int descAsc
        , int orderType)
    {
        ParserListErrorLogs parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_ERROR_LOGS);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("kioskId", kioskId);
            data.Add("adminUserId", adminUserId);
            data.Add("processType", processType);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListErrorLogs>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_LOCAL_LOGS).ToString());
        }

        return parser;
    }

    public ParserListMaintenance CallListMaintanenceService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListMaintenance parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_MAINTENANCE);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("kioskId", kioskId);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            data.Add("descAsc", descAsc);
            data.Add("orderSelectionColumn", orderSelectionColumn);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListMaintenance>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_MAINTENANCE).ToString());
        }

        return parser;
    }

    //public ParserListMaintenance CallListMaintanenceForExcelService(string searchText
    //                                               , DateTime startDate
    //                                               , DateTime endDate
    //                                               , string kioskId
    //                                               , string accessToken
    //                                               , Int64 adminUserId)
    //{

    //    ParserListMaintenance parser = null;
    //    try
    //    {
    //        HttpStatusCode statusCode = HttpStatusCode.Accepted;

    //        JObject data = new JObject();
    //        data.Add("apiKey", "TEST");
    //        data.Add("apiPass", "ABCD1234");
    //        data.Add("functionCode", (int)FC.EXCEL_LIST_MAINTENANCE);
    //        data.Add("searchText", searchText);
    //        data.Add("startDate", startDate);
    //        data.Add("endDate", endDate);
    //        data.Add("kioskId", kioskId);
    //        data.Add("adminUserId", adminUserId);

    //        string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

    //        string responseText = Utility.ExecuteHttpRequest
    //            (URL,
    //            "POST",
    //            System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
    //            ref statusCode);

    //        if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
    //        {

    //            parser = JsonConvert.DeserializeObject<ParserListMaintenance>(responseText);
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex, ((int)FC.EXCEL_LIST_MAINTENANCE).ToString());
    //    }

    //    return parser;
    //}

    public ParserListAccountTransactions CallListDailyAccountTransactionService(Int64 itemId
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListAccountTransactions parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_DAILY_ACCOUNT_TRANSACTION);
            data.Add("itemId", itemId);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListAccountTransactions>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_DAILY_ACCOUNT_TRANSACTION).ToString());
        }

        return parser;
    }

    public ParserListAccountTransactionsProkis CallListDailyAccountTransactionProkisService(Int64 itemId
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListAccountTransactionsProkis parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_DAILY_ACCOUNT_TRANSACTION_PROKIS);
            data.Add("itemId", itemId);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListAccountTransactionsProkis>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_DAILY_ACCOUNT_TRANSACTION_PROKIS).ToString());
        }

        return parser;
    }


    public ParserListMoneyCodes CallListDailyAskiCodesService(Int64 itemId
        , int isUsed
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListMoneyCodes parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_DAILY_MONEY_CODES);
            data.Add("itemId", itemId);
            data.Add("isUsed", isUsed);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListMoneyCodes>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_DAILY_MONEY_CODES).ToString());
        }

        return parser;
    }

    public ParserListCusomerAccounts CallListDailyCustomerAccountService(Int64 itemId
        , int isActive
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListCusomerAccounts parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_DAILY_CUSTOMER_ACCOUNTS);
            data.Add("itemId", itemId);
            data.Add("isActive", isActive);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCusomerAccounts>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_DAILY_CUSTOMER_ACCOUNTS).ToString());
        }

        return parser;
    }

    public ParserListAccountTransactions CallListAccountTransactionService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string platform
        , int whichAction
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListAccountTransactions parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_ACCOUNT_TRANSACTION);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("platform", platform);
            data.Add("whichAction", whichAction);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListAccountTransactions>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_ACCOUNT_TRANSACTION).ToString());
        }

        return parser;
    }

    public ParserListPurseTransactions CallListPurseTransactionService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc, string accessToken
        , Int64 adminUserId)
    {
        ParserListPurseTransactions parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_PURSE_TRANSACTION);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListPurseTransactions>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_PURSE_TRANSACTION).ToString());
        }

        return parser;
    }

    public ParserListPurseTransactions CallListPurseTransactionForExcelService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int orderSelectionColumn
        , int descAsc, string accessToken
        , Int64 adminUserId)
    {
        ParserListPurseTransactions parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_PURSE_TRANSACTION);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListPurseTransactions>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_PURSE_TRANSACTION).ToString());
        }

        return parser;
    }

    public ParserListAccountTransactions CallListAccountTransactionForExcelService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int orderSelectionColumn
        , int descAsc
        , string platform
        , int whichAction, string accessToken
        , Int64 adminUserId)
    {
        ParserListAccountTransactions parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_ACCOUNT_TRANSACTION);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("platform", platform);
            data.Add("whichAction", whichAction);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListAccountTransactions>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_ACCOUNT_TRANSACTION).ToString());
        }

        return parser;
    }

    public ParserListAccountTransactions CallListDailyAccountTransactionForExcelService(Int64 itemId
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListAccountTransactions parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_DAILY_ACCOUNT_TRANSACTION);
            data.Add("itemId", itemId);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListAccountTransactions>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_DAILY_ACCOUNT_TRANSACTION).ToString());
        }

        return parser;
    }

    public ParserListAccountTransactionsProkis CallListDailyAccountTransactionProkisForExcelService(Int64 itemId
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListAccountTransactionsProkis parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_DAILY_ACCOUNT_TRANSACTION_PROKIS);
            data.Add("itemId", itemId);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListAccountTransactionsProkis>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_DAILY_ACCOUNT_TRANSACTION_PROKIS).ToString());
        }

        return parser;
    }

    public ParserListMoneyCodes CallListDailyAskiCodesForExcelService(Int64 itemId
        , int isUsed
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListMoneyCodes parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_DAILY_MONEY_CODES);
            data.Add("itemId", itemId);
            data.Add("isUsed", isUsed);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListMoneyCodes>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_DAILY_MONEY_CODES).ToString());
        }

        return parser;
    }

    public ParserListCusomerAccounts CallListDailyCustomerAccountsForExcelService(Int64 itemId
        , int isActive
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListCusomerAccounts parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_DAILY_CUSTOMER_ACCOUNTS);
            data.Add("itemId", itemId);
            data.Add("isActive", isActive);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCusomerAccounts>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_DAILY_CUSTOMER_ACCOUNTS).ToString());
        }

        return parser;
    }

    public ParserListTransactions CallListTransactionForExcelService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int orderSelectionColumn
        , int descAsc
        , int customerId
        , string kioskId
        , int instutionId
        , int operationTypeId
        , string txnStatusId
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListTransactions parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_TRANSACTION);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("operationTypeId", operationTypeId);
            data.Add("txnStatusId", txnStatusId);
            data.Add("adminUserId", adminUserId);

            if (customerId != 0)
            {
                data.Add("customerId", customerId);
            }

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListTransactions>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_TRANSACTION).ToString());
        }

        return parser;
    }

    public ParserListTransactionDetails CallListTransactionDetailService(int transactionId, string accessToken
        , Int64 adminUserId)
    {
        ParserListTransactionDetails parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_TRANSACTION_DETAIL);
            data.Add("transactionId", transactionId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListTransactionDetails>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_TRANSACTION_DETAIL).ToString());
        }

        return parser;
    }

    public ParserListTransactionDetails CallListPurseTransactionDetailService(int transactionId,
        string accessToken,
        Int64 adminUserId)
    {
        ParserListTransactionDetails parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_PURSE_TRANSACTION_DETAIL);
            data.Add("transactionId", transactionId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListTransactionDetails>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_PURSE_TRANSACTION_DETAIL).ToString());
        }

        return parser;
    }

    public ParserListCustomers CallListCustomerService(string searchText
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , int instutionId
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListCustomers parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_CUSTOMERS);
            data.Add("searchText", searchText);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Customer.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCustomers>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_CUSTOMERS).ToString());
        }

        return parser;
    }

    public ParserListWebCustomers CallListWebCustomerService(string searchNameText
        , string searchCustNoText
        , string searchTcknText
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int tableType
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListWebCustomers parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_WEB_CUSTOMERS);
            data.Add("searchNameText", searchNameText);
            data.Add("searchCustNoText", searchCustNoText);
            data.Add("searchTcknText", searchTcknText);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("tableType", tableType);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Customer.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListWebCustomers>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_WEB_CUSTOMERS).ToString());
        }

        return parser;
    }

    public ParserListWebCustomers CallListWebCustomerForExcelService(string searchNameText
        , string searchCustNoText
        , string searchTcknText, string accessToken
        , Int64 adminUserId)
    {
        ParserListWebCustomers parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_WEB_CUSTOMERS);
            data.Add("searchNameText", searchNameText);
            data.Add("searchCustNoText", searchCustNoText);
            data.Add("searchTcknText", searchTcknText);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Customer.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListWebCustomers>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_WEB_CUSTOMERS).ToString());
        }

        return parser;
    }

    public ParserListCustomers CallListCustomerForExcelService(string searchText
        , int orderSelectionColumn
        , int descAsc, string accessToken
        , Int64 adminUserId)
    {
        ParserListCustomers parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_CUSTOMERS);
            data.Add("searchText", searchText);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Customer.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCustomers>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_CUSTOMERS).ToString());
        }

        return parser;
    }

    public ParserListRoles CallListRoleService(string searchText
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc, string accessToken
        , Int64 adminUserId)
    {
        ParserListRoles parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.SEARCH_ROLE);
            data.Add("searchText", searchText);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "UserRole.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListRoles>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.SEARCH_ROLE).ToString());
        }

        return parser;
    }


    //CallListUserLogDetailService
    public ParserListUserLogDetails CallListUserLogDetailService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListUserLogDetails parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.USER_LOG_DETAIL);

            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);

            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "UserRole.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListUserLogDetails>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.USER_LOG_DETAIL).ToString());
        }

        return parser;
    }

    public ParserListApproveRoles CallListApproveRoleService(string searchText
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount, string accessToken
        , Int64 adminUserId)
    {
        ParserListApproveRoles parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.APPROVE_ROLE);
            data.Add("searchText", searchText);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "UserRole.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListApproveRoles>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.APPROVE_ROLE).ToString());
        }

        return parser;
    }


    public ParserListKiosks CallListKioskService(string searchText
        , int kioskTypeId
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , int instutionId
        , int connectionId
        , int cardReaderId
        , int statusId
        , int regionId
        , int cityId
        , int riskId
        , string accessToken
        , Int64 adminUserId
        , int orderType
    )
    {
        ParserListKiosks parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSKS);
            data.Add("searchText", searchText);
            data.Add("kioskTypeId", kioskTypeId);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);
            data.Add("instutionId", instutionId);
            data.Add("connectionId", connectionId);
            data.Add("cardReaderId", cardReaderId);
            data.Add("statusId", statusId);
            data.Add("regionId", regionId);
            data.Add("cityId", cityId);
            data.Add("riskId", riskId);
            data.Add("kioskId", kioskId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKiosks>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSKS).ToString());
        }

        return parser;
    }

    public ParserListKiosks CallListKioskForExcelService(string searchText
        , int kioskTypeId
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId
        , int orderType
    )
    {
        ParserListKiosks parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSKS);
            data.Add("searchText", searchText);
            data.Add("kioskTypeId", kioskTypeId);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKiosks>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_KIOSKS).ToString());
        }

        return parser;
    }

    public ParserGetKiosk CallGetKioskService(int kioskId, string accessToken
        , Int64 adminUserId)
    {
        ParserGetKiosk parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_KIOSK);
            data.Add("kioskId", kioskId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserGetKiosk>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_KIOSK).ToString());
        }

        return parser;
    }

    //CallGetDispenserService
    public ParserGetKioskCashDispenser CallGetKioskCashDispenserService(int kioskId, string accessToken
        , Int64 adminUserId)
    {
        ParserGetKioskCashDispenser parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_KIOSKS_CASH_DISPENSER);
            data.Add("kioskId", kioskId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserGetKioskCashDispenser>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_KIOSKS_CASH_DISPENSER).ToString());
        }

        return parser;
    }

    public GetKioskCutofMonitoring CallGetCutOffMonitoringService(int cutId, string accessToken
        , Int64 adminUserId)
    {
        GetKioskCutofMonitoring parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_CUT_KIOSK);
            data.Add("cutId", cutId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<GetKioskCutofMonitoring>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_CUT_KIOSK).ToString());
        }

        return parser;
    }


    public ParserGetRepairCard CallGetRepairedCardDetailService(int repairId, string accessToken
        , Int64 adminUserId)
    {
        ParserGetRepairCard parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_REPAIRED_CARD);
            data.Add("repairId", repairId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserGetRepairCard>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_REPAIRED_CARD).ToString());
        }

        return parser;
    }


    public ParserOperation CallUpdateRepairedCardDetailsService(int repairId
        , string anaKredi
        , string yedekKredi
        , Int64 adminUserId
        , string accessToken)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.UPDATE_REPAIRED_CARD);
            data.Add("adminUserId", adminUserId);
            data.Add("anaKredi", anaKredi);
            data.Add("yedekKredi", yedekKredi);
            data.Add("repairId", repairId);

            data.Add("accessToken", accessToken);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.UPDATE_REPAIRED_CARD).ToString());
        }

        return parser;
    }


    //CallGetKioskErrorLogsService
    public ParserGetKioskErrorLogs CallGetKioskErrorLogsService(int errorId, string accessToken
        , Int64 adminUserId)
    {
        ParserGetKioskErrorLogs parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_ERROR_LOGS);
            data.Add("errorId", errorId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserGetKioskErrorLogs>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_ERROR_LOGS).ToString());
        }

        return parser;
    }


    public ParserSaveKiosk CallSaveKioskService(string kioskName, string kioskAddress, string kioskIp, int kioskStatus,
        Int64 updatedUser, string latitude, string longitude, int regionId, string usageFee, int kioskTypeId,
        string accessToken
        , Int64 adminUserId, int kioskCity, string insertionDate, int acceptorTypeId, string explanation,
        int connectionType, int cardReaderType, int institutionId, int riskClass
        , int isCredit, int isMobile)
    {
        ParserSaveKiosk parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.SAVE_KIOSK);
            data.Add("kioskName", kioskName);
            data.Add("kioskAddress", kioskAddress);
            data.Add("kioskIp", kioskIp);
            data.Add("kioskStatus", kioskStatus);
            data.Add("updatedUser", updatedUser);
            data.Add("latitude", latitude);
            data.Add("longitude", longitude);
            data.Add("regionId", regionId);
            data.Add("adminUserId", adminUserId);
            data.Add("usageFee", usageFee);
            data.Add("kioskTypeId", kioskTypeId);
            data.Add("kioskCity", kioskCity);
            data.Add("insertionDate", insertionDate);
            data.Add("acceptorTypeId", acceptorTypeId);
            data.Add("explanation", explanation);

            data.Add("connectionType", connectionType);
            data.Add("cardReaderType", cardReaderType);
            data.Add("institutionId", institutionId);
            data.Add("riskClass", riskClass);
            data.Add("isCredit", isCredit);
            data.Add("isMobile", isMobile);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserSaveKiosk>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.SAVE_KIOSK).ToString());
        }

        return parser;
    }

    public ParserOperation CallSaveCardRepairService(int transactionId, string anaKredi, string yedekKredi,
        string accessToken
        , Int64 adminUserId)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.SAVE_CARD_REPAIR);

            data.Add("adminUserId", adminUserId);
            data.Add("transactionId", transactionId);
            data.Add("anaKredi", anaKredi);
            data.Add("yedekKredi", yedekKredi);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.SAVE_CARD_REPAIR).ToString());
        }

        return parser;
    }


    //CallSaveDispenserMonitoringService
    public ParserOperation CallSaveDispenserMonitoringService(string kioskId,
        string accessToken,
        Int64 adminUserId,
        string firstDenomNumber,
        string secondDenomNumber,
        string thirdDenomNumber,
        string fourthDenomNumber,
        string fifthDenomNumber,
        string sixthDenomNumber,
        string seventhDenomNumber,
        string eighthDenomNumber,
        string ninthDenomNumber,
        string tenthDenomNumber,
        string eleventhDenomNumber,
        string twelfthDenomNumber,
        string firstDenomNumberOld,
        string secondDenomNumberOld,
        string thirdDenomNumberOld,
        string fourthDenomNumberOld,
        string fifthDenomNumberOld,
        string sixthDenomNumberOld,
        string seventhDenomNumberOld,
        string eighthDenomNumberOld,
        string ninthDenomNumberOld,
        string tenthDenomNumberOld,
        string eleventhDenomNumberOld,
        string twelfthDenomNumberOld
    )
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.SAVE_DISPENSER_CASH_COUNT_SERVICE);

            data.Add("kioskId", kioskId);
            data.Add("adminUserId", adminUserId);
            data.Add("firstDenomNumber", firstDenomNumber);
            data.Add("secondDenomNumber", secondDenomNumber);
            data.Add("thirdDenomNumber", thirdDenomNumber);
            data.Add("fourthDenomNumber", fourthDenomNumber);
            data.Add("fifthDenomNumber", fifthDenomNumber);
            data.Add("sixthDenomNumber", sixthDenomNumber);
            data.Add("seventhDenomNumber", seventhDenomNumber);
            data.Add("eighthDenomNumber", eighthDenomNumber);
            data.Add("ninthDenomNumber", ninthDenomNumber);
            data.Add("tenthDenomNumber", tenthDenomNumber);
            data.Add("eleventhDenomNumber", eleventhDenomNumber);
            data.Add("twelfthDenomNumber", twelfthDenomNumber);

            data.Add("firstDenomNumberOld", firstDenomNumberOld);
            data.Add("secondDenomNumberOld", secondDenomNumberOld);
            data.Add("thirdDenomNumberOld", thirdDenomNumberOld);
            data.Add("fourthDenomNumberOld", fourthDenomNumberOld);
            data.Add("fifthDenomNumberOld", fifthDenomNumberOld);
            data.Add("sixthDenomNumberOld", sixthDenomNumberOld);
            data.Add("seventhDenomNumberOld", seventhDenomNumberOld);
            data.Add("eighthDenomNumberOld", eighthDenomNumberOld);
            data.Add("ninthDenomNumberOld", ninthDenomNumberOld);
            data.Add("tenthDenomNumberOld", tenthDenomNumberOld);
            data.Add("eleventhDenomNumberOld", eleventhDenomNumberOld);
            data.Add("twelfthDenomNumberOld", twelfthDenomNumberOld);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.SAVE_DISPENSER_CASH_COUNT_SERVICE).ToString());
        }

        return parser;
    }


    public ParserSaveKiosk CallSaveKioskDeviceService(int kioskId, int deviceId, string portNumber, string accessToken,
        Int64 adminUserId)
    {
        ParserSaveKiosk parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.SAVE_KIOSK_DEVICE);
            data.Add("kioskId", kioskId);
            data.Add("deviceId", deviceId);
            data.Add("portNumber", portNumber);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserSaveKiosk>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.SAVE_KIOSK_DEVICE).ToString());
        }

        return parser;
    }

    public ParserUpdateKiosk CallUpdateKioskService(int kioskId, string kioskName, string kioskAddress, string kioskIp,
        int kioskStatus, Int64 updatedUser, string latitude, string longitude, int regionId, string usageFee,
        int kioskTypeId, string accessToken
        , Int64 adminUserId, int kioskCity, string insertionDate, int acceptorTypeId, string explanation,
        int connectionType, int cardReaderType, int instutionId, int riskClass, int isCredit, int isMobile)
    {
        ParserUpdateKiosk parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.UPDATE_KIOSK);
            data.Add("kioskId", kioskId);
            data.Add("kioskName", kioskName);
            data.Add("kioskAddress", kioskAddress);
            data.Add("kioskIp", kioskIp);
            data.Add("kioskStatus", kioskStatus);
            data.Add("updatedUser", updatedUser);
            data.Add("latitude", latitude);
            data.Add("longitude", longitude);
            data.Add("regionId", regionId);
            data.Add("adminUserId", adminUserId);
            data.Add("usageFee", usageFee);
            data.Add("kioskTypeId", kioskTypeId);
            data.Add("kioskCity", kioskCity);
            data.Add("insertionDate", insertionDate);
            data.Add("acceptorTypeId", acceptorTypeId);
            data.Add("explanation", explanation);

            data.Add("cardReaderType", cardReaderType);
            data.Add("connectionType", connectionType);
            data.Add("instutionId", instutionId);
            data.Add("riskClass", riskClass);
            data.Add("isCredit", isCredit);
            data.Add("isMobile", isMobile);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserUpdateKiosk>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.UPDATE_KIOSK).ToString());
        }

        return parser;
    }

    public ParserUpdateKiosk CallUpdateKioskDeviceService(Int64 itemId, string portNumber, string accessToken,
        Int64 adminUserId)
    {
        ParserUpdateKiosk parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.UPDATE_KIOSK_DEVICE);
            data.Add("portNumber", portNumber);
            data.Add("itemId", itemId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserUpdateKiosk>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.UPDATE_KIOSK_DEVICE).ToString());
        }

        return parser;
    }

    public ParserListKioskCommands CallListKioskCommandService(string searchText
        , DateTime startDate
        , DateTime endDate
        , string kioskId
        , int instutionId
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc, string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListKioskCommands parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSK_COMMANDS);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskCommands>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_COMMANDS).ToString());
        }

        return parser;
    }


    //CallGetKioskCommandService
    public ParserGetKioskCommand CallGetKioskCommandService(int commandId, string accessToken
        , Int64 adminUserId)
    {
        ParserGetKioskCommand parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_KIOSK_COMMANDS);
            data.Add("commandId", commandId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserGetKioskCommand>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_KIOSK_COMMANDS).ToString());
        }

        return parser;
    }


    public ParserSaveKioskCommand CallSaveKioskCommandService(int kioskId, int instutionId, int createdUserId,
        int commandTypeId, string commandString, string accessToken
        , Int64 adminUserId, int commandReasonId, string explanation)
    {
        ParserSaveKioskCommand parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.SAVE_KIOSK_COMMAND);
            data.Add("kioskId", kioskId);
            data.Add("createdUserId", createdUserId);
            data.Add("commandTypeId", commandTypeId);
            data.Add("commandString", commandString);
            data.Add("adminUserId", adminUserId);
            data.Add("commandReasonId", commandReasonId);
            data.Add("explanation", explanation);
            data.Add("instutionId", instutionId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserSaveKioskCommand>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex, "Ksk100");
        }

        return parser;
    }

    public ParserOperation CallUpdateKioskCommandService(int commandId, int createdUserId, int commandTypeId,
        string commandString, string accessToken
        , Int64 adminUserId, int commandReasonId, string explanation)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.UPDATE_KIOSK_COMMAND);
            data.Add("commandId", commandId);
            //data.Add("kioskId", kioskId);
            data.Add("createdUserId", createdUserId);
            data.Add("commandTypeId", commandTypeId);
            data.Add("commandString", commandString);
            data.Add("adminUserId", adminUserId);
            data.Add("commandReasonId", commandReasonId);
            data.Add("explanation", explanation);
            //data.Add("instutionId", instutionId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.UPDATE_KIOSK_COMMAND).ToString());
        }

        return parser;
    }


    public ParserListKioskReport CallListKioskReportsService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , int instutionId
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListKioskReport parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSK_REPORTS);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskReport>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_REPORT).ToString());
        }

        return parser;
    }


    public ParserListCodeReport CallListCodeDetails(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string kioskId
        , int instutionId
        , int paymentType
        , string accessToken
        , Int64 adminUserId
        , int orderType
        , int processType
        , int bankType)
    {
        ParserListCodeReport parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_CODE_DETAILS);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);
            data.Add("paymentType", paymentType);
            data.Add("processType", processType);
            data.Add("bankType", bankType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCodeReport>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_REPORT).ToString());
        }

        return parser;
    }

    public ParserListKioskName CallGetKioskNamesService(string accessToken
        , Int64 adminUserId)
    {
        ParserListKioskName parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSK_NAMES);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskName>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_NAMES).ToString());
        }

        return parser;
    }

    public CancelTransactionResult CancelTransaction(string transactionId, string accessToken, Int64 adminUserId)
    {
        CancelTransactionResult parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("adminUserId", adminUserId);
            data.Add("functionCode", (int) FC.GET_CANCEL_PARAMETERS);
            data.Add("transactionId", transactionId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<CancelTransactionResult>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_CANCEL_PARAMETERS).ToString());
        }

        return parser;
    }

    public ParserPhoneNumber CallPhoneNumbers(int operationType, string transactionId, string accessToken,
        Int64 adminUserId)
    {
        ParserPhoneNumber parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("adminUserId", adminUserId);
            data.Add("functionCode", (int) FC.GET_PHONE_NUMBER);
            data.Add("transactionId", transactionId);
            data.Add("operationType", operationType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserPhoneNumber>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_PHONE_NUMBER).ToString());
        }

        return parser;
    }


    public ParserOperation CallManuelEmptyKioskService(int kioskId
        , string accessToken
        , Int64 adminUserId)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.MANUEL_KIOSK_EMPTY);
            data.Add("kioskId", kioskId);
            data.Add("adminUserId", adminUserId);
            data.Add("expertUserId", adminUserId);

            //string URL = Utility.GetConfigStr("AdminServiceAddress", "localhost:80") + "User.aspx";

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.MANUEL_KIOSK_EMPTY).ToString());
        }

        return parser;
    }


    public ParserListInstutionName CallGetInstutionNamesService(string accessToken
        , Int64 adminUserId)
    {
        ParserListInstutionName parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_FOUNDATION_NAMES);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListInstutionName>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_FOUNDATION_NAMES).ToString());
        }

        return parser;
    }

    public ParserListCompleteStatus CallGetCompleteStatusService(string accessToken, Int64 adminUserId)
    {
        ParserListCompleteStatus parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_COMPLETESTATUS_NAMES);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCompleteStatus>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_FOUNDATION_NAMES).ToString());
        }

        return parser;
    }

    //CallGetParserListMuhasebeStatusService
    public ParserListMuhasebeStatus CallGetParserListMuhasebeStatusService(string accessToken
        , Int64 adminUserId)
    {
        ParserListMuhasebeStatus parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_MUHASEBE_STATUS);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListMuhasebeStatus>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_MUHASEBE_STATUS).ToString());
        }

        return parser;
    }


    //CallGetPaymentTypeService
    public ParserListPaymentType CallGetPaymentTypeService(string accessToken
        , Int64 adminUserId)
    {
        ParserListPaymentType parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_PAYMENT_TYPE);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListPaymentType>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_PAYMENT_TYPE).ToString());
        }

        return parser;
    }


    public ParserListBankName CallGetBankNamesService(string accessToken, Int64 adminUserId)
    {
        ParserListBankName parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int)FC.LIST_BANK_NAMES);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListBankName>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex, ((int)FC.LIST_PAYMENT_TYPE).ToString());
        }

        return parser;
    }

    //CallGetCardRepairTypeService
    public ParserListCardRepairType CallGetCardRepairStatusService(string accessToken
        , Int64 adminUserId)
    {
        ParserListCardRepairType parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_CARD_REPAIR_STATUS);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCardRepairType>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_CARD_REPAIR_STATUS).ToString());
        }

        return parser;
    }


    //CallGetCardRepairTypeService
    public ParserListReferenceStatus CallGetReferenceStatusService(string accessToken
        , Int64 adminUserId)
    {
        ParserListReferenceStatus parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_REFERENCE_STATUS);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListReferenceStatus>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_REFERENCE_STATUS).ToString());
        }

        return parser;
    }


    //CallGetIsCardDeletedService
    public ParserListIsCardDeleted CallGetIsCardDeletedService(string accessToken
        , Int64 adminUserId)
    {
        ParserListIsCardDeleted parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_IS_CARD_DELETED);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListIsCardDeleted>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_IS_CARD_DELETED).ToString());
        }

        return parser;
    }


    public ParserListRiskClass CallGetRiskClassService(string accessToken
        , Int64 adminUserId)
    {
        ParserListRiskClass parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_RISK_CLASS);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListRiskClass>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_FOUNDATION_NAMES).ToString());
        }

        return parser;
    }


    public ParserListStatus CallGetStatusService(string accessToken
        , Int64 adminUserId)
    {
        ParserListStatus parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_STATUS);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListStatus>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_STATUS).ToString());
        }

        return parser;
    }

    public ParserListInstutionName CallGetInstutionNamesForMutabakat(string accessToken
        , Int64 adminUserId)
    {
        ParserListInstutionName parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_FOUNDATION_NAMES_MUTABAKAT);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListInstutionName>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_FOUNDATION_NAMES).ToString());
        }

        return parser;
    }

    public ParserListProcessType CallGetProcessTypeFromLogs(string accessToken
        , Int64 adminUserId)
    {
        ParserListProcessType parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_PROCESS_TYPE);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListProcessType>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_FOUNDATION_NAMES).ToString());
        }

        return parser;
    }

    public ParserListTxnStatusName CallGetTxnStatusNamesService(string accessToken
        , Int64 adminUserId)
    {
        ParserListTxnStatusName parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_TXN_STATUS_NAMES);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListTxnStatusName>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_FOUNDATION_NAMES).ToString());
        }

        return parser;
    }

    public ParserListInstutionName CallGetDeviceNamesService(string accessToken
        , Int64 adminUserId)
    {
        ParserListInstutionName parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_DEVICE_NAMES);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListInstutionName>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_DEVICE_NAMES).ToString());
        }

        return parser;
    }


    public ParserListInstutionOperationName CallGetOperationTypeNamesService(string accessToken
        , Int64 adminUserId)
    {
        ParserListInstutionOperationName parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_INSTUTION_OPERATION_NAMES);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListInstutionOperationName>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_INSTUTION_OPERATION_NAMES).ToString());
        }

        return parser;
    }

    public ParserListCardTypeName CallGetCardTypeNamesService(string accessToken
        , Int64 adminUserId)
    {
        ParserListCardTypeName parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_CARD_TYPE_NAMES);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCardTypeName>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_CARD_TYPE_NAMES).ToString());
        }

        return parser;
    }


    public ParserListTransactionConditionName CallGetTransactionConditionNamesService(string accessToken
        , Int64 adminUserId)
    {
        ParserListTransactionConditionName parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_TXN_CONDITION_NAMES);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListTransactionConditionName>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_TXN_CONDITION_NAMES).ToString());
        }

        return parser;
    }


    public ParserListKioskTypeName CallGetKisokTypeNamesService(string accessToken
        , Int64 adminUserId)
    {
        ParserListKioskTypeName parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSK_TYPE_NAMES);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskTypeName>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_FOUNDATION_NAMES).ToString());
        }

        return parser;
    }

    public ParserListCommandTypeName CallCommandTypeNamesService(string accessToken
        , Int64 adminUserId)
    {
        ParserListCommandTypeName parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSK_COMMAND_TYPE_NAMES);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCommandTypeName>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_COMMAND_TYPE_NAMES).ToString());
        }

        return parser;
    }

    public ParserListCommandReasonName CallCommandReasonNamesService(string accessToken, Int64 adminUserId)
    {
        ParserListCommandReasonName parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSK_COMMAND_REASON_NAMES);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListCommandReasonName>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_COMMAND_REASON_NAMES).ToString());
        }

        return parser;
    }

    public ParserGetRole CallGetRoleService(int roleId, string accessToken
        , Int64 adminUserId)
    {
        ParserGetRole parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_ROLE);
            data.Add("roleId", roleId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "UserRole.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserGetRole>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_ROLE).ToString());
        }

        return parser;
    }

    public ParserApproveGetRole CallGetApproveRoleService(int roleId, string accessToken
        , Int64 adminUserId)
    {
        ParserApproveGetRole parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.APPROVE_GET_ROLE);
            data.Add("roleId", roleId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "UserRole.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserApproveGetRole>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.APPROVE_GET_ROLE).ToString());
        }

        return parser;
    }

    public ParserSaveRole CallSaveRoleService(string name, string description, int creationUserId, string accessToken
        , Int64 adminUserId)
    {
        ParserSaveRole parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.ADD_ROLE);
            data.Add("name", name);
            data.Add("description", description);
            data.Add("creationUserId", creationUserId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "UserRole.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserSaveRole>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.ADD_ROLE).ToString());
        }

        return parser;
    }

    public ParserSaveRoleSubMenus CallSaveRoleSubMenusService(JArray jsonArray, int createdserId, string accessToken
        , Int64 adminUserId)
    {
        ParserSaveRoleSubMenus parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.ADD_ROLE_SUBMENU);
            data.Add("createdserId", createdserId);
            data.Add("RoleSubMenus", jsonArray);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "UserRole.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserSaveRoleSubMenus>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.ADD_ROLE_SUBMENU).ToString());
        }

        return parser;
    }

    public ParserUpdateRole CallUpdateRoleService(int id, string name, string description, Int64 updatedUserId,
        string accessToken
        , Int64 adminUserId)
    {
        ParserUpdateRole parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.UPDATE_ROLE);
            data.Add("id", id);
            data.Add("name", name);
            data.Add("description", description);
            data.Add("updatedUserId", updatedUserId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "UserRole.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserUpdateRole>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.UPDATE_ROLE).ToString());
        }

        return parser;
    }


    public ParserOperation CallDeleteOperationService(int userId, string storedProcedure, int itemId, string accessToken
        , Int64 adminUserId)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.DELETE_OPERATION);
            data.Add("userId", userId);
            data.Add("storedProcedure", storedProcedure);
            data.Add("itemId", itemId);
            data.Add("adminUserId", adminUserId);

            //string URL = Utility.GetConfigStr("AdminServiceAddress", "localhost:80") + "User.aspx";

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Operation.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.DELETE_OPERATION).ToString());
        }

        return parser;
    }

    public ParserOperation CallOperationInformService(int userId, string storedProcedure, int itemId, int type,
        string accessToken
        , Int64 adminUserId)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.OPERATION_INFORM);
            data.Add("userId", userId);
            data.Add("storedProcedure", storedProcedure);
            data.Add("itemId", itemId);
            data.Add("type", type);
            data.Add("adminUserId", adminUserId);

            //string URL = Utility.GetConfigStr("AdminServiceAddress", "localhost:80") + "User.aspx";

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Operation.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.DELETE_OPERATION).ToString());
        }

        return parser;
    }

    public ParserOperation CallMoneyCodeDeleteService(int userId
        , string detailText
        , string vitelTicketNo
        , int itemId
        , string accessToken
        , Int64 adminUserId)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.DELETE_MONEY_CODE);
            data.Add("userId", userId);
            data.Add("storedProcedure", detailText);
            data.Add("itemId", itemId);
            data.Add("vitelTicketNo", vitelTicketNo);
            data.Add("adminUserId", adminUserId);

            //string URL = Utility.GetConfigStr("AdminServiceAddress", "localhost:80") + "User.aspx";

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Operation.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.DELETE_MONEY_CODE).ToString());
        }

        return parser;
    }

    public ParserOperation CallMakeMutabakat(string faturaAmount
        , string faturaCount
        , string kartDolumAmount
        , string kartDolumCount
        , string totalCount
        , string totalAmount
        , string instutionCount
        , string instutionAmount
        , string date
        , string kioskId
        , string instutionId
        , string whichSide
        , string accessToken
        , Int64 adminUserId
        , string cancelAmount
        , string cancelCount)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.COMPLETE_KIOSK_INSTUTION_MUTABAKAT);
            data.Add("userId", adminUserId);
            data.Add("mutabakatDate", date);
            data.Add("whichSide", whichSide);
            data.Add("kioskId", kioskId);
            data.Add("instutionId", instutionId);
            data.Add("faturaCount", faturaCount);
            data.Add("faturaAmount", faturaAmount);
            data.Add("kartDolumCount", kartDolumCount);
            data.Add("kartDolumAmount", kartDolumAmount);
            data.Add("totalCount", totalCount);
            data.Add("totalAmount", totalAmount);
            data.Add("instutionCount", instutionCount);
            data.Add("instutionAmount", instutionAmount);
            data.Add("adminUserId", adminUserId);
            data.Add("cancelAmount", cancelAmount);
            data.Add("cancelCount", cancelCount);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.COMPLETE_KIOSK_INSTUTION_MUTABAKAT).ToString());
        }

        return parser;
    }


    public ParserOperation CallChangeUserPasswordAdminService(Int64 admUserId, Int64 updatedUser, string accessToken
        , Int64 adminUserId)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.CHANGE_PASSWORD_ADMIN);
            data.Add("admUserId", admUserId);
            data.Add("updatedUser", updatedUser);
            data.Add("adminUserId", adminUserId);

            //string URL = Utility.GetConfigStr("AdminServiceAddress", "localhost:80") + "User.aspx";
            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "User.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.CHANGE_PASSWORD_ADMIN).ToString());
        }

        return parser;
    }

    public ParserOperation CallChangeUserPasswordService(Int64 admUserId, string newPassword, string oldPasswords,
        Int64 updatedUser, string accessToken
        , Int64 adminUserId)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.CHANGE_PASSWORD);
            data.Add("admUserId", admUserId);
            data.Add("newPassword", newPassword);
            data.Add("oldPasswords", oldPasswords);
            data.Add("updatedUser", updatedUser);
            data.Add("adminUserId", adminUserId);

            //string URL = Utility.GetConfigStr("AdminServiceAddress", "localhost:80") + "User.aspx";
            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "User.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.CHANGE_PASSWORD).ToString());
        }

        return parser;
    }

    public ParserListRoleName CallGetRoleNamesService(string accessToken
        , Int64 adminUserId)
    {
        ParserListRoleName parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_ROLE_NAMES);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "User.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListRoleName>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_ROLE_NAMES).ToString());
        }

        return parser;
    }

    public ParserListStatusName CallGetKioskStatusNamesService(string accessToken
        , Int64 adminUserId)
    {
        ParserListStatusName parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_KIOSK_STATUS);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListStatusName>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_KIOSK_STATUS).ToString());
        }

        return parser;
    }

    public ParserListStatusName CallGetStatusNamesService(string accessToken
        , Int64 adminUserId)
    {
        ParserListStatusName parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_STATUS_NAMES);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "User.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListStatusName>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_STATUS_NAMES).ToString());
        }

        return parser;
    }

    public ParserGetUser CallGetUserService(int userId, string accessToken
        , Int64 adminUserId)
    {
        ParserGetUser parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_USER);
            data.Add("userId", userId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "User.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserGetUser>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_USER).ToString());
        }

        return parser;
    }

    public ParserGetUser CallGetApproveUserService(int userId, string accessToken
        , Int64 adminUserId)
    {
        ParserGetUser parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.APPROVE_GET_USER);
            data.Add("userId", userId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "User.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserGetUser>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.APPROVE_GET_USER).ToString());
        }

        return parser;
    }

    public ParserOperation CallSaveUserService(string name,
        string userName,
        string email,
        string cellphone,
        string telephone,
        int roleId,
        int status,
        string password,
        int creationUserId, string accessToken
        , Int64 adminUserId)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.ADD_USER);
            data.Add("name", name);
            data.Add("userName", userName);
            data.Add("email", email);
            data.Add("cellphone", cellphone);
            data.Add("telephone", telephone);
            data.Add("roleId", roleId);
            data.Add("password", password);
            data.Add("status", status);
            data.Add("updatedUser", creationUserId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "User.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.ADD_USER).ToString());
        }

        return parser;
    }

    public ParserOperation CallSaveBlackListService(string name,
        string tckn,
        string birthYear,
        string description,
        int creationUserId, string accessToken
        , Int64 adminUserId)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.ADD_BLACK_LIST);
            data.Add("name", name);
            data.Add("tckn", tckn);
            data.Add("birthYear", birthYear);
            data.Add("description", description);
            data.Add("updatedUser", creationUserId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "User.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.ADD_BLACK_LIST).ToString());
        }

        return parser;
    }

    public ParserOperation CallUpdateUserService(int userId,
        string name,
        string userName,
        string email,
        string cellphone,
        string telephone,
        int roleId,
        int status,
        int creationUserId, string accessToken
        , Int64 adminUserId)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.UPDATE_USER);
            data.Add("name", name);
            data.Add("userName", userName);
            data.Add("email", email);
            data.Add("cellphone", cellphone);
            data.Add("telephone", telephone);
            data.Add("roleId", roleId);
            data.Add("id", userId);
            data.Add("status", status);
            data.Add("updatedUser", creationUserId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "User.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.UPDATE_USER).ToString());
        }

        return parser;
    }

    public ParserListUsers CallListUserService(string searchText
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc, string accessToken
        , Int64 adminUserId)
    {
        ParserListUsers parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.SEARCH_USER);
            data.Add("searchText", searchText);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);

            //string URL = Utility.GetConfigStr("AdminServiceAddress", "localhost:80") + "User.aspx";
            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "User.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListUsers>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.SEARCH_USER).ToString());
        }

        return parser;
    }

    public ParserListBlackLists CallListBlackListService(string searchText
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc, string accessToken
        , Int64 adminUserId)
    {
        ParserListBlackLists parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.SEARCH_BLACK_LIST);
            data.Add("searchText", searchText);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);

            //string URL = Utility.GetConfigStr("AdminServiceAddress", "localhost:80") + "User.aspx";
            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "User.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListBlackLists>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.SEARCH_BLACK_LIST).ToString());
        }

        return parser;
    }

    public ParserListApproveUsers CallListApproveUserService(string searchText
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount, string accessToken
        , Int64 adminUserId)
    {
        ParserListApproveUsers parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.APPROVE_SEARCH_USER);
            data.Add("searchText", searchText);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("adminUserId", adminUserId);

            //string URL = Utility.GetConfigStr("AdminServiceAddress", "localhost:80") + "User.aspx";
            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "User.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListApproveUsers>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.APPROVE_SEARCH_USER).ToString());
        }

        return parser;
    }

    public ParserLogin CallLoginService(string userName,
        string password,
        int minute,
        int tryLimit,
        int expireDayCount)
    {
        ParserLogin parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.GET_LOGIN_USER);
            data.Add("userName", userName);
            data.Add("password", password);
            data.Add("minute", minute);
            data.Add("tryLimit", tryLimit);
            data.Add("expireDayCount", expireDayCount);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Login.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserLogin>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.GET_LOGIN_USER).ToString());
        }

        return parser;
    }

    public ParserOperation CallControlAccessTokenService(string accessToken, Int64 adminUserId)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("adminUserId", adminUserId);
            data.Add("functionCode", (int) FC.NOKTALAR);
            data.Add("IslemTipi", 1);
            data.Add("Kod", "9001");
            data.Add("Adı", "9001-001");
            data.Add("GrupKod", "KIOSK");
            data.Add("IlKod", "6");
            data.Add("BirimKod", "3");
            data.Add("Adres", "sdsdsds");
            data.Add("Telefon", "5336295270");
            data.Add("Durum", true);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Desmer.aspx";

            string responseText = Utility.ExecuteHttpRequest(URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex, "ControlAccessToken");
        }

        return parser;
    }

    public ParserListKioskCondition CallListKioskConditionService(string kioskId
        , string searchText
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId
        , int kioskType
        , int orderType)
    {
        ParserListKioskCondition parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSK_CONDITION);
            data.Add("searchText", searchText);
            data.Add("kioskId", kioskId);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);
            data.Add("kioskType", kioskType);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskCondition>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_CONDITION).ToString());
        }

        return parser;
    }

    public ParserListKioskLocalLogDetail CallListKioskLocalLogDetailService(int localLogId
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListKioskLocalLogDetail parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSK_LOCAL_LOG_DETAIL);
            data.Add("adminUserId", adminUserId);
            data.Add("localLogId", localLogId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Transaction.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskLocalLogDetail>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_CONDITION).ToString());
        }

        return parser;
    }

    //CallUpdateDispenserCashCountService
    public ParserOperation CallUpdateDispenserCashCountService(string kioskId
        , string totalAmountTb
        , string totalValue
        , Int64 adminUserId
        , string accessToken)
    {
        ParserOperation parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.UPDATE_KIOSK_CONDITION_DISPENSER_CASH_COUNT);
            data.Add("adminUserId", adminUserId);

            data.Add("kioskId", kioskId);
            data.Add("dispenser", totalAmountTb);
            data.Add("dispenserAmount", totalValue);

            data.Add("accessToken", accessToken);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()),
                accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserOperation>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.UPDATE_KIOSK_CONDITION_DISPENSER_CASH_COUNT).ToString());
        }

        return parser;
    }


    public ParserListKioskMonitoring CallListKioskMonitoringService(string kioskId
        , string searchText
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListKioskMonitoring parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSK_MONITORING);
            data.Add("searchText", searchText);
            data.Add("kioskId", kioskId);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskMonitoring>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_MONITORING).ToString());
        }

        return parser;
    }

    //public ParserListKioskCondition CallListKioskConditionExcelService(string kioskId
    //                                                            , string searchText
    //                                                            , int numberOfItemsPerPage
    //                                                            , int currentPageNum
    //                                                            , int recordCount
    //                                                            , int pageCount
    //                                                            , int orderSelectionColumn
    //                                                            , int descAsc
    //                                                            , string accessToken
    //                                                            , Int64 adminUserId
    //                                                            , int kioskType)
    //{

    //    ParserListKioskCondition parser = null;
    //    try
    //    {
    //        HttpStatusCode statusCode = HttpStatusCode.Accepted;

    //        JObject data = new JObject();
    //        data.Add("apiKey", "TEST");
    //        data.Add("apiPass", "ABCD1234");
    //        data.Add("functionCode", (int)FC.EXCEL_LIST_KIOSK_CONDITION);
    //        data.Add("searchText", searchText);
    //        data.Add("kioskId", kioskId);
    //        data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
    //        data.Add("currentPageNum", currentPageNum);
    //        data.Add("recordCount", recordCount);
    //        data.Add("pageCount", pageCount);
    //        data.Add("orderSelectionColumn", orderSelectionColumn);
    //        data.Add("descAsc", descAsc);
    //        data.Add("adminUserId", adminUserId);
    //        data.Add("kioskType", kioskType);

    //        string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

    //        string responseText = Utility.ExecuteHttpRequest
    //            (URL,
    //            "POST",
    //            System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
    //            ref statusCode);

    //        if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
    //        {

    //            parser = JsonConvert.DeserializeObject<ParserListKioskCondition>(responseText);
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex, ((int)FC.EXCEL_LIST_KIOSK_CONDITION).ToString());
    //    }

    //    return parser;
    //}

    public ParserListKioskCashEmpty CallListKioskCashEmptyService(string searchText
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId
        //, int kioskType
        , int instutionId
        , string kioskId
        , int orderType)
    {
        ParserListKioskCashEmpty parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSK_CASH_EMPTY);
            data.Add("searchText", searchText);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);
            //data.Add("kioskType", kioskType);
            data.Add("kioskId", kioskId);
            data.Add("orderType", orderType);
            data.Add("instutionId", instutionId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskCashEmpty>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_CASH_EMPTY).ToString());
        }

        return parser;
    }

    public ParserListFullCashBox CallListFullCashBoxService(string kioskId
        , DateTime startDate
        , DateTime endDate
        , int instutionId
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListFullCashBox parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSK_FILL_CASHBOX);
            data.Add("kioskId", kioskId);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);
            data.Add("instutionId", instutionId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListFullCashBox>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_FILL_CASHBOX).ToString());
        }

        return parser;
    }


    public ParserListPOSEOD CallListPOSEODService(string kioskId
        , DateTime startDate
        , DateTime endDate
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId
        , int orderType)
    {
        ParserListPOSEOD parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_POSEOD);
            data.Add("kioskId", kioskId);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);
            data.Add("orderType", orderType);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Account.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListPOSEOD>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_POSEOD).ToString());
        }

        return parser;
    }

    public ParserListFullCashBox CallListFullCashBoxForExcelService(string kioskId
        , DateTime startDate
        , DateTime endDate
        , int orderSelectionColumn
        , int descAsc
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListFullCashBox parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_KIOSK_FILL_CASHBOX);
            data.Add("kioskId", kioskId);
            data.Add("startDate", startDate);
            data.Add("endDate", endDate);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListFullCashBox>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_KIOSK_FILL_CASHBOX).ToString());
        }

        return parser;
    }

    //public ParserListKioskCashEmpty CallListKioskCashEmptyForExcelService(string searchText
    //                                                                    , DateTime startDate
    //                                                                    , DateTime endDate
    //                                                                    , int orderSelectionColumn
    //                                                                    , int descAsc, string accessToken
    //                                                                    , Int64 adminUserId
    //                                                                    , int kioskType)
    //{

    //    ParserListKioskCashEmpty parser = null;
    //    try
    //    {
    //        HttpStatusCode statusCode = HttpStatusCode.Accepted;

    //        JObject data = new JObject();
    //        data.Add("apiKey", "TEST");
    //        data.Add("apiPass", "ABCD1234");
    //        data.Add("functionCode", (int)FC.EXCEL_LIST_KIOSK_CASH_EMPTY);
    //        data.Add("searchText", searchText);
    //        data.Add("endDate", endDate);
    //        data.Add("startDate", startDate);
    //        data.Add("orderSelectionColumn", orderSelectionColumn);
    //        data.Add("descAsc", descAsc);
    //        data.Add("adminUserId", adminUserId);
    //        data.Add("kioskType", kioskType);


    //        string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

    //        string responseText = Utility.ExecuteHttpRequest
    //            (URL,
    //            "POST",
    //            System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
    //            ref statusCode);

    //        if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
    //        {

    //            parser = JsonConvert.DeserializeObject<ParserListKioskCashEmpty>(responseText);
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex, ((int)FC.EXCEL_LIST_KIOSK_CASH_EMPTY).ToString());
    //    }

    //    return parser;
    //}

    public ParserListKioskConditionDetail CallListKioskConditionDetailService(int kioskId
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , string accessToken
        , Int64 adminUserId)
    {
        ParserListKioskConditionDetail parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.LIST_KIOSK_CONDITION_DETAIL);
            data.Add("kioskId", kioskId);
            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskConditionDetail>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.LIST_KIOSK_CONDITION_DETAIL).ToString());
        }

        return parser;
    }

    public ParserListKioskConditionDetail CallListKioskConditionDetailForExcelService(int kioskId, string accessToken
        , Int64 adminUserId)
    {
        ParserListKioskConditionDetail parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_KIOSK_CONDITION_DETAIL);
            data.Add("kioskId", kioskId);
            data.Add("adminUserId", adminUserId);

            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKioskConditionDetail>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_KIOSK_CONDITION_DETAIL).ToString());
        }

        return parser;
    }


    public ParserListKiosks CallListSettingService(string txnLimitofReceiptWarning,
        string txnLimitofCashBoxWarning,
        string txnActiveofReceiptWarning,
        string txnProduceMoneyCode,
        string txnLimitofMoneyCount,
        string LimitofBaşkentGazMaximumWarning,
        int ActiveofReceiptWarningBox,
        int ActiveofCashBoxWarningBox,
        int ActiveYellowAlarmBox,
        int ActiveRedAlarmBox,
        int numberOfItemsPerPage,
        int currentPageNum,
        int recordCount,
        int pageCount,
        int orderSelectionColumn,
        int descAsc,
        string accessToken,
        Int64 adminUserId)
    {
        ParserListKiosks parser = null;
        try
        {
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            JObject data = new JObject();
            data.Add("apiKey", "TEST");
            data.Add("apiPass", "ABCD1234");
            data.Add("functionCode", (int) FC.EXCEL_LIST_SETTING);
            //data.Add("kioskId", kioskId);

            data.Add("numberOfItemsPerPage", numberOfItemsPerPage);
            data.Add("currentPageNum", currentPageNum);
            data.Add("recordCount", recordCount);
            data.Add("pageCount", pageCount);
            data.Add("orderSelectionColumn", orderSelectionColumn);
            data.Add("descAsc", descAsc);
            data.Add("adminUserId", adminUserId);


            string URL = Utility.GetConfigValue("AdminWebServicesUrl") + "Kiosk.aspx";

            string responseText = Utility.ExecuteHttpRequest
            (URL,
                "POST",
                System.Text.Encoding.UTF8.GetBytes(data.ToString()), accessToken,
                ref statusCode);

            if (statusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(responseText))
            {
                parser = JsonConvert.DeserializeObject<ParserListKiosks>(responseText);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigStr("LogFilePath", "C:\\PRCN\\LOG\\"), ex,
                ((int) FC.EXCEL_LIST_KIOSK_CONDITION_DETAIL).ToString());
        }

        return parser;
    }
}