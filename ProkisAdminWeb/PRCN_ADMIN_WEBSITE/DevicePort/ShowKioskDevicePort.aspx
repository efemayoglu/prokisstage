﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowKioskDevicePort.aspx.cs" Inherits="DevicePort_ShowKioskDevicePort" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />
    <input type="hidden" id="hiddenId" runat="server" name="hiddenId" value="0" />
    <script language="javascript" type="text/javascript">

        function callOwn() {
            alert("User has been saved successfully!");
            window.close();
            window.parent.location = "../Kiosk/ListKiosk.aspx";
        }
        function checkForm() {
            var status = true;
            var messageText = "";

            if (document.getElementById('nameField').value == "") {
                status = false;
                messageText = "Kiosk Adını giriniz.";
            }
            else if (document.getElementById('addressField').value == "") {
                status = false;
                messageText = "Kiosk Adresini giriniz.";
            }
            else if (document.getElementById('ipField').value == "") {
                status = false;
                messageText = "Kiosk IP adresini giriniz.";
            }
            else if (ValidateIPaddress(document.getElementById('ipField').value) == false) {
                status = false;
                messageText = "Geçersiz IP formatı.";
            }
            else if (ValidateDecimalNumber(document.getElementById('usageFee').value) == false) {
                status = false;
                messageText = "Kiosk Ücret formatı hatalı !";
            }
            else {
                messageText = "";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }

        function validateIP(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]|\./;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function validateNo(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function ValidateIPaddress(inputText) {
            var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
            if (inputText.match(ipformat)) {
                return true;
            }
            else {
                return false;
            }
        }

        function validateDecimal(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]|\.|\,/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function ValidateDecimalNumber(inputText) {
            if (document.getElementById('amountTextBox').value == '')
                return true;
            var ipformat = /^[0-9]+((\.[0-9]{1,2})|(\,[0-9]{1,2}))?$/;
            if (inputText.match(ipformat)) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <base target="_self" />
    <style type="text/css">
        .auto-style1 {
            height: 20px;
        }

        .auto-style14 {
            font-family: Tahoma;
            font-size: 12px;
            font-weight: bold;
            width: 190px;
            height: 20px;
            border-radius: 5px;
            border: 1px solid #2372BE;
            padding-bottom: 5px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel7" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <div align="left" id="articleDetailsTab" class="windowTitle_container_autox30">
                    Kiosk Cihazı Port Ayarı<hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>
                <table border="0" cellpadding="2" cellspacing="0" id="Table1" runat="server">
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Kiosk Adı:
                        </td>
                        <td align="left" class="auto-style1">
                            <asp:ListBox ID="kioskBox" CssClass="inputLine_285x20" runat="server" SelectionMode="Single"
                                DataTextField="Name" DataValueField="Id" Rows="1"></asp:ListBox>
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Cihaz Adı:
                        </td>
                        <td align="left" class="auto-style1">
                            <asp:ListBox ID="deviceBox" CssClass="inputLine_285x20" runat="server" SelectionMode="Single"
                                DataTextField="Name" DataValueField="Id" Rows="1"></asp:ListBox>
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td align="left" class="auto-style14">Port Numarası:
                        </td>
                        <td align="left" class="auto-style1">
                            <asp:TextBox ID="portNumber" MaxLength="19" runat="server" CssClass="inputLine_185x20" ></asp:TextBox>
                            &nbsp;</td>
                    </tr>             
                </table>
                <br />
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Kaydet"></asp:Button>

                                <asp:Button ID="cancelButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClientClick="parent.hideModalPopup2();" Text="Vazgeç"></asp:Button>

                            </td>
                        </tr>
                    </table>
                </td>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
