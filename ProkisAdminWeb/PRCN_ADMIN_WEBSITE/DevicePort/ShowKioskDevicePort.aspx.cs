﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DevicePort_ShowKioskDevicePort : System.Web.UI.Page
{
    public Int64 itemId;
    private ParserLogin LoginDatas;
    public string kioskName = "";
    public string deviceName = "";
    public string oldPortNumber = "";
    private ParserListInstutionName deviceNames;
    private ParserListKioskName kioskNames;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../DevicePort/ListKioskDevicePort.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1" || this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1")
            {

            }
            else
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.itemId = Convert.ToInt64(Request.QueryString["itemID"]);

        if (this.itemId != 0)
        {
            this.kioskName = Request.QueryString["kioskName"].ToString();
            this.deviceName = Request.QueryString["deviceName"].ToString();
            this.oldPortNumber = Request.QueryString["oldPortNumber"].ToString();
        }
        if (this.itemId == 0 && ViewState["itemID"] != null)
        {
            this.itemId = Convert.ToInt64(ViewState["itemID"].ToString());
        }

        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }

    private void BindCtrls()
    {
        CallWebServices callWebServ = new CallWebServices();

        this.kioskNames= callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

        if (kioskNames != null)
        {
            this.kioskBox.DataSource = this.kioskNames.KioskNames;
            this.kioskBox.DataBind();
        }

        this.deviceNames = callWebServ.CallGetDeviceNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

        if (deviceNames != null)
        {
            this.deviceBox.DataSource = this.deviceNames.InstutionNames;
            this.deviceBox.DataBind();
        }

        this.SetControls();
    }

    private void SetControls()
    {
        try
        {
            if (this.itemId != 0)
            {
                this.kioskBox.Text = this.kioskName;
                this.kioskBox.Enabled = false;
                this.kioskBox.Attributes.Add("disabled", "disabled");

                this.deviceBox.Text = this.deviceName;
                this.deviceBox.Enabled = false;
                this.deviceBox.Attributes.Add("disabled", "disabled");

                this.portNumber.Text =  this.oldPortNumber;
            }
            else
            {
                ListItem itemKiosk = new ListItem();
                itemKiosk.Text = "Kiosk Seçiniz";
                itemKiosk.Value = "0";
                this.kioskBox.Items.Add(itemKiosk);
                this.kioskBox.SelectedValue = "0";

                ListItem itemDevice = new ListItem();
                itemDevice.Text = "Cihaz Seçiniz";
                itemDevice.Value = "0";
                this.deviceBox.Items.Add(itemDevice);
                this.deviceBox.SelectedValue = "0";
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowKioskDeviceSC");
        }
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        SaveDevice();
    }

    private void SaveDevice()
    {
        try
        {
            int updateStatus = 0;
            CallWebServices callWebServ = new CallWebServices();

            if (this.itemId == 0)
            {
                callWebServ.CallSaveKioskDeviceService(Convert.ToInt32(this.kioskBox.SelectedValue), Convert.ToInt32(this.deviceBox.SelectedValue), this.portNumber.Text, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            }
            else
            {
                callWebServ.CallUpdateKioskDeviceService(this.itemId, this.portNumber.Text, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            }

            string scriptText = "";

            switch (updateStatus)
            {
                case (int)ManagementScreenErrorCodes.SystemError:
                    this.messageArea.InnerHtml = "System error has occurred. Try again.";
                    break;
                case (int)ManagementScreenErrorCodes.SQLServerError:
                    this.messageArea.InnerHtml = "System error has occurred. Try again. (SQL Server Error)";
                    break;
                case -2:
                    this.messageArea.InnerHtml = "A record already exists.";
                    break;
                default:
                    this.messageArea.InnerHtml = "İşlem Başarılı.";
                    //ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:callOwn(); ", true);
                    //ViewState["merchantID"] = this.merchantID;
                    scriptText = "parent.showMessageWindowKioskDevice('İşlem Başarılı.');";
                    break;
            }
            if (scriptText != "")
            {
                ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", scriptText, true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowKioskDeviceSv");
        }
    }
}