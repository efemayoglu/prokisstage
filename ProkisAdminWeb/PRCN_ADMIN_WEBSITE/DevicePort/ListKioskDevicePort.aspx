﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ListKioskDevicePort.aspx.cs" Inherits="DevicePort_ListKioskDevicePort" MasterPageFile="~/webctrls/AdminSiteMPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<%@ Register Src="../MS_Control/MultipleSelection.ascx" TagName="MultipleSelection" TagPrefix="uc1" %>
<%@ Register Namespace="ClickableWebControl" TagPrefix="clkweb" %>
<asp:Content ID="pageContent" ContentPlaceHolderID="mainCPHolder" runat="server">
    <input type="hidden" id="pageNumRefField" runat="server" name="pageNumRefField" value="0" />
    <asp:Button ID="navigateButton" runat="server" OnClick="navigateButton_Click" CssClass="dummy" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { setInterval(function () { cache_clear() }, 120000); });
        function cache_clear() { window.location.reload(true); }
    </script>
    <script language="javascript" type="text/javascript">

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function searchButtonClicked(sender) {
            document.getElementById('pageNumRefField').value = "0";
            return true;
        }

        function deleteButtonClicked(storedProcedure, itemId) {
            var retVal = showDeleteWindowKioskDevice(storedProcedure, itemId);
        }


        function editButtonClicked(itemID, kioskName, deviceName, oldPortNumber) {
            var retVal = showKioskDeviceWindow(itemID, kioskName, deviceName, oldPortNumber);
        }

        function postForPaging(selectedPageNum) {
            document.getElementById('pageNumRefField').value = selectedPageNum;
            document.getElementById('navigateButton').click();
        }

        function strtDateChngd() {
            if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                document.getElementById('startDateField').value = document.getElementById('endDateField').value;
            }

        }

        function endDateChngd() {
            if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                document.getElementById('endDateField').value = document.getElementById('startDateField').value;
            }
        }

        function validateNo(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
    <script type="text/javascript" language="javascript" src="../js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery.tablednd_0_5.js"></script>

    <asp:HiddenField ID="idRefField" runat="server" Value="0" />
    <asp:HiddenField ID="oldOrderNumberField" runat="server" Value="0" />
    <asp:HiddenField ID="newOrderNumberField" runat="server" Value="0" />
    <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>

    <div align="center">
        <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp;</div>
        <asp:Panel ID="panel1" DefaultButton="searchButton" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto">
            <table border="0" cellpadding="0" cellspacing="0" id="Table2" runat="server" width="100%">
                <tr>
                    
                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 79px; text-align: left">
                        <asp:Label ID="mercLabel" runat="server" Visible="true">Kiosk:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px">
                      <uc1:MultipleSelection ID="MultipleSelection1" runat="server" CssClass="inputLine_100x20" /></td>
                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 50px; text-align: left">
                        <asp:Label ID="Label2" runat="server" Visible="true">Cihaz:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px">
                        <asp:ListBox ID="deviceBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>
                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 56px;">&nbsp;İşlem Ara:
                    </td>
                    <td align="left" style="width: 157px">
                        <asp:TextBox ID="searchTextField" CssClass="inputLine_150x20" runat="server"></asp:TextBox>
                    </td>
                    <td style="width: 6px">&nbsp;</td>
                    <td style="width: 86px">
                        <asp:Button ID="searchButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                            OnClick="searchButton_Click" Text="Ara"></asp:Button>
                    </td>
                    <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <div>&nbsp;</div>
    <div align="center">
        <asp:Panel ID="ListPanel" runat="server" CssClass="containerPanel_95Pxauto">
            <div align="left" class="windowTitle_container_autox30">
                İşlemler<hr style="border-bottom: 1px solid #b2b2b4;" />
            </div>
            <asp:UpdatePanel ID="ListUPanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" RenderMode="Block">
                <ContentTemplate>
                    <table cellpadding="0" cellspacing="0" width="95%" id="upTable" runat="server">
                        <tr>
                            <td style="width: 8px; height: 20px;"></td>
                            <td style="width: 200px; height: 20px;">
                                <asp:Label ID="recordInfoLabel" runat="server" Style="width: 200px; height: 20px; text-align: left" CssClass="data"></asp:Label>
                            </td>
                            <td style="height: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td style="height: 20px; width: 130px; text-align: right">
                                <asp:Label ID="Label1" Text="Listelenecek Kayıt Sayısı :  " runat="server" Style="width: 200px; height: 20px; text-align: left" CssClass="data"></asp:Label>
                            </td>
                            <td align="right" style="height: 20px; width: 33px">
                                <asp:TextBox ID="numberOfItemField" CssClass="inputLine_30x20_s" MaxLength="3" runat="server" onkeypress='validateNo(event)'></asp:TextBox></td>

                        </tr>
                        <tr>
                            <td align="center" class="tableStyle1" colspan="5">


                                <asp:Table ID="itemsTable" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                    BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="100%">
                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="#"></asp:TableHeaderCell>
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Kiosk Adı"></asp:TableHeaderCell>
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Cihaz Adı"></asp:TableHeaderCell>
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Port No"></asp:TableHeaderCell>
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text=""></asp:TableHeaderCell>
                                    </asp:TableRow>
                                </asp:Table>
                                <br />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="navigateButton" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </div>
    <br />
    <table width="95%">
        <tr align="right">
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="width: 80px">
                <asp:Label ID="Label5" runat="server" Text="powered by" CssClass="poweredbyTitle"></asp:Label>

            </td>
            <td style="width: 100px">
                <asp:LinkButton ID="LinkButton1" href="http://www.birlesikodeme.com/" runat="server" Text="BİRLEŞİK ÖDEME" CssClass="procenneTitle"></asp:LinkButton>

            </td>
        </tr>
    </table>
</asp:Content>
