﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Parser.Kiosk;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DevicePort_ListKioskDevicePort : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../DevicePort/ListKioskDevicePort.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        if (!Page.IsPostBack)
        {

            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            CallWebServices callWebServ = new CallWebServices();
            ParserListKioskName kiosks = callWebServ.CallGetKioskNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (kiosks != null)
            {
                MultipleSelection1.CreateCheckBox(kiosks.KioskNames);
            }

            ParserListInstutionName instutions = callWebServ.CallGetDeviceNamesService(this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            if (instutions != null)
            {

                this.deviceBox.DataSource = instutions.InstutionNames;
                this.deviceBox.DataBind();

            }

            ListItem item1 = new ListItem();
            item1.Text = "Kurum Seçiniz";
            item1.Value = "0";
            this.deviceBox.Items.Add(item1);
            this.deviceBox.SelectedValue = "0";

            this.pageNum = 0;
            this.SearchOrder();
        }
        else
        {
            MultipleSelection1.SetCheckBoxListValues(MultipleSelection1.sValue);
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder();
    }

    private void BindListTable(ParserListDevices device)
    {
        try
        {
            TableRow[] Row = new TableRow[device.Devices.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("deviceId", device.Devices[i].deviceId.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell kioskNameCell = new TableCell();
                TableCell deviceNameCell = new TableCell();
                TableCell portNoCell = new TableCell();
                TableCell detailsCell = new TableCell();


                indexCell.CssClass = "inputTitleCell4";
                idCell.CssClass = "inputTitleCell4";
                kioskNameCell.CssClass = "inputTitleCell4";
                deviceNameCell.CssClass = "inputTitleCell4";
                portNoCell.CssClass = "inputTitleCell4";
                detailsCell.CssClass = "inputTitleCell4";

                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1")
                {
                    deviceNameCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editButtonClicked('" + device.Devices[i].deviceId + "', '" + device.Devices[i].kioskName + "', '" + device.Devices[i].deviceName + "', '" + device.Devices[i].port + "');\" class=\"anylink\">" + device.Devices[i].deviceName + "</a>";
                }
                else
                {
                    deviceNameCell.Text = device.Devices[i].deviceName;
                }
                indexCell.Text = (startIndex + i).ToString();
                idCell.Text = device.Devices[i].deviceId.ToString();
                kioskNameCell.Text = device.Devices[i].kioskName;
                
                portNoCell.Text = device.Devices[i].port;
                idCell.Visible = false;
                detailsCell.Width = Unit.Pixel(30);

                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanDelete == "1")
                {
                    detailsCell.Text = "<a href=# onClick=\"deleteButtonClicked('[pk].[delete_kiosk_device]'," + device.Devices[i].deviceId + ");\"><img src=../images/delete.png border=0 title=\"Silme\" /></a>";
                }
                else
                {
                    detailsCell.Text = " ";
                }

                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        idCell,
						kioskNameCell,
                        deviceNameCell,
                        portNoCell,
                        detailsCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (device.recordCount < currentRecordEnd)
                currentRecordEnd = device.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + device.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(device.pageCount, this.pageNum, device.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;

            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
            {
                TableRow addNewRow = new TableRow();
                TableCell addNewCell = new TableCell();
                TableCell spaceCell = new TableCell();
                spaceCell.CssClass = "inputTitleCell4";
                spaceCell.ColumnSpan = (this.itemsTable.Rows[1].Cells.Count - 1);
                addNewCell.Text = "<a href=\"javascript:void(0);\" onclick=\"editButtonClicked('0','0','0','0');\" class=\"anylink\"><img src=../images/add.png border=0 title=\"Cihaz Ekle\" /></a>";
                addNewRow.Cells.Add(spaceCell);
                addNewRow.Cells.Add(addNewCell);
                this.itemsTable.Rows.Add(addNewRow);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListTransactionBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.SearchOrder();
    }

    private void SearchOrder()
    {
        try
        {
            string kioskIds = "";

            if (MultipleSelection1.sText == "Tümü" || MultipleSelection1.sValue == "" || MultipleSelection1.sValue == "Tümü")
                kioskIds = "0";
            else
                kioskIds = MultipleSelection1.sValue;

            this.numberOfItemsPerPage = Convert.ToInt32(this.numberOfItemField.Text);

            //ViewState["NumberOfItem"] = this.numberOfItemField.Text;
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListDevices devices = callWebServ.CallListDeviceService(this.searchTextField.Text,
                                                                             Convert.ToInt32(this.numberOfItemField.Text),
                                                                             this.pageNum,
                                                                             recordCount,
                                                                             pageCount,
                                                                             kioskIds,
                                                                             Convert.ToInt32(this.deviceBox.SelectedValue),
                                                                             this.LoginDatas.AccessToken,
                                                                             this.LoginDatas.User.Id);


            if (devices != null)
            {
                if (devices.errorCode == 0)
                {
                    this.BindListTable(devices);
                }
                else if (devices.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + devices.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListDeviceSrch");
        }
    }
  }