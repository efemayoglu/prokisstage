﻿using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Customer_EnterCodeChangeCustomerInfo : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private int customerId = 0;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];
        this.customerId = Convert.ToInt32(Request.QueryString["itemID"]);
        this.hiddenUserId.Value = Request.QueryString["itemID"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Customer/ListWebCustomer.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit != "1")
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }


        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }

    private void BindCtrls()
    {
        try
        {
            ViewState["tryCount"] = 0;
            CallWebServices callWebServ = new CallWebServices();
            ParserGetSecretKey getSecretKey = callWebServ.CallGetCustomerKeyService(this.customerId, this.LoginDatas.User.Id, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
            //int keyLength=Utility.DecryptString(getSecretKey.secretKey).Length;
            string secretKey = Utility.DecryptString(getSecretKey.secretKey);
            int keyLength = secretKey.Length;
            ViewState["secretLen"] = keyLength;

            int firstIndex = Convert.ToInt16(Utility.generateRandomNum(0, keyLength - 1));
            int secondIndex = Convert.ToInt16(Utility.generateRandomNum(0, keyLength - 1));
            do
            {
                secondIndex = Convert.ToInt16(Utility.generateRandomNum(0, keyLength - 1));
            }
            while (secondIndex == firstIndex);

            if (firstIndex > secondIndex)
            {
                ViewState["firtIndex"] = secondIndex;
                ViewState["secondIndex"] = firstIndex;
                this.firstLbl.Text = (secondIndex + 1).ToString() + ".";
                this.scndLbl.Text = (firstIndex + 1).ToString() + ".";
            }
            else
            {
                ViewState["firtIndex"] = firstIndex;
                ViewState["secondIndex"] = secondIndex;
                this.firstLbl.Text = (firstIndex + 1).ToString() + ".";
                this.scndLbl.Text = (secondIndex + 1).ToString() + ".";
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowSecretKeyBC");
        }
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        SaveAskiCode();
    }

    private void SaveAskiCode()
    {
        try
        {
            if (this.customerId != 0)
            {
                if (Convert.ToInt16(ViewState["tryCount"]) < 3)
                {
                    //Karakter doğruluklarını kontrol et
                    CallWebServices callWebServ = new CallWebServices();
                    ParserGetSecretKey getSecretKey = callWebServ.CallGetCustomerKeyService(this.customerId, this.LoginDatas.User.Id, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);
                    string secretKey = Utility.DecryptString(getSecretKey.secretKey);
                    if (secretKey.Substring(Convert.ToInt16(ViewState["firtIndex"]), 1) == firstCharacterField.Text.ToUpper() && secretKey.Substring(Convert.ToInt16(ViewState["secondIndex"]), 1) == secondCharacterField.Text.ToUpper())
                    {
                        Response.Redirect("../customer/ChangeCustomerInfo.aspx?itemID=" + this.customerId, true);
                    }
                    else
                    {
                        ViewState["tryCount"] = Convert.ToInt16(ViewState["tryCount"]) + 1;
                        this.messageArea.InnerHtml = "Yanlış Giriş Yaptınız !";
                        this.firstCharacterField.Text = "";
                        this.secondCharacterField.Text = "";
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.saveButton, this.saveButton.GetType(), "CloseScript", "parent.hideModalPopup2();", true);
                }
            }
            else
            {
                this.messageArea.InnerHtml = "İlgili müşteri alınamadı !";
            }


        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowUserSv");
        }
    }
}