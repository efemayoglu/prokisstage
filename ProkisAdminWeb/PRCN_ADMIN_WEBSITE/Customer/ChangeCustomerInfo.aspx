﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChangeCustomerInfo.aspx.cs" Inherits="Customer_ChangeCustomerInfo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />
    <input type="hidden" id="hiddenUserId" runat="server" name="hiddenUserId" value="0" />
    <script language="javascript" type="text/javascript">

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function checkInfoForm() {
            var status = true;
            var messageText = "";

            if (document.getElementById('cellPhoneField').value == "") {
                status = false;
                messageText = "Cep Telefonu Numaranızı Giriniz.";
            }
            else if (document.getElementById('mailField').value == "") {
                status = false;
                messageText = "E-Mail Adresini Giriniz.";
            }
            else if (ValidateEmail(document.getElementById('mailField').value) == false) {
                status = false;
                messageText = "Geçerli Bir E-Mail Adresi Giriniz.";
            }
            else {
                messageText = "";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }

        function ValidateEmail(mail) {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
                return true;
            }
            return false;
        }

        function validateNo(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
    <base target="_self" />
    <style type="text/css">
        .auto-style14 {
            font-family: Century Gothic;
            font-size: 14px;
            color: #8A8B8C;
            text-decoration: none;
            font-weight: bold;
            padding-bottom: 4px;
            text-align: center;
        }

        .auto-style16 {
            font-family: Century Gothic;
            font-size: 14px;
            color: #8A8B8C;
            text-decoration: none;
            font-weight: bold;
            padding-bottom: 4px;
            text-align: center;
            width: 16px;
        }

        .auto-style17 {
            font-family: Century Gothic;
            font-size: 14px;
            color: #8A8B8C;
            text-decoration: none;
            font-weight: bold;
            padding-bottom: 4px;
            text-align: center;
            width: 18px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel7" DefaultButton="updateCustomerInfoButton" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <div align="left" id="brandDetailsTab" class="windowTitle_container_autox30">
                    Müşteri Bilgi Güncelleme<hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>
                <table border="0" cellpadding="2" cellspacing="0" id="Table1" runat="server">
                    <tr style="height: 30px;">
                        <td align="left" style="text-align: left;" class="staticTextLine_200x20_noBorder">Maili:</td>
                        <td align="right" style="text-align: left;">
                            <asp:TextBox ID="mailField" CssClass="inputLine_150x20" runat="server" ClientIDMode="Static"></asp:TextBox></td>
                    </tr>
                    <tr style="height: 30px;">
                        <td align="left" style="text-align: left;" class="staticTextLine_200x20_noBorder">Cep Telefonu :</td>
                        <td align="right" style="text-align: left;">
                            <asp:TextBox ID="cellPhoneField" CssClass="inputLine_150x20" runat="server" ClientIDMode="Static"></asp:TextBox></td>
                    </tr>

                </table>

                <table>
                    <tr>
                        <td>
                            <asp:Button ID="updateCustomerInfoButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                OnClick="updateCustomerInfoButton_Click" OnClientClick="return checkInfoForm();" Text="Güncelle" Width="70px"></asp:Button>
                        </td>
                        <td>
                            <asp:Button ID="cancelButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                OnClientClick="parent.hideModalPopup2();" Text="Vazgeç"></asp:Button>
                        </td>
                    </tr>
                </table>
                <br />
            </asp:Panel>
        </div>
    </form>
</body>
</html>
