﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Customer_ListBlackList : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;
    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int orderSelectionColumn = 8;
    private int orderSelectionDescAsc = 1;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Customer/ListBlackList.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));

        for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
        {
            if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Customer/ListBlackList.aspx")
            {
                this.ownSubMenuIndex = i;
                break;
            }
        }

        if (!Page.IsPostBack)
        {
            ViewState["OrderColumn"] = 8;
            ViewState["OrderDesc"] = 1;

            this.pageNum = 0;
            this.SearchOrder(8, 1);
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder(8, 1);
    }

    public void SearchOrder(int orderSelectionColumn, int descAsc)
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;

            CallWebServices callWebServ = new CallWebServices();
            ParserListBlackLists users = callWebServ.CallListBlackListService(this.searchTextField.Text,
                                                                    this.numberOfItemsPerPage,
                                                                    this.pageNum,
                                                                    recordCount,
                                                                    pageCount,
                                                                    orderSelectionColumn,
                                                                    descAsc, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (users != null)
            {
                if (users.errorCode == 0 || users.errorCode == 2)
                {
                    this.BindListTable(users, users.recordCount, users.pageCount);
                }

                else if (users.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + users.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListUsersSrch");
        }
    }

    private void BindListTable(ParserListBlackLists list, int recordcount, int pagecount)
    {
        try
        {
            if (list.errorCode == 0)
            {
                TableRow[] Row = new TableRow[list.BlackList.Count];
                int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
                for (int i = 0; i < Row.Length; i++)
                {
                    Row[i] = new TableRow();
                    Row[i].Attributes.Add("Id", list.BlackList[i].Id.ToString());

                    TableCell indexCell = new TableCell();
                    TableCell idCell = new TableCell();
                    TableCell nameCell = new TableCell();
                    TableCell tcknCell = new TableCell();
                    TableCell birthYearCell = new TableCell();
                    TableCell descriptionCell = new TableCell();
                    TableCell createdUserCell = new TableCell();
                    TableCell creationDateCell = new TableCell();
                    TableCell statusCell = new TableCell();
                    TableCell detailsCell = new TableCell();

                    TableCell fakeCell1 = new TableCell();
                    TableCell fakeCell2 = new TableCell();
                    TableCell fakeCell3 = new TableCell();
                    TableCell fakeCell4 = new TableCell();
                    TableCell fakeCell5 = new TableCell();
                    TableCell fakeCell6 = new TableCell();
                    TableCell fakeCell7 = new TableCell();

                    fakeCell1.CssClass = "inputTitleCell4";
                    fakeCell2.CssClass = "inputTitleCell4";
                    fakeCell3.CssClass = "inputTitleCell4";
                    fakeCell4.CssClass = "inputTitleCell4";
                    fakeCell5.CssClass = "inputTitleCell4";
                    fakeCell6.CssClass = "inputTitleCell4";
                    fakeCell7.CssClass = "inputTitleCell4";

                    fakeCell1.Visible = false;
                    fakeCell2.Visible = false;
                    fakeCell3.Visible = false;
                    fakeCell4.Visible = false;
                    fakeCell5.Visible = false;
                    fakeCell6.Visible = false;
                    fakeCell7.Visible = false;

                    indexCell.CssClass = "inputTitleCell4";
                    idCell.CssClass = "inputTitleCell4";
                    nameCell.CssClass = "inputTitleCell4";
                    tcknCell.CssClass = "inputTitleCell4";
                    birthYearCell.CssClass = "inputTitleCell4";
                    descriptionCell.CssClass = "inputTitleCell4";
                    createdUserCell.CssClass = "inputTitleCell4";
                    creationDateCell.CssClass = "inputTitleCell4";
                    statusCell.CssClass = "inputTitleCell4";
                    detailsCell.CssClass = "inputTitleCell4";

                    nameCell.Width = Unit.Pixel(150);
                    nameCell.HorizontalAlign = HorizontalAlign.Center;
                    nameCell.Style.Add("padding-left", "5px");
                    nameCell.Text = list.BlackList[i].Name;

                    tcknCell.Width = Unit.Pixel(100);
                    tcknCell.HorizontalAlign = HorizontalAlign.Center;
                    tcknCell.Style.Add("padding-left", "5px");
                    tcknCell.Text = list.BlackList[i].Tckn;

                    birthYearCell.Width = Unit.Pixel(100);
                    birthYearCell.HorizontalAlign = HorizontalAlign.Center;
                    birthYearCell.Style.Add("padding-left", "5px");
                    birthYearCell.Text = list.BlackList[i].BirthYear;


                    descriptionCell.Width = Unit.Pixel(100);
                    descriptionCell.HorizontalAlign = HorizontalAlign.Center;
                    descriptionCell.Style.Add("padding-left", "5px");
                    descriptionCell.Text = list.BlackList[i].Description;


                    createdUserCell.Width = Unit.Pixel(100);
                    createdUserCell.HorizontalAlign = HorizontalAlign.Center;
                    createdUserCell.Style.Add("padding-left", "5px");
                    createdUserCell.Text = list.BlackList[i].CreatedUser;

                    creationDateCell.Width = Unit.Pixel(100);
                    creationDateCell.HorizontalAlign = HorizontalAlign.Center;
                    creationDateCell.Style.Add("padding-left", "5px");
                    creationDateCell.Text = list.BlackList[i].InsertionDate;

                    if (list.BlackList[i].Status == 1)
                    {
                        statusCell.Text = "Kara Listede";
                    }
                    else if (list.BlackList[i].Status == 2)
                    {
                        statusCell.Text = "Kara Liste için Onay Bekliyor";
                    }
                    else if (list.BlackList[i].Status == 3)
                    {
                        statusCell.Text = "Kara Listeden Silinmek için Onay Bekliyor";
                    }

                    indexCell.Width = Unit.Pixel(30);
                    indexCell.Text = (startIndex + i).ToString();
                    idCell.Visible = false;
                    idCell.Text = list.BlackList[i].Id.ToString();

                    detailsCell.Width = Unit.Pixel(75);

                    if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanDelete == "1" && this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1")
                    {
                        if (list.BlackList[i].Status == 1)
                        {
                            detailsCell.Text = "<a href=# onClick=\"deleteButtonClicked('[pp].[delete_from_black_list]'," + list.BlackList[i].Id + ");\"><img src=../images/delete.png border=0 title=\"Silme\" /></a>";
                        }
                        else if (list.BlackList[i].Status == 2)
                        {
                            detailsCell.Text = "<a href=# onClick=\"approveButtonClicked('[pp].[approve_add_to_black_list]'," + list.BlackList[i].Id + ");\"><img src=../images/apBL.png border=0 title=\"Eklemeyi Onaylama\" /></a>"
                                + "<a href=# onClick=\"approveButtonClicked('[pp].[delete_add_to__black_list]'," + list.BlackList[i].Id + ");\"><img src=../images/deladdBL.png border=0 title=\"Eklemeyi Silme\" /></a>";
                        }
                        else if (list.BlackList[i].Status == 3)
                        {
                            detailsCell.Text = "<a href=# onClick=\"approveButtonClicked('[pp].[approve_delete_from_black_list]'," + list.BlackList[i].Id + ");\"><img src=../images/delBL.png border=0 title=\"Silmeyi Onaylama\" /></a>"
                                + "<a href=# onClick=\"approveButtonClicked('[pp].[delete_delete_from_black_list]'," + list.BlackList[i].Id + ");\"><img src=../images/delapBL.png border=0 title=\"Silmeyi Silme\" /></a>";
                        }
                        else
                        {
                            detailsCell.Text = "";
                        }
                    }
                    else if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1")
                    {
                        if (list.BlackList[i].Status == 2)
                        {
                            detailsCell.Text = "<a href=# onClick=\"approveButtonClicked('[pp].[approve_add_to_black_list]'," + list.BlackList[i].Id + ");\"><img src=../images/apBL.png border=0 title=\"Eklemeyi Onaylama\" /></a>"
                                 + "<a href=# onClick=\"approveButtonClicked('[pp].[delete_add_to__black_list]'," + list.BlackList[i].Id + ");\"><img src=../images/deladdBL.png border=0 title=\"Eklemeyi Silme\" /></a>";

                        }
                        else if (list.BlackList[i].Status == 3)
                        {
                            detailsCell.Text = "<a href=# onClick=\"approveButtonClicked('[pp].[approve_delete_from_black_list]'," + list.BlackList[i].Id + ");\"><img src=../images/delBL.png border=0 title=\"Silmeyi Onaylama\" /></a>"
                                 + "<a href=# onClick=\"approveButtonClicked('[pp].[delete_delete_from_black_list]'," + list.BlackList[i].Id + ");\"><img src=../images/delapBL.png border=0 title=\"Silmeyi Silme\" /></a>";
                        }
                        else
                        {
                            detailsCell.Text = "";
                        }
                    }
                    else if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanDelete == "1")
                    {
                        if (list.BlackList[i].Status == 1)
                        {
                            detailsCell.Text = "<a href=# onClick=\"deleteButtonClicked('[pp].[delete_from_black_list]'," + list.BlackList[i].Id + ");\"><img src=../images/delete.png border=0 title=\"Silme\" /></a>";
                        }
                        else
                        {
                            detailsCell.Text = "";
                        }
                    }
                    else
                    {
                        detailsCell.Text = "";
                    }

                    Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
						idCell,
                        fakeCell1,
                        fakeCell2,
                        fakeCell3,
                        fakeCell4,
                        fakeCell5,
                        fakeCell6,
                        nameCell,
                        tcknCell,
                        birthYearCell,
                        descriptionCell,
                        createdUserCell,
                        creationDateCell,
                        statusCell,
                        detailsCell
					});

                    if (i % 2 == 0)
                        Row[i].CssClass = "listrow";
                    else
                        Row[i].CssClass = "listRowAlternate";
                }
                this.itemsTable.Rows.AddRange(Row);

                TableRow pagingRow = new TableRow();
                TableCell pagingCell = new TableCell();

                pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
                pagingCell.HorizontalAlign = HorizontalAlign.Right;
                pagingCell.Text = WebUtilities.GetPagingText(pagecount, this.pageNum, recordcount);
                pagingRow.Cells.Add(pagingCell);
                this.itemsTable.Rows.AddAt(0, pagingRow);
                this.itemsTable.Visible = true;

                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
                {
                    TableRow addNewRow = new TableRow();
                    TableCell addNewCell = new TableCell();
                    TableCell spaceCell = new TableCell();
                    spaceCell.CssClass = "inputTitleCell4";
                    spaceCell.ColumnSpan = (this.itemsTable.Rows[1].Cells.Count - 1);
                    addNewCell.Text = "<a href=\"javascript:void(0);\" onclick=\"addButtonClicked();\" class=\"anylink\"><img src=../images/add_BL.png border=0 title=\"Ekle\" /></a>";

                    addNewRow.Cells.Add(spaceCell);
                    addNewRow.Cells.Add(addNewCell);
                    this.itemsTable.Rows.Add(addNewRow);
                }

            }

            else if (list.errorCode == 2)
            {
                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
                {
                    TableRow addNewRow = new TableRow();
                    TableCell addNewCell = new TableCell();
                    TableCell spaceCell = new TableCell();
                    spaceCell.CssClass = "inputTitleCell4";
                    spaceCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count - 1);
                    addNewCell.Text = "<a href=\"javascript:void(0);\" onclick=\"addButtonClicked();\" class=\"anylink\"><img src=../images/add_BL.png border=0 title=\"Ekle\" /></a>";

                    addNewRow.Cells.Add(spaceCell);
                    addNewRow.Cells.Add(addNewCell);
                    this.itemsTable.Rows.Add(addNewRow);
                }
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListUsersBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.SearchOrder(orderSelectionColumn, orderSelectionDescAsc);
    }

    protected void deleteButton_Click(object sender, EventArgs e)
    {

        this.SearchOrder(8, 1);
    }

    public void Sort(Object sender, EventArgs e)
    {
        this.orderSelectionColumn = Convert.ToInt32(ViewState["OrderColumn"]);
        this.orderSelectionDescAsc = Convert.ToInt32(ViewState["OrderDesc"]);
        this.pageNum = 0;

        switch (((ClickableWebControl.ClickableTableHeaderCell)sender).ID)
        {

            case "Name":
                if (orderSelectionColumn == 1)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        Name.Text = "<a>Ad Soyad    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        Name.Text = "<a>Ad Soyad    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    Name.Text = "<a>Ad Soyad    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 1;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Tckn":
                if (orderSelectionColumn == 2)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        Tckn.Text = "<a>TCKN    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        Tckn.Text = "<a>TCKN    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    Tckn.Text = "<a>TCKN    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 2;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "BirthYear":
                if (orderSelectionColumn == 3)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        BirthYear.Text = "<a>Doğum Yılı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        BirthYear.Text = "<a>Doğum Yılı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    BirthYear.Text = "<a>Doğum Yılı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 3;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Description":
                if (orderSelectionColumn == 4)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        Description.Text = "<a>Açıklama    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        Description.Text = "<a>Açıklama    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    Description.Text = "<a>Açıklama    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 4;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CreatedUser":
                if (orderSelectionColumn == 5)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        CreatedUser.Text = "<a>Oluşturan Kullanıcı    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        CreatedUser.Text = "<a>Oluşturan Kullanıcı    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    CreatedUser.Text = "<a>Oluşturan Kullanıcı    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 5;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "CreationDate":
                if (orderSelectionColumn == 6)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        CreationDate.Text = "<a>Oluşturulma Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        CreationDate.Text = "<a>Oluşturulma Tarihi    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    CreationDate.Text = "<a>Oluşturulma Tarihi    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 6;
                    orderSelectionDescAsc = 1;
                }
                break;
            case "Status":
                if (orderSelectionColumn == 7)
                {
                    if (orderSelectionDescAsc == 0)
                    {
                        Status.Text = "<a>Durumu    <img src=../images/arrow_up.png border=0/></a>";
                        orderSelectionDescAsc = 1;
                    }
                    else
                    {
                        Status.Text = "<a>Durumu    <img src=../images/arrow_down.png border=0/></a>";
                        orderSelectionDescAsc = 0;
                    }
                }
                else
                {
                    Status.Text = "<a>Durumu    <img src=../images/arrow_up.png border=0/></a>";
                    orderSelectionColumn = 7;
                    orderSelectionDescAsc = 1;
                }
                break;
            default:

                orderSelectionColumn = 1;
                orderSelectionDescAsc = 1;

                break;
        }

        ViewState["OrderColumn"] = this.orderSelectionColumn;
        ViewState["OrderDesc"] = this.orderSelectionDescAsc;

        SearchOrder(this.orderSelectionColumn, this.orderSelectionDescAsc);
    }


}