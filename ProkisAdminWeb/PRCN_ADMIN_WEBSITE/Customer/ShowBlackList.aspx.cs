﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Customer_ShowBlackList : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Customer/ListBlackList.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit == "1" || this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
            {
            }
            else
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }

        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }

    private void BindCtrls()
    {
        try
        {

            this.SetControls();
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowBlackListBC");
        }
    }

    private void SetControls()
    {
        try
        {

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowBlackListSC");
        }
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        SaveUser();
    }

    private void SaveUser()
    {
        try
        {
            CallWebServices callWebServ = new CallWebServices();

            ParserOperation operationResult = null;

            operationResult = callWebServ.CallSaveBlackListService(this.nameField.Text, this.tcknField.Text, this.birthYearField.Text, this.descriptionField.Text, this.LoginDatas.User.Id, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (operationResult != null)
            {
                if (operationResult.errorCode != 0)
                {
                    this.messageArea.InnerHtml = operationResult.errorDescription;
                }
                else if (operationResult.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    this.messageArea.InnerHtml = operationResult.errorDescription;
                    string whichPage ="BlackList";
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "CloseScript", "parent.hideModalPopupLastandRefresh('" + whichPage + "');", true);
                }
            }
            else
            {
                this.messageArea.InnerHtml = "Sisteme Erişilemiyor !";
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ShowUserSv");
        }
    }
}