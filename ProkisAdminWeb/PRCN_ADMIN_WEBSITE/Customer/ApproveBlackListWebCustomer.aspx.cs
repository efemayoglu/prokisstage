﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Customer_ApproveBlackListWebCustomer : System.Web.UI.Page
{
    private ParserLogin LoginDatas = null;

    private int pageNum;
    private int numberOfItemsPerPage = 20;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Customer/ApproveBlackListWebCustomer.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex == -1)
        {
            Response.Redirect("../Default.aspx", true);
        }

        this.pageNum = Convert.ToInt32("0" + this.pageNumRefField.Value);

        numberOfItemsPerPage = Convert.ToInt16(Utility.GetConfigValue("NumberOfItemsPerPage"));



        if (!Page.IsPostBack)
        {
            this.numberOfItemField.Text = numberOfItemsPerPage.ToString();

            this.pageNum = 0;
            this.SearchOrder();
        }
    }

    protected void searchButton_Click(object sender, System.EventArgs e)
    {
        this.pageNum = 0;
        this.SearchOrder();
    }

    private void BindListTable(ParserListWebCustomers customers)
    {
        try
        {
            TableRow[] Row = new TableRow[customers.Customers.Count];
            int startIndex = (this.pageNum * this.numberOfItemsPerPage) + 1;
            for (int i = 0; i < Row.Length; i++)
            {
                Row[i] = new TableRow();
                Row[i].Attributes.Add("customerId", customers.Customers[i].id.ToString());

                TableCell indexCell = new TableCell();
                TableCell idCell = new TableCell();
                TableCell customerNameCell = new TableCell();
                TableCell customerNoCell = new TableCell();
                TableCell tcknCell = new TableCell();
                TableCell emailCell = new TableCell();
                TableCell cellphoneCell = new TableCell();
                TableCell statusCell = new TableCell();
                TableCell approveStatusCell = new TableCell();
                TableCell updatedUSerCell = new TableCell();
                TableCell updatedTimeCell = new TableCell();
                TableCell detailCell = new TableCell();

                indexCell.CssClass = "inputTitleCell4";
                idCell.CssClass = "inputTitleCell4";
                customerNameCell.CssClass = "inputTitleCell4";
                customerNoCell.CssClass = "inputTitleCell4";
                tcknCell.CssClass = "inputTitleCell4";
                emailCell.CssClass = "inputTitleCell4";
                cellphoneCell.CssClass = "inputTitleCell4";
                statusCell.CssClass = "inputTitleCell4";
                approveStatusCell.CssClass = "inputTitleCell4";
                updatedUSerCell.CssClass = "inputTitleCell4";
                updatedTimeCell.CssClass = "inputTitleCell4";
                detailCell.CssClass = "inputTitleCell4";

                indexCell.Text = (startIndex + i).ToString();
                idCell.Text = customers.Customers[i].id.ToString();


                customerNameCell.Text = customers.Customers[i].name;
                tcknCell.Text = customers.Customers[i].tckn;
                emailCell.Text = customers.Customers[i].email;
                cellphoneCell.Text = customers.Customers[i].cellphone;
                statusCell.Text = customers.Customers[i].status;
                if (customers.Customers[i].approveStatus == 1)
                {
                    approveStatusCell.Text = "Black List için Admin Onayı Bekliyor";
                }
                updatedUSerCell.Text = customers.Customers[i].approveUser;
                updatedTimeCell.Text = customers.Customers[i].approveTime;

                if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanAdd == "1")
                {
                    detailCell.Text = "<a href=# onClick=\"addToBlackListButtonClicked('[pp].[add_to_blacklist_from_customer_admin]'," + customers.Customers[i].id + ");\"><img src=../images/add_BL.png border=0 title=\"KaraListe\" /></a>"
                    + "<a href=# onClick=\"deleteButtonClicked('[pp].[delete_admin_approve_account_action]'," + customers.Customers[i].id + ");\"><img src=../images/delete.png border=0 title=\"Silme\" /></a>";
                }
                else
                {
                    customerNoCell.Text = customers.Customers[i].customerNo;
                    detailCell.Text = " ";
                }

                idCell.Visible = false;
                Row[i].Cells.AddRange(new TableCell[]{
						indexCell,
                        idCell,
                        customerNameCell,
                        customerNoCell,
						tcknCell,
                        emailCell,
                        cellphoneCell,
						statusCell,
                        approveStatusCell,
                        updatedUSerCell,
                        updatedTimeCell,
                        detailCell
					});

                if (i % 2 == 0)
                    Row[i].CssClass = "listrow";
                else
                    Row[i].CssClass = "listRowAlternate";
            }

            this.itemsTable.Rows.AddRange(Row);
            TableRow pagingRow = new TableRow();
            TableCell pagingCell = new TableCell();

            int currentRecordStart = this.pageNum * this.numberOfItemsPerPage + 1;
            int currentRecordEnd = (this.pageNum + 1) * this.numberOfItemsPerPage;

            if (customers.recordCount < currentRecordEnd)
                currentRecordEnd = customers.recordCount;

            if (currentRecordEnd > 0)
            {
                this.recordInfoLabel.Text = currentRecordStart.ToString() + " - " + currentRecordEnd.ToString() + " / " + customers.recordCount.ToString() + " kayıt bulundu.";
            }

            pagingCell.ColumnSpan = (this.itemsTable.Rows[0].Cells.Count);
            pagingCell.HorizontalAlign = HorizontalAlign.Right;
            pagingCell.Text = WebUtilities.GetPagingText(customers.pageCount, this.pageNum, customers.recordCount);
            pagingRow.Cells.Add(pagingCell);
            this.itemsTable.Rows.AddAt(0, pagingRow);
            this.itemsTable.Visible = true;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListCustomersBDT");
        }
    }

    protected void navigateButton_Click(object sender, EventArgs e)
    {
        this.SearchOrder();
    }

    private void SearchOrder()
    {
        try
        {
            int recordCount = 0;
            int pageCount = 0;
            CallWebServices callWebServ = new CallWebServices();
            ParserListWebCustomers customers = callWebServ.CallListWebCustomerService(this.searchNameTextField.Text,
                                                                                   this.searchCustNoTextField.Text,
                                                                                   this.searchTcknTextField.Text,
                                                                                   Convert.ToInt32(this.numberOfItemField.Text),
                                                                                   this.pageNum,
                                                                                   recordCount,
                                                                                   pageCount,
                                                                                   1
                                                                                   , this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (customers != null)
            {
                if (customers.errorCode == 0)
                {
                    this.BindListTable(customers);
                }
                else if (customers.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + customers.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme Erişilemiyor !'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ListCustomersSrch");
        }
    }

    protected void excelButton_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    private void ExportToExcel()
    {
        try
        {
            CallWebServices callWebService = new CallWebServices();
            ParserListWebCustomers lists = callWebService.CallListWebCustomerForExcelService(this.searchNameTextField.Text,
                                                                                   this.searchCustNoTextField.Text,
                                                                                   this.searchTcknTextField.Text, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (lists != null)
            {
                if (lists.errorCode == 0)
                {

                    ExportExcellDatas exportExcell = new ExportExcellDatas();
                    exportExcell.ExportExcell(lists.Customers, lists.recordCount, "Müşteri Listesi");
                }
                else if (lists.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('" + lists.errorDescription + "'); ", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "Javascript", "parent.showAlert('Sisteme erişilemiyor!'); ", true);
            }
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ExcelAccount");
        }
    }

}