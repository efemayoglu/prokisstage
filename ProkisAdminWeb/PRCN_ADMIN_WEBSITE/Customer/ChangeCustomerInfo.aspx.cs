﻿using PRCNCORE.Constants;
using PRCNCORE.Parser;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Customer_ChangeCustomerInfo : System.Web.UI.Page
{
    private ParserLogin LoginDatas;
    private int customerId = 0;
    private int ownSubMenuIndex = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoginDatas = (ParserLogin)Session["LoginData"];
        this.customerId = Convert.ToInt32(Request.QueryString["itemID"]);
        this.hiddenUserId.Value = Request.QueryString["itemID"];

        if (this.LoginDatas != null)
        {
            for (int i = 0; i < this.LoginDatas.UserRoles.Count; i++)
            {
                if (this.LoginDatas.UserRoles[i].SubMenuURL == "../Customer/ListWebCustomer.aspx")
                {
                    this.ownSubMenuIndex = i;
                    break;
                }
            }
        }
        else
        {
            Session.Abandon();
            Response.Redirect("../root/Login.aspx", true);
        }
        if (this.ownSubMenuIndex > -1)
        {
            if (this.LoginDatas.UserRoles[this.ownSubMenuIndex].CanEdit != "1")
            {
                Response.Redirect("../Default.aspx", true);
            }
        }
        else
        {
            Response.Redirect("../Default.aspx", true);
        }


        if (!this.IsPostBack)
        {
            this.BindCtrls();
        }
    }

    private void BindCtrls()
    {
        CallWebServices callWebServ = new CallWebServices();
        ParserChangeCustomerInfo changeCustomerInfo = callWebServ.CallGetChangeCustomerInfoService(this.customerId, this.LoginDatas.User.Id, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

        if (changeCustomerInfo != null)
        {
            if (changeCustomerInfo.errorCode == 0)
            {
                this.mailField.Text = changeCustomerInfo.email;
                this.cellPhoneField.Text = changeCustomerInfo.cellphone;
            }
            else if (changeCustomerInfo.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
            {
                Session.Abandon();
                Session.RemoveAll();
                Response.Redirect("../root/Login.aspx", true);
            }
            else
            {
                this.messageArea.InnerHtml = changeCustomerInfo.errorDescription;
            }
        }
        else
        {
            this.messageArea.InnerHtml = "Sisteme erişilemiyor";
        }
    }

    protected void updateCustomerInfoButton_Click(object sender, EventArgs e)
    {
        try
        {
            CallWebServices callWebServ = new CallWebServices();
            ParserOperation response = callWebServ.CallUpdateCustomerInfoService(this.customerId, this.LoginDatas.User.Id, this.mailField.Text, this.cellPhoneField.Text, this.LoginDatas.AccessToken, this.LoginDatas.User.Id);

            if (response != null)
            {
                if (response.errorCode == 0)
                {

                    this.messageArea.InnerHtml = "Bilgiler başarılı bir şekilde değiştirildi";
                }
                else if (response.errorCode == (int)ReturnCodes.INVALID_ACCESS_TOKEN)
                {
                    Session.Abandon();
                    Session.RemoveAll();
                    Response.Redirect("../root/Login.aspx", true);
                }
                else
                {
                    this.messageArea.InnerHtml = response.errorDescription;
                }
            }
            else
            {
                this.messageArea.InnerHtml = "Sisteme erişilemiyor.";
            }

        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "MyPageUpdateInfo");
        }


    }
}