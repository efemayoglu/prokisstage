using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace CheckBoxListExCtrl
{
    public class CheckBoxListExCtrl : CheckBoxList, IRepeatInfoUser
    {
        void IRepeatInfoUser.RenderItem(ListItemType itemType, int repeatIndex, RepeatInfo repeatInfo, HtmlTextWriter writer)
        {
            string clientID = UniqueID + this.ClientIDSeparator + repeatIndex.ToString(NumberFormatInfo.InvariantInfo); //var

            writer.WriteBeginTag("input");
            writer.WriteAttribute("type", "checkbox");
            writer.WriteAttribute("name", UniqueID + this.IdSeparator + repeatIndex.ToString(NumberFormatInfo.InvariantInfo));
            writer.WriteAttribute("id", clientID);
            writer.WriteAttribute("value", Items[repeatIndex].Value);
            if (Items[repeatIndex].Selected)
                writer.WriteAttribute("checked", "checked");
            System.Web.UI.AttributeCollection attrs = Items[repeatIndex].Attributes;
            foreach (string key in attrs.Keys)
            {
                writer.WriteAttribute(key, attrs[key]);
            }
            writer.Write("/>"); //09042009 BT - close the input tag 
            writer.Write("<label for='" + clientID + "'>"); //09042009 BT - added label to hold the checkbox text
            writer.Write(Items[repeatIndex].Text);
            writer.Write("</label>"); //close label tag

        }
    }
}
