﻿using System;
 
using MutabakatSERVICE2.WebServiceAskiKiosk;
using MutabakatSERVICE2.WebServiceDenizbankAskiMutabakat;
using MutabakatSERVICE2.WebServiceDenizbankBaskentGazMutabakat;
using PRCNCORE.Utilities;
using InvoicePaymentsResultMessage =
  MutabakatSERVICE2.WebServiceDenizbankBaskentGazMutabakat.InvoicePaymentsResultMessage;

namespace MutabakatSERVICE2
{
  public static class Cancels
  {
    // CANCEL ASKI
    public static int CancelAski(int aboneNo, DateTime makbuzDate, int makbuzNo)
    {
      try
      {
        using (var askiClient = new ServiceSoapClient())
        {
          Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), null, "(CancelAski) :" + aboneNo + " | " + makbuzDate + " | " + makbuzNo);

          var askiResult = askiClient.satisIptal(
            aboneNo,
            makbuzDate,
            makbuzNo,
            1512,
            "DeN5z12!",
            "İptal Edildi");

            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), null, "(CancelAski) : " +
            (askiResult == 1 ? "Success" : "Failure"));

          return askiResult;
        }
      }
      catch (Exception ex)
      {
        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), null, "(CancelAski) Failed to get results from the API.");
        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), null, "(CancelAski) " + ex.Message);
        return -1;
      }
    }

    // CANCEL DENIZBANK BGAZ
    public static InvoicePaymentsResultMessage CancelDenizbankBaskentGaz(string bankReferenceNumber, string vkonto)
    {
      try
      {
        using (var denizbankBaskentGazClient = new BaskentDogalgazCardLoadSoapClient())
        {


          //Logger.LoggerAdd("(CancelDenizbankBaskentGaz) :" + bankReferenceNumber + " | " + vkonto);

          var denizbankBaskentGazResult = denizbankBaskentGazClient.BaskentSatisIptal(bankReferenceNumber, vkonto);


          //Logger.LoggerAdd(
          //  "(CancelDenizbankBaskentGaz) : " +
          //  (denizbankBaskentGazResult?.ErrorCode == 1 ? "Success" : "Failure"));

          return denizbankBaskentGazResult;
        }
      }
      catch (Exception ex)
      {
        //Logger.LoggerAdd("(CancelDenizbankBaskentGaz) Failed to get results from the API.");
        //Logger.LoggerAdd("(CancelDenizbankBaskentGaz) " + ex.Message);
        return null;
      }
    }

    // CANCEL DENIZBANK ASKI
    public static WebServiceDenizbankAskiMutabakat.InvoicePaymentsResultMessage CancelDenizbankAski(
      string bankReferenceNumber)
    {
      try
      {
        using (var denizbankAskiClient = new AskiCardLoadSoapClient())
        {
          //Logger.LoggerAdd("(CancelDenizbankAski) :" + bankReferenceNumber);

          var denizbankAskiResult = denizbankAskiClient.AskiCreditLoadingConfirmationCancel(bankReferenceNumber);

          //Logger.LoggerAdd(
          //  "(CancelDenizbankAski) : " +
          //  (denizbankAskiResult?.ErrorCode == 1 ? "Success" : "Failure"));

          return denizbankAskiResult;
        }
      }
      catch (Exception ex)
      {
        //Logger.LoggerAdd("(CancelDenizbankAski) Failed to get results from the API.");
        //Logger.LoggerAdd("(CancelDenizbankAski) " + ex.Message);
        return null;
      }
    }
  }
}