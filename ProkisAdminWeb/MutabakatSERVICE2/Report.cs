﻿using System;
 
using MutabakatSERVICE2.WebServiceAskiKiosk;
using MutabakatSERVICE2.WebServiceDenizbankBaskentGazMutabakat;
using PRCNCORE.Utilities;

namespace MutabakatSERVICE2
{
  public static class Reports
  {
    // REPORT ASKI
    public static Muhasebe ReportAskiMutabakat(
      DateTime dateDate,
      decimal kioskTutar,
      int kioskAdet,
      decimal iptalTutar,
      int iptalAdet)
    {
      try
      {
        using (var askiClient = new ServiceSoapClient())
        {
          var askiResult = askiClient.satisMuhasebelestirme(
            dateDate,
            1512,
            "DeN5z12!",
            (float) kioskTutar,
            kioskAdet,
            (float) iptalTutar,
            iptalAdet);

          return askiResult;
        }
      }
      catch (Exception ex)
      {
        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), null, "(ReportAskiMutabakat) Failed to get results from the API.");
        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), null, "(ReportAskiMutabakat) " + ex.Message);
        return null;
      }
    }

    // REPORT BGAZ
    public static DT_BI_ES_KurumIslemListe_Res ReportDenizbankBaskentGaz(
      DateTime dateDate,
      decimal kioskTutar,
      int kioskAdet,
      decimal iptalTutar,
      int iptalAdet)
    {
      try
      {
        using (var denizbankBaskentGazClient = new BaskentDogalgazCardLoadSoapClient())
        {
          var denizbankBaskentGazResult = denizbankBaskentGazClient.MutabakatSaglamaGunSonuIslemleriSAPMutabakat(
            dateDate,
            "700",
            kioskAdet,
            kioskTutar,
            iptalAdet,
            iptalTutar);

          return denizbankBaskentGazResult;
        }
      }
      catch (Exception ex)
      {
        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), null, "(ReportDenizbankBaskentGaz) Failed to get results from the API.");
        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), null, "(ReportDenizbankBaskentGaz) " + ex.Message);
        return null;
      }
    }
  }
}