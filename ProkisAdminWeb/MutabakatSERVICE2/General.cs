﻿//using System.Collections.Generic;
 
//using MutabakatSERVICE2.ServiceSendMail;
//using PRCNCORE.Utilities;

//namespace MutabakatSERVICE2
//{
//  public static class General
//  {
//    public static void SendMail(
//      List<string> sendToAdresses,
//      string message,
//      string senderEmailAddress = "info@birlesikodeme.com",
//      string senderName = "Mutabakat Servisi",
//      string subject = "Mutabakat Raporu")
//    {
//      if (sendToAdresses == null || message == null)
//      {
//        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), null, "(SendMail) No mail or message for mail");
//        return;
//      }

//      using (var client = new SendEmailSoapClient())
//      {
//        //Logger.LoggerAdd("(SendMail) Mail to be sent");
//        //Logger.LoggerAdd(message);

//        var sendees = new ArrayOfString();
//        foreach (var address in sendToAdresses) sendees.Add(address);

//        var result = client.SendEmailTo(senderEmailAddress, senderName, sendees, subject, message, null);

//        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), null, "(SendMail) Mail result :" + result);
//      }
//    }
//  }
//}