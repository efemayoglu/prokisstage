﻿using System;
using System.Data;
using System.Globalization;
using System.Net;
using System.Security.Principal;
using System.Threading;
using MutabakatSERVICE2.WebReferenceAkbank1;
using MutabakatSERVICE2.WebServiceAskiMutabakat;
using MutabakatSERVICE2.WebServiceBaskentGaz;
using MutabakatSERVICE2.WebServiceDenizbankAskiMutabakat;
using MutabakatSERVICE2.WebServiceDenizbankBaskentGazMutabakat;
using MutabakatSERVICE2.WebServiceFibaBGaz;
using MutabakatSERVICE2.WebServiceMaskiMutabakat;
using DT_BI_ES_KurumIslemListe_Res =
  MutabakatSERVICE2.WebServiceDenizbankBaskentGazMutabakat.DT_BI_ES_KurumIslemListe_Res;
using PN_AssociationReconciliationInfo =
  MutabakatSERVICE2.WebServiceDenizbankAskiMutabakat.PN_AssociationReconciliationInfo;
using MutabakatSERVICE2.TeskiWaterSaleMngmnt;
using System.IO;
using PRCNCORE.Utilities;
 

namespace MutabakatSERVICE2
{
    public static class GetData
    {
        private const string FibaUser = "BNKFIBA";
        private const string FibaPass = "Aa1234**";

        // ASKI
        public static DataTable GetAskiMutabakat(DateTime reconciliationDate)
        {
            for (var i = 0; i < 2; i++)
            {
                try
                {
                    using (var askiClient = new KartliServiceSoapClient())
                    {
                        var startDate = reconciliationDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        var endDate = reconciliationDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                        var askiResult = askiClient.SatisListesi("bodeme", "b@odeme1453", startDate, endDate);


                         Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(),  "Successfully got results from the API.");

                        //reconciliationDate.ToShortDateString(),
                        //reconciliationDate.AddDays(1).ToShortDateString());
                        return askiResult;
                    }
                }
                catch (Exception ex)
                {
                   Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("",ex), "Failed to get results from the API.");
                  Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), ex.Message);
                }

                if (i == 0) Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(),  "Retrying...");
            }

            return null;
        }

        // DENIZBANK ASKI
        public static PN_AssociationReconciliationInfo GetDenizbankAskiMutabakat(
          DateTime reconciliationDate,
          bool getDetails)
        {
            for (var i = 0; i < 2; i++)
            {
                try
                {
                    using (var denizbankAskiClient = new AskiCardLoadSoapClient())
                    {
                        var askiResult = denizbankAskiClient.AskiCreditLoadingConfirmationReconciliation(
                          reconciliationDate,
                          getDetails ? "D" : "G");

                        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Successfully got results from the API.");
                        return askiResult;
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("",ex), "Failed to get results from the API.");
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), ex.Message);
                }

                if (i == 0) Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Retrying...");
            }

            return null;
        }

        // BGAZ
        public static DT_BI_ES_KurumIslemListe_Res GetBaskentGazOnDenizMutabakat(
          DateTime reconciliaitonDate,
          bool getDetails)
        {
            for (var i = 0; i < 2; i++)
            {
                try
                {
                    using (var denizbankBaskentGazClient = new BaskentDogalgazCardLoadSoapClient())
                    {
                        var baskentGazResult = denizbankBaskentGazClient.MutabakatSaglamaGunSonuIslemleriSAPRapor(
                          reconciliaitonDate,
                          getDetails ? "X" : "",
                          "700");


                        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Successfully got results from the API.");
                        return baskentGazResult;
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("",ex), "Failed to get results from the API.");
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), ex.Message);
                }

                if (i == 0) Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Retrying...");
            }

            return null;
        }

        // DENIZBANK BGAZ
        public static WebServiceDenizbankBaskentGazMutabakat.PN_AssociationReconciliationInfo
          GetDenizbankBaskentGazMutabakat(DateTime reconciliationDate, bool getDetails)
        {
            for (var i = 0; i < 2; i++)
            {
                try
                {
                    using (var denizbankBaskentGazClient = new BaskentDogalgazCardLoadSoapClient())
                    {
                        var baskentGazResult =
                          denizbankBaskentGazClient.BaskentLoadingConfirmationReconciliation(
                            reconciliationDate,
                            getDetails ? "D" : "G");

                        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Successfully got results from the API.");
                        return baskentGazResult;
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), ex, "Failed to get results from the API.");
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), ex.Message);
                }

                if (i == 0) Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Retrying...");
            }

            return null;
        }

        // MASKI
        public static ParserEODDetail GetMaskiMutabakat(DateTime startDate, int processType, decimal kioskId)
        {
            for (var i = 0; i < 2; i++)
            {
                try
                {
                    using (var maskiClient = new CardTransactionSoapClient())
                    {
                        var maskiResult = maskiClient.GetEODDetails(startDate, startDate, processType, 9999, "bostest", kioskId);
                        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Successfully got results from the API.");

                        return maskiResult;
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Failed to get results from the API.");
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("",ex), ex.Message);
                }

                if (i == 0) Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Retrying...");
            }

            return null;
        }

        // TESKI
        public static queryTransactionCreditResponseType GetTeskiMutabakat(DateTime startDate)
        {
            using (var teskiClient = new AkosWaterSaleMngmntServiceClient())
            {
                //startDate = DateTime.ParseExact("2018-02-23", "yyyy-MM-dd", CultureInfo.InvariantCulture);

                var teskiResult = teskiClient.queryTransactionCredit(
                  new queryTransactionCreditRequestType
                  {
                      durum = "1",
                      ilktarih = startDate,
                      ilktarihSpecified = true,
                      sayacfirma = "10",
                      sontarih = startDate.AddDays(1),
                      sontarihSpecified = true
                  });

                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Successfully got results from the API.");

                return teskiResult;
            }
        }


        // AKBANK
        public static DetailReconciliationResponse GetAkbankMutabakatDetail(
          DateTime reconciliationDate,
          AkbankKurum firmaKodu)
        {

            var auth = new Authentication
            {
                UserName = "URF_1011",
                Password = "gaKyoAJ3tCuqQZ5fNDcm" //"dRxyRbUHH71mdMplTu0Y"
            };

            var detail =
              new DetailReconciliationRequest
              {
                  BasTarihi = reconciliationDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture),
                  BitTarihi = reconciliationDate
                            .AddDays(1)
                            .ToString("yyyy-MM-dd", CultureInfo.InvariantCulture),
                  FirmaKoduSpecified = true
              };

            switch (firmaKodu)
            {
                case AkbankKurum.Aski:
                    detail.FirmaKodu = 7000449; //7000724
                    break;
                case AkbankKurum.BGaz:
                    detail.FirmaKodu = 6401000;
                    break;
            }

            for (var i = 0; i < 2; i++)
            {
                try
                {
                    using (var akbankAskiClient = new CollectionService())
                    {

                        var tempProtocol = ServicePointManager.SecurityProtocol;
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                        
                        var askiResult = akbankAskiClient.NYDetailReconciliation(
                          auth,
                          detail);

                        ServicePointManager.SecurityProtocol = tempProtocol;

                        return askiResult;
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), ex, ex.Message);
                }

                if (i == 0) Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Retrying...");
                Thread.Sleep(100);
            }

            return null;
        }

        public static DetailReconciliationResponse GetAkbankMutabakatDetail(
  DateTime reconciliationDate,
  AkbankKurum firmaKodu, int a)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Authentication int a.");
            return null;
        }

        // FIBA BGAZ
        public static DT_BI_ES_KurumIslemListe_K_Res GetBaskentGazOnFibaMutabakat(
          DateTime reconciliationDate,
          bool getDetails)
        {
            for (var i = 0; i < 2; i++)
            {
                try
                {
                    using (var fibaBaskentGazClient = new SI_BI_ES_KurumIslemListe_K_OBClient())
                    {
                        fibaBaskentGazClient.ClientCredentials.Windows.ClientCredential =
                          new System.Net.NetworkCredential(FibaUser, FibaPass);
                        fibaBaskentGazClient.ClientCredentials.Windows.AllowedImpersonationLevel =
                          TokenImpersonationLevel.Identification;
                        fibaBaskentGazClient.ClientCredentials.UserName.UserName = FibaUser;
                        fibaBaskentGazClient.ClientCredentials.UserName.Password = FibaPass;


                        var request = new DT_BI_ES_KurumIslemListe_K_Req
                        {
                            IV_DATUM = reconciliationDate.ToString("yyyy-MM-dd"),
                            IV_DETAY = getDetails ? "X" : "",
                            IV_ISLEM = "1",
                            IV_KURUM = "900"
                        };

                        var result = fibaBaskentGazClient.SI_BI_ES_KurumIslemListe_K_OB(request);

                        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Successfully got results from the API.");
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("",ex), "Failed to get results from the API.");
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), ex.Message);
                }

                if (i == 0) Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Retrying...");
            }

            return null;
        }

        public static object GetBaskentGazMutabakat(
          DateTime reconcilationDate,
          bool getDetails)
        {
            var request = new DT_BI_ES_KurumIslemListe_Req();


            for (var i = 0; i < 2; i++)
            {
                try
                {
                    using (var baskentGazClient = new SI_BI_ES_KurumIslemListe_OBClient())
                    {
                        var bgazResult = baskentGazClient.SI_BI_ES_KurumIslemListe_OB(request);

                        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Successfully got results from the API.");

                        //Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), null, "Successfully got results from the API.");
                        return bgazResult;
                    }
                }
                catch (Exception ex)
                {


                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("",ex), "Failed to get results from the API.");
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), ex.Message);
                }

                if (i == 0) Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Retrying...");
            }

            return null;
        }


        public enum AkbankKurum
        {
            Aski = 1,
            BGaz = 2
        }
    }
}
