﻿<%@ page language="C#" autoeventwireup="true" inherits="UserRole_ShowApproveUserRole, App_Web_jxxvv3ti" enableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />

    <script language="javascript" type="text/javascript">

        function checkForm() {
            var status = true;
            var messageText = "";

            if (document.getElementById('nameField').value == "") {
                status = false;
                messageText = "Rol Adı giriniz.";
            }
            else if (document.getElementById('descriptionField').value == "") {
                status = false;
                messageText = "Rol Açıklaması giriniz.";
            }
            else {
                messageText = "";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }
    </script>
    <base target="_self" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel7" DefaultButton="cancelButton" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <div align="left" id="brandDetailsTab" class="windowTitle_container_autox30">
                    Rol Onaylama Detayı
                </div>
                <br />
                <div align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Panel ID="userRoleListPanel" runat="server" CssClass="containerPanel_95Pxauto">
                                    <table border="0" cellpadding="2" cellspacing="0" id="Table3" runat="server">
                                        <tr valign="middle">
                                            <td align="left" class="staticTextLine_200x20">Eski Role Adı :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="oldnameField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr valign="middle">
                                            <td align="left" class="staticTextLine_200x20">Eski Rol Açıklama :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="olddescriptionField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <div align="left" class="windowTitle_container_autox30">
                                        Eski Menüler<hr style="border-bottom: 1px solid #b2b2b4;" />
                                    </div>
                                    <table cellpadding="0" cellspacing="0" width="95%" id="upTable" runat="server">
                                        <tr>
                                            <td align="center" class="tableStyle1">
                                                <asp:UpdatePanel ID="userRoleListUPanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" RenderMode="Block">
                                                    <ContentTemplate>
                                                        <asp:Table ID="userRolesTable" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                                            BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="95%">
                                                            <asp:TableRow CssClass="inputTitleCell3" Style="border-color: White; border-width: 1px; border-style: Double;">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Alt Menüler</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Görüntüle</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Güncelleme</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Ekleme</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Silme</asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m1">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kullanıcılar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_1" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_1" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_1" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_1" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m2">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Roller</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_2" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_2" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_2" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_2" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m3">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosklar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_3" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_3" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_3" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_3" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m4">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Şifre Değiştirme</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_4" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_4" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_4" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_4" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m5">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">İşlemler</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_5" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_5" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_5" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_5" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m6">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Müşteriler</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_6" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_6" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_6" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_6" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m7">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Komutları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_7" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_7" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_7" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_7" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m8">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Durumu</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_8" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_8" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_8" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_8" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m9">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Hesaplar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_9" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_9" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_9" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_9" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m10">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Aski Kodlar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_10" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_10" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_10" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_10" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m11">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Toplanan Para</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_11" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_11" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_11" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_11" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m12">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Toplanan Para Girişi</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_12" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_12" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_12" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_12" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m13">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Toplanan Para Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_13" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_13" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_13" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_13" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m14">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Mutabakat</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_14" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_14" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_14" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_14" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m15">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Raporları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_15" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_15" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_15" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_15" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m16">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Rol Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_16" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_16" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_16" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_16" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m17">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">E-Cüzdan Müşterileri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_17" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_17" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_17" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_17" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m18">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kullanıcı Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_18" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_18" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_18" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_18" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="m19">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">E-Cüzdan İşlemleri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_19" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_19" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_19" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_19" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m20">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Hesap Hareketleri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_20" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_20" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_20" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_20" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m21">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Fraud İşlem Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_21" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_21" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_21" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_21" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m22">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Fraud İşlemler</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_22" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_22" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_22" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_22" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m23">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Müşteri Black List Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_23" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_23" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_23" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_23" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m24">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Black List</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_24" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_24" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_24" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_24" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m25">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Günlük Hesap Raporları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_25" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_25" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_25" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_25" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m26">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Günlük Prokis Raporları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_26" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_26" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_26" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_26" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m27">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Manuel Kiosk Boşaltma</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_27" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_27" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_27" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_27" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m28">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Para Doldurma</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_28" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_28" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_28" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_28" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m29">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Geriye Dönük Para Kod Sorgulama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_29" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_29" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_29" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_29" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m30">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Kurum Mutabakat</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_30" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_30" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_30" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_30" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="m31">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Tarife İşlem Durumu</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="view_31" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="edit_31" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="add_31" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="delete_31" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:Panel ID="Panel1" runat="server" CssClass="containerPanel_95Pxauto">
                                    <table border="0" cellpadding="2" cellspacing="0" id="Table4" runat="server">
                                        <tr valign="middle">
                                            <td align="left" class="staticTextLine_200x20">Yeni Role Adı :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="newnameField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr valign="middle">
                                            <td align="left" class="staticTextLine_200x20">eni Rol Açıklama :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="newdescriptionField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <div align="left" class="windowTitle_container_autox30">
                                        Yeni Menüler<hr style="border-bottom: 1px solid #b2b2b4;" />
                                    </div>
                                    <table cellpadding="0" cellspacing="0" width="95%" id="Table1" runat="server">
                                        <tr>
                                            <td align="center" class="tableStyle1">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" RenderMode="Block">
                                                    <ContentTemplate>
                                                        <asp:Table ID="Table2" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                                            BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="95%">
                                                            <asp:TableRow CssClass="inputTitleCell3" Style="border-color: White; border-width: 1px; border-style: Double;">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Alt Menüler</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Görüntüle</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Güncelleme</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Ekleme</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4">Silme</asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om1">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kullanıcılar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_1" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_1" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_1" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_1" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om2">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Roller</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_2" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_2" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_2" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_2" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om3">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosklar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_3" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_3" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_3" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_3" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om4">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Şifre Değiştirme</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_4" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_4" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_4" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_4" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om5">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">İşlemler</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_5" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_5" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_5" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_5" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om6">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Müşteriler</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_6" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_6" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_6" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_6" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om7">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Komutları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_7" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_7" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_7" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_7" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om8">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Durumu</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_8" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_8" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_8" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_8" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om9">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Hesaplar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_9" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_9" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_9" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_9" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om10">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Aski Kodlar</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_10" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_10" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_10" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_10" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om11">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Toplanan Para</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_11" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_11" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_11" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_11" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om12">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Toplanan Para Girişi</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_12" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_12" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_12" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_12" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om13">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Toplanan Para Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_13" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_13" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_13" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_13" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om14">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Mutabakat</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_14" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_14" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_14" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_14" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om15">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Raporları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_15" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_15" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_15" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_15" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om16">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Rol Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_16" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_16" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_16" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_16" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om17">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">E-Cüzdan Müşterileri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_17" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_17" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_17" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_17" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om18">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kullanıcı Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_18" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_18" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_18" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_18" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listrow" ID="om19">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">E-Cüzdan İşlemleri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_19" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_19" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_19" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_19" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om20">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Hesap Hareketleri</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_20" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_20" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_20" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_20" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om21">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Fraud İşlem Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_21" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_21" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_21" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_21" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om22">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Fraud Islemler</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_22" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_22" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_22" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_22" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om23">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Müşteri Black List Onaylama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_23" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_23" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_23" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_23" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om24">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Black List</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_24" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_24" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_24" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_24" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om25">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Günlük Hesap Raporları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_25" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_25" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_25" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_25" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om26">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Günlük Prokis Raporları</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_26" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_26" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_26" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_26" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om27">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Manuel Kiosk Boşaltma</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_27" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_27" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_27" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_27" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om28">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Para Doldurma</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_28" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_28" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_28" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_28" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om29">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Geriye Dönük Para Kod Sorgulama</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_29" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_29" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_29" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_29" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow CssClass="listRowAlternate" ID="om30">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Kiosk Kurum Mutabakat</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_30" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_30" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_30" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_30" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                               <asp:TableRow CssClass="listRowAlternate" ID="om31">
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 400px; padding-left: 5px;">Tarife İşlem Durumu</asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oview_31" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oedit_31" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="oadd_31" />
                                                                </asp:TableHeaderCell>
                                                                <asp:TableHeaderCell CssClass="inputTitleCell4" Style="width: 50px; padding-left: 5px;">
                                                                    <asp:CheckBox runat="server" ID="odelete_31" />
                                                                </asp:TableHeaderCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>

                </div>
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="cancelButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClientClick="parent.hideModalPopup2();" Text="Kapat"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </td>
            </asp:Panel>
        </div>

    </form>
</body>
</html>
