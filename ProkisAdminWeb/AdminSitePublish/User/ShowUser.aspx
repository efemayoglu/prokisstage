﻿<%@ page language="C#" autoeventwireup="true" inherits="User_ShowUser, App_Web_cdwvuaxy" enableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />
    <input type="hidden" id="hiddenUserId" runat="server" name="hiddenUserId" value="0" />
    <script language="javascript" type="text/javascript">

        function checkForm() {
            var status = true;
            var messageText = "";

            if (document.getElementById('nameField').value == "") {
                status = false;
                messageText = "Adı Soyadı Giriniz.";
            }
            else if (document.getElementById('userNameField').value == "") {
                status = false;
                messageText = "Kullanıcı Adı giriniz.";
            }
            else if (document.getElementById('mailField').value == "") {
                status = false;
                messageText = "E-Mail Adresini Giriniz.";
            }
            else if (ValidateEmail(document.getElementById('mailField').value) == false) {
                status = false;
                messageText = "Geçerli Bir E-Mail Adresi Giriniz.";
            }
            else if (document.getElementById('passwordField').value == "") {
                if (document.getElementById('hiddenUserId').value == "0") {
                    status = false;
                    messageText = "Kullanıcı Şifresi Üretiniz.";
                }
            }
            else {
                messageText = "";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }

        function ValidateEmail(mail) {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
                return true;
            }
            return false;
        }
    </script>
    <base target="_self" />
    <style type="text/css">
        .auto-style1 {
            height: 20px;
        }
        .auto-style14 {
            font-family: Tahoma;
            font-size: 12px;
            font-weight: bold;
            width: 190px;
            height: 20px;
            border-radius: 5px;
            border: 1px solid #2372BE;
            padding-bottom: 5px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel7" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <div align="left" id="brandDetailsTab" class="windowTitle_container_autox30">
                    Kullanıcı Detayı<hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>
                <table border="0" cellpadding="2" cellspacing="0" id="Table1" runat="server">
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Adı Soyadı:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="nameField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Kullanıcı Adı:
                        </td>
                        <td align="left" class="auto-style1">
                            <asp:TextBox ID="userNameField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Maili:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="mailField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Şifre:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="passwordField" runat="server" CssClass="inputLine_285x20"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="genPassButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                OnClick="generatePassButton_Click" Text="Üret"></asp:Button>
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Cep Telefonu:
                        </td>
                        <td align="left" class="auto-style1">
                            <asp:TextBox ID="cellPhoneField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Telefonu:
                        </td>
                        <td align="left" class="auto-style1">
                            <asp:TextBox ID="telephoneField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="staticTextLine_200x20">Kullanıcı Durumu:
                        </td>
                        <td align="left" class="auto-style2">
                            <asp:ListBox ID="userStatusBox" CssClass="inputLine_150x20" runat="server" SelectionMode="Single" AutoPostBack="True"
                                DataTextField="Name" DataValueField="Id" Rows="1"></asp:ListBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="staticTextLine_200x20">Kullanıcı Rolü:
                        </td>
                        <td align="left" class="auto-style2">
                            <asp:ListBox ID="userRoleBox" CssClass="inputLine_150x20" runat="server" SelectionMode="Single" AutoPostBack="True"
                                DataTextField="Name" DataValueField="Id" Rows="1"></asp:ListBox>
                        </td>
                    </tr>
                </table>
                <br />
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Kaydet"></asp:Button>
                                <asp:Button ID="Button1" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClientClick="parent.hideModalPopup2();" Text="Vazgeç"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </td>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
