﻿<%@ page language="C#" inherits="User_GenerateUserPassword, App_Web_cdwvuaxy" enableEventValidation="false" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <title>::KIOSK YÖNETİM PANELİ::</title>
    <link rel="stylesheet" href="../styles/style.css" />
    <script language="javascript" type="text/javascript" src="../js/scripts-ie.js"></script>
    <script language="javascript" type="text/javascript">
		<!--
    function callDefault() {
        alert("Password has been changed successfully!");
        window.location = "../root/Default.aspx";

    }
    //-->
    </script>
    <base target="_self" />
</head>
<!--#F1EEE8-->
<body>
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <div align="center">
            <asp:Panel ID="panel1" DefaultButton="saveButton" runat="server" CssClass="containerPanel_95Pxauto_noShadow">
                <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp</div>
                <div align="left" class="windowTitle_container_autox30">
                    Yeni Şifre Oluşturma<hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>
                <table cellspacing="0" cellpadding="0" border="0" id="upTable" runat="server" style="width: 90%">
                    <tr>
                        <td align="center" valign="top">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table>
                                <tr>
                                    <td>

                                        <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                            OnClick="saveButton_Click" Text="Oluştur"></asp:Button>
                                        </td>
                                    <td>
                                        <asp:Button ID="cancelButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                            OnClientClick="parent.hideModalPopup2();" Text="Vazgeç"></asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
