﻿<%@ page language="C#" autoeventwireup="true" inherits="User_ChangePasswordLogin, App_Web_cdwvuaxy" enableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />
    <script language="javascript" type="text/javascript" src="../js/wdws.js"></script>
    <script language="javascript" type="text/javascript" src="../js/scripts-ie.js"></script>
    <script language="javascript" type="text/javascript">

		<!--
    function callDefault() {
        alert("Password has been changed successfully!");
        window.location = "../root/Default.aspx";

    }

    function callMessageWindowPwd(message) {
        var retVal = showAlertWindowPwd(message);
    }
    var anUpperCase = /[A-Z]/;
    var aLowerCase = /[a-z]/;
    var aNumber = /[0-9]/;
    var aSpecial = /[!|@|#|$|%|^|&|*|(|)|-|_]/;

    function checkPasswordComplexity(passValue) {
        if (passValue.length < 8 || passValue.search(anUpperCase) == -1 ||
            passValue.search(aLowerCase) == -1 || passValue.search(aNumber) == -1 || passValue.search(aSpecial) == -1) {
            resp = false;
        }
        else {
            resp = true;
        }
        return resp;
    }

    function checkForm() {
        var retVal = prepareFormForSubmitting();

        return retVal;
    }

    function prepareFormForSubmitting() {
        var status = true;
        var message = "";

        if (document.getElementById('oldPasswordField').value == "") {
            status = false;
            messageText = "Lütfen Eski Şifreyi giriniz !";
        }
        else if (document.getElementById('passwordField1').value.length < 8) {
            status = false;
            message = "Şifreniz enaz 8 karakter olmalıdır !";
        }
        else if (document.getElementById('passwordField1').value != document.getElementById('passwordField2').value) {
            status = false;
            message = "Şifreler eşleşmiyor, kontrol ediniz !";
        }
        else if (checkPasswordComplexity(document.getElementById('passwordField1').value) == false) {
            status = false;
            message = "Şifre büyükharf, küçük harf, rakam ve özel karakter içermelidir !";
        }
        document.getElementById('messageArea').innerHTML = message;
        return status;
    }
    //-->
    </script>
    <base target="_self" />
</head>
<!--#F1EEE8-->
<body>
    <form id="Form1" method="post" runat="server" onsubmit="return prepareFormForSubmitting();">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <div align="center">
            <div>&nbsp;</div>
            <asp:Panel ID="panel1" runat="server" CssClass="containerPanel_350Pxauto">
                <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp</div>
                <div align="left" class="windowTitle_container_autox30">Change Password<hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>
                            <table cellpadding="2" cellspacing="2" width="100%">
                                <tr>
                                    <td align="left" class="auto-style13">Eski Şifre:
                                    </td>
                                    <td align="left" class="auto-style10">
                                        <asp:TextBox ID="oldPasswordField" CssClass="inputLine_150x20" runat="server" TextMode="Password"
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                 <tr>
                                    <td align="left" class="auto-style13">Yeni Şifre:
                                    </td>
                                    <td align="left" class="auto-style10">
                                        <asp:TextBox ID="passwordField1" CssClass="inputLine_150x20" runat="server" TextMode="Password"
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" class="auto-style13">Yeni Şifre (Tekrar):
                                    </td>
                                    <td align="left" class="auto-style10">
                                        <asp:TextBox ID="passwordField2" CssClass="inputLine_150x20" runat="server" TextMode="Password"
                                            ClientIDMode="Static"></asp:TextBox>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style12">&nbsp;</td>
                                    <td class="auto-style10" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                                OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Kaydet"></asp:Button>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
            </asp:Panel>
        </div>
    </form>
</body>
</html>