﻿<%@ page language="C#" autoeventwireup="true" inherits="User_ShowApproveUser, App_Web_cdwvuaxy" enableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />
    <input type="hidden" id="hiddenUserId" runat="server" name="hiddenUserId" value="0" />

    <base target="_self" />
    <style type="text/css">
        .auto-style1 {
            height: 20px;
        }

        .auto-style14 {
            font-family: Tahoma;
            font-size: 12px;
            font-weight: bold;
            width: 190px;
            height: 20px;
            border-radius: 5px;
            border: 1px solid #2372BE;
            padding-bottom: 5px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel2" DefaultButton="cancelButton" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <div align="left" id="Div2" class="windowTitle_container_autox30">
                    Kullanıcı Bilgileri Detayı
                </div>
                <br />
                <table>
                    <tr>
                        <td>
                            <asp:Panel ID="panel7" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto">

                                <div align="left" id="brandDetailsTab" class="windowTitle_container_autox30">
                                    Eski Kullanıcı Bilgileri<hr style="border-bottom: 1px solid #b2b2b4;" />
                                </div>
                                <table border="0" cellpadding="2" cellspacing="0" id="Table1" runat="server">
                                    <tr valign="middle">
                                        <td align="left" class="staticTextLine_200x20">Adı Soyadı:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="nameField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr valign="middle">
                                        <td align="left" class="staticTextLine_200x20">Kullanıcı Adı:
                                        </td>
                                        <td align="left" class="auto-style1">
                                            <asp:TextBox ID="userNameField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr valign="middle">
                                        <td align="left" class="staticTextLine_200x20">Maili:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="mailField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr valign="middle">
                                        <td align="left" class="staticTextLine_200x20">Cep Telefonu:
                                        </td>
                                        <td align="left" class="auto-style1">
                                            <asp:TextBox ID="cellPhoneField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr valign="middle">
                                        <td align="left" class="staticTextLine_200x20">Telefonu:
                                        </td>
                                        <td align="left" class="auto-style1">
                                            <asp:TextBox ID="telephoneField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="staticTextLine_200x20">Kullanıcı Durumu:
                                        </td>
                                        <td align="left" class="auto-style1">
                                            <asp:TextBox ID="statusField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="staticTextLine_200x20">Kullanıcı Rolü:
                                        </td>
                                        <td align="left" class="auto-style1">
                                            <asp:TextBox ID="roleField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                        <td>
                            <asp:Panel ID="panel1" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto">

                                <div align="left" id="Div1" class="windowTitle_container_autox30">
                                    Yeni Kullanıcı Bilgileri<hr style="border-bottom: 1px solid #b2b2b4;" />
                                </div>
                                <table border="0" cellpadding="2" cellspacing="0" id="Table2" runat="server">
                                    <tr valign="middle">
                                        <td align="left" class="staticTextLine_200x20">Adı Soyadı:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="newNameField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr valign="middle">
                                        <td align="left" class="staticTextLine_200x20">Kullanıcı Adı:
                                        </td>
                                        <td align="left" class="auto-style1">
                                            <asp:TextBox ID="newUsernameField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr valign="middle">
                                        <td align="left" class="staticTextLine_200x20">Maili:
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="newEmailField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr valign="middle">
                                        <td align="left" class="staticTextLine_200x20">Cep Telefonu:
                                        </td>
                                        <td align="left" class="auto-style1">
                                            <asp:TextBox ID="newCellphoneField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr valign="middle">
                                        <td align="left" class="staticTextLine_200x20">Telefonu:
                                        </td>
                                        <td align="left" class="auto-style1">
                                            <asp:TextBox ID="newPhoneField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="staticTextLine_200x20">Kullanıcı Durumu:
                                        </td>
                                        <td align="left" class="auto-style1">
                                            <asp:TextBox ID="newStatusField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="staticTextLine_200x20">Kullanıcı Rolü:
                                        </td>
                                        <td align="left" class="auto-style1">
                                            <asp:TextBox ID="newRoleField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="cancelButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                OnClientClick="parent.hideModalPopup2();" Text="Kapat"></asp:Button>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
