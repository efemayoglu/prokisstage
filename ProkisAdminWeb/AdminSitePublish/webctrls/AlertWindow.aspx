﻿<%@ page language="C#" autoeventwireup="true" inherits="webctrls_AlertWindow, App_Web_xyzbhd0f" enableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />

    <script language="javascript" type="text/javascript">
    </script>
    <base target="_self" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel7" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <br />
                <br />
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="okButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                OnClick="okButton_Click" Text="Tamam"></asp:Button>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
