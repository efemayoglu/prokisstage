﻿<%@ page language="C#" autoeventwireup="true" inherits="Customer_ShowBlackList, App_Web_3yb3ylem" enableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />
    <input type="hidden" id="hiddenUserId" runat="server" name="hiddenUserId" value="0" />
    <script language="javascript" type="text/javascript">

        function addButtonClicked() {
            var retVal = showBLWindow();
        }

        function checkForm() {
            var status = true;
            var messageText = "";

            if (document.getElementById('nameField').value == "") {
                status = false;
                messageText = "Adı Soyadı Giriniz.";
            }
            else if (document.getElementById('tcknField').value == "") {
                status = false;
                messageText = "TCKN giriniz.";
            }
            else if (document.getElementById('birthYearField').value == "") {
                status = false;
                messageText = "Doğum Yılı Giriniz.";
            }
            else if (document.getElementById('descriptionField').value == "") {
                status = false;
                messageText = "Açıklama Giriniz.";
            }
           
            else {
                messageText = "";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }
   
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel7" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <div align="left" id="brandDetailsTab" class="windowTitle_container_autox30">
                    Black List Kişi Bilgileri<hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>
                <table border="0" cellpadding="2" cellspacing="0" id="Table1" runat="server">
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Adı Soyadı:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="nameField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">TCKN:
                        </td>
                        <td align="left" class="auto-style1">
                            <asp:TextBox ID="tcknField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Doğum Yılı:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="birthYearField" CssClass="inputLine_285x20" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr valign="middle">
                        <td align="left" class="staticTextLine_200x20">Açıklama:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="descriptionField" runat="server" CssClass="inputLine_285x20"></asp:TextBox>
                        </td>
                        </tr>                
                </table>
                <br />
                <td align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Kaydet"></asp:Button>
                                <asp:Button ID="Button1" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClientClick="parent.hideModalPopup2();" Text="Vazgeç"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </td>
            </asp:Panel>
        </div>
    </form>
</body>
</html>