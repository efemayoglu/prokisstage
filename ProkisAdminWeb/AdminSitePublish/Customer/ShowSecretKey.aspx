﻿<%@ page language="C#" autoeventwireup="true" inherits="Customer_ShowSecretKey, App_Web_3yb3ylem" enableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />
    <input type="hidden" id="hiddenUserId" runat="server" name="hiddenUserId" value="0" />
    <script language="javascript" type="text/javascript">

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function checkForm() {
            var status = true;
            var messageText = "";

            if (document.getElementById('firstCharacterField').value == '') {
                status = false;
                messageText = "Karakterleri Giriniz !";
            }
            else if (document.getElementById('secondCharacterField').value == '') {
                status = false;
                messageText = "Karakterleri Giriniz !";
            }
            else {

                messageText = " ";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }

        function ValidateAlpha(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /^[a-zA-Z]*$/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
    <base target="_self" />
    <style type="text/css">
        .auto-style14 {
            font-family: Century Gothic;
            font-size: 14px;
            color: #8A8B8C;
            text-decoration: none;
            font-weight: bold;
            padding-bottom: 4px;
            text-align:center;
        }

        .auto-style16 {
            font-family: Century Gothic;
            font-size: 14px;
            color: #8A8B8C;
            text-decoration: none;
            font-weight: bold;
            padding-bottom: 4px;
            text-align: center;
            width: 16px;
        }
        .auto-style17 {
            font-family: Century Gothic;
            font-size: 14px;
            color: #8A8B8C;
            text-decoration: none;
            font-weight: bold;
            padding-bottom: 4px;
            text-align: center;
            width: 18px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel7" DefaultButton="saveButton" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <div align="left" id="brandDetailsTab" class="windowTitle_container_autox30">
                    Gizli Kelime Doğrulama ve Pin Üretme<hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>
                <table border="0" cellpadding="2" cellspacing="0" id="Table1" runat="server">
                    <tr valign="middle">
                        <td align="center" class="auto-style14">
                            <asp:Label ID="Label1" runat="server" style="text-align:center">Gizli kelimenin istenilen harflarini giriniz</asp:Label></td>
                    </tr>

                </table>
                 <table width="100px" > <tr valign="middle">
                        <td align="right" ><asp:Label ID="firstLbl" runat="server"></asp:Label></td>
                        <td align="left" class="auto-style16">
                            <asp:TextBox ID="firstCharacterField" CssClass="inputLine_100x20" runat="server" MaxLength="1" Width="12px" onkeypress='ValidateAlpha(event)'></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                        <td align="right"><asp:Label ID="scndLbl" runat="server"></asp:Label></td>
                        <td align="left" class="auto-style17">
                            <asp:TextBox ID="secondCharacterField" CssClass="inputLine_100x20" runat="server" MaxLength="1" Width="12px" onkeypress='ValidateAlpha(event)'></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr></table>
                          <br />
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Üret" Width="70px"></asp:Button>
                            </td>
                            <td>
                                <asp:Button ID="cancelButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                    OnClientClick="parent.hideModalPopup2();" Text="Vazgeç"></asp:Button>
                            </td>
                            </tr>
                    </table>
                <br />
            </asp:Panel>
        </div>
    </form>
</body>
</html>