﻿<%@ page language="C#" autoeventwireup="true" inherits="Customer_ListBlackList, App_Web_3yb3ylem" masterpagefile="~/webctrls/AdminSiteMPage.master" enableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<%@ Register Namespace="ClickableWebControl" TagPrefix="clkweb" %>
<asp:Content ID="pageContent" ContentPlaceHolderID="mainCPHolder" runat="server">
    <input type="hidden" id="pageNumRefField" runat="server" name="pageNumRefField" value="0" />
    <asp:Button ID="navigateButton" runat="server" OnClick="navigateButton_Click" CssClass="dummy" />
    <asp:Button ID="deleteButton" runat="server" CssClass="dummy" OnClick="deleteButton_Click" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { setInterval(function () { cache_clear() }, 120000); });
        function cache_clear() { window.location.reload(true); }
    </script>
    <script language="javascript" type="text/javascript">

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function deleteButtonClicked(storedProcedure, itemId) {
            var retVal = showDeleteWindowBL(storedProcedure, itemId);
        }

        function approveButtonClicked(storedProcedure, itemId) {
            var retVal = showApproveWindowBL(storedProcedure, itemId);
        }

        function searchButtonClicked(sender) {
            document.getElementById('pageNumRefField').value = "0";
            return true;
        }

        function addButtonClicked() {
            var retVal = showBLWindow();
        }

        function postForPaging(selectedPageNum) {
            document.getElementById('pageNumRefField').value = selectedPageNum;
            document.getElementById('navigateButton').click();
        }

    </script>
    <script type="text/javascript" language="javascript" src="../js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery.tablednd_0_5.js"></script>
    <asp:HiddenField ID="idRefField" runat="server" Value="0" />
    <asp:HiddenField ID="oldOrderNumberField" runat="server" Value="0" />
    <asp:HiddenField ID="newOrderNumberField" runat="server" Value="0" />
    <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>

    <div align="center">
        <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp;</div>
        <asp:Panel ID="panel1" DefaultButton="searchButton" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto">
            <table border="0" cellpadding="0" cellspacing="0" id="Table1" runat="server" width="90%">
                <tr>
                    <td align="left" class="inputTitle" style="width: 78px;">&nbsp;Kullanıcı Ara:
                    </td>
                    <td align="left" style="width: 157px">
                        <asp:TextBox ID="searchTextField" CssClass="inputLine_150x20" runat="server"></asp:TextBox>
                    </td>
                    <td style="width: 6px">&nbsp;</td>
                    <td style="width: 53px">
                        <asp:Button ID="searchButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                            OnClick="searchButton_Click" Text="Ara" OnClientClick="return searchButtonClicked(this);"></asp:Button>
                    </td>
                    <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <div>&nbsp;</div>
    <div align="center">
        <asp:Panel ID="ListPanel" runat="server" CssClass="containerPanel_95Pxauto">
            <div align="left" class="windowTitle_container_autox30">
                Black List<hr style="border-bottom: 1px solid #b2b2b4;" />
            </div>
            <table cellpadding="0" cellspacing="0" width="95%" id="upTable" runat="server">
                <tr>
                    <td align="center" class="tableStyle1">
                        <asp:UpdatePanel ID="ListUPanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" RenderMode="Block">
                            <ContentTemplate>
                                <asp:Table ID="itemsTable" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                    BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="95%">
                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="#"></asp:TableHeaderCell>
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Ad Soyad    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Name" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="TCKN    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Tckn" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Doğum Yılı    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="BirthYear" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Açıklama    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Description" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Oluşturan Kullanıcı    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="CreatedUser" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Oluşturulma Tarihi    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="CreationDate" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Durumu    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Status" />
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text=""></asp:TableHeaderCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="deleteButton" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="navigateButton" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <br />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <br />
    <table width="95%">
        <tr align="right">
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="width: 80px">
                <asp:Label ID="Label5" runat="server" Text="powered by" CssClass="poweredbyTitle"></asp:Label>

            </td>
            <td style="width: 70px">
                <asp:LinkButton ID="LinkButton1" href="http://www.procenne.com/" runat="server" Text="PROCENNE" CssClass="procenneTitle"></asp:LinkButton>

            </td>
        </tr>
    </table>
</asp:Content>
