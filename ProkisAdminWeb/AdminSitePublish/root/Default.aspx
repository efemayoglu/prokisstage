﻿<%@ page language="C#" autoeventwireup="true" inherits="root_Default, App_Web_1jsocvzf" masterpagefile="~/webctrls/AdminSiteMPage.master" enableEventValidation="false" %>

<asp:Content ID="pageContent" ContentPlaceHolderID="mainCPHolder" runat="server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { setInterval(function () { cache_clear() }, 120000); });
        function cache_clear() { window.location.reload(true); }
    </script>
    <div align="center">
        <div>&nbsp;</div>
        <div align="center">
            <asp:Panel ID="ListPanel" runat="server" CssClass="containerPanel_95Pxauto">

                <table cellpadding="0" cellspacing="0" width="95%" id="upTable" runat="server">
                    <tr>

                        <td align="center">
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <div id="bos_log" runat="server" style="overflow: hidden; height: 100px; vertical-align: middle; margin-top: 5px;">
                                <a href="http://www.birlesikodeme.com/" title="BİRLEŞİK ÖDEME">
                                    <img src="../images/bos.bmp" alt="BİRLEŞİK ÖDEME SİSTEMLERİ" border="0" />
                                </a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="bosHeader" runat="server" Width="233px" Text="KİOSK YÖNETİM PANELİ" CssClass="bosTitle"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="Label1" runat="server" Width="233px" Text="Detaylı bilgi için :" CssClass="bosinfoTitle"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="height: 16px">
                            <asp:Label ID="Label2" runat="server" Width="233px" Text="info@birlesikodeme.com" CssClass="bosmailTitle"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                            <br />
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </asp:Panel>
        </div>
    </div>
    <br />
    <table width="95%">
        <tr align="right">
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="width: 80px">
                <asp:Label ID="Label5" runat="server" Text="powered by" CssClass="poweredbyTitle"></asp:Label>

            </td>
            <td style="width: 70px">
                <asp:LinkButton ID="LinkButton1" href="http://www.procenne.com/" runat="server" Text="PROCENNE" CssClass="procenneTitle"></asp:LinkButton>

            </td>
        </tr>
    </table>
</asp:Content>
