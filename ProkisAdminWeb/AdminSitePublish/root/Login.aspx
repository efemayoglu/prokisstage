<%@ page language="c#" inherits="Login, App_Web_1jsocvzf" masterpagefile="~/webctrls/AdminSiteMPage.master" enableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<asp:Content ID="pageContent" runat="server" ContentPlaceHolderID="mainCPHolder">
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <ajaxControlToolkit:DropShadowExtender ID="dropShadowExtender1" runat="server" TargetControlID="loginPanel" Opacity="0.5" Rounded="true" TrackPosition="false" />
    <div align="center">
        <asp:Panel ID="loginPanel" runat="server" CssClass="containerPanel_95Pxauto" DefaultButton="loginButtonLink" Style="width: 450px; background-color: White;" HorizontalAlign="Center">
            <table border="0" cellpadding="4" cellspacing="0" align="center" style="width: inherit; height: auto; background-color: White;" id="loginElementsTable" runat="server">
                <tr>           
                          <td align="center" colspan="4">
                            <div id="bos_log" runat="server" style="overflow: hidden; height: 36px; vertical-align: middle; margin-top: 5px;">
                                <a href="http://www.birlesikodeme.com/" title="BİRLEŞİK ÖDEME">
                                    <img src="../images/bosksk.bmp" alt="BİRLEŞİK ÖDEME SİSTEMLERİ" border="0" />
                                </a>
                            </div>
                        </td>
                    </tr>
                   <tr>
                    <td align="center" colspan="4">
                        <br />
                        <div id="messageArea" align="center" class="loginMessageArea" runat="server">Kullanıcı adı ve şifrenizi giriniz.</div>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td align="center" style="width: 50px">&nbsp;</td>
                    <td align="right" valign="middle" class="inputTitle" style="padding-right: 10px">KULLANICI ADI: </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="usernameField" runat="server" CssClass="inputLine_150x20"></asp:TextBox>
                    </td>
                    <td align="center" style="width: 100px">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" style="width: 50px">&nbsp;</td>
                    <td align="right" class="inputTitle" valign="middle" style="padding-right: 10px">ŞİFRE: </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="passwordField" runat="server" CssClass="inputLine_150x20" TextMode="Password"></asp:TextBox>

                    </td>
                    <td align="center" style="width: 100px">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" colspan="4">
                        <asp:Button ID="loginButtonLink" CssClass="buttonCSSDesign" runat="server"
                            ClientIDMode="Static" OnClick="loginButtonLink_Click" Text="Giriş"></asp:Button>
                        <br />
                        <br />
                    </td>
                </tr>
            </table>
              <br />
                <table width="95%">
                    <tr align="right">
                        <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td style="width: 80px">
                            <asp:Label ID="Label5" runat="server" Text="powered by" CssClass="poweredbyTitle"></asp:Label>

                        </td>
                        <td style="width: 70px">
                            <asp:LinkButton ID="LinkButton1" href="http://www.procenne.com/" runat="server" Text="PROCENNE" CssClass="procenneTitle"></asp:LinkButton>

                        </td>
                    </tr>
                </table>
        </asp:Panel>
    </div>
</asp:Content>
