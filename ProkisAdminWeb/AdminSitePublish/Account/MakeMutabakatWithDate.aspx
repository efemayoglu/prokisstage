﻿<%@ page language="C#" autoeventwireup="true" inherits="Account_MakeMutabakatWithDate, App_Web_xjaoppgl" masterpagefile="~/webctrls/AdminSiteMPage.master" enableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<%@ Register Namespace="ClickableWebControl" TagPrefix="clkweb" %>
<asp:Content ID="pageContent" ContentPlaceHolderID="mainCPHolder" runat="server">
    <input type="hidden" id="pageNumRefField" runat="server" name="pageNumRefField" value="0" />
    <asp:Button ID="navigateButton" runat="server" OnClick="navigateButton_Click" CssClass="dummy" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { setInterval(function () { cache_clear() }, 120000); });
        function cache_clear() { window.location.reload(true); }
    </script>
    <script language="javascript" type="text/javascript">

        function checkForm() {
            var status = true;
            var messageText = "";

            if (document.getElementById('kioskBox').value == "0") {
                if (document.getElementById('instutionBox').value == "2") {
                    status = false;
                    messageText = "Kiosk seçmelisiniz.";
                }
            }
            else if (document.getElementById('instutionBox').value == "0") {
                status = false;
                messageText = "Kurum seçmelisiniz.";
            }
            else {
                messageText = "";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function useBOSValuesClicked(faturaAmount, faturaCount, kartDolumAmount, kartDolumCount, totalCount, totalAmount, instutionCount, instutionAmount, date, kioskId, instutionId) {
            var url = "../webctrls/ApproveMutabakat.aspx?faturaAmount=" + faturaAmount + "&faturaCount=" + faturaCount + "&kartDolumAmount=" + kartDolumAmount + "&kartDolumCount=" + kartDolumCount + "&totalCount=" + totalCount + "&totalAmount=" + totalAmount + "&instutionCount=" + instutionCount + "&instutionAmount=" + instutionAmount + "&date=" + date + "&kioskId=" + kioskId + "&instutionId=" + instutionId + "&WhichSide=0";
            var retValue = openDialoagWindow(url, 350, 150, "", "BOS Verileri ile Mutabakat");
        }

        function useBGazValuesClicked(faturaAmount, faturaCount, kartDolumAmount, kartDolumCount, totalCount, totalAmount, instutionCount, instutionAmount, date, kioskId, instutionId) {
            var url = "../webctrls/ApproveMutabakat.aspx?faturaAmount=" + faturaAmount + "&faturaCount=" + faturaCount + "&kartDolumAmount=" + kartDolumAmount + "&kartDolumCount=" + kartDolumCount + "&totalCount=" + totalCount + "&totalAmount=" + totalAmount + "&instutionCount=" + instutionCount + "&instutionAmount=" + instutionAmount + "&date=" + date + "&kioskId=" + kioskId + "&instutionId=" + instutionId + "&WhichSide=1";
            var retValue = openDialoagWindow(url, 350, 150, "", "Kurum Verileri ile Mutabakat");
        }

        function searchButtonClicked(sender) {
            document.getElementById('pageNumRefField').value = "0";
            return true;
        }

        function showTransactionDetailClicked(transactionID) {
            var retVal = showTransactionDetailWindow(transactionID);
        }

        function editButtonClicked(itemID) {
            var retVal = showKioskWindow(itemID);
        }

        function postForPaging(selectedPageNum) {
            document.getElementById('pageNumRefField').value = selectedPageNum;
            document.getElementById('navigateButton').click();
        }
        function showMutabakatListWindow(mutabakatDate, kioskId, instutionId, whichDifferences) {
            var url = "../Account/DailyMutabakatDetail.aspx?whichDifferences=" + whichDifferences + "&kioskId=" + kioskId + "&instutionId=" + instutionId + "&mutabakatDate=" + mutabakatDate;
            var retVal = openDialoagWindow(url, 750, 500, window, "Detaylar");
        }


        function validateNo(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
    <script type="text/javascript" language="javascript" src="../js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery.tablednd_0_5.js"></script>

    <asp:HiddenField ID="idRefField" runat="server" Value="0" />
    <asp:HiddenField ID="oldOrderNumberField" runat="server" Value="0" />
    <asp:HiddenField ID="newOrderNumberField" runat="server" Value="0" />
    <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>
    <div align="center">
        <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp;</div>
        <asp:Panel ID="panel1" DefaultButton="searchButton" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto">
            <table border="0" cellpadding="0" cellspacing="0" id="Table2" runat="server" width="100%">
                <tr>
                    <td align="left" class="inputTitle" style="width: 130px">
                        <asp:Label ID="startDateLabel" runat="server" Visible="true">Mutabakat Zamanı:</asp:Label>
                    </td>
                    <td style="width: 159px">
                        <asp:TextBox ID="startDateField" runat="server" CssClass="inputLine_150x20"
                            Width="150px"></asp:TextBox>

                        <ajaxControlToolkit:CalendarExtender ID="startDateCalendar" runat="server" TargetControlID="startDateField"
                            Format="yyyy-MM-dd" Enabled="True">
                        </ajaxControlToolkit:CalendarExtender>
                    </td>
                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 79px; text-align: left">
                        <asp:Label ID="mercLabel" runat="server" Visible="true">Kiosk:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px">
                        <asp:ListBox ID="kioskBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>
                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 50px; text-align: left">
                        <asp:Label ID="Label2" runat="server" Visible="true">Kurum:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px">
                        <asp:ListBox ID="instutionBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>
                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    <td style="width: 6px">&nbsp;</td>
                    <td style="width: 86px">
                        <asp:Button ID="searchButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                            OnClick="searchButton_Click" OnClientClick="return checkForm();" Text="Getir"></asp:Button>
                    </td>
                    <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <div>&nbsp;</div>
    <div align="center">
        <asp:Panel ID="ListPanel" runat="server" CssClass="containerPanel_95Pxauto">
            <div align="left" class="windowTitle_container_autox30">
                Mutabakat<hr style="border-bottom: 1px solid #b2b2b4;" />
            </div>
            <asp:UpdatePanel ID="ListUPanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" RenderMode="Block">
                <ContentTemplate>
                    <table cellpadding="0" cellspacing="0" width="95%" id="upTable" runat="server">
                        <tr>
                            <td align="center" class="tableStyle1" colspan="6">


                                <asp:Table ID="itemsTable" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                    BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="100%">
                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="#"></asp:TableHeaderCell>
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Fatura Adedi"></asp:TableHeaderCell>
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Fatura Tutarı"></asp:TableHeaderCell>
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Kart Dolum Adedi"></asp:TableHeaderCell>
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Kart Dolum Tutarı"></asp:TableHeaderCell>
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Toplam Adet"></asp:TableHeaderCell>
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Toplam Tutar"></asp:TableHeaderCell>
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Kurum Toplam Adet"></asp:TableHeaderCell>
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Kurum Toplam Tutar"></asp:TableHeaderCell>
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Farklar"></asp:TableHeaderCell>
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text=""></asp:TableHeaderCell>
                                    </asp:TableRow>
                                </asp:Table>
                                <br />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="navigateButton" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </div>
    <br />
    <table width="95%">
        <tr align="right">
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="width: 80px">
                <asp:Label ID="Label5" runat="server" Text="powered by" CssClass="poweredbyTitle"></asp:Label>

            </td>
            <td style="width: 70px">
                <asp:LinkButton ID="LinkButton1" href="http://www.procenne.com/" runat="server" Text="PROCENNE" CssClass="procenneTitle"></asp:LinkButton>

            </td>
        </tr>
    </table>
</asp:Content>
