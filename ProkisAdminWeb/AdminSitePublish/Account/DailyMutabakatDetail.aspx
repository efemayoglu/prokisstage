﻿<%@ page language="C#" autoeventwireup="true" inherits="Account_DailyMutabakatDetail, App_Web_xjaoppgl" enableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="../styles/style.css" />
    <script type="text/javascript" language="javascript" src="../js/wdws.js"></script>
    <script language="javascript" type="text/javascript">

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function postForPaging(selectedPageNum) {
            document.getElementById('pageNumRefField').value = selectedPageNum;
            document.getElementById('navigateButton').click();
        }

    </script>
    <base target="_self" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <input type="hidden" id="pageNumRefField" runat="server" name="pageNumRefField" value="0" />
        <div align="center" style="padding: 4px;">
            <asp:Panel ID="panel7" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto_noShadow">
                <div id="messageArea" align="center" class="messageArea" runat="server">&nbsp;</div>
                <div id="articleDetailsTab" align="left" class="windowTitle_container_autox30" runat="server">
                    <hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>
                <table cellpadding="0" cellspacing="0" width="95%" id="upTable" runat="server">
                    <tr>
                        <td align="center" class="tableStyle1">
                            <asp:UpdatePanel ID="galleryListUPanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" RenderMode="Block">
                                <ContentTemplate>
                                    <asp:Table ID="itemsTable" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                        BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="95%">
                                        <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="#"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="İşlem No"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Abone No"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Tutar"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Fatura Id"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell CssClass="inputTitleCell4" Text="İşlem Zamanı"></asp:TableHeaderCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
