﻿<%@ page language="C#" autoeventwireup="true" inherits="Account_EmptyKioskMutabakat, App_Web_xjaoppgl" masterpagefile="~/webctrls/AdminSiteMPage.master" enableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<asp:Content ID="pageContent" ContentPlaceHolderID="mainCPHolder" runat="server">
    <input type="hidden" id="pageNumRefField" runat="server" name="pageNumRefField" value="0" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { setInterval(function () { cache_clear() }, 120000); });
        function cache_clear() { window.location.reload(true); }
    </script>
    <script language="javascript" type="text/javascript">

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function checkForm() {
            var status = true;
            var messageText = "";

            if (document.getElementById('kioskBox').value == "0") {
                status = false;
                messageText = "Onaylanacak Kiosku Seçiniz !";
            }
            else {
                messageText = "";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }

    </script>
    <script type="text/javascript" language="javascript" src="../js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery.tablednd_0_5.js"></script>

    <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>

    <div align="center">
        <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp;</div>
    </div>
    <div>&nbsp;</div>
    <div align="center">
        <asp:Panel ID="ListPanel" runat="server" CssClass="containerPanel_95Pxauto">
            <div align="left" class="windowTitle_container_autox30">
                Kiosk Toplanan Para Mutabakat<hr style="border-bottom: 1px solid #b2b2b4;" />
            </div>
            <div align="center">
                <table>
                    <tr>
                        <td align="center" class="staticTextLine_30x20" style="width: 67px">Kiosk:</td>
                        <td align="left" style="width: 58px">
                            <asp:ListBox ID="kioskBox" CssClass="inputLine_120x25" runat="server" SelectionMode="Single" AutoPostBack="True"
                                DataTextField="Name" DataValueField="Id" Rows="1" OnSelectedIndexChanged="kioskBox_SelectedIndexChanged"></asp:ListBox>
                        </td>
                    </tr>
                </table>
            </div>
            <table>
                <%--                <tr>
                    <td style="width: 230px"></td>
                    <td align="center" class="staticTextLine_30x25" style="width: 67px" >Kiosk:</td>
                    <td align="left" style="width: 58px">
                        <asp:ListBox ID="kioskBox" CssClass="inputLine_120x25" runat="server" SelectionMode="Single" AutoPostBack="True"
                            DataTextField="Name" DataValueField="Id" Rows="1" OnSelectedIndexChanged="kioskBox_SelectedIndexChanged"></asp:ListBox>
                    </td>
                    <td align="right" style="width: 170px"></td>
                </tr>--%>
                <tr>
                    <td colspan="4">
                        <table cellpadding="0" cellspacing="0" width="600px" id="Table2" runat="server">
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" width="200px" id="Table1" runat="server">
                                        <tr>
                                            <td align="center" colspan="2" class="staticTextLine_200x20">Merkezde Hesaplanan Miktar</td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2">
                                                <asp:Table ID="itemsTable" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                                    BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="95%">
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Para Türü" Width="60px"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Para Adedi"></asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="200 TL"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="MerkezTB1" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="100 TL"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="MerkezTB2" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="50 TL"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="MerkezTB3" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="20 TL"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="MerkezTB4" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="10 TL"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="MerkezTB5" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="5 TL"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="MerkezTB6" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="1 TL"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="MerkezTB7" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="50 Krş"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="MerkezTB8" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="25 Krş"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="MerkezTB9" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="10 Krş"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="MerkezTB10" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="5 Krş"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="MerkezTB11" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </td>
                                        </tr>

                                        <tr valign="middle">
                                            <td align="center" class="staticTextLine_60x30">Toplam Tutar</td>
                                            <td align="center">
                                                <asp:TextBox ID="MerkezTotalAmountTb" runat="server" CssClass="inputLine_120x20" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table cellpadding="0" cellspacing="0" width="200px" id="Table3" runat="server">
                                        <tr>
                                            <td align="center" colspan="2" class="staticTextLine_200x20">Kioskdan Alınan Miktar</td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2">
                                                <asp:Table ID="Table4" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                                    BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="95%">
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Para Türü" Width="60px"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Para Adedi"></asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="200 TL"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="ExpertTB1" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="100 TL"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="ExpertTB2" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="50 TL"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="ExpertTB3" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="20 TL"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="ExpertTB4" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="10 TL"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="ExpertTB5" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="5 TL"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="ExpertTB6" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="1 TL"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="ExpertTB7" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="50 Krş"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="ExpertTB8" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="25 Krş"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="ExpertTB9" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="10 Krş"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="ExpertTB10" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                                        <asp:TableHeaderCell CssClass="inputTitleCell8" Text="5 Krş"></asp:TableHeaderCell>
                                                        <asp:TableHeaderCell CssClass="inputTitleCell7">
                                                            <asp:TextBox ID="ExpertTB11" CssClass="inputLine_90x20" runat="server" Enabled="false"></asp:TextBox>
                                                        </asp:TableHeaderCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </td>
                                        </tr>

                                        <tr valign="middle">
                                            <td align="center" class="staticTextLine_60x30">Toplam Tutar :
                                            </td>
                                            <td align="center">
                                                <asp:TextBox ID="ExpertTotalAmountTb" runat="server" CssClass="inputLine_120x20" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
            <div align="center">
                <table>
                    <tr>
                        <td colspan="4" align="center" style="padding-top: 3px; padding-bottom: 5px;">
                            <asp:Button ID="saveButton" CssClass="buttonCSSDesign" Style="height: 30px; width: 80px;" runat="server" ClientIDMode="Static"
                                OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Onayla"></asp:Button>

                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>

    <br />
    <table width="95%">
        <tr align="right">
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="width: 80px">
                <asp:Label ID="Label5" runat="server" Text="powered by" CssClass="poweredbyTitle"></asp:Label>

            </td>
            <td style="width: 70px">
                <asp:LinkButton ID="LinkButton1" href="http://www.procenne.com/" runat="server" Text="PROCENNE" CssClass="procenneTitle"></asp:LinkButton>

            </td>
        </tr>
    </table>
</asp:Content>
