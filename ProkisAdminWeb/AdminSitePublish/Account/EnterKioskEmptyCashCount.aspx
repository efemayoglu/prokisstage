﻿<%@ page language="C#" autoeventwireup="true" inherits="Account_EnterKioskEmptyCashCount, App_Web_xjaoppgl" masterpagefile="~/webctrls/AdminSiteMPage.master" enableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<asp:Content ID="pageContent" ContentPlaceHolderID="mainCPHolder" runat="server">
    <input type="hidden" id="pageNumRefField" runat="server" name="pageNumRefField" value="0" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { setInterval(function () { cache_clear() }, 120000); });
        function cache_clear() { window.location.reload(true); }
    </script>
    <script language="javascript" type="text/javascript">

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function checkForm() {
            var status = true;
            var messageText = "";

            if (document.getElementById('codeField').value == "") {
                status = false;
                messageText = "Onay Kodu Giriniz !.";
            }
            else if (document.getElementById('kioskBox').value == "0") {
                status = false;
                messageText = "Kiosk Seçiniz !";
            }
            else {
                messageText = "";
            }

            document.getElementById('messageArea').innerHTML = messageText;

            return status;
        }

        function validateNo(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

    </script>
    <script type="text/javascript" language="javascript" src="../js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery.tablednd_0_5.js"></script>

    <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>

    <div align="center">
        <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp;</div>
    </div>
    <div>&nbsp;</div>
    <div align="center">
        <asp:Panel ID="ListPanel" runat="server" CssClass="containerPanel_95Pxauto">
            <div align="left" class="windowTitle_container_autox30">
                Toplanan Para Adetleri Girişi<hr style="border-bottom: 1px solid #b2b2b4;" />
            </div>
            <table cellpadding="0" cellspacing="0" width="200px" id="upTable" runat="server">
                <tr valign="middle">
                    <td align="center" class="staticTextLine_60x30">Onay Kodu:
                    </td>
                    <td align="center">
                        <asp:TextBox ID="codeField" runat="server" CssClass="inputLine_120x20"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" class="staticTextLine_60x30">Kiosk:
                    </td>
                    <td align="center">
                        <asp:ListBox ID="kioskBox" CssClass="inputLine_120x20" runat="server" SelectionMode="Single" AutoPostBack="True"
                            DataTextField="Name" DataValueField="Id" Rows="1"></asp:ListBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" class="tableStyle1" colspan="2">
                        <asp:Table ID="itemsTable" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                            BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="95%">
                            <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Para Türü" Width="60px"></asp:TableHeaderCell>
                                <asp:TableHeaderCell CssClass="inputTitleCell4" Text="Para Adedi"></asp:TableHeaderCell>
                            </asp:TableRow>
                            <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                <asp:TableHeaderCell CssClass="inputTitleCell8" Text="200 TL"></asp:TableHeaderCell>
                                <asp:TableHeaderCell CssClass="inputTitleCell7">
                                    <asp:TextBox ID="CashYpeTB1" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                                </asp:TableHeaderCell>
                            </asp:TableRow>
                            <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                <asp:TableHeaderCell CssClass="inputTitleCell8" Text="100 TL"></asp:TableHeaderCell>
                                <asp:TableHeaderCell CssClass="inputTitleCell7">
                                    <asp:TextBox ID="CashYpeTB2" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                                </asp:TableHeaderCell>
                            </asp:TableRow>
                            <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                <asp:TableHeaderCell CssClass="inputTitleCell8" Text="50 TL"></asp:TableHeaderCell>
                                <asp:TableHeaderCell CssClass="inputTitleCell7">
                                    <asp:TextBox ID="CashYpeTB3" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                                </asp:TableHeaderCell>
                            </asp:TableRow>
                            <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                <asp:TableHeaderCell CssClass="inputTitleCell8" Text="20 TL"></asp:TableHeaderCell>
                                <asp:TableHeaderCell CssClass="inputTitleCell7">
                                    <asp:TextBox ID="CashYpeTB4" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                                </asp:TableHeaderCell>
                            </asp:TableRow>
                            <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                <asp:TableHeaderCell CssClass="inputTitleCell8" Text="10 TL"></asp:TableHeaderCell>
                                <asp:TableHeaderCell CssClass="inputTitleCell7">
                                    <asp:TextBox ID="CashYpeTB5" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                                </asp:TableHeaderCell>
                            </asp:TableRow>
                            <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                <asp:TableHeaderCell CssClass="inputTitleCell8" Text="5 TL"></asp:TableHeaderCell>
                                <asp:TableHeaderCell CssClass="inputTitleCell7">
                                    <asp:TextBox ID="CashYpeTB6" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                                </asp:TableHeaderCell>
                            </asp:TableRow>
                            <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                <asp:TableHeaderCell CssClass="inputTitleCell8" Text="1 TL"></asp:TableHeaderCell>
                                <asp:TableHeaderCell CssClass="inputTitleCell7">
                                    <asp:TextBox ID="CashYpeTB7" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                                </asp:TableHeaderCell>
                            </asp:TableRow>
                            <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                <asp:TableHeaderCell CssClass="inputTitleCell8" Text="50 Krş"></asp:TableHeaderCell>
                                <asp:TableHeaderCell CssClass="inputTitleCell7">
                                    <asp:TextBox ID="CashYpeTB8" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                                </asp:TableHeaderCell>
                            </asp:TableRow>
                            <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                <asp:TableHeaderCell CssClass="inputTitleCell8" Text="25 Krş"></asp:TableHeaderCell>
                                <asp:TableHeaderCell CssClass="inputTitleCell7">
                                    <asp:TextBox ID="CashYpeTB9" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                                </asp:TableHeaderCell>
                            </asp:TableRow>
                            <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                <asp:TableHeaderCell CssClass="inputTitleCell8" Text="10 Krş"></asp:TableHeaderCell>
                                <asp:TableHeaderCell CssClass="inputTitleCell7">
                                    <asp:TextBox ID="CashYpeTB10" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                                </asp:TableHeaderCell>
                            </asp:TableRow>
                            <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3x20">
                                <asp:TableHeaderCell CssClass="inputTitleCell8" Text="5 Krş"></asp:TableHeaderCell>
                                <asp:TableHeaderCell CssClass="inputTitleCell7">
                                    <asp:TextBox ID="CashYpeTB11" CssClass="inputLine_90x20" runat="server" AutoPostBack="true" OnTextChanged="CashYpeTB1_TextChanged" onkeypress='validateNo(event)'></asp:TextBox>
                                </asp:TableHeaderCell>
                            </asp:TableRow>
                        </asp:Table>
                        <br />
                    </td>
                </tr>

                <tr valign="middle">
                    <td align="center" class="staticTextLine_60x30">Toplam Tutar :
                    </td>
                    <td align="center">
                        <asp:TextBox ID="TotalAmountTb" runat="server" CssClass="inputLine_120x20" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr>

                    <td></td>
                </tr>
                <tr>

                    <td colspan="2" align="center" style="padding-top: 3px; padding-bottom: 5px;">
                        <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                            OnClick="saveButton_Click" OnClientClick="return checkForm(); " Text="Kaydet"></asp:Button>

                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <br />
    <table width="95%">
        <tr align="right">
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="width: 80px">
                <asp:Label ID="Label5" runat="server" Text="powered by" CssClass="poweredbyTitle"></asp:Label>

            </td>
            <td style="width: 70px">
                <asp:LinkButton ID="LinkButton1" href="http://www.procenne.com/" runat="server" Text="PROCENNE" CssClass="procenneTitle"></asp:LinkButton>

            </td>
        </tr>
    </table>
</asp:Content>
