﻿<%@ page language="C#" autoeventwireup="true" inherits="Transaction_ListPurseTransactions, App_Web_xattgf5u" masterpagefile="~/webctrls/AdminSiteMPage.master" enableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<%@ Register Namespace="ClickableWebControl" TagPrefix="clkweb" %>
<asp:Content ID="pageContent" ContentPlaceHolderID="mainCPHolder" runat="server">
    <input type="hidden" id="pageNumRefField" runat="server" name="pageNumRefField" value="0" />
    <asp:Button ID="navigateButton" runat="server" OnClick="navigateButton_Click" CssClass="dummy" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { setInterval(function () { cache_clear() }, 120000); });
        function cache_clear() { window.location.reload(true); }
    </script>
    <script language="javascript" type="text/javascript">

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function generateAskiCodeClicked(transactionId) {
            var url = "../AskiCode/GenerateAskiCode.aspx?itemID=" + transactionId;
            var retValue = openDialoagWindow(url, 350, 250, "", "Aski Kod Üretme");
        }

        function searchButtonClicked(sender) {
            document.getElementById('pageNumRefField').value = "0";
            return true;
        }

        function showPurseTransactionDetailClicked(transactionID) {
            var retVal = showPurseTransactionDetailWindow(transactionID);
        }

        function editButtonClicked(itemID) {
            var retVal = showKioskWindow(itemID);
        }

        function postForPaging(selectedPageNum) {
            document.getElementById('pageNumRefField').value = selectedPageNum;
            document.getElementById('navigateButton').click();
        }

        function strtDateChngd() {
            if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                document.getElementById('startDateField').value = document.getElementById('endDateField').value;
            }

        }

        function endDateChngd() {
            if (document.getElementById('startDateField').value > document.getElementById('endDateField').value) {
                document.getElementById('endDateField').value = document.getElementById('startDateField').value;
            }
        }

        function validateNo(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
    <script type="text/javascript" language="javascript" src="../js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery.tablednd_0_5.js"></script>

    <asp:HiddenField ID="idRefField" runat="server" Value="0" />
    <asp:HiddenField ID="oldOrderNumberField" runat="server" Value="0" />
    <asp:HiddenField ID="newOrderNumberField" runat="server" Value="0" />
    <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>

    <div align="center">
        <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp;</div>
        <asp:Panel ID="panel1" DefaultButton="searchButton" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto">
            <table border="0" cellpadding="0" cellspacing="0" id="Table2" runat="server" width="90%">
                <tr>
                    <td align="left" class="inputTitle" style="width: 99px">
                        <asp:Label ID="startDateLabel" runat="server" Visible="true">Başlangıç Zamanı:</asp:Label>
                    </td>
                    <td style="width: 159px">
                        <asp:TextBox ID="startDateField" runat="server" CssClass="inputLine_150x20"
                            Width="150px"></asp:TextBox>

                        <ajaxControlToolkit:CalendarExtender ID="startDateCalendar" runat="server" TargetControlID="startDateField"
                            Format="yyyy-MM-dd HH:mm:ss" DaysModeTitleFormat="yyyy-MM-dd HH:mm:ss" Enabled="True"  OnClientDateSelectionChanged="strtDateChngd">
                        </ajaxControlToolkit:CalendarExtender>
                    </td>

                    <td align="left" class="inputTitle" style="width: 70px">
                        <asp:Label ID="endDateLabel" runat="server" Visible="true">Bitiş Zamanı:</asp:Label>
                    </td>
                    <td style="width: 161px">
                        <asp:TextBox ID="endDateField" runat="server" CssClass="inputLine_150x20"
                            Width="150px"></asp:TextBox>

                        <ajaxControlToolkit:CalendarExtender ID="endDateCalendar" runat="server" TargetControlID="endDateField"
                           Format="yyyy-MM-dd HH:mm:ss" DaysModeTitleFormat="yyyy-MM-dd HH:mm:ss" Enabled="True"  OnClientDateSelectionChanged="endDateChngd">
                        </ajaxControlToolkit:CalendarExtender>
                    </td>
                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 56px;">&nbsp;İşlem Ara:
                    </td>
                    <td align="left" style="width: 157px">
                        <asp:TextBox ID="searchTextField" CssClass="inputLine_150x20" runat="server"></asp:TextBox>
                    </td>
                    <td style="width: 6px">&nbsp;</td>
                    <td style="width: 86px">
                        <asp:Button ID="searchButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                            OnClick="searchButton_Click" Text="Ara"></asp:Button>
                    </td>
                    <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <div>&nbsp;</div>
    <div align="center">
        <asp:Panel ID="ListPanel" runat="server" CssClass="containerPanel_95Pxauto">
            <div align="left" class="windowTitle_container_autox30">
                E-cüzdan İşlemleri<hr style="border-bottom: 1px solid #b2b2b4;" />
            </div>
            <table cellpadding="0" cellspacing="0" width="95%" id="upTable" runat="server">
                <tr>
                    <td style="width: 22px; height: 20px;">
                        <asp:ImageButton ID="excellButton" runat="server" ClientIDMode="Static" OnClick="excelButton_Click" ImageUrl="~/images/excel.jpg" />
                    </td>
                    <td style="width: 8px; height: 20px;"></td>
                    <td style="width: 200px; height: 20px;">
                        <asp:Label ID="recordInfoLabel" runat="server" Style="width: 200px; height: 20px; text-align: left" CssClass="data"></asp:Label>
                    </td>
                    <td style="height: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td style="height: 20px; width: 130px; text-align: right">
                        <asp:Label ID="Label1" Text="Listelenecek Kayıt Sayısı :  " runat="server" Style="width: 200px; height: 20px; text-align: left" CssClass="data"></asp:Label>
                    </td>
                    <td align="right" style="height: 20px; width: 33px">
                        <asp:TextBox ID="numberOfItemField" CssClass="inputLine_30x20_s" MaxLength="3" runat="server" onkeypress='validateNo(event)'></asp:TextBox></td>

                </tr>
                <tr>
                    <td align="center" class="tableStyle1" colspan="6">
                        <asp:UpdatePanel ID="ListUPanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" RenderMode="Block">
                            <ContentTemplate>
                                <asp:Table ID="itemsTable" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                    BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="95%">
                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="#"></asp:TableHeaderCell>
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="İşlem Id    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="TransactionId" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Müşteri Adı    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="CustomerName" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Müşteri No    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="CustomerNo" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="İşlem Zamanı    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="TransactionDate" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kiosk Adı    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="KioskName" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="İşlem Yeri    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="TransactionKind" />
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text=""></asp:TableHeaderCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="navigateButton" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <br />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <br />
    <table width="95%">
        <tr align="right">
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="width: 80px">
                <asp:Label ID="Label5" runat="server" Text="powered by" CssClass="poweredbyTitle"></asp:Label>

            </td>
            <td style="width: 70px">
                <asp:LinkButton ID="LinkButton1" href="http://www.procenne.com/" runat="server" Text="PROCENNE" CssClass="procenneTitle"></asp:LinkButton>

            </td>
        </tr>
    </table>
</asp:Content>
