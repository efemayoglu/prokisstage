﻿
var windowRetVal = false;

function setDialogReturnValue(retVal) {
    windowRetVal = retVal;
}

function openDialoagWindow(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopup(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowUser(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupUser(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowBL(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupBL(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowApproveUser(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupApproveUser(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowAskiCode(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupAskiCode(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowBL(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupBL(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowApproveRole(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupApproveRole(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowRole(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupRole(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowApproveRole(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupApproveRole(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowApproveUser(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupApproveUser(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowAAA(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupAAA(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowAdminAAA(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupAdminAAA(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowAdminBLC(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupAdminBLC(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowBLC(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupBLC(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowAdminATBL(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupAdminAATBL(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowAdminBLC(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupAdminBLC(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowBLC(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupBLC(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}


function openDialoagWindowATBL(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupATBL(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}
function openDialoagWindowKiosk(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupKiosk(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowCreateKioskCommand(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupCreateKioskCommand(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindowKioskCommand(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopupKioskCommand(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindow2(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopup2(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function openDialoagWindow3(url, dWidth, dHeight, args, title) {
    var browserMajorVersion = getBrowserMajorVersion();

    showModalPopup3(url, 'yes', dWidth, dHeight, false, title);
    return windowRetVal;
}

function createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, popupID) {

    var modalPopupWindow = document.createElement("div");
    var modalPopupTitleArea = document.createElement("div");
    var modalPopupTitle = document.createElement("div");
    var modalPopupCloseButtonArea = document.createElement("div");
    var modalPopupIFrame = document.createElement("iframe");

    modalPopupWindow.id = popupID;
    modalPopupWindow.appendChild(modalPopupTitleArea);
    modalPopupWindow.appendChild(modalPopupIFrame);
    modalPopupWindow.setAttribute("align", "center");
    modalPopupWindow.setAttribute("class", "modalPopupInternal");
    modalPopupWindow.style.width = frameWidth + "px";
    modalPopupWindow.style.height = (frameHeight + 29) + "px";

    modalPopupTitleArea.appendChild(modalPopupTitle);
    modalPopupTitleArea.appendChild(modalPopupCloseButtonArea);
    modalPopupTitleArea.setAttribute("align", "right");
    modalPopupTitleArea.setAttribute("class", "modalPopupTitleArea");
    modalPopupTitleArea.id = popupID + "_tA";


    modalPopupCloseButtonArea.innerHTML = "<a href=\"javascript:void(0);\" title=\"Close\" onclick=\"hideModalPopup(\'" + popupID + "\');\">" +
            "<img src=\"../images/logout_icon.png\" alt=\"Close\" border=\"0\" /></a>";
    modalPopupCloseButtonArea.setAttribute("class", "modalPopupCloseButtonArea");

    modalPopupTitle.innerHTML = title;
    modalPopupTitle.setAttribute("class", "modalPopupTitle");

    modalPopupIFrame.src = popupUrl;
    modalPopupIFrame.width = frameWidth;
    modalPopupIFrame.height = frameHeight;
    modalPopupIFrame.scrolling = popupScrollable;
    modalPopupIFrame.setAttribute("frameborder", "0");
    modalPopupIFrame.setAttribute("allowtransparency", "true");

    positionModalPopup(modalPopupWindow);

    return modalPopupWindow;
}

function createModalPopup6_(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, popupID) {

    var modalPopupWindow = document.createElement("div");
    var modalPopupTitleArea = document.createElement("div");
    var modalPopupTitle = document.createElement("div");
    var modalPopupCloseButtonArea = document.createElement("div");
    var modalPopupIFrame = document.createElement("iframe");

    modalPopupWindow.id = popupID;
    modalPopupWindow.appendChild(modalPopupTitleArea);
    modalPopupWindow.appendChild(modalPopupIFrame);
    modalPopupWindow.setAttribute("align", "center");
    modalPopupWindow.setAttribute("class", "modalPopupInternal");
    modalPopupWindow.style.width = frameWidth + "px";
    modalPopupWindow.style.height = (frameHeight + 29) + "px";

    modalPopupTitleArea.appendChild(modalPopupTitle);
    modalPopupTitleArea.appendChild(modalPopupCloseButtonArea);
    modalPopupTitleArea.setAttribute("align", "right");
    modalPopupTitleArea.setAttribute("class", "modalPopupTitleArea");
    modalPopupTitleArea.id = popupID + "_tA";

    modalPopupTitle.innerHTML = title;
    modalPopupTitle.setAttribute("class", "modalPopupTitle");

    if (popupID == 'modalPopup_Merchant' || popupID == 'modalPopup_HSM' || popupID == 'modalPopup_User') {
    }
    else {
        modalPopupCloseButtonArea.innerHTML = "<a href=\"javascript:void(0);\" title=\"Close\" onclick=\"hideModalPopup(\'" + popupID + "\');\">" +
            "<img src=\"../images/logout_icon.png\" alt=\"Close\" border=\"0\" /></a>";
        modalPopupCloseButtonArea.setAttribute("class", "modalPopupCloseButtonArea");
    }

    modalPopupIFrame.src = popupUrl;
    modalPopupIFrame.width = frameWidth;
    modalPopupIFrame.height = frameHeight;
    modalPopupIFrame.scrolling = popupScrollable;
    modalPopupIFrame.setAttribute("frameborder", "0");
    modalPopupIFrame.setAttribute("allowtransparency", "true");

    positionModalPopup(modalPopupWindow);

    return modalPopupWindow;
}

function createModalPopupCardTxn(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, popupID) {

    var modalPopupWindow = document.createElement("div");
    var modalPopupTitleArea = document.createElement("div");
    var modalPopupTitle = document.createElement("div");
    var modalPopupCloseButtonArea = document.createElement("div");
    var modalPopupIFrame = document.createElement("iframe");

    modalPopupWindow.id = popupID;
    modalPopupWindow.appendChild(modalPopupTitleArea);
    modalPopupWindow.appendChild(modalPopupIFrame);
    modalPopupWindow.setAttribute("align", "center");
    modalPopupWindow.setAttribute("class", "modalPopupInternal");
    modalPopupWindow.style.width = frameWidth + "px";
    modalPopupWindow.style.height = (frameHeight + 29) + "px";

    modalPopupTitleArea.appendChild(modalPopupTitle);
    modalPopupTitleArea.appendChild(modalPopupCloseButtonArea);
    modalPopupTitleArea.setAttribute("align", "right");
    modalPopupTitleArea.setAttribute("class", "modalPopupTitleArea");
    modalPopupTitleArea.id = popupID + "_tA";

    modalPopupTitle.innerHTML = title;
    modalPopupTitle.setAttribute("class", "modalPopupTitle");

    modalPopupCloseButtonArea.innerHTML = "<a href=\"javascript:void(0);\" title=\"Close\" onclick=\"hideModalPopup(\'" + popupID + "\');\">" +
        "<img src=\"../images/logout_icon.png\" alt=\"Close\" border=\"0\" /></a>";
    modalPopupCloseButtonArea.setAttribute("class", "modalPopupCloseButtonArea");

    modalPopupIFrame.src = popupUrl;
    modalPopupIFrame.width = frameWidth;
    modalPopupIFrame.height = frameHeight;
    modalPopupIFrame.scrolling = popupScrollable;
    modalPopupIFrame.setAttribute("frameborder", "0");
    modalPopupIFrame.setAttribute("allowtransparency", "true");

    positionModalPopupCardTxn(modalPopupWindow);

    return modalPopupWindow;
}

function createModalPopup2(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, popupID) {

    var modalPopupWindow = document.createElement("div");
    var modalPopupTitleArea = document.createElement("div");
    var modalPopupTitle = document.createElement("div");
    var modalPopupCloseButtonArea = document.createElement("div");
    var modalPopupIFrame = document.createElement("iframe");

    modalPopupWindow.id = popupID;
    modalPopupWindow.appendChild(modalPopupTitleArea);
    modalPopupWindow.appendChild(modalPopupIFrame);
    modalPopupWindow.setAttribute("align", "center");
    modalPopupWindow.setAttribute("class", "modalPopupInternal");
    modalPopupWindow.style.width = frameWidth + "px";
    modalPopupWindow.style.height = (frameHeight + 29) + "px";

    modalPopupTitleArea.appendChild(modalPopupTitle);
    modalPopupTitleArea.appendChild(modalPopupCloseButtonArea);
    modalPopupTitleArea.setAttribute("align", "right");
    modalPopupTitleArea.setAttribute("class", "modalPopupTitleArea");
    modalPopupTitleArea.id = popupID + "_tA";

    modalPopupTitle.innerHTML = title;
    modalPopupTitle.setAttribute("class", "modalPopupTitle");

    var lastModalPopup = getLastModalPopup();

    modalPopupIFrame.src = popupUrl;
    modalPopupIFrame.width = frameWidth;
    modalPopupIFrame.height = frameHeight;
    modalPopupIFrame.scrolling = popupScrollable;
    modalPopupIFrame.setAttribute("frameborder", "0");
    modalPopupIFrame.setAttribute("allowtransparency", "true");

    positionModalPopup2(modalPopupWindow);

    return modalPopupWindow;
}

function createModalPopup5_2(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, popupID) {

    var modalPopupWindow = document.createElement("div");
    var modalPopupTitleArea = document.createElement("div");
    var modalPopupTitle = document.createElement("div");
    var modalPopupCloseButtonArea = document.createElement("div");
    var modalPopupIFrame = document.createElement("iframe");

    modalPopupWindow.id = popupID;
    modalPopupWindow.appendChild(modalPopupTitleArea);
    modalPopupWindow.appendChild(modalPopupIFrame);
    modalPopupWindow.setAttribute("align", "center");
    modalPopupWindow.setAttribute("class", "modalPopupInternal");
    modalPopupWindow.style.width = frameWidth + "px";
    modalPopupWindow.style.height = (frameHeight + 29) + "px";

    modalPopupTitleArea.appendChild(modalPopupTitle);
    modalPopupTitleArea.appendChild(modalPopupCloseButtonArea);
    modalPopupTitleArea.setAttribute("align", "right");
    modalPopupTitleArea.setAttribute("class", "modalPopupTitleArea");
    modalPopupTitleArea.id = popupID + "_tA";

    modalPopupTitle.innerHTML = title;
    modalPopupTitle.setAttribute("class", "modalPopupTitle");

    var lastModalPopup = getLastModalPopup();

    if (popupID == 'modalPopup_Merchant' || popupID == 'modalPopup_HSM' || popupID == 'modalPopup_User' || lastModalPopup.id == 'modalPopup_Merchant' || lastModalPopup.id == 'modalPopup_HSM' || lastModalPopup.id == 'modalPopup_User') {
    }
    else {
        modalPopupCloseButtonArea.innerHTML = "<a href=\"javascript:void(0);\" title=\"Close\" onclick=\"hideModalPopup(\'" + popupID + "\');\">" +
            "<img src=\"../images/logout_icon.png\" alt=\"Close\" border=\"0\" /></a>";
        modalPopupCloseButtonArea.setAttribute("class", "modalPopupCloseButtonArea");
    }

    modalPopupIFrame.src = popupUrl;
    modalPopupIFrame.width = frameWidth;
    modalPopupIFrame.height = frameHeight;
    modalPopupIFrame.scrolling = popupScrollable;
    modalPopupIFrame.setAttribute("frameborder", "0");
    modalPopupIFrame.setAttribute("allowtransparency", "true");

    positionModalPopup2(modalPopupWindow);

    return modalPopupWindow;
}

function createModalPopup3(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, popupID) {

    var modalPopupWindow = document.createElement("div");
    var modalPopupTitleArea = document.createElement("div");
    var modalPopupTitle = document.createElement("div");
    var modalPopupCloseButtonArea = document.createElement("div");
    var modalPopupIFrame = document.createElement("iframe");

    modalPopupWindow.id = popupID;
    modalPopupWindow.appendChild(modalPopupTitleArea);
    modalPopupWindow.appendChild(modalPopupIFrame);
    modalPopupWindow.setAttribute("align", "center");
    modalPopupWindow.setAttribute("class", "modalPopupInternal");
    modalPopupWindow.style.width = frameWidth + "px";
    modalPopupWindow.style.height = (frameHeight + 29) + "px";

    modalPopupTitleArea.appendChild(modalPopupTitle);
    modalPopupTitleArea.appendChild(modalPopupCloseButtonArea);
    modalPopupTitleArea.setAttribute("align", "right");
    modalPopupTitleArea.setAttribute("class", "modalPopupTitleArea");
    modalPopupTitleArea.id = popupID + "_tA";

    modalPopupTitle.innerHTML = title;
    modalPopupTitle.setAttribute("class", "modalPopupTitle");

    var lastModalPopup = getLastModalPopup();

    modalPopupIFrame.src = popupUrl;
    modalPopupIFrame.width = frameWidth;
    modalPopupIFrame.height = frameHeight;
    modalPopupIFrame.scrolling = popupScrollable;
    modalPopupIFrame.setAttribute("frameborder", "0");
    modalPopupIFrame.setAttribute("allowtransparency", "true");

    positionModalPopup3(modalPopupWindow);

    return modalPopupWindow;
}

function createModalPopup4_3(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, popupID) {

    var modalPopupWindow = document.createElement("div");
    var modalPopupTitleArea = document.createElement("div");
    var modalPopupTitle = document.createElement("div");
    var modalPopupCloseButtonArea = document.createElement("div");
    var modalPopupIFrame = document.createElement("iframe");

    modalPopupWindow.id = popupID;
    modalPopupWindow.appendChild(modalPopupTitleArea);
    modalPopupWindow.appendChild(modalPopupIFrame);
    modalPopupWindow.setAttribute("align", "center");
    modalPopupWindow.setAttribute("class", "modalPopupInternal");
    modalPopupWindow.style.width = frameWidth + "px";
    modalPopupWindow.style.height = (frameHeight + 29) + "px";

    modalPopupTitleArea.appendChild(modalPopupTitle);
    modalPopupTitleArea.appendChild(modalPopupCloseButtonArea);
    modalPopupTitleArea.setAttribute("align", "right");
    modalPopupTitleArea.setAttribute("class", "modalPopupTitleArea");
    modalPopupTitleArea.id = popupID + "_tA";

    modalPopupTitle.innerHTML = title;
    modalPopupTitle.setAttribute("class", "modalPopupTitle");

    var lastModalPopup = getLastModalPopup();

    if (popupID == 'modalPopup_Merchant' || popupID == 'modalPopup_HSM' || popupID == 'modalPopup_User' || lastModalPopup.id == 'modalPopup_Merchant' || lastModalPopup.id == 'modalPopup_HSM' || lastModalPopup.id == 'modalPopup_User') {
    }
    else {
        modalPopupCloseButtonArea.innerHTML = "<a href=\"javascript:void(0);\" title=\"Close\" onclick=\"hideModalPopup(\'" + popupID + "\');\">" +
            "<img src=\"../images/logout_icon.png\" alt=\"Close\" border=\"0\" /></a>";
        modalPopupCloseButtonArea.setAttribute("class", "modalPopupCloseButtonArea");
    }

    modalPopupIFrame.src = popupUrl;
    modalPopupIFrame.width = frameWidth;
    modalPopupIFrame.height = frameHeight;
    modalPopupIFrame.scrolling = popupScrollable;
    modalPopupIFrame.setAttribute("frameborder", "0");
    modalPopupIFrame.setAttribute("allowtransparency", "true");

    positionModalPopup3(modalPopupWindow);

    return modalPopupWindow;
}

function positionModalPopup(modalPopup) {
    var previousModalPopup = getLastModalPopup();
    var lefPos = "";
    var topPos = "";

    modalPopup.style.position = "absolute";
    if (previousModalPopup == null) {
        leftPos = ((window.screen.availWidth - modalPopup.style.width.replace("px", "")) / 2);
        topPos = ((window.screen.availHeight - modalPopup.style.height.replace("px", "")) / 2) - 50;
        leftPos = parseInt(leftPos);
        topPos = parseInt(topPos);
    }
    else {
        leftPos = previousModalPopup.style.left.replace("px", "");
        topPos = previousModalPopup.style.top.replace("px", "");
        leftPos = parseInt(leftPos) + 30;
        topPos = parseInt(topPos) + 30;
    }
    modalPopup.style.top = topPos + "px";
    modalPopup.style.left = leftPos + "px";
}

function positionModalPopup2(modalPopup) {
    var previousModalPopup = getLastModalPopup();
    var lefPos = "";
    var topPos = "";

    modalPopup.style.position = "absolute";
    if (previousModalPopup == null) {
        leftPos = ((window.screen.availWidth - modalPopup.style.width.replace("px", "")) / 2);
        topPos = ((window.screen.availHeight - modalPopup.style.height.replace("px", "")) / 2);
        leftPos = parseInt(leftPos);
        topPos = parseInt(topPos);
    }
    else {
        leftPos = previousModalPopup.style.left.replace("px", "");
        topPos = previousModalPopup.style.top.replace("px", "");
        leftPos = parseInt(leftPos) + 40;
        topPos = parseInt(topPos) + 120;
    }
    modalPopup.style.top = topPos + "px";
    modalPopup.style.left = leftPos + "px";
}

function positionModalPopup3(modalPopup) {
    var previousModalPopup = getLastModalPopup();
    var lefPos = "";
    var topPos = "";

    modalPopup.style.position = "absolute";
    if (previousModalPopup == null) {
        leftPos = ((window.screen.availWidth - modalPopup.style.width.replace("px", "")) / 2);
        topPos = ((window.screen.availHeight - modalPopup.style.height.replace("px", "")) / 2);
        leftPos = parseInt(leftPos);
        topPos = parseInt(topPos);
    }
    else {
        leftPos = previousModalPopup.style.left.replace("px", "");
        topPos = previousModalPopup.style.top.replace("px", "");
        leftPos = parseInt(leftPos) + 190;
        topPos = parseInt(topPos) + 120;
    }
    modalPopup.style.top = topPos + "px";
    modalPopup.style.left = leftPos + "px";
}

var modalPopupZIndex = 9000;

function showModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_" + generateRandomNumber(10001, 20001);
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupKiosk(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_Kiosk";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupCreateKioskCommand(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_CreateKioskCommand";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}
function showModalPopupKioskCommand(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_KioskCommand";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupUser(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_User";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupBL(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_BL";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupApproveUser(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_ApproveUser";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupAAA(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_Fraud";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupAdminAAA(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_FraudAdmin";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupAdminBLC(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_BLCAdmin";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupBLC(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_BLC";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupAdminAATBL(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_FraudAdmin";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupATBL(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_Fraud";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupAskiCode(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_AskiCode";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupBL(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_BL";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupApproveRole(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_ApproveRole";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupApproveRole(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_ApproveUser";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupRole(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_Role";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopupApproveRole(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_ApproveRole";
    var modalPopupWindow = createModalPopup(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 500;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopup2(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_" + generateRandomNumber(10001, 20001);
    var modalPopupWindow = createModalPopup2(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 600;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function showModalPopup3(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title) {

    var modalPopupID = "modalPopup_" + generateRandomNumber(10001, 20001);
    var modalPopupWindow = createModalPopup3(popupUrl, popupScrollable, frameWidth, frameHeight, flatBackground, title, modalPopupID);
    var modalPopupContainer = document.getElementById("modalPopupArea");

    var paddingTop = ((window.screen.height - frameHeight) / 2) + 600;

    modalPopupContainer.appendChild(modalPopupWindow);
    modalPopupZIndex++;
    modalPopupWindow.style.zIndex = modalPopupZIndex;
    focusModalPopup(modalPopupID);

    modalPopupContainer.style.visibility = "visible";
    modalPopupContainer.style.display = "block";
    modalPopupContainer.style.paddingTop = paddingTop + "px";

    if (flatBackground) {
        modalPopupContainer.style.backgroundImage = 'none';
        modalPopupContainer.style.backgroundColor = 'black';
    }
    else {
        modalPopupContainer.style.backgroundImage = 'url(../images/transparent_bg.png)';
        modalPopupContainer.style.backgroundColor = 'Transparent';
    }
}

function focusModalPopup(modalPopupID) {
    var modalPopupContainer = document.getElementById("modalPopupArea");
    var modalPopupCount = modalPopupContainer.childNodes.length;

    for (var i = (modalPopupCount - 1) ; i >= 0; i--) {
        try {
            if (modalPopupContainer.childNodes[i].tagName.toLowerCase() == "div" &&
                    modalPopupContainer.childNodes[i].id.indexOf("modalPopup_") == 0) {
                document.getElementById(modalPopupContainer.childNodes[i].id + "_tA").className = "modalPopupTitleArea_Inactive";
                modalPopupContainer.childNodes[i].style.borderColor = "#999999";
            }
        }
        catch (exception) {
        }
    }

    var lastModalPopup = getLastModalPopup();

    if (lastModalPopup != null) {
        document.getElementById(lastModalPopup.id + "_tA").className = "modalPopupTitleArea";
        lastModalPopup.style.borderColor = "#8A8B8C";
    }
}

function hideModalPopup(id) {
    var modalPopupContainer = document.getElementById("modalPopupArea");
    var modalPopupCount = modalPopupContainer.childNodes.length;

    for (var i = 0; i < modalPopupCount; i++) {
        try {
            if (modalPopupContainer.childNodes[i].id == id) {
                var modalPopup = modalPopupContainer.childNodes[i];
                modalPopupContainer.removeChild(modalPopup);
                modalPopup.innerHTML = "";
                break;
            }
        }
        catch (exception) {
        }
    }

    var lastModalPopup = getLastModalPopup();

    if (lastModalPopup == null) {
        modalPopupContainer.style.visibility = "hidden";
        modalPopupContainer.style.display = "none";
    }
    else {
        focusModalPopup(lastModalPopup.id);
    }

    if (id == 'modalPopup_User')
        window.parent.location = "../User/ListUsers.aspx";
    else if (id == 'modalPopup_Kiosk')
        window.parent.location = "../Kiosk/ListKiosk.aspx";
    else if (id == 'modalPopup_Role')
        window.parent.location = "../UserRole/ListUserRoles.aspx";
    else if (id == 'modalPopup_KioskCommand')
        window.parent.location = "../Kiosk/ListKioskCommand.aspx";
    else if (id == 'modalPopup_ApproveRole')
        window.parent.location = "../UserRole/ApproveUserRole.aspx";
    else if (id == 'modalPopup_ApproveUser')
        window.parent.location = "../User/ApproveUser.aspx";

}

function getLastModalPopup() {
    var modalPopupContainer = document.getElementById("modalPopupArea");
    var modalPopupCount = modalPopupContainer.childNodes.length;
    var lastModalPopup = null;

    for (var i = (modalPopupCount - 1) ; i >= 0; i--) {
        try {
            if (modalPopupContainer.childNodes[i].tagName.toLowerCase() == "div" &&
                    modalPopupContainer.childNodes[i].id.indexOf("modalPopup_") == 0) {
                lastModalPopup = modalPopupContainer.childNodes[i];
                break;
            }
        }
        catch (exception) {
        }
    }
    return lastModalPopup;
}

function getPreviousModalPopup(modalPopupID) {
    var modalPopupContainer = document.getElementById("modalPopupArea");
    var modalPopupCount = modalPopupContainer.childNodes.length;
    var previousModalPopup = null;

    for (var i = (modalPopupCount - 1) ; i >= 0; i--) {
        try {
            if (modalPopupContainer.childNodes[i].tagName.toLowerCase() == "div" &&
                    modalPopupContainer.childNodes[i].id.indexOf("modalPopup_") == 0 &&
                    modalPopupContainer.childNodes[i] != modalPopupID) {
                previousModalPopup = modalPopupContainer.childNodes[i];
                break;
            }
        }
        catch (exception) {
        }
    }
    return previousModalPopup;
}

function hideModalPopup2() {
    var lastModalPopupID = "";

    var lastModalPopup = getLastModalPopup();

    if (lastModalPopup != null)
        hideModalPopup(lastModalPopup.id);
}

function hideModalPopupLastandPrevious(id) {
    var lastModalPopupID = "";
    hideModalPopup(id);

    var lastModalPopup = getLastModalPopup();

    if (lastModalPopup != null)
        hideModalPopup(lastModalPopup.id);

    if (id == 'modalPopup_User')
        window.parent.location = "../User/ListUsers.aspx";
    else if (id == 'modalPopup_Role')
        window.parent.location = "../UserRole/ListUserRoles.aspx";
    else if (id == 'modalPopup_Kiosk')
        window.parent.location = "../Kiosk/ListKiosk.aspx";
    else if (id == 'modalPopup_KioskCommand')
        window.parent.location = "../Kiosk/ListKioskCommand.aspx";
    else if (id == 'modalPopup_AskiCode')
        window.parent.location = "../AskiCode/ListAskiCodes.aspx";
    else if (id == 'modalPopup_ApproveRole')
        window.parent.location = "../UserRole/ApproveUserRole.aspx";
    else if (id == 'modalPopup_ApproveUser')
        window.parent.location = "../User/ApproveUser.aspx";
}

function hideModalPopupLastandRefresh(id) {
    if (id == 'User') {
        hideModalPopup('modalPopup_User');
        window.parent.location = "../User/ListUsers.aspx";
    }
    else if (id == 'Role') {
        hideModalPopup('modalPopup_Role');
        window.parent.location = "../UserRole/ListUserRoles.aspx";
    }
    else if (id == 'Kiosk') {
        hideModalPopup('modalPopup_Kiosk');
        window.parent.location = "../Kiosk/ListKiosk.aspx";
    }
    else if (id == 'KioskCommand') {
        hideModalPopup('modalPopup_KioskCommand');
        window.parent.location = "../Kiosk/ListKioskCommand.aspx";
    }

    else if (id == 'AskiCode') {
        hideModalPopup('modalPopup_AskiCode');
        window.parent.location = "../AskiCode/ListAskiCodes.aspx";
    }
    else if (id == 'ApproveRole') {
        hideModalPopup('modalPopup_ApproveRole');
        window.parent.location = "../UserRole/ApproveUserRole.aspx";
    }
    else if (id == 'ApproveUser') {
        hideModalPopup('modalPopup_ApproveUser');
        window.parent.location = "../User/ApproveUser.aspx";
    }
    else if (id == 'Fraud') {
        hideModalPopup('modalPopup_Fraud');
        window.parent.location = "../Transaction/ApproveAccountAction.aspx";
    }
    else if (id == 'FraudAdmin') {
        hideModalPopup('modalPopup_FraudAdmin');
        window.parent.location = "../Transaction/AdminApproveAccountAction.aspx";
    }
    else if (id == 'BLC') {
        hideModalPopup('modalPopup_BLC');
        window.parent.location = "../Customer/ListWebCustomer.aspx";
    }
    else if (id == 'BLCAdmin') {
        hideModalPopup('modalPopup_BLCAdmin');
        window.parent.location = "../Customer/ApproveBlackListWebCustomer.aspx";
    }
    else if (id == 'BlackList') {
        hideModalPopup('modalPopup_BL');
        window.parent.location = "../Customer/ListBlackList.aspx";
    }
    }

function getOpenerModalPopup() {
    var modalPopupContainer = parent.document.getElementById("modalPopupArea");
    var modalPopupCount = modalPopupContainer.childNodes.length;
    var openerModalPopup = null;
    var count = 0;

    for (var i = (modalPopupCount - 1) ; i >= 0; i--) {
        try {
            if (modalPopupContainer.childNodes[i].tagName.toLowerCase() == "div" &&
                    modalPopupContainer.childNodes[i].id.indexOf("modalPopup_") == 0) {
                count++;
                if (count == 2) {
                    openerModalPopup = modalPopupContainer.childNodes[i];
                    break;
                }
            }
        }
        catch (exception) {
        }
    }
    return openerModalPopup;
}

function getFrameInModalPopup(modalPopup) {
    var childNodes = modalPopup.childNodes.length;
    var iframe = null;

    for (var i = 0; i < childNodes; i++) {
        if (modalPopup.childNodes[i].tagName.toLowerCase() == "iframe") {
            iframe = modalPopup.childNodes[i];
            break;
        }
    }

    return iframe;
}

function runDialogCloseScript(userMessage, retVal) {

    retval = (retVal == null ? false : retVal);

    var openerModalPopup = getOpenerModalPopup();
    var iframe = null;

    if (openerModalPopup != null) {
        iframe = getFrameInModalPopup(openerModalPopup);
    }

    parent.setDialogReturnValue(retVal);

    if (retVal) {
        if (iframe != null) {
            iframe.contentWindow.refresh();
        }
        else {
            parent.refresh();
        }
    }

    parent.alert(userMessage);
    parent.hideModalPopup2();
}

function showUserWindow(itemID) {
    var url = "../User/ShowUser.aspx?itemID=" + itemID;
    var retVal = openDialoagWindowUser(url, 510, 360, window, "Detaylar");
    return retVal;
}

function showBLWindow() {
    var url = "../Customer/ShowBlackList.aspx";
    var retVal = openDialoagWindowBL(url, 400, 270, window, "Black List Kişi Ekleme");
    return retVal;
}

function showApproveUserWindow(itemID) {
    var url = "../User/ShowApproveUser.aspx?itemID=" + itemID;
    var retVal = openDialoagWindowApproveUser(url, 800, 420, window, "Detaylar");
    return retVal;
}

function showRoleWindow(itemID) {
    var url = "../UserRole/ShowUserRole.aspx?itemID=" + itemID;
    var retVal = openDialoagWindowRole(url, 510, 600, window, "Detaylar");
    return retVal;
}

function showApproveRoleWindow(itemID) {
    var url = "../UserRole/ShowApproveUserRole.aspx?itemID=" + itemID;
    var retVal = openDialoagWindowApproveRole(url, 900, 600, window, "Detaylar");
    return retVal;
}

function showTransactionDetailWindow(transactionID) {
    var url = "../Transaction/ListTransactionDetails.aspx?transactionId=" + transactionID;
    var retVal = openDialoagWindow(url, 1000, 600, window, "İşlem Detayı");
    return retVal;
}

function showPurseTransactionDetailWindow(transactionID) {
    var url = "../Transaction/ListPurseTransactionDetails.aspx?transactionId=" + transactionID;
    var retVal = openDialoagWindow(url, 1000, 600, window, "İşlem Detayı");
    return retVal;
}
function showConditionDetailWindow(kioskID) {
    var url = "../Kiosk/ListKioskConditionDetails.aspx?kioskId=" + kioskID;
    var retVal = openDialoagWindow(url, 800, 350, window, "Para Akışı");
    return retVal;
}

function showCashCountWindow(kioskID) {
    var url = "../Kiosk/ListKioskCashCount.aspx?kioskId=" + kioskID;
    var retVal = openDialoagWindow(url, 400, 300, window, "Para Adetleri");
    return retVal;
}

function showCashBoxCashCountWindow(kioskID) {
    var url = "../Account/ListFullCashBoxCashCount.aspx?kioskId=" + kioskID;
    var retVal = openDialoagWindow(url, 400, 300, window, "Para Üstü Adetleri");
    return retVal;
}

function showKioskEmptyCashCountWindow(operationId) {
    var url = "../Kiosk/ListKioskCashCount.aspx?operationId=" + operationId;
    var retVal = openDialoagWindow(url, 400, 300, window, "Toplanan Para Adetleri");
    return retVal;
}
function showKioskFullCashBoxCountWindow(operationId) {
    var url = "../Account/ListFullCashBoxCashCount.aspx?operationId=" + operationId;
    var retVal = openDialoagWindow(url, 400, 300, window, "Doldurulan Para Adetleri");
    return retVal;
}

function showAccountDetailWindow(accountId, instutionId) {
    var url = "../Account/ListAccountDetails.aspx?accountId=" + accountId + "&instutionId=" + instutionId;
    var retVal = openDialoagWindow(url, 800, 350, window, "Hesap Detay");
    return retVal;
}

function showKioskWindow(itemID) {
    var url = "../Kiosk/ShowKiosk.aspx?kioskID=" + itemID;
    var retVal = openDialoagWindowKiosk(url, 510, 310, window, "Detaylar");
    return retVal;
}

function showCreateKioskCommandWindow() {
    var url = "../Kiosk/CreateKioskCommand.aspx";
    var retVal = openDialoagWindowCreateKioskCommand(url, 510, 310, window, "Komut Oluşturma");
    return retVal;
}

function showDeleteWindow(sqlQuery) {
    var url = "../webctrls/ShowDeleteWindow.aspx?sqlQuery=" + sqlQuery;
    var retVal = openDialoagWindow(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showDeleteWindowUser(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=User";
    var retVal = openDialoagWindowUser(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showDeleteWindowApproveUser(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=User";
    var retVal = openDialoagWindowApproveUser(url, 400, 90, window, "Silme");
    return retVal;
}

function showApproveWindowApproveUser(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=User";
    var retVal = openDialoagWindowApproveUser(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showApproveWindowAAA(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=Fraud";
    var retVal = openDialoagWindowAAA(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showDeleteWindowAAA(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=Fraud";
    var retVal = openDialoagWindowAAA(url, 400, 90, window, "Silme");
    return retVal;
}

function showApproveWindowAdminAAA(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=FraudAdmin&type=app";
    var retVal = openDialoagWindowAdminAAA(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showApproveWindowAdminDAA(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=FraudAdmin&type=del";
    var retVal = openDialoagWindowAdminAAA(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showDeleteWindowAdminAAA(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=FraudAdmin&type=de";
    var retVal = openDialoagWindowAdminAAA(url, 400, 90, window, "Silme");
    return retVal;
}

function showDeleteWindowAdminBLC(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=BLCAdmin";
    var retVal = openDialoagWindowAdminBLC(url, 400, 90, window, "Silme");
    return retVal;
}

function showDeleteWindowBLC(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=BLC";
    var retVal = openDialoagWindowBLC(url, 400, 90, window, "Silme");
    return retVal;
}

function showWindowAdminATBL(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=FraudAdmin&type=de";
    var retVal = openDialoagWindowAdminATBL(url, 400, 90, window, "Black List Ekleme");
    return retVal;
}

function showWindowAdminBLC(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=BLCAdmin";
    var retVal = openDialoagWindowAdminBLC(url, 400, 90, window, "Black List Ekleme");
    return retVal;
}

function showWindowBLC(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=BLC";
    var retVal = openDialoagWindowBLC(url, 400, 90, window, "Black List Ekleme");
    return retVal;
}

function showWindowATBL(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=Fraud";
    var retVal = openDialoagWindowATBL(url, 400, 90, window, "Black List Ekleme");
    return retVal;
}

function showDeleteWindowAskiCode(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=AskiCode";
    var retVal = openDialoagWindowAskiCode(url, 400, 80, window, "Onaylama");
    return retVal;
}

function showDeleteWindowBL(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=BlackList";
    var retVal = openDialoagWindowBL(url, 350, 100, window, "Silme");
    return retVal;
}

function showApproveWindowBL(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=BlackList";
    var retVal = openDialoagWindowBL(url, 350, 100, window, "Onaylama");
    return retVal;
}

function showDeleteWindowApproveRole(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=ApproveRole";
    var retVal = openDialoagWindowApproveRole(url, 400, 80, window, "Silme");
    return retVal;
}

function showApproveWindowApproveRole(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=ApproveRole";
    var retVal = openDialoagWindowApproveRole(url, 400, 80, window, "Onaylama");
    return retVal;
}
function showDeleteWindowRole(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=Role";
    var retVal = openDialoagWindowRole(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showDeleteWindowKiosk(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=Kiosk";
    var retVal = openDialoagWindowKiosk(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showDeleteWindowKioskCommand(storedProcedure, itemId) {
    var url = "../webctrls/ShowDeleteWindow.aspx?storedProcedure=" + storedProcedure + "&itemId=" + itemId + "&whichPage=KioskCommand";
    var retVal = openDialoagWindowKioskCommand(url, 400, 90, window, "Onaylama");
    return retVal;
}

function showMessageWindow(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText;
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}

function showAlertWindow(messageText) {
    var url = "../webctrls/AlertWindow.aspx?messageText=" + messageText;
    var retVal = openDialoagWindow(url, 400, 90, window, "Uyarı");
    return retVal;
}

function showAlertWindow2(messageText) {
    var url = "../webctrls/AlertWindow.aspx?messageText=" + messageText;
    var retVal = openDialoagWindow3(url, 500, 100, window, "Uyarı");
    return retVal;
}

function showMessageWindowUser(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_User";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}

function showMessageWindowRole(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_Role";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}

function showMessageWindowKiosk(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_Kiosk";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}

function showMessageWindowKioskCommand(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_KioskCommand";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}

function showMessageWindowDefault(messageText) {
    var url = "../webctrls/MessageWindow.aspx?messageText=" + messageText + "&mainModalPopupId=modalPopup_Default";
    var retVal = openDialoagWindow2(url, 400, 90, window, "Operasyon Mesaj Detayı");
    return retVal;
}