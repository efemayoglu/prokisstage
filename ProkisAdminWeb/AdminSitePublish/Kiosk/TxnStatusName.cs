﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Transactions
{
    public class TxnStatusName
    {
        private string id;
        public string Id
        {
            get { return id; }
        }

        private string name;
        public string Name
        {
            get { return name; }
        }

        internal TxnStatusName(string id
            , string name)
        {
            this.id = id;
            this.name = name;
        }
    }
}
