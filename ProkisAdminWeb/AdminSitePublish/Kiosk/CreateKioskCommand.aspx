﻿<%@ page language="C#" autoeventwireup="true" inherits="Kiosk_CreateKioskCommand, App_Web_wtuencvq" enableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="../styles/style.css" />
    <title></title>
</head>
<body>
    <form id="Form1" method="post" runat="server" onsubmit="return prepareFormForSubmitting();">
        <asp:ScriptManager ID="ajaxScriptManager" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true"></asp:ScriptManager>
        <div align="center">
            <div>&nbsp;</div>
            <asp:Panel ID="panel1" DefaultButton="saveButton" runat="server" CssClass="containerPanel_95Pxauto">
                <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp</div>
                <div align="left" class="windowTitle_container_autox30">
                    Kiosk Komut Oluşturma<hr style="border-bottom: 1px solid #b2b2b4;" />
                </div>
                <table cellspacing="0" cellpadding="0" border="0" id="upTable" runat="server" style="width: 90%">
                    <tr>
                        <td align="center" valign="top">
                            <table cellpadding="2" cellspacing="2" style="width: 52%">
                                <tr>
                                    <td align="left" class="staticTextLine_200x20">Kiosk Seçiniz:
                                    </td>
                                    <td align="left" class="auto-style2">
                                        <asp:ListBox ID="kioskBox" CssClass="inputLine_150x20" runat="server" SelectionMode="Single" AutoPostBack="True"
                                            DataTextField="Name" DataValueField="Id" Rows="1"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="staticTextLine_200x20">Komut Tipi Seçiniz:
                                    </td>
                                    <td align="left" class="auto-style2">
                                        <asp:ListBox ID="commandTypeBox" CssClass="inputLine_150x20" runat="server" SelectionMode="Single" AutoPostBack="True"
                                            DataTextField="Name" DataValueField="Id" Rows="1"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="staticTextLine_200x20">Komutu Yazınız:
                                    </td>
                                    <td align="left" class="auto-style2">
                                        <asp:TextBox runat="server" ID="CommandTextBox" Height="71px" TextMode="MultiLine" Width="217px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="saveButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                            OnClick="saveButton_Click" OnClientClick="return checkForm();" Text="Kaydet"></asp:Button>

                                    </td>
                                    <td>
                                        <asp:Button ID="cancelButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                                            OnClientClick="parent.hideModalPopup2();" Text="Vazgeç"></asp:Button>
                                    </td>
                        </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
