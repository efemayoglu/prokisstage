﻿<%@ page language="C#" autoeventwireup="true" inherits="AskiCode_ListAskiCodes_SweepBack, App_Web_etorzike" masterpagefile="~/webctrls/AdminSiteMPage.master" enableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxControlToolkit" %>
<%@ Register Namespace="ClickableWebControl" TagPrefix="clkweb" %>
<asp:Content ID="pageContent" ContentPlaceHolderID="mainCPHolder" runat="server">
    <input type="hidden" id="pageNumRefField" runat="server" name="pageNumRefField" value="0" />
    <asp:Button ID="navigateButton" runat="server" OnClick="navigateButton_Click" CssClass="dummy" />
    <asp:Button ID="deleteButton" runat="server" CssClass="dummy" OnClick="deleteButton_Click" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { setInterval(function () { cache_clear() }, 120000); });
        function cache_clear() { window.location.reload(true); }
    </script>
    <script language="javascript" type="text/javascript">

        function changeUserPasswordClicked(userId) {
            var url = "../User/GenerateUserPassword.aspx?userId=" + userId;
            var retValue = openDialoagWindow(url, 350, 170, "", "Şifre Değiştirme");
        }

        function showAlert(message) {
            var retVal = showAlertWindow(message);
        }

        function approveButtonClicked(storedProcedure, itemId) {
            var retVal = showDeleteWindowAskiCode(storedProcedure, itemId);
        }

        function deleteButtonClicked(storedProcedure, itemId) {
            var retVal = showDeleteWindowAskiCode(storedProcedure, itemId);
        }

        function searchButtonClicked(sender) {
            document.getElementById('pageNumRefField').value = "0";
            return true;
        }

        function showTransactionDetailClicked(transactionID) {
            var retVal = showTransactionDetailWindow(transactionID);
        }

        function postForPaging(selectedPageNum) {
            document.getElementById('pageNumRefField').value = selectedPageNum;
            document.getElementById('navigateButton').click();
        }

        function validateNo(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

    </script>
    <script type="text/javascript" language="javascript" src="../js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery.tablednd_0_5.js"></script>
    <asp:HiddenField ID="idRefField" runat="server" Value="0" />
    <asp:HiddenField ID="oldOrderNumberField" runat="server" Value="0" />
    <asp:HiddenField ID="newOrderNumberField" runat="server" Value="0" />
    <asp:Literal ID="scriptLiteral" runat="server"></asp:Literal>

    <div align="center">
        <div class="messageArea" id="messageArea" style="color: red" align="center" runat="server">&nbsp;</div>
        <asp:Panel ID="panel1" DefaultButton="searchButton" runat="server" BackColor="White" Width="95%" CssClass="containerPanel_95Pxauto">
            <table border="0" cellpadding="0" cellspacing="0" id="Table2" runat="server" width="90%">
                <tr>
                    <td align="left" class="inputTitle" style="width: 70px">
                        Tarih:
                    </td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px">
                        <asp:TextBox ID="endDateField" runat="server" CssClass="inputLine_100x20"></asp:TextBox>
                        <ajaxControlToolkit:CalendarExtender ID="endDateCalendar" runat="server" TargetControlID="endDateField"
                            Format="yyyy-MM-dd HH:mm:ss" DaysModeTitleFormat="yyyy-MM-dd HH:mm:ss" Enabled="True">
                        </ajaxControlToolkit:CalendarExtender>
                    </td>
                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 79px; text-align: left">
                        <asp:Label ID="mercLabel" runat="server" Visible="true">Kod Durumu:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px">
                        <asp:ListBox ID="codeStatusBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>
                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 79px; text-align: left">
                        <asp:Label ID="Label1" runat="server" Visible="true">Üretilme Tipi:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px">
                        <asp:ListBox ID="generatedTypeBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single" Rows="1" Visible="true">
                            <asp:ListItem Value="0">Üretilme Tipini Seç</asp:ListItem>
                            <asp:ListItem Value="1">Otomatik Üretilen</asp:ListItem>
                            <asp:ListItem Value="2">Manuel Üretilen</asp:ListItem>
                        </asp:ListBox></td>
                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 50px; text-align: left">
                        <asp:Label ID="Label3" runat="server" Visible="true">Kurum:</asp:Label></td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 102px">
                        <asp:ListBox ID="instutionBox" CssClass="inputLine_100x20" runat="server" SelectionMode="Single"
                            DataTextField="Name" DataValueField="Id" Rows="1" Visible="true"></asp:ListBox></td>
                    <td style="width: 18px">&nbsp;&nbsp;</td>
                    <td align="left" class="inputTitle" style="width: 56px;">&nbsp;Kod Ara:
                    </td>
                    <td style="width: 9px">&nbsp;</td>
                    <td align="left" style="width: 102px">
                        <asp:TextBox ID="searchTextField" CssClass="inputLine_100x20" runat="server"></asp:TextBox>
                    </td>
                    <td style="width: 9px">&nbsp;</td>
                    <td style="width: 86px">
                        <asp:Button ID="searchButton" CssClass="buttonCSSDesign" runat="server" ClientIDMode="Static"
                            OnClick="searchButton_Click" Text="Ara"></asp:Button>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <div>&nbsp;</div>
    <div align="center">
        <asp:Panel ID="ListPanel" runat="server" CssClass="containerPanel_95Pxauto">
            <div align="left" class="windowTitle_container_autox30">
                Para Kodlar<hr style="border-bottom: 1px solid #b2b2b4;" />
            </div>
            <table cellpadding="0" cellspacing="0" width="95%" id="upTable" runat="server">
                <tr style="height: 20px">
                    <td style="width: 22px;">
                        <asp:ImageButton ID="excellButton" runat="server" ClientIDMode="Static" OnClick="excelButton_Click" ImageUrl="~/images/excel.jpg" />
                    </td>
                    <td style="width: 8px;">&nbsp;</td>
                    <td style="width: 200px;">
                        <asp:Label ID="recordInfoLabel" runat="server" Style="width: 200px; height: 20px; text-align: left" CssClass="data"></asp:Label>
                    </td>
                    <td style="height: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td style="height: 20px; width: 130px; text-align: right">
                        <asp:Label ID="Label2" Text="Listelenecek Kayıt Sayısı :  " runat="server" Style="width: 200px; height: 20px; text-align: left" CssClass="data"></asp:Label>
                    </td>
                    <td align="right" style="height: 20px; width: 33px">
                        <asp:TextBox ID="numberOfItemField" CssClass="inputLine_30x20_s" MaxLength="3" runat="server" onkeypress='validateNo(event)'></asp:TextBox></td>


                </tr>
                <tr>
                    <td align="center" class="tableStyle1" colspan="6">
                        <asp:UpdatePanel ID="ListUPanel" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional" RenderMode="Block">
                            <ContentTemplate>
                                <asp:Table ID="itemsTable" runat="server" CellPadding="2" CellSpacing="0" BorderWidth="0px"
                                    BorderStyle="Inset" BorderColor="White" BackColor="White" GridLines="None" CssClass="data" Width="95%">
                                    <asp:TableRow BorderColor="White" BorderWidth="1px" BorderStyle="Double" CssClass="inputTitleCell3">
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" Text="#"></asp:TableHeaderCell>
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="İşlem No    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="TransactionId" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kurum    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Instution" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Müşteri Adı    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="CustomerName" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Abone No    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="AboneNo" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Durumu    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="Status" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Oluşturulma Tarihi    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="GeneretionDate" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Tutarı    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="CodeAmount" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kod    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="AskiCode" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kod Üretilme Adımı    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="GenerationCase" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Kullanılma Tarihi    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="UsedDate" />
                                        <clkweb:ClickableTableHeaderCell CssClass="inputTitleCell4" Text="Üreten Kullanıcı    <img src=../images/finger.png border=0 title=Sorting /></a>" OnTableHeaderCellClicked="Sort" ID="GeneratedUser" />
                                        <asp:TableHeaderCell CssClass="inputTitleCell4" ID="DetailColumn" runat="server" Text=""></asp:TableHeaderCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="deleteButton" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="navigateButton" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <br />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <br />
    <table width="95%">
        <tr align="right">
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="inputTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="width: 80px">
                <asp:Label ID="Label5" runat="server" Text="powered by" CssClass="poweredbyTitle"></asp:Label>

            </td>
            <td style="width: 70px">
                <asp:LinkButton ID="LinkButton1" href="http://www.procenne.com/" runat="server" Text="PROCENNE" CssClass="procenneTitle"></asp:LinkButton>

            </td>
        </tr>
    </table>
</asp:Content>
