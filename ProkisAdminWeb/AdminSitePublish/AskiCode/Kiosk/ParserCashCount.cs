﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRCNCORE.Parser
{
    public class ParserCashCount
    {
        [JsonProperty("kioskId")]
        public int KioskId { get; set; }

        [JsonProperty("kioskName")]
        public string KioskName { get; set; }

        [JsonProperty("cashTypeName")]
        public string cashTypeName { get; set; }

        [JsonProperty("cashCount")]
        public int cashCount { get; set; }
    }
}
