﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Backend;
using PRCNCORE.Constants;
using PRCNCORE.Input;
using PRCNCORE.RoleSubMenus;
using PRCNCORE.Users;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Services_Login : System.Web.UI.Page
{
    private ReturnCodes errorCode = ReturnCodes.SUCCESSFULL;
    JObject data = new JObject();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.HttpMethod == "POST")
        {
            ProcessRequest();
        }
    }

    private void ProcessRequest()
    {
        SwitchInput inputData = null;
        try
        {
            string sData = WebServiceUtils.LoadParameters(Request, InputType.Clear);

            if (!string.IsNullOrEmpty(sData))
            {
                inputData = JsonConvert.DeserializeObject<SwitchInput>(sData);

                if (!WebServiceUtils.CheckCredentials(inputData.ApiKey, inputData.ApiPass))
                {
                    errorCode = ReturnCodes.CREDENTIALS_ERROR;
                }
                //else if (!WebServiceUtils.MobileBrowserCheck("SwitchInput_Count"))
                //{
                //    errorCode = FastPOSReturnCodes.INVALID_BROWSER;
                //    data = WebServiceUtils.AddErrorNodes(data, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
                //}
                else
                {
                    data = PrepareResponse(inputData.FunctionCode, sData);
                    //responseData = WebServiceUtils.AddErrorNodes(responseData, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
                }
            }
            else
            {
                errorCode = ReturnCodes.LOAD_JSON_PARAMETERS_ERROR;
                
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ProcessRequestLogin");
        }
        data = WebServiceUtils.AddErrorNodes(data, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
        WebServiceUtils.SendResponse(data, Response, false);
    }


    private JObject PrepareResponse(int functionCode, string input)
    {
        JObject data = new JObject();


        try
        {
            switch ((FC)functionCode)
            {

                case FC.GET_LOGIN_USER:
                    InputGetUser inputLogin = JsonConvert.DeserializeObject<InputGetUser>(input);
                    data = GetLoginUser(inputLogin.userName, inputLogin.password, inputLogin.minute, inputLogin.tryLimit, inputLogin.expireDayCount);
                    break;

                default:
                    break;
            }


        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "PrepareResponseLogin");
        }

        return data;
    }

    private JObject GetLoginUser(string username, string userpassword, int minute, int tryLimit, int expireDay)
    {

        JObject data = new JObject();
        try
        {

            UserOpr loginOpr = new UserOpr();
            int operationResult = 0;
            string accessToken = "";
            WebUser user = loginOpr.GetLoginUser(username, userpassword, minute, tryLimit, ref operationResult, expireDay, ref accessToken);
            loginOpr.Dispose();
            loginOpr = null;

            if (user != null)
            {
                data = GetJSON(user);


                SubMenuOpr subMenuOpr = new SubMenuOpr();

                RoleSubMenuCollection roleSubMenus = subMenuOpr.GetRoleMenus(user.RoleId);

                List<SubMenuContext> contextMenu = new List<SubMenuContext>();
                contextMenu.Add(new SubMenuContext("fa fa-search", "Düzenle","Edit", true));

                data.Add("UserRoles", roleSubMenus.GetJSONJArray());
                data.Add("accessToken", accessToken);
                //data.Add("UserRolesContext", contextMenu);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                if (operationResult == -2)
                    errorCode = ReturnCodes.EXIST_USER_WRONG_PASS;
                else if (operationResult == -3)
                    errorCode = ReturnCodes.EXCEED_TRLIMIT_USER_MUST_WAIT;
                else if (operationResult == -4)
                    errorCode = ReturnCodes.EXPIRE_USER;
                else if (operationResult == -5)
                    errorCode = ReturnCodes.DEACTIVE_USER;
                else
                    errorCode = ReturnCodes.EXIST_USER_WRONG_PASS;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetLoginUser");
        }
        return data;
    }

    public JObject GetJSON(WebUser theUser)
    {
        JObject jObjectDis = new JObject();

        string a = JsonConvert.SerializeObject(theUser, Newtonsoft.Json.Formatting.None);

        JObject x = JObject.Parse(a);


        jObjectDis.Add("User", x);

        return jObjectDis;
    }
}