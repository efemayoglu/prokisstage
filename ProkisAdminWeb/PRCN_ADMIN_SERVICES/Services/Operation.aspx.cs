﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Backend;
using PRCNCORE.Constants;
using PRCNCORE.Input;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Services_Operation : System.Web.UI.Page
{
    private ReturnCodes errorCode = ReturnCodes.SUCCESSFULL;
    InputDeleteOperation deleteOperation = null;
    JObject data = new JObject();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.HttpMethod == "POST")
        {
            ProcessRequest();
        }
    }

    private void ProcessRequest()
    {
        SwitchInput inputData = null;

        try
        {
            string sData = WebServiceUtils.LoadParameters(Request, InputType.Clear);

            if (!string.IsNullOrEmpty(sData))
            {
                inputData = JsonConvert.DeserializeObject<SwitchInput>(sData);

                if (!WebServiceUtils.CheckCredentials(inputData.ApiKey, inputData.ApiPass))
                {
                    errorCode = ReturnCodes.CREDENTIALS_ERROR;
                 }
                else if (Request.Headers["AccessToken"] == null || !WebServiceUtils.CheckAccessTokenSecurity(Request.Headers["AccessToken"], inputData.adminUserId))
                {
                    errorCode = ReturnCodes.INVALID_ACCESS_TOKEN;
                }
                else
                {
                    data = PrepareResponse(inputData.FunctionCode, sData);
                    //responseData = WebServiceUtils.AddErrorNodes(responseData, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
                }
            }
            else
            {
                errorCode = ReturnCodes.LOAD_JSON_PARAMETERS_ERROR;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ProcessRequestOperation");
        }
        data = WebServiceUtils.AddErrorNodes(data, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));

        WebServiceUtils.SendResponse(data, Response, false);
    }


    private JObject PrepareResponse(int functionCode, string input)
    {
        JObject data = new JObject();
        try
        {
            switch ((FC)functionCode)
            {

                case FC.DELETE_OPERATION:
                    deleteOperation = JsonConvert.DeserializeObject<InputDeleteOperation>(input);
                    data = DeleteOperation(deleteOperation.userId, deleteOperation.storedProcedure, deleteOperation.itemId);
                    break;
                case FC.OPERATION_INFORM:
                    deleteOperation = JsonConvert.DeserializeObject<InputDeleteOperation>(input);
                    data = OperationInform(deleteOperation.userId, deleteOperation.storedProcedure, deleteOperation.itemId, deleteOperation.type);
                    break;
                case FC.DELETE_MONEY_CODE:
                    deleteOperation = JsonConvert.DeserializeObject<InputDeleteOperation>(input);
                    data = DeleteMoneyCode(deleteOperation.userId, deleteOperation.storedProcedure, deleteOperation.vitelTicketNo, deleteOperation.itemId);
                    break;
                default:
                    break;
            }

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            data = WebServiceUtils.AddErrorNodes(data, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "PrepareResponseOperation");
        }

        return data;
    }

    private JObject DeleteOperation(int userId, string storedProcedure, int itemId)
    {
        JObject data = new JObject();
        try
        {
            int returnCode = 0;
            TransactionOpr opr = new TransactionOpr();
            opr.DeleteOperation(userId, storedProcedure, itemId, ref returnCode);
            opr.Dispose();
            opr = null;

            if (returnCode == 1)
                errorCode = ReturnCodes.SUCCESSFULL;
            else if (returnCode == -99)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;
            else if (returnCode == -2)
                errorCode = ReturnCodes.INVALID_SENDER_ACCOUNT;
            else if (returnCode == -3)
                errorCode = ReturnCodes.SENDER_HAS_NO_ACCOUNT;
            else if (returnCode == -4)
                errorCode = ReturnCodes.INVALID_RECEIVER_ACCOUNT;
            else if (returnCode == -5)
                errorCode = ReturnCodes.INSUFFICIENT_BALANCE;
            else if (returnCode == -13)
                errorCode = ReturnCodes.ITEM_NOT_FOUND;
            else if (returnCode == -14)
                errorCode = ReturnCodes.CUSTOMER_EXIST_BL;
            else
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;


        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "DeleteOperation");
        }
        return data;
    }

    private JObject OperationInform(int userId, string storedProcedure, int itemId, int type)
    {
        JObject data = new JObject();
        try
        {
            int returnCode = 0;
            TransactionOpr opr = new TransactionOpr();
            opr.OperationInform(userId, storedProcedure, itemId, ref returnCode, type);
            opr.Dispose();
            opr = null;

            if (returnCode == 1)
                errorCode = ReturnCodes.SUCCESSFULL;
            else if (returnCode == -99)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;
            else if (returnCode == -2)
                errorCode = ReturnCodes.INVALID_SENDER_ACCOUNT;
            else if (returnCode == -3)
                errorCode = ReturnCodes.SENDER_HAS_NO_ACCOUNT;
            else if (returnCode == -4)
                errorCode = ReturnCodes.INVALID_RECEIVER_ACCOUNT;
            else if (returnCode == -5)
                errorCode = ReturnCodes.INSUFFICIENT_BALANCE;
            else if (returnCode == -13)
                errorCode = ReturnCodes.ITEM_NOT_FOUND;
            else if (returnCode == -14)
                errorCode = ReturnCodes.CUSTOMER_EXIST_BL;
            else
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;


        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "DeleteOperation");
        }
        return data;
    }

    private JObject DeleteMoneyCode(int userId, string detailText, string vitelTicketNo, int itemId)
    {
        JObject data = new JObject();
        try
        {
            int returnCode = 0;
            TransactionOpr opr = new TransactionOpr();
            opr.DeleteMoneyCode(userId, detailText, vitelTicketNo, itemId, ref returnCode);
            opr.Dispose();
            opr = null;

            if (returnCode == 1)
                errorCode = ReturnCodes.SUCCESSFULL;
            else if (returnCode == -99)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;
            else if (returnCode == -2)
                errorCode = ReturnCodes.INVALID_SENDER_ACCOUNT;
            else if (returnCode == -3)
                errorCode = ReturnCodes.SENDER_HAS_NO_ACCOUNT;
            else if (returnCode == -4)
                errorCode = ReturnCodes.INVALID_RECEIVER_ACCOUNT;
            else if (returnCode == -5)
                errorCode = ReturnCodes.INSUFFICIENT_BALANCE;
            else if (returnCode == -13)
                errorCode = ReturnCodes.ITEM_NOT_FOUND;
            else if (returnCode == -14)
                errorCode = ReturnCodes.CUSTOMER_EXIST_BL;
            else
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;


        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "DeleteMoneyCode");
        }
        return data;
    }
}