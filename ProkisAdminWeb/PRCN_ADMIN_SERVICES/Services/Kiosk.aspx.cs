﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE;
using PRCNCORE.Constants;
using PRCNCORE.Backend;
using PRCNCORE.Input;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PRCNCORE.Input.Kiosk;
using PRCNCORE.KioskCommands;
using PRCNCORE.KioskConditions;
using PRCNCORE.Roles;
using PRCNCORE.Kiosks;
using PRCNCORE.Cities;
using PRCNCORE.Transactions;
using PRCNCORE.Accounts;
using PRCNCORE.Input.Source;

public partial class Services_Kiosk : System.Web.UI.Page
{
    InputListKiosks listKiosks = null;
    private ReturnCodes errorCode = ReturnCodes.SUCCESSFULL;
    private KioskCollection kiosks;

    JObject data = new JObject();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.HttpMethod == "POST")
        {
            ProcessRequest();
        }
    }

    private void ProcessRequest()
    {
        SwitchInput inputData = null;

        try
        {
            string sData = WebServiceUtils.LoadParameters(Request, InputType.Clear);

            if (!string.IsNullOrEmpty(sData))
            {
                inputData = JsonConvert.DeserializeObject<SwitchInput>(sData);

                if (!WebServiceUtils.CheckCredentials(inputData.ApiKey, inputData.ApiPass))
                {
                    errorCode = ReturnCodes.CREDENTIALS_ERROR;
                }
                else if (Request.Headers["AccessToken"] == null || !WebServiceUtils.CheckAccessTokenSecurity(Request.Headers["AccessToken"], inputData.adminUserId))
                {
                    errorCode = ReturnCodes.INVALID_ACCESS_TOKEN;
                }
                else
                {
                    data = PrepareResponse(inputData.FunctionCode, sData, inputData.adminUserId, inputData.pageUrl);
                    //responseData = WebServiceUtils.AddErrorNodes(responseData, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
                }
            }
            else
            {
                errorCode = ReturnCodes.LOAD_JSON_PARAMETERS_ERROR;
                
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteError(Utility.GetConfigValue("LogFilePath"), "ProcessRequest", exp.Message, exp.StackTrace);
        }
        data = WebServiceUtils.AddErrorNodes(data, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
        WebServiceUtils.SendResponse(data, Response, false);
    }

    private JObject PrepareResponse(int functionCode, string input, long adminUserId, string pageUrl)
    {
        JObject data = new JObject();

        try
        {
            switch ((FC)functionCode)
            {

                case FC.LIST_KIOSK_REPORTS_NEW:
                    var list_kiosk_reports_new = JsonConvert.DeserializeObject<InputListKiosksReportsNew>(input);
                    data = SearchListKioskReports(list_kiosk_reports_new);
                    break;

                case FC.GET_SYSTEM_SETTING:
                    var getSystemSettings = JsonConvert.DeserializeObject<SystemSetting>(input);
                    data = SearchSystemSetting(getSystemSettings.StartDate, getSystemSettings.EndDate);
                    break;

                case FC.UPDATE_SYSTEM_SETTING:
                    var updateSystemSetting = JsonConvert.DeserializeObject<SystemSetting>(input);
                    data = UpdateSystemSetting(  updateSystemSetting.Id , adminUserId);
                    break;

                case FC.DELETE_SYSTEM_SETTING:
                    var DELETE_SYSTEM_SETTING = JsonConvert.DeserializeObject<SystemSetting>(input);
                    data = DeleteSystemSetting(DELETE_SYSTEM_SETTING.Id, adminUserId);
                    break;

                //[dbo].[delete_SystemSetting]
                case FC.SAVE_SYSTEM_SETTING:
                    var saveSystemSettng = JsonConvert.DeserializeObject<SaveSystemSettingParser>(input);
                    data = SaveSystemSetting(saveSystemSettng.Id, adminUserId, saveSystemSettng.InstutionId, saveSystemSettng.NewStatus, saveSystemSettng.ApprovedUserId, saveSystemSettng.ScreenMessage);
                    break;

                case FC.GET_EMPLOYEE_SCHEDULING:
                    var get_employee_scheduling = JsonConvert.DeserializeObject<EmployeeScheduling>(input);
                    data = SearchEmployeeScheduling(get_employee_scheduling.Id, get_employee_scheduling.StartDate, get_employee_scheduling.EndDate);
                    break;

                case FC.SAVE_EMPLOYEE_SCHEDULING:
                    var save_employee_scheduling = JsonConvert.DeserializeObject<EmployeeScheduling>(input);
                    data = SaveEmployeeScheduling(save_employee_scheduling.Id, save_employee_scheduling.Title, save_employee_scheduling.AllDay, save_employee_scheduling.BackgroundColor, save_employee_scheduling.BorderColor
                        , save_employee_scheduling.StartDate, save_employee_scheduling.EndDate, save_employee_scheduling.isDeleted);
                    //data = SearchEmployeeScheduling(get_employee_scheduling.Id, get_employee_scheduling.StartDate, get_employee_scheduling.EndDate);
                    break;

                case FC.GET_SOURCES:
                    var sources = JsonConvert.DeserializeObject<Sources>(input);
                    data = SearchSource(sources.SourceCategoryId, sources.Id);
                    break;

                case FC.GET_SOURCE_CATEGORY:
                    var source_category = JsonConvert.DeserializeObject<Sources>(input);
                    data = SearchSourceCategory(source_category.Id);
                    break;

                case FC.GET_SOURCE_CATEGORY_TYPE:
                    var source_category_type = JsonConvert.DeserializeObject<Sources>(input);
                    data = SearchSourceCategoryType(source_category_type.Id);
                    break;

                case FC.ADD_SOURCE:
                    var source = JsonConvert.DeserializeObject<Sources>(input);
                    data = AddSource(source.Explanation, source.CreatedUserId, source.CreatedUserName, source.Link, source.SourceCategoryId, source.SourceCategoryTypeId,source.SourceTypeId);
                    break;

                case FC.UPDATE_SOURCE:
                    var update_source = JsonConvert.DeserializeObject<Sources>(input);
                    data = UpdateSource(update_source.Id , update_source.Explanation, update_source.Link, update_source.SourceCategoryId, update_source.SourceCategoryTypeId,update_source.SourceTypeId);
                    break;

                case FC.GET_SOURCE_TYPE:
                    var source_type = JsonConvert.DeserializeObject<Sources>(input);
                    data = SearchSourceType(source_type.Id);
                    break;

                case FC.LIST_KIOSKS:
                    listKiosks = JsonConvert.DeserializeObject<InputListKiosks>(input);
                    data = SearchKiosks(listKiosks.SearchText
                        , listKiosks.NumberOfItemsPerPage
                        , listKiosks.CurrentPageNum
                        , listKiosks.RecordCount
                        , listKiosks.PageCount
                        , listKiosks.OrderSelectionColumn
                        , listKiosks.DescAsc
                        , listKiosks.KioskId
                        , listKiosks.KioskTypeId
                        , listKiosks.OrderType
                        , listKiosks.InstutionId
                        , listKiosks.ConnectionId
                        , listKiosks.CardReaderId
                        , listKiosks.StatusId
                        , listKiosks.RegionId
                        , listKiosks.CityId
                        , listKiosks.RiskId
                        , listKiosks.BankNameId
                        , listKiosks.cameraIp
                        , listKiosks.channelNumber
                        , listKiosks.userName
                        , listKiosks.password
                        );
                    
                    break;
                case FC.LIST_KIOSKS_DISPENSER_ERRORS:
                    InputListKioskDispenseErrors listKioskDispenserError = JsonConvert.DeserializeObject<InputListKioskDispenseErrors>(input);
                    data = SearchKioskDispenserErrors(listKioskDispenserError.SearchText
                        , listKioskDispenserError.NumberOfItemsPerPage
                        , listKioskDispenserError.CurrentPageNum
                        , listKioskDispenserError.RecordCount
                        , listKioskDispenserError.PageCount
                        , listKioskDispenserError.OrderSelectionColumn
                        , listKioskDispenserError.DescAsc
                        , listKioskDispenserError.KioskId
                        , listKioskDispenserError.OrderType
                        , listKioskDispenserError.InstitutionId
                        , listKioskDispenserError.ErrorId
                        );

                    break;
                case FC.GET_KIOSK:
                    InputGetKiosk kioskId = JsonConvert.DeserializeObject<InputGetKiosk>(input);
                    data = GetKiosk(kioskId.kioskId);
                    break;

                case FC.SAVE_KIOSK:
                    InputSaveKiosk kiosk = JsonConvert.DeserializeObject<InputSaveKiosk>(input);
                    data = SaveKiosk(kiosk);
                    break;

                case FC.SAVE_KIOSK_DEVICE:
                    InputSaveKioskDevice kioskDevice = JsonConvert.DeserializeObject<InputSaveKioskDevice>(input);
                    data = SaveKioskDevice(kioskDevice);
                    break;
                case FC.UPDATE_KIOSK_DEVICE:
                    InputSaveKioskDevice updateKioskDevice = JsonConvert.DeserializeObject<InputSaveKioskDevice>(input);
                    data = UpdateKioskDevice(updateKioskDevice);
                    break;
                case FC.UPDATE_KIOSK:
                    InputUpdateKiosk updateKiosk = JsonConvert.DeserializeObject<InputUpdateKiosk>(input);
                    data = UpdateKiosk(updateKiosk);
                    break;
                case FC.LIST_KIOSK_COMMANDS:
                    InputListKioskCommands listKioskCommands = JsonConvert.DeserializeObject<InputListKioskCommands>(input);
                    data = SearchKioskCommands(listKioskCommands.SearchText
                        , listKioskCommands.StartDate
                        , listKioskCommands.EndDate
                        , listKioskCommands.KioskId
                        , listKioskCommands.InstutionId
                        , listKioskCommands.NumberOfItemsPerPage
                        , listKioskCommands.CurrentPageNum
                        , listKioskCommands.RecordCount
                        , listKioskCommands.PageCount
                        , listKioskCommands.OrderSelectionColumn
                        , listKioskCommands.DescAsc
                        , listKioskCommands.OrderType);
                    break;

                case FC.GET_KIOSK_COMMANDS:
                    InputListKioskCommands commandkiosk = JsonConvert.DeserializeObject<InputListKioskCommands>(input);
                    data = GetKioskCommand(commandkiosk.CommandId);
                    break;

                case FC.SAVE_KIOSK_COMMAND:
                    InputSaveKioskCommand inputSaveKioskCommand = JsonConvert.DeserializeObject<InputSaveKioskCommand>(input);
                    data = SaveKioskCommand(inputSaveKioskCommand.kioskId, inputSaveKioskCommand.institutionId, inputSaveKioskCommand.createdUserId, inputSaveKioskCommand.commandTypeId, inputSaveKioskCommand.commandString,
                        inputSaveKioskCommand.commandReasonId, inputSaveKioskCommand.explanation);
                    break;

                case FC.UPDATE_KIOSK_COMMAND:
                    InputSaveKioskCommand inputUpdateKioskCommand = JsonConvert.DeserializeObject<InputSaveKioskCommand>(input);
                    data = UpdateKioskCommand(inputUpdateKioskCommand.commandId,
                                            //inputUpdateKioskCommand.kioskId, 
                                            //inputUpdateKioskCommand.institutionId,
                                            inputUpdateKioskCommand.createdUserId,
                                            inputUpdateKioskCommand.commandTypeId,
                                            inputUpdateKioskCommand.commandString,
                                            inputUpdateKioskCommand.commandReasonId,
                                            inputUpdateKioskCommand.explanation);
                                        break;


                case FC.LIST_KIOSK_COMMAND_TYPE_NAMES:
                    data = GetKioskCommandTypeNames();
                    break;
                case FC.LIST_KIOSK_COMMAND_REASON_NAMES:
                    data = GetKioskCommandReasonNames();
                    break;
                case FC.LIST_KIOSK_NAMES:
                    data = GetKioskNames();
                    break;

                case FC.LIST_REFERENCE_STATUS:
                    data = GetReferenceStatus();
                    break;


                case FC.GET_KIOSK_REFERENCE_SYSTEM:
                    InputGetKioskReferenceSystem kioskRefernceSystem2 = JsonConvert.DeserializeObject<InputGetKioskReferenceSystem>(input);
                    data = GetKioskReferenceSystem(kioskRefernceSystem2.referenceNo);
                    break;


                case FC.UPDATE_KIOSK_REFERENCE_SYSTEM:
                    InputGetKioskReferenceSystem kioskRefernceSystem3 = JsonConvert.DeserializeObject<InputGetKioskReferenceSystem>(input);
                    data = UpdateKioskReferenceSystem(kioskRefernceSystem3.Id, kioskRefernceSystem3.referenceNo,adminUserId);
                    break;


                case FC.LIST_KIOSK_REFERENCE_SYSTEM:
                    InputSaveControlCashNotification kioskreferencesystem = JsonConvert.DeserializeObject<InputSaveControlCashNotification>(input);
                    data = SearchKioskReferenceSystem(kioskreferencesystem.SearchText,
                        kioskreferencesystem.StartDate
                        , kioskreferencesystem.EndDate
                        , kioskreferencesystem.ReferenceStatus
                        , kioskreferencesystem.NumberOfItemsPerPage
                        , kioskreferencesystem.CurrentPageNum
                        , kioskreferencesystem.RecordCount
                        , kioskreferencesystem.PageCount
                        , kioskreferencesystem.OrderSelectionColumn
                        , kioskreferencesystem.DescAsc
                        , kioskreferencesystem.InstutionId
                        , kioskreferencesystem.KioskIds
                        , kioskreferencesystem.OrderType
                        );
                    break;

                case FC.SAVE_KIOSK_REFERENCE_SYSTEM:
                    InputSaveKioskReferenceSystem inputSaveKioskReferenceSystem = JsonConvert.DeserializeObject<InputSaveKioskReferenceSystem>(input);
                    data = SaveKioskReferenceSystem(inputSaveKioskReferenceSystem);
                    break;

                case FC.LIST_KIOSK_CONDITION:
                    InputListKioskCondition listKioskMoney = JsonConvert.DeserializeObject<InputListKioskCondition>(input);
                    data = SearchKioskCondition(listKioskMoney.KioskId
                        , listKioskMoney.SearchText
                        , listKioskMoney.NumberOfItemsPerPage
                        , listKioskMoney.CurrentPageNum
                        , listKioskMoney.RecordCount
                        , listKioskMoney.PageCount
                        , listKioskMoney.OrderSelectionColumn
                        , listKioskMoney.DescAsc
                        , listKioskMoney.KioskType
                        , listKioskMoney.OrderType);
                    break;

                case FC.UPDATE_KIOSK_CONDITION_DISPENSER_CASH_COUNT:
                    InputListKioskCondition dispenserCashCount = JsonConvert.DeserializeObject<InputListKioskCondition>(input);
                    data = UpdateKioskConditionDispenserCashCount(dispenserCashCount);
                    break;

                case FC.LIST_KIOSK_MONITORING:
                    InputListKioskMonitoring listKioskMonitoring = JsonConvert.DeserializeObject<InputListKioskMonitoring>(input);
                    data = SearchKioskMonitoring(listKioskMonitoring.KioskId
                        , listKioskMonitoring.SearchText
                        , listKioskMonitoring.NumberOfItemsPerPage
                        , listKioskMonitoring.CurrentPageNum
                        , listKioskMonitoring.RecordCount
                        , listKioskMonitoring.PageCount
                        , listKioskMonitoring.OrderSelectionColumn
                        , listKioskMonitoring.DescAsc
                        , listKioskMonitoring.OrderType,
                        listKioskMonitoring.StartDate,listKioskMonitoring.EndDate, listKioskMonitoring.InstutionId);
                    break;


                //case FC.EXCEL_LIST_KIOSK_CONDITION:
                //    InputListKioskCondition listKioskMoneyExcel = JsonConvert.DeserializeObject<InputListKioskCondition>(input);
                //    data = SearchKioskConditionExcel(listKioskMoneyExcel.KioskId
                //        , listKioskMoneyExcel.SearchText
                //        , listKioskMoneyExcel.NumberOfItemsPerPage
                //        , listKioskMoneyExcel.CurrentPageNum
                //        , listKioskMoneyExcel.RecordCount
                //        , listKioskMoneyExcel.PageCount
                //        , listKioskMoneyExcel.OrderSelectionColumn
                //        , listKioskMoneyExcel.DescAsc
                //        , listKioskMoneyExcel.KioskType);
                //    break;
                case FC.LIST_KIOSK_CONDITION_DETAIL:
                    InputListKioskConditionDetail listKioskMoneyDetail = JsonConvert.DeserializeObject<InputListKioskConditionDetail>(input);
                    data = GetKioskConditionDetail(listKioskMoneyDetail.KioskId
                        , listKioskMoneyDetail.NumberOfItemsPerPage
                        , listKioskMoneyDetail.CurrentPageNum
                        , listKioskMoneyDetail.RecordCount
                        , listKioskMoneyDetail.PageCount);
                    break;
                case FC.GET_KIOSK_STATUS:
                    data = GetKoskStatusNames();
                    break;
                case FC.GET_KIOSK_CASH_COUNT:
                    InputListCashCounts listCashCounts = JsonConvert.DeserializeObject<InputListCashCounts>(input);
                    data = GetKioskCashCounts(listCashCounts.OperatinType
                                            , listCashCounts.ItemId
                                            , listCashCounts.NumberOfItemsPerPage
                                            , listCashCounts.CurrentPageNum
                                            , listCashCounts.OrderSelectionColumn
                                            , listCashCounts.DescAsc);
                    break;

                case FC.GET_KIOSK_TOTAL_CASH_COUNT:
                    InputListCashCounts listTotalCashCounts = JsonConvert.DeserializeObject<InputListCashCounts>(input);
                    data = GetKioskTotalCashCounts(listTotalCashCounts.StartDate,
                                            listTotalCashCounts.EndDate,
                                            listTotalCashCounts.OperatinType
                                            //, listCashCounts.ItemId
                                            , listTotalCashCounts.NumberOfItemsPerPage
                                            , listTotalCashCounts.CurrentPageNum
                                            , listTotalCashCounts.OrderSelectionColumn
                                            , listTotalCashCounts.DescAsc);
                    break;


                case FC.LIST_KIOSK_CASH_EMPTY:
                    InputListKioskCashEmpty listKioskCashEmpty = JsonConvert.DeserializeObject<InputListKioskCashEmpty>(input);
                    data = SearchKioskCashEmty(listKioskCashEmpty.SearchText
                        , listKioskCashEmpty.StartDate
                        , listKioskCashEmpty.EndDate
                        , listKioskCashEmpty.NumberOfItemsPerPage
                        , listKioskCashEmpty.CurrentPageNum
                        , listKioskCashEmpty.RecordCount
                        , listKioskCashEmpty.PageCount
                        , listKioskCashEmpty.OrderSelectionColumn
                        , listKioskCashEmpty.DescAsc
                        //, listKioskCashEmpty.KioskType
                        , listKioskCashEmpty.InstutionId
                        , listKioskCashEmpty.KioskId
                        , listKioskCashEmpty.OrderType);
                    break;
                //case FC.EXCEL_LIST_KIOSKS:
                //    InputListKiosks listKiosksForExcel = JsonConvert.DeserializeObject<InputListKiosks>(input);
                //    data = SearchKiosksForExcel(listKiosksForExcel.SearchText
                //        , listKiosksForExcel.OrderSelectionColumn
                //        , listKiosksForExcel.DescAsc
                //        , listKiosksForExcel.KioskTypeId);
                //    break;
                //case FC.EXCEL_LIST_KIOSK_CASH_EMPTY:
                //    InputListKioskCashEmpty listKioskCashEmptyForExcel = JsonConvert.DeserializeObject<InputListKioskCashEmpty>(input);
                //    data = SearchKioskCashEmtyForExcel(listKioskCashEmptyForExcel.SearchText
                //        , listKioskCashEmptyForExcel.StartDate
                //        , listKioskCashEmptyForExcel.EndDate
                //        , listKioskCashEmptyForExcel.OrderSelectionColumn
                //        , listKioskCashEmptyForExcel.DescAsc
                //        , listKioskCashEmptyForExcel.KioskType);
                //    break;
                case FC.EXCEL_LIST_KIOSK_CONDITION_DETAIL:
                    InputListKioskConditionDetail listKioskMoneyDetailForExcel = JsonConvert.DeserializeObject<InputListKioskConditionDetail>(input);
                    data = GetKioskConditionDetailForExcel(listKioskMoneyDetailForExcel.KioskId);
                    break;
                case FC.EXCEL_GET_KIOSK_CASH_COUNT:
                    InputListCashCounts listCashCountsForExcel = JsonConvert.DeserializeObject<InputListCashCounts>(input);
                    data = GetKioskCashCountsForExcel(listCashCountsForExcel.OperatinType
                                            , listCashCountsForExcel.ItemId
                                            , listCashCountsForExcel.OrderSelectionColumn
                                            , listCashCountsForExcel.DescAsc);
                    break;

                case FC.GET_CITIES:
                    data = GetCities();
                    break;
                case FC.GET_REGIONS:
                    InputGetRegions cityId = JsonConvert.DeserializeObject<InputGetRegions>(input);
                    data = GetRegions(cityId.cityId);
                    break;
                case FC.GET_KIOSK_CONNECTION_TYPE:
                    data = GetConnectionType();
                    break;
                case FC.LIST_DISTRICT:
                    InputGetRegions cityId2 = JsonConvert.DeserializeObject<InputGetRegions>(input);
                    data = GetDistrict(cityId2.cityId);
                    break;
                case FC.LIST_NEIGHBORHOOD:
                    InputGetDistrict districtId = JsonConvert.DeserializeObject<InputGetDistrict>(input);
                    data = GetNeighborhood(districtId.districtId);
                    break;
                case FC.LIST_DISTRICT_AND_NEIGHBORHOOD:
                    data = GetConnectionType();
                    break;


                case FC.GET_KIOSK_CARD_TYPE:
                    //InputGetCardReaderType cardReaderTypeId = JsonConvert.DeserializeObject<InputGetCardReaderType>(input);
                    data = GetCardReaderType();
                    break;
                case FC.GET_CAPACITIES:
                    data = GetCapacities();
                    break;
                case FC.LIST_KIOSK_REPORT:
                    InputListKioskReport listKiskReport = JsonConvert.DeserializeObject<InputListKioskReport>(input);
                    data = SearchKioskReports(listKiskReport.SearchText
                                            , listKiskReport.StartDate
                                            , listKiskReport.EndDate
                                            , listKiskReport.NumberOfItemsPerPage
                                            , listKiskReport.CurrentPageNum
                                            , listKiskReport.RecordCount
                                            , listKiskReport.PageCount
                                            , listKiskReport.OrderSelectionColumn
                                            , listKiskReport.DescAsc
                                            , listKiskReport.kioskId
                                            , listKiskReport.instutionId);
                    break;
                case FC.LIST_KIOSK_REPORTS:
                    InputListKioskReport listKiskReports = JsonConvert.DeserializeObject<InputListKioskReport>(input);
                    data = SearchNewKioskReports(listKiskReports.SearchText
                                            , listKiskReports.StartDate
                                            , listKiskReports.EndDate
                                            , listKiskReports.NumberOfItemsPerPage
                                            , listKiskReports.CurrentPageNum
                                            , listKiskReports.RecordCount
                                            , listKiskReports.PageCount
                                            , listKiskReports.OrderSelectionColumn
                                            , listKiskReports.DescAsc
                                            , listKiskReports.kioskId
                                            , listKiskReports.instutionId
                                            , listKiskReports.OrderType);
                    break;
                case FC.EXCEL_LIST_KIOSK_REPORT:
                    InputListKioskReport listKiskReportForExcel = JsonConvert.DeserializeObject<InputListKioskReport>(input);
                    data = SearchKioskReportsForExcel(listKiskReportForExcel.SearchText
                                            , listKiskReportForExcel.StartDate
                                            , listKiskReportForExcel.EndDate
                                            , listKiskReportForExcel.OrderSelectionColumn
                                            , listKiskReportForExcel.DescAsc
                                            , listKiskReportForExcel.kioskId
                                            , listKiskReportForExcel.instutionId);
                    break;
                case FC.EXCEL_LIST_KIOSK_REPORT_NEW:
                    InputListKioskReport listKiskReportForExcelNew = JsonConvert.DeserializeObject<InputListKioskReport>(input);
                    data = SearchKioskReportsForExcelNew(listKiskReportForExcelNew.SearchText
                                            , listKiskReportForExcelNew.StartDate
                                            , listKiskReportForExcelNew.EndDate
                                            , listKiskReportForExcelNew.OrderSelectionColumn
                                            , listKiskReportForExcelNew.DescAsc
                                            , listKiskReportForExcelNew.kioskId
                                            , listKiskReportForExcelNew.instutionId);
                    break;
                case FC.LIST_FOUNDATION_NAMES:
                    data = GetInstutionNames();
                    break;


                case FC.LIST_BANK_NAMES:
                    data = GetBankNames();
                    break;
                     
                case FC.LIST_COMPLETESTATUS_NAMES:
                    data = GetCompleteStatusNames();
                    break;

                case FC.LIST_RISK_CLASS:
                    data = GetRiskClass();
                    break;

             

                case FC.LIST_FOUNDATION_NAMES_MUTABAKAT:
                    data = GetInstutionNamesMutabakat();
                    break;

                case FC.LIST_PROCESS_TYPE:
                    data = GetProcessType();
                    break;

                case FC.LIST_TXN_STATUS_NAMES:
                    data = GetTxnStatusNames();
                    break;
                case FC.LIST_DEVICE_NAMES:
                    data = GetDeviceNames();
                    break;
                case FC.LIST_KIOSK_TYPE_NAMES:
                    data = GetKioskTypeNames();
                    break;
                case FC.LIST_INSTUTION_OPERATION_NAMES:
                    data = GetInstutionOperationNames();
                    break;
                case FC.LIST_TXN_CONDITION_NAMES:
                    data = GetTransactionConditionNames();
                    break;
                case FC.LIST_CARD_TYPE_NAMES:
                    data = GetCardTypeNames();
                    break;

                case FC.LIST_KIOSK_FILL_CASHBOX:
                    InputListKioskFillCashBox listKioskFillCashBox = JsonConvert.DeserializeObject<InputListKioskFillCashBox>(input);
                    data = SearchKioskFillCashBox(listKioskFillCashBox.kioskId
                        , listKioskFillCashBox.StartDate
                        , listKioskFillCashBox.EndDate
                        , listKioskFillCashBox.InstutionId
                        , listKioskFillCashBox.NumberOfItemsPerPage
                        , listKioskFillCashBox.CurrentPageNum
                        , listKioskFillCashBox.RecordCount
                        , listKioskFillCashBox.PageCount
                        , listKioskFillCashBox.OrderSelectionColumn
                        , listKioskFillCashBox.DescAsc
                        , listKioskFillCashBox.OrderType);
                    break;

               /* case FC.GET_KIOSK_CASHBOX_REJECT_COUNT:
                    InputListKioskCashBoxRejectCount listKioskCashBoxReject = JsonConvert.DeserializeObject<InputListKioskCashBoxRejectCount>(input);
                    data = SearchKioskCashBoxRejectCount(listKioskFillCashBox.kioskId );
                    break;
*/


               

                //case FC.EXCEL_LIST_KIOSK_FILL_CASHBOX:
                //    InputListKioskFillCashBox listKioskFillCashBoxForExcel = JsonConvert.DeserializeObject<InputListKioskFillCashBox>(input);
                //    data = SearchKioskFillCashBoxForExcel(listKioskFillCashBoxForExcel.kioskId
                //        , listKioskFillCashBoxForExcel.StartDate
                //        , listKioskFillCashBoxForExcel.EndDate
                //        , listKioskFillCashBoxForExcel.OrderSelectionColumn
                //        , listKioskFillCashBoxForExcel.DescAsc);
                //    break;

                case FC.GET_KIOSK_CASHBOX_COUNT:
                    InputListCashBoxCounts listCashBoxCounts = JsonConvert.DeserializeObject<InputListCashBoxCounts>(input);
                    data = GetKioskCashBoxCounts(listCashBoxCounts.OperatinType
                                            , listCashBoxCounts.ItemId
                                            , listCashBoxCounts.NumberOfItemsPerPage
                                            , listCashBoxCounts.CurrentPageNum
                                            , listCashBoxCounts.OrderSelectionColumn
                                            , listCashBoxCounts.DescAsc);
                    break;
                case FC.EXCEL_GET_KIOSK_CASHBOX_COUNT:
                    InputListCashBoxCounts listCashBoxCountsForExcel = JsonConvert.DeserializeObject<InputListCashBoxCounts>(input);
                    data = GetKioskCashBoxCountsForExcel(listCashBoxCountsForExcel.OperatinType
                                            , listCashBoxCountsForExcel.ItemId
                                            , listCashBoxCountsForExcel.OrderSelectionColumn
                                            , listCashBoxCountsForExcel.DescAsc);
                    break;
                case FC.LIST_LOCAL_LOGS:
                    InputListTransactions listLocalLogs = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = SearchLocalLogs(listLocalLogs.SearchText
                        , listLocalLogs.StartDate
                        , listLocalLogs.EndDate
                        , listLocalLogs.NumberOfItemsPerPage
                        , listLocalLogs.CurrentPageNum
                        , listLocalLogs.RecordCount
                        , listLocalLogs.PageCount
                      
                        , listLocalLogs.OrderSelectionColumn       
                        , listLocalLogs.DescAsc 
                        , listLocalLogs.KioskId
                        , listLocalLogs.processType
                        , listLocalLogs.OrderType);
                    break;

                case FC.LIST_ERROR_LOGS:
                    InputListTransactions listErrorLogs = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = SearchErrorLogs(listErrorLogs.SearchText
                        , listErrorLogs.StartDate
                        , listErrorLogs.EndDate
                        , listErrorLogs.NumberOfItemsPerPage
                        , listErrorLogs.CurrentPageNum
                        , listErrorLogs.RecordCount
                        , listErrorLogs.PageCount
                        , listErrorLogs.KioskId
                        , listErrorLogs.processType
                        , listErrorLogs.OrderSelectionColumn
                        , listErrorLogs.DescAsc 
                        , listErrorLogs.OrderType);
                    break;

                case FC.GET_ERROR_LOGS:
                    InputGetKioskErrorLogs errorKiosk = JsonConvert.DeserializeObject<InputGetKioskErrorLogs>(input);
                    data = GetKioskErrorLogs(errorKiosk.errorId);
                    break;

                case FC.GET_REFERENCE_NUMBER:
                    InputGetKioskErrorLogs referenceNumber = JsonConvert.DeserializeObject<InputGetKioskErrorLogs>(input);
                    data = GetKioskReferenceNumber();
                    break;

                case FC.UPDATE_ERROR_LOGS:
                    InputListSaveErrorLogs inputListUpdateErrorLogs = JsonConvert.DeserializeObject<InputListSaveErrorLogs>(input);
                    data = UpdateErrorLogs(inputListUpdateErrorLogs);
                    break;   
                case FC.LIST_MAINTENANCE:
                    InputListTransactions listMaintenance = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = SearchKioskMaintenance(listMaintenance.SearchText
                        , listMaintenance.StartDate
                        , listMaintenance.EndDate
                        , listMaintenance.NumberOfItemsPerPage
                        , listMaintenance.CurrentPageNum
                        , listMaintenance.RecordCount
                        , listMaintenance.PageCount
                        , listMaintenance.OrderSelectionColumn
                        , listMaintenance.DescAsc
                        , listMaintenance.KioskId
                        , listMaintenance.OrderType);
                    break;
                //case FC.EXCEL_LIST_MAINTENANCE:
                //    InputListTransactions listMaintenanceExcel = JsonConvert.DeserializeObject<InputListTransactions>(input);
                //    data = SearchKioskMaintenanceForExcel(listMaintenanceExcel.SearchText
                //        , listMaintenanceExcel.StartDate
                //        , listMaintenanceExcel.EndDate
                //        , listMaintenanceExcel.KioskId);
                //    break;
                case FC.GET_PHONE_NUMBER:
                    InputPhoneNumber listPhoneNumber = JsonConvert.DeserializeObject<InputPhoneNumber>(input);
                    data = SearchForPhoneNumber(listPhoneNumber.transactionId,listPhoneNumber.operationType);
                    break;
                case FC.LIST_DEVICE:
                    InputListTransactions listKioskDevice = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = SearchKioskDevices(listKioskDevice.SearchText
                                            , listKioskDevice.KioskId
                                            , listKioskDevice.CustomerId
                                            , listKioskDevice.NumberOfItemsPerPage
                                            , listKioskDevice.CurrentPageNum
                                            , listKioskDevice.RecordCount
                                            , listKioskDevice.PageCount);
                    break;

                case FC.GET_MANAGECONTEXT_ROLES:
                    var datacpy = JObject.Parse(input)["o"];
                    var str = JsonConvert.SerializeObject(datacpy);
                    data = JObject.Parse(str);
                    break;

                default:
                    break;
            }

            ManageContextRolescs contextRoles = new ManageContextRolescs();
            contextRoles.EditByRoles(functionCode, data, adminUserId, pageUrl);

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "PrepareResponseKiosk");
        }
        return data;
    }

    private JObject SearchLocalLogs(string searchText
                            , DateTime startDate
                            , DateTime endDate
                            , int numberOfItemsPerPage
                            , int currentPageNum
                            , int recordCount
                            , int pageCount
                            , int orderSelectionColumn
                            , int descAsc
                            , string kioskId
                            , int processType
                            , int orderType)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            KioskOpr kioskOpr = new KioskOpr();
            LocalLogCollection locallogs = kioskOpr.SearchLocalLogs(searchText, startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, kioskId, processType, orderType);


            kioskOpr.Dispose();
            kioskOpr = null;

            if (locallogs != null & locallogs.Count > 0)
            {
                data = locallogs.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchLocalLogs");
        }
        return data;

    }



    private JObject GetKioskReferenceNumber()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr opr = new KioskOpr();
            KioskReferenceNumber kioskReferenceNumber = opr.GetKioskReferenceNumberOrder();
            opr.Dispose();
            opr = null;

            if (kioskReferenceNumber != null)
            {
                data = GetJSON(kioskReferenceNumber);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetKioskReferenceNumber");
        }
        return data;
    }

    private JObject SearchErrorLogs(string searchText
                        , DateTime startDate
                        , DateTime endDate
                        , int numberOfItemsPerPage
                        , int currentPageNum
                        , int recordCount
                        , int pageCount
                        , string kioskId
                        , int processType
                        , int orderSelectionColumn
                        , int descAsc
                        , int orderType)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            KioskOpr kioskOpr = new KioskOpr();
            ErrorLogCollection locallogs = kioskOpr.SearchErrorLogs(searchText, startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, kioskId, processType, orderSelectionColumn, descAsc, orderType);


            kioskOpr.Dispose();
            kioskOpr = null;

            if (locallogs != null & locallogs.Count > 0)
            {
                data = locallogs.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchErrorLogs");
        }
        return data;

    }

    private JObject SearchKioskMaintenance(string searchText
                        , DateTime startDate
                        , DateTime endDate
                        , int numberOfItemsPerPage
                        , int currentPageNum
                        , int recordCount
                        , int pageCount
                        , int orderSelectionColumn
                        , int descAsc
                        , string kioskId
                        , int orderType)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            KioskOpr kioskOpr = new KioskOpr();
            MaintenanceCollection maintenance = kioskOpr.SearchKioskMaintenance(searchText, startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn ,descAsc, kioskId, orderType);


            kioskOpr.Dispose();
            kioskOpr = null;

            if (maintenance != null & maintenance.Count > 0)
            {
                data = maintenance.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskMaintenance");
        }
        return data;

    }

    //private JObject SearchKioskMaintenanceForExcel(string searchText
    //                , DateTime startDate
    //                , DateTime endDate
    //                , string kioskId)
    //{

    //    JObject data = new JObject();
    //    try
    //    {
    //        KioskOpr kioskOpr = new KioskOpr();
    //        MaintenanceCollection maintenance = kioskOpr.SearchKioskMaintenanceForExcel(searchText, startDate, endDate, kioskId);


    //        kioskOpr.Dispose();
    //        kioskOpr = null;

    //        if (maintenance != null & maintenance.Count > 0)
    //        {
    //            data = maintenance.GetJSON();
    //            errorCode = ReturnCodes.SUCCESSFULL;
    //        }
    //        else
    //        {
    //            errorCode = ReturnCodes.NO_DATA_FOUND;
    //        }
    //    }
    //    catch (Exception exp)
    //    {
    //        errorCode = ReturnCodes.SYSTEM_ERROR;
    //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskMaintenanceForExcel");
    //    }
    //    return data;

    //}

    private JObject SearchForPhoneNumber(string transactionId,string operationType)
    {
        JObject data = new JObject();
        string phoneNumber; 
        try
        {
            MoneyCodeOpr moneyOpr = new MoneyCodeOpr();
            phoneNumber = moneyOpr.SearchPhoneNumber(Convert.ToInt64(transactionId), Convert.ToByte(operationType));

            data.Add("PhoneNumber", phoneNumber);

            moneyOpr.Dispose();
            moneyOpr = null;

        
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetMobilePhoneNumber");
        }

        return data;

    }


    private JObject SearchKioskDevices(string searchText 
                                        , string kioskId
                                        , int deviceKindId
                                        , int numberOfItemsPerPage
                                        , int currentPageNum
                                        , int recordCount
                                        , int pageCount)
                    {

                        JObject data = new JObject();
                        try
                        {
                            KioskOpr kioskOpr = new KioskOpr();
                            HardwareDeviceCollection devices = kioskOpr.SearchKioskDevices(searchText, kioskId, deviceKindId, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount);

                            kioskOpr.Dispose();
                            kioskOpr = null;

                            if (devices != null & devices.Count > 0)
                            {
                                data = devices.GetJSON();
                                errorCode = ReturnCodes.SUCCESSFULL;
                            }
                            else
                            {
                                errorCode = ReturnCodes.NO_DATA_FOUND;
                            }
                            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
                        }
                        catch (Exception exp)
                        {
                            errorCode = ReturnCodes.SYSTEM_ERROR;
                            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskDevices");
                        }
                        return data;

                    }




    private JObject UpdateSource(int id, string explanation, string link, int sourceCategoryId, int sourceCategoryTypeId, int sourceTypeId)
    {


        JObject data = new JObject();
        try
        {

            KioskOpr koskOpr = new KioskOpr();

            var sources = koskOpr.UpdateSourceOrder(id, explanation, link, sourceCategoryId, sourceCategoryTypeId, sourceTypeId);
            koskOpr.Dispose();
            koskOpr = null;
            if (sources)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKiosks");
        }
        return data;
    }

    private JObject AddSource(string explanation, int createdUserId, string createdUserName, string link, int sourceCategoryId, int sourceCategoryTypeId, int sourceTypeId)
    {


        JObject data = new JObject();
        try
        {

            KioskOpr koskOpr = new KioskOpr();

            var sources = koskOpr.AddSourceOrder(explanation, createdUserId, createdUserName,  link,  sourceCategoryId, sourceCategoryTypeId, sourceTypeId);
            koskOpr.Dispose();
            koskOpr = null;
            if (sources)
            {
                errorCode = ReturnCodes.SUCCESSFULL;

                try
                {
                    //using (SendEmailProject.SendEmailSoapClient client = new SendEmailProject.SendEmailSoapClient())
                    //{
                    //    SendEmailProject.EmailResult result =
                    //    client.SendEmailDekont("info@birlesikodeme.com", "Birleşik Ödeme Hizmetleri",
                    //    "Yeni Dosya Eklendi", "", 0, null, new SendEmailProject.ArrayOfString { "efe.mayoglu@birlesikodeme.com" }, new SendEmailProject.DekontFields());
                    //}
                }
                catch (Exception ex)
                {

                    throw;
                }
              
            
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKiosks");
        }
        return data;
    }



    private JObject SearchSourceType(int id = 0)
    {
        JObject data = new JObject();
        try
        {

            KioskOpr koskOpr = new KioskOpr();

            var sources = koskOpr.SearchSourceTypeOrder(id);
            koskOpr.Dispose();
            koskOpr = null;
            if (sources != null & sources.Count > 0)
            {
                data = GetJSON(sources);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKiosks");
        }
        return data;
    }


    private JObject SearchSourceCategoryType(int id = 0)
    {
        JObject data = new JObject();
        try
        {

            KioskOpr koskOpr = new KioskOpr();

            var sources = koskOpr.SearchSourceCategoryTypeOrder(id);
            koskOpr.Dispose();
            koskOpr = null;
            if (sources != null & sources.Count > 0)
            {
                data = GetJSON(sources);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKiosks");
        }
        return data;
    }

    private JObject SearchSourceCategory(int id = 0)
    {
        JObject data = new JObject();
        try
        {

            KioskOpr koskOpr = new KioskOpr();

            var sources = koskOpr.SearchSourceCategoryOrder(id);
            koskOpr.Dispose();
            koskOpr = null;
            if (sources != null & sources.Count > 0)
            {
                data = GetJSON(sources);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKiosks");
        }
        return data;
    }



    private JObject SaveSystemSetting(int id ,long adminUserId ,int instutionId, int newStatus, int approvedUserId, string screenMessage)
    {
        JObject data = new JObject();
        try
        {

            KioskOpr koskOpr = new KioskOpr();

            var sources = koskOpr.SaveSystemSettingOrder(id, adminUserId, instutionId,newStatus,approvedUserId, screenMessage);
            koskOpr.Dispose();
            koskOpr = null;
            if (sources)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKiosks");
        }
        return data;
    }
    

      private JObject DeleteSystemSetting(int id, long adminUserId)
    {
        JObject data = new JObject();
        try
        {

            KioskOpr koskOpr = new KioskOpr();

            var sources = koskOpr.DeleteSystemSettingOrder(id, adminUserId);
            koskOpr.Dispose();
            koskOpr = null;
            if (sources)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKiosks");
        }
        return data;
    }

    private JObject UpdateSystemSetting(int id, long adminUserId)
    {
        JObject data = new JObject();
        try
        {

            KioskOpr koskOpr = new KioskOpr();

            var sources = koskOpr.UpdateSystemSettingOrder(id, adminUserId);
            koskOpr.Dispose();
            koskOpr = null;
            if (sources)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKiosks");
        }
        return data;
    }


    private JObject SaveEmployeeScheduling(int id, string title, bool allDay, string backgrounColor, string borderColor, DateTime startDate, DateTime endDate, bool isDeleted)
    {
        JObject data = new JObject();
       try
        {

            KioskOpr koskOpr = new KioskOpr();

            var sources = koskOpr.SaveEmployeeSchedulingOrder(id, title,  allDay,  backgrounColor,  borderColor,  startDate,  endDate,isDeleted);
            koskOpr.Dispose();
            koskOpr = null;
            if (sources)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKiosks");
        }
        return data;
    }


        private JObject SearchListKioskReports(InputListKiosksReportsNew myInput)
    {
        JObject data = new JObject();
        try
        {

            KioskOpr koskOpr = new KioskOpr();
            var b = 0;
            var sources = koskOpr.SearchListKiosksReportsNewsOrder(myInput,ref b,ref b);
            koskOpr.Dispose();
            koskOpr = null;
            if (sources != null & sources.Count > 0)
            {
                data = GetJSON(sources);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKiosks");
        }
        return data;
    }
    private JObject SearchSystemSetting( DateTime startDate, DateTime endDate)
    {
        JObject data = new JObject();
        try
        {

            KioskOpr koskOpr = new KioskOpr();

            var sources = koskOpr.SearchSystemSettingOrder(startDate, endDate);
            koskOpr.Dispose();
            koskOpr = null;
            if (sources != null & sources.Count > 0)
            {
                data = GetJSON(sources);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKiosks");
        }
        return data;
    }


    private JObject SearchEmployeeScheduling(int id, DateTime startDate, DateTime endDate)
    {
        JObject data = new JObject();
        try
        {

            KioskOpr koskOpr = new KioskOpr();

            var sources = koskOpr.SearchEmployeeSchedulingOrder(id, startDate, endDate);
            koskOpr.Dispose();
            koskOpr = null;
            if (sources != null & sources.Count > 0)
            {
                data = GetJSON(sources);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKiosks");
        }
        return data;
    }




    private JObject SearchSource(int categoryId,int id=-1)
    {
        JObject data = new JObject();
        try
        {
         
            KioskOpr koskOpr = new KioskOpr();

            var sources = koskOpr.SearchSourceOrder(categoryId, id);
            koskOpr.Dispose();
            koskOpr = null;
            if (sources != null & sources.Count > 0)
            {
                data = GetJSON(sources);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKiosks");
        }
        return data;
    }

    private JObject SearchKiosks(string searchText
                                , int numberOfItemsPerPage
                                , int currentPageNum
                                , int recordCount
                                , int pageCount
                                , int orderSelectionColumn
                                , int descAsc
                                , string kioskId
                                , int kioskTypeId
                                , int orderType
                                , int instutionId
                                , int connectionId
                                , int cardReaderId
                                , int statusId
                                , int regionId
                                , int cityId
                                , int riskId
                                , int bankNameId
        , string cameraIp, byte channelNumber, string userName, string password
        )


    {
        JObject data = new JObject();

        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;
            DateTime serverTime = DateTime.Now;

            KioskOpr koskOpr = new KioskOpr();

            this.kiosks = koskOpr.SearchKiosksOrder(searchText, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, kioskId, kioskTypeId, ref serverTime, orderType, instutionId, connectionId, cardReaderId, statusId, regionId, cityId, riskId, bankNameId, cameraIp,channelNumber,userName,password);
            koskOpr.Dispose();
            koskOpr = null;

            if (this.kiosks != null & this.kiosks.Count > 0)
            {
                data = this.kiosks.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
            data.Add("serverTime", serverTime);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKiosks");
        }
        return data;
    }

    private JObject SearchKioskDispenserErrors(string searchText
                                , int numberOfItemsPerPage
                                , int currentPageNum
                                , int recordCount
                                , int pageCount
                                , int orderSelectionColumn
                                , int descAsc
                                , string kioskId
                                , int orderType
                                , int instutionId
                                , int errorId)
    {
        JObject data = new JObject();

        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;
            DateTime serverTime = DateTime.Now;

            KioskOpr koskOpr = new KioskOpr();

            KioskDispenserErrorCollection kiosks = koskOpr.SearchKioskDispenserErrorsOrder(searchText, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, kioskId, orderType, instutionId, errorId);
            koskOpr.Dispose();
            koskOpr = null;

            if (kiosks != null & kiosks.Count > 0)
            {
                data = kiosks.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKiosks");
        }
        return data;
    }
    //private JObject SearchKiosksForExcel(string searchText
    //                           , int orderSelectionColumn
    //                           , int descAsc
    //                           , int kioskTypeId)
    //{

    //    JObject data = new JObject();

    //    try
    //    {
    //        KioskOpr koskOpr = new KioskOpr();

    //        this.kiosks = koskOpr.SearchKiosksOrderForExcel(searchText, orderSelectionColumn, descAsc, kioskTypeId);
    //        koskOpr.Dispose();
    //        koskOpr = null;

    //        if (this.kiosks != null & this.kiosks.Count > 0)
    //        {
    //            data = this.kiosks.GetJSON();
    //            errorCode = ReturnCodes.SUCCESSFULL;
    //        }
    //        else
    //        {
    //            errorCode = ReturnCodes.NO_DATA_FOUND;
    //        }

    //    }
    //    catch (Exception exp)
    //    {
    //        errorCode = ReturnCodes.SYSTEM_ERROR;
    //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKiosksForExcel");
    //    }
    //    return data;
    //}

    private JObject SearchKioskCondition(string kioskId
                               , string searchText
                               , int numberOfItemsPerPage
                               , int currentPageNum
                               , int recordCount
                               , int pageCount
                               , int orderSelectionColumn
                               , int descAsc
                               , int kioskType
                               , int orderType)
    {

        JObject data = new JObject();

        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;
            string TotalAmount = "";
            string TotalCashCount = "";
            string TotalCoinCount = "";
            string TotalDispenserAmount = "";
            string TotalDispenserCount = "";

            KioskOpr koskOpr = new KioskOpr();

            KioskConditionCollection kioskMoney = koskOpr.SearchKioskConditionOrder(kioskId, 
                                                                                    searchText, 
                                                                                    numberOfItemsPerPage, 
                                                                                    currentPageNum, 
                                                                                    ref recordCount, 
                                                                                    ref pageCount,
                                                                                    ref TotalAmount,
                                                                                    ref TotalCashCount,
                                                                                    ref TotalCoinCount,
                                                                                    ref TotalDispenserAmount,
                                                                                    ref TotalDispenserCount,
                                                                                    orderSelectionColumn, 
                                                                                    descAsc,
                                                                                    kioskType,
                                                                                    orderType);
            koskOpr.Dispose();
            koskOpr = null;

            if (kioskMoney != null & kioskMoney.Count > 0)
            {
                data = kioskMoney.GetJSON();
                data.Add("TotalAmount", TotalAmount);
                data.Add("TotalCashCount", TotalCashCount);
                data.Add("TotalCoinCount", TotalCoinCount);
                // data.Add("TotalDispenserAmount", TotalDispenserAmount);
                // data.Add("TotalDispenserCount", TotalDispenserCount);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskMoney");
        }
        return data;
    }
    private JObject UpdateKioskReferenceSystem(int Id,int referenceNo, long adminUserId)
    {
        JObject data = new JObject();
        try
        {

            KioskOpr koskOpr = new KioskOpr();
            int kca = koskOpr.UpdateKioskReferenceSystemOrder(Id,referenceNo, adminUserId);
            koskOpr.Dispose();
            koskOpr = null;


            if (kca==1)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskReferenceSystemOrder");
        }
        return data;

    }



    private JObject GetKioskReferenceSystem(int referenceNo)
    {
        JObject data = new JObject();
        try
        {
       
            KioskOpr koskOpr = new KioskOpr();
            KioskGetReferenceSystem kca = koskOpr.GetKioskReferenceSystemOrder(referenceNo);
            koskOpr.Dispose();
            koskOpr = null;

            if (kca != null)
            {

                data = GetJSON(kca);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            } 
 
             
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskReferenceSystemOrder");
        }
        return data;
 
    }





    private JObject SearchKioskReferenceSystem(string searhText
                                           , DateTime startDate
                                            , DateTime endDate
                                           , int referenceStatus
                                           , int numberOfItemsPerPage
                                           , int currentPageNum
                                           , int recordCount
                                           , int pageCount
                                           , int orderSelectionColumn
                                           , int descAsc
                                           , int instutionId
                                           , string kioskId
                                           , int orderType)
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;



            KioskOpr koskOpr = new KioskOpr();
            KioskReferenceSystemCollection kca = koskOpr.SearchKioskReferenceSystemOrder(searhText, 
                                                                                        startDate, 
                                                                                        endDate, 
                                                                                        referenceStatus,
                                                                                        numberOfItemsPerPage, 
                                                                                        currentPageNum, 
                                                                                        ref recordCount, 
                                                                                        ref pageCount, 
                                                                                        orderSelectionColumn, 
                                                                                        descAsc, 
                                                                                        instutionId, 
                                                                                        kioskId,
                                                                                        orderType);


            koskOpr.Dispose();
            koskOpr = null;

            if (kca != null & kca.Count > 0)
            {
                data = kca.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }


            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);

 
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskReferenceSystemOrder");
        }
        return data;
    }



    private JObject SaveKioskReferenceSystem(InputSaveKioskReferenceSystem inputListSaveKioskReferenceSystem)
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOprr = new KioskOpr();

            int resp = koskOprr.SaveKioskReferenceSystemOrder(inputListSaveKioskReferenceSystem.adminUserId,
                                                     inputListSaveKioskReferenceSystem.referenceNo,
                                                     inputListSaveKioskReferenceSystem.kioskId,
                                                     inputListSaveKioskReferenceSystem.explanation,
                                                     "");

            koskOprr.Dispose();
            koskOprr = null;

            if (resp == 1)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveKioskReferenceSystemOrder");
        }

        return data;
    }



    //UpdateKioskConditionDispenserCashCount
    private JObject UpdateKioskConditionDispenserCashCount(InputListKioskCondition dispenserCashCount)
    {


        JObject data = new JObject();
        try
        {

            KioskOpr opr = new KioskOpr();
            int resp = opr.UpdateKioskConditionDispenserCashCountOrder(dispenserCashCount.KioskId,
                                                dispenserCashCount.dispenser,
                                                dispenserCashCount.dispenserAmount
                                                );

            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

            opr.Dispose();
            opr = null;

            if (resp == 0)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UpdateKioskConditionDispenserCashCount");
        }
        return data;
    }

    private JObject SearchKioskMonitoring(string kioskId
                               , string searchText
                               , int numberOfItemsPerPage
                               , int currentPageNum
                               , int recordCount
                               , int pageCount
                               , int orderSelectionColumn
                               , int descAsc
                             
                               , int orderType,
                                 DateTime startDate, DateTime endDate, int instutionId)
    {

        JObject data = new JObject();

        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;
         

            KioskOpr koskOpr = new KioskOpr();

            KioskMonitoringCollection kioskMoney = koskOpr.SearchKioskMonitoringOrder(kioskId,
                                                                                    searchText,
                                                                                    numberOfItemsPerPage,
                                                                                    currentPageNum,
                                                                                    ref recordCount,
                                                                                    ref pageCount, 
                                                                                    orderSelectionColumn,
                                                                                    descAsc,                                                                                   
                                                                                    orderType,startDate, endDate, instutionId);
            koskOpr.Dispose();
            koskOpr = null;

            if (kioskMoney != null & kioskMoney.Count > 0)
            {
                data = kioskMoney.GetJSON();
                data.Add("recordCount", recordCount);
                data.Add("pageCount", pageCount);
                //data.Add("TotalCoinCount", TotalCoinCount);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

           // data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskMoney");
        }
        return data;
    }

    //private JObject SearchKioskConditionExcel(string kioskId
    //                           , string searchText
    //                           , int numberOfItemsPerPage
    //                           , int currentPageNum
    //                           , int recordCount
    //                           , int pageCount
    //                           , int orderSelectionColumn
    //                           , int descAsc
    //                           , int kioskType)
    //{

    //    JObject data = new JObject();

    //    try
    //    {
    //        int recordCnt = recordCount;
    //        int pageCnt = pageCount;
    //        string TotalAmount = "";
    //        string TotalCashCount = "";
    //        string TotalCoinCount = "";


    //        KioskOpr koskOpr = new KioskOpr();

    //        KioskConditionCollection kioskMoney = koskOpr.SearchKioskConditionExcelOrder(kioskId,
    //                                                                                searchText,
    //                                                                                numberOfItemsPerPage,
    //                                                                                currentPageNum,
    //                                                                                ref recordCount,
    //                                                                                ref pageCount,
    //                                                                                ref TotalAmount,
    //                                                                                ref TotalCashCount,
    //                                                                                ref TotalCoinCount,
    //                                                                                orderSelectionColumn,
    //                                                                                descAsc,
    //                                                                                kioskType);
    //        koskOpr.Dispose();
    //        koskOpr = null;

    //        if (kioskMoney != null & kioskMoney.Count > 0)
    //        {
    //            data = kioskMoney.GetJSON();
    //            errorCode = ReturnCodes.SUCCESSFULL;
    //        }
    //        else
    //        {
    //            errorCode = ReturnCodes.NO_DATA_FOUND;
    //        }

    //    }
    //    catch (Exception exp)
    //    {
    //        errorCode = ReturnCodes.SYSTEM_ERROR;
    //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskMoneyExcel");
    //    }
    //    return data;
    //}

    private JObject SearchKioskCashEmty(string searchText
                                      , DateTime startDate
                                      , DateTime endDate
                                      , int numberOfItemsPerPage
                                      , int currentPageNum
                                      , int recordCount
                                      , int pageCount
                                      , int orderSelectionColumn
                                      , int descAsc
                                      //, int kioskType
                                      , int instutionId
                                      ,  string kioskId
                                      , int orderType)
    {

        JObject data = new JObject();

        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;
            string TotalAmount = "";
            string TotalCashCount = "";
            string TotalCoinCount = "";


            KioskOpr koskOpr = new KioskOpr();

            KioskCashEmptyCollection item = koskOpr.SearchKioskCashEmptyOrder(searchText, 
                                                                              startDate, 
                                                                              endDate, 
                                                                              numberOfItemsPerPage, 
                                                                              currentPageNum, 
                                                                              ref recordCount, 
                                                                              ref pageCount,
                                                                              ref  TotalAmount,
                                                                              ref  TotalCashCount,
                                                                              ref  TotalCoinCount,
                                                                              orderSelectionColumn, 
                                                                              descAsc,
                                                                              //kioskType,
                                                                              instutionId,
                                                                                kioskId,
                                                                              orderType);
            koskOpr.Dispose();
            koskOpr = null;

            if (item != null & item.Count > 0)
            {
                data = item.GetJSON();
                data.Add("TotalAmount", TotalAmount);
                data.Add("TotalCashCount", TotalCashCount);
                data.Add("TotalCoinCount", TotalCoinCount);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskCashEmpty");
        }
        return data;
    }

    //private JObject SearchKioskCashEmtyForExcel(string searchText
    //                                          , DateTime startDate
    //                                          , DateTime endDate
    //                                          , int orderSelectionColumn
    //                                          , int descAsc
    //                                          , int kioskType)
    //{
    //    JObject data = new JObject();

    //    try
    //    {
    //        KioskOpr koskOpr = new KioskOpr();

    //        KioskCashEmptyCollection item = koskOpr.SearchKioskCashEmptyOrderForExcel(searchText, startDate, endDate, orderSelectionColumn, descAsc, kioskType);
    //        koskOpr.Dispose();
    //        koskOpr = null;

    //        if (item != null & item.Count > 0)
    //        {
    //            data = item.GetJSON();
    //            errorCode = ReturnCodes.SUCCESSFULL;
    //        }
    //        else
    //        {
    //            errorCode = ReturnCodes.NO_DATA_FOUND;
    //        }
    //    }
    //    catch (Exception exp)
    //    {
    //        errorCode = ReturnCodes.SYSTEM_ERROR;
    //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskCashEmptyForExcel");
    //    }
    //    return data;
    //}




    ////SearchKioskCashBoxRejectCount
    //private JObject SearchKioskCashBoxRejectCount(string kioskId)
    //{

    //    JObject data = new JObject();
    //    KioskReportCollections collection = null;
    //    try
    //    {
    //        //int recordCnt = recordCount;
    //        //int pageCnt = pageCount;

    //        Int64 TotalTxnCount = 0;
    //        string TotalGeneratedAskiAmount = "";
    //        string TotalUsedAskiAmount = "";
    //        Int64 TotalGeneratedAskiCount = 0;
    //        Int64 TotalUsedAskiCount = 0;
    //        string TotalReceivedAmount = "";
    //        string TotalKioskEmptyAmount = "";
    //        string TotalEarnedAmount = "";
    //        string TotalParaUstu = "";
    //        string TotalSuccesTxnCount = "";
    //        string TotalInstitutionAmount = "";
    //        string TotalTotalAmount = "";
    //        string TotalCreditCardAmount = "";
    //        string TotalCommisionAmount = "";

    //        KioskOpr opr = new KioskOpr();

    //        collection = opr.SearchKioskCashBoxRejectCountOrder(kioskId,
    //            ref TotalTxnCount,
    //            ref TotalGeneratedAskiAmount,
    //            ref TotalUsedAskiAmount,
    //            ref TotalGeneratedAskiCount,
    //            ref TotalUsedAskiCount,
    //            ref TotalReceivedAmount,
    //            ref TotalKioskEmptyAmount,
    //            ref TotalEarnedAmount,
    //            ref TotalParaUstu,
    //            ref TotalSuccesTxnCount,
    //            ref TotalInstitutionAmount,
    //            ref TotalTotalAmount,
    //            ref TotalCreditCardAmount,
    //            ref TotalCommisionAmount);

    //        opr.Dispose();
    //        opr = null;

    //        //if (collection != null & collection.Count > 0)
    //        //{
    //        //    data = collection.GetJSON();
    //        //    errorCode = ReturnCodes.SUCCESSFULL;
    //        //}
    //        //else
    //        //{
    //        //    errorCode = ReturnCodes.NO_DATA_FOUND;
    //        //}

    //        // data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
    //        data.Add("TotalTxnCount", TotalTxnCount);
    //        data.Add("TotalGeneratedAskiAmount", TotalGeneratedAskiAmount);
    //        data.Add("TotalUsedAskiAmount", TotalUsedAskiAmount);
    //        data.Add("TotalGeneratedAskiCount", TotalGeneratedAskiCount);
    //        data.Add("TotalUsedAskiCount", TotalUsedAskiCount);
    //        data.Add("TotalReceivedAmount", TotalReceivedAmount);
    //        data.Add("TotalKioskEmptyAmount", TotalKioskEmptyAmount);
    //        data.Add("TotalEarnedAmount", TotalEarnedAmount);
    //        data.Add("TotalParaUstu", TotalParaUstu);
    //        data.Add("TotalSuccesTxnCount", TotalSuccesTxnCount);
    //        data.Add("TotalInstitutionAmount", TotalInstitutionAmount);
    //        data.Add("TotalTotalAmount", TotalTotalAmount);
    //        data.Add("TotalCreditCardAmount", TotalCreditCardAmount);
    //        data.Add("TotalCommisionAmount", TotalCommisionAmount);
    //    }
    //    catch (Exception exp)
    //    {
    //        errorCode = ReturnCodes.SYSTEM_ERROR;
    //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskCashBoxRejectCountOrder");
    //    }
    //    return data;
    //}

    private JObject SearchKioskFillCashBox(string kioskId
                                      , DateTime startDate
                                      , DateTime endDate
                                      , int instutionId
                                      , int numberOfItemsPerPage
                                      , int currentPageNum
                                      , int recordCount
                                      , int pageCount
                                      , int orderSelectionColumn
                                      , int descAsc
                                      , int orderType)
    {

        JObject data = new JObject();
        string TotalAmount = "";
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;


            KioskOpr koskOpr = new KioskOpr();

            KioskFillCashBoxCollection item = koskOpr.SearchKioskFillCashBoxOrder(kioskId
                                                                                , startDate
                                                                                , endDate
                                                                                , instutionId
                                                                                , numberOfItemsPerPage
                                                                                , currentPageNum
                                                                                , ref recordCount
                                                                                , ref pageCount
                                                                                , orderSelectionColumn
                                                                                , descAsc
                                                                                , ref TotalAmount
                                                                                , orderType);
            koskOpr.Dispose();
            koskOpr = null;

            if (item != null & item.Count > 0)
            {
                data = item.GetJSON();
                data.Add("TotalAmount", TotalAmount);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskCashEmpty");
        }
        return data;
    }

   

    //private JObject SearchKioskFillCashBoxForExcel(string kioskId
    //                                          , DateTime startDate
    //                                          , DateTime endDate
    //                                          , int orderSelectionColumn
    //                                          , int descAsc)
    //{
    //    JObject data = new JObject();

    //    try
    //    {
    //        KioskOpr koskOpr = new KioskOpr();

    //        KioskFillCashBoxCollection item = koskOpr.SearchKioskFillCashBoxOrderForExcel(kioskId, startDate, endDate, orderSelectionColumn, descAsc);
    //        koskOpr.Dispose();
    //        koskOpr = null;

    //        if (item != null & item.Count > 0)
    //        {
    //            data = item.GetJSON();
    //            errorCode = ReturnCodes.SUCCESSFULL;
    //        }
    //        else
    //        {
    //            errorCode = ReturnCodes.NO_DATA_FOUND;
    //        }
    //    }
    //    catch (Exception exp)
    //    {
    //        errorCode = ReturnCodes.SYSTEM_ERROR;
    //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskCashEmptyForExcel");
    //    }
    //    return data;
    //}

    private JObject GetKioskConditionDetail(int kioskId
                               , int numberOfItemsPerPage
                               , int currentPageNum
                               , int recordCount
                               , int pageCount)
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            KioskOpr koskOpr = new KioskOpr();

            KioskConditionDetailCollection kioskMoney = koskOpr.GetKioskConditionDetailOrder(kioskId, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount);
            koskOpr.Dispose();
            koskOpr = null;

            if (kioskMoney != null & kioskMoney.Count > 0)
            {
                data = kioskMoney.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetKioskMoneyDetail");
        }
        return data;
    }

    private JObject GetKioskConditionDetailForExcel(int kioskId)
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();

            KioskConditionDetailCollection kioskMoney = koskOpr.GetKioskConditionDetailOrderForExcel(kioskId);
            koskOpr.Dispose();
            koskOpr = null;

            if (kioskMoney != null & kioskMoney.Count > 0)
            {
                data = kioskMoney.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetKioskMoneyDetailForExcel");
        }
        return data;
    }

    private JObject SearchKioskCommands(string searchText
                               , DateTime startDate
                               , DateTime endDate
                               , string kioskId
                               , int instutionId
                               , int numberOfItemsPerPage
                               , int currentPageNum
                               , int recordCount
                               , int pageCount
                               , int orderSelectionColumn
                               , int descAsc
                               , int orderType)
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            KioskOpr koskOpr = new KioskOpr();
            KioskCommandCollection kioskCommands = koskOpr.SearchKioskCommandsOrder(searchText, startDate, endDate, kioskId, instutionId,numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, orderType);
            koskOpr.Dispose();
            koskOpr = null;

            if (kioskCommands != null & kioskCommands.Count > 0)
            {
                data = kioskCommands.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskCommands");
        }
        return data;
    }

    
    private JObject GetKioskCommand(int commandId)
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            KioskCommand commandkiosk = koskOpr.GetKioskCommandOrder(commandId);
            koskOpr.Dispose();
            koskOpr = null;

            if (commandkiosk != null)
            {
                data = GetJSON(commandkiosk);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetKioskCommand");
        }
        return data;
    }

    public JObject GetJSON(KioskCommand thecommandkiosk)
    {
        JObject jObjectDis = new JObject();
        string a = JsonConvert.SerializeObject(thecommandkiosk, Newtonsoft.Json.Formatting.None);
        JObject x = JObject.Parse(a);
        jObjectDis.Add("GetKioskCommand", x);
        return jObjectDis;
    }//todo del

    public JObject GetJSON(List< OutputListKiosksReportsNew> thecommandkiosk)
    {
        JObject jObjectDis = new JObject();
        string a = JsonConvert.SerializeObject(thecommandkiosk, Newtonsoft.Json.Formatting.None);
        JArray x = JArray.Parse(a);
        jObjectDis.Add("KioskReports", x);
        return jObjectDis;
    }//todo del

    public JObject GetJSON(List<PRCNCORE.Input.SystemSetting> thecommandkiosk)
    {
        JObject jObjectDis = new JObject();
        string a = JsonConvert.SerializeObject(thecommandkiosk, Newtonsoft.Json.Formatting.None);
        JArray x = JArray.Parse(a);
        jObjectDis.Add("GetSystemSetting", x);
        return jObjectDis;
    }//todo del

    public JObject GetJSON(List<EmployeeScheduling> thecommandkiosk)
    {
        JObject jObjectDis = new JObject();
        string a = JsonConvert.SerializeObject(thecommandkiosk, Newtonsoft.Json.Formatting.None);
        JArray x = JArray.Parse(a);
        jObjectDis.Add("GetEmployeeScheduling", x);
        return jObjectDis;
    }//todo del

    public JObject GetJSON(List<Sources> thecommandkiosk)
    {
        JObject jObjectDis = new JObject();
        string a = JsonConvert.SerializeObject(thecommandkiosk, Newtonsoft.Json.Formatting.None);
        JArray x = JArray.Parse(a);
        jObjectDis.Add("GetSources", x);
        return jObjectDis;
    }//todo del
    public JObject GetJSON(KioskGetReferenceSystem kioskReferenceSystem)
    {
        JObject jObjectDis = new JObject();
        string a = JsonConvert.SerializeObject(kioskReferenceSystem, Newtonsoft.Json.Formatting.None);
        JObject x = JObject.Parse(a);
        jObjectDis.Add("KioskReferenceSystem", x);
        return jObjectDis;
    }

    public JObject GetJSON(JObject data, object collection, string propertyName)
    {
        string a = JsonConvert.SerializeObject(collection, Newtonsoft.Json.Formatting.None);
        JObject x = JObject.Parse(a);
        data.Add(propertyName, x);
        return data;
    }

    public JObject GetJSON(KioskReferenceNumber kioskReference)
    {
        JObject jObjectDis = new JObject();
        string a = JsonConvert.SerializeObject(kioskReference, Newtonsoft.Json.Formatting.None);
        JObject x = JObject.Parse(a);
        jObjectDis.Add("GetKioskReferenceNumber", x);
        return jObjectDis;
    }
   

    private JObject SaveKioskCommand(int kioskId, int  institutionId, int createdUserId, int commandTypeId, string commandString,int commandTypeReason, string explanation)
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            int resp = koskOpr.SaveKioskCommand(kioskId, institutionId, createdUserId, commandTypeId, commandString, commandTypeReason, explanation);
            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;
            koskOpr.Dispose();
            koskOpr = null;

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveKioskCommand");
        }
        return data;
    }

    //UpdateKioskCommand
    private JObject UpdateKioskCommand(int commandId,   int createdUserId, int commandTypeId, string commandString, int commandTypeReason, string explanation)
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            int resp = koskOpr.UpdateKioskCommandOrder(commandId,  createdUserId, commandTypeId, commandString, commandTypeReason, explanation);
            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;
            koskOpr.Dispose();
            koskOpr = null;

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UpdateKioskCommand");
        }
        return data;
    }

    private JObject GetKioskCommandTypeNames()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            KioskCommandTypeNameCollection kioskCommandTypes = koskOpr.GetKioskCommandTypeName();
            koskOpr.Dispose();
            koskOpr = null;

            if (kioskCommandTypes != null & kioskCommandTypes.Count > 0)
            {
                data = kioskCommandTypes.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetKioskCommandTypeNames");
        }
        return data;
    }

    private JObject GetKioskCommandReasonNames()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            KioskCommandReasonCollection kioskCommandReasons = koskOpr.GetKioskCommandReasonName();
            koskOpr.Dispose();
            koskOpr = null;

            if (kioskCommandReasons != null & kioskCommandReasons.Count > 0)
            {
                data = kioskCommandReasons.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetKioskCommandReasonNames");
        }
        return data;
    }
    private JObject GetKioskNames()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            KioskNameCollection kioskNames = koskOpr.GetKioskName();
            koskOpr.Dispose();
            koskOpr = null;

            if (kioskNames != null & kioskNames.Count > 0)
            {
                data = kioskNames.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetKioskNames");
        }
        return data;
    }


   
    private JObject GetReferenceStatus()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            ReferenceStatusCollection names = koskOpr.GetReferenceStatusOrder();
            koskOpr.Dispose();
            koskOpr = null;

            if (names != null & names.Count > 0)
            {
                data = names.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetReferenceStatus");
        }
        return data;
    }


    private JObject GetProcessType()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            ProcessTypeCollection processType = koskOpr.GetProcessType();
            koskOpr.Dispose();
            koskOpr = null;

            if (processType != null & processType.Count > 0)
            {
                data = processType.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetInstutionNames");
        }
        return data;
    }

    private JObject GetInstutionNames()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            InstutionNameCollection names = koskOpr.GetInstutionNames();
            koskOpr.Dispose();
            koskOpr = null;

            if (names != null & names.Count > 0)
            {
                data = names.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetInstutionNames");
        }
        return data;
    }

    private JObject GetBankNames()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            BankNameCollection names = koskOpr.GetBankNames();
            koskOpr.Dispose();
            koskOpr = null;

            if (names != null & names.Count > 0)
            {
                data = names.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetInstutionNames");
        }
        return data;
    }


    private JObject GetCompleteStatusNames()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            CompleteStatusNameCollection names = koskOpr.GetCompleteStatusNames();
            koskOpr.Dispose();
            koskOpr = null;

            if (names != null & names.Count > 0)
            {
                data = names.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetCompleteStatusNames");
        }
        return data;
    }

    private JObject GetRiskClass()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            RiskClassCollection names = koskOpr.GetRiskClass();
            koskOpr.Dispose();
            koskOpr = null;

            if (names != null & names.Count > 0)
            {
                data = names.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetRiskClass");
        }
        return data;
    }
   

    private JObject GetInstutionNamesMutabakat()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            InstutionNameCollection names = koskOpr.GetInstutionNamesMutabakat();
            koskOpr.Dispose();
            koskOpr = null;

            if (names != null & names.Count > 0)
            {
                data = names.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetInstutionNamesMutabakat");
        }
        return data;
    }

    private JObject GetTxnStatusNames()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            TxnStatusNameCollection names = koskOpr.GetTxnStatusNames();
            koskOpr.Dispose();
            koskOpr = null;

            if (names != null & names.Count > 0)
            {
                data = names.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetTxnStatusNames");
        }
        return data;
    }

    private JObject GetDeviceNames()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            InstutionNameCollection names = koskOpr.GetDeviceNames();
            koskOpr.Dispose();
            koskOpr = null;

            if (names != null & names.Count > 0)
            {
                data = names.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetDeviceNames");
        }
        return data;
    }

    private JObject GetInstutionOperationNames()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            InstutionOperationNameCollection names = koskOpr.GetInstutionOperationNames();
            koskOpr.Dispose();
            koskOpr = null;

            if (names != null & names.Count > 0)
            {
                data = names.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetInstutionOperationNames");
        }
        return data;
    }

    private JObject GetCardTypeNames()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            CardTypeNameColection names = koskOpr.GetCardTypeNames();
            koskOpr.Dispose();
            koskOpr = null;

            if (names != null & names.Count > 0)
            {
                data = names.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetCardTypeNames");
        }
        return data;
    }

    private JObject GetTransactionConditionNames()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            TransactionConditionNameCollection names = koskOpr.GetTransactionConditionNames();
            koskOpr.Dispose();
            koskOpr = null;

            if (names != null & names.Count > 0)
            {
                data = names.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetTransactionConditionNames");
        }
        return data;
    }

    private JObject GetKioskTypeNames()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            KioskTypeNameCollection names = koskOpr.GetKioskTypeNames();
            koskOpr.Dispose();
            koskOpr = null;

            if (names != null & names.Count > 0)
            {
                data = names.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetKioskTypeNames");
        }
        return data;
    }

    private JObject GetKoskStatusNames()
    {
        StatusNameCollection statusNames = null;
        JObject data = new JObject();
        try
        {
            RoleOpr roleOpr = new RoleOpr();

            statusNames = roleOpr.GetKioskStatusNames();
            roleOpr.Dispose();
            roleOpr = null;

            if (statusNames != null & statusNames.Count > 0)
            {
                data = statusNames.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetKoskStatusNames");
        }
        return data;
    }

    private JObject SaveKiosk(InputSaveKiosk kiosk)
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            int resp = koskOpr.AddNewKiosk(kiosk.KioskName, kiosk.KioskAddress, kiosk.KioskIp, kiosk.KioskStatus, kiosk.UpdatedUser, kiosk.Latitude, kiosk.Longitude, kiosk.RegionId, kiosk.usageFee, kiosk.kioskTypeId, kiosk.kioskCity, kiosk.insertionDate, kiosk.acceptorTypeId, kiosk.explanation, kiosk.connectionType, kiosk.cardReaderType, kiosk.institutionId, kiosk.riskClass, kiosk.isCredit, kiosk.isMobile,kiosk.BankNameId, kiosk.IsInvoiceActive, kiosk.IsPrepaidActive, kiosk.IsExtendedScreenActive, kiosk.districtId, kiosk.neighborhoodId);
            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

            koskOpr.Dispose();
            koskOpr = null;

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveKiosk");
        }
        return data;
    }

    private JObject SaveKioskDevice(InputSaveKioskDevice kioskDevice)
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            int resp = koskOpr.AddNewKioskDevice(kioskDevice.kioskId, kioskDevice.deviceId, kioskDevice.portNumber, kioskDevice.adminUserId);
            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

            koskOpr.Dispose();
            koskOpr = null;

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveKioskDevice");
        }
        return data;
    }

    private JObject UpdateKiosk(InputUpdateKiosk kiosk)
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            int resp = koskOpr.UpdateKiosk(kiosk.KioskId, kiosk.KioskName, 
                kiosk.KioskAddress, kiosk.KioskIp, kiosk.KioskStatus, kiosk.UpdatedUser, kiosk.Latitude, kiosk.Longitude, kiosk.RegionId, kiosk.usageFee, 
                kiosk.kioskTypeId, kiosk.kioskCity, kiosk.insertionDate, kiosk.acceptorTypeId, kiosk.explanation, kiosk.connectionType, 
                kiosk.cardReaderType, kiosk.instutionId, kiosk.riskClass, kiosk.isCredit, kiosk.isMobile,kiosk.BankNameId, kiosk.IsInvoiceActive, 
                kiosk.IsPrepaidActive, kiosk.IsExtendedScreenActive, kiosk.IsCreditCard, kiosk.IsMobilePayment,
                kiosk.cameraIp,kiosk.channelNumber, kiosk.userName, kiosk.password, kiosk.districtId, kiosk.neighborhoodId
                );
            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

            koskOpr.Dispose();
            koskOpr = null;

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UpdateKiosk");
        }

        return data;
    }

    private JObject UpdateKioskDevice(InputSaveKioskDevice kioskDevice)
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            int resp = koskOpr.UpdateKioskDevice(kioskDevice.itemId, kioskDevice.portNumber, kioskDevice.adminUserId);
            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

            koskOpr.Dispose();
            koskOpr = null;

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UpdateKioskDevice");
        }

        return data;
    }

    private JObject GetKiosk(int kioskId)
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            Kiosk kiosk = koskOpr.GetKiosk(kioskId);
            koskOpr.Dispose();
            koskOpr = null;

            if (kiosk != null)
            {
                data = GetJSON(kiosk);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetKiosk");
        }
        return data;
    }

    private JObject GetRegions(int cityId)
    {
        JObject data = new JObject();
        try
        {
            CityOpr opr = new CityOpr();
            RegionCollection regions = opr.GetRegions(cityId);
            opr.Dispose();
            opr = null;

            if (regions != null & regions.Count > 0)
            {
                data = regions.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetRegion");
        }
        return data;
    }


    private JObject GetDistrict(int cityId)
    {
        JObject data = new JObject();
        try
        {
            CityOpr opr = new CityOpr();
            RegionCollection regions = opr.GetDistricts(cityId);
            opr.Dispose();
            opr = null;

            if (regions != null & regions.Count > 0)
            {
                data = regions.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetRegion");
        }
        return data;
    }
    private JObject GetConnectionType()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            ConnectionTypeCollection connectionType = koskOpr.GetConnectionType();
            koskOpr.Dispose();
            koskOpr = null;

            if (connectionType != null & connectionType.Count > 0)
            {
                data = connectionType.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetConnectionType");
        }
        return data;
    }




    private JObject GetNeighborhood(int districtId)
    {
        JObject data = new JObject();
        try
        {
            CityOpr opr = new CityOpr();
            RegionCollection regions = opr.GetNeighborhoods(districtId);
            opr.Dispose();
            opr = null;

            if (regions != null & regions.Count > 0)
            {
                data = regions.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetRegion");
        }
        return data;
    }


    private JObject GetDistrictAndneighborhood()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            ConnectionTypeCollection connectionType = koskOpr.GetDistrictAndneighborhoods();
            koskOpr.Dispose();
            koskOpr = null;

            if (connectionType != null & connectionType.Count > 0)
            {
                data = connectionType.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetConnectionType");
        }
        return data;
    }


    private JObject GetCardReaderType()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr koskOpr = new KioskOpr();
            CardReaderTypeCollection cardReaderType = koskOpr.GetCardReaderType();
            koskOpr.Dispose();
            koskOpr = null;

            if (cardReaderType != null & cardReaderType.Count > 0)
            {
                data = cardReaderType.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetCardReaderType");
        }

        return data;
    }

    private JObject GetCities()
    {
        JObject data = new JObject();
        try
        {
            CityOpr opr = new CityOpr();
            CityCollection cities = opr.GetCities();
            opr.Dispose();
            opr = null;

            if (cities != null & cities.Count > 0)
            {
                data = cities.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetCities");
        }
        return data;
    }

    private JObject GetCapacities()
    {
        JObject data = new JObject();
        try
        {
            CityOpr opr = new CityOpr();
            CapacityCollection capacities = opr.GetCapacities();
            opr.Dispose();
            opr = null;

            if (capacities != null & capacities.Count > 0)
            {
                data = capacities.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetCapacities");
        }
        return data;
    }

    private JObject GetKioskCashCounts(int operationType
                                     , int itemId
                                     , int numberOfItemsPerPage
                                     , int currentPageNum
                                     , int orderSelectionColumn
                                     , int descAsc)
    {

        JObject data = new JObject();

        try
        {
            int recordCount = 0;
            int pageCount = 0;

            KioskOpr opr = new KioskOpr();

            KioskCashCountCollection cashCounts = opr.SearchKiosksCashCountOrder(operationType, itemId, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc);
            opr.Dispose();
            opr = null;

            if (cashCounts != null & cashCounts.Count > 0)
            {
                data = cashCounts.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskCashCount");
        }
        return data;
    }


    private JObject GetKioskTotalCashCounts(DateTime startDate
                                 , DateTime endDate
                                 , int operationType
                                 //, int itemId
                                 , int numberOfItemsPerPage
                                 , int currentPageNum
                                 , int orderSelectionColumn
                                 , int descAsc)
    {

        JObject data = new JObject();

        try
        {
            int recordCount = 0;
            int pageCount = 0;

            KioskOpr opr = new KioskOpr();

            KioskTotalCashCountCollection cashCounts = opr.SearchKiosksTotalCashCountOrder(startDate, endDate, operationType, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc);
            opr.Dispose();
            opr = null;

            if (cashCounts != null & cashCounts.Count > 0)
            {
                data = cashCounts.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskTotalCashCount");
        }
        return data;
    }

    private JObject GetKioskCashCountsForExcel(int operationType
                                     , int itemId
                                     , int orderSelectionColumn
                                     , int descAsc)
    {

        JObject data = new JObject();

        try
        {
            int recordCount = 0;
            int pageCount = 0;

            KioskOpr opr = new KioskOpr();

            KioskCashCountCollection cashCounts = opr.SearchKiosksCashCountOrderForExcel(operationType, itemId, orderSelectionColumn, descAsc);
            opr.Dispose();
            opr = null;

            if (cashCounts != null & cashCounts.Count > 0)
            {
                data = cashCounts.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskCashCountForExcel");
        }
        return data;
    }


    private JObject GetKioskCashBoxCounts(int operationType
                                 , int itemId
                                 , int numberOfItemsPerPage
                                 , int currentPageNum
                                 , int orderSelectionColumn
                                 , int descAsc)
    {

        JObject data = new JObject();

        try
        {
            int recordCount = 0;
            int pageCount = 0;

            KioskOpr opr = new KioskOpr();

            KioskCashBoxCountTotalCollection cashCounts = opr.SearchKiosksCashBoxCountOrder(operationType, itemId, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc);
            opr.Dispose();
            opr = null;

            if (cashCounts != null & cashCounts.Count > 0)
            {
                data = cashCounts.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskCashCount");
        }
        return data;
    }

    private JObject GetKioskCashBoxCountsForExcel(int operationType
                                     , int itemId
                                     , int orderSelectionColumn
                                     , int descAsc)
    {

        JObject data = new JObject();

        try
        {
            int recordCount = 0;
            int pageCount = 0;

            KioskOpr opr = new KioskOpr();

            KioskCashBoxCountCollection cashCounts = opr.SearchKiosksCashBoxCountOrderForExcel(operationType, itemId, orderSelectionColumn, descAsc);
            opr.Dispose();
            opr = null;

            if (cashCounts != null & cashCounts.Count > 0)
            {
                data = cashCounts.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskCashCountForExcel");
        }
        return data;
    }
    private JObject SearchKioskReports(string searchText
                               , DateTime startDate
                               , DateTime endDate
                               , int numberOfItemsPerPage
                               , int currentPageNum
                               , int recordCount
                               , int pageCount
                               , int orderSelectionColumn
                               , int descAsc
                               , string kioskId
                               , int instutionId)
    {

        JObject data = new JObject();
        KioskReportCollection collection = null;
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            Int64 TotalTxnCount = 0;
            string TotalGeneratedAskiAmount = "";
            string TotalUsedAskiAmount = "";
            Int64 TotalGeneratedAskiCount = 0;
            Int64 TotalUsedAskiCount = 0;
            string TotalReceivedAmount ="";
            //string TotalKioskEmptyAmount = "";
            string TotalEarnedAmount = "";
            string TotalParaUstu = "";
            string TotalSuccesTxnCount = "";

            KioskOpr opr = new KioskOpr();

            collection = opr.SearchKioskReportOrder(searchText, 
                startDate, 
                endDate, 
                numberOfItemsPerPage, 
                currentPageNum, 
                ref recordCount, 
                ref pageCount, 
                orderSelectionColumn, 
                descAsc, 
                kioskId,
                instutionId,
                ref TotalTxnCount,
                ref TotalGeneratedAskiAmount,
                ref TotalUsedAskiAmount,
                ref TotalGeneratedAskiCount,
                ref TotalUsedAskiCount,
                ref TotalReceivedAmount,
                //ref TotalKioskEmptyAmount,
                ref TotalEarnedAmount,
                ref TotalParaUstu,
                ref TotalSuccesTxnCount);

            opr.Dispose();
            opr = null;

            if (collection != null & collection.Count > 0)
            {
                data = collection.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
            data.Add("TotalTxnCount", TotalTxnCount);
            data.Add("TotalGeneratedAskiAmount", TotalGeneratedAskiAmount);
            data.Add("TotalUsedAskiAmount", TotalUsedAskiAmount);
            data.Add("TotalGeneratedAskiCount", TotalGeneratedAskiCount);
            data.Add("TotalUsedAskiCount", TotalUsedAskiCount);
            data.Add("TotalReceivedAmount", TotalReceivedAmount);
            //data.Add("TotalKioskEmptyAmount", TotalKioskEmptyAmount);
            data.Add("TotalEarnedAmount", TotalEarnedAmount);
            data.Add("TotalParaUstu", TotalParaUstu);
            data.Add("TotalSuccesTxnCount", TotalSuccesTxnCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskReports");
        }
        return data;
    }

    private JObject SearchNewKioskReports(string searchText
                           , DateTime startDate
                           , DateTime endDate
                           , int numberOfItemsPerPage
                           , int currentPageNum
                           , int recordCount
                           , int pageCount
                           , int orderSelectionColumn
                           , int descAsc
                           , string kioskId
                           , int instutionId
                           , int orderType)
    {

        JObject data = new JObject();
        KioskReportCollections collection = null;
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            Int64 TotalTxnCount = 0;
            string TotalGeneratedAskiAmount = "";
            string TotalUsedAskiAmount = "";
            Int64 TotalGeneratedAskiCount = 0;
            Int64 TotalUsedAskiCount = 0;
            string TotalReceivedAmount = "";
            string TotalKioskEmptyAmount = "";
            string TotalEarnedAmount = "";
            string TotalParaUstu = "";
            string TotalSuccesTxnCount = "";
            string TotalInstitutionAmount = "";
            string TotalTotalAmount = "";
            string TotalCreditCardAmount = "";
            string TotalCommisionAmount = "";

            KioskOpr opr = new KioskOpr();

            collection = opr.SearchKioskReportsOrder(searchText,
                startDate,
                endDate,
                numberOfItemsPerPage,
                currentPageNum,
                ref recordCount,
                ref pageCount,
                orderSelectionColumn,
                descAsc,
                kioskId,
                instutionId,
                ref TotalTxnCount,
                ref TotalGeneratedAskiAmount,
                ref TotalUsedAskiAmount,
                ref TotalGeneratedAskiCount,
                ref TotalUsedAskiCount,
                ref TotalReceivedAmount,
                ref TotalKioskEmptyAmount,
                ref TotalEarnedAmount,
                ref TotalParaUstu,
                ref TotalSuccesTxnCount,
                ref TotalInstitutionAmount,
                ref TotalTotalAmount,
                ref TotalCreditCardAmount,
                ref TotalCommisionAmount,
                orderType);

            opr.Dispose();
            opr = null;

            if (collection != null & collection.Count > 0)
            {
                data = collection.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
            data.Add("TotalTxnCount", TotalTxnCount);
            data.Add("TotalGeneratedAskiAmount", TotalGeneratedAskiAmount);
            data.Add("TotalUsedAskiAmount", TotalUsedAskiAmount);
            data.Add("TotalGeneratedAskiCount", TotalGeneratedAskiCount);
            data.Add("TotalUsedAskiCount", TotalUsedAskiCount);
            data.Add("TotalReceivedAmount", TotalReceivedAmount);
            data.Add("TotalKioskEmptyAmount", TotalKioskEmptyAmount);
            data.Add("TotalEarnedAmount", TotalEarnedAmount);
            data.Add("TotalParaUstu", TotalParaUstu);
            data.Add("TotalSuccesTxnCount", TotalSuccesTxnCount);
            data.Add("TotalInstitutionAmount", TotalInstitutionAmount);
            data.Add("TotalTotalAmount", TotalTotalAmount);
            data.Add("TotalCreditCardAmount", TotalCreditCardAmount);
            data.Add("TotalCommisionAmount", TotalCommisionAmount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskReports");
        }
        return data;
    }

    private JObject SearchKioskReportsForExcel(string searchText
                               , DateTime startDate
                               , DateTime endDate                             
                               , int orderSelectionColumn
                               , int descAsc
                               , string kioskId
                               , int instutionId)
    {

        JObject data = new JObject();
        KioskReportCollection collection = null;
        try
        {

            KioskOpr opr = new KioskOpr();

            collection = opr.SearchKioskReportForExcelOrder(searchText, startDate, endDate, orderSelectionColumn, descAsc, kioskId, instutionId);

            opr.Dispose();
            opr = null;

            if (collection != null & collection.Count > 0)
            {
                data = collection.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskReportsForExcel");
        }
        return data;
    }

    private JObject SearchKioskReportsForExcelNew(string searchText
                           , DateTime startDate
                           , DateTime endDate
                           , int orderSelectionColumn
                           , int descAsc
                           , string kioskId
                           , int instutionId)
    {

        JObject data = new JObject();
        KioskReportCollections collection = null;
        try
        {

            KioskOpr opr = new KioskOpr();

            collection = opr.SearchKioskReportForExcelOrderNew(searchText, startDate, endDate, orderSelectionColumn, descAsc, kioskId, instutionId);

            opr.Dispose();
            opr = null;

            if (collection != null & collection.Count > 0)
            {
                data = collection.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskReportsForExcel");
        }
        return data;
    }

    public JObject GetJSON(Kiosk theKiosk)
    {
        JObject jObjectDis = new JObject();
        string a = JsonConvert.SerializeObject(theKiosk, Newtonsoft.Json.Formatting.None);
        JObject x = JObject.Parse(a);
        jObjectDis.Add("Kiosk", x);
        return jObjectDis;
    }

    //GetKioskErrorLogs
    private JObject GetKioskErrorLogs(int errorId)
    {
        JObject data = new JObject();
        try
        {
            KioskOpr opr = new KioskOpr();
            ErrorLog errorLogs = opr.GetKioskErrorLogsOrder(errorId);
            opr.Dispose();
            opr = null;

            if (errorLogs != null)
            {
                data = GetJSON(errorLogs);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetKioskErrorLogs");
        }
        return data;
    }

    //UpdateErrorLogs
    private JObject UpdateErrorLogs(InputListSaveErrorLogs inputListUpdateErrorLogs)
    {


        JObject data = new JObject();
        try
        {

            KioskOpr opr = new KioskOpr();
            int resp = opr.UpdateErrorLogsOrder(inputListUpdateErrorLogs.errorId,
                                                inputListUpdateErrorLogs.explanation,
                                                inputListUpdateErrorLogs.expertUserId);

            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

            opr.Dispose();
            opr = null;

            if (resp == 0)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UpdateErrorLogs");
        }
        return data;
    }

    public JObject GetJSON(ErrorLog theerrorLogs)
    {
        JObject jObjectDis = new JObject();
        string a = JsonConvert.SerializeObject(theerrorLogs, Newtonsoft.Json.Formatting.None);
        JObject x = JObject.Parse(a);
        jObjectDis.Add("GetKioskErrorLogs", x);
        return jObjectDis;
    }
}