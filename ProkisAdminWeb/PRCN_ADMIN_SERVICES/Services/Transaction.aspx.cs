﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE;
using PRCNCORE.Accounts;
using PRCNCORE.MoneyCodes;
using PRCNCORE.Backend;
using PRCNCORE.Constants;
using PRCNCORE.Input;
using PRCNCORE.Transactions;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using PRCNCORE.Input.Transaction;

public partial class Services_Transaction : System.Web.UI.Page
{
    private TransactionCollection transactions;
    private PurseTransactionCollection purseTransactions;
    private ReturnCodes errorCode = ReturnCodes.SUCCESSFULL;
    JObject data = new JObject();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.HttpMethod == "POST")
        {
            ProcessRequest();
        }
    }

    private void ProcessRequest()
    {
        SwitchInput inputData = null;
        try
        {
            string sData = WebServiceUtils.LoadParameters(Request, InputType.Clear);

            if (!string.IsNullOrEmpty(sData))
            {
                inputData = JsonConvert.DeserializeObject<SwitchInput>(sData);

                if (!WebServiceUtils.CheckCredentials(inputData.ApiKey, inputData.ApiPass))
                {
                    errorCode = ReturnCodes.CREDENTIALS_ERROR;
                }
                else if (Request.Headers["AccessToken"] == null || !WebServiceUtils.CheckAccessTokenSecurity(Request.Headers["AccessToken"], inputData.adminUserId))
                {
                    errorCode = ReturnCodes.INVALID_ACCESS_TOKEN;
                }
                else
                {
                    data = PrepareResponse(inputData.FunctionCode, sData, inputData.adminUserId, inputData.pageUrl);
                    //responseData = WebServiceUtils.AddErrorNodes(responseData, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
                }
            }
            else
            {
                errorCode = ReturnCodes.LOAD_JSON_PARAMETERS_ERROR;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ProcessRequestTransaction");
        }
        data = WebServiceUtils.AddErrorNodes(data, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
        WebServiceUtils.SendResponse(data, Response, false);
    }

    private JObject PrepareResponse(int functionCode, string input, long adminUserId, string pageUrl)
    {
        JObject data = new JObject();
        try
        {
            switch ((FC)functionCode)
            {

                 case FC.LIST_TRANSACTION:
                    InputListTransactions listTransactions = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = SearchTransactions(listTransactions.SearchText
                        , listTransactions.StartDate
                        , listTransactions.EndDate
                        , listTransactions.NumberOfItemsPerPage
                        , listTransactions.CurrentPageNum
                        , listTransactions.RecordCount
                        , listTransactions.PageCount
                        , listTransactions.OrderSelectionColumn
                        , listTransactions.DescAsc
                        , listTransactions.CustomerId
                        , listTransactions.KioskId
                        , listTransactions.instutionId
                        , listTransactions.operationTypeId
                        , listTransactions.txnStatusId
                        , listTransactions.OrderType);
                    break;

                 case FC.LIST_TRANSACTION_DETAILS:
                    InputListTransactionsDetails listTransactionsDetails = JsonConvert.DeserializeObject<InputListTransactionsDetails>(input);
                    data = SearchTransactionsDetails(listTransactionsDetails.TransactionId);
                    break;

             
               

                 case FC.LIST_KIOSK_LOCAL_LOG:
                    InputSaveControlCashNotification kiosklocallog = JsonConvert.DeserializeObject<InputSaveControlCashNotification>(input);
                    data = SearchKioskLocalLog(kiosklocallog.SearchText,
                        kiosklocallog.StartDate
                        , kiosklocallog.EndDate
                        , kiosklocallog.LogStatus
                        , kiosklocallog.NumberOfItemsPerPage
                        , kiosklocallog.CurrentPageNum
                        , kiosklocallog.RecordCount
                        , kiosklocallog.PageCount
                        , kiosklocallog.OrderSelectionColumn
                        , kiosklocallog.DescAsc
                        // , kioskreferencesystem.InstutionId
                        , kiosklocallog.KioskIds
                        , kiosklocallog.OrderType);
                    break;

                 case FC.LIST_KIOSK_LOCAL_LOG_DETAIL:
                    InputShowLocalLogDetail kioskLocalLogDetail = JsonConvert.DeserializeObject<InputShowLocalLogDetail>(input);
                    data = SearchKioskLocalLogDetail(kioskLocalLogDetail.localLogId);
                    break;

                 case FC.LIST_CUSTOMER_CONTACTS:
                    InputListCustomerContact customerContactDetail = JsonConvert.DeserializeObject<InputListCustomerContact>(input);
                    data = SearchCustomerContactDetail(customerContactDetail.SearchText
                        , customerContactDetail.StartDate
                        , customerContactDetail.EndDate
                        , customerContactDetail.NumberOfItemsPerPage
                        , customerContactDetail.CurrentPageNum
                        , customerContactDetail.RecordCount
                        , customerContactDetail.PageCount
                        , customerContactDetail.OrderSelectionColumn
                        , customerContactDetail.DescAsc
                        , customerContactDetail.instutionId
                        , customerContactDetail.TransactionId
                        , customerContactDetail.OrderType);
                    break;

                 case FC.SAVE_KIOSK_LOCAL_LOG:
                    InputSaveKioskLocalLog inputKioskLocalLog = JsonConvert.DeserializeObject<InputSaveKioskLocalLog>(input);
                    data = SaveKioskLocalLog(inputKioskLocalLog);
                    break;

                 case FC.LIST_CREDIT_CARD_TRANSACTION:
                    InputListTransactions listCreditCardTransactions = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = SearchCreditCardTransactions(listCreditCardTransactions.SearchText
                        , listCreditCardTransactions.StartDate
                        , listCreditCardTransactions.EndDate
                        , listCreditCardTransactions.NumberOfItemsPerPage
                        , listCreditCardTransactions.CurrentPageNum
                        , listCreditCardTransactions.RecordCount
                        , listCreditCardTransactions.PageCount
                        , listCreditCardTransactions.OrderSelectionColumn
                        , listCreditCardTransactions.DescAsc
                        , listCreditCardTransactions.KioskId
                        , listCreditCardTransactions.instutionId
                        , listCreditCardTransactions.MuhasebeStatusId
                        , listCreditCardTransactions.OrderType);
                    break;
 
                 case FC.LIST_MOBILE_PAYMENT_TRANSACTION:
                    InputListTransactions listMobilePaymentTransactions = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = SearchMobilePaymentTransactions(listMobilePaymentTransactions.SearchText
                        , listMobilePaymentTransactions.StartDate
                        , listMobilePaymentTransactions.EndDate
                        , listMobilePaymentTransactions.NumberOfItemsPerPage
                        , listMobilePaymentTransactions.CurrentPageNum
                        , listMobilePaymentTransactions.RecordCount
                        , listMobilePaymentTransactions.PageCount
                        , listMobilePaymentTransactions.OrderSelectionColumn
                        , listMobilePaymentTransactions.DescAsc
                        , listMobilePaymentTransactions.KioskId
                        , listMobilePaymentTransactions.instutionId
                        , listMobilePaymentTransactions.MuhasebeStatusId
                        , listMobilePaymentTransactions.OrderType);
                    break;

                 case FC.LIST_CARD_REPAIR:
                    InputListTransactions listRepairCard = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = SearchCardRepair(listRepairCard.SearchText
                        , listRepairCard.StartDate
                        , listRepairCard.EndDate
                        , listRepairCard.NumberOfItemsPerPage
                        , listRepairCard.CurrentPageNum
                        , listRepairCard.RecordCount
                        , listRepairCard.PageCount
                        , listRepairCard.OrderSelectionColumn
                        , listRepairCard.DescAsc
                        , listRepairCard.KioskId
                        , listRepairCard.instutionId
                        , listRepairCard.CardRepairStatus
                        , listRepairCard.IsCardDeleted
                        , listRepairCard.OrderType);
                    break;


                 case FC.GET_REPAIRED_CARD:
                    InputListTransactions listRepairedCard = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = GetRepairedCard(listRepairedCard.RepairId);
                    break;

                 case FC.GET_MUHASEBE_STATUS:
                    InputListTransactions listMuhasebe = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = GetMuhasebeStatus();
                    break;


                 case FC.UPDATE_REPAIRED_CARD:
                    InputListUpdateRepairedCard inputListUpdateRepairedCard = JsonConvert.DeserializeObject<InputListUpdateRepairedCard>(input);
                    data = UpdateRepairedCard(inputListUpdateRepairedCard);
                    break;  


                 case FC.SAVE_CARD_REPAIR:
                    InputSaveCardRepair cardRepair = JsonConvert.DeserializeObject<InputSaveCardRepair>(input);
                    data = SaveCardRepair(cardRepair);
                    break;

                 case FC.LIST_FAILED_TRANSACTION:
                    InputListFailedTransactions listFailedTransactions = JsonConvert.DeserializeObject<InputListFailedTransactions>(input);
                    data = SearchFailedTransactions(listFailedTransactions.SearchText
                        , listFailedTransactions.StartDate
                        , listFailedTransactions.EndDate
                        , listFailedTransactions.NumberOfItemsPerPage
                        , listFailedTransactions.CurrentPageNum
                        , listFailedTransactions.RecordCount
                        , listFailedTransactions.PageCount
                        , listFailedTransactions.OrderSelectionColumn
                        , listFailedTransactions.DescAsc
                        , listFailedTransactions.CustomerId
                        , listFailedTransactions.KioskId
                        , listFailedTransactions.instutionId
                        , listFailedTransactions.operationTypeId
                        , listFailedTransactions.txnStatusId
                        , listFailedTransactions.OrderType);
                    break;
                case FC.LIST_PURSE_TRANSACTION:
                    InputListTransactions listPurseTransactions = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = SearchPurseTransactions(listPurseTransactions.SearchText
                        , listPurseTransactions.StartDate
                        , listPurseTransactions.EndDate
                        , listPurseTransactions.NumberOfItemsPerPage
                        , listPurseTransactions.CurrentPageNum
                        , listPurseTransactions.RecordCount
                        , listPurseTransactions.PageCount
                        , listPurseTransactions.OrderSelectionColumn
                        , listPurseTransactions.DescAsc);
                    break;
                case FC.GET_TRANSACTION_DETAIL:
                    InputGetTransaction transactionId = JsonConvert.DeserializeObject<InputGetTransaction>(input);
                    data = GetTransactionDetail(transactionId.transactionId);
                    break;
                case FC.GET_PURSE_TRANSACTION_DETAIL:
                    InputGetTransaction pursetransactionId = JsonConvert.DeserializeObject<InputGetTransaction>(input);
                    data = GetPurseTransactionDetail(pursetransactionId.transactionId);
                    break;
                case FC.CALL_CANCEL_TXN_SERVICE:
                    InputCancelTransactionService cancelServiceResult = JsonConvert.DeserializeObject<InputCancelTransactionService>(input);
                    int res = CallTxnCancelService(cancelServiceResult.transactionId, cancelServiceResult.userId, cancelServiceResult.adminUserId,
                        cancelServiceResult.customerNo, cancelServiceResult.billDate, cancelServiceResult.billNumber);
                    data.Add("cancelResult",res);
                    break;

                case FC.CALL_STATUS_TXN_SERVICE:
                    InputChangeStatusTransactionService changeStatusResult = JsonConvert.DeserializeObject<InputChangeStatusTransactionService>(input);
                    int resultOfChange = CallChangeTxnService(changeStatusResult.transactionId, changeStatusResult.userId, changeStatusResult.adminUserId,
                        changeStatusResult.newStatus, changeStatusResult.explanation);
                    data.Add("opResult", resultOfChange);
                    break;

                case FC.CALL_UPDATE_CANCEL_TXN_LOG:
                    InputCancelTransactionService cancelTnxLog = JsonConvert.DeserializeObject<InputCancelTransactionService>(input);
                    data = CancelTransactionLog(cancelTnxLog.transactionId, cancelTnxLog.userId, cancelTnxLog.adminUserId,
                        cancelTnxLog.customerNo.ToString(), cancelTnxLog.billDate, cancelTnxLog.billNumber.ToString(),cancelTnxLog.serviceResult);

                    break;
                case FC.EXCEL_LIST_TRANSACTION:
                    InputListTransactions listTransactionsForExcel = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = SearchTransactionsForExcel(listTransactionsForExcel.SearchText
                        , listTransactionsForExcel.StartDate
                        , listTransactionsForExcel.EndDate
                        , listTransactionsForExcel.OrderSelectionColumn
                        , listTransactionsForExcel.DescAsc
                        , listTransactionsForExcel.CustomerId
                        , listTransactionsForExcel.KioskId
                        , listTransactionsForExcel.instutionId
                        , listTransactionsForExcel.operationTypeId
                        , listTransactionsForExcel.txnStatusId);
                    break;
                case FC.EXCEL_LIST_PURSE_TRANSACTION:
                    InputListTransactions listPurseTransactionsForExcel = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = SearchPurseTransactionsForExcel(listPurseTransactionsForExcel.SearchText
                        , listPurseTransactionsForExcel.StartDate
                        , listPurseTransactionsForExcel.EndDate
                        , listPurseTransactionsForExcel.OrderSelectionColumn
                        , listPurseTransactionsForExcel.DescAsc);
                    break;
                case FC.EXCEL_LIST_ACCOUNT_TRANSACTION:
                    InputListTransactions listAccountTransactionsForExcel = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = SearchAccountTransactionsForExcel(listAccountTransactionsForExcel.SearchText
                        , listAccountTransactionsForExcel.StartDate
                        , listAccountTransactionsForExcel.EndDate
                        , listAccountTransactionsForExcel.OrderSelectionColumn
                        , listAccountTransactionsForExcel.DescAsc
                        , listAccountTransactionsForExcel.platform
                        , listAccountTransactionsForExcel.whichAction);
                    break;
                case FC.LIST_ACCOUNT_TRANSACTION:
                    InputListTransactions listAccountTransactions = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = SearchAccountTransactions(listAccountTransactions.SearchText
                        , listAccountTransactions.StartDate
                        , listAccountTransactions.EndDate
                        , listAccountTransactions.NumberOfItemsPerPage
                        , listAccountTransactions.CurrentPageNum
                        , listAccountTransactions.RecordCount
                        , listAccountTransactions.PageCount
                        , listAccountTransactions.OrderSelectionColumn
                        , listAccountTransactions.DescAsc
                        , listAccountTransactions.platform
                        , listAccountTransactions.whichAction);
                    break;

                case FC.LIST_DAILY_ACCOUNT_TRANSACTION:
                    InputListTransactions listDailyAccountTransactions = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = ListAcDailycountTransactions(listDailyAccountTransactions.itemId
                        , listDailyAccountTransactions.NumberOfItemsPerPage
                        , listDailyAccountTransactions.CurrentPageNum
                        , listDailyAccountTransactions.RecordCount
                        , listDailyAccountTransactions.PageCount
                        , listDailyAccountTransactions.OrderSelectionColumn
                        , listDailyAccountTransactions.DescAsc);
                    break;
                case FC.LIST_DAILY_ACCOUNT_TRANSACTION_PROKIS:
                    InputListTransactions listDailyAccountTransactionsProkis = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = ListAcDailycountTransactionsProkis(listDailyAccountTransactionsProkis.itemId
                        , listDailyAccountTransactionsProkis.NumberOfItemsPerPage
                        , listDailyAccountTransactionsProkis.CurrentPageNum
                        , listDailyAccountTransactionsProkis.RecordCount
                        , listDailyAccountTransactionsProkis.PageCount
                        , listDailyAccountTransactionsProkis.OrderSelectionColumn
                        , listDailyAccountTransactionsProkis.DescAsc);
                    break;
                case FC.EXCEL_LIST_DAILY_ACCOUNT_TRANSACTION:
                    InputListTransactions listDailyAccountTransactionsForExcel = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = ListDailyAccountTransactionsForExcel(listDailyAccountTransactionsForExcel.itemId
                        , listDailyAccountTransactionsForExcel.OrderSelectionColumn
                        , listDailyAccountTransactionsForExcel.DescAsc);
                    break;
                case FC.EXCEL_LIST_DAILY_ACCOUNT_TRANSACTION_PROKIS:
                    InputListTransactions listDailyAccountTransactionsProkisForExcel = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = ListDailyAccountTransactionsProkisForExcel(listDailyAccountTransactionsProkisForExcel.itemId
                        , listDailyAccountTransactionsProkisForExcel.OrderSelectionColumn
                        , listDailyAccountTransactionsProkisForExcel.DescAsc);
                    break;
                case FC.LIST_DAILY_MONEY_CODES:
                    InputListTransactions listDailyAskiCodes = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = ListDailyMoneyCodes(listDailyAskiCodes.itemId
                        , listDailyAskiCodes.isUsed
                        , listDailyAskiCodes.NumberOfItemsPerPage
                        , listDailyAskiCodes.CurrentPageNum
                        , listDailyAskiCodes.RecordCount
                        , listDailyAskiCodes.PageCount
                        , listDailyAskiCodes.OrderSelectionColumn
                        , listDailyAskiCodes.DescAsc);
                    break;
                case FC.EXCEL_LIST_DAILY_MONEY_CODES:
                    InputListTransactions listDailyAskiCodesForExcel = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = ListDailyAskiCodesForExcel(listDailyAskiCodesForExcel.itemId
                        , listDailyAskiCodesForExcel.isUsed
                        , listDailyAskiCodesForExcel.OrderSelectionColumn
                        , listDailyAskiCodesForExcel.DescAsc);
                    break;
                case FC.LIST_DAILY_CUSTOMER_ACCOUNTS:
                    InputListTransactions listDailyCustomerAccounts = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = ListDailyCustomerAccounts(listDailyCustomerAccounts.itemId
                        , listDailyCustomerAccounts.isActive
                        , listDailyCustomerAccounts.NumberOfItemsPerPage
                        , listDailyCustomerAccounts.CurrentPageNum
                        , listDailyCustomerAccounts.RecordCount
                        , listDailyCustomerAccounts.PageCount
                        , listDailyCustomerAccounts.OrderSelectionColumn
                        , listDailyCustomerAccounts.DescAsc);
                    break;
                case FC.EXCEL_LIST_DAILY_CUSTOMER_ACCOUNTS:
                    InputListTransactions listDailyCustomerAccountsForExcel = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = ListDailyCustomerAccountsForExcel(listDailyCustomerAccountsForExcel.itemId
                        , listDailyCustomerAccountsForExcel.isActive
                        , listDailyCustomerAccountsForExcel.OrderSelectionColumn
                        , listDailyCustomerAccountsForExcel.DescAsc);
                    break;

                case FC.GET_CANCEL_PARAMETERS:
                    InputCancelTnx cancelApprovedTnx = JsonConvert.DeserializeObject<InputCancelTnx>(input);
                    data = CancelApprovedTransaction(cancelApprovedTnx.transactionId);
                    break;

                case FC.LIST_TRANSACTION_CONDITION:
                    InputListTransactionConditions listTransactionConditions = JsonConvert.DeserializeObject<InputListTransactionConditions>(input);
                    data = SearchTransactionConditions(listTransactionConditions.SearchText
                        , listTransactionConditions.StartDate
                        , listTransactionConditions.EndDate
                        , listTransactionConditions.NumberOfItemsPerPage
                        , listTransactionConditions.CurrentPageNum
                        , listTransactionConditions.RecordCount
                        , listTransactionConditions.PageCount
                        , listTransactionConditions.OrderSelectionColumn
                        , listTransactionConditions.DescAsc
                        , listTransactionConditions.KioskId
                        , listTransactionConditions.instutionId
                        , listTransactionConditions.paymentTypeId
                        , listTransactionConditions.cardTypeId
                        , listTransactionConditions.completeStatusId
                        , listTransactionConditions.PaymentType
                        ,listTransactionConditions.OrderType);
                    break;

                case FC.LIST_PAYMENT_TYPE:
                    data = GetPaymentType();
                    break;

                case FC.LIST_CARD_REPAIR_STATUS:
                    data = GetCardRepairStatus();
                    break;

                case FC.LIST_IS_CARD_DELETED:
                    data = GetIsCardDeleted();
                    break;

                //case FC.EXCEL_LIST_TRANSACTION_CONDITION:
                //    InputListTransactionConditions listTransactionConditionExcel = JsonConvert.DeserializeObject<InputListTransactionConditions>(input);
                //    data = SearchTransactionConditionExcel(listTransactionConditionExcel.SearchText
                //        , listTransactionConditionExcel.StartDate
                //        , listTransactionConditionExcel.EndDate
                //        , listTransactionConditionExcel.OrderSelectionColumn
                //        , listTransactionConditionExcel.DescAsc
                //        , listTransactionConditionExcel.KioskId
                //        , listTransactionConditionExcel.instutionId
                //        , listTransactionConditionExcel.paymentTypeId
                //        , listTransactionConditionExcel.cardTypeId
                //        , listTransactionConditionExcel.completeStatusId);
                //    break;
                default:
                    break;
            }
            ManageContextRolescs contextRoles = new ManageContextRolescs();
            contextRoles.EditByRoles(functionCode, data, adminUserId, pageUrl);

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "PrepareResponseTransaction");
        }

        return data;
    }

    private JObject SearchTransactions(string searchText
                                , DateTime startDate
                                , DateTime endDate
                                , int numberOfItemsPerPage
                                , int currentPageNum
                                , int recordCount
                                , int pageCount
                                , int orderSelectionColumn
                                , int descAsc
                                , int customerId
                                , string kioskId
                                , int instutionId
                                , int operationTypeId
                                , string txnStatusId
                                , int orderType)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            TransactionOpr transactionOpr = new TransactionOpr();
            if (customerId == 0)
            {
                this.transactions = transactionOpr.SearchTransactionsOrder(searchText, startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, kioskId, instutionId, operationTypeId, txnStatusId, orderType);

            }
            else
            {
                this.transactions = transactionOpr.SearchCustomerTransactionsOrder(searchText, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, customerId, kioskId, instutionId, operationTypeId, txnStatusId);
            }
            transactionOpr.Dispose();
            transactionOpr = null;

            if (this.transactions != null & this.transactions.Count > 0)
            {
                data = this.transactions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchTransactions");
        }
        return data;
    }


    private JObject SearchTransactionsDetails(long transactionId)
    {

        JObject data = new JObject();
        try
        {

            TransactionOpr transactionOpr = new TransactionOpr();

            TransactionDetailCollectionGroup transactions = transactionOpr.SearchTransactionsDetailsOrderDet(transactionId);

      
            if (transactions != null & transactions.Count > 0)
            {
                data = transactions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
            transactionOpr.Dispose();
            transactionOpr = null;

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchTransactions");
        }
        return data;
    }




    //SearchKioskLocalLog
    private JObject SearchKioskLocalLog(string searhText
                                           , DateTime startDate
                                            , DateTime endDate
                                           , int logStatus
                                           , int numberOfItemsPerPage
                                           , int currentPageNum
                                           , int recordCount
                                           , int pageCount
                                           , int orderSelectionColumn
                                           , int descAsc
        // , int instutionId
                                           , string kioskId
                                           , int orderType)
    {
        JObject data = new JObject();
        try
        {
             int recordCnt = recordCount;
             int pageCnt = pageCount;



            TransactionOpr transactionOpr = new TransactionOpr();
            KioskLocalLogCollection kllc = transactionOpr.SearchKioskLocalLogOrder(searhText,
                                                                                        startDate,
                                                                                        endDate,
                                                                                        logStatus,
                                                                                        numberOfItemsPerPage,
                                                                                        currentPageNum,
                                                                                        ref recordCount,
                                                                                        ref pageCount,
                                                                                        orderSelectionColumn,
                                                                                        descAsc,
                //instutionId, 
                                                                                        kioskId,
                                                                                        orderType);


            transactionOpr.Dispose();
            transactionOpr = null;

            if (kllc != null & kllc.Count > 0)
            {
                data = kllc.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }


            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);


        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskReferenceSystemOrder");
        }
        return data;
    }

    private JObject SearchKioskLocalLogDetail(string localLogDetail)
    {
        JObject data = new JObject();
        try
        {

            TransactionOpr transactionOpr = new TransactionOpr();
            KioskLocalLogDetailCollection kllc = transactionOpr.SearchKioskLocalLogDetailOrder(localLogDetail);


            transactionOpr.Dispose();
            transactionOpr = null;

            if (kllc != null & kllc.Count > 0)
            {
                data = kllc.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }


            data = WebServiceUtils.AddPaggingNodes(data, 0, 0);


        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskReferenceSystemOrder");
        }
        return data;
    }

    private JObject SearchCustomerContactDetail(string searchText
                               , DateTime startDate
                               , DateTime endDate
                               , int numberOfItemsPerPage
                               , int currentPageNum
                               , int recordCount
                               , int pageCount
                               , int orderSelectionColumn
                               , int descAsc
                               , int instutionId
                               , long transactionId
                               , int orderType)
    {
        JObject data = new JObject();
        try
        {

            TransactionOpr transactionOpr = new TransactionOpr();
            GetCustomerContactsCollection kllc = transactionOpr.SearchCustomerContactsOrder(searchText,
                              startDate,
                              endDate,
                              numberOfItemsPerPage,
                              currentPageNum,
                              recordCount,
                              pageCount,
                              orderSelectionColumn,
                              descAsc,
                              instutionId,
                              transactionId,
                              orderType
                              );


            transactionOpr.Dispose();
            transactionOpr = null;

            if (kllc != null & kllc.Count > 0)
            {
                data = kllc.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }


            data = WebServiceUtils.AddPaggingNodes(data, 0, 0);


        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskReferenceSystemOrder");
        }
        return data;
    }


    private JObject SaveKioskLocalLog(InputSaveKioskLocalLog inputKioskLocalLog)
    {
        JObject data = new JObject();
        try
        {
            TransactionOpr transactionOpr = new TransactionOpr();
            int resp = transactionOpr.SaveKioskLocalLogOrder(inputKioskLocalLog.TransactionId, inputKioskLocalLog.AdminUserId);

            transactionOpr.Dispose();
            transactionOpr = null;

            if (resp == 1)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveKioskLocalLogOrder");
        }

        return data;
    }


    //SearchCreditCardTransactions
    private JObject SearchCreditCardTransactions(string searchText
                               , DateTime startDate
                               , DateTime endDate
                               , int numberOfItemsPerPage
                               , int currentPageNum
                               , int recordCount
                               , int pageCount
                               , int orderSelectionColumn
                               , int descAsc
                               , string kioskId
                               , int instutionId
                               , int muhasebeStatusId
                               , int orderType)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            string TotalTransactionAmount = "";

            TransactionOpr transactionOpr = new TransactionOpr();
            //if (customerId == 0)
            //{
            CreditCardTransactionCollection creditCardTransaction = transactionOpr.SearchCreditCardTransactionsOrder(searchText, startDate, endDate, numberOfItemsPerPage, 
                                                                                    currentPageNum, ref recordCount, ref pageCount,
                                                                                    orderSelectionColumn, descAsc, kioskId, instutionId, muhasebeStatusId, orderType, ref TotalTransactionAmount);

          


            transactionOpr.Dispose();
            transactionOpr = null;

            if (creditCardTransaction != null & creditCardTransaction.Count > 0)
            {
                data = creditCardTransaction.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);

            data.Add("TotalTransactionAmount", TotalTransactionAmount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchCreditCardTransactions");
        }
        return data;
    }

  
    //SearchMobilePaymentTransactions
    private JObject SearchMobilePaymentTransactions(string searchText
                               , DateTime startDate
                               , DateTime endDate
                               , int numberOfItemsPerPage
                               , int currentPageNum
                               , int recordCount
                               , int pageCount
                               , int orderSelectionColumn
                               , int descAsc
                               , string kioskId
                               , int instutionId
                               , int muhasebeStatusId
                               , int orderType)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            string TotalTransactionAmount = "";

            TransactionOpr transactionOpr = new TransactionOpr();
            //if (customerId == 0)
            //{
            MobilePaymentTransactionCollection creditCardTransaction = transactionOpr.SearchMobilePaymentTransactionsOrder(searchText, startDate, endDate, numberOfItemsPerPage,
                                                                                    currentPageNum, ref recordCount, ref pageCount,
                                                                                    orderSelectionColumn, descAsc, kioskId, instutionId, muhasebeStatusId, orderType, ref TotalTransactionAmount);




            transactionOpr.Dispose();
            transactionOpr = null;

            if (creditCardTransaction != null & creditCardTransaction.Count > 0)
            {
                data = creditCardTransaction.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);

            data.Add("TotalTransactionAmount", TotalTransactionAmount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchCreditCardTransactions");
        }
        return data;
    }

    /*
    //SearchCreditCardTransactions
    private JObject SearchCreditCardTransactions(string searchText
                               , DateTime startDate
                               , DateTime endDate
                               , int numberOfItemsPerPage
                               , int currentPageNum
                               , int recordCount
                               , int pageCount
                               , int orderSelectionColumn
                               , int descAsc
                               , string kioskId
                               , int instutionId
                               , int orderType)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            string TotalTransactionAmount = "";

            TransactionOpr transactionOpr = new TransactionOpr();
            //if (customerId == 0)
            //{
            CreditCardTransactionCollection creditCardTransaction = transactionOpr.SearchCreditCardTransactionsOrder(searchText, startDate, endDate, numberOfItemsPerPage,
                                                                                    currentPageNum, ref recordCount, ref pageCount,
                                                                                    orderSelectionColumn, descAsc, kioskId, instutionId, orderType, ref TotalTransactionAmount);




            transactionOpr.Dispose();
            transactionOpr = null;

            if (creditCardTransaction != null & creditCardTransaction.Count > 0)
            {
                data = creditCardTransaction.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);

            data.Add("TotalTransactionAmount", TotalTransactionAmount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchCreditCardTransactions");
        }
        return data;
    }
    */

    //SearchRepairCard
    //SearchCreditCardTransactions
    private JObject SearchCardRepair(string searchText
                               , DateTime startDate
                               , DateTime endDate
                               , int numberOfItemsPerPage
                               , int currentPageNum
                               , int recordCount
                               , int pageCount
                               , int orderSelectionColumn
                               , int descAsc
                               , string kioskId
                               , int instutionId
                               , int cardRepairStatus
                               , int isCardDeleted 
                               , int orderType)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            TransactionOpr transactionOpr = new TransactionOpr();
            //if (customerId == 0)
            //{
            RepairCardCollection repairCard = transactionOpr.SearchRepairCardOrder(searchText, startDate, endDate, numberOfItemsPerPage,
                                                                                    currentPageNum, ref recordCount, ref pageCount,
                                                                                    orderSelectionColumn, descAsc, kioskId, instutionId, cardRepairStatus,isCardDeleted, orderType);


            transactionOpr.Dispose();
            transactionOpr = null;

            if (repairCard != null & repairCard.Count > 0)
            {
                data = repairCard.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchRepairCard");
        }
        return data;
    }


    private JObject GetRepairedCard(int repairId)
    {
        JObject data = new JObject();
        try
        {
            TransactionOpr opr = new TransactionOpr();
            RepairCard repairedCard = opr.GetRepairedCardOrder(repairId);
            opr.Dispose();
            opr = null;

            if (repairedCard != null)
            {
                data = GetJSON(repairedCard);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetRepairedCard");
        }
        return data;
    }

    public JObject GetJSON(RepairCard thecutkiosk)
    {
        JObject jObjectDis = new JObject();
        string a = JsonConvert.SerializeObject(thecutkiosk, Newtonsoft.Json.Formatting.None);
        JObject x = JObject.Parse(a);
        jObjectDis.Add("CardRepair", x);
        return jObjectDis;
    }

    // SearchRepairCard
    private JObject SaveCardRepair(InputSaveCardRepair cardRepair)
    {
        JObject data = new JObject();
        try
        {
            TransactionOpr opr = new TransactionOpr();
            int resp = opr.AddCardRepairOrder(cardRepair.transactionId, cardRepair.anaKredi, cardRepair.yedekKredi, cardRepair.adminUserId);
            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

            opr.Dispose();
            opr = null;

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveCardRepair");
        }
        return data;
    }


    //UpdateErrorLogs
    private JObject UpdateRepairedCard(InputListUpdateRepairedCard inputListUpdateRepairedCard)
    {


        JObject data = new JObject();
        try
        {

            TransactionOpr opr = new TransactionOpr();
            int resp = opr.UpdateRepairedCardOrder(inputListUpdateRepairedCard.repairId,
                                                inputListUpdateRepairedCard.anaKredi,
                                                inputListUpdateRepairedCard.yedekKredi,
                                                inputListUpdateRepairedCard.expertUserId);

            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

            opr.Dispose();
            opr = null;

            if (resp == 0)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UpdateRepairedCard");
        }
        return data;
    }



    private JObject GetMuhasebeStatus()
    {
        JObject data = new JObject();
        try
        {
            TransactionOpr opr = new TransactionOpr();
            MuhasebeStatusCollection names = opr.GetMuhasebeStatusOrder();
            opr.Dispose();
            opr = null;

            if (names != null & names.Count > 0)
            {
                data = names.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetMuhasebeStatus");
        }
        return data;
    }

    private JObject SearchFailedTransactions(string searchText
                                , DateTime startDate
                                , DateTime endDate
                                , int numberOfItemsPerPage
                                , int currentPageNum
                                , int recordCount
                                , int pageCount
                                , int orderSelectionColumn
                                , int descAsc
                                , int customerId
                                , string kioskId
                                , int instutionId
                                , int operationTypeId
                                , string txnStatusId
                                , int orderType)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            TransactionOpr transactionOpr = new TransactionOpr();
            if (customerId == 0)
            {
                FailedTransactionCollection transactions = transactionOpr.SearchFailedTransactionsOrder(searchText, startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, kioskId, instutionId, operationTypeId, txnStatusId, orderType);

            }
            else
            {
                this.transactions = transactionOpr.SearchCustomerTransactionsOrder(searchText, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, customerId, kioskId, instutionId, operationTypeId, txnStatusId);
            }
            transactionOpr.Dispose();
            transactionOpr = null;

            if (this.transactions != null & this.transactions.Count > 0)
            {
                data = this.transactions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchFailedTransactions");
        }
        return data;
    }

    private JObject SearchTransactionConditions(string searchText
                           , DateTime startDate
                           , DateTime endDate
                           , int numberOfItemsPerPage
                           , int currentPageNum
                           , int recordCount
                           , int pageCount
                           , int orderSelectionColumn
                           , int descAsc
                           , string kioskId
                           , int instutionId
                           , int paymentTypeId
                           , int cardTypeId
                           , int completeStatusId
                           , int paymentType
                           , int orderType                    
        )
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;
            string totalTotalAmount = "";
            string totalCashAmount = "";
            string totalCodeAmount = "";
            string totalInsttutionAmount = "";
            string totalKioskUsageFee = "";
            string totalCalculatedChangeAmount = "";
            string totalGivenChangeAmount = "";
            string totalPaidFaturaCount = "";
            string totalCreditCardAmount = "";
            string totalCommisionAmount = "";

            TransactionOpr transactionOpr = new TransactionOpr();

            TransactionConditionCollection transactionConditions = transactionOpr.SearchTransactionConditionsOrder(searchText
                                                                                                                , startDate
                                                                                                                , endDate
                                                                                                                , numberOfItemsPerPage
                                                                                                                , currentPageNum
                                                                                                                , ref recordCount
                                                                                                                , ref pageCount
                                                                                                                , orderSelectionColumn
                                                                                                                , descAsc
                                                                                                                , kioskId
                                                                                                                , instutionId
                                                                                                                , paymentTypeId
                                                                                                                , cardTypeId
                                                                                                                , completeStatusId
                                                                                                                , paymentType
                                                                                                                , ref totalTotalAmount
                                                                                                                , ref totalCashAmount
                                                                                                                , ref totalCodeAmount
                                                                                                                , ref totalInsttutionAmount
                                                                                                                , ref totalKioskUsageFee
                                                                                                                , ref totalCalculatedChangeAmount
                                                                                                                , ref totalGivenChangeAmount
                                                                                                                , ref totalPaidFaturaCount
                                                                                                                , ref totalCreditCardAmount
                                                                                                                , ref totalCommisionAmount
                                                                                                                , orderType);

            transactionOpr.Dispose();
            transactionOpr = null;

            if (transactionConditions != null & transactionConditions.Count > 0)
            {
                data = transactionConditions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
            data.Add("totalTotalAmount", totalTotalAmount);
            data.Add("totalCashAmount", totalCashAmount);
            data.Add("totalCodeAmount", totalCodeAmount);
            data.Add("totalInsttutionAmount", totalInsttutionAmount);
            data.Add("totalKioskUsageFee", totalKioskUsageFee);
            data.Add("totalCalculatedChangeAmount", totalCalculatedChangeAmount);
            data.Add("totalGivenChangeAmount", totalGivenChangeAmount);
            data.Add("totalPaidFaturaCount", totalPaidFaturaCount);
            data.Add("totalCreditCardAmount", totalCreditCardAmount);
            data.Add("totalCommisionAmount", totalCommisionAmount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchTransactionConditions");
        }
        return data;
    }


    private JObject GetPaymentType()
    {
        JObject data = new JObject();
        try
        {
            TransactionOpr transactionOpr = new TransactionOpr();
            PaymentTypeCollection names = transactionOpr.GetPaymentTypeOrder();
            transactionOpr.Dispose();
            transactionOpr = null;

            if (names != null & names.Count > 0)
            {
                data = names.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetPaymentType");
        }
        return data;
    }

    //GetCardRepairType
    private JObject GetCardRepairStatus()
    {
        JObject data = new JObject();
        try
        {
            TransactionOpr transactionOpr = new TransactionOpr();
            CardRepairTypeCollection names = transactionOpr.GetCardRepairStatusOrder();
            transactionOpr.Dispose();
            transactionOpr = null;

            if (names != null & names.Count > 0)
            {
                data = names.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetCardRepairType");
        }
        return data;
    }


    //GetCardRepairType
    private JObject GetIsCardDeleted()
    {
        JObject data = new JObject();
        try
        {
            TransactionOpr transactionOpr = new TransactionOpr();
            IsCardDeletedCollection names = transactionOpr.GetIsCardDeletedOrder();
            transactionOpr.Dispose();
            transactionOpr = null;

            if (names != null & names.Count > 0)
            {
                data = names.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetIsCardDeleted");
        }
        return data;
    }


    private JObject SearchTransactionConditionExcel(string searchText
                       , DateTime startDate
                       , DateTime endDate
                       , int orderSelectionColumn
                       , int descAsc
                       , string kioskId
                       , int instutionId
                       , int paymentTypeId
                       , int cardTypeId
                       , int completeStatusId)
    {

        JObject data = new JObject();
        try
        {
            TransactionOpr transactionOpr = new TransactionOpr();

            TransactionConditionCollection transactionConditions = transactionOpr.SearchTransactionConditionExcelOrder(searchText
                                                                                                                , startDate
                                                                                                                , endDate
                                                                                                                , orderSelectionColumn
                                                                                                                , descAsc
                                                                                                                , kioskId
                                                                                                                , instutionId
                                                                                                                , paymentTypeId
                                                                                                                , cardTypeId
                                                                                                                , completeStatusId);

            transactionOpr.Dispose();
            transactionOpr = null;

            if (transactionConditions != null & transactionConditions.Count > 0)
            {
                data = transactionConditions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchTransactionConditionExcel");
        }
        return data;
    }

    private JObject SearchPurseTransactions(string searchText
                              , DateTime startDate
                              , DateTime endDate
                              , int numberOfItemsPerPage
                              , int currentPageNum
                              , int recordCount
                              , int pageCount
                              , int orderSelectionColumn
                              , int descAsc)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            TransactionOpr transactionOpr = new TransactionOpr();

            this.purseTransactions = transactionOpr.SearchPurseTransactionsOrder(searchText, startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc);

            transactionOpr.Dispose();
            transactionOpr = null;

            if (this.purseTransactions != null & this.purseTransactions.Count > 0)
            {
                data = this.purseTransactions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchTransactions");
        }
        return data;
    }

    private JObject SearchAccountTransactions(string searchText
                              , DateTime startDate
                              , DateTime endDate
                              , int numberOfItemsPerPage
                              , int currentPageNum
                              , int recordCount
                              , int pageCount
                              , int orderSelectionColumn
                              , int descAsc
                              , string platform
                              , int whichAction)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;
            string earnMoney = "";
            string transferMoney = "";

            TransactionOpr transactionOpr = new TransactionOpr();

            AccountTransactionCollection accountTransactions = transactionOpr.SearchAccountTransactionsOrder(searchText, startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, ref transferMoney, ref earnMoney, platform, whichAction);

            transactionOpr.Dispose();
            transactionOpr = null;

            if (accountTransactions != null & accountTransactions.Count > 0)
            {
                data = accountTransactions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
            data.Add("totalEarnedAmount", earnMoney);
            data.Add("totalTransferAmount", transferMoney);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchAccountTransactions");
        }
        return data;
    }

    private JObject ListAcDailycountTransactions(Int64 itemId
                              , int numberOfItemsPerPage
                              , int currentPageNum
                              , int recordCount
                              , int pageCount
                              , int orderSelectionColumn
                              , int descAsc)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;
            string earnMoney = "";
            string transferMoney = "";

            TransactionOpr transactionOpr = new TransactionOpr();

            AccountTransactionCollection accountTransactions = transactionOpr.ListDailyAccountTransactionsOrder(itemId, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, ref transferMoney, ref earnMoney);

            transactionOpr.Dispose();
            transactionOpr = null;

            if (accountTransactions != null & accountTransactions.Count > 0)
            {
                data = accountTransactions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
            data.Add("totalEarnedAmount", earnMoney);
            data.Add("totalTransferAmount", transferMoney);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchAccountTransactions");
        }
        return data;
    }

    private JObject ListDailyMoneyCodes(Int64 itemId
                          , int isUsed
                          , int numberOfItemsPerPage
                          , int currentPageNum
                          , int recordCount
                          , int pageCount
                          , int orderSelectionColumn
                          , int descAsc)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;
            string totalMoney = "";

            MoneyCodeOpr opr = new MoneyCodeOpr();

            MoneyCodeCollection accountTransactions = opr.ListDailyMoneyCodes(itemId, isUsed, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, ref totalMoney);

            opr.Dispose();
            opr = null;

            if (accountTransactions != null & accountTransactions.Count > 0)
            {
                data = accountTransactions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
            data.Add("totalAmount", totalMoney);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchAccountTransactions");
        }
        return data;
    }

    private JObject ListDailyCustomerAccounts(Int64 itemId
                                            , int isActive
                                            , int numberOfItemsPerPage
                                            , int currentPageNum
                                            , int recordCount
                                            , int pageCount
                                            , int orderSelectionColumn
                                            , int descAsc)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;
            string totalMoney = "";

            TransactionOpr opr = new TransactionOpr();

            CustomerAccountCollection accountTransactions = opr.ListDailyCustomerAccounts(itemId, isActive, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, ref totalMoney);

            opr.Dispose();
            opr = null;

            if (accountTransactions != null & accountTransactions.Count > 0)
            {
                data = accountTransactions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
            data.Add("totalAmount", totalMoney);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchAccountTransactions");
        }
        return data;
    }

    private JObject ListAcDailycountTransactionsProkis(Int64 itemId
                             , int numberOfItemsPerPage
                             , int currentPageNum
                             , int recordCount
                             , int pageCount
                             , int orderSelectionColumn
                             , int descAsc)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;
            string earnMoney = "";
            string transferMoney = "";

            TransactionOpr transactionOpr = new TransactionOpr();

            AccountTransactionProkisCollection accountTransactions = transactionOpr.ListDailyAccountTransactionsProkisOrder(itemId, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, ref transferMoney, ref earnMoney);

            transactionOpr.Dispose();
            transactionOpr = null;

            if (accountTransactions != null & accountTransactions.Count > 0)
            {
                data = accountTransactions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
            data.Add("totalEarnedAmount", earnMoney);
            data.Add("totalTransferAmount", transferMoney);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchAccountTransactions");
        }
        return data;
    }

    private JObject SearchTransactionsForExcel(string searchText
                               , DateTime startDate
                               , DateTime endDate
                               , int orderSelectionColumn
                               , int descAsc
                               , int customerId
                               , string kioskId
                               , int instutionId
                               , int operationTypeId
                               , string txnStatusId)
    {

        JObject data = new JObject();
        try
        {

            TransactionOpr transactionOpr = new TransactionOpr();
            if (customerId == 0)
            {
                this.transactions = transactionOpr.SearchTransactionsOrderForExcel(searchText, startDate, endDate, orderSelectionColumn, descAsc, kioskId, instutionId, operationTypeId, txnStatusId);

            }
            else
            {
                this.transactions = transactionOpr.SearchCustomerTransactionsOrderForExcel(searchText, orderSelectionColumn, descAsc, customerId, kioskId, instutionId, operationTypeId);
            }
            transactionOpr.Dispose();
            transactionOpr = null;

            if (this.transactions != null & this.transactions.Count > 0)
            {
                data = this.transactions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchTransactionsForExcel");
        }
        return data;
    }

    private JObject SearchPurseTransactionsForExcel(string searchText
                              , DateTime startDate
                              , DateTime endDate
                              , int orderSelectionColumn
                              , int descAsc)
    {

        JObject data = new JObject();
        try
        {

            TransactionOpr transactionOpr = new TransactionOpr();

            this.purseTransactions = transactionOpr.SearchPurseTransactionsOrderForExcel(searchText, startDate, endDate, orderSelectionColumn, descAsc);

            transactionOpr.Dispose();
            transactionOpr = null;

            if (this.purseTransactions != null & this.purseTransactions.Count > 0)
            {
                data = this.purseTransactions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchTransactionsForExcel");
        }
        return data;
    }

    private JObject SearchAccountTransactionsForExcel(string searchText
                              , DateTime startDate
                              , DateTime endDate
                              , int orderSelectionColumn
                              , int descAsc
                              , string platform
                              , int whichAction)
    {

        JObject data = new JObject();
        try
        {

            TransactionOpr transactionOpr = new TransactionOpr();

            AccountTransactionCollection accountTransactions = transactionOpr.SearchAccountTransactionsOrderForExcel(searchText, startDate, endDate, orderSelectionColumn, descAsc, platform, whichAction);

            transactionOpr.Dispose();
            transactionOpr = null;

            if (accountTransactions != null & accountTransactions.Count > 0)
            {
                data = accountTransactions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchTransactionsForExcel");
        }
        return data;
    }

    private JObject ListDailyAccountTransactionsForExcel(Int64 itemId
                             , int orderSelectionColumn
                             , int descAsc)
    {

        JObject data = new JObject();
        try
        {

            TransactionOpr transactionOpr = new TransactionOpr();

            AccountTransactionCollection accountTransactions = transactionOpr.ListDailyAccountTransactionsOrderForExcel(itemId, orderSelectionColumn, descAsc);

            transactionOpr.Dispose();
            transactionOpr = null;

            if (accountTransactions != null & accountTransactions.Count > 0)
            {
                data = accountTransactions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchTransactionsForExcel");
        }
        return data;
    }

    private JObject ListDailyAccountTransactionsProkisForExcel(Int64 itemId
                            , int orderSelectionColumn
                            , int descAsc)
    {

        JObject data = new JObject();
        try
        {

            TransactionOpr transactionOpr = new TransactionOpr();

            AccountTransactionProkisCollection accountTransactions = transactionOpr.ListDailyAccountTransactionsProkisOrderForExcel(itemId, orderSelectionColumn, descAsc);

            transactionOpr.Dispose();
            transactionOpr = null;

            if (accountTransactions != null & accountTransactions.Count > 0)
            {
                data = accountTransactions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchTransactionsForExcel");
        }
        return data;
    }

    private JObject ListDailyAskiCodesForExcel(Int64 itemId
                        , int isUsed
                        , int orderSelectionColumn
                        , int descAsc)
    {

        JObject data = new JObject();
        try
        {

            MoneyCodeOpr transactionOpr = new MoneyCodeOpr();

            MoneyCodeCollection accountTransactions = transactionOpr.ListDailyMoneyCodesOrderForExcel(itemId, isUsed, orderSelectionColumn, descAsc);

            transactionOpr.Dispose();
            transactionOpr = null;

            if (accountTransactions != null & accountTransactions.Count > 0)
            {
                data = accountTransactions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchTransactionsForExcel");
        }
        return data;
    }

    private JObject ListDailyCustomerAccountsForExcel(Int64 itemId
                       , int isActive
                       , int orderSelectionColumn
                       , int descAsc)
    {

        JObject data = new JObject();
        try
        {

            TransactionOpr transactionOpr = new TransactionOpr();

            CustomerAccountCollection accountTransactions = transactionOpr.ListDailyCustomerAccountsOrderForExcel(itemId, isActive, orderSelectionColumn, descAsc);

            transactionOpr.Dispose();
            transactionOpr = null;

            if (accountTransactions != null & accountTransactions.Count > 0)
            {
                data = accountTransactions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchTransactionsForExcel");
        }
        return data;
    }

    private JObject CancelApprovedTransaction(string itemId)
    {

        JObject data = new JObject();
        try
        {

            TransactionOpr transactionOpr = new TransactionOpr();

            CancelTransactionCollection accountTransactions = transactionOpr.GetApprovedSaleCancelParameters(Convert.ToInt64(itemId));

            transactionOpr.Dispose();
            transactionOpr = null;

            if (accountTransactions != null & accountTransactions.Count > 0)
            {
                data = accountTransactions.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchTransactionsForExcel");
        }
        return data;
    }

    private JObject GetTransactionDetail(int transactionId)
    {

        JObject data = new JObject();
        try
        {
            TransactionOpr transactionOpr = new TransactionOpr();

            TransactionDetailCollection transactionDetails = transactionOpr.GetTransactionDetails(transactionId);
            transactionOpr.Dispose();
            transactionOpr = null;

            if (transactionDetails != null & transactionDetails.Count > 0)
            {
                data = transactionDetails.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetTransactionDetail");
        }
        return data;
    }

    public static string CancelTnxServiceBodyData(int customerNo,
                                          string billDate,
                                          int billNumber,
                                          int vezneNo,
                                          string veznePassword,
                                          string explanation)
    {
        return "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
   "<soap:Body>" +
      "<satisIptal xmlns=\"http://tempuri.org/\">" +
       "<pAboneNo>" + customerNo + "</pAboneNo>" +
       "<pMakbuzTarihi>" + billDate + "</pMakbuzTarihi>" +
       "<pMakbuzNo>" + billNumber + "</pMakbuzNo>" +
       "<pSicil>" + vezneNo + "</pSicil>" +
       "<pSifre>" + veznePassword + "</pSifre>" +
       "<pAciklama>" + explanation + "</pAciklama>" +
      "</satisIptal>" +
   "</soap:Body>" +
"</soap:Envelope>";

    }

    private JObject CancelTransactionLog(int transactionId, int userId, int adminUserId, string customerNo, string billDate, string billNumber, string serviceResult)
    {

        JObject data = new JObject();
        try
        {
            TransactionOpr transactionOpr = new TransactionOpr();

            string transactionRes = transactionOpr.LogCancelTnxOperation(transactionId, userId, adminUserId, customerNo, billDate, billNumber, serviceResult);
            transactionOpr.Dispose();
            transactionOpr = null;

            if (transactionRes != null)
            {
                data.Add("returnCode", transactionRes);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetTransactionDetail");
        }
        return data;
    }

    private int CallTxnCancelService(int transactionId, int userId, int adminUserId, int customerNo, string billDate, int billNumber)
    {
        int resp = 0;
        try
        {

            AskiWebService.ServiceSoapClient askiWebServis = new AskiWebService.ServiceSoapClient();
            int result=askiWebServis.satisIptal(customerNo, Convert.ToDateTime(billDate), billNumber, Convert.ToInt32(Utility.GetConfigValue("AskiSicil")), Utility.GetConfigValue("AskiSifre"), "");
       
            /*
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            string responseText = Utility.ExecuteHttpRequest
                       ("http://kioskservis.aski.gov.tr/service.asmx?op=satisIptal",
                       "POST",
                       "text/xml; charset=utf-8",
                       "",
                       System.Text.Encoding.UTF8.GetBytes(CancelTnxServiceBodyData(customerNo,
                                                                                       Convert.ToDateTime(billDate).ToString("yyyy-MM-ddT00:00:00"),
                                                                                       billNumber,
                                                                                       Convert.ToInt32(Utility.GetConfigValue("AskiSicil")),
                                                                                       Utility.GetConfigValue("AskiSifre"),
                                                                                       "")),
                                                                                       "", ref statusCode);
            
            //Log.Debug("CPO84 - AskiLoading Operasyonu bitti. Sonuç : " + responseText);

            if (statusCode == System.Net.HttpStatusCode.OK)
            {
                resp = Utility.ConvertXML2Json(responseText);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
                errorCode = ReturnCodes.NO_DATA_FOUND;
            */

            if (result==1)
            {
                resp = result;
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
                errorCode = ReturnCodes.NO_DATA_FOUND;
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
        }
        return resp;
    }

    private int CallChangeTxnService(int transactionId, int userId, int adminUserId, string newStatus, string explanation)
    {
        int transactionDetails=0;

        try
        {
            TransactionOpr transactionOpr = new TransactionOpr();

            transactionDetails = transactionOpr.ChangeTransactionStatus(
                transactionId,
                userId,
                adminUserId,
                newStatus,
                explanation);

            transactionOpr.Dispose();
            transactionOpr = null;

            if (transactionDetails != null)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CallChangeTxnService");
        }
        return transactionDetails;
    }

    private JObject GetPurseTransactionDetail(int transactionId)
    {

        JObject data = new JObject();
        try
        {
            TransactionOpr transactionOpr = new TransactionOpr();

            TransactionDetailCollection transactionDetails = transactionOpr.GetPurseTransactionDetails(transactionId);
            transactionOpr.Dispose();
            transactionOpr = null;

            if (transactionDetails != null & transactionDetails.Count > 0)
            {
                data = transactionDetails.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetTransactionDetail");
        }
        return data;
    }


    public JObject GetJSON(Transaction theTransaction)
    {
        JObject jObjectDis = new JObject();

        string a = JsonConvert.SerializeObject(theTransaction, Newtonsoft.Json.Formatting.None);

        JObject x = JObject.Parse(a);


        jObjectDis.Add("Transaction", x);

        return jObjectDis;
    }
}