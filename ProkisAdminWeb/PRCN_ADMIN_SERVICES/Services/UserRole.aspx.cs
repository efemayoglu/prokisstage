﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Backend;
using PRCNCORE.Constants;
using PRCNCORE.Input;
using PRCNCORE.Input.Role;
using PRCNCORE.Roles;
using PRCNCORE.RoleSubMenus;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Services_UserRole : System.Web.UI.Page
{
    private ReturnCodes errorCode = ReturnCodes.SUCCESSFULL;
    InputListRoles listRoles = null;
    private RoleCollection roles;
    private RoleNameCollection roleNames;
    private StatusNameCollection statusNames;


    JObject data = new JObject();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.HttpMethod == "POST")
        {
            ProcessRequest();
        }
    }

    private void ProcessRequest()
    {
        SwitchInput inputData = null;


        try
        {
            string sData = WebServiceUtils.LoadParameters(Request, InputType.Clear);

            if (!string.IsNullOrEmpty(sData))
            {
                inputData = JsonConvert.DeserializeObject<SwitchInput>(sData);

                if (!WebServiceUtils.CheckCredentials(inputData.ApiKey, inputData.ApiPass))
                {
                    errorCode = ReturnCodes.CREDENTIALS_ERROR;
                }
                else if (Request.Headers["AccessToken"] == null ||
                         !WebServiceUtils.CheckAccessTokenSecurity(Request.Headers["AccessToken"],
                             inputData.adminUserId))
                {
                    errorCode = ReturnCodes.INVALID_ACCESS_TOKEN;
                }
                else
                {
                    data = PrepareResponse(inputData.FunctionCode, sData, inputData.adminUserId, inputData.pageUrl);
                    //responseData = WebServiceUtils.AddErrorNodes(responseData, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
                }
            }
            else
            {
                errorCode = ReturnCodes.LOAD_JSON_PARAMETERS_ERROR;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;

            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ProcessRequestUserRole");
        }

        data = WebServiceUtils.AddErrorNodes(data, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
        WebServiceUtils.SendResponse(data, Response, false);
    }


    private JObject PrepareResponse(int functionCode, string input, long adminUserId, string pageUrl)
    {
        JObject data = new JObject();


        try
        {
            switch ((FC) functionCode)
            {
                case FC.SEARCH_ROLE:
                    listRoles = JsonConvert.DeserializeObject<InputListRoles>(input);
                    data = SearchRoles(listRoles.SearchText
                        , listRoles.NumberOfItemsPerPage
                        , listRoles.CurrentPageNum
                        , listRoles.RecordCount
                        , listRoles.PageCount
                        , listRoles.OrderSelectionColumn
                        , listRoles.DescAsc);
                    break;

                case FC.GET_ROLE:
                    InputGetRole roleId = JsonConvert.DeserializeObject<InputGetRole>(input);
                    data = GetRole(roleId.roleId);
                    break;
                case FC.APPROVE_GET_ROLE:
                    InputGetRole rId = JsonConvert.DeserializeObject<InputGetRole>(input);
                    data = GetApproveRole(rId.roleId);
                    break;
                case FC.ADD_ROLE:
                    InputSaveRole saveRole = JsonConvert.DeserializeObject<InputSaveRole>(input);
                    data = SaveRole(saveRole);
                    break;

                case FC.ADD_ROLE_SUBMENU:
                    InputSaveRoleSubMenus inputSaveRoleSubMenus =
                        JsonConvert.DeserializeObject<InputSaveRoleSubMenus>(input);
                    data = SaveRoleSubMenus(inputSaveRoleSubMenus);
                    break;

                case FC.UPDATE_ROLE:
                    InputUpdateRole updateRole = JsonConvert.DeserializeObject<InputUpdateRole>(input);
                    data = UpdateRole(updateRole);
                    break;



                case FC.APPROVE_ROLE_CONTEXT:
                    InputApproveRoleContext inputApproveRoleContext = JsonConvert.DeserializeObject<InputApproveRoleContext>(input);
                    inputApproveRoleContext.ApprovedUserId = adminUserId;
                    data = ApproveRoleContext(inputApproveRoleContext);
                    break;
                case FC.UPDATE_ROLE_CONTEXT:
                    InputSaveRoleSubMenusContext inputSaveRoleSubMenusContext = JsonConvert.DeserializeObject<InputSaveRoleSubMenusContext>(input);
                    data = SaveRoleSubMenusContext(inputSaveRoleSubMenusContext);
                    break;

                case FC.APPROVE_ROLE:
                    listRoles = JsonConvert.DeserializeObject<InputListRoles>(input);
                    data = SearchApproveRoles(listRoles.SearchText
                        , listRoles.NumberOfItemsPerPage
                        , listRoles.CurrentPageNum
                        , listRoles.RecordCount
                        , listRoles.PageCount);
                    break;
                //InputLıstMoneyCodes
                case FC.USER_LOG_DETAIL:
                    InputListTransactions userlogdetail = JsonConvert.DeserializeObject<InputListTransactions>(input);
                    data = SearchUserLogDetail(userlogdetail);
                    break;
                case FC.ROLE_ACTION:
                    data = CallRoleActionList();
                    break;
                case FC.ROLE_CONTEXT:
                    var roleid = JsonConvert.DeserializeObject<RoleContext>(input);
                    if (roleid != null) data = CallRoleSubContextMenuList(roleid.RoleId);
                    break;
                default:
                    break;
            }   
            ManageContextRolescs contextRoles = new ManageContextRolescs();
            contextRoles.EditByRoles(functionCode, data, adminUserId, pageUrl);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;

            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "PrepareResponseUserRole");
        }

        return data;
    }

    private JObject GetRoleNames()
    {
        JObject data = new JObject();
        try
        {
            RoleOpr roleOpr = new RoleOpr();
            this.roleNames = roleOpr.GetRoleNames();
            roleOpr.Dispose();
            roleOpr = null;

            if (this.roleNames != null & this.roleNames.Count > 0)
            {
                data = this.roleNames.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetRoleNames");
        }


        return data;
    }

    private JObject GetStatusNames()
    {
        JObject data = new JObject();
        try
        {
            RoleOpr roleOpr = new RoleOpr();
            this.statusNames = roleOpr.GetStatusNames();
            roleOpr.Dispose();
            roleOpr = null;

            if (this.statusNames != null & this.statusNames.Count > 0)
            {
                data = this.statusNames.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetStatusNames");
        }


        return data;
    }

    private JObject SearchRoles(string searchText
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount
        , int orderSelectionColumn
        , int descAsc)
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            RoleOpr roleOpr = new RoleOpr();
            this.roles = roleOpr.SearchRolesOrder(searchText, numberOfItemsPerPage, currentPageNum, ref recordCount,
                ref pageCount, orderSelectionColumn, descAsc);
            roleOpr.Dispose();
            roleOpr = null;

            if (this.roles != null & this.roles.Count > 0)
            {
                data = this.roles.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchRoles");
        }


        return data;
    }

    private JObject SearchApproveRoles(string searchText
        , int numberOfItemsPerPage
        , int currentPageNum
        , int recordCount
        , int pageCount)
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            RoleOpr roleOpr = new RoleOpr();
            ApproveRoleCollection approveRoles = roleOpr.SearchApproveRoles(searchText, numberOfItemsPerPage,
                currentPageNum, ref recordCount, ref pageCount);
            roleOpr.Dispose();
            roleOpr = null;

            if (approveRoles != null & approveRoles.Count > 0)
            {
                data = approveRoles.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchApproveRoles");
        }


        return data;
    }


    private JObject SearchUserLogDetail(InputListTransactions userlogdetail)
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = userlogdetail.RecordCount;
            int pageCnt = userlogdetail.PageCount;

            RoleOpr roleOpr = new RoleOpr();
            UserLogDetailCollection userlogdetailCollection = roleOpr.SearchUserLogDetail(userlogdetail.SearchText
                , userlogdetail.StartDate
                , userlogdetail.EndDate
                , userlogdetail.NumberOfItemsPerPage
                , userlogdetail.CurrentPageNum
                , ref recordCnt
                , ref pageCnt
                , userlogdetail.OrderSelectionColumn
                , userlogdetail.DescAsc
                , userlogdetail.OrderType);
            roleOpr.Dispose();
            roleOpr = null;

            if (userlogdetail != null & userlogdetailCollection.Count > 0)
            {
                data = userlogdetailCollection.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCnt, pageCnt);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchApproveRoles");
        }


        return data;
    }

    private JObject GetApproveRole(int roleId)
    {
        JObject data = new JObject();
        try
        {
            string oldName = "";
            string newName = "";
            string oldDesc = "";
            string newDesc = "";
            SubMenuOpr subMenuOpr = new SubMenuOpr();

            RoleSubMenuCollection oldroleSubMenus = subMenuOpr.GetRoleMenus(roleId);
            RoleSubMenuCollection roleSubMenus = subMenuOpr.GetNewRoleMenus(roleId);
            subMenuOpr.GetRoleNameDesc(roleId, 1, ref oldName, ref oldDesc);
            subMenuOpr.GetRoleNameDesc(roleId, 2, ref newName, ref newDesc);

            data.Add("OldName", oldName);
            data.Add("OldDescription", oldDesc);
            data.Add("OldRoleSubMenus", oldroleSubMenus.GetJSONJArray());
            data.Add("NewName", newName);
            data.Add("NewDescription", newDesc);
            data.Add("NewRoleSubMenus", roleSubMenus.GetJSONJArray());
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetRole");
        }


        return data;
    }


    private JObject GetRole(int roleId)
    {
        JObject data = new JObject();
        try
        {
            RoleOpr roleOpr = new RoleOpr();
            Role role = roleOpr.GetRole(roleId);
            roleOpr.Dispose();
            roleOpr = null;

            if (role != null)
            {
                data = GetJSON(role);

                SubMenuOpr subMenuOpr = new SubMenuOpr();

                RoleSubMenuCollection roleSubMenus = subMenuOpr.GetRoleMenus(role.Id);

                data.Add("RoleSubMenus", roleSubMenus.GetJSONJArray());

                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetRole");
        }


        return data;
    }

    public JObject GetJSON(Role role)
    {
        JObject jObjectDis = new JObject();

        string a = JsonConvert.SerializeObject(role, Newtonsoft.Json.Formatting.None);

        JObject x = JObject.Parse(a);

        jObjectDis.Add("Role", x);

        return jObjectDis;
    }

    private JObject SaveRole(InputSaveRole role)
    {
        JObject data = new JObject();
        try
        {
            RoleOpr roleOpr = new RoleOpr();
            int roleId = 0;

            int i = roleOpr.AddNewRole(ref roleId, role.name, role.description, role.creationUserId);
            if (i == 0)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
                data.Add("roleId", roleId);
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveRole");
        }


        return data;
    }

    private JObject UpdateRole(InputUpdateRole role)
    {
        JObject data = new JObject();
        try
        {
            RoleOpr roleOpr = new RoleOpr();

            bool resp = roleOpr.UpdateRole(role.id, role.name, role.description, role.updatedUserId);
            if (resp == true)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UpdateRole");
        }


        return data;
    }

    private JObject SaveRoleSubMenus(InputSaveRoleSubMenus roleSubMenus)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("RoleId", typeof(System.Int32)));
        dt.Columns.Add(new DataColumn("Edit", typeof(System.Int32)));
        dt.Columns.Add(new DataColumn("Delete", typeof(System.Int32)));
        dt.Columns.Add(new DataColumn("Add", typeof(System.Int32)));
        dt.Columns.Add(new DataColumn("SubMenuId", typeof(System.String)));
        dt.Columns.Add(new DataColumn("CreatedserId", typeof(System.Int32)));


        JObject data = new JObject();
        try
        {
            for (int i = 0; i < roleSubMenus.RoleSubMenus.Count; i++)
            {
                dt.Rows.Add(roleSubMenus.RoleSubMenus[i].roleId, roleSubMenus.RoleSubMenus[i].canEdit,
                    roleSubMenus.RoleSubMenus[i].canDelete, roleSubMenus.RoleSubMenus[i].canAdd,
                    roleSubMenus.RoleSubMenus[i].subMenuId, roleSubMenus.CreatedserId);
            }

            SubMenuOpr subMenuOpr = new SubMenuOpr();

            bool resp = subMenuOpr.SaveRoleSubMenus(dt);
            if (resp == true)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveRoleSubMenus");
        }


        return data;
    }

    private JObject ApproveRoleContext(InputApproveRoleContext approveRoleContext)
    {
        JObject data = new JObject();
        try
        {
            RoleOpr roleOpr = new RoleOpr();

            if(approveRoleContext.OperationType == 0)// Onaylama
            {
                bool resp = roleOpr.ApproveRoleContext(approveRoleContext.RoleId, approveRoleContext.ApprovedUserId);
                if (resp == true)
                {
                    errorCode = ReturnCodes.SUCCESSFULL;
                }
            }
            else if(approveRoleContext.OperationType == 1)//Onaylamama Silme
            {
                bool resp = roleOpr.CancelRoleContext(approveRoleContext.RoleId, approveRoleContext.ApprovedUserId);
                if (resp == true)
                {
                    errorCode = ReturnCodes.SUCCESSFULL;
                }
            }


        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UpdateRole");
        }


        return data;
    }


    private JObject SaveRoleSubMenusContext(InputSaveRoleSubMenusContext roleSubMenus)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("RoleId", typeof(System.Int32)));
        dt.Columns.Add(new DataColumn("SubMenuId", typeof(System.Int32)));
        dt.Columns.Add(new DataColumn("RoleActionId", typeof(System.Int32)));
        dt.Columns.Add(new DataColumn("RoleActionValue", typeof(System.Int32)));


        JObject data = new JObject();
        try
        {
            for (int i = 0; i < roleSubMenus.Contexts.Count; i++)
            {
                dt.Rows.Add(
                    roleSubMenus.RoleId,
                    roleSubMenus.Contexts[i].SubMenuId,
                    roleSubMenus.Contexts[i].RoleActionId,
                    roleSubMenus.Contexts[i].Status);
            }

            SubMenuOpr subMenuOpr = new SubMenuOpr();

            bool resp = subMenuOpr.SaveRoleSubMenusContext(dt);
            if (resp == true)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveRoleSubMenus");
        }


        return data;
    }


    private JObject CallRoleActionList()
    {
        JObject data = new JObject();
        try
        {
            RoleOpr roleOpr = new RoleOpr();

            var roles = roleOpr.RoleActionList();
            if (roles != null && roles.Any())
                errorCode = ReturnCodes.SUCCESSFULL;

            data["Roles"] = JToken.FromObject(roles);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CallRoleActionList");
        }


        return data;
    }

    private JObject CallRoleSubContextMenuList(int roleId)
    {
        JObject data = new JObject();
        try
        {
            RoleOpr roleOpr = new RoleOpr();

            var roleContexts = roleOpr.RoleSubContextMenuList(roleId);
            if (roleContexts != null && roleContexts.Any())
                    errorCode = ReturnCodes.SUCCESSFULL;
            
            data["RoleContexts"] = JToken.FromObject(roleContexts);

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CallRoleSubContextMenuList");
        }


        return data;
    }
}