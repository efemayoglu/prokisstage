﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE;
using PRCNCORE.Backend;
using PRCNCORE.Constants;
using PRCNCORE.Input;
using PRCNCORE.Roles;
using PRCNCORE.Users;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Services_User : System.Web.UI.Page
{
    private ReturnCodes errorCode = ReturnCodes.SUCCESSFULL;
    JObject data = new JObject();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.HttpMethod == "POST")
        {
            ProcessRequest();
        }
    }

    private void ProcessRequest()
    {
        SwitchInput inputData = null;

        

        try
        {
            string sData = WebServiceUtils.LoadParameters(Request, InputType.Clear);

            if (!string.IsNullOrEmpty(sData))
            {
                inputData = JsonConvert.DeserializeObject<SwitchInput>(sData);

                if (!WebServiceUtils.CheckCredentials(inputData.ApiKey, inputData.ApiPass))
                {
                    errorCode = ReturnCodes.CREDENTIALS_ERROR;
                     
                }
                else if (Request.Headers["AccessToken"] == null || !WebServiceUtils.CheckAccessTokenSecurity(Request.Headers["AccessToken"], inputData.adminUserId))
                {
                    errorCode = ReturnCodes.INVALID_ACCESS_TOKEN;
                }
                else
                {
                    data = PrepareResponse(inputData.FunctionCode, sData, inputData.adminUserId, inputData.pageUrl);
                    //responseData = WebServiceUtils.AddErrorNodes(responseData, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
                }
            }
            else
            {
                errorCode = ReturnCodes.LOAD_JSON_PARAMETERS_ERROR;
                 
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
             
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ProcessRequestUser");
        }
        data = WebServiceUtils.AddErrorNodes(data, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
        WebServiceUtils.SendResponse(data, Response, false);
    }

    private JObject PrepareResponse(int functionCode, string input, long adminUserId, string pageUrl)
    {
        JObject data = new JObject();
        
        try
        {
            switch ((FC)functionCode)
            {
                case FC.SEARCH_USER:
                    InputSearchUsers inputSearchUsers = JsonConvert.DeserializeObject<InputSearchUsers>(input);
                    data = SearchAdminUsers(inputSearchUsers.SearchText, inputSearchUsers.NumberOfItemsPerPage,
                        inputSearchUsers.CurrentPageNum, inputSearchUsers.RecordCount, inputSearchUsers.PageCount, inputSearchUsers.OrderSelectionColumn, inputSearchUsers.DescAsc);
                    break;
                case FC.GET_ROLE_ACTION:
                    data = SearchRoleActions();
                    break;
                case FC.UPDATE_ROLE_ACTION:
                    InputRoleAction inputRoleAction = JsonConvert.DeserializeObject<InputRoleAction>(input);
                    data = UpdateRoleActions(inputRoleAction.Id, inputRoleAction.RoleName, inputRoleAction.Status, inputRoleAction.RoleLabel);
                    break;
                case FC.ADD_ROLE_ACTION:
                    InputRoleAction inputAddRoleAction = JsonConvert.DeserializeObject<InputRoleAction>(input);
                    data = AddRoleAction(inputAddRoleAction.RoleName, inputAddRoleAction.Status, inputAddRoleAction.RoleLabel);
                    break;

                case FC.DELETE_ROLE_ACTION:
                    InputRoleAction roleAction = JsonConvert.DeserializeObject<InputRoleAction>(input);
                    data = DeleteRoleAction(roleAction.Id);
                    break;
                case FC.APPROVE_SEARCH_USER:
                    InputSearchUsers inputSearchApproveUsers = JsonConvert.DeserializeObject<InputSearchUsers>(input);
                    data = SearchApproveUsers(inputSearchApproveUsers.SearchText, inputSearchApproveUsers.NumberOfItemsPerPage,
                        inputSearchApproveUsers.CurrentPageNum, inputSearchApproveUsers.RecordCount, inputSearchApproveUsers.PageCount);
                    break;
                case FC.CHANGE_PASSWORD:
                    InputChangeAdminUserPasswordFirstLogin inputChangeAdminUserPasswordFirstLogin = JsonConvert.DeserializeObject<InputChangeAdminUserPasswordFirstLogin>(input);
                    data = ChangeUserPassword(inputChangeAdminUserPasswordFirstLogin.adminUserID, inputChangeAdminUserPasswordFirstLogin.newPassword,
                          inputChangeAdminUserPasswordFirstLogin.oldPasswords, inputChangeAdminUserPasswordFirstLogin.updatedUser);
                    break;
                case FC.CHANGE_PASSWORD_ADMIN:
                    InputChangeAdminUserPasswordFirstLogin inputChangeAdminUserPasswordFirstLoginAdmin = JsonConvert.DeserializeObject<InputChangeAdminUserPasswordFirstLogin>(input);
                    data = ChangeUserPasswordAdmin(inputChangeAdminUserPasswordFirstLoginAdmin.adminUserID,
                           inputChangeAdminUserPasswordFirstLoginAdmin.updatedUser);
                    break;
                case FC.GET_USER:
                    InputGetAdminUser inputGetAdminUser = JsonConvert.DeserializeObject<InputGetAdminUser>(input);
                    data = GetAdminUser(inputGetAdminUser.userId);
                    break;
                case FC.APPROVE_GET_USER:
                    InputGetAdminUser inputGetApeprovUser = JsonConvert.DeserializeObject<InputGetAdminUser>(input);
                    data = GetApproveUser(inputGetApeprovUser.userId);
                    break;
                case FC.ADD_USER:
                    InputAddNewAdminUser inputAddNewAdminUser = JsonConvert.DeserializeObject<InputAddNewAdminUser>(input);
                    data = AddNewAdminUser(inputAddNewAdminUser.name, inputAddNewAdminUser.userName, inputAddNewAdminUser.email,
                                           inputAddNewAdminUser.password, inputAddNewAdminUser.telephone, inputAddNewAdminUser.cellphone,
                                           inputAddNewAdminUser.status, inputAddNewAdminUser.roleId, inputAddNewAdminUser.updatedUser);
                    break;
                case FC.UPDATE_USER:
                    InputUpdateAdminUser inputUpdateAdminUser = JsonConvert.DeserializeObject<InputUpdateAdminUser>(input);
                    data = UpdateAdminUser(inputUpdateAdminUser.id, inputUpdateAdminUser.name, inputUpdateAdminUser.userName,
                                           inputUpdateAdminUser.email, inputUpdateAdminUser.cellphone, inputUpdateAdminUser.telephone,
                                           inputUpdateAdminUser.roleId, inputUpdateAdminUser.status, inputUpdateAdminUser.updatedUser);
                    break;
                case FC.DELETE_OPERATION:
                    InputDeleteOperation inputDeleteOperation = JsonConvert.DeserializeObject<InputDeleteOperation>(input);
                    data = DeleteOperation(inputDeleteOperation.userId, inputDeleteOperation.storedProcedure, inputDeleteOperation.itemId);
                    break;
                case FC.GET_ROLE_NAMES:
                    data = GetRoleNames();
                    break;
                case FC.LIST_USER_ROLECONNECTION:
                    InputUserRoleConnection inputUserRoleConnection = JsonConvert.DeserializeObject<InputUserRoleConnection>(input);
                    data = GetUserRoleConnection(inputUserRoleConnection.userId);
                    break;
                case FC.LIST_PROJECTS:
                    data = GetProjectNames();
                    break;
                case FC.LIST_ROLE_ROJECT:
                    InputRoleProject inputRoleProject = JsonConvert.DeserializeObject<InputRoleProject>(input);
                    data = GetRoleProjectNames(inputRoleProject.roleId);
                    break;

                case FC.ADD_PROJECT:
                    InputAddProject inputAddProject = JsonConvert.DeserializeObject<InputAddProject>(input);
                    data = AddProject(inputAddProject.name);
                    break;

                case FC.UPDATE_ROLE_PROJECT:
                    InputUpdateRoleProject inputUpdateRoleProject = JsonConvert.DeserializeObject<InputUpdateRoleProject>(input);
                    data = UpdateRoleProject(inputUpdateRoleProject.roleId, inputUpdateRoleProject.projectId);
                    break;


                case FC.UPDATE_ROLECONNECTION:
                    InputUpdateUserRoleConnection inputUpdateUserRoleConnection = JsonConvert.DeserializeObject<InputUpdateUserRoleConnection>(input);
                    data = UpdateUserRoleConnection(inputUpdateUserRoleConnection.userId, inputUpdateUserRoleConnection.roleId);
                    break;
                    


                case FC.GET_STATUS_NAMES:
                    data = GetStatusNames();
                    break;
                case FC.SEARCH_BLACK_LIST:
                    InputSearchUsers inputSearchBlackList = JsonConvert.DeserializeObject<InputSearchUsers>(input);
                    data = SearchBlackList(inputSearchBlackList.SearchText
                        , inputSearchBlackList.NumberOfItemsPerPage
                        , inputSearchBlackList.CurrentPageNum
                        , inputSearchBlackList.RecordCount
                        , inputSearchBlackList.PageCount
                        , inputSearchBlackList.OrderSelectionColumn
                        , inputSearchBlackList.DescAsc);
                    break;
                case FC.ADD_BLACK_LIST:
                    InputAddNewBlackList inputAddNewBlackList = JsonConvert.DeserializeObject<InputAddNewBlackList>(input);
                    data = AddNewBlackList(inputAddNewBlackList.name, inputAddNewBlackList.tckn, inputAddNewBlackList.birthYear,
                                           inputAddNewBlackList.description, inputAddNewBlackList.updatedUser);
                    break;
                default:
                    break;
            }
               
            ManageContextRolescs contextRoles = new ManageContextRolescs();
            contextRoles.EditByRoles(functionCode, data, adminUserId, pageUrl);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
             
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "PrepareResponseUser");
        }

        return data;
    }

    private JObject GetAdminUser(int userId)
    {

         
        JObject data = new JObject();
        WebUser user = null;
        try
        {
            UserOpr opr = new UserOpr();
            user = opr.GetUser(userId);
            opr.Dispose();
            opr = null;

            if (user != null)
            {
                data = GetJSON(user);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetAdminUser");
        }

         
        return data;
    }

    private JObject GetApproveUser(int userId)
    {


        JObject data = new JObject();
        WebUser user = null;
        try
        {
            UserOpr opr = new UserOpr();
            user = opr.GetApproveUser(userId);
            opr.Dispose();
            opr = null;

            if (user != null)
            {
                data = GetJSON(user);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetAdminUser");
        }


        return data;
    }

    private JObject ChangeUserPassword(int adminUserID, string newPassword, string oldPasswords, int updatedUser)
    {

         
        JObject data = new JObject();
        try
        {
            UserOpr opr = new UserOpr(); ;
            bool resp = opr.ChangeUserPassword(adminUserID, newPassword, oldPasswords, updatedUser);
            opr.Dispose();
            opr = null;

            if (resp != true)
                errorCode = ReturnCodes.SYSTEM_ERROR;
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ChangeUserPassword");
        }

         
        return data;
    }

    private JObject ChangeUserPasswordAdmin(int adminUserID, int updatedUser)
    {
        string telephone = "";
        string name = "";
        string email = "";

        JObject data = new JObject();
        try
        {
            string newPin = Utility.GeneratePassword(8, true);

            //string newPassword = Utility.CalculateSHA512Pin(Utility.CalculateSHA512Pin(newPin));
            string newPassword = Utility.CalculateSHA512Pin(newPin);

            UserOpr opr = new UserOpr();
            bool resp = opr.ChangeUserPasswordAdmin(adminUserID, newPassword, updatedUser, out telephone, out email, out name );
            opr.Dispose();
            opr = null;

            if (resp != true)
                errorCode = ReturnCodes.SYSTEM_ERROR;
            else
            {
                string webServiceUrl = Utility.GetConfigValue("SMSWebServiceUrl");
                Utility.SendSMS(webServiceUrl, "Sn. " + name + ", Sistemine giriş yapabilmeniz için yeni şifreniz:  " + newPin + " \r\nİyi Günler Dileriz.", telephone);
                string[] d = { email };
                Utility.SendEmail("info@birlesikodeme.com", "Pratik Nokta", d, "Bilgilendirme", "Sn. " + name + ", Pratik Cüzdan sistemine giriş yapabilmeniz için yeni şifreniz:  " + newPin + " </br>İyi Günler Dileriz.</br>", null);

            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ChangeUserPassword");
        }


        return data;
    }

    private JObject DeleteRoleAction(int roleActionId)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = 0;
            int pageCnt = 0;

            //RoleAction[] roleActions = null;

            UserOpr opr = new UserOpr();

            var response = opr.DeleteRoleAction(roleActionId);
            opr.Dispose();
            opr = null;

            if (response == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

            //if (roleActions != null & roleActions.Length > 0)
            //{
            //    data = DeleteRoleActionGetJSON(roleActions);
            //    errorCode = ReturnCodes.SUCCESSFULL;
            //}
            //else
            //{
            //    errorCode = ReturnCodes.NO_DATA_FOUND;
            //}

            //data = WebServiceUtils.AddPaggingNodes(data, recordCnt, pageCnt);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchAdminUsers");
        }


        return data;
    }



    private JObject AddRoleAction(string roleName, int status, string roleLabel)
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = 0;
            int pageCnt = 0;



            UserOpr opr = new UserOpr();

            var response = opr.AddRoleAction(roleName, status, roleLabel);
            opr.Dispose();
            opr = null;

            if (response == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;


            //data = GetJSON();
            //errorCode = ReturnCodes.SUCCESSFULL;

            //else
            //{
            //    errorCode = ReturnCodes.NO_DATA_FOUND;
            //}

            //data = WebServiceUtils.AddPaggingNodes(data, recordCnt, pageCnt);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchAdminUsers");
        }


        return data;
    }



    private JObject UpdateRoleActions(int roleActionId, string roleName, int status, string roleLabel)
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = 0;
            int pageCnt = 0;

           

            UserOpr opr = new UserOpr();

            var response = opr.UpdateRoleAction(roleActionId, roleName, status, roleLabel);
            opr.Dispose();
            opr = null;

            if (response == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

            
                //data = GetJSON();
                //errorCode = ReturnCodes.SUCCESSFULL;
            
            //else
            //{
            //    errorCode = ReturnCodes.NO_DATA_FOUND;
            //}

            //data = WebServiceUtils.AddPaggingNodes(data, recordCnt, pageCnt);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchAdminUsers");
        }


        return data;
    }




    private JObject SearchRoleActions()
    {


        JObject data = new JObject();
        try
        {
            int recordCnt = 0;
            int pageCnt = 0;

            RoleAction[] roleActions = null;

            UserOpr opr = new UserOpr();

            roleActions = opr.SearchRoleActionsOrder();
            opr.Dispose();
            opr = null;

            if (roleActions != null & roleActions.Length > 0)
            {
                data = GetJSON(roleActions);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCnt, pageCnt);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchAdminUsers");
        }


        return data;
    }

    private JObject SearchAdminUsers(string searchText, int numberOfItemsPerPage, int currentPageNum, int recordCount, int pageCount, int orderSelectionColumn, int orderSelectionDescAsc)
    {

         
        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            WebUser[] users = null;

            UserOpr opr = new UserOpr();
            users = opr.SearchUsersOrder(searchText, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, orderSelectionDescAsc);
            opr.Dispose();
            opr = null;

            if (users != null & users.Length > 0)
            {
                data = GetJSON(users);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchAdminUsers");
        }

         
        return data;
    }

    private JObject SearchBlackList(string searchText, int numberOfItemsPerPage, int currentPageNum, int recordCount, int pageCount, int orderSelectionColumn, int orderSelectionDescAsc)
    {


        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            BlackList[] users = null;

            UserOpr opr = new UserOpr();
            users = opr.SearchBlackListOrder(searchText, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, orderSelectionDescAsc);
            opr.Dispose();
            opr = null;

            if (users != null & users.Length > 0)
            {
                data = GetJSON(users);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchBlackList");
        }


        return data;
    }

    private JObject SearchApproveUsers(string searchText, int numberOfItemsPerPage, int currentPageNum, int recordCount, int pageCount)
    {


        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            ApproveUser[] users = null;

            UserOpr opr = new UserOpr();
            users = opr.SearchApproveUsersOrder(searchText, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount);
            opr.Dispose();
            opr = null;

            if (users != null & users.Length > 0)
            {
                data = GetJSON(users);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchAdminUsers");
        }


        return data;
    }
    

    private JObject UpdateUserRoleConnection(int userId, string roleId)
    {

        JObject data = new JObject();
        try
        {
            UserOpr opr = new UserOpr();
            int resp = opr.UpdateRoleConnection(userId, roleId);
            opr.Dispose();
            opr = null;

            if (resp < 0)
            {
                if (resp == -2)
                    errorCode = ReturnCodes.EXIST_USERNAME;
                else
                    errorCode = ReturnCodes.SYSTEM_ERROR;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "AddNewAdminUser");
        }


        return data;
    }
    private JObject UpdateRoleProject(int roleId, int projectId)
    {

        JObject data = new JObject();
        try
        {
            UserOpr opr = new UserOpr();
            int resp = opr.UpdateRoleProject(roleId, projectId);
            opr.Dispose();
            opr = null;

            if (resp < 0)
            {
                if (resp == -2)
                    errorCode = ReturnCodes.EXIST_USERNAME;
                else
                    errorCode = ReturnCodes.SYSTEM_ERROR;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "AddNewAdminUser");
        }


        return data;
    }
    private JObject AddProject(string name)
    {


        JObject data = new JObject();
        try
        {
            UserOpr opr = new UserOpr();
            int resp = opr.SaveProject(name);
            opr.Dispose();
            opr = null;

            if (resp < 0)
            {
                if (resp == -2)
                    errorCode = ReturnCodes.EXIST_USERNAME;
                else
                    errorCode = ReturnCodes.SYSTEM_ERROR;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "AddNewAdminUser");
        }


        return data;
    }

    private JObject AddNewAdminUser(string name, string userName, string email, string password, string telephone, string cellphone, int status, int roleId, int updatedUser)
    {

         
        JObject data = new JObject();
        try
        {
            UserOpr opr = new UserOpr();
            int resp = opr.SaveUser(name, userName, email, password, telephone, cellphone, status, roleId, updatedUser);
            opr.Dispose();
            opr = null;

            if (resp < 0)
            {
                if (resp == -2)
                    errorCode = ReturnCodes.EXIST_USERNAME;
                else
                    errorCode = ReturnCodes.SYSTEM_ERROR;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "AddNewAdminUser");
        }

         
        return data;
    }

    private JObject AddNewBlackList(string name, string tckn, string birthYear, string description, int updatedUser)
    {


        JObject data = new JObject();
        try
        {
            UserOpr opr = new UserOpr();
            int resp = opr.SaveBlackList(name, tckn, birthYear, description, updatedUser);
            opr.Dispose();
            opr = null;

            if (resp < 0)
            {
                if (resp == -2)
                    errorCode = ReturnCodes.EXIST_USERNAME;
                else
                    errorCode = ReturnCodes.SYSTEM_ERROR;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "AddNewAdminUser");
        }


        return data;
    }

    private JObject UpdateAdminUser(int id, string name, string userName, string email, string cellphone, string telephone, int roleId, int status, int updatedUse)
    {
        JObject data = new JObject();
        try
        {
            UserOpr opr = new UserOpr();
            opr.UpdateUser(id, name, userName, email, cellphone, telephone, roleId, status, updatedUse);
            opr.Dispose();
            opr = null;
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UpdateAdminUser");
        } 
        return data;
    }

    private JObject DeleteOperation(int userId, string storedProcedure, int itemId)
    {   
        JObject data = new JObject();
        try
        {
            int returnCode = 0;
            UserOpr opr = new UserOpr();
            opr.DeleteOperation(userId, storedProcedure, itemId, ref returnCode);
            opr.Dispose();
            opr = null;

            if (returnCode == 1)
                errorCode = ReturnCodes.SUCCESSFULL;
            else if (returnCode == -99)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;
            else if (returnCode == -2)
                errorCode = ReturnCodes.INVALID_SENDER_ACCOUNT;
            else if (returnCode == -3)
                errorCode = ReturnCodes.SENDER_HAS_NO_ACCOUNT;
            else if (returnCode == -4)
                errorCode = ReturnCodes.INVALID_RECEIVER_ACCOUNT;
            else if (returnCode == -5)
                errorCode = ReturnCodes.INSUFFICIENT_BALANCE;

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "DeleteOperation");
        }

         
        return data;
    }


    //public JObject GetJSON(int roleActionId)
    //{
    //    JObject jObjectDis = new JObject();
    //    JArray jArray = new JArray();

    //    foreach (RoleAction item in adminUsers)
    //    {
    //        JObject jo = new JObject();
    //        string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);
    //        JObject x = JObject.Parse(a);
    //        jArray.Add(x);
    //    }
    //    jObjectDis.Add("Roles", jArray);

    //    return jObjectDis;
    //}

    public JObject DeleteRoleActionGetJSON(RoleAction[] roleaction)
    {
        JObject jObjectDis = new JObject();
        JArray jArray = new JArray();

        //foreach (RoleAction item in adminUsers)
        //{
        //    JObject jo = new JObject();
        //    string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);
        //    JObject x = JObject.Parse(a);
        //    jArray.Add(x);
        //}
        //jObjectDis.Add("Roles", jArray);

        return jObjectDis;
    }


    public JObject GetJSON(RoleAction[] adminUsers)
    {
        JObject jObjectDis = new JObject();
        JArray jArray = new JArray();

        foreach (RoleAction item in adminUsers)
        {
            JObject jo = new JObject();
            string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);
            JObject x = JObject.Parse(a);
            jArray.Add(x);
        }
        jObjectDis.Add("Roles", jArray);

        return jObjectDis;
    }



    public JObject GetJSON(WebUser[] adminUsers)
    {
        JObject jObjectDis = new JObject();
        JArray jArray = new JArray();

        foreach (WebUser item in adminUsers)
        {
            JObject jo = new JObject();
            string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);
            JObject x = JObject.Parse(a);
            jArray.Add(x);
        }
        jObjectDis.Add("Users", jArray);

        return jObjectDis;
    }

    public JObject GetJSON(BlackList[] adminUsers)
    {
        JObject jObjectDis = new JObject();
        JArray jArray = new JArray();

        foreach (BlackList item in adminUsers)
        {
            JObject jo = new JObject();
            string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);
            JObject x = JObject.Parse(a);
            jArray.Add(x);
        }
        jObjectDis.Add("BlackLists", jArray);

        return jObjectDis;
    }

    public JObject GetJSON(ApproveUser[] adminUsers)
    {
        JObject jObjectDis = new JObject();
        JArray jArray = new JArray();

        foreach (ApproveUser item in adminUsers)
        {
            JObject jo = new JObject();
            string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);
            JObject x = JObject.Parse(a);
            jArray.Add(x);
        }
        jObjectDis.Add("Users", jArray);

        return jObjectDis;
    }

    private JObject GetRoleProjectNames(int roleId)
    {
        RoleNameCollection roleNames = null;

        JObject data = new JObject();
        try
        {
            RoleOpr roleOpr = new RoleOpr();
            roleNames = roleOpr.GetRoleProjectNames(roleId);
            roleOpr.Dispose();
            roleOpr = null;

            if (roleNames != null & roleNames.Count > 0)
            {
                data = roleNames.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetRoleNames");
        }


        return data;
    }
    private JObject GetProjectNames()
    {
        RoleNameCollection roleNames = null;

        JObject data = new JObject();
        try
        {
            RoleOpr roleOpr = new RoleOpr();
            roleNames = roleOpr.GetProjectNames();
            roleOpr.Dispose();
            roleOpr = null;

            if (roleNames != null & roleNames.Count > 0)
            {
                data = roleNames.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetRoleNames");
        }


        return data;
    }

    private JObject GetUserRoleConnection(int userId)
    {
        RoleNameCollection roleNames = null;

        JObject data = new JObject();
        try
        {
            RoleOpr roleOpr = new RoleOpr();
            roleNames = roleOpr.GetUserRoleConnection(userId);
            roleOpr.Dispose();
            roleOpr = null;

            if (roleNames != null & roleNames.Count > 0)
            {
                data = roleNames.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetRoleNames");
        }


        return data;
    }

    private JObject GetRoleNames()
    {
        RoleNameCollection roleNames = null;
         
        JObject data = new JObject();
        try
        {
            RoleOpr roleOpr = new RoleOpr();
            roleNames = roleOpr.GetRoleNames();
            roleOpr.Dispose();
            roleOpr = null;

            if (roleNames != null & roleNames.Count > 0)
            {
                data = roleNames.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetRoleNames");
        }

         
        return data;
    }

    private JObject GetStatusNames()
    {
        StatusNameCollection statusNames = null;
         
        JObject data = new JObject();
        try
        {
            RoleOpr roleOpr = new RoleOpr();

            statusNames = roleOpr.GetStatusNames();
            roleOpr.Dispose();
            roleOpr = null;

            if (statusNames != null & statusNames.Count > 0)
            {
                data = statusNames.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetStatusNamesUser");
        }

         
        return data;
    }

    public JObject GetJSON(WebUser User)
    {
        JArray jArray = new JArray();
        JObject jObjectDis = new JObject();
        string a = JsonConvert.SerializeObject(User, Newtonsoft.Json.Formatting.None);
        JObject x = JObject.Parse(a);
        jArray.Add(x);
        jObjectDis.Add("Users", jArray);

        return jObjectDis;
    }
}