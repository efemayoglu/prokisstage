﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Backend;
using PRCNCORE.Constants;
using PRCNCORE.Customers;
using PRCNCORE.Input;
using PRCNCORE.Input.Customer;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Services_Customer : System.Web.UI.Page
{
    private CustomerCollection customers;
    private ReturnCodes errorCode = ReturnCodes.SUCCESSFULL;
    JObject data = new JObject();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.HttpMethod == "POST")
        {
            ProcessRequest();
        }
    }

    private void ProcessRequest()
    {
        SwitchInput inputData = null;
        try
        {
            string sData = WebServiceUtils.LoadParameters(Request, InputType.Clear);

            if (!string.IsNullOrEmpty(sData))
            {
                inputData = JsonConvert.DeserializeObject<SwitchInput>(sData);

                if (!WebServiceUtils.CheckCredentials(inputData.ApiKey, inputData.ApiPass))
                {
                    errorCode = ReturnCodes.CREDENTIALS_ERROR;
                }
                else if (Request.Headers["AccessToken"] == null || !WebServiceUtils.CheckAccessTokenSecurity(Request.Headers["AccessToken"], inputData.adminUserId))
                {
                    errorCode = ReturnCodes.INVALID_ACCESS_TOKEN;
                }
                else
                {
                    data = PrepareResponse(inputData.FunctionCode, sData,inputData.adminUserId, inputData.pageUrl);
                    //responseData = WebServiceUtils.AddErrorNodes(responseData, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
                }
            }
            else
            {
                errorCode = ReturnCodes.LOAD_JSON_PARAMETERS_ERROR;

            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ProcessRequestCustomer");
        }
        data = WebServiceUtils.AddErrorNodes(data, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
        WebServiceUtils.SendResponse(data, Response, false);
    }

    private JObject PrepareResponse(int functionCode, string input, long adminUserId, string pageUrl)
    {
        JObject data = new JObject();
        try
        {
            switch ((FC)functionCode)
            {

                case FC.LIST_CUSTOMERS:
                    InputListCustomers listCustomers = JsonConvert.DeserializeObject<InputListCustomers>(input);
                    data = SearchCustomers(listCustomers.SearchText
                        , listCustomers.NumberOfItemsPerPage
                        , listCustomers.CurrentPageNum
                        , listCustomers.RecordCount
                        , listCustomers.PageCount
                        , listCustomers.OrderSelectionColumn
                        , listCustomers.DescAsc
                        , listCustomers.InstutionId);
                    break;
                case FC.LIST_WEB_CUSTOMERS:
                    InputListWebCustomers listWebCustomers = JsonConvert.DeserializeObject<InputListWebCustomers>(input);
                    data = SearchWebCustomers(listWebCustomers.searchNameText
                        , listWebCustomers.searchCustNoText
                        , listWebCustomers.searchTcknText
                        , listWebCustomers.NumberOfItemsPerPage
                        , listWebCustomers.CurrentPageNum
                        , listWebCustomers.RecordCount
                        , listWebCustomers.PageCount
                        , listWebCustomers.TableType);
                    break;
                case FC.EXCEL_LIST_CUSTOMERS:
                    InputListCustomers listCustomersForExcel = JsonConvert.DeserializeObject<InputListCustomers>(input);
                    data = SearchCustomersForExcel(listCustomersForExcel.SearchText
                        , listCustomersForExcel.OrderSelectionColumn
                        , listCustomersForExcel.DescAsc);
                    break;
                case FC.EXCEL_LIST_WEB_CUSTOMERS:
                    InputListWebCustomers listWebCustomersForExcel = JsonConvert.DeserializeObject<InputListWebCustomers>(input);
                    data = SearchWebCustomersForExcel(listWebCustomersForExcel.searchNameText
                        , listWebCustomersForExcel.searchCustNoText
                        , listWebCustomersForExcel.searchTcknText);
                    break;
                case FC.GET_WEB_CUSTOMER_KEY:
                    InputGetSecretKey inputGetSecretKey = JsonConvert.DeserializeObject<InputGetSecretKey>(input);
                    data = GetSecretKey(inputGetSecretKey.customerId, inputGetSecretKey.userId);
                    break;
                case FC.GENERATE_WEB_CUSTOMER_PIN:
                    InputGetSecretKey inputGeneratePin = JsonConvert.DeserializeObject<InputGetSecretKey>(input);
                    data = GenerateCustomerPin(inputGeneratePin.customerId, inputGeneratePin.userId);
                    break;
                case FC.GET_WEB_CUSTOMER_INFO:
                    InputGetSecretKey inputGetCustomerInfo = JsonConvert.DeserializeObject<InputGetSecretKey>(input);
                    data = GetCustomerInfo(inputGetCustomerInfo.customerId, inputGetCustomerInfo.userId);
                    break;
                case FC.UPDATE_WEB_CUSTOMER_INFO:
                    InputUpdateCustomerInfo inputUpdateCustomerInfo = JsonConvert.DeserializeObject<InputUpdateCustomerInfo>(input);
                    data = UpdateCustomerInfo(inputUpdateCustomerInfo.customerId, inputUpdateCustomerInfo.userId, inputUpdateCustomerInfo.email, inputUpdateCustomerInfo.cellphone);
                    break;
                case FC.CLOSE_CUSTOMER_ACCOUNT_FIRST_STEP:
                    CloseCustomerAccountFirstStepInput closeCustomerAccountFirstStepInput = JsonConvert.DeserializeObject<CloseCustomerAccountFirstStepInput>(input);
                    data = CloseCustomerAccountFirstStep(closeCustomerAccountFirstStepInput.itemId, closeCustomerAccountFirstStepInput.userId);
                    break;

                case FC.GET_CUSTOMER_EMAIL:
                    InputCustomerEmailAdress2 inputCustomerEmailAdress = JsonConvert.DeserializeObject<InputCustomerEmailAdress2>(input);
                    data = GetCustomerEmails(inputCustomerEmailAdress.CustomerId);
                    break;

                case FC.SEND_CUSTOMER_EMAIL:               

                    InputCustomerEmailAdress3 inputCustomerEmailAdress3 = JsonConvert.DeserializeObject<InputCustomerEmailAdress3>(input);
                    data = SendCustomerEmails(inputCustomerEmailAdress3.EmailAddress, inputCustomerEmailAdress3.TransactionId);
                    break;


                case FC.UPDATE_CUSTOMER_EMAIL_STATUS:

                    InputCustomerEmailAdressStatus inputCustomerEmailAdressStatus = JsonConvert.DeserializeObject<InputCustomerEmailAdressStatus>(input);
                    data = UpdateCustomerEmailStatus(inputCustomerEmailAdressStatus.Status, inputCustomerEmailAdressStatus.AboneNo);
                    break;

                //case FC.CLOSE_CUSTOMER_ACCOUNT_LAST_STEP:
                //    CloseCustomerAccountLastStepInput closeCustomerAccountLastStepInput = JsonConvert.DeserializeObject<CloseCustomerAccountLastStepInput>(input);
                //    data = UpdateCustomerInfo(inputUpdateCustomerInfo.customerId, inputUpdateCustomerInfo.userId, inputUpdateCustomerInfo.email, inputUpdateCustomerInfo.cellphone);
                //    break;
                //case FC.REOPEN_CUSTOMER_ACCOUNT_FIRST_STEP:
                //    ReopenCustomerAccountFirstStep reopenCustomerAccountFirstStep = JsonConvert.DeserializeObject<ReopenCustomerAccountFirstStep>(input);
                //    data = UpdateCustomerInfo(inputUpdateCustomerInfo.customerId, inputUpdateCustomerInfo.userId, inputUpdateCustomerInfo.email, inputUpdateCustomerInfo.cellphone);
                //    break;
                //case FC.REOPEN_CUSTOMER_ACCOUNT_LAST_STEP:
                //    ReopenCustomerAccountLastStep reopenCustomerAccountLastStep = JsonConvert.DeserializeObject<ReopenCustomerAccountLastStep>(input);
                //    data = UpdateCustomerInfo(inputUpdateCustomerInfo.customerId, inputUpdateCustomerInfo.userId, inputUpdateCustomerInfo.email, inputUpdateCustomerInfo.cellphone);
                //    break;
                default:
                    break;
            }
            
            ManageContextRolescs contextRoles = new ManageContextRolescs();
            contextRoles.EditByRoles(functionCode, data, adminUserId, pageUrl);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "PrepareResponseCustomer");
        }
        return data;
    }


    private JObject SearchCustomers(string searchText
                                , int numberOfItemsPerPage
                                , int currentPageNum
                                , int recordCount
                                , int pageCount
                                , int orderSelectionColumn
                                , int descAsc
                                , int instutionId)
    {
        JObject data = new JObject();

        try
        {

            int recordCnt = recordCount;
            int pageCnt = pageCount;

            CustomerOpr customerOpr = new CustomerOpr();

            this.customers = customerOpr.SearchCustomersOrder(searchText, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, instutionId);
            customerOpr.Dispose();
            customerOpr = null;

            if (this.customers != null & this.customers.Count > 0)
            {
                data = this.customers.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchCustomers");
        }
        return data;
    }

    private JObject GetSecretKey(int customerId, int userId)
    {
        JObject data = new JObject();

        try
        {
            string secretKey = "";

            CustomerOpr customerOpr = new CustomerOpr();

            customerOpr.GetSecretKey(customerId, userId, ref secretKey);
            customerOpr.Dispose();
            customerOpr = null;

            if (!String.IsNullOrEmpty(secretKey))
            {
                data.Add("secretKey", secretKey);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetSecretKey");
        }
        return data;
    }

    private JObject GetCustomerInfo(int customerId, int userId)
    {
        JObject data = new JObject();

        try
        {
            string email = "";
            string cellphone = "";
            string name = "";

            CustomerOpr customerOpr = new CustomerOpr();

            customerOpr.GetCustomerInfoChange(customerId, userId, ref email, ref cellphone, ref name);
            customerOpr.Dispose();
            customerOpr = null;

            if (!String.IsNullOrEmpty(email))
            {
                data.Add("email", email);
                data.Add("cellphone", cellphone);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetCustomerInfo");
        }
        return data;
    }

    private JObject UpdateCustomerInfo(int customerId, int userId, string email, string cellphone)
    {
        JObject data = new JObject();

        try
        {
            CustomerOpr customerOpr = new CustomerOpr();

            customerOpr.UpdateCustomerInfo(ref customerId, userId, email, cellphone);
            customerOpr.Dispose();
            customerOpr = null;

            if (customerId == 1)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UpdateCustomerInfo");
        }
        return data;
    }

    private JObject GenerateCustomerPin(int customerId, int userId)
    {
        JObject data = new JObject();

        try
        {

            string cellPhone = "";
            string email = "";
            string name = "";

            string secretKey = "";

            CustomerOpr customerOpr = new CustomerOpr();

            customerOpr.GetCustomerInfo(customerId, userId, ref cellPhone, ref email, ref name);
            string[] recievers = { email };
            string newPin = Utility.generateRandomNum(111111, 999999);

            int operationReult = 0;
            customerOpr.GenerateCustomerPin(customerId, userId, Utility.CalculateSHA512Pin(newPin), ref operationReult);
            customerOpr.Dispose();
            customerOpr = null;

            string webServiceUrl = Utility.GetConfigValue("SMSWebServiceUrl");
            Utility.SendSMS(webServiceUrl, "Sn. " + name + ", Pratik Cüzdan sistemine giriş yapabilmeniz için yeni şifreniz:  " + newPin + " \r\nİyi Günler Dileriz.", cellPhone);
            string[] d = { email };
            Utility.SendEmail("info@birlesikodeme.com", "Pratik Nokta", d, "Bilgilendirme", "Sn. " + name + ", Pratik Cüzdan sistemine giriş yapabilmeniz için yeni şifreniz:  " + newPin + " </br>İyi Günler Dileriz.</br>", null);


            if (operationReult == 1)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GenerateCustomerPin");
        }
        return data;
    }

    private JObject SearchWebCustomers(string searchNameText
                                     , string searchCustNoText
                                     , string searchcknText
                                     , int numberOfItemsPerPage
                                     , int currentPageNum
                                     , int recordCount
                                     , int pageCount
                                     , int tableType)
    {
        JObject data = new JObject();

        try
        {

            int recordCnt = recordCount;
            int pageCnt = pageCount;

            CustomerOpr customerOpr = new CustomerOpr();

            WebCustomerCollection customers = customerOpr.SearchWebCustomersOrder(searchNameText, searchCustNoText, searchcknText, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, tableType);
            customerOpr.Dispose();
            customerOpr = null;

            if (customers != null & customers.Count > 0)
            {
                data = customers.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchCustomers");
        }
        return data;
    }

    private JObject SearchCustomersForExcel(string searchText
                               , int orderSelectionColumn
                               , int descAsc)
    {
        JObject data = new JObject();

        try
        {
            CustomerOpr customerOpr = new CustomerOpr();

            this.customers = customerOpr.SearchCustomersOrderForExcel(searchText, orderSelectionColumn, descAsc);
            customerOpr.Dispose();
            customerOpr = null;

            if (this.customers != null & this.customers.Count > 0)
            {
                data = this.customers.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchCustomersForExcel");
        }
        return data;
    }

    private JObject SearchWebCustomersForExcel(string searchNameText
                                     , string searchCustNoText
                                     , string searchTcknText)
    {
        JObject data = new JObject();

        try
        {
            CustomerOpr customerOpr = new CustomerOpr();

            WebCustomerCollection customers = customerOpr.SearchWebCustomersOrderForExcel(searchNameText, searchCustNoText, searchTcknText);
            customerOpr.Dispose();
            customerOpr = null;

            if (customers != null & customers.Count > 0)
            {
                data = customers.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchCustomersForExcel");
        }
        return data;
    }

    public JObject GetJSON(Customer theCustomer)
    {
        JObject jObjectDis = new JObject();

        string a = JsonConvert.SerializeObject(theCustomer, Newtonsoft.Json.Formatting.None);

        JObject x = JObject.Parse(a);


        jObjectDis.Add("Customer", x);

        return jObjectDis;
    }

    private JObject CloseCustomerAccountFirstStep(Int64 customerId, Int64 userId)
    {
        JObject data = new JObject();

        try
        {
            int returnCode = 0;
            CustomerOpr customerOpr = new CustomerOpr();

            customerOpr.CloseCustomerAccountFirstStep(customerId, userId, out returnCode);
            customerOpr.Dispose();
            customerOpr = null;

            if (returnCode == 1)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UpdateCustomerInfo");
        }
        return data;
    }


    /*
     
       
        public Kiosk GetKiosk(int kioskId)
        {
            Kiosk kiosk = null;
            DataSet dataSet = null;
            try
            {

                SqlParameter[] sqlParameters = new SqlParameter[] { new SqlParameter("@kioskId", kioskId) };

                dataSet = SqlHelper.ExecuteDataset(base.GetConnection(), CommandType.StoredProcedure, "pk.get_kiosk", Convert.ToInt32(Utility.GetConfigValue("TIMEOUT")), sqlParameters);

                dataSet.Tables[0].Columns.Add("ClassName", typeof(string));

                if (dataSet.Tables[0].Rows.Count == 1)
                {
                    kiosk = this.GetKiosk(dataSet.Tables[0].Rows[0], dataSet.Tables[0].Columns);
                }

            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "COREGetKiosk");

            }
            finally
            {
                base.Dispose();

                if (dataSet != null)
                {
                    dataSet.Dispose();
                    dataSet = null;
                }
            }

            return kiosk;
        }  
         
         
         
         
         
         
         
         
         
         
         
         */




    private JObject UpdateCustomerEmailStatus(string status, string aboneNo)
    {

        JObject data = new JObject();
        try
        {
            CustomerOpr csOpr = new CustomerOpr();
            var response = csOpr.UpdateCustomerEmailStatus(status, aboneNo);

            if (response.errorCode ==0)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetCustomerEmails");
        }
        return data;
    }

    private JObject SendCustomerEmails(string email, long transactionId)
    {

        JObject data = new JObject();
        try
        { 
            EmailBackend emailBackend = new EmailBackend();
            var response = emailBackend.SendEmail(email, transactionId);
            
            if (response != 2 )
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
 
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetCustomerEmails");
        }
        return data;
    }



    private JObject GetCustomerEmails(long customerId)
    {

        JObject data = new JObject();
        try
        {

            CustomerOpr customerOpr = new CustomerOpr();

            var emails = customerOpr.GetCustomerEmail(customerId);


            if (emails != null & emails.Count > 0)
            {
                data = GetJSON(emails);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
            customerOpr.Dispose();
            customerOpr = null;

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetCustomerEmails");
        }
        return data;
    }


    

    public JObject GetJSON(List<InputCustomerEmailAdress> emails)
    {
        JObject jObjectDis = new JObject();
        JArray jArray = new JArray();

        foreach (InputCustomerEmailAdress item in emails)
        {
            JObject jo = new JObject();

            string a = JsonConvert.SerializeObject(item, Newtonsoft.Json.Formatting.None);

            JObject x = JObject.Parse(a);
            jArray.Add(x);
        }

        jObjectDis.Add("CustomersEmails", jArray);

        return jObjectDis;
    }


    private JObject CloseCustomerAccountLastStep(Int64 customerId, Int64 userId)
    {
        JObject data = new JObject();

        try
        {
            int returnCode = 0;
            CustomerOpr customerOpr = new CustomerOpr();

            customerOpr.CloseCustomerAccountFirstStep(customerId, userId, out returnCode);
            customerOpr.Dispose();
            customerOpr = null;

            if (returnCode == 1)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UpdateCustomerInfo");
        }
        return data;
    }

    private JObject ReOpenCustomerAccountFirstStep(Int64 customerId, Int64 userId)
    {
        JObject data = new JObject();

        try
        {
            int returnCode = 0;
            CustomerOpr customerOpr = new CustomerOpr();

            customerOpr.CloseCustomerAccountFirstStep(customerId, userId, out returnCode);
            customerOpr.Dispose();
            customerOpr = null;

            if (returnCode == 1)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UpdateCustomerInfo");
        }
        return data;
    }

    private JObject ReOpenCustomerAccountLastStep(Int64 customerId, Int64 userId)
    {
        JObject data = new JObject();

        try
        {
            int returnCode = 0;
            CustomerOpr customerOpr = new CustomerOpr();

            customerOpr.CloseCustomerAccountFirstStep(customerId, userId, out returnCode);
            customerOpr.Dispose();
            customerOpr = null;

            if (returnCode == 1)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UpdateCustomerInfo");
        }
        return data;
    }
}