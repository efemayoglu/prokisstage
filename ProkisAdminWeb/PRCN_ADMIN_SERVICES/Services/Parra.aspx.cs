﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Accounts;
using PRCNCORE.Backend;
using PRCNCORE.Constants;
using PRCNCORE.Input;
using PRCNCORE.Input.Account;
using PRCNCORE.Utilities;
using System;
//using TeskiTahsilatServiceReference;

public partial class Services_Account : System.Web.UI.Page
{
    private ReturnCodes errorCode = ReturnCodes.SUCCESSFULL;
    JObject data = new JObject();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.HttpMethod == "POST")
        {
            ProcessRequest();
        }
    }

    private void ProcessRequest()
    {
        SwitchInput inputData = null;
        try
        {
            string sData = WebServiceUtils.LoadParameters(Request, InputType.Clear);

            if (!string.IsNullOrEmpty(sData))
            {
                inputData = JsonConvert.DeserializeObject<SwitchInput>(sData);

                if (!WebServiceUtils.CheckCredentials(inputData.ApiKey, inputData.ApiPass))
                {
                    errorCode = ReturnCodes.CREDENTIALS_ERROR;
                }
                else if (Request.Headers["AccessToken"] == null || !WebServiceUtils.CheckAccessTokenSecurity(Request.Headers["AccessToken"], inputData.adminUserId))
                {
                    errorCode = ReturnCodes.INVALID_ACCESS_TOKEN;
                }
                else
                {
                    data = PrepareResponse(inputData.FunctionCode, sData, inputData.adminUserId, inputData.pageUrl);
                }
            }
            else
            {
                errorCode = ReturnCodes.LOAD_JSON_PARAMETERS_ERROR;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ProcessRequestAccount");
        }
        data = WebServiceUtils.AddErrorNodes(data, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
        WebServiceUtils.SendResponse(data, Response, false);
    }

    
   private ParraApiClient _client = null;
    private JObject PrepareResponse(int functionCode, string input ,long adminUserId, string pageUrl)
    {
        JObject data = new JObject();
        try
        {
            switch ((FC)functionCode)
            {//update_kiosk_empty_kioskEmptyStatusId
                case FC.PARRA_GET_BALANCE:

                    //HttpClientHandler httpClientHandler = new HttpClientHandler
                    //{ ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true };
                    //HttpClient httpClient = new HttpClient(httpClientHandler);
                    _client = new ParraApiClient("http://172.16.81.10:8070");
                  var resp =  _client.GetBalanceAsync(new GetBalanceRequest
                    {
                        CardId = 11776,
                        CustomerId= "zk7g7b6o7pr8dp18fn9zxb6ffa",
                        SupplierId =3
                    });

                    break;

                default:
                    break;
            }

            ManageContextRolescs contextRoles = new ManageContextRolescs();
            contextRoles.EditByRoles(functionCode, data, adminUserId, pageUrl);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "PrepareResponseAccount");
        }

        return data;
    }


      


    //SearchKioskCashBoxCashCountError InputListKioskDispenserCashCounts
    private JObject SearchKioskCashBoxCashCountError(InputListKioskDispenserCashCounts listKioskCashBoxCashCountError)
    {

        JObject data = new JObject();

        try
        {
            int recordCount = 0;
            int pageCount = 0;

            AccountOpr opr = new AccountOpr();

            KioskCashBoxCashCountErrorCollection cashCounts = opr.SearchKioskCashBoxCashCountErrorOrder(listKioskCashBoxCashCountError.KioskId,
                                                                                                  listKioskCashBoxCashCountError.SearchText,
                                                                                                  listKioskCashBoxCashCountError.StartDate,
                                                                                                  listKioskCashBoxCashCountError.EndDate,
                                                                                                  listKioskCashBoxCashCountError.InstutionId,
                                                                                                  listKioskCashBoxCashCountError.NumberOfItemsPerPage,
                                                                                                  listKioskCashBoxCashCountError.CurrentPageNum,
                                                                                                  ref recordCount,
                                                                                                  ref pageCount,
                                                                                                  listKioskCashBoxCashCountError.OrderSelectionColumn,
                                                                                                  listKioskCashBoxCashCountError.DescAsc,
                                                                                                  listKioskCashBoxCashCountError.AdminUserId,
                                                                                                  listKioskCashBoxCashCountError.OrderType);

            opr.Dispose();
            opr = null;

            if (cashCounts != null & cashCounts.Count > 0)
            {
                data = cashCounts.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskCashBoxCashCountError");
        }
        return data;
    }
}





//----------------------
// <auto-generated>
//     Generated using the NSwag toolchain v13.1.2.0 (NJsonSchema v10.0.24.0 (Newtonsoft.Json v11.0.0.0)) (http://NSwag.org)
// </auto-generated>
//----------------------
 