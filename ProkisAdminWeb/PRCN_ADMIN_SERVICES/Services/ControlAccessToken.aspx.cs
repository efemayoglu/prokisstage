﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Constants;
using PRCNCORE.Input;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Services_ControlAccessToken : System.Web.UI.Page
{
    private ReturnCodes errorCode = ReturnCodes.SUCCESSFULL;
    ControlAccessTokenInput inputData = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.HttpMethod == "POST")
        {
            ProcessRequest();
        }
    }

    private void ProcessRequest()
    {
        string data = WebServiceUtils.LoadParameters(Request, InputType.Clear);

        JObject responseData = PrepareResponse(data);
        WebServiceUtils.SendResponse(responseData, Response, false);
    }

    private JObject PrepareResponse(string input)
    {
        ReturnCodes errorCode = ReturnCodes.SUCCESSFULL;
        JObject data = new JObject();

        try
        {
            if (string.IsNullOrEmpty(input))
            {
                errorCode = ReturnCodes.LOAD_JSON_PARAMETERS_ERROR;
                data = WebServiceUtils.AddErrorNodes(data, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
                return data;
            }

            inputData = JsonConvert.DeserializeObject<ControlAccessTokenInput>(input);

            if (!WebServiceUtils.CheckCredentials(inputData.ApiKey, inputData.ApiPass))
            {
                errorCode = ReturnCodes.CREDENTIALS_ERROR;
                data = WebServiceUtils.AddErrorNodes(data, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
                return data;
            }

            if (Request.Headers["AccessToken"] == null || !WebServiceUtils.CheckExpiredAccessTokenSecurity(Request.Headers["AccessToken"], inputData.adminUserId))
            {
                errorCode = ReturnCodes.INVALID_ACCESS_TOKEN;
                data = WebServiceUtils.AddErrorNodes(data, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
                return data;
            }

            
            if(inputData.Retokenize=="1")
            {
                data.Add("RetokenizeResult", WebServiceUtils.CheckAccessTokenSecurity(Request.Headers["AccessToken"], inputData.adminUserId));
            }

            errorCode = ReturnCodes.SUCCESSFULL;
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ProcessRequestCustomer");
        }

        data = WebServiceUtils.AddErrorNodes(data, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
        return data;
    }
}