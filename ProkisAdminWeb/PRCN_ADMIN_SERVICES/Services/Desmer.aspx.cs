﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.UI;
using DesmerProd;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Backend;
using PRCNCORE.Constants;
using PRCNCORE.Desmer;
using PRCNCORE.Input;
using PRCNCORE.Utilities;

public partial class Services_Desmer : Page
{
    private JObject _data = new JObject();
    private ReturnCodes _errorCode = ReturnCodes.SUCCESSFULL;
    private static Users credentials;

    protected void Page_Load(object sender, EventArgs e)
    {
        credentials = new Users
        {
            USERID = Convert.ToInt32(Utility.DecryptString(Utility.GetConfigValue("DesmerUserId"))),
            MUSTERIID = Convert.ToInt32(Utility.DecryptString(Utility.GetConfigValue("DesmerMusteriId"))),
            UserName = Utility.DecryptString(Utility.GetConfigValue("DesmerUser")),
            PassWord = Utility.DecryptString(Utility.GetConfigValue("DesmerPass"))
        };

        if (Request.HttpMethod == "POST")
        {
            ProcessRequest();
        }
    }

    private void ProcessRequest()
    {
        SwitchInput inputData = null;
        try
        {
            var sData = WebServiceUtils.LoadParameters(Request, InputType.Clear);

            if (!string.IsNullOrEmpty(sData))
            {
                inputData = JsonConvert.DeserializeObject<SwitchInput>(sData);

                if (!WebServiceUtils.CheckCredentials(inputData.ApiKey, inputData.ApiPass))
                    _errorCode = ReturnCodes.CREDENTIALS_ERROR;
                else if (Request.Headers["AccessToken"] == null ||
                         !WebServiceUtils.CheckAccessTokenSecurity(Request.Headers["AccessToken"],
                             inputData.adminUserId))
                    _errorCode = ReturnCodes.INVALID_ACCESS_TOKEN;
                else
                    _data = PrepareResponse(inputData.FunctionCode, sData, inputData.adminUserId, inputData.pageUrl);
            }
            else
            {
                _errorCode = ReturnCodes.LOAD_JSON_PARAMETERS_ERROR;
            }
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ProcessRequestAccount");
        }

        _data = WebServiceUtils.AddErrorNodes(_data, _errorCode, WebServiceUtils.GetReturnCodeDescription(_errorCode));
        WebServiceUtils.SendResponse(_data, Response, false);
    }

    private JObject PrepareResponse(int functionCode, string input, long adminUserId, string pageUrl)
    {
        var data = new JObject();
        try
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;

            switch ((FC) functionCode)
            {
                case FC.DESMERBIRIMLER:
                    data = GetDesmerBirimler();
                    break;
                case FC.NOKTAGRUPLAR:
                    data = GetNoktaGruplar();
                    break;
                case FC.NOKTA:
                    var inputNokta = JsonConvert.DeserializeObject<NoktaInput>(input);
                    data = SetNokta(
                        inputNokta.IslemTipi,
                        inputNokta.Kod,
                        inputNokta.Adı,
                        inputNokta.GrupKod,
                        inputNokta.BirimKod,
                        inputNokta.Adres,
                        inputNokta.Telefon,
                        inputNokta.Durum
                    );
                    break;
                case FC.NOKTALAR:
                    var inputNoktalar = JsonConvert.DeserializeObject<NoktaInput>(input);
                    data = GetNoktalar(
                        inputNoktalar.IlKod
                    );
                    break;
                case FC.PARABIRIMLER:
                    data = GetParaBirimler();
                    break;
                case FC.TALEPDURUMLAR:
                    data = GetTalepDurumlar();
                    break;
                case FC.TALEPTIPLER:
                    data = GetTalepTipler();
                    break;
                case FC.ARIZATIPLER:
                    data = GetArizaTipler();
                    break;
                case FC.TALEPSORGULA:
                    var inputTalepSorgula = JsonConvert.DeserializeObject<TalepSorguInput>(input);
                    data = GetTalepSorgu(
                        inputTalepSorgula.ReferansId
                    );
                    break;
                case FC.TALEP:
                    var inputTalep = JsonConvert.DeserializeObject<TalepSorguInput>(input);
                    data = SetTalep(
                        inputTalep.IslemTipi,
                        inputTalep.ReferansId,
                        inputTalep.HizmetNoktaKod,
                        inputTalep.NomKod,
                        inputTalep.Tutar,
                        inputTalep.TutarParaBirimi,
                        inputTalep.TalepTipi,
                        inputTalep.ArizaTipi,
                        inputTalep.TalepTarih,
                        inputTalep.Aciklama,
                        inputTalep.Kupurler,
                        inputTalep.Barkodlar
                    );
                    break;
                case FC.ADDTALEPBOSALTMA:
                    var inputTalepBosaltma = JsonConvert.DeserializeObject<AddTalepBosaltmaInput>(input);
                    data = AddTalepBosaltma(inputTalepBosaltma.TalepReferenceInputs);
                    break;
                case FC.ADDTALEPDOLDURMA:
                    var inputTalepDoldurma = JsonConvert.DeserializeObject<AddTalepDoldurmaInput>(input);
                    data = AddTalepDoldurma(
                        inputTalepDoldurma.HizmetNoktaKod,
                        inputTalepDoldurma.NomKod,
                        inputTalepDoldurma.Tutar,
                        inputTalepDoldurma.TutarParaBirimi,
                        inputTalepDoldurma.TalepTipi,
                        inputTalepDoldurma.ArizaTipi,
                        inputTalepDoldurma.TalepTarih,
                        inputTalepDoldurma.Aciklama,
                        inputTalepDoldurma.Kupurler,
                        inputTalepDoldurma.Barkodlar,
                        inputTalepDoldurma.UserId);
                    break;
                case FC.ADDTALEPOTHER:
                    var inputTalepOther = JsonConvert.DeserializeObject<AddTalepDoldurmaInput>(input);
                    data = AddTalepOther(
                        inputTalepOther.HizmetNoktaKod,
                        inputTalepOther.NomKod,
                        inputTalepOther.TalepTipi,
                        inputTalepOther.ArizaTipi,
                        inputTalepOther.TalepTarih,
                        inputTalepOther.Aciklama);
                    break;
                case FC.EDITTALEPBOSALTMA:
                    var inputTalepBosaltmaEdit = JsonConvert.DeserializeObject<TalepBosaltmaSingle>(input);
                    data = EditTalepBosaltma(inputTalepBosaltmaEdit);
                    break;
                case FC.EDITTALEPDOLDURMA:
                    var inputTalepDoldurmaEdit = JsonConvert.DeserializeObject<AddTalepDoldurmaInput>(input);
                    data = EditTalepDoldurma(
                        inputTalepDoldurmaEdit.ReferansId,
                        inputTalepDoldurmaEdit.HizmetNoktaKod,
                        inputTalepDoldurmaEdit.NomKod,
                        inputTalepDoldurmaEdit.Tutar,
                        inputTalepDoldurmaEdit.TutarParaBirimi,
                        inputTalepDoldurmaEdit.TalepTipi,
                        inputTalepDoldurmaEdit.ArizaTipi,
                        inputTalepDoldurmaEdit.TalepTarih,
                        inputTalepDoldurmaEdit.Aciklama,
                        inputTalepDoldurmaEdit.Kupurler,
                        inputTalepDoldurmaEdit.Barkodlar,
                        inputTalepDoldurmaEdit.UserId);
                    break;
                case FC.EDITTALEPOTHER:
                    var inputTalepOtherEdit = JsonConvert.DeserializeObject<AddTalepDoldurmaInput>(input);
                    data = EditTalepOther(
                        inputTalepOtherEdit.ReferansId,
                        inputTalepOtherEdit.HizmetNoktaKod,
                        inputTalepOtherEdit.NomKod,
                        inputTalepOtherEdit.TalepTipi,
                        inputTalepOtherEdit.ArizaTipi,
                        inputTalepOtherEdit.TalepTarih,
                        inputTalepOtherEdit.Aciklama);
                    break;
                case FC.UPDATETALEP:
                    var inputUpdateTalep = JsonConvert.DeserializeObject<Talep>(input);
                    data = UpdateTalep(inputUpdateTalep.ReferansId);
                    break;
                case FC.CANCELTALEP:
                    var inputCancelTalep = JsonConvert.DeserializeObject<Talep>(input);
                    data = EditTalepIslemType(inputCancelTalep.ReferansId, ISLEMTIPI.Iptal);
                    break;
                case FC.READYTALEP:
                    var inputReadyTalep = JsonConvert.DeserializeObject<Talep>(input);
                    data = EditTalepIslemType(inputReadyTalep.ReferansId, ISLEMTIPI.Hazir);
                    break;

                case FC.TALEPLER:
                    var inputTaleps = JsonConvert.DeserializeObject<TaleplerInput>(input);
                    data = GetTaleps(
                        inputTaleps.StartDate,
                        inputTaleps.EndDate,
                        inputTaleps.IslemTipi,
                        inputTaleps.TalepDurum,
                        inputTaleps.TalepTipi,
                        inputTaleps.ReferansId,
                        inputTaleps.IlKod,
                        inputTaleps.KioskList);
                    break;
                default:
                    break;
            }

            ManageContextRolescs contextRoles = new ManageContextRolescs();
            contextRoles.EditByRoles(functionCode, data, adminUserId, pageUrl);
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "PrepareResponseDesmer");
        }

        return data;
    }

    private JObject GetDesmerBirimler()
    {
        var json = new JObject();

        try
        {
            string errorMessage;
            var birimList = new List<Birim>();

            using (var desmer = new CITSoapClient())
            {
                var birimArrayRaw = desmer.DESMERBIRIMLER(credentials);

                errorMessage = birimArrayRaw.RESULT.MESSAGE;


                var birimArray = Array.ConvertAll(
                    birimArrayRaw.BIRIMLIST,
                    s => new Birim
                    {
                        Kod = s.KOD,
                        Adı = s.ADI,
                        Il = s.IL,
                        IlKod = s.ILKOD
                    });

                birimList = new List<Birim>(birimArray);
            }
            //var birimList = GetDesmerBirimlerRaw(out errorMessage);

            json["birimList"] = JToken.FromObject(birimList);
            json["errorMessage"] = errorMessage;
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetDesmerBirimler");
        }

        return json;
    }

    private JObject GetNoktaGruplar()
    {
        var json = new JObject();

        try
        {
            using (var desmer = new CITSoapClient())
            {
                var noktaGrupArrayRaw = desmer.NOKTAGRUPLAR(credentials);

                var noktaGrupArray = Array.ConvertAll(
                    noktaGrupArrayRaw.GRUPLIST,
                    s => new Grup
                    {
                        Kod = s.KOD,
                        Adı = s.ADI
                    });

                var noktaGrupList = new List<Grup>(noktaGrupArray);


                json["grupList"] = JToken.FromObject(noktaGrupList);
                json["errorMessage"] = noktaGrupArrayRaw.RESULT.MESSAGE;
            }
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetNoktaGruplar");
        }

        return json;
    }

    private JObject SetNokta(
        int islemTipi,
        string kod,
        string adi,
        string grupKod,
        string birimKod,
        string adres,
        string telefon,
        bool durum)
    {
        var json = new JObject();

        try
        {
            using (var desmer = new CITSoapClient())
            {
                var nokta = new NOKTA
                {
                    ISLEMTIPI = (ISLEMTIP) islemTipi,
                    KOD = kod,
                    ADI = adi,
                    GRUPKOD = grupKod,
                    BIRIMKOD = birimKod,
                    ADRES = adres,
                    TELEFON = telefon,
                    DURUM = durum
                };

                var NoktaRaw = desmer.NOKTA(credentials, nokta);

                json["errorMessage"] = NoktaRaw.RESULT.MESSAGE;
                if (NoktaRaw.RESULT.MESSAGE.StartsWith("E0")) _errorCode = ReturnCodes.SYSTEM_ERROR;
            }
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SetNokta");
        }

        return json;
    }

    private JObject GetNoktalar(string ilKod)
    {
        var json = new JObject();
        try
        {
            string errorMessage;
            var noktaList = GetNoktalarRaw(ilKod, out errorMessage);

            json["noktaList"] = JToken.FromObject(noktaList);
            json["errorMessage"] = errorMessage;
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetNoktalar");
        }

        return json;
    }

    private static List<Nokta> GetNoktalarRaw(string ilKod, out string errorMessage)
    {
        errorMessage = "";
        var noktaList = new List<Nokta>();
        using (var desmer = new CITSoapClient())
        {
            var nokta = new NOKTALAR {ILKOD = ilKod};

            var NoktaArrayRaw = desmer.NOKTALAR(credentials, nokta);

            var NoktaGrupArrayRaw = desmer.NOKTAGRUPLAR(credentials);

            var CityOperations = new CityOpr();
            var Cities = CityOperations.GetCities();

            //var dbg1 = NoktaGrupArrayRaw.GRUPLIST.FirstOrDefault(o => o.KOD == "4");
            //var dbg2 = Cities.FirstOrDefault(o => o.Id.ToString() == "5");

            var noktaArray = Array.ConvertAll(
                NoktaArrayRaw.NOKTALARLIST,
                s => new Nokta
                {
                    Kod = s.KOD,
                    Adı = s.ADI,
                    GrupKod = s.GRUP,
                    GrupName = NoktaGrupArrayRaw.GRUPLIST.FirstOrDefault(o => o.KOD == s.GRUP).ADI,
                    IlKod = s.ILKOD,
                    IlName = Cities.FirstOrDefault(o => o.Id.ToString() == s.ILKOD).Name,
                    Adres = s.ADRES,
                    Telefon = s.TELEFON,
                    Durum = s.DURUM
                });

            noktaList = new List<Nokta>(noktaArray);

            if (NoktaArrayRaw.RESULT != null)
                errorMessage = NoktaArrayRaw.RESULT.MESSAGE;
            else
                errorMessage = "BOS Noktalar alınamadı";
        }

        return noktaList;
    }

    private JObject GetParaBirimler()
    {
        var json = new JObject();
        try
        {
            using (var desmer = new CITSoapClient())
            {
                var paraArrayRaw = desmer.PARABIRIMLER(credentials);

                var paraArray = Array.ConvertAll(
                    paraArrayRaw.PARALIST,
                    s => new ParaBirim
                    {
                        Kod = s.KOD,
                        Deger = s.DEGER,
                        Tur = s.CINS
                    });

                var paraList = new List<ParaBirim>(paraArray);


                json["paraList"] = JToken.FromObject(paraList);
                json["errorMessage"] = paraArrayRaw.RESULT.MESSAGE;
            }
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetParaBirimler");
        }

        return json;
    }

    private JObject GetTalepDurumlar()
    {
        var json = new JObject();
        string errorMessage = "";
        var talepDurumList = new List<TalepDurum>();
        try
        {
            talepDurumList = GetTalepDurumlarRaw(out errorMessage);

            json["talepDurumlist"] = JToken.FromObject(talepDurumList);
            json["errorMessage"] = errorMessage;
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetTalepDurumlar");
        }

        return json;
    }

    private static List<TalepDurum> GetTalepDurumlarRaw(out string errorMessage)
    {
        using (var desmer = new CITSoapClient())
        {
            var talepDurumArrayRaw = desmer.TALEPDURUMLAR(credentials);

            var talepDurumArray = Array.ConvertAll(
                talepDurumArrayRaw.DURUMLIST,
                s => new TalepDurum
                {
                    Kod = s.KOD,
                    Adı = s.ADI
                });

            var talepDurumList = new List<TalepDurum>(talepDurumArray);

            errorMessage = talepDurumArrayRaw.RESULT.MESSAGE;
            return talepDurumList;
        }
    }

    private JObject GetTalepTipler()
    {
        var json = new JObject();
        string errorMessage = "";
        try
        {
            var talepTipList = GetTalepTiplerRaw(out errorMessage);

            json["talepTipList"] = JToken.FromObject(talepTipList);
            json["errorMessage"] = errorMessage;
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetTalepTipler");
        }

        return json;
    }

    private static List<TalepTipi> GetTalepTiplerRaw(out string errorMessage)
    {
        var talepTipList = new List<TalepTipi>();
        using (var desmer = new CITSoapClient())
        {
            var talepTipArrayRaw = desmer.TALEPTIPLER(credentials);

            var talepTipArray = Array.ConvertAll(
                talepTipArrayRaw.TALEPTIPLERI,
                s => new TalepTipi
                {
                    Kod = s.KOD,
                    Adı = s.ADI
                });

            talepTipList = new List<TalepTipi>(talepTipArray);

            errorMessage = talepTipArrayRaw.RESULT.MESSAGE;
        }

        return talepTipList;
    }

    private JObject GetArizaTipler()
    {
        var json = new JObject();

        try
        {
            using (var desmer = new CITSoapClient())
            {
                var arizaTipArrayRaw = desmer.ARIZATIPLERI(credentials);

                var arizaTipArray = Array.ConvertAll(
                    arizaTipArrayRaw,
                    s => new TalepTipi
                    {
                        Kod = s.KOD,
                        Adı = s.ADI
                    });

                var arizaTipList = new List<TalepTipi>(arizaTipArray);


                json["arizaTiplist"] = JToken.FromObject(arizaTipList);
                json["errorMessage"] = "";
            }
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetArizaTipler");
        }

        return json;
    }

    private JObject GetTalepSorgu(string referansId)
    {
        var json = new JObject();

        try
        {
            string errorMessage;
            var talep = GetTalepSorguRaw(referansId, out errorMessage);


            var tarih = (DateTime) talep.TalepTarih;

            using (var desmer = new DesmerOpr())
            {
                var databaseTalep = desmer
                    .GetTaleps(tarih.AddDays(-1), tarih.AddDays(1)) // tarih.adddays exceptiona düşürüyor.
                    .FirstOrDefault(x => x.ReferansId == referansId);
                talep.FulfilId = databaseTalep.FulfilId;

                //json["fulfilId"] = fulfilId.FulfilId;
            }


            json["talepSorgu"] = JToken.FromObject(talep);
            json["errorMessage"] = errorMessage;
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetTalepSorgu");
        }

        return json;
    }

    private static Talep GetTalepSorguRaw(string referansId, out string errorMessage)
    {
        using (var desmer = new CITSoapClient())
        {
            var referenceId = new TALEP {REFERANSID = referansId};

            var TalepRaw = desmer.TALEPSORGULA(credentials, referenceId);
            errorMessage = TalepRaw.RESULT.MESSAGE;

            var kupurArray = Array.ConvertAll(
                TalepRaw.KUPURLER,
                s => new Kupur
                {
                    KupurKod = s.KUPURKOD,
                    Adet = s.ADET
                });
            var barkodArray = Array.ConvertAll(
                TalepRaw.BARKODLAR,
                s => new Barkod
                {
                    Id = s.ID,
                    BarkodL = s.BARKOD
                });
            var personelArray = Array.ConvertAll(
                TalepRaw.PERSONELLER,
                s => new Personnel
                {
                    TcKimlik = s.TCKIMLIK,
                    AdiSoyadi = s.ADISOYADI
                });

            var talep = new Talep
            {
                IslemTipi = (int) TalepRaw.ISLEMTIPI,
                ReferansId = TalepRaw.REFERANSID,
                HizmetNoktaKod = TalepRaw.HIZMETNOKTAKOD,
                NomKod = TalepRaw.NOMKOD,
                Tutar = TalepRaw.TUTAR,
                TutarParaBirimi = TalepRaw.TUTARPARABIRIMI,
                TalepTipi = TalepRaw.TALEPTIPI,
                TalepDurum = TalepRaw.TALEPDURUM,
                TalepTarih = TalepRaw.TALEPTARIH,
                Aciklama = TalepRaw.ACIKLAMA,
                Kupurler = new List<Kupur>(kupurArray),
                Barkodlar = new List<Barkod>(barkodArray),
                Personneller = new List<Personnel>(personelArray),
                AracPlaka = TalepRaw.ARACPLAKA,
                Result = TalepRaw.RESULT.MESSAGE
            };


            return talep;
        }
    }

    private JObject GetTaleps(
        DateTime startDate,
        DateTime endDate,
        int islemTipi,
        int talepDurum,
        string talepTipi,
        string referansId,
        string ilKod,
        List<string> kioskList)
    {
        var json = new JObject();

        try
        {
            var errorMessage = "";
            using (var desmer = new DesmerOpr())
            {
                var talepList =
                    desmer.GetTaleps(
                        startDate,
                        endDate);

                if (islemTipi != -1)
                    talepList = talepList.Where(x => x.IslemTipi == islemTipi).ToList();

                if (referansId != null)
                    talepList = talepList.Where(x => x.ReferansId == referansId).ToList();

                if (talepDurum != -1)
                    talepList = talepList.Where(x => x.TalepDurum == talepDurum).ToList();

                if (talepTipi != null)
                    talepList = talepList.Where(x => x.TalepTipi == talepTipi).ToList();

                if (ilKod != null && ilKod != "0")
                    if (kioskList != null && kioskList.Any())
                        talepList = talepList.Join(kioskList, o => o.HizmetNoktaKod, i => i, (o, i) => o).ToList();
                    //TalepList = TalepList.Where(x => KioskList.Contains(x.HizmetNoktaKod)).ToList();
                    else if (kioskList == null || !kioskList.Any())
                    {
                        var noktaList = GetNoktalarRaw(ilKod, out errorMessage);
                        //var birimList = GetDesmerBirimlerRaw(out errorMessage);
                        talepList = talepList.Join(noktaList, o => o.HizmetNoktaKod, i => i.Kod, (o, i) => o)
                            .ToList();
                    }

                var talepDurumDictionary = GetTalepDurumlarRaw(out errorMessage)
                    .ToDictionary(x => int.Parse(x.Kod), x => x.Adı);

                var value = "";
                decimal totalTutar = 0;
                foreach (var talep in talepList)
                {
                    talepDurumDictionary.TryGetValue(talep.TalepDurum, out value);
                    talep.TalepDurumDetay = value;

                    try
                    {
                        totalTutar +=Convert.ToDecimal(talep.Tutar);

                    }
                    catch (Exception ex)
                    {

                    }

                    switch (talep.IslemTipi)
                    {
                        case 0:
                            talep.IslemTipiDetay = "Yeni";
                            break;
                        case 1:
                            talep.IslemTipiDetay = "Güncelle";
                            break;
                        case 2:
                            talep.IslemTipiDetay = "İptal";
                            break;
                        case 3:
                            talep.IslemTipiDetay = "Hazır";
                            break;
                        case 4:
                            talep.IslemTipiDetay = "Kapat";
                            break;
                        default:
                            talep.IslemTipiDetay = "";
                            break;
                    }
                }

                using (var kioskOpr = new KioskOpr())
                {
                    foreach (var talep in talepList)
                    {
                        var kiosk = kioskOpr.GetKiosk(Convert.ToInt32(talep.HizmetNoktaKod));
                        talep.HizmetNoktaName = kiosk != null ? kiosk.KioskName : "";
                    }
                }
                talepList.Add(new Talep
                {
                    Tutar = totalTutar,
                    StatusColor = "info"
                });
                json["talepListesi"] = JToken.FromObject(talepList);
                json["errorMessage"] = errorMessage;
            }
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetTaleps");
        }


        return json;
    }

    private JObject SetTalep(
        int islemTipi,
        string referansId,
        string hizmetNoktaKod,
        string nomKod,
        decimal tutar,
        string tutarParaBirimi,
        string talepTipi,
        string arizaTipi,
        DateTime talepTarih,
        string aciklama,
        List<Kupur> kupurler,
        List<Barkod> barkodlar)
    {
        var json = new JObject();

        try
        {
            string errorMessage;
            bool errorStatus;

            var endReferansId = SetTalepRaw(
                islemTipi,
                referansId,
                hizmetNoktaKod,
                nomKod,
                tutar,
                tutarParaBirimi,
                talepTipi,
                arizaTipi,
                talepTarih,
                aciklama,
                kupurler,
                barkodlar,
                out errorStatus,
                out errorMessage);

            json["talepReferansId"] = endReferansId;
            json["errorMessage"] = errorMessage;
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SetTalep");
        }

        return json;
    }

    private static string SetTalepRaw(
        int islemTipi,
        string referansId,
        string hizmetNoktaKod,
        string nomKod,
        decimal tutar,
        string tutarParaBirimi,
        string talepTipi,
        string arizaTipi,
        DateTime talepTarih,
        string aciklama,
        List<Kupur> kupurler,
        List<Barkod> barkodlar,
        out bool errorStatus,
        out string errorMessage)
    {
        TALEP talepRaw;
        using (var desmer = new CITSoapClient())
        {
            _KUPURLER[] kupurArray = null;
            if (kupurler != null)
            {
                kupurArray = kupurler.ConvertAll(
                        s => new _KUPURLER
                        {
                            KUPURKOD = s.KupurKod,
                            ADET = s.Adet
                        })
                    .ToArray();
            }

            _BARKOD[] barkodArray = null;
            if (barkodlar != null)
            {
                barkodArray = barkodlar.ConvertAll(
                        s => new _BARKOD
                        {
                            ID = s.Id,
                            BARKOD = s.BarkodL
                        })
                    .ToArray();
            }

            var talepDetay = new TALEP
            {
                ISLEMTIPI = (ISLEMTIPI) islemTipi,
                REFERANSID = referansId,
                HIZMETNOKTAKOD = hizmetNoktaKod,
                NOMKOD = nomKod,
                TUTAR = tutar,
                TUTARPARABIRIMI = tutarParaBirimi,
                TALEPTIPI = talepTipi,
                ARIZATIPI = arizaTipi,
                TALEPTARIH = talepTarih,
                ACIKLAMA = aciklama,
                KUPURLER = kupurArray,
                BARKODLAR = barkodArray
            };

            talepRaw = desmer.TALEP(credentials, talepDetay);
        }

        if (talepRaw != null)
        {
            errorStatus = talepRaw.RESULT.RESULTTYPE == RESULTTYPE.SUCCESS;
            errorMessage = talepRaw.RESULT.MESSAGE;
        }
        else
        {
            errorStatus = false;
            errorMessage = "BOS Talep sonucu yok.";
        }


        return talepRaw.REFERANSID;
    }

    //
    //
    //

    private Talep AddTalep(
        string referansId,
        string hizmetNoktaKod,
        string nomKod,
        decimal tutar,
        string tutarParaBirimi,
        string talepTipi,
        string arizaTipi,
        DateTime talepTarih,
        string aciklama,
        List<Kupur> kupurler,
        List<Barkod> barkodlar,
        out string errorMessage)
    {
        try
        {
            bool errorStatus;
            var endReferansId = SetTalepRaw(
                0,
                referansId,
                hizmetNoktaKod,
                nomKod,
                tutar,
                tutarParaBirimi,
                talepTipi,
                arizaTipi,
                talepTarih,
                aciklama,
                kupurler,
                barkodlar,
                out errorStatus,
                out errorMessage);

            if (!errorStatus)
                return null;

            var talep = GetTalepSorguRaw(endReferansId, out errorMessage);
            return talep;
        }
        catch (Exception exp)
        {
            errorMessage = "BOS Talep Ekleme Başarısız.";
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "AddTalep");
        }

        return null;
    }

    private JObject AddTalepDoldurma(
        string hizmetNoktaKod,
        string nomKod,
        decimal tutar,
        string tutarParaBirimi,
        string talepTipi,
        string arizaTipi,
        DateTime talepTarih,
        string aciklama,
        List<Kupur> kupurler,
        List<Barkod> barkodlar,
        int userId)
    {
        var json = new JObject();

        string errorMessage;

        var talep = new Talep()
        {
            HizmetNoktaKod = hizmetNoktaKod,
            NomKod = nomKod,
            Tutar = tutar,
            TutarParaBirimi = tutarParaBirimi ?? "TL",
            TalepTipi = talepTipi,
            ArizaTipi = arizaTipi,
            TalepTarih = talepTarih,
            Aciklama = aciklama,
            Kupurler = kupurler,
            Barkodlar = barkodlar
        };

        try
        {
            // INSERT TALEP TO BOS
            var result = false;
            int refId;
            using (var desmer = new DesmerOpr())
            {
                result = desmer.InsertTalep(talep, "", out refId);
                talep.ReferansId = refId.ToString();

                if (!result)
                {
                    json["errorMessage"] = "BOS Talep veritabanına eklenemedi";
                    return json;
                }
            }

            if (talep.TalepTarih == null)
                talep.TalepTarih = DateTime.Now;

            // ADD TALEP
            talep = AddTalep(
                talep.ReferansId,
                talep.HizmetNoktaKod,
                talep.NomKod,
                talep.Tutar,
                talep.TutarParaBirimi,
                talep.TalepTipi,
                talep.ArizaTipi,
                (DateTime) talep.TalepTarih,
                talep.Aciklama,
                talep.Kupurler,
                talep.Barkodlar,
                out errorMessage);

            if (errorMessage.StartsWith("BOS") || !errorMessage.StartsWith("S0003") || talep == null)
            {
                using (var desmer = new DesmerOpr())
                {
                    desmer.EditTalepStatus(refId.ToString(), 4);
                }

                json["errorMessage"] = errorMessage;
                return json;
            }

            // ADD BILDIRIM KONTROL
            using (var opr = new AccountOpr())
            {
                var kioskId = 0;
                var parseResult = int.TryParse(talep.HizmetNoktaKod, out kioskId);

                if (!parseResult)
                {
                    json["errorMessage"] = "KioskNo Uyumsuz";
                    return json;
                }

                var TL200 = kupurler.Where(x => x.KupurKod == "TL200").Sum(c => c.Adet);
                var TL100 = kupurler.Where(x => x.KupurKod == "TL100").Sum(c => c.Adet);
                var TL50 = kupurler.Where(x => x.KupurKod == "TL50").Sum(c => c.Adet);
                var TL20 = kupurler.Where(x => x.KupurKod == "TL20").Sum(c => c.Adet);
                var TL10 = kupurler.Where(x => x.KupurKod == "TL10").Sum(c => c.Adet);
                var TL5 = kupurler.Where(x => x.KupurKod == "TL5").Sum(c => c.Adet);
                var TL1 = kupurler.Where(x => x.KupurKod == "TL1").Sum(c => c.Adet);
                var TL05 = kupurler.Where(x => x.KupurKod == "TL0,5").Sum(c => c.Adet);
                var TL025 = kupurler.Where(x => x.KupurKod == "TL0,25").Sum(c => c.Adet);
                var TL01 = kupurler.Where(x => x.KupurKod == "TL0,1").Sum(c => c.Adet);
                var TL005 = kupurler.Where(x => x.KupurKod == "TL0,05").Sum(c => c.Adet);


                var resp = opr.SaveControlCashNotification(
                    kioskId: kioskId,
                    expertUserId: userId,
                    firstDenomNumber: TL200.ToString(),
                    secondDenomNumber: TL100.ToString(),
                    thirdDenomNumber: (TL50).ToString(),
                    fourthDenomNumber: TL20.ToString(),
                    fifthDenomNumber: (TL10).ToString(),
                    sixthDenomNumber: (TL5).ToString(),
                    seventhDenomNumber: (TL1).ToString(),
                    eighthDenomNumber: (TL05).ToString(),
                    ninthDenomNumber: (TL025).ToString(),
                    tenthDenomNumber: (TL01).ToString(),
                    eleventhDenomNumber: (TL005).ToString(),
                    totalAmount: talep.Tutar,
                    desmerId: talep.ReferansId
                );

                if (resp == (int) ManagementScreenErrorCodes.SystemError)
                    _errorCode = ReturnCodes.SERVER_SIDE_ERROR;

                if (resp == 0)
                {
                    _errorCode = ReturnCodes.SUCCESSFULL;
                }
                else
                {
                    using (var desmer = new DesmerOpr())
                    {
                        desmer.EditTalepStatus(talep.ReferansId, 3);
                    }

                    _errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
                    json["errorMessage"] = "BOS Para bildirim eklenemedi";
                    return json;
                }
            }
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "AddTalepDoldurma");
        }

        return json;
    }

    private JObject AddTalepBosaltma(
        List<TalepBosaltmaSingle> inputs)
    {
        var json = new JObject();

        var failedInputs = new List<TalepBosaltmaSingle>();

        foreach (var input in inputs)
        {
            string errorMessage;


            var talep = new Talep()
            {
                ReferansId = input.ReferansId,
                HizmetNoktaKod = input.HizmetNoktaKod,
                NomKod = input.NomKod,
                Tutar = input.Tutar,
                TutarParaBirimi = input.TutarParaBirimi ?? "TL",
                TalepTipi = input.TalepTipi,
                ArizaTipi = input.ArizaTipi,
                TalepTarih = input.TalepTarih,
                Aciklama = input.Aciklama,
                Kupurler = input.Kupurler,
                Barkodlar = input.Barkodlar
            };

            try
            {
                if (talep.TalepTarih == null)
                    talep.TalepTarih = DateTime.Now;

                var referans = input.Reference;

                // INSERT TALEP TO BOS
                var refId = 0;
                using (var desmer = new DesmerOpr())
                {
                    var result = desmer.InsertTalep(talep, referans.referenceNo, out refId);
                    talep.ReferansId = refId.ToString();

                    if (!result)
                        failedInputs.Add(new TalepBosaltmaSingle
                        {
                            ReferansId = talep.ReferansId,
                            HizmetNoktaKod = talep.HizmetNoktaKod,
                            NomKod = talep.NomKod,
                            Tutar = talep.Tutar,
                            TutarParaBirimi = talep.TutarParaBirimi,
                            TalepTipi = talep.TalepTipi,
                            ArizaTipi = talep.ArizaTipi,
                            TalepTarih = talep.TalepTarih,
                            Aciklama = talep.Aciklama,
                            Kupurler = talep.Kupurler,
                            Barkodlar = talep.Barkodlar,
                            Reference = referans,
                            Message = "BOS Talep veritabanına eklenemedi."
                        });
                }


                // ADD TALEP
                talep = AddTalep(
                    talep.ReferansId,
                    talep.HizmetNoktaKod,
                    talep.NomKod,
                    talep.Tutar,
                    talep.TutarParaBirimi,
                    talep.TalepTipi,
                    talep.ArizaTipi,
                    (DateTime) talep.TalepTarih,
                    talep.Aciklama,
                    talep.Kupurler,
                    talep.Barkodlar,
                    out errorMessage);

                if (errorMessage.StartsWith("BOS") || !errorMessage.StartsWith("S0003") || talep == null)
                {
                    using (var desmer = new DesmerOpr())
                    {
                        desmer.EditTalepStatus(refId.ToString(), 4);
                    }

                    failedInputs.Add(new TalepBosaltmaSingle
                    {
                        ReferansId = talep.ReferansId,
                        HizmetNoktaKod = talep.HizmetNoktaKod,
                        NomKod = talep.NomKod,
                        Tutar = talep.Tutar,
                        TutarParaBirimi = talep.TutarParaBirimi,
                        TalepTipi = talep.TalepTipi,
                        ArizaTipi = talep.ArizaTipi,
                        TalepTarih = talep.TalepTarih,
                        Aciklama = talep.Aciklama,
                        Kupurler = talep.Kupurler,
                        Barkodlar = talep.Barkodlar,
                        Reference = referans,
                        Message = errorMessage
                    });
                    continue;
                }

                // ADD BOS REFERENCE
                using (var koskOpr = new KioskOpr())
                {
                    var resp = koskOpr.SaveKioskReferenceSystemOrder(referans.adminUserId,
                        referans.referenceNo,
                        referans.kioskId,
                        referans.explanation,
                        talep.ReferansId);
                    if (resp == 1)
                    {
                        _errorCode = ReturnCodes.SUCCESSFULL;
                    }
                    else
                    {
                        using (var desmer = new DesmerOpr())
                        {
                            desmer.EditTalepStatus(refId.ToString(), 3);
                        }

                        _errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
                        failedInputs.Add(new TalepBosaltmaSingle
                        {
                            ReferansId = talep.ReferansId,
                            HizmetNoktaKod = talep.HizmetNoktaKod,
                            NomKod = talep.NomKod,
                            Tutar = talep.Tutar,
                            TutarParaBirimi = talep.TutarParaBirimi,
                            TalepTipi = talep.TalepTipi,
                            ArizaTipi = talep.ArizaTipi,
                            TalepTarih = talep.TalepTarih,
                            Aciklama = talep.Aciklama,
                            Kupurler = talep.Kupurler,
                            Barkodlar = talep.Barkodlar,
                            Reference = referans,
                            Message = "BOS Referans eklenemedi."
                        });
                        continue;
                    }
                }
            }
            catch (Exception exp)
            {
                _errorCode = ReturnCodes.SYSTEM_ERROR;
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "AddTalepBosaltma");
            }
        }

        json["failedInputs"] = JToken.FromObject(failedInputs);
        json["errorMessage"] = failedInputs.Any() ? "BOS Başarısız işlemler var" : "";

        return json;
    }

    private JObject AddTalepOther(
        string hizmetNoktaKod,
        string nomKod,
        //decimal tutar,
        //string tutarParaBirimi,
        string talepTipi,
        string arizaTipi,
        DateTime talepTarih,
        string aciklama)
    {
        var json = new JObject();

        string errorMessage;

        var talep = new Talep()
        {
            HizmetNoktaKod = hizmetNoktaKod,
            NomKod = nomKod,
            //Tutar = tutar,
            //TutarParaBirimi = tutarParaBirimi ?? "TL",
            TalepTipi = talepTipi,
            ArizaTipi = arizaTipi,
            TalepTarih = talepTarih,
            Aciklama = aciklama
        };

        try
        {
            // INSERT TALEP TO BOS
            var result = false;
            int refId;
            using (var desmer = new DesmerOpr())
            {
                result = desmer.InsertTalep(talep, "", out refId);
                talep.ReferansId = refId.ToString();

                if (!result)
                {
                    json["errorMessage"] = "BOS Talep veritabanına eklenemedi";
                    return json;
                }
            }

            if (talep.TalepTarih == null)
                talep.TalepTarih = DateTime.Now;

            // ADD TALEP
            talep = AddTalep(
                talep.ReferansId,
                talep.HizmetNoktaKod,
                talep.NomKod,
                talep.Tutar,
                talep.TutarParaBirimi,
                talep.TalepTipi,
                talep.ArizaTipi,
                (DateTime) talep.TalepTarih,
                talep.Aciklama,
                talep.Kupurler,
                talep.Barkodlar,
                out errorMessage);

            if (errorMessage.StartsWith("BOS") || !errorMessage.StartsWith("S0003") || talep == null)
            {
                using (var desmer = new DesmerOpr())
                {
                    desmer.EditTalepStatus(refId.ToString(), 4);
                }

                json["errorMessage"] = errorMessage;
                return json;
            }
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "AddTalepOther");
        }

        return json;
    }

    private Talep EditTalep(
        string referansId,
        string hizmetNoktaKod,
        string nomKod,
        decimal tutar,
        string tutarParaBirimi,
        string talepTipi,
        string arizaTipi,
        DateTime talepTarih,
        string aciklama,
        List<Kupur> kupurler,
        List<Barkod> barkodlar,
        out string errorMessage)
    {
        try
        {
            bool errorStatus;
            var endReferansId = SetTalepRaw(
                1,
                referansId,
                hizmetNoktaKod,
                nomKod,
                tutar,
                tutarParaBirimi,
                talepTipi,
                arizaTipi,
                talepTarih,
                aciklama,
                kupurler,
                barkodlar,
                out errorStatus,
                out errorMessage);

            if (!errorStatus)
                return null;

            var talep = GetTalepSorguRaw(endReferansId, out errorMessage);
            return talep;
        }
        catch (Exception exp)
        {
            errorMessage = "BOS Talep Değiştirme Başarısız.";
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "EditTalep");
        }

        return null;
    }

    private JObject EditTalepDoldurma(
        string referansId,
        string hizmetNoktaKod,
        string nomKod,
        decimal tutar,
        string tutarParaBirimi,
        string talepTipi,
        string arizaTipi,
        DateTime talepTarih,
        string aciklama,
        List<Kupur> kupurler,
        List<Barkod> barkodlar,
        int userId)
    {
        var json = new JObject();

        string errorMessage;

        var talep = GetTalepSorguRaw(referansId, out errorMessage);

        talep.HizmetNoktaKod = hizmetNoktaKod;
        talep.NomKod = nomKod;
        talep.Tutar = tutar;
        talep.TutarParaBirimi = tutarParaBirimi ?? "TL";
        talep.TalepTipi = talepTipi;
        talep.ArizaTipi = arizaTipi;
        talep.TalepTarih = talepTarih;
        talep.Aciklama = aciklama;
        talep.Kupurler = kupurler;
        talep.Barkodlar = barkodlar;

        if (talep.TalepTarih == null)
        {
            json["errorMessage"] = "Talep tarihi hatalı aktarıldı";
            return json;
        }

        try
        {
            // EDIT TALEP
            talep = EditTalep(
                talep.ReferansId,
                talep.HizmetNoktaKod,
                talep.NomKod,
                talep.Tutar,
                talep.TutarParaBirimi,
                talep.TalepTipi,
                talep.ArizaTipi,
                (DateTime) talep.TalepTarih,
                talep.Aciklama,
                talep.Kupurler,
                talep.Barkodlar,
                out errorMessage);

            if (errorMessage.StartsWith("BOS") || !errorMessage.StartsWith("S0003") || talep == null)
            {
                using (var desmer = new DesmerOpr())
                {
                    desmer.EditTalepStatus(talep != null ? talep.ReferansId : referansId, 4);
                }

                json["errorMessage"] = errorMessage;
                return json;
            }

            // ADD BILDIRIM KONTROL
            using (var opr = new DesmerOpr())
            {
                var kioskId = 0;
                var parseResult = int.TryParse(talep.HizmetNoktaKod, out kioskId);

                if (!parseResult)
                {
                    json["errorMessage"] = "KioskNo Uyumsuz";
                    return json;
                }

                var TL200 = kupurler.Where(x => x.KupurKod == "TL200").Sum(c => c.Adet);
                var TL100 = kupurler.Where(x => x.KupurKod == "TL100").Sum(c => c.Adet);
                var TL50 = kupurler.Where(x => x.KupurKod == "TL50").Sum(c => c.Adet);
                var TL20 = kupurler.Where(x => x.KupurKod == "TL20").Sum(c => c.Adet);
                var TL10 = kupurler.Where(x => x.KupurKod == "TL10").Sum(c => c.Adet);
                var TL5 = kupurler.Where(x => x.KupurKod == "TL5").Sum(c => c.Adet);
                var TL1 = kupurler.Where(x => x.KupurKod == "TL1").Sum(c => c.Adet);
                var TL05 = kupurler.Where(x => x.KupurKod == "TL0,5").Sum(c => c.Adet);
                var TL025 = kupurler.Where(x => x.KupurKod == "TL0,25").Sum(c => c.Adet);
                var TL01 = kupurler.Where(x => x.KupurKod == "TL0,1").Sum(c => c.Adet);
                var TL005 = kupurler.Where(x => x.KupurKod == "TL0,05").Sum(c => c.Adet);

                var resp = opr.EditControlCashNotification(
                    expertUserId: userId,
                    firstDenomNumber: TL200.ToString(),
                    secondDenomNumber: TL100.ToString(),
                    thirdDenomNumber: (TL50).ToString(),
                    fourthDenomNumber: TL20.ToString(),
                    fifthDenomNumber: (TL10).ToString(),
                    sixthDenomNumber: (TL5).ToString(),
                    seventhDenomNumber: (TL1).ToString(),
                    eighthDenomNumber: (TL05).ToString(),
                    ninthDenomNumber: (TL025).ToString(),
                    tenthDenomNumber: (TL01).ToString(),
                    eleventhDenomNumber: (TL005).ToString(),
                    totalAmount: talep.Tutar,
                    desmerId: talep.ReferansId
                );

                if (resp == (int) ManagementScreenErrorCodes.SystemError)
                    _errorCode = ReturnCodes.SERVER_SIDE_ERROR;

                if (resp == 1)
                {
                    _errorCode = ReturnCodes.SUCCESSFULL;
                }
                else
                {
                    using (var desmer = new DesmerOpr())
                    {
                        desmer.EditTalepStatus(talep != null ? talep.ReferansId : referansId, 3);
                    }

                    _errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
                    json["errorMessage"] = "BOS Para bildirim eklenemedi";
                    return json;
                }
            }

            // UPDATE TALEP TO BOS
            var result = false;
            using (var desmer = new DesmerOpr())
            {
                result = desmer.EditTalep(
                    talep.ReferansId,
                    talep.HizmetNoktaKod,
                    talep.IslemTipi,
                    talep.TalepDurum,
                    talep.TalepTipi,
                    talep.TalepTarih,
                    talep.Tutar,
                    talep.Aciklama,
                    talep.Personneller == null ? null : string.Join(",", talep.Personneller.Select(x => x.AdiSoyadi))
                );

                if (!result)
                {
                    desmer.EditTalepStatus(talep != null ? talep.ReferansId : referansId, 2);
                    json["errorMessage"] = "BOS Talep veritabanına eklenemedi";
                }
            }
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "EditTalepDoldurma");
        }

        return json;
    }

    private JObject EditTalepBosaltma(
        TalepBosaltmaSingle input)
    {
        var json = new JObject();

        var FailedInputs = new List<TalepBosaltmaSingle>();

        string errorMessage;
        var talep = GetTalepSorguRaw(input.ReferansId, out errorMessage);

        talep.Aciklama = input.Reference.explanation;
        talep.HizmetNoktaKod = input.Reference.kioskId;

        var referans = input.Reference;


        if (talep.TalepTarih == null)
        {
            json["errorMessage"] = "Talep tarihi hatalı aktarıldı";
            return json;
        }

        try
        {
            // EDIT TALEP
            talep = EditTalep(
                talep.ReferansId,
                talep.HizmetNoktaKod,
                talep.NomKod,
                talep.Tutar,
                talep.TutarParaBirimi,
                talep.TalepTipi,
                talep.ArizaTipi,
                (DateTime) talep.TalepTarih,
                talep.Aciklama,
                talep.Kupurler,
                talep.Barkodlar,
                out errorMessage);

            if (errorMessage.StartsWith("BOS") || !errorMessage.StartsWith("S0003") || talep == null)
            {
                using (var desmer = new DesmerOpr())
                {
                    desmer.EditTalepStatus(talep != null ? talep.ReferansId : input.ReferansId, 4);
                }

                FailedInputs.Add(new TalepBosaltmaSingle
                {
                    ReferansId = talep.ReferansId,
                    HizmetNoktaKod = talep.HizmetNoktaKod,
                    NomKod = talep.NomKod,
                    Tutar = talep.Tutar,
                    TutarParaBirimi = talep.TutarParaBirimi,
                    TalepTipi = talep.TalepTipi,
                    ArizaTipi = talep.ArizaTipi,
                    TalepTarih = talep.TalepTarih,
                    Aciklama = talep.Aciklama,
                    Kupurler = talep.Kupurler,
                    Barkodlar = talep.Barkodlar,
                    Reference = referans,
                    Message = errorMessage
                });
                json["failedInputs"] = JToken.FromObject(FailedInputs);
                return json;
            }

            // EDIT BOS REFERENCE
            using (var koskOpr = new DesmerOpr())
            {
                var resp = koskOpr.EditKioskReferenceSystemOrder(referans.adminUserId,
                    referans.referenceNo,
                    referans.kioskId,
                    referans.explanation,
                    talep.ReferansId);

                if (resp == 1)
                {
                    _errorCode = ReturnCodes.SUCCESSFULL;
                }
                else
                {
                    using (var desmer = new DesmerOpr())
                    {
                        desmer.EditTalepStatus(talep.ReferansId, 3);
                    }

                    _errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
                    FailedInputs.Add(new TalepBosaltmaSingle
                    {
                        ReferansId = talep.ReferansId,
                        HizmetNoktaKod = talep.HizmetNoktaKod,
                        NomKod = talep.NomKod,
                        Tutar = talep.Tutar,
                        TutarParaBirimi = talep.TutarParaBirimi,
                        TalepTipi = talep.TalepTipi,
                        ArizaTipi = talep.ArizaTipi,
                        TalepTarih = talep.TalepTarih,
                        Aciklama = talep.Aciklama,
                        Kupurler = talep.Kupurler,
                        Barkodlar = talep.Barkodlar,
                        Reference = referans,
                        Message = "BOS Referans eklenemedi."
                    });
                    json["failedInputs"] = JToken.FromObject(FailedInputs);
                    return json;
                }
            }

            // UPDATE TALEP TO BOS
            var result = false;
            using (var desmer = new DesmerOpr())
            {
                result = desmer.EditTalep(
                    talep.ReferansId,
                    talep.HizmetNoktaKod,
                    talep.IslemTipi,
                    talep.TalepDurum,
                    talep.TalepTipi,
                    talep.TalepTarih,
                    talep.Tutar,
                    talep.Aciklama,
                    talep.Personneller == null ? null : string.Join(",", talep.Personneller.Select(x => x.AdiSoyadi))
                );

                if (!result)
                {
                    desmer.EditTalepStatus(talep.ReferansId, 2);

                    FailedInputs.Add(new TalepBosaltmaSingle
                    {
                        ReferansId = talep.ReferansId,
                        HizmetNoktaKod = talep.HizmetNoktaKod,
                        NomKod = talep.NomKod,
                        Tutar = talep.Tutar,
                        TutarParaBirimi = talep.TutarParaBirimi,
                        TalepTipi = talep.TalepTipi,
                        ArizaTipi = talep.ArizaTipi,
                        TalepTarih = talep.TalepTarih,
                        Aciklama = talep.Aciklama,
                        Kupurler = talep.Kupurler,
                        Barkodlar = talep.Barkodlar,
                        Reference = referans,
                        Message = "BOS Talep veritabanına eklenemedi."
                    });
                }
            }
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "EditTalepBosaltma");
        }


        json["failedInputs"] = JToken.FromObject(FailedInputs);
        json["errorMessage"] = FailedInputs.Any() ? "BOS Başarısız işlemler var" : "";

        return json;
    }

    private JObject EditTalepOther(
        string referansId,
        string hizmetNoktaKod,
        string nomKod,
        string talepTipi,
        string arizaTipi,
        DateTime talepTarih,
        string aciklama)
    {
        var json = new JObject();

        string errorMessage;
        var talep = GetTalepSorguRaw(referansId, out errorMessage);

        talep.HizmetNoktaKod = hizmetNoktaKod;
        talep.NomKod = nomKod;
        talep.TalepTipi = talepTipi;
        talep.ArizaTipi = arizaTipi;
        talep.TalepTarih = talepTarih;
        talep.Aciklama = aciklama;

        if (talep.TalepTarih == null)
        {
            json["errorMessage"] = "Talep tarihi hatalı aktarıldı";
            return json;
        }

        try
        {
            // EDIT TALEP
            talep = EditTalep(
                talep.ReferansId,
                talep.HizmetNoktaKod,
                talep.NomKod,
                talep.Tutar,
                talep.TutarParaBirimi,
                talep.TalepTipi,
                talep.ArizaTipi,
                (DateTime) talep.TalepTarih,
                talep.Aciklama,
                talep.Kupurler,
                talep.Barkodlar,
                out errorMessage);

            if (errorMessage.StartsWith("BOS") || !errorMessage.StartsWith("S0003") || talep == null)
            {
                using (var desmer = new DesmerOpr())
                {
                    desmer.EditTalepStatus(talep != null ? talep.ReferansId : referansId, 4);
                }

                json["errorMessage"] = errorMessage;
                return json;
            }

            // UPDATE TALEP TO BOS
            var result = false;
            using (var desmer = new DesmerOpr())
            {
                result = desmer.EditTalep(
                    talep.ReferansId,
                    talep.HizmetNoktaKod,
                    talep.IslemTipi,
                    talep.TalepDurum,
                    talep.TalepTipi,
                    talep.TalepTarih,
                    talep.Tutar,
                    talep.Aciklama,
                    talep.Personneller == null ? null : string.Join(",", talep.Personneller.Select(x => x.AdiSoyadi))
                );

                if (!result)
                {
                    desmer.EditTalepStatus(talep.ReferansId, 2);

                    json["errorMessage"] = "BOS Talep veritabanına eklenemedi";
                }
            }
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "EditTalepOther");
        }

        return json;
    }

    private JObject UpdateTalep(string referansId)
    {
        var json = new JObject();

        string errorMessage;
        var talep = new Talep();

        // GET TALEP
        try
        {
            talep = GetTalepSorguRaw(referansId, out errorMessage);
        }
        catch (Exception exp)
        {
            errorMessage = "BOS Talep Alımı Başarısız.";
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UpdateTalepSorgu");
        }


        if (errorMessage.StartsWith("BOS") || !errorMessage.StartsWith("S0003"))
        {
            using (var desmer = new DesmerOpr())
            {
                desmer.EditTalepStatus(talep != null ? talep.ReferansId : referansId, 4);
            }

            json["errorMessage"] = errorMessage;
            return json;
        }

        // EDIT TALEP IN BOS
        var result = false;
        using (var desmer = new DesmerOpr())
        {
            result = desmer.EditTalep(
                talep.ReferansId,
                talep.HizmetNoktaKod,
                talep.IslemTipi,
                talep.TalepDurum,
                talep.TalepTipi,
                talep.TalepTarih,
                talep.Tutar,
                talep.Aciklama,
                talep.Personneller == null ? null : string.Join(",", talep.Personneller.Select(x => x.AdiSoyadi))
            );

            if (!result)
            {
                //if (talep != null)
                desmer.EditTalepStatus(talep.ReferansId, 2);

                json["errorMessage"] = "BOS Talep veritabanına eklenemedi";
                return json;
            }

            desmer.EditTalepStatus(talep.ReferansId, 0);
        }

        json["errorMessage"] = "İşlem Başarılı";
        return json;
    }

    private JObject EditTalepIslemType(string referansId, ISLEMTIPI islemType)
    {
        var json = new JObject();

        string errorMessage;
        var talep = GetTalepSorguRaw(referansId, out errorMessage);

        if (talep.TalepTarih == null)
        {
            json["errorMessage"] = "Talep tarihi hatalı aktarıldı";
            return json;
        }

        // TALEP IPTAL
        try
        {
            bool errorStatus;
            var endReferansId = SetTalepRaw(
                (int) islemType,
                referansId,
                talep.HizmetNoktaKod,
                talep.NomKod,
                talep.Tutar,
                talep.TutarParaBirimi,
                talep.TalepTipi,
                talep.ArizaTipi,
                (DateTime) talep.TalepTarih,
                talep.Aciklama,
                talep.Kupurler,
                talep.Barkodlar,
                out errorStatus,
                out errorMessage);

            talep = GetTalepSorguRaw(endReferansId, out errorMessage);

            if (!errorStatus || errorMessage.StartsWith("BOS") || !errorMessage.StartsWith("S0003") || talep == null)
            {
                json["errorMessage"] = errorMessage;
                return json;
            }

            // UPDATE TALEP TO BOS
            var result = false;
            using (var desmer = new DesmerOpr())
            {
                result = desmer.EditTalep(
                    talep.ReferansId,
                    talep.HizmetNoktaKod,
                    talep.IslemTipi,
                    talep.TalepDurum,
                    talep.TalepTipi,
                    talep.TalepTarih,
                    talep.Tutar,
                    talep.Aciklama,
                    talep.Personneller == null ? null : string.Join(",", talep.Personneller.Select(x => x.AdiSoyadi))
                );

                if (!result)
                {
                    desmer.EditTalepStatus(talep.ReferansId, 2);
                    json["errorMessage"] = "BOS Talep veritabanı değiştirilemedi";
                }
            }
        }
        catch (Exception exp)
        {
            _errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "EditTalepOther");
        }

        json["errorMessage"] = "İşlem Başarılı";
        return json;
    }
}