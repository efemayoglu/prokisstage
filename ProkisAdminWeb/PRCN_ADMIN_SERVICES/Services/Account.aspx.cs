﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRCNCORE.Accounts;
using PRCNCORE.MoneyCodes;
using PRCNCORE.Backend;
using PRCNCORE.Constants;
using PRCNCORE.Input;
using PRCNCORE.Kiosks;
using PRCNCORE.Roles;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Net;
using PRCNCORE.Input.Account;
using System.Globalization;
using PRCNCORE.Input.AskiCode;
using System.Web.Script.Serialization;
using PRCNCORE.Input.Kiosk;
using PRCNCORE.BankActions;
using PRCNCORE.Mutabakat;
using System.Security.Principal;
using System.Xml.Serialization;
using System.Xml.Linq;
using MutabakatSERVICE2.WebReferenceAkbank1;
using System.Threading;
using TeskiTahsilatServiceReference;
//using TeskiTahsilatServiceReference;

public partial class Services_Account : System.Web.UI.Page
{
    private ReturnCodes errorCode = ReturnCodes.SUCCESSFULL;
    JObject data = new JObject();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.HttpMethod == "POST")
        {
            ProcessRequest();
        }
    }

    private void ProcessRequest()
    {
        SwitchInput inputData = null;
        try
        {
            string sData = WebServiceUtils.LoadParameters(Request, InputType.Clear);

            if (!string.IsNullOrEmpty(sData))
            {
                inputData = JsonConvert.DeserializeObject<SwitchInput>(sData);

                if (!WebServiceUtils.CheckCredentials(inputData.ApiKey, inputData.ApiPass))
                {
                    errorCode = ReturnCodes.CREDENTIALS_ERROR;
                }
                else if (Request.Headers["AccessToken"] == null || !WebServiceUtils.CheckAccessTokenSecurity(Request.Headers["AccessToken"], inputData.adminUserId))
                {
                    errorCode = ReturnCodes.INVALID_ACCESS_TOKEN;
                }
                else
                {
                    data = PrepareResponse(inputData.FunctionCode, sData, inputData.adminUserId, inputData.pageUrl);
                }
            }
            else
            {
                errorCode = ReturnCodes.LOAD_JSON_PARAMETERS_ERROR;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ProcessRequestAccount");
        }
        data = WebServiceUtils.AddErrorNodes(data, errorCode, WebServiceUtils.GetReturnCodeDescription(errorCode));
        WebServiceUtils.SendResponse(data, Response, false);
    }

    private JObject PrepareResponse(int functionCode, string input ,long adminUserId, string pageUrl)
    {
        JObject data = new JObject();
        try
        {
            switch ((FC)functionCode)
            {//update_kiosk_empty_kioskEmptyStatusId
                case FC.TESKI_CANCEL:
                    var t = JsonConvert.DeserializeObject<WSTahsilat>(input);
                    using (TeskiTahsilatServiceReference.TahsilatWsClient client = new TeskiTahsilatServiceReference.TahsilatWsClient())
                    {
                        //decimal prodBankaKodu = 37, testBankaKodu = 937;
                        //decimal testuserName = 90937;
                        //string testpassword = "WsH2iLf4xC";

                        decimal produserName = Convert.ToDecimal(Utility.GetConfigValue("TeskiUserName"));
                        var prodpassword = Utility.GetConfigValue("TeskiPassword");

                        if (true)
                        {
                            //t.makbuzSeri = "T";
                            t.aciklama = ".";
                        }

                        var teskiIptalResp = client
                            .tahsilatIptal(t.makbuzSeri, t.makbuzNo.Value, t.aciklama,
                                produserName, prodpassword, t.islemTarihi.Value.ToString("dd/MM/yyyy"));

                        errorCode = ReturnCodes.NO_DATA_FOUND;

                        if (teskiIptalResp != null)
                        {
                            if (teskiIptalResp.Contains("Code:000"))
                            {
                                errorCode = ReturnCodes.SUCCESSFULL;
                                break;
                            }
                            data = new JObject();
                            data.Add("ErrorMessage", teskiIptalResp);

                        }
                        else
                        {
                            data = new JObject();
                            data.Add("ErrorMessage","Teski data bulunamadi.");
                        }
                    }

                    break;

                case FC.UPDATE_KISOKEMPTY_STATUS:
                    var update_inputStatus = JsonConvert.DeserializeObject<KioskEmptyStatus>(input);
                    data = UpdateKioskEmptyStatus(update_inputStatus.KioskEmptyId, update_inputStatus.Id, update_inputStatus.Explanation);
                    break; 


                case FC.LIST_UsageFeeDetail:


    //    public int kioskId { get; set; }
    //public int instutionId { get; set; }
    //public bool isKioskListed { get; set; }
    //public DateTime startDate { get; set; }
    //public DateTime endDate { get; set; }
    var ls_usageFee = JsonConvert.DeserializeObject<UsageFeeDetail>(input);
                    data = GetUsageFeeDetail(ls_usageFee.kioskId, ls_usageFee.instutionId, ls_usageFee.isKioskListed, ls_usageFee.startDate, ls_usageFee.endDate);
                    break;  

                case FC.GET_KISOKEMPTY_STATUS:
                    var inputStatus = JsonConvert.DeserializeObject<KioskEmptyStatus>(input);
                    data = GetKioskEmptyStatus(inputStatus.Id);
                    break;

                case FC.LIST_ACCOUNT:
                    InputListAccounts inputListAccounts = JsonConvert.DeserializeObject<InputListAccounts>(input);
                    data = GetAccounts(inputListAccounts.InstutionId,
                                       inputListAccounts.NumberOfItemsPerPage,
                                       inputListAccounts.CurrentPageNum,
                                       inputListAccounts.RecordCount,
                                       inputListAccounts.PageCount,
                                       inputListAccounts.OrderSelectionColumn,
                                       inputListAccounts.DescAsc,
                                       inputListAccounts.OrderType);
                    break;
                case FC.GET_ACCOUNT_DETAIL:
                    InputGetAccountDetail inputGetAccountDetail = JsonConvert.DeserializeObject<InputGetAccountDetail>(input);
                    data = GetAccountDetails(inputGetAccountDetail.id,
                                             inputGetAccountDetail.instutionId,
                                             inputGetAccountDetail.NumberOfItemsPerPage,
                                             inputGetAccountDetail.CurrentPageNum);
                    break;
                case FC.LIST_MONEY_CODE:
                    InputLıstMoneyCodes listMoneyCodes = JsonConvert.DeserializeObject<InputLıstMoneyCodes>(input);
                    data = SearchMoneyCodes(listMoneyCodes.SearchText
                                            , listMoneyCodes.StartDate
                                            , listMoneyCodes.EndDate
                                            , listMoneyCodes.NumberOfItemsPerPage
                                            , listMoneyCodes.CurrentPageNum
                                            , listMoneyCodes.RecordCount
                                            , listMoneyCodes.PageCount
                                            , listMoneyCodes.OrderSelectionColumn
                                            , listMoneyCodes.DescAsc
                                            , listMoneyCodes.codeStatus
                                            , listMoneyCodes.generatedType
                                            , listMoneyCodes.instutionId
                                            , listMoneyCodes.GeneratedKioskIds
                                            , listMoneyCodes.UsedKioskIds
                                            , listMoneyCodes.PaymentType
                                            , listMoneyCodes.OrderType);
                    break;
                case FC.LIST_MONEY_CODE_SWEEP_BACK:
                    InputLıstMoneyCodes listMoneyCodesSweepBack = JsonConvert.DeserializeObject<InputLıstMoneyCodes>(input);
                    data = SearchMoneyCodesSweepBack(listMoneyCodesSweepBack.SearchText
                                            , listMoneyCodesSweepBack.StartDate
                                            , listMoneyCodesSweepBack.EndDate
                                            , listMoneyCodesSweepBack.NumberOfItemsPerPage
                                            , listMoneyCodesSweepBack.CurrentPageNum
                                            , listMoneyCodesSweepBack.RecordCount
                                            , listMoneyCodesSweepBack.PageCount
                                            , listMoneyCodesSweepBack.OrderSelectionColumn
                                            , listMoneyCodesSweepBack.DescAsc
                                            , listMoneyCodesSweepBack.codeStatus
                                            , listMoneyCodesSweepBack.generatedType
                                            , listMoneyCodesSweepBack.instutionId
                                            , listMoneyCodesSweepBack.OrderType);
                    break;
                case FC.GET_MONEY_CODE_STATUS:
                    data = GetMoneyCodeStatusNames();
                    break;
                case FC.MANUEL_KIOSK_EMPTY:
                    InputManuelKioskEmpty inputManuelKioskEmpty = JsonConvert.DeserializeObject<InputManuelKioskEmpty>(input);
                    data = ManuelKioskEmpty(inputManuelKioskEmpty.expertUserId, inputManuelKioskEmpty.kioskId);
                    break;
                case FC.GENERATE_MONEY_CODE:
                    InputSaveMoneyCode inputSaveAskiCode = JsonConvert.DeserializeObject<InputSaveMoneyCode>(input);
                    data = SaveMoneyCode(inputSaveAskiCode.transactionId,
                                             inputSaveAskiCode.userId,
                                             inputSaveAskiCode.codeAmount,
                                             inputSaveAskiCode.cellPhone);
                    break;
                case FC.GET_BASKENT_MUTABAKAT_DATA_LIST:
                    InputListGunsonuMutabakat parserList = JsonConvert.DeserializeObject<InputListGunsonuMutabakat>(input);
                    data = MakeBaskentGazMutabakatOperation(parserList.mutabakatDate
                                                          , parserList.kioskId
                                                          , parserList.instutionId
                                                          , parserList.adminUserId);

                    data.Add("DbDatasList", GetMutabakatDBList(parserList.mutabakatDate
                                                             , parserList.kioskId
                                                             , parserList.instutionId
                                                             , parserList.adminUserId));
                    break;

                case FC.GET_ASKI_MUTABAKAT_DATA_LIST:
                    InputListGunsonuMutabakat parserListAski = JsonConvert.DeserializeObject<InputListGunsonuMutabakat>(input);
                    data = MakeAskiMutabakatOperation(parserListAski.mutabakatDate
                                                          , parserListAski.kioskId
                                                          , parserListAski.instutionId
                                                          , parserListAski.adminUserId);

                    data.Add("DbDatasList", GetMutabakatDBList(parserListAski.mutabakatDate
                                                             , parserListAski.kioskId
                                                             , parserListAski.instutionId
                                                             , parserListAski.adminUserId));
                    break;

                case FC.LIST_CODE_DETAILS:
                    InputListKioskReport listKiskReports = JsonConvert.DeserializeObject<InputListKioskReport>(input);
                    data = SearchCodeReports(listKiskReports.SearchText
                                            , listKiskReports.StartDate
                                            , listKiskReports.EndDate
                                            , listKiskReports.NumberOfItemsPerPage
                                            , listKiskReports.CurrentPageNum
                                            , listKiskReports.RecordCount
                                            , listKiskReports.PageCount
                                            , listKiskReports.OrderSelectionColumn
                                            , listKiskReports.DescAsc
                                            , listKiskReports.kioskId
                                            , listKiskReports.instutionId
                                            , listKiskReports.PaymentType
                                            , listKiskReports.OrderType
                                            , listKiskReports.ProcessType
                                            , listKiskReports.BankType);
                    break;

                case FC.EXCEL_LIST_CODE_REPORT:
                    InputListKioskReport listKiskReportForExcelNew = JsonConvert.DeserializeObject<InputListKioskReport>(input);
                    data = SearchCodeReportsForExcelNew(listKiskReportForExcelNew.SearchText
                                            , listKiskReportForExcelNew.StartDate
                                            , listKiskReportForExcelNew.EndDate
                                            , listKiskReportForExcelNew.OrderSelectionColumn
                                            , listKiskReportForExcelNew.DescAsc
                                            , listKiskReportForExcelNew.kioskId
                                            , listKiskReportForExcelNew.instutionId);
                    break;

                case FC.GET_ASKI_DB_MUTABAKAT_DATA_LIST:
                    InputListGunsonuMutabakat parserListAskiDB = JsonConvert.DeserializeObject<InputListGunsonuMutabakat>(input);
                    data = MakeAskiDBListMutabakatOperation(parserListAskiDB.mutabakatDate
                                                          , parserListAskiDB.kioskId
                                                          , parserListAskiDB.instutionId
                                                          , parserListAskiDB.adminUserId);

                    data.Add("DbDatasList", GetMutabakatDBList(parserListAskiDB.mutabakatDate
                                                             , parserListAskiDB.kioskId
                                                             , parserListAskiDB.instutionId
                                                             , parserListAskiDB.adminUserId));

                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(data.ToString()), "GET_ASKI_DB_MUTABAKAT_DATA_LIST");


                    break;

                case FC.GET_BASKENT_DB_MUTABAKAT_DATA_LIST:
                    InputListGunsonuMutabakat parserListBgazDB = JsonConvert.DeserializeObject<InputListGunsonuMutabakat>(input);
                    data = MakeBgazDBListMutabakatOperation(parserListBgazDB.mutabakatDate
                                                          , parserListBgazDB.kioskId
                                                          , parserListBgazDB.instutionId
                                                          , parserListBgazDB.adminUserId);

                    data.Add("DbDatasList", GetMutabakatDBList(parserListBgazDB.mutabakatDate
                                                             , parserListBgazDB.kioskId
                                                             , parserListBgazDB.instutionId
                                                             , parserListBgazDB.adminUserId));

                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(data.ToString()), "GET_ASKI_DB_MUTABAKAT_DATA_LIST");


                    break;


                case FC.GET_MASKI_MUTABAKAT_DATA_LIST:
                    InputListGunsonuMutabakat parserListMaski = JsonConvert.DeserializeObject<InputListGunsonuMutabakat>(input);
                    data = MakeMaskiMutabakatListOperation(parserListMaski.mutabakatDate
                                                          , parserListMaski.kioskId
                                                          , parserListMaski.instutionId
                                                          , parserListMaski.adminUserId);

                    data.Add("DbDatasList", GetMutabakatDBList(parserListMaski.mutabakatDate
                                                             , parserListMaski.kioskId
                                                             , parserListMaski.instutionId
                                                             , parserListMaski.adminUserId));
                    break;

                case FC.MAKE_MUTABAKAT_COMPARISON:

                    //todoefe
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("FC.MAKE_MUTABAKAT_COMPARISON:"), "1");

                    InputListGunsonuMutabakat parserListComparison = JsonConvert.DeserializeObject<InputListGunsonuMutabakat>(input);
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("FC.MAKE_MUTABAKAT_COMPARISON:"), "2");

                    data = MakeMutabakatComparison(parserListComparison.mutabakatDate
                                                          , parserListComparison.kioskId
                                                          , parserListComparison.instutionId
                                                          , parserListComparison.adminUserId
                                                          , parserListComparison.processTypeId??1);
/*
                    data.Add("DbDatasList", GetMutabakatDBList(parserListComparison.mutabakatDate
                                                             , parserListComparison.kioskId
                                                             , parserListComparison.instutionId
                                                            , parserListComparison.adminUserId));*/
                    break;

                //MakeMutabakatComparison

                case FC.SAVE_KIOSK_EMPTY_CASH_COUNT:
                    InputListSaveKioskEmptyCashCount inputListSaveKioskEmptyCashCount = JsonConvert.DeserializeObject<InputListSaveKioskEmptyCashCount>(input);
                    data = SaveKioskEmptyCashCounts(inputListSaveKioskEmptyCashCount,0);
                    break;
                case FC.SAVE_CONTROL_CASH_NOTIFICATION_SERVICE:
                    InputListSaveControlCashNotification inputListSaveControlCashNotification = JsonConvert.DeserializeObject<InputListSaveControlCashNotification>(input);
                    data = SaveControlCashNotification(inputListSaveControlCashNotification);
                    break;

                case FC.DELETE_CONTROL_CASH_NOTIFICATION_SERVICE:
                    DeleteControlCashNotification inputListDeleteControlCashNotification = JsonConvert.DeserializeObject<DeleteControlCashNotification>(input);
                    data = DeleteControlCashNotification(inputListDeleteControlCashNotification);
                    break;

                case FC.UPDATE_CONTROL_CASH_NOTIFICATION_SERVICE:
                    InputListSaveControlCashNotification inputListUpdateControlCashNotification = JsonConvert.DeserializeObject<InputListSaveControlCashNotification>(input);
                    data = UpdateControlCashNotification(inputListUpdateControlCashNotification);
                    break;


                case FC.LIST_KIOSK_DISPENSER_CASH_COUNT:
                    InputListKioskDispenserCashCounts listDispenserCashCounts = JsonConvert.DeserializeObject<InputListKioskDispenserCashCounts>(input);
                    data = SearchKioskDispenserCashCount(listDispenserCashCounts);
                    break;


                case FC.GET_KIOSK_DISPENSER_CASH_COUNT:
                    InputListKioskDispenserCashCounts inputListGetDispenserCashCount = JsonConvert.DeserializeObject<InputListKioskDispenserCashCounts>(input);
                    data = GetKioskDispenserCashCount(inputListGetDispenserCashCount.KioskId);
                    break;


                case FC.UPDATE_DISPENSER_CASH_COUNT_SERVICE:
                    InputListSaveControlCashNotification inputListUpdateDispenserCashCount = JsonConvert.DeserializeObject<InputListSaveControlCashNotification>(input);
                    data = UpdateDispenserCashCount(inputListUpdateDispenserCashCount);
                    break;

                case FC.SAVE_DISPENSER_CASH_COUNT_SERVICE:
                    InputListSaveControlCashNotification inputListSaveDispenserCashCount = JsonConvert.DeserializeObject<InputListSaveControlCashNotification>(input);
                    data = SaveDispenserCashCount(inputListSaveDispenserCashCount);
                    break;



                case FC.LIST_KIOSK_DISPENSER_MONITORING_SERVICE:
                    InputListKioskDispenserMonitoring inputListKioskDispenserMonitoring = JsonConvert.DeserializeObject<InputListKioskDispenserMonitoring>(input);
                    data = SearchKioskDispenserMonitoring(inputListKioskDispenserMonitoring);
                    break;


                case FC.LIST_KIOSK_CASHBOX_CASH_COUNT_ERROR:
                    InputListKioskDispenserCashCounts inputListKiosCashBoxCashCountError = JsonConvert.DeserializeObject<InputListKioskDispenserCashCounts>(input);
                    data = SearchKioskCashBoxCashCountError(inputListKiosCashBoxCashCountError);
                    break;

                case FC.SAVE_KIOSK_EMPTY_CASH_COUNT_SERVICE:
                    InputListSaveControlCashNotification inputListSaveControlCashNotification2 = JsonConvert.DeserializeObject<InputListSaveControlCashNotification>(input);
                    data = SaveControlCashNotification(inputListSaveControlCashNotification2);
                    break;



                case FC.GET_KIOSK_SETTINGS:
                    InputListGetKioskSettings inputListGetKioskSettings = JsonConvert.DeserializeObject<InputListGetKioskSettings>(input);
                    data = GetKioskSettings(inputListGetKioskSettings);
                    break;

                case FC.SAVE_KIOSK_SETTINGS:
                    InputSaveKioskSettings inputListSaveKioskSettings = JsonConvert.DeserializeObject<InputSaveKioskSettings>(input);
                    data = SaveKioskSettings(inputListSaveKioskSettings);
                    break;

                case FC.SAVE_COMMISION_SETTINGS:
                    InputSaveCommisionSettings inputListSaveCommisionSettings = JsonConvert.DeserializeObject<InputSaveCommisionSettings>(input);
                    data = SaveCommisionSettings(inputListSaveCommisionSettings);
                    break;

                case FC.KIOSK_COMMISION_SETTINGS:
                    InputListGetKioskSettings inputListGetKioskCommisionSettings = JsonConvert.DeserializeObject<InputListGetKioskSettings>(input);
                    data = KioskCommisionSettings(inputListGetKioskCommisionSettings);
                    break;

                case FC.GET_KIOSK_COMMISION_SETTINGS:
                    InputGetCutofMonitoring inputKioskCommisionSettings = JsonConvert.DeserializeObject<InputGetCutofMonitoring>(input);
                    data = GetKioskCommisionSettings(inputKioskCommisionSettings.cutId);
                    break;

                case FC.UPDATE_KIOSK_COMMISION_SETTINGS
:
                    InputSaveCommisionSettings inputUpdateKioskCommisionSettings = JsonConvert.DeserializeObject<InputSaveCommisionSettings>(input);
                    data = UpdateKioskCommisionSettings(inputUpdateKioskCommisionSettings);
                    break;


                case FC.GET_EMPTY_KIOSK_NAME:
                    InputGetEmptyKioskName inputGetEmptyKioskName = JsonConvert.DeserializeObject<InputGetEmptyKioskName>(input);
                    data = GetEmptyKioskName(inputGetEmptyKioskName.expertUserId);
                    break;
                case FC.GET_EMPTY_KIOSK_NAME_FOR_MUTABAKAT:
                    data = GetEmptyKioskName();
                    break;
                case FC.GET_MUTABAKAT_CASH_COUNT:
                    InputGetMutabakatCashCount inputGetMutabakatCashCount = JsonConvert.DeserializeObject<InputGetMutabakatCashCount>(input);
                    data = GetMutabakatCashCount(inputGetMutabakatCashCount.whichSide, inputGetMutabakatCashCount.kioskEmptyId);
                    break;
                case FC.SAVE_EMPTY_KIOSK_MUTABAKAT:
                    InputSaveEmptyKioskMutabakat inputSaveEmptyKioskMutabakat = JsonConvert.DeserializeObject<InputSaveEmptyKioskMutabakat>(input);
                    data = SaveEmptyKioskMutabakat(inputSaveEmptyKioskMutabakat.userId, inputSaveEmptyKioskMutabakat.kioskEmptyId, inputSaveEmptyKioskMutabakat.inputAmount, inputSaveEmptyKioskMutabakat.outputAmount, inputSaveEmptyKioskMutabakat.explanation);
                    break;
                case FC.SEND_MONEY_CODE:
                    InputSaveSendMoneyCode inputSaveSendMoneyCode = JsonConvert.DeserializeObject<InputSaveSendMoneyCode>(input);
                    data = SendMoneyCode(inputSaveSendMoneyCode);
                    break;

                case FC.GET_MONEY_CODE_SMS_NUMBER:
                    InputListMoneyCodeSMSNumber listMoneyCodeSMSNumber = JsonConvert.DeserializeObject<InputListMoneyCodeSMSNumber>(input);
                    data = GetMoneyCodeSMSNumber(listMoneyCodeSMSNumber);
                    break;

                case FC.LIST_MUTABAKAT:
                    InputListMutabakat listMutabakats = JsonConvert.DeserializeObject<InputListMutabakat>(input);
                    data = SearchMutabakat(listMutabakats.SearchText
                        , listMutabakats.StartDate
                        , listMutabakats.EndDate
                        , listMutabakats.NumberOfItemsPerPage
                        , listMutabakats.CurrentPageNum
                        , listMutabakats.RecordCount
                        , listMutabakats.PageCount
                        , listMutabakats.OrderSelectionColumn
                        , listMutabakats.DescAsc
                        , listMutabakats.KioskIds
                        , listMutabakats.InstutionId
                        , listMutabakats.Status
                        , listMutabakats.KioskEmptyStatus
                        , listMutabakats.OrderType);
                    break;
                case FC.LIST_MUTABAKAT_FOR_REPORT:
                    InputListMutabakat listMutabakatsForReport = JsonConvert.DeserializeObject<InputListMutabakat>(input);
                    data = SearchMutabakatForReport(listMutabakatsForReport.SearchText
                        , listMutabakatsForReport.StartDate
                        , listMutabakatsForReport.EndDate
                        , listMutabakatsForReport.NumberOfItemsPerPage
                        , listMutabakatsForReport.CurrentPageNum
                        , listMutabakatsForReport.RecordCount
                        , listMutabakatsForReport.PageCount
                        , listMutabakatsForReport.OrderSelectionColumn
                        , listMutabakatsForReport.DescAsc
                        , listMutabakatsForReport.OrderType);
                    break;
                case FC.EXCEL_LIST_MUTABAKAT:
                    InputListMutabakat listMutabakatsForExcel = JsonConvert.DeserializeObject<InputListMutabakat>(input);
                    data = SearchMutabakatForExcel(listMutabakatsForExcel.SearchText
                        , listMutabakatsForExcel.StartDate
                        , listMutabakatsForExcel.EndDate
                        , listMutabakatsForExcel.OrderSelectionColumn
                        , listMutabakatsForExcel.DescAsc);
                    break;
                //case FC.EXCEL_LIST_ACCOUNT:
                //    InputListAccounts inputListAccountExcel = JsonConvert.DeserializeObject<InputListAccounts>(input);
                //    data = GetAccountsForExcel(inputListAccountExcel.InstutionId);
                //    break;

                //case FC.EXCEL_LIST_MONEY_CODE_SWEEP_BACK:
                //    InputLıstMoneyCodes listMoneyCodesSweepBackForExcel = JsonConvert.DeserializeObject<InputLıstMoneyCodes>(input);
                //    data = SearchMoneyCodesSweepBackForExcel(listMoneyCodesSweepBackForExcel.SearchText
                //                            , listMoneyCodesSweepBackForExcel.StartDate
                //                            , listMoneyCodesSweepBackForExcel.EndDate
                //                            , listMoneyCodesSweepBackForExcel.NumberOfItemsPerPage
                //                            , listMoneyCodesSweepBackForExcel.CurrentPageNum
                //                            , listMoneyCodesSweepBackForExcel.RecordCount
                //                            , listMoneyCodesSweepBackForExcel.PageCount
                //                            , listMoneyCodesSweepBackForExcel.OrderSelectionColumn
                //                            , listMoneyCodesSweepBackForExcel.DescAsc
                //                            , listMoneyCodesSweepBackForExcel.codeStatus
                //                            , listMoneyCodesSweepBackForExcel.generatedType
                //                            , listMoneyCodesSweepBackForExcel.instutionId);
                //    break;
                case FC.EXCEL_LIST_MONEY_CODE:
                    InputLıstMoneyCodes listMoneyCodesForExcel = JsonConvert.DeserializeObject<InputLıstMoneyCodes>(input);
                    data = SearchMoneyCodesForExcel(listMoneyCodesForExcel.SearchText
                                            , listMoneyCodesForExcel.StartDate
                                            , listMoneyCodesForExcel.EndDate
                                            , listMoneyCodesForExcel.NumberOfItemsPerPage
                                            , listMoneyCodesForExcel.CurrentPageNum
                                            , listMoneyCodesForExcel.RecordCount
                                            , listMoneyCodesForExcel.PageCount
                                            , listMoneyCodesForExcel.OrderSelectionColumn
                                            , listMoneyCodesForExcel.DescAsc
                                            , listMoneyCodesForExcel.codeStatus
                                            , listMoneyCodesForExcel.generatedType
                                            , listMoneyCodesForExcel.instutionId
                                            , listMoneyCodesForExcel.GeneratedKioskIds
                                            , listMoneyCodesForExcel.UsedKioskIds);
                    break;
                case FC.LIST_DAILY_ACCOUNT_REPORTS:
                    InputListMutabakat listDailyAccountReports = JsonConvert.DeserializeObject<InputListMutabakat>(input);
                    data = SearchDailyAccountReport(listDailyAccountReports.StartDate
                        , listDailyAccountReports.EndDate
                        , listDailyAccountReports.NumberOfItemsPerPage
                        , listDailyAccountReports.CurrentPageNum
                        , listDailyAccountReports.RecordCount
                        , listDailyAccountReports.PageCount
                        , listDailyAccountReports.OrderSelectionColumn
                        , listDailyAccountReports.DescAsc
                        , listDailyAccountReports.OrderType);
                    break;
                case FC.EXCEL_LIST_DAILY_ACCOUNT_REPORTS:
                    InputListMutabakat listDailyAccounReportsForExcel = JsonConvert.DeserializeObject<InputListMutabakat>(input);
                    data = SearchDailyAccounReportsForExcel(listDailyAccounReportsForExcel.StartDate
                        , listDailyAccounReportsForExcel.EndDate
                        , listDailyAccounReportsForExcel.OrderSelectionColumn
                        , listDailyAccounReportsForExcel.DescAsc);
                    break;
                case FC.LIST_DAILY_ACCOUNT_REPORTS_PROKIS:
                    InputListMutabakat listDailyAccountProkisReports = JsonConvert.DeserializeObject<InputListMutabakat>(input);
                    data = SearchDailyAccountReportProkis(listDailyAccountProkisReports.StartDate
                        , listDailyAccountProkisReports.EndDate
                        , listDailyAccountProkisReports.NumberOfItemsPerPage
                        , listDailyAccountProkisReports.CurrentPageNum
                        , listDailyAccountProkisReports.RecordCount
                        , listDailyAccountProkisReports.PageCount
                        , listDailyAccountProkisReports.OrderSelectionColumn
                        , listDailyAccountProkisReports.DescAsc
                        , listDailyAccountProkisReports.OrderType);
                    break;
                case FC.LIST_KIOSKS_CASH_ACCEPTOR:
                    InputListMutabakat listKioskCashAcceptor = JsonConvert.DeserializeObject<InputListMutabakat>(input);
                    data = SearchKioskCashAcceptor(listKioskCashAcceptor.StartDate
                        , listKioskCashAcceptor.EndDate
                        , listKioskCashAcceptor.NumberOfItemsPerPage
                        , listKioskCashAcceptor.CurrentPageNum
                        , listKioskCashAcceptor.RecordCount
                        , listKioskCashAcceptor.PageCount
                        , listKioskCashAcceptor.OrderSelectionColumn
                        , listKioskCashAcceptor.DescAsc
                        , listKioskCashAcceptor.InstutionId
                        , listKioskCashAcceptor.KioskIds
                        , listKioskCashAcceptor.OrderType);
                    break;

                case FC.LIST_KIOSKS_CASH_DISPENSER:
                    InputListMutabakat listKioskCashDispenser = JsonConvert.DeserializeObject<InputListMutabakat>(input);
                    data = SearchKioskCashDispenser(listKioskCashDispenser.StartDate
                        , listKioskCashDispenser.EndDate
                        , listKioskCashDispenser.NumberOfItemsPerPage
                        , listKioskCashDispenser.CurrentPageNum
                        , listKioskCashDispenser.RecordCount
                        , listKioskCashDispenser.PageCount
                        , listKioskCashDispenser.OrderSelectionColumn
                        , listKioskCashDispenser.DescAsc
                        , listKioskCashDispenser.InstutionId
                        , listKioskCashDispenser.KioskIds
                        , listKioskCashDispenser.OrderType);
                    break;

                case FC.LIST_KIOSKS_CASH_DISPENSER_NEW:
                    InputListMutabakat listKioskCashDispenserNew = JsonConvert.DeserializeObject<InputListMutabakat>(input);
                    data = SearchKioskCashDispenserNew(listKioskCashDispenserNew.StartDate
                        , listKioskCashDispenserNew.EndDate
                        , listKioskCashDispenserNew.NumberOfItemsPerPage
                        , listKioskCashDispenserNew.CurrentPageNum
                        , listKioskCashDispenserNew.RecordCount
                        , listKioskCashDispenserNew.PageCount
                        , listKioskCashDispenserNew.OrderSelectionColumn
                        , listKioskCashDispenserNew.DescAsc
                        , listKioskCashDispenserNew.InstutionId
                        , listKioskCashDispenserNew.KioskIds
                        , listKioskCashDispenserNew.OrderType);
                    break;

                case FC.GET_KIOSKS_CASH_DISPENSER:
                    InputGetKiosk kioskId = JsonConvert.DeserializeObject<InputGetKiosk>(input);
                    data = GetKioskCashDispenser(kioskId.kioskId);
                    break;

                case FC.LIST_KIOSKS_CASH_DISPENSER_LCDM_HOPPER:
                    InputListMutabakat listKioskCashDispenserLCDMHopper = JsonConvert.DeserializeObject<InputListMutabakat>(input);
                    data = SearchKioskCashDispenserLCDMHopper(listKioskCashDispenserLCDMHopper.StartDate
                        , listKioskCashDispenserLCDMHopper.EndDate
                        , listKioskCashDispenserLCDMHopper.NumberOfItemsPerPage
                        , listKioskCashDispenserLCDMHopper.CurrentPageNum
                        , listKioskCashDispenserLCDMHopper.RecordCount
                        , listKioskCashDispenserLCDMHopper.PageCount
                        , listKioskCashDispenserLCDMHopper.OrderSelectionColumn
                        , listKioskCashDispenserLCDMHopper.DescAsc
                        , listKioskCashDispenserLCDMHopper.InstutionId
                        , listKioskCashDispenserLCDMHopper.KioskIds
                        , listKioskCashDispenserLCDMHopper.OrderType);
                    break;


                case FC.CONTROL_CASH_NOTIFICATION:
                    InputSaveControlCashNotification controlcashnotification = JsonConvert.DeserializeObject<InputSaveControlCashNotification>(input);
                    data = ControlCashNotification(controlcashnotification.SearchText,
                        controlcashnotification.StartDate
                        , controlcashnotification.EndDate
                        , controlcashnotification.NumberOfItemsPerPage
                        , controlcashnotification.CurrentPageNum
                        , controlcashnotification.RecordCount
                        , controlcashnotification.PageCount
                        , controlcashnotification.OrderSelectionColumn
                        , controlcashnotification.DescAsc
                        , controlcashnotification.InstutionId
                        , controlcashnotification.KioskIds
                        , controlcashnotification.OrderType
                        , controlcashnotification.StatusId
                        , controlcashnotification.Kioskfullfillstatus
                        );
                    break;

                case FC.GET_CONTROL_CASH_NOTIFICATION:
                    InputSaveControlCashNotification controlCashNotification = JsonConvert.DeserializeObject<InputSaveControlCashNotification>(input);
                    data = GetControlCashNotification(controlCashNotification.ControlCashId);
                    break;


                case FC.CUT_OFF_MONITORING:
                    InputSaveControlCashNotification cutofmonitoring = JsonConvert.DeserializeObject<InputSaveControlCashNotification>(input);
                    data = CutOffMonitoring(cutofmonitoring.SearchText,
                          cutofmonitoring.StartDate
                        , cutofmonitoring.EndDate
                        , cutofmonitoring.NumberOfItemsPerPage
                        , cutofmonitoring.CurrentPageNum
                        , cutofmonitoring.RecordCount
                        , cutofmonitoring.PageCount
                        , cutofmonitoring.OrderSelectionColumn
                        , cutofmonitoring.DescAsc
                        , cutofmonitoring.InstutionId
                        , cutofmonitoring.KioskIds
                        , cutofmonitoring.OrderType
                        );
                    break;

                case FC.GET_CUT_KIOSK:
                    InputGetCutofMonitoring cutkiosk = JsonConvert.DeserializeObject<InputGetCutofMonitoring>(input);
                    data = GetCutOffMonitoring(cutkiosk.cutId);
                    break;

                case FC.SAVE_CUT_CASH_OFF_MONITORING:
                    InputListSaveCutOffMonitoring inputListSaveCutCashOffMonitoring = JsonConvert.DeserializeObject<InputListSaveCutOffMonitoring>(input);
                    data = SaveCutOffMonitoring(inputListSaveCutCashOffMonitoring);
                    break;

                case FC.UPDATE_CUT_CASH_OFF_MONITORING:
                    InputListSaveCutOffMonitoring inputListUpdateCutCashOffMonitoring = JsonConvert.DeserializeObject<InputListSaveCutOffMonitoring>(input);
                    data = UpdateCutOffMonitoring(inputListUpdateCutCashOffMonitoring);
                    break;

                case FC.BANK_ACCOUNT_ACTION:
                    InputListMutabakat bankaccount = JsonConvert.DeserializeObject<InputListMutabakat>(input);
                    data = BankAccountAction(bankaccount.SearchText
                        , bankaccount.StartDate
                        , bankaccount.EndDate
                        , bankaccount.NumberOfItemsPerPage
                        , bankaccount.CurrentPageNum
                        , bankaccount.RecordCount
                        , bankaccount.PageCount
                        , bankaccount.OrderSelectionColumn
                        , bankaccount.DescAsc
                        , bankaccount.OrderType

                        );
                    break;

                case FC.ENTER_KIOSK_EMPTY_CASH_COUNT:
                    InputListMutabakat enterekioskemptycashcount = JsonConvert.DeserializeObject<InputListMutabakat>(input);
                    data = EntereKioskEmptyCashCount(enterekioskemptycashcount.SearchText,
                        enterekioskemptycashcount.StartDate
                        , enterekioskemptycashcount.EndDate
                        , enterekioskemptycashcount.NumberOfItemsPerPage
                        , enterekioskemptycashcount.CurrentPageNum
                        , enterekioskemptycashcount.RecordCount
                        , enterekioskemptycashcount.PageCount
                        , enterekioskemptycashcount.OrderSelectionColumn
                        , enterekioskemptycashcount.DescAsc
                        // , controlcashnotification.InstutionId
                        , enterekioskemptycashcount.KioskIds
                        , enterekioskemptycashcount.EnteredUser
                        , enterekioskemptycashcount.OperatorUser
                        , enterekioskemptycashcount.Status

                        );
                    break;

                case FC.EXCEL_LIST_DAILY_ACCOUNT_REPORTS_PROKIS:
                    InputListMutabakat listDailyAccounReportsProkisForExcel = JsonConvert.DeserializeObject<InputListMutabakat>(input);
                    data = SearchDailyAccounReportsProkisForExcel(listDailyAccounReportsProkisForExcel.StartDate
                        , listDailyAccounReportsProkisForExcel.EndDate
                        , listDailyAccounReportsProkisForExcel.OrderSelectionColumn
                        , listDailyAccounReportsProkisForExcel.DescAsc);
                    break;
                case FC.GET_BASKENT_MUTABAKAT_DATA:
                    InputListGunsonuMutabakat parser = JsonConvert.DeserializeObject<InputListGunsonuMutabakat>(input);
                    data = MakeBaskentGazMutabakatOperation(parser.mutabakatDate, parser.kioskId, parser.instutionId, parser.adminUserId);
                    data.Add("KioskInfos", GetKioskInfo(Convert.ToInt32(parser.kioskId), Convert.ToDateTime(parser.mutabakatDate), Convert.ToDateTime(parser.mutabakatDate).AddDays(1)));
                    data.Add("DbDatas", GetMutabakatDB(parser.mutabakatDate, parser.kioskId, parser.instutionId, parser.adminUserId, parser.OrderType));
                    break;
                case FC.GET_ASKI_MUTABAKAT_DATA:
                    InputListGunsonuMutabakat parserAski = JsonConvert.DeserializeObject<InputListGunsonuMutabakat>(input);
                    data = MakeAskiMutabakatOperation(parserAski.mutabakatDate, parserAski.kioskId, parserAski.instutionId, parserAski.adminUserId);
                    data.Add("DbDatas", GetMutabakatDB(parserAski.mutabakatDate, parserAski.kioskId, parserAski.instutionId, parserAski.adminUserId, parserAski.OrderType));
                    break;

                case FC.GET_ASKI_DB_MUTABAKAT_DATA:
                    InputListGunsonuMutabakat parserAskiDB = JsonConvert.DeserializeObject<InputListGunsonuMutabakat>(input);
                    data = MakeDBAskiMutabakatOperation(parserAskiDB.mutabakatDate, parserAskiDB.kioskId, parserAskiDB.instutionId, parserAskiDB.adminUserId);
                    data.Add("DbDatas", GetMutabakatDB(parserAskiDB.mutabakatDate, parserAskiDB.kioskId, parserAskiDB.instutionId, parserAskiDB.adminUserId, parserAskiDB.OrderType));
                    break;

                case FC.GET_BASKENT_DB_MUTABAKAT_DATA:
                    InputListGunsonuMutabakat parserBaskentGAZDB = JsonConvert.DeserializeObject<InputListGunsonuMutabakat>(input);
                    data = MakeDBBaskentGAZMutabakatOperation(parserBaskentGAZDB.mutabakatDate, parserBaskentGAZDB.kioskId, parserBaskentGAZDB.instutionId, parserBaskentGAZDB.adminUserId);
                    data.Add("DbDatas", GetMutabakatDB(parserBaskentGAZDB.mutabakatDate, parserBaskentGAZDB.kioskId, parserBaskentGAZDB.instutionId, parserBaskentGAZDB.adminUserId, parserBaskentGAZDB.OrderType));
                    break;

                case FC.LIST_AUTO_MUTABAKAT_TRANSACTION:
                    InputListAutoMutabakat listTransactions = JsonConvert.DeserializeObject<InputListAutoMutabakat>(input);
                    data = SearchAutoMutabakatTransactions(listTransactions.SearchText
                        , listTransactions.StartDate
                        , listTransactions.EndDate
                        , listTransactions.NumberOfItemsPerPage
                        , listTransactions.CurrentPageNum
                        , listTransactions.RecordCount
                        , listTransactions.PageCount
                        , listTransactions.OrderSelectionColumn
                        , listTransactions.DescAsc
                        , listTransactions.UserId
                        , listTransactions.AdminUserId.ToString()
                        , listTransactions.InstutionId
                        , listTransactions.State
                        , listTransactions.OrderType);
                    break;

                case FC.LIST_STATUS:
                    data = GetStatus();
                    break;

                case FC.LIST_POSEOD:
                    InputListKioskFillCashBox listPOSEOD = JsonConvert.DeserializeObject<InputListKioskFillCashBox>(input);
                    data = SearchPOSEOD(listPOSEOD.kioskId
                        , listPOSEOD.StartDate
                        , listPOSEOD.EndDate
                        , listPOSEOD.NumberOfItemsPerPage
                        , listPOSEOD.CurrentPageNum
                        , listPOSEOD.RecordCount
                        , listPOSEOD.PageCount
                        , listPOSEOD.OrderSelectionColumn
                        , listPOSEOD.DescAsc
                        , listPOSEOD.OrderType);
                    break;

                case FC.GET_MASKI_MUTABAKAT_DATA:
                    InputListGunsonuMutabakat parserMaski = JsonConvert.DeserializeObject<InputListGunsonuMutabakat>(input);
                    data = MakeMaskiMutabakatOperation(parserMaski.mutabakatDate, parserMaski.kioskId, parserMaski.instutionId, parserMaski.adminUserId);
                    data.Add("DbDatas", GetMutabakatDB(parserMaski.mutabakatDate, parserMaski.kioskId, parserMaski.instutionId, parserMaski.adminUserId, parserMaski.OrderType));
                    break;

                case FC.COMPLETE_KIOSK_INSTUTION_MUTABAKAT:
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("girdi.whichSide = 0"), "MakeBaskentGazMutabakatOperation11");

                    InputCompleteKioskInstutionMutabakat parserInputCompleteKioskInstutionMutabakat = JsonConvert.DeserializeObject<InputCompleteKioskInstutionMutabakat>(input);

                    if (parserInputCompleteKioskInstutionMutabakat.whichSide == "0")
                    {
                        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("parserInputCompleteKioskInstutionMutabakat.whichSide = 0"), "MakeBaskentGazMutabakatOperation11");
                        data = CompleteBaskentGazMutabakatOperation(parserInputCompleteKioskInstutionMutabakat.mutabakatDate,
                                                                    parserInputCompleteKioskInstutionMutabakat.kioskId,
                                                                    parserInputCompleteKioskInstutionMutabakat.totalCount,
                                                                    parserInputCompleteKioskInstutionMutabakat.totalAmount,
                                                                    0,
                                                                    "0");
                    }
                    else
                    {
                        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("parserInputCompleteKioskInstutionMutabakat.whichSide = 1"), "MakeBaskentGazMutabakatOperation11");

                        data = CompleteBaskentGazMutabakatOperation(parserInputCompleteKioskInstutionMutabakat.mutabakatDate,
                                                                    parserInputCompleteKioskInstutionMutabakat.kioskId,
                                                                    parserInputCompleteKioskInstutionMutabakat.instutionCount,
                                                                    parserInputCompleteKioskInstutionMutabakat.instutionAmount,
                                                                    Convert.ToInt32(parserInputCompleteKioskInstutionMutabakat.cancelCount),
                                                                    parserInputCompleteKioskInstutionMutabakat.cancelAmount);
                    }
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("CompleteBaskentGazMutabakatOperation bitti"), "MakeBaskentGazMutabakatOperation11");

                    int returnCode = -1;
                    AccountOpr opr = new AccountOpr();
                    JObject ret = opr.SaveMutabakatResult(parserInputCompleteKioskInstutionMutabakat.userId
                                                        , parserInputCompleteKioskInstutionMutabakat.mutabakatDate.ToString()
                                                        , parserInputCompleteKioskInstutionMutabakat.kioskId
                                                        , parserInputCompleteKioskInstutionMutabakat.instutionId
                                                        , parserInputCompleteKioskInstutionMutabakat.faturaCount
                                                        , parserInputCompleteKioskInstutionMutabakat.faturaAmount
                                                        , parserInputCompleteKioskInstutionMutabakat.kartDolumCount
                                                        , parserInputCompleteKioskInstutionMutabakat.kartDolumAmount
                                                        , parserInputCompleteKioskInstutionMutabakat.totalCount
                                                        , parserInputCompleteKioskInstutionMutabakat.totalAmount
                                                        , parserInputCompleteKioskInstutionMutabakat.instutionCount
                                                        , parserInputCompleteKioskInstutionMutabakat.instutionAmount
                                                        , parserInputCompleteKioskInstutionMutabakat.whichSide
                                                        , data.ToString()
                                                        , out returnCode);

                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("SaveMutabakatResult bitti"), "MakeBaskentGazMutabakatOperation11");

                    data.Add("mutabakatDbResult", ret);
                    break;
                default:
                    break;
            }

            ManageContextRolescs contextRoles = new ManageContextRolescs();
            contextRoles.EditByRoles(functionCode, data, adminUserId, pageUrl);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "PrepareResponseAccount");
        }

        return data;
    }




    private JArray GetMutabakatDBList(DateTime mutabakatDate, Int64 kioskId, int instutionId, Int64 adminUserId)
    {

        MutabakatListCollection items = null;
        try
        {
            AccountOpr opr = new AccountOpr();
            items = opr.GetMutabakatListValues(mutabakatDate, kioskId, instutionId, adminUserId);
            
            opr.Dispose();
            opr = null;
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetMutabakatDBList");
        }

        return items.GetJSONJArray();
    }


    private JObject CompleteBaskentGazMutabakatOperation(DateTime tarih, Int64 kioskId, int tahsilAdet, string tahsilTutar, int iptalAdet, string iptalTutar)
    {
        try
        {
            CultureInfo culture = new CultureInfo("tr-TR");
            tahsilTutar = tahsilTutar.Replace('.', ',');
            iptalTutar = iptalTutar.Replace('.', ',');

            string serializedData = "";

            BGaz.KIOSK client = new BGaz.KIOSK();
            BGaz.MutabakatVerme ts = new BGaz.MutabakatVerme();
            client.Url = Utility.GetConfigValue("BGaz.KIOSK");

            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(client.Url), "CBGMO01");
            BGaz.KullaniciGiris kg = new BGaz.KullaniciGiris();
            WebMethodAttribute a = new WebMethodAttribute();
            a.EnableSession = true;
            client.CookieContainer = new CookieContainer();
            AccountOpr opr = new AccountOpr();
            string userName = "";
            string password = "";
            bool resp = false;
            //opr.GetKioskUserPassWithId(kioskId, ref userName, ref password);
            if (resp == true)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("GetKioskUserPassWithId returned true ;)"), "CBGMO02");
                kg = client.KullaniciGirisi(userName, password);
            }
            else
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("GetKioskUserPassWithId returned false :("), "CBGMO03");
                kg = client.KullaniciGirisi(Utility.GetConfigValue("BaskentUserValue"), Utility.GetConfigValue("BaskentPassValue"));
            }
            ts = client.MutabakatSaglama(tarih.Year + "-" + tarih.Month.ToString().PadLeft(2, '0') + "-" + tarih.Day.ToString().PadLeft(2, '0'), tahsilAdet, Convert.ToDecimal(tahsilTutar, culture), iptalAdet, Convert.ToDecimal(iptalTutar, culture));
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(JsonConvert.SerializeObject(ts).ToString() + tarih.ToString()), "CBGMO04");

            //'MUTABAKAT BAŞARILI' yada 'MUTABAKAT BAŞARISIZ' 
            if (ts != null && (ts.Durumu == "BAŞARILI" && ts.MutabakatSonucu == "MUTABAKAT BAŞARILI"))
            {

                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("MUTABAKAT BAŞARILI'"), "CBGMO05");
                serializedData = JsonConvert.SerializeObject(ts);
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(serializedData), "CBGMO06");

                errorCode = ReturnCodes.MUTABAKAT_BASARILI;
            }
            else
            {
                errorCode = ReturnCodes.MUTABAKAT_BASARISIZ;
            }
            //if (ts != null) data.Add("message", ts.MutabakatSonucu); else data.Add("message", "null");
            data.Add("message", ts.Mesaj);

            if (string.IsNullOrEmpty(serializedData))
                serializedData = "{}";
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("will return serialized data"), "CBGMO07");
            return JObject.Parse(serializedData);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            return data;
        }
    }

    private JObject MakeBaskentGazMutabakatOperation(DateTime tarih, Int64 kioskId, int instutionId, Int64 adminUserId)
    {
        try
        {
            string serializedData = "";


            DBBaskentGAZSAP.BaskentDogalgazCardLoadSoapClient DBBaskentGAZ = new DBBaskentGAZSAP.BaskentDogalgazCardLoadSoapClient();
            DBBaskentGAZSAP.DT_BI_ES_KurumIslemListe_Res result = DBBaskentGAZ.MutabakatSaglamaGunSonuIslemleriSAPRapor(Convert.ToDateTime(tarih.ToString("yyyy-MM-dd")), "", "700");
            //DBBaskentGAZ.GunSonuFaturaSorgu result = DBBaskentGAZ.BaskentDogalgazGunsonuIslemRaporu(tarih.ToString("dd.MM.yyyy"), userName, password);

            if (result != null && result.EV_RETURN_CODE == "0")
            {
                if (result.ET_VEZNE != null) //Başarılı
                {
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("4"), "MakeBaskentGazMutabakatOperation11");
                    serializedData = JsonConvert.SerializeObject(result);
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("5" + " " + serializedData), "MakeBaskentGazMutabakatOperation12");
                }
            }

            if (string.IsNullOrEmpty(serializedData))
                serializedData = "{}";
            //Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("12345"), "MakeBaskentGazMutabakatOperation13");
            return JObject.Parse(serializedData);



        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(exp.ToString()), "MakeBaskentGazMutabakatOperation");

            errorCode = ReturnCodes.SYSTEM_ERROR;
            return data;
        }

    }
    private JObject MakeAskiMutabakatOperation(DateTime tarih, Int64 kioskId, int instutionId, Int64 adminUserId)
    {
        JObject resp = null;
        try
        {
            //tarih.AddDays(1).ToString("dd-MM-yyyy")
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

            string responseText = Utility.ExecuteHttpRequest
                       ("https://bilgi.aski.gov.tr/kartlisayac/kartliservice.asmx?op=SatisListesi",
                       "POST",
                       "text/xml; charset=utf-8",
                       "",
                       System.Text.Encoding.UTF8.GetBytes(GetAskiSatisListesiDBodyData(Utility.GetConfigValue("AskiUser"),
                                                                                       Utility.GetConfigValue("AskiPass"),
                                                                                       tarih.ToString("dd-MM-yyyy"),
                                                                                       tarih.ToString("dd-MM-yyyy"))),
                                                                                       "", ref statusCode);
 
     
         
            //Log.Debug("CPO84 - AskiLoading Operasyonu bitti. Sonuç : " + responseText);

            if (statusCode == System.Net.HttpStatusCode.OK)
            {
                //todo efe2

                resp = Utility.ConvertXML2Json(responseText);
                //Utility.WriteErrorLog(
                //   Utility.GetConfigValue("ErrorLogPath"),
                //   new Exception("resp:" + resp), "XX");

                //var dd = JsonConvert.DeserializeObject<AskiMutabakat>(responseText);

                //Utility.WriteErrorLog(
                //    Utility.GetConfigValue("ErrorLogPath"),
                //    new Exception("dd:" + dd), "XX");

                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
                errorCode = ReturnCodes.NO_DATA_FOUND;
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
        }
        return resp;
    }
    private string MakeAskiMutabakatOperationToString(DateTime tarih, Int64 kioskId, int instutionId, Int64 adminUserId)
    {
        string responseText = "";
        try
        {
            //tarih.AddDays(1).ToString("dd-MM-yyyy")
            HttpStatusCode statusCode = HttpStatusCode.Accepted;

             responseText = Utility.ExecuteHttpRequest
                       ("https://bilgi.aski.gov.tr/kartlisayac/kartliservice.asmx?op=SatisListesi",
                       "POST",
                       "text/xml; charset=utf-8",
                       "",
                       System.Text.Encoding.UTF8.GetBytes(GetAskiSatisListesiDBodyData(Utility.GetConfigValue("AskiUser"),
                                                                                       Utility.GetConfigValue("AskiPass"),
                                                                                       tarih.ToString("dd-MM-yyyy"),
                                                                                       tarih.ToString("dd-MM-yyyy"))),
                                                                                       "", ref statusCode)??"";



            //Log.Debug("CPO84 - AskiLoading Operasyonu bitti. Sonuç : " + responseText);

            if (statusCode == System.Net.HttpStatusCode.OK)
            {
                errorCode = ReturnCodes.SUCCESSFULL;

                return responseText;
                
            }
            else
                errorCode = ReturnCodes.NO_DATA_FOUND;
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
        }
        return responseText;
    }



    private JObject MakeAskiDBListMutabakatOperation(DateTime tarih, Int64 kioskId, int instutionId, Int64 adminUserId)
    {
        //JObject resp = null;
        try
        {
            DBAskiServices.AskiCardLoadSoapClient dbAskiServ = new DBAskiServices.AskiCardLoadSoapClient();
            DBAskiServices.PN_AssociationReconciliationInfo response = dbAskiServ.AskiCreditLoadingConfirmationReconciliation(tarih.Date, "D");


            if (response != null)
            {
                List<PRCNCORE.BankActions.DBAskiActionsFromServiceDetail> dataList = new List<PRCNCORE.BankActions.DBAskiActionsFromServiceDetail>();

                foreach (var dd in response.ReconDetailItems)
                {
                    PRCNCORE.BankActions.DBAskiActionsFromServiceDetail detail = new PRCNCORE.BankActions.DBAskiActionsFromServiceDetail();
                    detail.bankReferenceNumber = dd.SubscriberNumer.ToString();
                    detail.insProcessDate = dd.TransactionDate.ToString();
                    detail.institutionAmount = dd.PaymentAmount;
                    detail.status = dd.RecordStatus;
                    dataList.Add(detail);
                }

                string convertToJSON = new JavaScriptSerializer().Serialize(dataList);

                data.Add("Response", convertToJSON);
                errorCode = ReturnCodes.SUCCESSFULL;

                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("MakeAskiDBListMutabakatOperation Success:" + convertToJSON), "MADBL02");

            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("MakeAskiDBListMutabakatOperation Fail:"), "MADBL03");
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("MakeAskiDBListMutabakatOperation Exception:" + exp.ToString()), "MADBL01");

        }
        return data;
    }

    private JObject MakeBgazDBListMutabakatOperation(DateTime tarih, Int64 kioskId, int instutionId, Int64 adminUserId)
    {
        //JObject resp = null;
        try
        {

            DBBaskentGAZSAP.BaskentDogalgazCardLoadSoapClient dbBgazServ = new DBBaskentGAZSAP.BaskentDogalgazCardLoadSoapClient();
            DBBaskentGAZSAP.PN_AssociationReconciliationInfo response = dbBgazServ.BaskentLoadingConfirmationReconciliation(tarih.Date, "D");

            if (response != null)
            {
                List<PRCNCORE.BankActions.DBAskiActionsFromServiceDetail> dataList = new List<PRCNCORE.BankActions.DBAskiActionsFromServiceDetail>();

                foreach (var dd in response.ReconDetailItems)
                {
                    PRCNCORE.BankActions.DBAskiActionsFromServiceDetail detail = new PRCNCORE.BankActions.DBAskiActionsFromServiceDetail();
                    detail.bankReferenceNumber = dd.SubscriberNumer.ToString();
                    detail.insProcessDate = dd.TransactionDate.ToString();
                    detail.institutionAmount = dd.PaymentAmount;
                    detail.status = dd.RecordStatus;
                    dataList.Add(detail);
                }

                string convertToJSON = new JavaScriptSerializer().Serialize(dataList);

                data.Add("Response", convertToJSON);
                errorCode = ReturnCodes.SUCCESSFULL;

                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("MakeAskiDBListMutabakatOperation Success:" + convertToJSON), "MADBL02");

            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("MakeAskiDBListMutabakatOperation Fail:"), "MADBL03");
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("MakeAskiDBListMutabakatOperation Exception:" + exp.ToString()), "MADBL01");

        }
        return data;
    }

    private JObject MakeDBAskiMutabakatOperation(DateTime tarih, Int64 kioskId, int instutionId, Int64 adminUserId)
    {
        try
        {
            DBAskiServices.AskiCardLoadSoapClient dbAskiServ = new DBAskiServices.AskiCardLoadSoapClient();
            DBAskiServices.PN_AssociationReconciliationInfo response = dbAskiServ.AskiCreditLoadingConfirmationReconciliation(tarih.Date, "G");

            if (response != null)
            {
                data.Add("ResponseCode", response.ResponseCode);
                data.Add("ErrorMessage", response.ErrorMessage);
                data.Add("PaymentTotalCount", response.PaymentTotalCount);
                data.Add("PaymentTotalAmount", response.PaymentTotalAmount);
                data.Add("PaymentCancelCount", response.CancelPaymentTotalCount);
                data.Add("PaymentCancelAmount", response.CancelPaymentTotalAmount);

                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;

            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("DB Aski Mutabakat Exception:" + exp.ToString()), "MBGMO01");
        }
        return data;
    }

    private JObject MakeDBBaskentGAZMutabakatOperation(DateTime tarih, Int64 kioskId, int instutionId, Int64 adminUserId)
    {
        try
        {
            DBBaskentGAZSAP.BaskentDogalgazCardLoadSoapClient dbBgazServ = new DBBaskentGAZSAP.BaskentDogalgazCardLoadSoapClient();
            DBBaskentGAZSAP.PN_AssociationReconciliationInfo response = dbBgazServ.BaskentLoadingConfirmationReconciliation(tarih.Date, "G");

            if (response != null)
            {
                data.Add("ResponseCode", response.ResponseCode);
                data.Add("ErrorMessage", response.ErrorMessage);
                data.Add("PaymentTotalCount", response.PaymentTotalCount);
                data.Add("PaymentTotalAmount", response.PaymentTotalAmount);
                data.Add("PaymentCancelCount", response.CancelPaymentTotalCount);
                data.Add("PaymentCancelAmount", response.CancelPaymentTotalAmount);

                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;

            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("DB Aski Mutabakat Exception:" + exp.ToString()), "MBGMO01");
        }
        return data;
    }


    private JObject MakeMaskiMutabakatOperation(DateTime tarih, Int64 kioskId, int instutionId, Int64 adminUserId)
    {
        JObject resp = new JObject();
        try
        {
       
            MutabakatSERVICE2.WebServiceMaskiMutabakat.ParserEODDetail result;

            //using (MaskiOperations.CardTransactionSoapClient maskiOps = new MaskiOperations.CardTransactionSoapClient())
            //{
            //    result = maskiOps.GetEODDetails(tarih, tarih, 0, 9999, "bostest", kioskId);
            //}

            result = MutabakatSERVICE2.GetData.GetMaskiMutabakat(tarih, 0, kioskId);
            //Log.Debug("CPO84 - AskiLoading Operasyonu bitti. Sonuç : " + responseText);

            if (result.general != null)
            {
                string serializedData = JsonConvert.SerializeObject(result);
                //data.Add("InvoiceData", JObject.Parse(serializedData));

                resp.Add("paymentTotalAmount", result.general.IslemTutar);
                resp.Add("paymentCancelAmount", 0);
                resp.Add("paymentTotalCount", result.general.IslemAdet);
                resp.Add("paymentCancelCount", 0);
                //resp = JObject.Parse(serializedData);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
                errorCode = ReturnCodes.NO_DATA_FOUND;
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
        }
        return resp;
    }

    private JObject MakeMutabakatComparison(DateTime tarih, Int64 kioskId, int instutionId, Int64 adminUserId, int processTypeId)
    {
        //var m = MakeAskiMutabakatOperationToString(tarih, kioskId, instutionId, adminUserId);
       
        JObject resp = new JObject();
        try
        {
            MutabakatCompare compare = new MutabakatCompare();
            int InstitutionSuccessTxnNumber = 0;
            decimal InstitutionSuccessTxnAmount = 0;

            int InstitutionFailTxnNumber = 0;
            decimal InstitutionFailTxnAmount = 0;

            int BOSSuccessTxnNumber = 0;
            decimal BOSSuccessTxnAmount = 0;

            int BOSFailTxnNumber = 0;
            decimal BOSFailTxnAmount = 0;

            AccountOpr opr = new AccountOpr();
            
            // BOS Rakamlarını kurum id ye göre getiriyor.
            MutabakatListInvoiceCollection items = opr.GetMutabakatListValuesInvoice(tarih, kioskId, instutionId, adminUserId,processTypeId,0,0);

            List<MutabakatCompareDetails> detailListBOSSuccess = new List<MutabakatCompareDetails>();
            List<MutabakatCompareDetails> detailListBOSFail = new List<MutabakatCompareDetails>();

            List<MutabakatCompareDetails> detailListSuccess = new List<MutabakatCompareDetails>();
            List<MutabakatCompareDetails> detailListFail = new List<MutabakatCompareDetails>();

            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("MakeMutabakatComparison başatılıyor"), "11");

            //BOS rakamları başarılı ve başarısız olarak iki listeye ayrılıyor.
            foreach (var item in items)
            {
                MutabakatCompareDetails detail = new MutabakatCompareDetails();
                detail.ReferenceId = item.BankReferenceNumber.ToString();
                detail.Amount = item.Amount.ToString();
                detail.InvoiceNumber = item.FaturaIds.ToString();
                detail.InsertionDate = item.InsertionDate.ToString();
                detail.Status = item.Status.ToString();
                detail.CustomerName = "";
                detail.CustomerId = item.AboneNo.ToString();
                detail.TransactionId = item.TransactionId.ToString();
                detail.ProcessResult = item.CompleteStatus.ToString();
                detail.CompareResult = "0";
                if(item.Status>0)
                {
                    BOSSuccessTxnAmount = BOSSuccessTxnAmount + Convert.ToDecimal(item.Amount);
                    BOSSuccessTxnNumber++;

                    detailListBOSSuccess.Add(detail);
                }
                else
                {
                    BOSFailTxnAmount = BOSFailTxnAmount + Convert.ToDecimal(item.Amount);
                    BOSFailTxnNumber++;

                    detailListBOSFail.Add(detail);
                }
            }

            //Maski rakamaı web servis üzerinden alınıyor
            if (instutionId == (int)Institutions.Maski)
            {

                MutabakatSERVICE2.WebServiceMaskiMutabakat.ParserEODDetail result;
                using (MutabakatSERVICE2.WebServiceMaskiMutabakat.CardTransactionSoapClient maskiOps = new MutabakatSERVICE2.WebServiceMaskiMutabakat.CardTransactionSoapClient())
                {
                    //GetMaskiMutabakat
                    result = MutabakatSERVICE2.GetData.GetMaskiMutabakat(tarih,  1,  kioskId);

                    //result = maskiOps.GetEODDetails(tarih, tarih, 1, 9999, "bostest", kioskId);
                }

                
                if (result.details != null)
                {
                    //Maski rakamını başarılı başarısız olarak iki listeye topluyor
                    foreach (var dd in result.details)
                    {
                        MutabakatCompareDetails detail = new MutabakatCompareDetails();
                        detail.ReferenceId = dd.SessionId.ToString();
                        detail.Amount = dd.Tutar.ToString();
                        detail.InvoiceNumber = dd.SessionId.ToString();
                        detail.InsertionDate = dd.IslemTarihi.ToString();
                        detail.Status = dd.IslemDurumu.ToString();
                        detail.CustomerName = "";
                        detail.CompareResult = "0";

                        if (dd.IslemDurumu == 1)
                        {
                            InstitutionSuccessTxnAmount = InstitutionSuccessTxnAmount + Convert.ToDecimal(dd.Tutar); //todo efe convert double to decimal
                            InstitutionSuccessTxnNumber++;

                            detailListSuccess.Add(detail);
                        }
                        else
                        {
                            InstitutionFailTxnAmount = InstitutionFailTxnAmount + Convert.ToDecimal(dd.Tutar);//todo efe convert double to decimal;
                            InstitutionFailTxnNumber++;

                            detailListFail.Add(detail);
                        }
                    }

                }

            }
            else if (instutionId == (int)Institutions.BaskentGAZ)
            {

                if (processTypeId == 2)
                {
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("process type id:"), "1");

                    DetailReconciliationResponse res = null;
                    try
                    {
                        //var tempProtocol = ServicePointManager.SecurityProtocol;
                        //Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("process type id:" + ServicePointManager.SecurityProtocol), "X1");

                        //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("process type id:" + ServicePointManager.SecurityProtocol), "X2");


                        res = MutabakatSERVICE2.GetData.GetAkbankMutabakatDetail(tarih, MutabakatSERVICE2.GetData.AkbankKurum.BGaz);
                        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("process type id:" + ServicePointManager.SecurityProtocol), "X3");


                        //ServicePointManager.SecurityProtocol = tempProtocol;


                        //Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Authentication.");

                        //var auth = new Authentication
                        //{
                        //    UserName = "URF_1011",
                        //    Password = "gaKyoAJ3tCuqQZ5fNDcm" //"dRxyRbUHH71mdMplTu0Y"
                        //};
                        //Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "DetailReconciliationRequest");

                        //var detail =
                        //  new DetailReconciliationRequest
                        //  {
                        //      BasTarihi = tarih.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture),
                        //      BitTarihi = tarih
                        //                .AddDays(1)
                        //                .ToString("yyyy-MM-dd", CultureInfo.InvariantCulture),
                        //      FirmaKoduSpecified = true
                        //  };
                        //Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "firmaKodu");
                        //detail.FirmaKodu = 6401000;
                      
                        //Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "akbankAskiClient");

                        //for (var i = 0; i < 2; i++)
                        //{
                        //    try
                        //    {
                        //        using (var akbankAskiClient = new CollectionService())
                        //        {

                        //            var tempProtocol = ServicePointManager.SecurityProtocol;
                        //            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
 

                        //            var askiResult = akbankAskiClient.NYDetailReconciliation(
                        //              auth,
                        //              detail);
                        //            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "askiResult");

                        //            ServicePointManager.SecurityProtocol = tempProtocol;

                        //            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Successfully got results from the API.");
                        //            res = askiResult;
                        //        }
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Failed to get results from the API.");
                        //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), ex.Message);
                        //    }

                        //    if (i == 0) Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Retrying...");
                        //    Thread.Sleep(100);
                        //}


                         


                    }
                    catch (Exception ex)
                    {
                        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("exceptionnnn:", ex), "3");

                    }

                    //Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("process type id:"), new JavaScriptSerializer().Serialize(res));

                    if (res != null)
                    {
                        if (res.DetailReconciliation != null)
                        {
                            foreach (var dd in res.DetailReconciliation.CollectionTypeDetails)
                            {
                                foreach (var dd2 in dd.CollectionDetails)
                                {
                                    MutabakatCompareDetails detail = new MutabakatCompareDetails();
                                    //detail.ReferenceId = dd.ZZGUID.ToString();
                                    detail.ReferenceId = dd2.FomTahsilatRefId;
                                    
                             

                                    detail.Amount = dd2.TahsilatTutari.ToString();
                                  
                                    detail.InvoiceNumber = dd2.FaturaNo.ToString();
                                    detail.InsertionDate = dd2.TahsilatTarihi.ToString();
                                    detail.Status = string.IsNullOrEmpty(dd2.IptalTarihi.ToString()) ? "1" : "0";
                                    detail.CustomerName = "";
                                    detail.CompareResult = "0";

                                    if (detail.Status == "0")
                                    {
                                        InstitutionSuccessTxnAmount = InstitutionSuccessTxnAmount + Convert.ToDecimal(dd2.TahsilatTutari);
                                        InstitutionSuccessTxnNumber++;

                                        detailListSuccess.Add(detail);
                                    }
                                    else
                                    {
                                        InstitutionFailTxnAmount = InstitutionFailTxnAmount + Convert.ToDecimal(dd2.TahsilatTutari);
                                        InstitutionFailTxnNumber++;

                                        detailListFail.Add(detail);
                                    }

                                }

                            }
                        }
                    }
                    //todo efe
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("processtype2:" + new JavaScriptSerializer().Serialize(res)), "8");

                }
                else
                {

                    BaskentGAZFibabank.DT_BI_ES_KurumIslemListe_K_Res result;

                    BaskentGAZFibabank.DT_BI_ES_KurumIslemListe_K_Req request = new BaskentGAZFibabank.DT_BI_ES_KurumIslemListe_K_Req();
                   
                    using (BaskentGAZFibabank.SI_BI_ES_KurumIslemListe_K_OBClient client = new BaskentGAZFibabank.SI_BI_ES_KurumIslemListe_K_OBClient())
                    {

                        string userName = "BNKFIBA";
                        string password = "Aa1234**";

                        client.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(userName, password);
                        client.ClientCredentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Identification;
                        client.ClientCredentials.UserName.Password = password;
                        client.ClientCredentials.UserName.UserName = userName;

                        request.IV_DATUM = tarih.ToString("yyyy-MM-dd"); //tarih.ToString("yyyy -MM-dd");
                        request.IV_DETAY = "X";
                        request.IV_ISLEM = "1";
                        request.IV_KURUM = "900";

                        result = client.SI_BI_ES_KurumIslemListe_K_OB(request);

                    }
                    if (result.ET_DETAY != null)
                    {
                        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("DT_BI_ES_KurumIslemListe_K_Res başatılıyor "+result.ET_DETAY.Count()), "12");

                        foreach (var dd in result.ET_DETAY)
                        {

                            MutabakatCompareDetails detail = new MutabakatCompareDetails();
                            detail.ReferenceId = dd.ZZGUID.ToString();

                            detail.Amount = dd.TUTAR.ToString();

                            detail.InvoiceNumber = dd.FATURA.ToString();
                            detail.InsertionDate = dd.ERDAT.ToString()+" "+dd.ERZET;
                            detail.Status = string.IsNullOrEmpty(dd.IPTAL.ToString()) ? "1" : "0";
                            detail.CustomerName = "";
                            detail.CompareResult = "0";

                            if (detail.Status == "1")
                            {
                                InstitutionSuccessTxnAmount = InstitutionSuccessTxnAmount + Convert.ToDecimal(dd.TUTAR);
                                InstitutionSuccessTxnNumber++;

                                detailListSuccess.Add(detail);
                            }
                            else
                            {
                                InstitutionFailTxnAmount = InstitutionFailTxnAmount + Convert.ToDecimal(dd.TUTAR);
                                InstitutionFailTxnNumber++;

                                detailListFail.Add(detail);
                            }
                        }
                        //Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("processtype3:" + new JavaScriptSerializer().Serialize(result)), "8");

                    }
                }

            }

            else if (instutionId == (int)Institutions.Aski)
            {

                if (processTypeId == 2)
                {
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("process type id:"), "1");

                    DetailReconciliationResponse res = null;
                    try
                    {
                        //var tempProtocol = ServicePointManager.SecurityProtocol;
                        //Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("process type id:" + ServicePointManager.SecurityProtocol), "X1");

                        //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("process type id:" + ServicePointManager.SecurityProtocol), "X2");





                        res = MutabakatSERVICE2.GetData.GetAkbankMutabakatDetail(tarih, MutabakatSERVICE2.GetData.AkbankKurum.Aski);
                        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("process type id:" + ServicePointManager.SecurityProtocol), "X3");


                        //ServicePointManager.SecurityProtocol = tempProtocol;


                        //Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Authentication.");

                        //var auth = new Authentication
                        //{
                        //    UserName = "URF_1011",
                        //    Password = "gaKyoAJ3tCuqQZ5fNDcm" //"dRxyRbUHH71mdMplTu0Y"
                        //};
                        //Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "DetailReconciliationRequest");

                        //var detail =
                        //  new DetailReconciliationRequest
                        //  {
                        //      BasTarihi = tarih.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture),
                        //      BitTarihi = tarih
                        //                .AddDays(1)
                        //                .ToString("yyyy-MM-dd", CultureInfo.InvariantCulture),
                        //      FirmaKoduSpecified = true
                        //  };
                        //Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "firmaKodu");
                        //detail.FirmaKodu = 6401000;

                        //Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "akbankAskiClient");

                        //for (var i = 0; i < 2; i++)
                        //{
                        //    try
                        //    {
                        //        using (var akbankAskiClient = new CollectionService())
                        //        {

                        //            var tempProtocol = ServicePointManager.SecurityProtocol;
                        //            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;


                        //            var askiResult = akbankAskiClient.NYDetailReconciliation(
                        //              auth,
                        //              detail);
                        //            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "askiResult");

                        //            ServicePointManager.SecurityProtocol = tempProtocol;

                        //            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Successfully got results from the API.");
                        //            res = askiResult;
                        //        }
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Failed to get results from the API.");
                        //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), ex.Message);
                        //    }

                        //    if (i == 0) Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception(), "Retrying...");
                        //    Thread.Sleep(100);
                        //}





                    }
                    catch (Exception ex)
                    {
                        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("exceptionnnn:", ex), "Aski");

                    }

                    //Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("process type id:"), new JavaScriptSerializer().Serialize(res));

                    if (res != null)
                    {
                        if (res.DetailReconciliation != null)
                        {
                            foreach (var dd in res.DetailReconciliation.CollectionTypeDetails)
                            {
                                foreach (var dd2 in dd.CollectionDetails)
                                {
                                    MutabakatCompareDetails detail = new MutabakatCompareDetails();
                                    //detail.ReferenceId = dd.ZZGUID.ToString();
                                    detail.ReferenceId = dd2.FomTahsilatRefId;



                                    detail.Amount = dd2.TahsilatTutari.ToString();

                                    detail.InvoiceNumber = dd2.FaturaNo.ToString();
                                    detail.InsertionDate = dd2.TahsilatTarihi.ToString();
                                    detail.Status = string.IsNullOrEmpty(dd2.IptalTarihi.ToString()) ? "1" : "0";
                                    detail.CustomerName = "";
                                    detail.CompareResult = "0";

                                    if (detail.Status == "0")
                                    {
                                        InstitutionSuccessTxnAmount = InstitutionSuccessTxnAmount + Convert.ToDecimal(dd2.TahsilatTutari);
                                        InstitutionSuccessTxnNumber++;

                                        detailListSuccess.Add(detail);
                                    }
                                    else
                                    {
                                        InstitutionFailTxnAmount = InstitutionFailTxnAmount + Convert.ToDecimal(dd2.TahsilatTutari);
                                        InstitutionFailTxnNumber++;

                                        detailListFail.Add(detail);
                                    }

                                }

                            }
                        }
                    }
                    //todo efe
                    Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("processtype2:" + new JavaScriptSerializer().Serialize(res)), "8");

                }

                else
                {

                    string askiResponse = MakeAskiMutabakatOperationToString(tarih, kioskId, instutionId, adminUserId);
                    XDocument xml = XDocument.Parse(askiResponse);
                    var soapResponse = xml.Descendants()
                        .Where(x => x.Name.LocalName == "sonuc").Select(x => new AskiSoapMutabakat()
                        {
                            ISLEM_KODU = (string)x.Element(x.Name.Namespace + "ISLEM_KODU"),
                            MAKBUZ_NO = (string)x.Element(x.Name.Namespace + "MAKBUZ_NO"),
                            SYSTARIH = (string)x.Element(x.Name.Namespace + "SYSTARIH"),
                            SATIS_TUTARI_YTL = (string)x.Element(x.Name.Namespace + "SATIS_TUTARI_YTL")
                        }).ToList();

                    if (soapResponse != null)
                    {
                        foreach (var dd in soapResponse)
                        {
                            MutabakatCompareDetails detail = new MutabakatCompareDetails();
                            detail.ReferenceId = dd.MAKBUZ_NO;
                            detail.Amount = dd.SATIS_TUTARI_YTL;
                            detail.InvoiceNumber = dd.MAKBUZ_NO;
                            detail.InsertionDate = dd.SYSTARIH;
                            detail.Status = dd.ISLEM_KODU == "1" ? "1" : "0";//  string.IsNullOrEmpty(dd.ISLEM_KODU.ToString()) ? "1" : "0";
                            detail.CustomerName = "";
                            detail.CompareResult = "0";

                            if (detail.Status == "1")
                            {
                                InstitutionSuccessTxnAmount = InstitutionSuccessTxnAmount + Convert.ToDecimal(dd.SATIS_TUTARI_YTL);
                                InstitutionSuccessTxnNumber++;

                                detailListSuccess.Add(detail);
                            }
                            else
                            {
                                InstitutionFailTxnAmount = InstitutionFailTxnAmount + Convert.ToDecimal(dd.SATIS_TUTARI_YTL);
                                InstitutionFailTxnNumber++;

                                detailListFail.Add(detail);
                            }
                        }
                    }
                }



            }

            else if(instutionId  == (int)Institutions.Teski)
            {
                /*
                 
                 
                    BaskentGAZFibabank.DT_BI_ES_KurumIslemListe_K_Res result;

                    BaskentGAZFibabank.DT_BI_ES_KurumIslemListe_K_Req request = new BaskentGAZFibabank.DT_BI_ES_KurumIslemListe_K_Req();
                   
                    using (BaskentGAZFibabank.SI_BI_ES_KurumIslemListe_K_OBClient client = new BaskentGAZFibabank.SI_BI_ES_KurumIslemListe_K_OBClient())
                    {

                        string userName = "BNKFIBA";
                        string password = "Aa1234**";

                        client.ClientCredentials.Windows.ClientCredential = new System.Net.NetworkCredential(userName, password);
                        client.ClientCredentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Identification;
                        client.ClientCredentials.UserName.Password = password;
                        client.ClientCredentials.UserName.UserName = userName;

                        request.IV_DATUM = tarih.ToString("yyyy-MM-dd"); //tarih.ToString("yyyy -MM-dd");
                        request.IV_DETAY = "X";
                        request.IV_ISLEM = "1";
                        request.IV_KURUM = "900";

                        result = client.SI_BI_ES_KurumIslemListe_K_OB(request);

                    }
                    if (result.ET_DETAY != null)
                    {
                 
                 */


                //MQCS

              using (TeskiTahsilatServiceReference.TahsilatWsClient client = new TeskiTahsilatServiceReference.TahsilatWsClient())
              {
                    decimal prodBankaKodu = Convert.ToDecimal(Utility.GetConfigValue("TeskiBankCode"));
                        //decimal, testBankaKodu = 937;
                        //decimal testuserName = 90937;
                        //string testpassword = "WsH2iLf4xC";
                    decimal produserName = Convert.ToDecimal(Utility.GetConfigValue("TeskiUserName"));
                    var prodpassword = Utility.GetConfigValue("TeskiPassword");


                    var formattedTarih = tarih.ToString("dd/MM/yyyy");
                
                  var teskiSuccessTrans = client.gunSonuDetay(string.Empty, formattedTarih, produserName, prodpassword, prodBankaKodu);
                    if (teskiSuccessTrans != null && teskiSuccessTrans.Length > 0/* && respTeski.Count() > 1*/)
                    {
                        teskiSuccessTrans.ToList()
                            .ForEach(x => detailListSuccess.Add(new MutabakatCompareDetails
                            {
                                Amount = x.toplam,
                                CustomerId = x.kulNo,
                                CustomerName = "",
                                InsertionDate = "",
                                InvoiceNumber = x.makbuzNo,
                                CompareResult = "0",
                                ReferenceId = x.makbuzNo,
                            }));
                        InstitutionSuccessTxnAmount = teskiSuccessTrans.Sum(x => Convert.ToDecimal(x.toplam));
                        InstitutionSuccessTxnNumber = teskiSuccessTrans.Length;
                    }
                    else
                    {
                        InstitutionSuccessTxnAmount = 0;
                        InstitutionSuccessTxnNumber = 0;
                    }
                    var teskiFailTrans = client.getIptalList(string.Empty, formattedTarih, produserName, prodpassword, prodBankaKodu);
                    if (teskiFailTrans != null && teskiFailTrans.Length > 0)
                    {
                        teskiFailTrans.ToList()
                        .ForEach(x => detailListFail.Add(new MutabakatCompareDetails
                        {
                            Amount = x.toplam,
                            CustomerId = x.kulNo,
                            CustomerName = "",
                            InsertionDate = "",
                            InvoiceNumber = x.makbuzNo,
                            CompareResult = "0",
                            ReferenceId = x.makbuzNo,
                        }));
                            InstitutionFailTxnAmount = teskiFailTrans.Sum(x => Convert.ToDecimal(x.toplam));
                            InstitutionFailTxnNumber = teskiFailTrans.Length;
                    }
                    else
                    {
                        InstitutionFailTxnAmount = 0;
                        InstitutionFailTxnNumber = 0;
                    }
                }
            }

            FindDifferencesInList(ref detailListFail, ref detailListSuccess, ref detailListBOSSuccess, ref detailListBOSFail);

            compare.BOSPrepaidNumber = BOSSuccessTxnNumber.ToString();
            compare.BOSPrepaidTotal = BOSSuccessTxnAmount.ToString();
            compare.BOSNumber = BOSSuccessTxnNumber.ToString();
            compare.BOSTotal = BOSSuccessTxnAmount.ToString();

            compare.BOSInvoiceNumber = "0";
            compare.BOSInvoiceTotal = "0";
            compare.BOSCancelNumber = BOSFailTxnNumber.ToString();
            compare.BOSCancelTotal = BOSFailTxnAmount.ToString();

            compare.BOSSuccessedTxn = detailListBOSSuccess.Where(x => x.CompareResult == "0").ToList();
            compare.BOSFailedTxn = detailListBOSFail.Where(x => x.CompareResult == "0").ToList(); 

            compare.InstitutionPrepaidNumber = InstitutionSuccessTxnNumber.ToString();
            compare.InstitutionPrepaidTotal = InstitutionSuccessTxnAmount.ToString();
            compare.InstitutionTotalNumber = InstitutionSuccessTxnNumber.ToString();
            compare.InstitutionTotalAmount = InstitutionSuccessTxnAmount.ToString();

            compare.InstitutionInvoiceNumber = "0";
            compare.InstitutionInvoiceTotal = "0";
            compare.InstitutionCancelNumber = InstitutionFailTxnNumber.ToString();
            compare.InstitutionCancelTotal = InstitutionFailTxnAmount.ToString();

            compare.InstitutionSuccessedTxn = detailListSuccess.Where(x => x.CompareResult == "0").ToList();
            compare.InstitutionFailedTxn = detailListFail.Where(x => x.CompareResult == "0").ToList(); 
             
            compare.TotalDifferenceNumber = (BOSSuccessTxnNumber - InstitutionSuccessTxnNumber).ToString();
            compare.TotalDifferenceTotal = (BOSSuccessTxnAmount - InstitutionSuccessTxnAmount).ToString();

            Utility util = new Utility();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;

            //string serializedData = util.DeleteLine(JsonConvert.SerializeObject(compare));
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("serializedData başatılıyor"), "10");
            string serializedData = util.DeleteLine(serializer.Serialize(compare));
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("Veri uzunluğu:"+serializedData.Length), "9");
            resp.Add("MutabakatCompareResult", JObject.Parse(serializedData));
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), new Exception("resp.Add çalıştı"), "10");

            errorCode = ReturnCodes.SUCCESSFULL;

            return resp;
        }
        catch (Exception exp)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp ,"8");
            errorCode = ReturnCodes.SYSTEM_ERROR;
        }
        finally
        {
            resp = null;
        }
        return resp;
    }


    public double ConvertToDouble(string amount)
    {
        try
        {
            double transferedAmount = 0;
            CultureInfo culture = new CultureInfo("tr-TR");
            transferedAmount = Convert.ToDouble(amount.Replace('.', ','), culture);
            return transferedAmount;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }


    public decimal ConvertToDecimal(string input)
    {
        try
        {
            return Convert.ToDecimal(input.Replace('.', ','), new System.Globalization.CultureInfo("tr-TR", false));
        }
        catch (Exception)
        {
            return 0;
        }
    }





    private void FindDifferencesInList(ref List<MutabakatCompareDetails> detailListInsFail,
                                       ref List<MutabakatCompareDetails> detailListInsSuccess,
                                       ref List<MutabakatCompareDetails> detailListBOSSuccess,
                                       ref List<MutabakatCompareDetails> detailListBOSFail)
    {
        try
        {
            foreach (var itemBOS in detailListBOSSuccess)
            {
                foreach (var itemIns in detailListInsSuccess)
                {
                    if (itemBOS.ReferenceId!= null && itemBOS.ReferenceId.Split('-').Length>0)
                    {
                        itemBOS.ReferenceId = itemBOS.ReferenceId.Split('-')[0];
                    }
                    
                    if ((itemBOS.ReferenceId == itemIns.ReferenceId ||  itemBOS.TransactionId == itemIns.ReferenceId)  && ConvertToDecimal(itemBOS.Amount) == ConvertToDecimal(itemIns.Amount)&& itemIns.CompareResult == "0")
                    {
                        itemBOS.CompareResult = "1";
                        itemIns.CompareResult = "1";
                    }
                }
            }

            foreach (var itemBOS in detailListInsFail)
            {
                foreach (var itemIns in detailListBOSFail)
                {
                    if (itemBOS.ReferenceId != null && itemBOS.ReferenceId.Split('-').Length > 0)
                    {
                        itemBOS.ReferenceId = itemBOS.ReferenceId.Split('-')[0];
                    }
                    if ((itemBOS.ReferenceId == itemIns.ReferenceId || itemBOS.TransactionId == itemIns.ReferenceId) && ConvertToDecimal(itemBOS.Amount) == ConvertToDecimal(itemIns.Amount) && itemIns.CompareResult == "0")
                    {
                        itemBOS.CompareResult = "1";
                        itemIns.CompareResult = "1";
                    }

                }
            }
        }
        catch (Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), ex, "FindDifferencesInList");

            errorCode = ReturnCodes.SYSTEM_ERROR;
        }
    }

    private JObject MakeMaskiMutabakatListOperation(DateTime tarih, Int64 kioskId, int instutionId, Int64 adminUserId)
    {
        JObject resp = new JObject();
        try
        {

            MutabakatSERVICE2.WebServiceMaskiMutabakat.ParserEODDetail result;
            using (MutabakatSERVICE2.WebServiceMaskiMutabakat.CardTransactionSoapClient maskiOps = new MutabakatSERVICE2.WebServiceMaskiMutabakat.CardTransactionSoapClient())
            {
                result = maskiOps.GetEODDetails(tarih, tarih, 1, 9999, "bostest", kioskId);
            }

            //Log.Debug("CPO84 - AskiLoading Operasyonu bitti. Sonuç : " + responseText);

            if (result.details != null)
            {
                //data.Add("InvoiceData", JObject.Parse(serializedData));
                List<DBAskiActionsFromServiceDetail> detailList = new List<DBAskiActionsFromServiceDetail>();

                foreach (var dd in result.details)
                {
                    DBAskiActionsFromServiceDetail detail = new DBAskiActionsFromServiceDetail();
                    detail.bankReferenceNumber = dd.SessionId.ToString();
                    detail.institutionAmount = Convert.ToDecimal(dd.Tutar); //todo efe convert from double to decimal;
                    detail.faturaNumber = dd.SessionId.ToString();
                    detail.insProcessDate = dd.IslemTarihi.ToString();
                    detail.status = dd.IslemDurumu.ToString();
                    detail.customerNumber = "0";
                    detailList.Add(detail);
                }
                string serializedData = JsonConvert.SerializeObject(detailList);
                resp.Add("Response", serializedData);
                //resp = JObject.Parse(serializedData);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
                errorCode = ReturnCodes.NO_DATA_FOUND;
            return resp;
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
        }
        finally
        {
            resp = null;
        }
        return resp;
    }

    public static string GetAskiSatisListesiDBodyData(string userName,
                                            string password,
                                            string firstDate,
                                            string lastDate)
    {
        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
               "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                 "<soap:Body>" +
                   "<SatisListesi xmlns=\"http://tempuri.org/\">" +
                     "<KulAdi>" + userName + "</KulAdi>" +
                     "<Sifre>" + password + "</Sifre>" +
                     "<BasTarih>" + firstDate + "</BasTarih>" +
                     "<BitisTarihi>" + lastDate + "</BitisTarihi>" +
                   "</SatisListesi>" +
                 "</soap:Body>" +
               "</soap:Envelope>";

    }

    public static string GetMaskiSatisListesiDBodyData(string vezneNo,
                                           string vezneSifre,
                                           string applicationType,
                                           string kullaniciNo,
                                           string mutabakatate)
    {
        return "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:tem=\"http://tempuri.org/\">" +
   "<soap:Header>" +
      "<tem:VezneKullanici>" +
       "<tem:VezneNo>" + vezneNo + "</tem:VezneNo>" +
       "<tem:Sifre>" + vezneSifre + "</tem:Sifre>" +
       "<tem:ApplicationType>" + applicationType + "</tem:ApplicationType>" +
       "<tem:KullaniciNo>" + kullaniciNo + "</tem:KullaniciNo>" +
      "</tem:VezneKullanici>" +
   "</soap:Header>" +
   "<soap:Body>" +
      "<tem:BankaGenelMutabakat>" +
         "<tem:MutabakatTarihi>" + mutabakatate + " </tem:MutabakatTarihi>" +
      "</tem:BankaGenelMutabakat>" +
   "</soap:Body>" +
"</soap:Envelope>";

    }

    public static string GetMaskiSatisListesiDBodyDataListe(string vezneNo,
                                           string vezneSifre,
                                           string applicationType,
                                           string kullaniciNo,
                                           string mutabakatate)
    {
        return "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:tem=\"http://tempuri.org/\">" +
   "<soap:Header>" +
      "<tem:VezneKullanici>" +
       "<tem:VezneNo>" + vezneNo + "</tem:VezneNo>" +
       "<tem:Sifre>" + vezneSifre + "</tem:Sifre>" +
       "<tem:ApplicationType>" + applicationType + "</tem:ApplicationType>" +
       "<tem:KullaniciNo>" + kullaniciNo + "</tem:KullaniciNo>" +
      "</tem:VezneKullanici>" +
   "</soap:Header>" +
   "<soap:Body>" +
      "<tem:BankaDetayMutabakat>" +
         "<tem:MutabakatTarihi>" + mutabakatate + " </tem:MutabakatTarihi>" +
      "</tem:BankaDetayMutabakat>" +
   "</soap:Body>" +
"</soap:Envelope>";

    }

    private JObject SaveEmptyKioskMutabakat(int userId, int kioskEmptyId, string inputAmount, string outputAmount, string explanation)
    {
        JObject data = new JObject();
        try
        {
            AccountOpr opr = new AccountOpr();

            int resp = opr.SaveEmptyKioskMutabakat(userId, kioskEmptyId, inputAmount, outputAmount, explanation);
            opr.Dispose();
            opr = null;

            if (resp == 0)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveEmptyKioskMutabakat");
        }

        return data;
    }

    private JObject GetMutabakatCashCount(int whichSide, int kioskEmptyId)
    {
        JObject data = new JObject();
        try
        {
            AccountOpr opr = new AccountOpr();

            MutabakatCashCountCollection items = opr.GetMutabakatCashCount(whichSide, kioskEmptyId);
            opr.Dispose();
            opr = null;

            if (items != null & items.Count > 0)
            {
                data = items.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetEmptyKioskName");
        }

        return data;
    }


    private JObject SearchPOSEOD(string kioskId
                                      , DateTime startDate
                                      , DateTime endDate
                                      , int numberOfItemsPerPage
                                      , int currentPageNum
                                      , int recordCount
                                      , int pageCount
                                      , int orderSelectionColumn
                                      , int descAsc
                                      , int orderType)
    {

        JObject data = new JObject();
        string TotalAmount = "";
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;


            AccountOpr opr = new AccountOpr();

            POSEODCollection item = opr.SearchPOSEODOrder(kioskId
                                                                   , startDate
                                                                   , endDate
                                                                   , numberOfItemsPerPage
                                                                   , currentPageNum
                                                                   , ref recordCount
                                                                   , ref pageCount
                                                                   , orderSelectionColumn
                                                                   , descAsc
                                                                   , ref TotalAmount
                                                                   , orderType);
            opr.Dispose();
            opr = null;

            if (item != null & item.Count > 0)
            {
                data = item.GetJSON();
                data.Add("TotalAmount", TotalAmount);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchPOSEOD");
        }
        return data;
    }

    private JObject GetEmptyKioskName(int expertUserId)
    {
        JObject data = new JObject();
        try
        {
            KioskOpr opr = new KioskOpr();

            EmptyKioskNameCollection kioskNames = opr.GetEmptyKioskName(expertUserId);
            opr.Dispose();
            opr = null;

            if (kioskNames != null & kioskNames.Count > 0)
            {
                data = kioskNames.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetEmptyKioskName");
        }

        return data;
    }



    private JObject SearchDailyAccountReport(DateTime startDate
                                , DateTime endDate
                                , int numberOfItemsPerPage
                                , int currentPageNum
                                , int recordCount
                                , int pageCount
                                , int orderSelectionColumn
                                , int descAsc
                                , int orderType)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            AccountOpr opr = new AccountOpr();
            DailyAccountReportCollection mutabakats = opr.SearchDailyAccountReportOrder(startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, orderType);
            opr.Dispose();
            opr = null;

            if (mutabakats != null & mutabakats.Count > 0)
            {
                data = mutabakats.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchDailyAccountReportMutabakat");
        }
        return data;
    }

    private JObject SearchDailyAccountReportProkis(DateTime startDate
                                , DateTime endDate
                                , int numberOfItemsPerPage
                                , int currentPageNum
                                , int recordCount
                                , int pageCount
                                , int orderSelectionColumn
                                , int descAsc
                                , int orderType)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            AccountOpr opr = new AccountOpr();
            DailyAccountReportProkisCollection mutabakats = opr.SearchDailyAccountReportProkisOrder(startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, orderType);
            opr.Dispose();
            opr = null;

            if (mutabakats != null & mutabakats.Count > 0)
            {
                data = mutabakats.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchDailyAccountReportMutabakat");
        }
        return data;
    }


    private JObject SearchKioskCashAcceptor(DateTime startDate
                                , DateTime endDate
                                , int numberOfItemsPerPage
                                , int currentPageNum
                                , int recordCount
                                , int pageCount
                                , int orderSelectionColumn
                                , int descAsc
                                , int instutionId
                                , string kioskId
                                , int orderType
                                )
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            AccountOpr opr = new AccountOpr();
            KioskCashAcceptorCollection kca = opr.SearchKioskCashAcceptorOrder(startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, instutionId, kioskId, orderType);
            opr.Dispose();
            opr = null;

            if (kca != null & kca.Count > 0)
            {
                data = kca.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskCashAcceptor");
        }
        return data;
    }



    private JObject SearchKioskCashDispenser(DateTime startDate
                                , DateTime endDate
                                , int numberOfItemsPerPage
                                , int currentPageNum
                                , int recordCount
                                , int pageCount
                                , int orderSelectionColumn
                                , int descAsc
                                , int instutionId
                                , string kioskId
                                , int orderType
        )
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            AccountOpr opr = new AccountOpr();
            KioskCashDispenserCollection kca = opr.SearchKioskCashDispenserOrder(startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, instutionId, kioskId, orderType);
            opr.Dispose();
            opr = null;

            if (kca != null & kca.Count > 0)
            {
                data = kca.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskCashDispenser");
        }
        return data;
    }

    private JObject SearchKioskCashDispenserNew(DateTime startDate
                                , DateTime endDate
                                , int numberOfItemsPerPage
                                , int currentPageNum
                                , int recordCount
                                , int pageCount
                                , int orderSelectionColumn
                                , int descAsc
                                , int instutionId
                                , string kioskId
                                , int orderType
        )
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            AccountOpr opr = new AccountOpr();
            KioskCashDispenserCollection kca = opr.SearchKioskCashDispenserNewOrder(startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, instutionId, kioskId, orderType);
            opr.Dispose();
            opr = null;

            if (kca != null & kca.Count > 0)
            {
                data = kca.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskCashDispenser");
        }
        return data;
    }


    private JObject SearchKioskCashDispenserLCDMHopper(DateTime startDate
                                , DateTime endDate
                                , int numberOfItemsPerPage
                                , int currentPageNum
                                , int recordCount
                                , int pageCount
                                , int orderSelectionColumn
                                , int descAsc
                                , int instutionId
                                , string kioskId
                                , int orderType
        )
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            AccountOpr opr = new AccountOpr();
            KioskCashDispenserLCDMHopperCollection kca = opr.SearchKioskCashDispenserLCDMHopperOrder(startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, instutionId, kioskId, orderType);
            opr.Dispose();
            opr = null;

            if (kca != null & kca.Count > 0)
            {
                data = kca.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskCashDispenser");
        }
        return data;
    }


    //
    private JObject GetKioskCashDispenser(int kioskId)
    {
        JObject data = new JObject();
        try
        {
            AccountOpr opr = new AccountOpr();
            KioskCashDispenser kcd = opr.GetKioskCashDispenserOrder(kioskId);
            opr.Dispose();
            opr = null;

            if (kcd != null)
            {
                data = GetJSON(kcd);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetKioskCashDispenser");
        }
        return data;
    }

    public JObject GetJSON(KioskCashDispenser thekcd)
    {
        JObject jObjectDis = new JObject();
        string a = JsonConvert.SerializeObject(thekcd, Newtonsoft.Json.Formatting.None);
        JObject x = JObject.Parse(a);
        jObjectDis.Add("GetKioskCashDispenser", x);
        return jObjectDis;
    }

    public JObject GetJSON( List<UsageFeeDetails >thekcd)
    {
        JObject jObjectDis = new JObject();
        string a = JsonConvert.SerializeObject(thekcd, Newtonsoft.Json.Formatting.None);
        JArray x = JArray.Parse(a);
        jObjectDis.Add("Datas", x);
        return jObjectDis;
    }

    public JObject GetJSON(List<KioskEmptyStatus> statues )
    {
        JObject jObjectDis = new JObject();
        string a = JsonConvert.SerializeObject(statues, Newtonsoft.Json.Formatting.None);
        JArray x = JArray.Parse(a);
        jObjectDis.Add("Status", x);
        return jObjectDis;
    }


    private JObject ControlCashNotification(string searhText
                                           , DateTime startDate
                                            , DateTime endDate
                                           , int numberOfItemsPerPage
                                           , int currentPageNum
                                           , int recordCount
                                           , int pageCount
                                           , int orderSelectionColumn
                                           , int descAsc
                                           , int instutionId
                                           , string kioskId
                                           , int orderType
                                           , int statusId
                                           , int kioskFullFillId)
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            string totalfirstDenomNumber = "0";
            string totalsecondDenomNumber = "0";
            string totalthirdDenomNumber = "0";
            string totalfourthDenomNumber = "0";
            string totalfifthDenomNumber = "0";
            string totalsixthDenomNumber = "0";
            string totalseventhDenomNumber = "0";
            string totaleighthDenomNumber = "0";
            string totalninthDenomNumber = "0";
            string totaltenthDenomNumber = "0";
            string totaleleventhDenomNumber = "0";
            string totalAmount = "0";

            AccountOpr opr = new AccountOpr();
            KioskControlCashNotificationCollection kca = opr.ControlCashNotificationOrder(searhText, startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, instutionId, kioskId
                                                          , ref totalfirstDenomNumber
                                                          , ref totalsecondDenomNumber
                                                          , ref totalthirdDenomNumber
                                                          , ref totalfourthDenomNumber
                                                          , ref totalfifthDenomNumber
                                                          , ref totalsixthDenomNumber
                                                          , ref totalseventhDenomNumber
                                                          , ref totaleighthDenomNumber
                                                          , ref totalninthDenomNumber
                                                          , ref totaltenthDenomNumber
                                                          , ref totaleleventhDenomNumber
                                                          , ref totalAmount
                                                          , orderType
                                                          , statusId
                                                          , kioskFullFillId);
            opr.Dispose();
            opr = null;

            if (kca != null & kca.Count > 0)
            {
                data = kca.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }


            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);

            data.Add("totalfirstDenomNumber", totalfirstDenomNumber);
            data.Add("totalsecondDenomNumber", totalsecondDenomNumber);
            data.Add("totalthirdDenomNumber", totalthirdDenomNumber);
            data.Add("totalfourthDenomNumber", totalfourthDenomNumber);
            data.Add("totalfifthDenomNumber", totalfifthDenomNumber);
            data.Add("totalsixthDenomNumber", totalsixthDenomNumber);
            data.Add("totalseventhDenomNumber", totalseventhDenomNumber);
            data.Add("totaleighthDenomNumber", totaleighthDenomNumber);
            data.Add("totalninthDenomNumber", totalninthDenomNumber);
            data.Add("totaltenthDenomNumber", totaltenthDenomNumber);
            data.Add("totaleleventhDenomNumber", totaleleventhDenomNumber);
            data.Add("totalAmount", totalAmount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ControlCashNotification");
        }
        return data;
    }

    private JObject GetControlCashNotification(int controlCashId)
    {
        JObject data = new JObject();
        try
        {
            AccountOpr opr = new AccountOpr();
            KioskControlCashNotification controlCashNotification = opr.GetControlCashNotificationOrder(controlCashId);
            opr.Dispose();
            opr = null;

            if (controlCashNotification != null)
            {
                data = GetJSON(controlCashNotification);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetControlCashNotification");
        }
        return data;
    }

    public JObject GetJSON(KioskControlCashNotification controlCashNotification)
    {
        JObject jObjectDis = new JObject();
        string a = JsonConvert.SerializeObject(controlCashNotification, Newtonsoft.Json.Formatting.None);
        JObject x = JObject.Parse(a);
        jObjectDis.Add("GetControlCashNotification", x);
        return jObjectDis;
    }

    private JObject CutOffMonitoring(string searhText
                                           , DateTime startDate
                                           , DateTime endDate
                                           , int numberOfItemsPerPage
                                           , int currentPageNum
                                           , int recordCount
                                           , int pageCount
                                           , int orderSelectionColumn
                                           , int descAsc
                                           , int instutionId
                                           , string kioskId
                                           , int orderType)
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            AccountOpr opr = new AccountOpr();
            KioskCutofMonitoringCollection kca = opr.CutOffMonitoringOrder(searhText, startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, instutionId, kioskId, orderType);
            opr.Dispose();
            opr = null;

            if (kca != null & kca.Count > 0)
            {
                data = kca.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "CutOffMonitoring");
        }
        return data;
    }

    //GetCutOffMonitoring

    private JObject GetCutOffMonitoring(int cutId)
    {
        JObject data = new JObject();
        try
        {
            AccountOpr opr = new AccountOpr();
            KioskCutofMonitoring cutkiosk = opr.GetCutOffMonitoringOrder(cutId);
            opr.Dispose();
            opr = null;

            if (cutkiosk != null)
            {
                data = GetJSON(cutkiosk);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetCutOffMonitoring");
        }
        return data;
    }

    public JObject GetJSON(KioskCutofMonitoring thecutkiosk)
    {
        JObject jObjectDis = new JObject();
        string a = JsonConvert.SerializeObject(thecutkiosk, Newtonsoft.Json.Formatting.None);
        JObject x = JObject.Parse(a);
        jObjectDis.Add("GetKioskCutofMonitoring", x);
        return jObjectDis;
    }


    private JObject SaveCutOffMonitoring(InputListSaveCutOffMonitoring kioskSaveCutOffMonitoring)
    {


        JObject data = new JObject();
        try
        {

            AccountOpr opr = new AccountOpr();
            int resp = opr.SaveCutOffMonitoring(kioskSaveCutOffMonitoring.expertUserId,
                                                kioskSaveCutOffMonitoring.startDate,
                                                kioskSaveCutOffMonitoring.endDate,
                                                kioskSaveCutOffMonitoring.cutDurationText,
                                                kioskSaveCutOffMonitoring.instutionId,
                                                kioskSaveCutOffMonitoring.cutReasonText,
                                                kioskSaveCutOffMonitoring.kioskId,
                                                kioskSaveCutOffMonitoring.insertDate);

            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

            opr.Dispose();
            opr = null;

            if (resp == 0)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveCutOffMonitoring");
        }
        return data;
    }

    //UpdateCutOffMonitoring
    private JObject UpdateCutOffMonitoring(InputListSaveCutOffMonitoring kioskUpdateCutOffMonitoring)
    {


        JObject data = new JObject();
        try
        {

            AccountOpr opr = new AccountOpr();
            int resp = opr.UpdateCutOffMonitoring(kioskUpdateCutOffMonitoring.cutId,
                                                kioskUpdateCutOffMonitoring.expertUserId,
                                                kioskUpdateCutOffMonitoring.startDate,
                                                kioskUpdateCutOffMonitoring.endDate,
                                                kioskUpdateCutOffMonitoring.cutDurationText,
                                                kioskUpdateCutOffMonitoring.instutionId,
                                                kioskUpdateCutOffMonitoring.cutReasonText,
                                                kioskUpdateCutOffMonitoring.kioskId,
                                                kioskUpdateCutOffMonitoring.insertDate);

            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

            opr.Dispose();
            opr = null;

            if (resp == 0)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UpdateCutOffMonitoring");
        }
        return data;
    }

    private JObject BankAccountAction(string searhText
                                           , DateTime startDate
                                           , DateTime endDate
                                           , int numberOfItemsPerPage
                                           , int currentPageNum
                                           , int recordCount
                                           , int pageCount
                                           , int orderSelectionColumn
                                           , int descAsc
                                           , int orderType)
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            AccountOpr opr = new AccountOpr();
            KioskBankAccountActionCollection kca = opr.BankAccountActionOrder(searhText, startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, orderType);
            opr.Dispose();
            opr = null;

            if (kca != null & kca.Count > 0)
            {
                data = kca.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "BankAccountAction");
        }
        return data;
    }


    private JObject EntereKioskEmptyCashCount(string searhText
                                           , DateTime startDate
                                           , DateTime endDate
                                           , int numberOfItemsPerPage
                                           , int currentPageNum
                                           , int recordCount
                                           , int pageCount
                                           , int orderSelectionColumn
                                           , int descAsc
        //, int instutionId
                                           , string kioskId
                                           , string enteredUser
                                           , string operatorUser
                                           , string status)
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            AccountOpr opr = new AccountOpr();
            EntereKioskEmptyCashCountCollection kca = opr.EnteredKioskEmptyCashCountOrder(searhText, startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, kioskId, enteredUser, operatorUser, status);
            opr.Dispose();
            opr = null;

            if (kca != null & kca.Count > 0)
            {
                data = kca.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "ControlCashNotification");
        }
        return data;
    }
    private JObject SearchMutabakat(string searchText
                            , DateTime startDate
                            , DateTime endDate
                            , int numberOfItemsPerPage
                            , int currentPageNum
                            , int recordCount
                            , int pageCount
                            , int orderSelectionColumn
                            , int descAsc
                            , string kioskId
                            , int institutionId
                            , string statusId
                            , string  kioskEmptyStatus
                            , int orderType)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            AccountOpr opr = new AccountOpr();
            MutabakatCollection mutabakats = opr.SearchMutabakatOrder(searchText,
                                                                      startDate,
                                                                      endDate,
                                                                      numberOfItemsPerPage,
                                                                      currentPageNum,
                                                                      ref recordCount,
                                                                      ref pageCount,
                                                                      orderSelectionColumn,
                                                                      descAsc,
                                                                      kioskId,
                                                                      institutionId,
                                                                      Convert.ToInt32(statusId),
                                                                      Convert.ToInt32(kioskEmptyStatus),
                                                                      orderType
                                                                      );
            opr.Dispose();
            opr = null;

            if (mutabakats != null & mutabakats.Count > 0)
            {
                data = mutabakats.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchMutabakat");
        }
        return data;
    }

    private JObject SearchMutabakatForReport(string searchText
                             , DateTime startDate
                             , DateTime endDate
                             , int numberOfItemsPerPage
                             , int currentPageNum
                             , int recordCount
                             , int pageCount
                             , int orderSelectionColumn
                             , int descAsc
                             , int orderType)
    {

        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            AccountOpr opr = new AccountOpr();
            MutabakatReportCollection mutabakats = opr.SearchMutabakatOrderForReport(searchText, startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, orderType);
            opr.Dispose();
            opr = null;

            if (mutabakats != null & mutabakats.Count > 0)
            {
                data = mutabakats.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchMutabakat");
        }
        return data;
    }



    private JObject SearchDailyAccounReportsForExcel(DateTime startDate
                                , DateTime endDate
                                , int orderSelectionColumn
                                , int descAsc)
    {

        JObject data = new JObject();
        try
        {
            AccountOpr opr = new AccountOpr();
            DailyAccountReportCollection mutabakats = opr.SearchDailyAccounReportsOrderForExcel(startDate, endDate, orderSelectionColumn, descAsc);
            opr.Dispose();
            opr = null;

            if (mutabakats != null & mutabakats.Count > 0)
            {
                data = mutabakats.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchDailyAccounReportsForExcel");
        }
        return data;
    }

    private JObject SearchDailyAccounReportsProkisForExcel(DateTime startDate
                               , DateTime endDate
                               , int orderSelectionColumn
                               , int descAsc)
    {

        JObject data = new JObject();
        try
        {
            AccountOpr opr = new AccountOpr();
            DailyAccountReportProkisCollection mutabakats = opr.SearchDailyAccounReportsProkisOrderForExcel(startDate, endDate, orderSelectionColumn, descAsc);
            opr.Dispose();
            opr = null;

            if (mutabakats != null & mutabakats.Count > 0)
            {
                data = mutabakats.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchDailyAccounReportsForExcel");
        }
        return data;
    }

    private JObject GetMutabakatDB(DateTime mutabakatDate, Int64 kioskId, int instutionId, Int64 adminUserId, int orderType)
    {

        JObject data = new JObject();
        try
        {
            AccountOpr opr = new AccountOpr();
            //todo efe  1
           data = opr.GetMutabakatValues(mutabakatDate, kioskId, instutionId, adminUserId,0);
            opr.Dispose();
            opr = null;
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetMutabakatDB");
        }
        return data;
    }


    private JObject GetKioskInfo(int kioskId, DateTime startdate, DateTime enddate)
    {

        JObject data = new JObject();
        try
        {
            string userName = "";
            string password = "";

            AccountOpr opr = new AccountOpr();
            GetKioskUsersAndPasswordCollection resp = opr.GetKioskUserPassWithId(kioskId, startdate, enddate);
            opr.Dispose();
            opr = null;

            return resp.GetJSONJObject();

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetKioskInfo");
        }
        return data;
    }

    private JObject SearchMutabakatForExcel(string searchText
                                , DateTime startDate
                                , DateTime endDate
                                , int orderSelectionColumn
                                , int descAsc)
    {

        JObject data = new JObject();
        try
        {
            AccountOpr opr = new AccountOpr();
            MutabakatCollection mutabakats = opr.SearchMutabakatOrderForExcel(searchText, startDate, endDate, orderSelectionColumn, descAsc);
            opr.Dispose();
            opr = null;

            if (mutabakats != null & mutabakats.Count > 0)
            {
                data = mutabakats.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchMutabakatForExcel");
        }
        return data;
    }

    private JObject GetEmptyKioskName()
    {
        JObject data = new JObject();
        try
        {
            KioskOpr opr = new KioskOpr();

            EmptyKioskNameCollection kioskNames = opr.GetEmptyKioskName();
            opr.Dispose();
            opr = null;

            if (kioskNames != null & kioskNames.Count > 0)
            {
                data = kioskNames.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetEmptyKioskNameMutabakat");
        }

        return data;
    }


    private JObject UpdateKioskEmptyStatus(int kioskEmptyId, int statusId, string explanation)
    {
        JObject data = new JObject();
        //int recordCount = 0;
        //int pageCount = 0;
        try
        {
            AccountOpr opr = new AccountOpr();
            var kioskEmptyStatuses = opr.UpdateKioskEmptyStatus( kioskEmptyId,  statusId,  explanation);
            opr.Dispose();
            opr = null;

            if (kioskEmptyStatuses)
            {
                //data = GetJSON(kioskEmptyStatuses);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;

            }

            data = WebServiceUtils.AddPaggingNodes(data, 0, 0);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetAccounts");
        }

        return data;
    }

    //    public int kioskId { get; set; }
    //public int instutionId { get; set; }
    //public bool isKioskListed { get; set; }
    //public DateTime startDate { get; set; }
    //public DateTime endDate { get; set; }
    private JObject GetUsageFeeDetail(string kioskId, int instutionId, int isKioskListed, DateTime startDate , DateTime endDate)
    {
        JObject data = new JObject();
        try
        {
            AccountOpr opr = new AccountOpr();
            var kioskEmptyStatuses = opr.GetUsageFeeDetails( kioskId,  instutionId,  isKioskListed,  startDate,  endDate);
            opr.Dispose();
            opr = null;

            if (kioskEmptyStatuses != null & kioskEmptyStatuses.Count > 0)
            {
                data = GetJSON(kioskEmptyStatuses);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, 0, 0);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetAccounts");
        }

        return data;
    }


    private JObject GetKioskEmptyStatus(int id)
    {
        JObject data = new JObject();
        //int recordCount = 0;
        //int pageCount = 0;
        try
        {
            AccountOpr opr = new AccountOpr();
            var  kioskEmptyStatuses = opr.GetKioskEmptyStatus(id);
            opr.Dispose();
            opr = null;

            if (kioskEmptyStatuses != null & kioskEmptyStatuses.Count > 0)
            {
                data = GetJSON(kioskEmptyStatuses);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, 0, 0);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetAccounts");
        }

        return data;
    }







    private JObject GetAccounts(int instutionId,
                                int numberOfItemsPerPage,
                                int currentPageNum,
                                int recordCount,
                                int pageCount,
                                int orderSelectionColumn,
                                int descAsc,
                                int orderType)
    {
        JObject data = new JObject();
        //int recordCount = 0;
        //int pageCount = 0;
        try
        {
            AccountOpr opr = new AccountOpr();
            AccountCollection accounts = opr.GetAccounts(instutionId, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, orderType);
            opr.Dispose();
            opr = null;

            if (accounts != null & accounts.Count > 0)
            {
                data = accounts.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetAccounts");
        }

        return data;
    }

    private JObject GetAccountsForExcel(int instutionId)
    {
        JObject data = new JObject();
        try
        {
            AccountOpr opr = new AccountOpr();
            AccountCollection accounts = opr.GetAccountsForExcel(instutionId);
            opr.Dispose();
            opr = null;

            if (accounts != null & accounts.Count > 0)
            {
                data = accounts.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetAccountsForExcel");
        }

        return data;
    }

    private JObject GetAccountDetails(int itemId, int instutionId, int numberOfItemsPerPage, int currentPageNum)
    {
        JObject data = new JObject();
        int recordCount = 0;
        int pageCount = 0;
        try
        {
            AccountOpr opr = new AccountOpr();
            AccountDetailCollection accountDetails = opr.GetAccountDetails(itemId, instutionId, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount);
            opr.Dispose();
            opr = null;

            if (accountDetails != null & accountDetails.Count > 0)
            {
                data = accountDetails.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetAccountDetails");
        }

        return data;
    }

    private JObject ManuelKioskEmpty(int expertUserId, int kioskId)
    {
        JObject data = new JObject();
        try
        {
            KioskOpr opr = new KioskOpr();

            bool rep = opr.ManuelKioskEmpty(expertUserId, kioskId);
            opr.Dispose();
            opr = null;

            if (rep == true)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetEmptyKioskName");
        }

        return data;
    }

    private JObject SearchAutoMutabakatTransactions(string searchText
                                , DateTime startDate
                                , DateTime endDate
                                , int numberOfItemsPerPage
                                , int currentPageNum
                                , int recordCount
                                , int pageCount
                                , int orderSelectionColumn
                                , int descAsc
                                , int UserId
                                , string AdminUserId
                                , int instutionId
                                , int State
                                , int orderType)
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            AccountOpr opr = new AccountOpr();
            AutoMutabakatCollection autoMutabakats = opr.SearchAutoMutabakatOrder(searchText, startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, UserId, AdminUserId, instutionId, State, orderType);
            opr.Dispose();
            opr = null;

            if (autoMutabakats != null & autoMutabakats.Count > 0)
            {
                data = autoMutabakats.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchDailyAccountReportMutabakat");
        }
        return data;
    }



    private JObject SearchKiosksCachAcceptorForExcel(
                                DateTime startDate
                                , DateTime endDate
                                , int numberOfItemsPerPage
                                , int currentPageNum
                                , int recordCount
                                , int pageCount
                                , int orderSelectionColumn
                                , int descAsc
                                , string kioskId
                                , int instutionId
)
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            AccountOpr opr = new AccountOpr();
            CodeReportCollections kca = opr.SearchKioskCashAcceptorOrderForExcel(startDate,
                                                                                 endDate,
                                                                                 numberOfItemsPerPage,
                                                                                 currentPageNum,
                                                                                 ref recordCount,
                                                                                 ref pageCount,
                                                                                 orderSelectionColumn
                                                                                 , descAsc,
                                                                                 instutionId
                                                                                 , kioskId);
            opr.Dispose();
            opr = null;

            if (kca != null & kca.Count > 0)
            {
                data = kca.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskCashAcceptorForExcel");
        }
        return data;
    }


    private JObject SearchKiosksCachDispenserForExcel(
                            DateTime startDate
                            , DateTime endDate
                            , int numberOfItemsPerPage
                            , int currentPageNum
                            , int recordCount
                            , int pageCount
                            , int orderSelectionColumn
                            , int descAsc
                            , string kioskId
                            , int instutionId
)
    {
        JObject data = new JObject();
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            AccountOpr opr = new AccountOpr();
            KioskCashDispenserCollection kca = opr.SearchKioskCashDispenserOrderForExcel(startDate,
                                                                                 endDate,
                                                                                 numberOfItemsPerPage,
                                                                                 currentPageNum,
                                                                                 ref recordCount,
                                                                                 ref pageCount,
                                                                                 orderSelectionColumn
                                                                                 , descAsc,
                                                                                 instutionId
                                                                                 , kioskId);
            opr.Dispose();
            opr = null;

            if (kca != null & kca.Count > 0)
            {
                data = kca.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskCashAcceptorForExcel");
        }
        return data;
    }

    private JObject SearchCodeReports(string searchText
                          , DateTime startDate
                          , DateTime endDate
                          , int numberOfItemsPerPage
                          , int currentPageNum
                          , int recordCount
                          , int pageCount
                          , int orderSelectionColumn
                          , int descAsc
                          , string kioskId
                          , int instutionId
                          , int paymentType
                          , int orderType
                          , int processType
                          , int bankType)
    {

        JObject data = new JObject();
        CodeReportCollections collection = null;
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;

            Int64 TotalTxnCount = 0;
            string TotalGeneratedAskiAmount = "";
            string TotalUsedAskiAmount = "";
            Int64 TotalGeneratedAskiCount = 0;
            Int64 TotalUsedAskiCount = 0;
            string TotalReceivedAmount = "";
            string TotalKioskEmptyAmount = "";
            string TotalEarnedAmount = "";
            string TotalParaUstu = "";
            string TotalSuccesTxnCount = "";
            string TotalInstitutionAmount = "";
            string TotalCreditCardAmount = "";
            string TotalMobilPaymentAmount = "";
            AccountOpr opr = new AccountOpr();

            collection = opr.SearchCodeDetailsOrder(searchText,
                startDate,
                endDate,
                numberOfItemsPerPage,
                currentPageNum,
                ref recordCount,
                ref pageCount,
                orderSelectionColumn,
                descAsc,
                kioskId,
                instutionId,
                ref TotalTxnCount,
                ref TotalGeneratedAskiAmount,
                ref TotalUsedAskiAmount,
                ref TotalGeneratedAskiCount,
                ref TotalUsedAskiCount,
                ref TotalReceivedAmount,
                ref TotalKioskEmptyAmount,
                ref TotalEarnedAmount,
                ref TotalParaUstu,
                ref TotalSuccesTxnCount,
                ref TotalInstitutionAmount,
                ref TotalCreditCardAmount,
                ref TotalMobilPaymentAmount,
                paymentType,
                orderType,
                processType,
                bankType);

            opr.Dispose();
            opr = null;

            if (collection != null & collection.Count > 0)
            {
                data = collection.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
            data.Add("TotalTxnCount", TotalTxnCount);
            data.Add("TotalGeneratedAskiAmount", TotalGeneratedAskiAmount);
            data.Add("TotalUsedAskiAmount", TotalUsedAskiAmount);
            data.Add("TotalGeneratedAskiCount", TotalGeneratedAskiCount);
            data.Add("TotalUsedAskiCount", TotalUsedAskiCount);
            data.Add("TotalReceivedAmount", TotalReceivedAmount);
            data.Add("TotalKioskEmptyAmount", TotalKioskEmptyAmount);
            data.Add("TotalEarnedAmount", TotalEarnedAmount);
            data.Add("TotalParaUstu", TotalParaUstu);
            data.Add("TotalSuccesTxnCount", TotalSuccesTxnCount);
            data.Add("TotalInstitutionAmount", TotalInstitutionAmount);
            //data.Add("TotalCreditCardAmount", TotalCreditCardAmount);
            //data.Add("TotalMobilPaymentAmount", TotalMobilPaymentAmount);

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskReports");
        }
        return data;
    }



    private JObject SearchMoneyCodes(string searchText
                                , DateTime startDate
                                , DateTime endDate
                                , int numberOfItemsPerPage
                                , int currentPageNum
                                , int recordCount
                                , int pageCount
                                , int orderSelectionColumn
                                , int descAsc
                                , int codeStatus
                                , int generatedType
                                , int instutionId
                                , string generatedKioskIds
                                , string usedKioskIds
                                , int paymentType
                                , int orderType)
    {

        JObject data = new JObject();
        MoneyCodeCollection collection = null;
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;
            string totalAmount = "";
            MoneyCodeOpr opr = new MoneyCodeOpr();

            collection = opr.SearchMoneyCodesOrder(searchText
                , startDate
                , endDate
                , numberOfItemsPerPage
                , currentPageNum
                , ref recordCount
                , ref pageCount
                , orderSelectionColumn
                , descAsc
                , codeStatus
                , generatedType
                , ref totalAmount
                , instutionId
                , generatedKioskIds
                , usedKioskIds
                , paymentType
                , orderType);

            opr.Dispose();
            opr = null;

            if (collection != null & collection.Count > 0)
            {
                data = collection.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
            data.Add("totalAmount", totalAmount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchMoneyCodes");
        }
        return data;
    }

    private JObject SearchMoneyCodesSweepBack(string searchText
                               , DateTime startDate
                               , DateTime endDate
                               , int numberOfItemsPerPage
                               , int currentPageNum
                               , int recordCount
                               , int pageCount
                               , int orderSelectionColumn
                               , int descAsc
                               , int codeStatus
                               , int generatedType
                               , int instutionId
                               , int orderType)
    {

        JObject data = new JObject();
        MoneyCodeSweepBackCollection collection = null;
        try
        {
            int recordCnt = recordCount;
            int pageCnt = pageCount;
            string totalAmount = "";
            MoneyCodeOpr opr = new MoneyCodeOpr();

            collection = opr.SearchMoneyCodesOrderSweepBack(searchText, startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, codeStatus, generatedType, ref totalAmount, instutionId, orderType);

            opr.Dispose();
            opr = null;

            if (collection != null & collection.Count > 0)
            {
                data = collection.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
            data.Add("totalAmount", totalAmount);
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchMoneyCodesOrderSweepBack");
        }
        return data;
    }

    private JObject SearchMoneyCodesForExcel(string searchText
                                , DateTime startDate
                                , DateTime endDate
                                , int numberOfItemsPerPage
                                , int currentPageNum
                                , int recordCount
                                , int pageCount
                                , int orderSelectionColumn
                                , int descAsc
                                , int codeStatus
                                , int generatedType
                                , int instutionId
                                , string generatedKioskIds
                                , string usedKioskIds)
    {

        JObject data = new JObject();
        int recordCnt = recordCount;
        int pageCnt = pageCount;
        string totalAmount = "";
        MoneyCodeCollection collection = null;
        try
        {
            MoneyCodeOpr opr = new MoneyCodeOpr();

            collection = opr.SearchMoneyCodesOrderForExcel(searchText
                , startDate
                , endDate
                , numberOfItemsPerPage
                , currentPageNum
                , ref recordCount
                , ref pageCount
                , orderSelectionColumn
                , descAsc
                , codeStatus
                , generatedType
                , ref totalAmount
                , instutionId
                , generatedKioskIds
                , usedKioskIds);

            opr.Dispose();
            opr = null;

            if (collection != null & collection.Count > 0)
            {
                data = collection.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchMoneyCodesForExcel");
        }
        return data;
    }

    //private JObject SearchMoneyCodesSweepBackForExcel(string searchText
    //                           , DateTime startDate
    //                           , DateTime endDate
    //                           , int numberOfItemsPerPage
    //                           , int currentPageNum
    //                           , int recordCount
    //                           , int pageCount
    //                           , int orderSelectionColumn
    //                           , int descAsc
    //                           , int codeStatus
    //                           , int generatedType
    //                           , int instutionId)
    //{

    //    JObject data = new JObject();
    //    MoneyCodeSweepBackCollection collection = null;
    //    try
    //    {
    //        int recordCnt = recordCount;
    //        int pageCnt = pageCount;
    //        string totalAmount = "";

    //        MoneyCodeOpr opr = new MoneyCodeOpr();

    //        collection = opr.SearchMoneyCodesOrderSweepBackForExcel(searchText, startDate, endDate, numberOfItemsPerPage, currentPageNum, ref recordCount, ref pageCount, orderSelectionColumn, descAsc, codeStatus, generatedType, ref totalAmount, instutionId);

    //        opr.Dispose();
    //        opr = null;

    //        if (collection != null & collection.Count > 0)
    //        {
    //            data = collection.GetJSON();
    //            errorCode = ReturnCodes.SUCCESSFULL;
    //        }
    //        else
    //        {
    //            errorCode = ReturnCodes.NO_DATA_FOUND;
    //        }

    //        data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
    //        data.Add("totalAmount", totalAmount);
    //    }
    //    catch (Exception exp)
    //    {
    //        errorCode = ReturnCodes.SYSTEM_ERROR;
    //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchMoneyCodesForExcel");
    //    }
    //    return data;
    //}


    private JObject GetMoneyCodeStatusNames()
    {
        StatusNameCollection statusNames = null;

        JObject data = new JObject();
        try
        {
            MoneyCodeOpr opr = new MoneyCodeOpr();

            statusNames = opr.GetMoneyCodeStatusNames();
            opr.Dispose();
            opr = null;

            if (statusNames != null & statusNames.Count > 0)
            {
                data = statusNames.GetJSONJObject();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetMoneyCodeStatusNamesAccount");
        }
        return data;
    }

    private JObject SaveMoneyCode(int transactionId, int userId, string codeAmount, string cellPhone)
    {
        JObject data = new JObject();
        string askiCode = "";
        try
        {
            MoneyCodeOpr opr = new MoneyCodeOpr();

            int resp = opr.SaveMoneyCode(transactionId, userId, codeAmount, ref askiCode, cellPhone);
            opr.Dispose();
            opr = null;
            if (resp > 0)
            {
                if (!String.IsNullOrEmpty(askiCode))
                {
                    errorCode = ReturnCodes.SUCCESSFULL;
                    data.Add("moneyCode", askiCode);
                }
                else
                {
                    errorCode = ReturnCodes.ASKICODE_NOT_GENERATED;
                }
            }
            else
            {

                errorCode = ReturnCodes.ASKICODE_NOT_GENERATED;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveMoneyCode");
        }

        return data;
    }

    private JObject SendMoneyCode(InputSaveSendMoneyCode inputSaveSendMoneyCode)
    {
        JObject data = new JObject();
        string askiCode = "";
        try
        {
            MoneyCodeOpr opr = new MoneyCodeOpr();

            int resp = opr.SendMoneyCode(inputSaveSendMoneyCode.transactionId, inputSaveSendMoneyCode.moneyCodeId, inputSaveSendMoneyCode.cellPhone, inputSaveSendMoneyCode.adminUserId);
            int updateStaus = opr.UpdateMoneyCodeSMSNumber(inputSaveSendMoneyCode.transactionId, inputSaveSendMoneyCode.moneyCodeId, inputSaveSendMoneyCode.adminUserId);
            opr.Dispose();
            opr = null;
            if (resp > 0)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
                data.Add("moneyCode", askiCode);
            }
            else
            {

                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SendMoneyCode");
        }

        return data;
    }

    //GetMoneyCodeSMSNumber
    private JObject GetMoneyCodeSMSNumber(InputListMoneyCodeSMSNumber listMoneyCodeSMSNumber)
    {

        JObject data = new JObject();

        try
        {
            int recordCount = 0;
            int pageCount = 0;

            MoneyCodeOpr opr = new MoneyCodeOpr();

            MoneyCodeSMSNumberCollection cashCounts = opr.SearchMoneyCodeSMSNumberOrder(listMoneyCodeSMSNumber.moneyCodeId, listMoneyCodeSMSNumber.adminUserId);
            opr.Dispose();
            opr = null;

            if (cashCounts != null & cashCounts.Count > 0)
            {
                data = cashCounts.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchMoneyCodeSMSNumber");
        }
        return data;
    }


    private JObject SearchCodeReportsForExcelNew(string searchText
                       , DateTime startDate
                       , DateTime endDate
                       , int orderSelectionColumn
                       , int descAsc
                       , string kioskId
                       , int instutionId)
    {

        JObject data = new JObject();
        CodeReportCollections collection = null;
        try
        {

            AccountOpr opr = new AccountOpr();

            collection = opr.SearchCodeReportForExcelOrderNew(searchText, startDate, endDate, orderSelectionColumn, descAsc, kioskId, instutionId);

            opr.Dispose();
            opr = null;

            if (collection != null & collection.Count > 0)
            {
                data = collection.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskReportsForExcel");
        }
        return data;
    }

    //GetKioskCommisionSettings
    private JObject KioskCommisionSettings(InputListGetKioskSettings kioskGetCommisionSettings)
    {


        JObject data = new JObject();
        try
        {
            //int creditAski = 0;
            //int mobileAski = 0;
            //int cashAski = 0;
            //int yellowAski = 0;
            //int redAski = 0;
            //
            //int creditBaskent = 0;
            //int mobileBaskent = 0;
            //int cashBaskent = 0;
            //int yellowBaskent = 0;
            //int redBaskent = 0;
            //
            //int creditAksaray = 0;
            //int mobileAksaray = 0;
            //int cashAksaray = 0;
            //int yellowAksaray = 0;
            //int redAksaray = 0;


            AccountOpr opr = new AccountOpr();

            KioskCommisionSettingCollection commision = opr.GetKioskCommisionSettingsOrder(kioskGetCommisionSettings.adminUserId
                                                                //ref   creditAski,
                                                                //ref   mobileAski,
                                                                //ref   cashAski,
                                                                //ref   yellowAski,
                                                                //ref   redAski,
                                                                //ref   creditBaskent,
                                                                //ref   mobileBaskent,
                                                                //ref   cashBaskent,
                                                                //ref   yellowBaskent,
                                                                //ref   redBaskent,
                                                                //ref   creditAksaray,
                                                                //ref   mobileAksaray,
                                                                //ref   cashAksaray,
                                                                //ref   yellowAksaray,
                                                                //ref   redAksaray
                                                                );

            opr.Dispose();
            opr = null;

            if (commision != null & commision.Count > 0)
            {
                data = commision.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            //data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);
            //data.Add("serverTime", serverTime);

            //data.Add("creditAski", creditAski);
            //data.Add("mobileAski", mobileAski);
            //data.Add("cashAski",   cashAski);
            //data.Add("yellowAski", yellowAski);
            //data.Add("redAski", redAski);
            //
            //data.Add("creditBaskent", creditBaskent);
            //data.Add("mobileBaskent", mobileBaskent);
            //data.Add("cashBaskent", cashBaskent);
            //data.Add("yellowBaskent", yellowBaskent);
            //data.Add("redBaskent", redBaskent);
            //
            //data.Add("creditAksaray", creditAksaray);
            //data.Add("mobileAksaray", mobileAksaray);
            //data.Add("cashAksaray", cashAksaray);
            //data.Add("yellowAksaray", yellowAksaray);
            //data.Add("redAksaray", redAksaray);
 
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetKioskCommisionSettings");
        }
        return data;
    }


    //KioskCommisionSettings
    private JObject GetKioskCommisionSettings(int instutionId)
    {
        JObject data = new JObject();
        try
        {

            int creditCommision = 0;
            int mobileCommision = 0;
            int cashCommision = 0;
            int yellowAlarm =   0;
            int redAlarm = 0;
            string instutionName = "";


            AccountOpr opr = new AccountOpr();

            int resp = opr.KioskCommisionSettingsOrder(instutionId,
                                                        ref   creditCommision,
                                                                ref   mobileCommision,
                                                                ref   cashCommision,
                                                                ref   yellowAlarm,
                                                                ref   redAlarm,
                                                                ref instutionName);




            data.Add("creditCommision", creditCommision);
            data.Add("mobileCommision", mobileCommision);
            data.Add("cashCommision", cashCommision);
            data.Add("yellowAlarm", yellowAlarm);
            data.Add("redAlarm", redAlarm);
            data.Add("instutionName", instutionName);

            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetKioskSettings");
        }
        return data;
    }


  
    //UpdateKioskCommisionSettings
    private JObject UpdateKioskCommisionSettings(InputSaveCommisionSettings inputUpdateKioskCommisionSettings)
    {


        JObject data = new JObject();
        try
        {

            AccountOpr opr = new AccountOpr();
            int resp = opr.UpdateKioskCommisionSettingsOrder(inputUpdateKioskCommisionSettings.instutionId,
                                                inputUpdateKioskCommisionSettings.creditCommision,
                                                inputUpdateKioskCommisionSettings.mobileCommision,
                                                inputUpdateKioskCommisionSettings.cashCommision,
                                                inputUpdateKioskCommisionSettings.yellowAlarm,
                                                inputUpdateKioskCommisionSettings.redAlarm,
                                                inputUpdateKioskCommisionSettings.adminUserId);

            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

            opr.Dispose();
            opr = null;

            if (resp == 0)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UpdateKioskCommisionSettings");
        }
        return data;
    }

    private JObject GetKioskSettings(InputListGetKioskSettings kioskGetSettings)
    {


        JObject data = new JObject();
        try
        {
            string billFieldNotificationLimit = kioskGetSettings.billFieldNotificationLimit;
            int isNotificationLimitReceiptActive = kioskGetSettings.isNotificationLimitReceiptActive;
            string cashBoxNotificationLimit = kioskGetSettings.cashBoxNotificationLimit;
            int isNotificationCashBoxLimitActive = kioskGetSettings.isNotificationCashBoxLimitActive;
            int isYellowAlertActive = kioskGetSettings.isYellowAlertActive;
            int isRedAlertActive = kioskGetSettings.isRedAlertActive;
            string moneyCodeGenerationLimit = kioskGetSettings.moneyCodeGenerationLimit;
            string moneyCodeGenerationCountLimit = kioskGetSettings.moneyCodeGenerationCountLimit;
            string oneyCodeGenerationCountLimitPeriod = kioskGetSettings.oneyCodeGenerationCountLimitPeriod;
            string bGazUpperLoadingLimit = kioskGetSettings.bGazUpperLoadingLimit;

            AccountOpr opr = new AccountOpr();
            int resp = opr.GetKioskSettings(kioskGetSettings.adminUserId,
                                                       ref billFieldNotificationLimit,
                                                       ref isNotificationLimitReceiptActive,
                                                       ref cashBoxNotificationLimit,
                                                       ref isNotificationCashBoxLimitActive,
                                                       ref isYellowAlertActive,
                                                       ref isRedAlertActive,
                                                       ref moneyCodeGenerationLimit,
                                                       ref moneyCodeGenerationCountLimit,
                                                       ref oneyCodeGenerationCountLimitPeriod,
                                                       ref bGazUpperLoadingLimit);


            data.Add("BillFieldNotificationLimit", billFieldNotificationLimit);
            data.Add("IsNotificationLimitReceiptActive", isNotificationLimitReceiptActive);
            data.Add("CashBoxNotificationLimit", cashBoxNotificationLimit);
            data.Add("IsNotificationCashBoxLimitActive", isNotificationCashBoxLimitActive);
            data.Add("IsYellowAlertActive", isYellowAlertActive);
            data.Add("IsRedAlertActive", isRedAlertActive);
            data.Add("MoneyCodeGenerationLimit", moneyCodeGenerationLimit);
            data.Add("MoneyCodeGenerationCountLimit", moneyCodeGenerationCountLimit);
            data.Add("OneyCodeGenerationCountLimitPeriod", oneyCodeGenerationCountLimitPeriod);
            data.Add("BGazUpperLoadingLimit", bGazUpperLoadingLimit);

            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetKioskSettings");
        }
        return data;
    }


    private JObject SaveKioskSettings(InputSaveKioskSettings kioskSaveSettings)
    {
        JObject data = new JObject();
        try
        {
            AccountOpr opr = new AccountOpr();

            int resp = opr.SaveKioskSettings(kioskSaveSettings.adminUserId,
                                                     kioskSaveSettings.billFieldNotificationLimit,
                                                     kioskSaveSettings.isNotificationLimitReceiptActive,
                                                     kioskSaveSettings.cashBoxNotificationLimit,
                                                     kioskSaveSettings.isNotificationCashBoxLimitActive,
                                                     kioskSaveSettings.isYellowAlertActive,
                                                     kioskSaveSettings.isRedAlertActive,
                                                     kioskSaveSettings.moneyCodeGenerationLimit,
                                                     kioskSaveSettings.moneyCodeGenerationCountLimit,
                                                     kioskSaveSettings.oneyCodeGenerationCountLimitPeriod,
                                                     kioskSaveSettings.bGazUpperLoadingLimit);
            opr.Dispose();
            opr = null;

            if (resp == 1)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveKioskSettings");
        }

        return data;
    }

    //SaveCommisionSettings
    private JObject SaveCommisionSettings(InputSaveCommisionSettings kioskSaveCommisionSettings)
    {
        JObject data = new JObject();
        try
        {
            AccountOpr opr = new AccountOpr();

            int resp = opr.SaveCommisionSettingsOrder(kioskSaveCommisionSettings.adminUserId,
                                                      kioskSaveCommisionSettings.creditAski,
                                                      kioskSaveCommisionSettings.mobileAski,
                                                      kioskSaveCommisionSettings.cashAski,
                                                      kioskSaveCommisionSettings.yellowAski,
                                                      kioskSaveCommisionSettings.redAski,
                                                      kioskSaveCommisionSettings.creditBaskent,
                                                      kioskSaveCommisionSettings.mobileBaskent,
                                                      kioskSaveCommisionSettings.cashBaskent,
                                                      kioskSaveCommisionSettings.yellowBaskent,
                                                      kioskSaveCommisionSettings.redBaskent,
                                                      kioskSaveCommisionSettings.creditAksaray,
                                                      kioskSaveCommisionSettings.mobileAksaray,
                                                      kioskSaveCommisionSettings.cashAksaray,
                                                      kioskSaveCommisionSettings.yellowAksaray,
                                                      kioskSaveCommisionSettings.redAksaray);
            opr.Dispose();
            opr = null;

            if (resp == 1)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveCommisionSettings");
        }

        return data;
    }

    private JObject DeleteControlCashNotification(DeleteControlCashNotification kioskEmptyCashCount)
    {


        JObject data = new JObject();
        try
        {

            AccountOpr opr = new AccountOpr();
            int resp = opr.DeleteControlCashNotification(kioskEmptyCashCount.id, kioskEmptyCashCount.status
                                                        //,kioskEmptyCashCount.approvedCodeapprovedCode
                                                        );

            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

            opr.Dispose();
            opr = null;

            if (resp == 0)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveControlCashNotification");
        }
        return data;
    }


    private JObject SaveControlCashNotification(InputListSaveControlCashNotification kioskEmptyCashCount)
    {


        JObject data = new JObject();
        try
        {

            AccountOpr opr = new AccountOpr();
            int resp = opr.SaveControlCashNotification(kioskEmptyCashCount.kioskId,
                                                        kioskEmptyCashCount.expertUserId,
                                                        kioskEmptyCashCount.firstDenomNumber,
                                                        kioskEmptyCashCount.secondDenomNumber,
                                                        kioskEmptyCashCount.thirdDenomNumber,
                                                        kioskEmptyCashCount.fourthDenomNumber,
                                                        kioskEmptyCashCount.fifthDenomNumber,
                                                        kioskEmptyCashCount.sixthDenomNumber,
                                                        kioskEmptyCashCount.seventhDenomNumber,
                                                        kioskEmptyCashCount.eighthDenomNumber,
                                                        kioskEmptyCashCount.ninthDenomNumber,
                                                        kioskEmptyCashCount.tenthDenomNumber,
                                                        kioskEmptyCashCount.eleventhDenomNumber,
                                                        kioskEmptyCashCount.totalAmount,
                                                        null
                //,kioskEmptyCashCount.approvedCodeapprovedCode
                                                        );

            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

            opr.Dispose();
            opr = null;

            if (resp == 0)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveControlCashNotification");
        }
        return data;
    }

    //UpdateControlCashNotification
    //UpdateCutOffMonitoring
    private JObject UpdateControlCashNotification(InputListSaveControlCashNotification inputListUpdateControlCashNotification)
    {


        JObject data = new JObject();
        try
        {

            AccountOpr opr = new AccountOpr();
            int resp = opr.UpdateControlCashNotification(inputListUpdateControlCashNotification.controlCashId,
                                                        inputListUpdateControlCashNotification.expertUserId,
                                                        inputListUpdateControlCashNotification.firstDenomNumber,
                                                        inputListUpdateControlCashNotification.secondDenomNumber,
                                                        inputListUpdateControlCashNotification.thirdDenomNumber,
                                                        inputListUpdateControlCashNotification.fourthDenomNumber,
                                                        inputListUpdateControlCashNotification.fifthDenomNumber,
                                                        inputListUpdateControlCashNotification.sixthDenomNumber,
                                                        inputListUpdateControlCashNotification.seventhDenomNumber,
                                                        inputListUpdateControlCashNotification.eighthDenomNumber,
                                                        inputListUpdateControlCashNotification.ninthDenomNumber,
                                                        inputListUpdateControlCashNotification.tenthDenomNumber,
                                                        inputListUpdateControlCashNotification.eleventhDenomNumber,
                                                        inputListUpdateControlCashNotification.totalAmount);

            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

            opr.Dispose();
            opr = null;

            if (resp == 0)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "UpdateControlCashNotification");
        }
        return data;
    }



    private JObject SaveKioskEmptyCashCounts(InputListSaveKioskEmptyCashCount kioskEmptyCashCount,long _userId)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("KioskId", typeof(System.Int32)));
        dt.Columns.Add(new DataColumn("ExpertUserId", typeof(System.Int32)));
        dt.Columns.Add(new DataColumn("CashTypeId", typeof(System.Int32)));
        dt.Columns.Add(new DataColumn("Count", typeof(System.Int32)));
        dt.Columns.Add(new DataColumn("KioskEmptyId", typeof(System.Int32)));


        JObject data = new JObject();
        try
        {
            for (int i = 0; i < kioskEmptyCashCount.cashCounts.Count; i++)
            {
                dt.Rows.Add(kioskEmptyCashCount.cashCounts[i].kioskId,
                    kioskEmptyCashCount.cashCounts[i].expertUserId,
                    kioskEmptyCashCount.cashCounts[i].cashTypeId,
                    kioskEmptyCashCount.cashCounts[i].count,
                    kioskEmptyCashCount.cashCounts[i].kioskEmptyId);
            }


            AccountOpr opr = new AccountOpr();
            int resp = opr.SaveKioskEmptyCashCounts(dt, kioskEmptyCashCount.kioskId, kioskEmptyCashCount.expertUserId, kioskEmptyCashCount.code, kioskEmptyCashCount.emptyKioskId, Convert.ToInt32( _userId));


            //int resp = opr.SaveKioskEmptyCashCounts(dt, kioskEmptyCashCount.kioskId, kioskEmptyCashCount.expertUserId, kioskEmptyCashCount.code, kioskEmptyCashCount.emptyKioskId);
            if (resp == 0)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else if (resp == -1)
            {
                errorCode = ReturnCodes.APPROCE_CODE_INCORRECT;
            }
            else if (resp == -2)
            {
                errorCode = ReturnCodes.CODE_CORRECT_OPERATION_FAIL;
            }
            else
            {
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;
            }

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveKioskEmptyCashCounts");
        }


        return data;
    }





    //private JObject SaveKioskEmptyCashCounts(InputListSaveKioskEmptyCashCount kioskEmptyCashCount)
    //{
    //    DataTable dt = new DataTable();
    //    dt.Columns.Add(new DataColumn("KioskId", typeof(System.Int32)));
    //    dt.Columns.Add(new DataColumn("ExpertUserId", typeof(System.Int32)));
    //    dt.Columns.Add(new DataColumn("CashTypeId", typeof(System.Int32)));
    //    dt.Columns.Add(new DataColumn("Count", typeof(System.Int32)));
    //    dt.Columns.Add(new DataColumn("KioskEmptyId", typeof(System.Int32)));


    //    JObject data = new JObject();
    //    try
    //    {
    //        for (int i = 0; i < kioskEmptyCashCount.cashCounts.Count; i++)
    //        {
    //            dt.Rows.Add(kioskEmptyCashCount.cashCounts[i].kioskId,
    //                kioskEmptyCashCount.cashCounts[i].expertUserId,
    //                kioskEmptyCashCount.cashCounts[i].cashTypeId,
    //                kioskEmptyCashCount.cashCounts[i].count,
    //                kioskEmptyCashCount.cashCounts[i].kioskEmptyId);
    //        }


    //        AccountOpr opr = new AccountOpr();
    //        int resp = opr.SaveKioskEmptyCashCounts(dt, kioskEmptyCashCount.kioskId, kioskEmptyCashCount.expertUserId, kioskEmptyCashCount.code, kioskEmptyCashCount.emptyKioskId);
               

    //        //int resp = opr.SaveKioskEmptyCashCounts(dt, kioskEmptyCashCount.kioskId, kioskEmptyCashCount.expertUserId, kioskEmptyCashCount.code, kioskEmptyCashCount.emptyKioskId);
    //        if (resp == 0)
    //        {
    //            errorCode = ReturnCodes.SUCCESSFULL;
    //        }
    //        else if (resp == -1)
    //        {
    //            errorCode = ReturnCodes.APPROCE_CODE_INCORRECT;
    //        }
    //        else if (resp == -2)
    //        {
    //            errorCode = ReturnCodes.CODE_CORRECT_OPERATION_FAIL;
    //        }
    //        else
    //        {
    //            errorCode = ReturnCodes.SERVER_SIDE_ERROR;
    //        }

    //    }
    //    catch (Exception exp)
    //    {
    //        errorCode = ReturnCodes.SYSTEM_ERROR;
    //        Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveKioskEmptyCashCounts");
    //    }


    //    return data;
    //}

    private JObject GetStatus()
    {
        JObject data = new JObject();
        try
        {
            AccountOpr koskOpr = new AccountOpr();
            StatusCollection status = koskOpr.GetStatusOrder();
            koskOpr.Dispose();
            koskOpr = null;

            if (status != null & status.Count > 0)
            {
                data = status.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetInstutionNames");
        }
        return data;
    }

    //SearchKioskDispenserCashCount
    private JObject SearchKioskDispenserCashCount(InputListKioskDispenserCashCounts listDispenserCashCount)
    {

        JObject data = new JObject();

        try
        {
            int recordCount = 0;
            int pageCount = 0;

            AccountOpr opr = new AccountOpr();

            KioskDispenserCashCountCollection cashCounts = opr.SearchKioskDispenserCashCountOrder(listDispenserCashCount.KioskId,
                                                                                             listDispenserCashCount.SearchText,
                                                                                             listDispenserCashCount.InstutionId,
                                                                                              listDispenserCashCount.NumberOfItemsPerPage,
                                                                                              listDispenserCashCount.CurrentPageNum,
                                                                                              ref recordCount,
                                                                                              ref pageCount,
                                                                                              listDispenserCashCount.OrderSelectionColumn,
                                                                                              listDispenserCashCount.DescAsc,
                                                                                              listDispenserCashCount.AdminUserId,
                                                                                              listDispenserCashCount.OrderType);

            opr.Dispose();
            opr = null;

            if (cashCounts != null & cashCounts.Count > 0)
            {
                data = cashCounts.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskDispenserCashCount");
        }
        return data;
    }

    //GetKioskDispenserCashCount
    private JObject GetKioskDispenserCashCount(string Id)
    {
        JObject data = new JObject();
        try
        {
            AccountOpr opr = new AccountOpr();
            CashDispenser dispensercash = opr.GetKioskDispenserCashCountOrder(Id);
            opr.Dispose();
            opr = null;

            if (dispensercash != null)
            {
                data = GetDispenserCashCountJSON(dispensercash);
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "GetKiosk");
        }
        return data;
    }

    public JObject GetDispenserCashCountJSON(CashDispenser dispenserCash)
    {
        JObject jObjectDis = new JObject();
        string a = JsonConvert.SerializeObject(dispenserCash, Newtonsoft.Json.Formatting.None);
        JObject x = JObject.Parse(a);
        jObjectDis.Add("GetKioskCashDispenser", x);
        return jObjectDis;
    }


    //UpdateDispenserCashCount
    private JObject UpdateDispenserCashCount(InputListSaveControlCashNotification kioskEmptyCashCount)
    {

        //SaveDispenserCashCountOrder
        JObject data = new JObject();
        try
        {

            AccountOpr opr = new AccountOpr();
            int resp = opr.UpdateDispenserCashCountOrder(kioskEmptyCashCount.kioskId,
                                                        kioskEmptyCashCount.expertUserId,
                                                        kioskEmptyCashCount.firstDenomNumber,
                                                        kioskEmptyCashCount.secondDenomNumber,
                                                        kioskEmptyCashCount.thirdDenomNumber,
                                                        kioskEmptyCashCount.fourthDenomNumber,
                                                        kioskEmptyCashCount.fifthDenomNumber,
                                                        kioskEmptyCashCount.sixthDenomNumber,
                                                        kioskEmptyCashCount.seventhDenomNumber,
                                                        kioskEmptyCashCount.eighthDenomNumber,
                                                        kioskEmptyCashCount.ninthDenomNumber,
                                                        kioskEmptyCashCount.tenthDenomNumber,
                                                        kioskEmptyCashCount.eleventhDenomNumber,
                                                        kioskEmptyCashCount.twelfthDenomNumber,
                                                        kioskEmptyCashCount.cassette1Status,
                                                        kioskEmptyCashCount.cassette2Status,
                                                        kioskEmptyCashCount.cassette3Status,
                                                        kioskEmptyCashCount.cassette4Status,
                                                        kioskEmptyCashCount.hopper1Status,
                                                        kioskEmptyCashCount.hopper2Status,
                                                        kioskEmptyCashCount.hopper3Status,
                                                        kioskEmptyCashCount.hopper4Status,
                                                        kioskEmptyCashCount.totalAmount
                //,kioskEmptyCashCount.approvedCodeapprovedCode
                                                        );
            SaveDispenserCashCount(kioskEmptyCashCount);
            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

            opr.Dispose();
            opr = null;

            if (resp == 0)
            {
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.UNSUCCESSFUL_UPDATE;
            }
        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveKioskSettings");
        }
        return data;
    }

    //SaveDispenserCashCount
    private JObject SaveDispenserCashCount(InputListSaveControlCashNotification inputListSaveDispenserCashCount)
    {
        JObject data = new JObject();
        try
        {
            AccountOpr opr = new AccountOpr();
            int resp = opr.SaveDispenserCashCountOrder(inputListSaveDispenserCashCount.kioskId,
                inputListSaveDispenserCashCount.expertUserId,
                inputListSaveDispenserCashCount.firstDenomNumber,
                inputListSaveDispenserCashCount.firstDenomNumberOld,
                inputListSaveDispenserCashCount.secondDenomNumber,
                inputListSaveDispenserCashCount.secondDenomNumberOld,
                inputListSaveDispenserCashCount.thirdDenomNumber,
                inputListSaveDispenserCashCount.thirdDenomNumberOld,
                inputListSaveDispenserCashCount.fourthDenomNumber,
                inputListSaveDispenserCashCount.fourthDenomNumberOld,
                inputListSaveDispenserCashCount.fifthDenomNumber,
                inputListSaveDispenserCashCount.fifthDenomNumberOld,
                inputListSaveDispenserCashCount.sixthDenomNumber,
                inputListSaveDispenserCashCount.sixthDenomNumberOld,
                inputListSaveDispenserCashCount.seventhDenomNumber,
                inputListSaveDispenserCashCount.seventhDenomNumberOld,
                inputListSaveDispenserCashCount.eighthDenomNumber,
                inputListSaveDispenserCashCount.eighthDenomNumberOld,
                inputListSaveDispenserCashCount.ninthDenomNumber,
                inputListSaveDispenserCashCount.ninthDenomNumberOld,
                inputListSaveDispenserCashCount.tenthDenomNumber,
                inputListSaveDispenserCashCount.tenthDenomNumberOld,
                inputListSaveDispenserCashCount.eleventhDenomNumber,
                inputListSaveDispenserCashCount.eleventhDenomNumberOld,
                inputListSaveDispenserCashCount.twelfthDenomNumber,
                inputListSaveDispenserCashCount.twelfthDenomNumberOld
                );







            if (resp == (int)ManagementScreenErrorCodes.SystemError)
                errorCode = ReturnCodes.SERVER_SIDE_ERROR;

            opr.Dispose();
            opr = null;

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SaveDispenserCashCountOrder");
        }
        return data;
    }

    //SearchKioskDispenserMonitoring
    private JObject SearchKioskDispenserMonitoring(InputListKioskDispenserMonitoring listKioskDispenserMonitoring)
    {

        JObject data = new JObject();

        try
        {
            int recordCount = 0;
            int pageCount = 0;

            AccountOpr opr = new AccountOpr();

            KioskDispenserMonitoringCollection cashCounts = opr.SearchKioskDispenserMonitoringOrder(listKioskDispenserMonitoring.SearchText,
                                                                                                    listKioskDispenserMonitoring.InstutionId,
                                                                                                    listKioskDispenserMonitoring.NumberOfItemsPerPage,
                                                                                                    listKioskDispenserMonitoring.CurrentPageNum,
                                                                                                    ref recordCount,
                                                                                                    ref pageCount,
                                                                                                    listKioskDispenserMonitoring.OrderSelectionColumn,
                                                                                                    listKioskDispenserMonitoring.DescAsc,
                                                                                                    listKioskDispenserMonitoring.StartDate,
                                                                                                    listKioskDispenserMonitoring.EndDate,
                                                                                                    listKioskDispenserMonitoring.AdminUserId,
                                                                                                    listKioskDispenserMonitoring.OrderType);


            opr.Dispose();
            opr = null;

            if (cashCounts != null & cashCounts.Count > 0)
            {
                data = cashCounts.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskDispenserMonitoring");
        }
        return data;
    }



    //SearchKioskCashBoxCashCountError InputListKioskDispenserCashCounts
    private JObject SearchKioskCashBoxCashCountError(InputListKioskDispenserCashCounts listKioskCashBoxCashCountError)
    {

        JObject data = new JObject();

        try
        {
            int recordCount = 0;
            int pageCount = 0;

            AccountOpr opr = new AccountOpr();

            KioskCashBoxCashCountErrorCollection cashCounts = opr.SearchKioskCashBoxCashCountErrorOrder(listKioskCashBoxCashCountError.KioskId,
                                                                                                  listKioskCashBoxCashCountError.SearchText,
                                                                                                  listKioskCashBoxCashCountError.StartDate,
                                                                                                  listKioskCashBoxCashCountError.EndDate,
                                                                                                  listKioskCashBoxCashCountError.InstutionId,
                                                                                                  listKioskCashBoxCashCountError.NumberOfItemsPerPage,
                                                                                                  listKioskCashBoxCashCountError.CurrentPageNum,
                                                                                                  ref recordCount,
                                                                                                  ref pageCount,
                                                                                                  listKioskCashBoxCashCountError.OrderSelectionColumn,
                                                                                                  listKioskCashBoxCashCountError.DescAsc,
                                                                                                  listKioskCashBoxCashCountError.AdminUserId,
                                                                                                  listKioskCashBoxCashCountError.OrderType);

            opr.Dispose();
            opr = null;

            if (cashCounts != null & cashCounts.Count > 0)
            {
                data = cashCounts.GetJSON();
                errorCode = ReturnCodes.SUCCESSFULL;
            }
            else
            {
                errorCode = ReturnCodes.NO_DATA_FOUND;
            }

            data = WebServiceUtils.AddPaggingNodes(data, recordCount, pageCount);

        }
        catch (Exception exp)
        {
            errorCode = ReturnCodes.SYSTEM_ERROR;
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "SearchKioskCashBoxCashCountError");
        }
        return data;
    }
}