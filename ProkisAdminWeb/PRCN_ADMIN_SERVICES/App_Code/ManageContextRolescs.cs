﻿using Newtonsoft.Json.Linq;
using PRCNCORE.Backend;
using PRCNCORE.Constants;
using PRCNCORE.Roles;
using PRCNCORE.RoleSubMenus;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for ManageContextRolescs
/// </summary>


public class ContextMenuPermisionDetials
{
    public ContextMenuPermisionDetials()
    {

    }
    public string eventName { get; set; }
    public string eventValue { get; set; }
    public string eventUniqueId { get; set; }
    public string eventActionValue { get; set; }
}

public class Condition
{
    public Condition(string roleName, string value, ConditionTypes condition, string columnName)
    {
        this.roleName = roleName;
        this.value = value;
        this.condition = condition;
        this.columnName = columnName;
    }

    public string roleName { get; set; }
    public string value { get; set; }
    public ConditionTypes condition { get; set; }
    public string columnName { get; set; }
}
public enum ConditionTypes
{
    equals,
    greaterThen,
    smallerThen,
    notEqual,
    contains,
    notNull
}
public class ManageContextRolescs
{
    public ManageContextRolescs()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public void EditByRoles(int function, JObject data, long userId, string pageUrl)
    {
        try
        {
            // RoleOpr roleopr = new RoleOpr();

            //var m = roleopr.GetRole(Convert.ToInt32(userId));
            //var m2 = roleopr.GetRoles();
            //var m3 = roleopr.GetRoleNames();
            //var m4 = roleopr.GetAdminRoleNames();
            UserOpr userOpr = new UserOpr();
            var user = userOpr.GetUser(Convert.ToInt32(userId));
            
            List<ContextMenuPermisionDetials> listcontextMenuPerm = new List<ContextMenuPermisionDetials>(); // Tableda birden fazla context itemli
            bool customArraysName = false;

            string arraysName = "";
           
            foreach (var x in data)
            {
                if (x.Value.Type.ToString() == "Array")
                {
                    arraysName = x.Key;
                    break;
                }
            }

         

            //Rollere göre default roles

            SubMenuOpr submenu = new SubMenuOpr();
            RoleSubMenuContextCollection menuRoles = submenu.GetRoleMenusContext(Convert.ToInt32(user.RoleId));//userId den user.Id ye çekildi

            foreach (var dd in menuRoles)
            {
                if (dd.SubMenuURL == pageUrl)
                {
                    if ((dd.RoleActionValue != 0))
                    {
                        ContextMenuPermisionDetials contextMenuPerm = new ContextMenuPermisionDetials();
                        contextMenuPerm.eventValue = dd.RoleLabel;
                        contextMenuPerm.eventName = dd.RoleActionName;
                        contextMenuPerm.eventUniqueId = dd.UniqueId;
                        contextMenuPerm.eventActionValue = dd.RoleActionValue.ToString();
                        listcontextMenuPerm.Add(contextMenuPerm);
                    }
                    else if ((dd.Status == 2 && dd.OldRoleActionValue == 1 && dd.RoleActionValue == 0))
                    {
                        ContextMenuPermisionDetials contextMenuPerm = new ContextMenuPermisionDetials();
                        contextMenuPerm.eventValue = dd.RoleLabel;
                        contextMenuPerm.eventName = dd.RoleActionName;
                        contextMenuPerm.eventUniqueId = dd.UniqueId;
                        contextMenuPerm.eventActionValue = dd.OldRoleActionValue.ToString(); // old RoleActionValue has been setted
                        listcontextMenuPerm.Add(contextMenuPerm);
                    }
                }
            }
            if (function == 3064)
            {//kiosk kurum mutabakat rolleri tarife işlemdeki roller gibi atandı
                foreach (var item in listcontextMenuPerm)
                {
                    if(item.eventName == "TransactionCondition")
                        data.Add("TransactionCondition", true);
                    if (item.eventName == "Detail")
                        data.Add("Detail", true);

                }

            }
            List<Condition> conditionList = new List<Condition>();

            // Exceptional Roles

            switch ((FC)function)
            {
                case FC.LIST_ACCOUNT:
                    break;
                case FC.LIST_KIOSK_COMMANDS:
                    conditionList.Add(new Condition("DeleteKioskCommand", "1", ConditionTypes.equals, "StatusId"));
                    break;
                case FC.LIST_TRANSACTION:
                    conditionList.Add(new Condition("CustomerEmails", "0", ConditionTypes.notNull, "AboneNo"));
                    break;
                //conditionList.Add(new Condition("MoneyCodeControl", "Para Kod Üretiliyor", ConditionTypes.equals, "ProcessStatus"));
                //conditionList.Add(new Condition("TelephoneNumber", "0", ConditionTypes.notNull, "ProcessStatus"));

                //conditionList.Add(new Condition("MoneyCodeControl", "Yükleme Basarili", ConditionTypes.contains, "ProcessStatus"));
                //conditionList.Add(new Condition("MoneyCodeControl", "Para Girisi Yapildi", ConditionTypes.equals, "ProcessStatus"));
                //conditionList.Add(new Condition("MoneyCodeControl", "Tamamlama", ConditionTypes.contains, "ProcessStatus"));

                //conditionList.Add(new Condition("TelephoneNumber", "Para Kod Üretiliyor", ConditionTypes.equals, "ProcessStatus"));
                //conditionList.Add(new Condition("TelephoneNumber", "Yükleme Basarili", ConditionTypes.contains, "ProcessStatus"));
                //conditionList.Add(new Condition("TelephoneNumber", "Para Girisi Yapildi", ConditionTypes.equals, "ProcessStatus"));
                //conditionList.Add(new Condition("TelephoneNumber", "Tamamlama", ConditionTypes.contains, "ProcessStatus"));

                ;
                case FC.GET_SYSTEM_SETTING:
                    conditionList.Add(new Condition("ApprovedSystemSetting", userId.ToString(), ConditionTypes.notEqual, "CreatedUserId"));
                    conditionList.Add(new Condition("ApprovedSystemSetting", "0", ConditionTypes.equals, "OldValue"));
                    conditionList.Add(new Condition("DeleteSystemSetting", "0", ConditionTypes.equals, "OldValue"));

                    break;
                case FC.LIST_KIOSK_REFERENCE_SYSTEM:
                    conditionList.Add(new Condition("DeleteKioskReferenceSystem", "Sifirlandi", ConditionTypes.notEqual, "Status"));
                    break;

                case FC.LIST_FAILED_TRANSACTION:
                    conditionList.Add(new Condition("MoneyCodeControl", "Para Kod Üretiliyor", ConditionTypes.equals, "ProcessStatus"));
                    conditionList.Add(new Condition("MoneyCodeControl", "Yükleme Basarili", ConditionTypes.contains, "ProcessStatus"));
                    conditionList.Add(new Condition("MoneyCodeControl", "Para Girisi Yapildi", ConditionTypes.equals, "ProcessStatus"));
                    conditionList.Add(new Condition("MoneyCodeControl", "Tamamlama", ConditionTypes.contains, "ProcessStatus"));

                    conditionList.Add(new Condition("TelephoneNumber", "Para Kod Üretiliyor", ConditionTypes.equals, "ProcessStatus"));
                    conditionList.Add(new Condition("TelephoneNumber", "Yükleme Basarili", ConditionTypes.contains, "ProcessStatus"));
                    conditionList.Add(new Condition("TelephoneNumber", "Para Girisi Yapildi", ConditionTypes.equals, "ProcessStatus"));
                    conditionList.Add(new Condition("TelephoneNumber", "Tamamlama", ConditionTypes.contains, "ProcessStatus"));

                    break;
                case FC.LIST_CARD_REPAIR:
                    conditionList.Add(new Condition("CardRepairEdit", "Onarim Bekleniyor", ConditionTypes.equals, "Status"));
                    conditionList.Add(new Condition("CardRepairDetail", "Onarildi", ConditionTypes.equals, "Status"));

                    break;
                case FC.LIST_KIOSK_LOCAL_LOG:
                    conditionList.Add(new Condition("MoneyEntries", "Log Kontrolü Saglandi", ConditionTypes.equals, "LogStatusText"));

                    break;
                case FC.LIST_MUTABAKAT:
                    //
                    //conditionList.Add(new Condition("EditKioskEmptyStatus", "1", ConditionTypes.equals, "KioskEmptyStatusId"));
                    //statusId = 2 Onay Bekleniyor
                    conditionList.Add(new Condition("EnterDetail", "2", ConditionTypes.equals, "StatusId"));
                    conditionList.Add(new Condition("CenterDetail", "2", ConditionTypes.equals, "StatusId")); 
                    conditionList.Add(new Condition("DifferenceDetail", "2", ConditionTypes.equals, "StatusId"));
                    conditionList.Add(new Condition("EditSpecial", "1", ConditionTypes.notEqual, "StatusId"));
                    conditionList.Add(new Condition("EditCollectedMoney", "1", ConditionTypes.equals, "StatusId"));
                    conditionList.Add(new Condition("UndoApprovedCollectedMoney", "2", ConditionTypes.equals, "StatusId"));
                    break;
                case FC.LIST_MONEY_CODE:
                    conditionList.Add(new Condition("ApproveMoneyCode", "4", ConditionTypes.equals, "StatusId"));
                    conditionList.Add(new Condition("ApproveMoneyCode", userId.ToString(), ConditionTypes.notEqual, "GeneratedUserId"));
                    break;
                case FC.CONTROL_CASH_NOTIFICATION:
                    //Sadece para girişi bekleniyor statüsündeki 
                    conditionList.Add(new Condition("DeleteControlCashNotification", "1", ConditionTypes.equals, "StatusId"));

                    conditionList.Add(new Condition("ControlCashEdit", "1", ConditionTypes.equals, "StatusId"));
                    break;

                default:
                    break;

            }


            if (!customArraysName) // Arrays olmayıp tabloya bastıgımız data varsa if e girmesin.
                data = AddContextRoles(data, arraysName, listcontextMenuPerm, conditionList);

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    public List<ContextMenuPermisionDetials> DeepCopy(List<ContextMenuPermisionDetials> newList)
    {
        List<ContextMenuPermisionDetials> newListC = new List<ContextMenuPermisionDetials>();

        foreach (var item in newList)
        {
            ContextMenuPermisionDetials detals = new ContextMenuPermisionDetials();
            detals.eventActionValue = item.eventActionValue;
            detals.eventName = item.eventName;
            detals.eventUniqueId = item.eventUniqueId;
            detals.eventValue = item.eventValue;
            newListC.Add(detals);
        }
        return newListC;
    }

    public JObject AddContextRoles(JObject data, string propName, List<ContextMenuPermisionDetials> contextRow, List<Condition> conditionList)
    {
        JArray myarray = (JArray)data[propName];
        if (myarray != null && myarray.Count > 0)
        {
            for (var i = 0; i < myarray.Count; i++)
            {
                //ColumnName ile ilişkili olarak veri ile ilişkili role value değiştirilmelidir.
                JObject item = (JObject)myarray[i];
                List<ContextMenuPermisionDetials> contextData = DeepCopy(contextRow);
                //if(contextData[0].eventActionValue == "1")
                //{
                //}
                //if (contextRowModified[0].eventActionValue == "1")
                //{
                //}
                List<ContextMenuPermisionDetials> contextRowModified = CheckCollectionConditions(item, conditionList, contextData);
               
                item.Add(new JProperty("ManageContextRoles", JToken.FromObject(contextRowModified)));
            }
        }
        return data;
    }

    private List<ContextMenuPermisionDetials> CheckCollectionConditions(JObject item, List<Condition> conditions, List<ContextMenuPermisionDetials> contextRowNew)
    {
        try
        {
            //List<ContextMenuPermisionDetials> contextRowMod = new List<ContextMenuPermisionDetials>();
            foreach (var dd in conditions)
            {
                string valueOfRow = "";
                var value = item.GetValue(dd.columnName);
                if (value != null) valueOfRow = value.ToString();
                contextRowNew = ConsiderValueAndCondition(dd, valueOfRow, contextRowNew);

            }
            return contextRowNew;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private List<ContextMenuPermisionDetials> ConsiderValueAndCondition(Condition dd, string value, List<ContextMenuPermisionDetials> contextRowEdit)
    {
        try
        {
            foreach (var item in contextRowEdit)
            {
                if (item.eventName == dd.roleName)
                {
                    if (value != "")
                    {
                        int valueOfItem = Convert.ToInt32(item.eventActionValue);
                        if (dd.condition == ConditionTypes.equals)
                        {
                            item.eventActionValue = (value == dd.value) ? (valueOfItem * 1).ToString() : (valueOfItem * 0).ToString();
                        }
                        else if (dd.condition == ConditionTypes.greaterThen)
                        {
                            item.eventActionValue = (Convert.ToDouble(value) > Convert.ToDouble(dd.value)) ? (valueOfItem * 1).ToString() : (valueOfItem * 0).ToString();
                        }
                        else if (dd.condition == ConditionTypes.smallerThen)
                        {
                            item.eventActionValue = (Convert.ToDouble(value) < Convert.ToDouble(dd.value)) ? (valueOfItem * 1).ToString() : (valueOfItem * 0).ToString();
                        }
                        else if (dd.condition == ConditionTypes.notEqual)
                        {
                            item.eventActionValue = (value != dd.value) ? (valueOfItem * 1).ToString() : (valueOfItem * 0).ToString();
                        }
                        else if (dd.condition == ConditionTypes.contains)
                        {
                            item.eventActionValue = value.Contains(dd.value) ? (valueOfItem * 1).ToString() : (valueOfItem * 0).ToString();
                        }
                        else if(dd.condition == ConditionTypes.notNull)
                        {
                            item.eventActionValue = (!String.IsNullOrEmpty(dd.value)) ? (valueOfItem * 1).ToString() : (valueOfItem * 0).ToString();
                        }
                    }
                    else
                        item.eventActionValue = "0";
                }

            }
            return contextRowEdit;
        }
        catch (Exception)
        {
            return null;
        }
    }

}