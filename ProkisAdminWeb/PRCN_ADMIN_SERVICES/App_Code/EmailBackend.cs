﻿
using PRCNCORE.Backend;
using PRCNCORE.Constants;
using PRCNCORE.Utilities;
using SendEmailProject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EmailBackend
/// </summary>
public class EmailBackend
{
    public int SendEmail(string emailAdress, long transactionId)
    {
        try
        {
            using (SendEmailProject.SendEmailSoapClient client = new SendEmailProject.SendEmailSoapClient())
            {
                SendEmailProject.ArrayOfString attachments = new SendEmailProject.ArrayOfString();
                SendEmailProject.ArrayOfString emailAddress = new SendEmailProject.ArrayOfString();
                emailAddress.Add(emailAdress);

                SendEmailProject.DekontFields dekontFields = new SendEmailProject.DekontFields();
                dekontFields.AccountAddress = "";

                EmailOpr emailOPr = new EmailOpr();
                PRCNCORE.Utilities.DekontFields dekontfields = emailOPr.GetDekontFieldsByTransactionId(transactionId);
                string accountAddress = emailOPr.GetSettingNameByLabel("DekontAccountAddress");
                string accountNumber = emailOPr.GetSettingNameByLabel("DekontAccountNo");
                string address = emailOPr.GetSettingNameByLabel("DekontAddress");

                emailOPr.Dispose();

                if (dekontfields != null && dekontfields.TransactionId != null)
                {
                    dekontFields.AccountAddress = accountAddress;
                    dekontFields.Commision = dekontfields.CommisionFee;
                    dekontFields.AccountNumber = accountNumber;
                    dekontFields.Address = address;
                    dekontFields.Amount = dekontfields.InstitutionAmount;
                    dekontFields.CreditCardHolder = dekontfields.CreditCardCustomer;
                    dekontFields.CreditCardNumber = dekontfields.CreditCardNumber;
                    dekontFields.CustomerName = dekontfields.CustomerName;
                    dekontFields.CustomerNumber = dekontfields.CustomerId;

                    dekontFields.InsertionDate = DateTime.Now.ToString();
                    dekontFields.InstitutionName = dekontfields.Institution;
                    dekontFields.InstitutionNumber = dekontfields.InstitutionId;
                    dekontFields.KioskName = dekontfields.KioskName;
                    dekontFields.PaymentChannel = "Kiosk";
                    dekontFields.PaymentType = dekontfields.PaymentType;
                    dekontFields.ReferenceNumber = dekontfields.TransactionId;
                    dekontFields.TotalAmount = dekontfields.TotalAmount;
                    dekontFields.TransactionDate = dekontfields.InsertionDate;
                    dekontFields.TransactionNumber = dekontfields.TransactionId;
                    dekontFields.UsageFee = dekontfields.UsageFee;

                    SendEmailProject.EmailResult result = client.SendEmailDekont("info@birlesikodeme.com", "Birleşik Ödeme Hizmetleri", "Dekontunuz", "", 0, attachments, emailAddress, dekontFields);

                    return result.ErrorCode;
                }
                else
                    return (int)GeneralResult.FAIL;


            }
        }
        catch (Exception ex)
        {
            return -1;
        }
    }
	public EmailBackend()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}