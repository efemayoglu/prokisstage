﻿using PRCNCORE.BankActions;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for ListBankActions
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class ListBankActions : System.Web.Services.WebService {

    public ListBankActions () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public BankActionsListAski GetBankActions(int institutionId, DateTime startDate, DateTime endDate)
    {
        try
        {
            DataAccessLayer DAL=new DataAccessLayer();
            BankActionsListAski ACT=DAL.GetBankActionsInstitution(institutionId, startDate, endDate);
            ACT.actionDetails.TableName="BankActionsAski";
            return ACT;
        }
        catch(Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), ex, "GetBankActions");
            return null;
        }
    }
    
}
