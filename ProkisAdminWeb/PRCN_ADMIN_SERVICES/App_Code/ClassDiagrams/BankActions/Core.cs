﻿using Microsoft.ApplicationBlocks.Data;
using PRCNCORE.Accounts;
using PRCNCORE.Backend;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Core
/// </summary>
public class Core 
{


	public Core()
	{
               

	}

    private string GetDBConnectionString()
    {
        return Utility.DecryptString(Utility.GetConfigValue("DBConnectionText"));
    }


    public DataTable GetDataFromDB(List<StoredProcParams> parameters, string procName)
    {

        SqlConnection conn = null;
        SqlDataReader rdr = null;
        try
        {
            
            //conn = new SqlConnection("Server=(local);DataBase=master;Integrated Security=SSPI");
            conn = new SqlConnection(GetDBConnectionString());
            conn.Open();
            SqlCommand cmd = new SqlCommand(procName, conn);
            cmd.CommandType = CommandType.StoredProcedure;

            for (int i = 0; i < parameters.Count; i++)
            {
                cmd.Parameters.Add(new SqlParameter(parameters[i].paramName, parameters[i].value));
            }
            
            rdr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(rdr);
            return dt;

        }
        finally
        {
            if (conn != null)
            {
                conn.Close();
            }
            if (rdr != null)
            {
                rdr.Close();
            }

        }
    }

}