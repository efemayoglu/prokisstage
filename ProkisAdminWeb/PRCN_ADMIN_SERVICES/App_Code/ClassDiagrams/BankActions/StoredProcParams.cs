﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for StoredProcParams
/// </summary>
public class StoredProcParams
{
    public string paramName { get; set; }
    public object value { get; set; }
	public StoredProcParams()
	{
		
	}

    public StoredProcParams(string paramName, object value)
    {
        this.paramName = paramName;
        this.value = value;
    }

}