﻿using PRCNCORE.BankActions;
using PRCNCORE.Constants;
using PRCNCORE.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DataAccessLayer
/// </summary>
public class DataAccessLayer
{
	public DataAccessLayer()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public BankActionsListAski GetBankActionsInstitution(int institutionId, DateTime startDate, DateTime endDate)
    {
        try
        {
            Core core = new Core();
            List<StoredProcParams> parameters = new List<StoredProcParams>();

            parameters.Add(new StoredProcParams("@instutionId", institutionId));
            parameters.Add(new StoredProcParams("@startDate", startDate));
            parameters.Add(new StoredProcParams("@endDate", endDate));
            parameters.Add(new StoredProcParams("@paymentTypeId", 1));
            parameters.Add(new StoredProcParams("@cardTypeId", 0));
            parameters.Add(new StoredProcParams("@orderSelectionColumn", 1));
            parameters.Add(new StoredProcParams("@orderSelectionDescAsc", 1));
            parameters.Add(new StoredProcParams("@completeStatusId", 12));

            DataTable result = new DataTable();
            result = core.GetDataFromDB(parameters, "[pk].[search_bankactions_detail]");

            List<BankActionListAskiList> actions = new List<BankActionListAskiList>();
           
            /*
            while (result.Read())
            {
                BankActionListAskiList actionList = new BankActionListAskiList();
                //actionList.bankReferenceNumber=result["ReferenceNumber"].ToString();
                //actionList.customerId=result["CustomerId"].ToString();
                //actionList.insProcessDate = result["ProcessDate"].ToString();
                //actionList.institutionAmount = Convert.ToDecimal(result["institutionAmount"]);
                actionList.institutionId = Convert.ToInt32(result["InstutionId"]);
                actionList.kioskName = result["KioskName"].ToString();
                //actionList.status = result["Status"].ToString();
                actionList.transactionId = result["TransactionId"].ToString();
                actions.Add(actionList);
            }
            */
            BankActionsListAski processResult;
            if (result.Rows.Count>0)
                processResult = new BankActionsListAski((int)ProcessResult.Successful, result);
            else
                processResult = new BankActionsListAski((int)ProcessResult.Fail, null);

            return processResult;

        }
        catch(Exception ex)
        {
            Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), ex, "GetBankActionsInstitution");

            BankActionsListAski processResult = new BankActionsListAski((int)ProcessResult.Exception, null);
            return processResult;
        }
        finally
        {

        }
    }
}