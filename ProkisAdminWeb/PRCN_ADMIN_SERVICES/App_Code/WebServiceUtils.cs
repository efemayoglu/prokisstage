﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using PRCNCORE.Utilities;
using PRCNCORE.Constants;
using PRCNCORE.Backend;
using System.Configuration;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using PRCNCORE.Input;
using System.Collections.Specialized;

/// <summary>
/// Summary description for WebServiceUtils
/// </summary>
public class WebServiceUtils
{

    public WebServiceUtils()
    {

    }

    public enum OutputFormats
    {
        XML = 1,
        JSON = 2
    };

    public enum ErrorCodes
    {
        Success = 0,
        MsisdnError = 1,
        MemberError = 2,
        CredentialError = 3,
        SaleTransactionError = 4,
        SaveMemberError = 5,
        ParameterError = 6,
        ExistingMsisdn = 7,
        ExistingSale = 8,
        PhrServiceError = 9,
        NoDataFound = 10

    }

    public static string LoadParameters(HttpRequest request, InputType inputType = InputType.Clear)
    {
        string reqString = "";

        if (request.HttpMethod == "POST")
        {
            try
            {
                StreamReader sr = new StreamReader(request.InputStream, request.ContentEncoding);
                reqString = sr.ReadToEnd();
                sr.Close();
                sr.Dispose();

                if (inputType == InputType.Encrypted)
                    reqString = Utility.DecryptString(reqString);
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "WSULoadParameters");
            }
        }

        return reqString;
    }

    public static bool CheckExpiredAccessTokenSecurity(string sessionToken, Int64 adminUserId)
    {
        bool result = false;

        if (!string.IsNullOrEmpty(sessionToken) && !string.IsNullOrEmpty(sessionToken) && CheckExpiredAccessTokenfromDB(sessionToken, adminUserId))
            result = true;

        return result;
    }

    private static bool CheckExpiredAccessTokenfromDB(string sessionToken, Int64 adminUserId)
    {
        bool result = false;

        AuthenticateOpr authenticateOpr = new AuthenticateOpr();

        result = authenticateOpr.ControlExpiredAccessToken(sessionToken,adminUserId);

        authenticateOpr.Dispose();
        return result;
    }

    public static bool CheckAccessTokenSecurity(string sessionToken, Int64 adminUserId)
    {
        bool result = false;

        if (!string.IsNullOrEmpty(sessionToken) && !string.IsNullOrEmpty(sessionToken) && CheckAccessTokenfromDB(sessionToken,adminUserId))
            result = true;

        return result;
    }

    private static bool CheckAccessTokenfromDB(string sessionToken, Int64 adminUserId)
    {
        bool result = false;

        AuthenticateOpr authenticateOpr = new AuthenticateOpr();

        result = authenticateOpr.ControlAccessToken(sessionToken,adminUserId);

        authenticateOpr.Dispose();
        return result;
    }



    public static NameValueCollection LoadParameters(HttpRequest request, string dummy, InputType inputType = InputType.Clear)
    {
        string reqString = "";
        NameValueCollection data = null;

        if (request.HttpMethod == "POST")
        {
            try
            {
                data = request.Form;

                if (inputType == InputType.Encrypted)
                    reqString = Utility.DecryptString(reqString);
            }
            catch (Exception exp)
            {
                Utility.WriteErrorLog(Utility.GetConfigValue("ErrorLogPath"), exp, "WSULoadParameters1");

            }
        }

        return data;
    }

    public static void SendResponse(JObject responseData, HttpResponse response, bool encryptResponse)
    {
        response.Clear();

        string outputText = "";
        string mimeType = "application/json";

        outputText = responseData.ToString();

        if (encryptResponse)
        {
            outputText = Utility.EncryptString(outputText);
            mimeType = "text/plain";
        }

        response.Clear();
        response.ContentType = mimeType;
        response.Write(outputText);

        response.End();
    }

    public static string GetReturnCodeDescription(ReturnCodes returnCode)
    {
        string errDesc = "";

        if (returnCode == ReturnCodes.SUCCESSFULL)
            errDesc = "İşlem Başarılı";
        else if (returnCode == ReturnCodes.INVALID_BROWSER)
            errDesc = "Invalid Browser";
        else if (returnCode == ReturnCodes.NO_DATA_FOUND)
            errDesc = "Veri Bulunamadı";
        else if (returnCode == ReturnCodes.CREDENTIALS_ERROR)
            errDesc = "Credential Error";
        else if (returnCode == ReturnCodes.INCONSISTENT_AGENT)
            errDesc = "Inconsistent Agent";
        else if (returnCode == ReturnCodes.EXCEED_TRLIMIT_USER_MUST_WAIT)
            errDesc = "Hatalı deneme sayısı aşıldı, Kullanıcı bloklanmıştır.";//qweqwe Süreli bloklamayı bloklanmış gibi gösteriyoruz.
        else if (returnCode == ReturnCodes.EXIST_USER_WRONG_PASS)
            errDesc = "Kullanıcı Adı veya Şifre hatalı!";
        else if (returnCode == ReturnCodes.DEACTIVE_USER)
            errDesc = "Kullanıcı aktif değil!";
        else if (returnCode == ReturnCodes.EXPIRE_USER)
            errDesc = "Kullanıcı expire olmuş!";
        else if (returnCode == ReturnCodes.ASKICODE_NOT_GENERATED)
            errDesc = "ASki Kod Üretilemedi !";
        else if (returnCode == ReturnCodes.APPROCE_CODE_INCORRECT)
            errDesc = "Onay Kodu Hatalı !";
        else if (returnCode == ReturnCodes.UNSUCCESSFUL_UPDATE)
            errDesc = "İşlem Başarısız !";
        else if (returnCode == ReturnCodes.EXIST_USERNAME)
            errDesc = "Kullanıcı Adı kullanımda !";
        else if (returnCode == ReturnCodes.CODE_CORRECT_OPERATION_FAIL)
            errDesc = "İşlem Başarısız !";
        else if (returnCode == ReturnCodes.SERVER_SIDE_ERROR)
            errDesc = "Sunucuda bir hata oluştu !";
        else if (returnCode == ReturnCodes.ITEM_NOT_FOUND)
            errDesc = "İşlem Bulunamadı. Başka kullanıcı tarafından güncellenmiş olabilir !";
        else if (returnCode == ReturnCodes.INVALID_SENDER_ACCOUNT)
            errDesc = "Gönderici aktif değil !";
        else if (returnCode == ReturnCodes.SENDER_HAS_NO_ACCOUNT)
            errDesc = "Gönderici aktif bir hesaba sahip değil !";
        else if (returnCode == ReturnCodes.INVALID_RECEIVER_ACCOUNT)
            errDesc = "Alıcı aktif bir hesap değil !";
        else if (returnCode == ReturnCodes.INSUFFICIENT_BALANCE)
            errDesc = "Yeterli bakiye mevcut değil !";
        else if (returnCode == ReturnCodes.CUSTOMER_EXIST_BL)
            errDesc = "Müşteri Black List için Onay bekliyor !";
        else if (returnCode == ReturnCodes.INVALID_ACCESS_TOKEN)
            errDesc = "Oturumunuz sonlandırılmıştır!";
        else if (returnCode == ReturnCodes.MUTABAKAT_BASARILI)
            errDesc = "Mutabakat Başarıyla Yapılmıştır!";
        else if (returnCode == ReturnCodes.MUTABAKAT_BASARISIZ)
            errDesc = "Mutabakat Yapılamamıştır !";
        return errDesc;
    }

    public static bool CheckCredentials(string apiKey, string apiPass)
    {
        bool result = false;

        if (!string.IsNullOrEmpty(apiKey) && !string.IsNullOrEmpty(apiPass) && CheckTokenfromDB(apiKey, apiPass))
            result = true;


        return result;
    }

    private static bool CheckTokenfromDB(string apiKey, string apiPass)
    {
        bool result = false;

        AuthenticateOpr authenticateOpr = new AuthenticateOpr();

        result = authenticateOpr.ControlApiKeyApiPass(apiKey, apiPass);

        authenticateOpr.Dispose();


        return result;
    }

    public static JObject AddErrorNodes(JObject responseJSON, ReturnCodes errorCode, string errorDescription)
    {
        responseJSON.Add("errorCode", (int)errorCode);
        responseJSON.Add("errorDescription", errorDescription);
        return responseJSON;
    }

    public static JObject AddPaggingNodes(JObject responseJSON, int recordCount, int pageCount)
    {
        responseJSON.Add("recordCount", recordCount);
        responseJSON.Add("pageCount", pageCount);
        return responseJSON;
    }
}